//
//  AlSeerCustomerDashboardViewController.h
//  Salesworx
//
//  Created by Unique Computer Systems on 3/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerOrderHistoryView.h"
#import "CustomerDetailView.h"
#import "CustomerTargetView.h"
#import "CustomerPriceList.h"
#import <MapKit/MapKit.h>
#import <MapKit/MKMapView.h>
#import "MapView.h"
#import "Place.h"
#import "CSMapAnnotation.h"
#import "CustomerTargetView.h"
#import "PCPieChart.h"
#import "OutletPopoverTableViewController.h"
#import "XYPieChart.h"
#import "ZBarReaderViewController.h"
#import "MIMBarGraph.h"
#import "UUChart.h"
#import "PNCircleChart.h"
#import "MedRepElementDescriptionLabel.h"


@interface AlSeerCustomerDashboardViewController : UIViewController<  CustomerOrderHistoryCellDelegate,GridViewDataSource,GridViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate,StringPopOverDelegate,XYPieChartDataSource,XYPieChartDelegate,ZBarReaderViewDelegate,UITableViewDataSource,UITableViewDelegate,BarGraphDelegate,UIActionSheetDelegate>


{
    NSMutableDictionary* customerDict;
    IBOutlet UILabel *monthLbl;
    CustomerDetailView *customerDetailView;
    CustomerTargetView *customerTargetView;
    CustomerOrderHistoryView *customerOrderHistoryView;
    CustomerPriceList* customerPriceListView;

//    MapView* mapView;
    CSMapAnnotation *Annotation;
    UIPopoverController *locationFilterPopOver;

    Place *place;
    double actualAchievementPercent;
    
    
    NSArray *yValuesArray,*xValuesArray;
    NSArray *xTitlesArray;
    NSMutableArray* dsgArr;
    
    NSMutableArray*  salesTargetItemsArray;

    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    PCPieChart * pieChartNew;
    NSMutableArray* components;
    
    NSMutableArray* categoriesArray;
    
    UIActionSheet *actions;

    NSString* isCollectionEnable;
    
    ZBarReaderViewController *reader;

    
    BOOL popoverOriented;
    
    NSInteger month;
    
    
    NSMutableArray* topLevelDataArray;

    
    OutletPopoverTableViewController* outletPopOver;
    UIPopoverController *valuePopOverController,*optionsPopover,*stockPopOver;    
    
    double maxTimeGone;
    
    
    NSMutableArray* orderHistoryArray;
    
    
    IBOutlet UIView *customerDetView;
    
    IBOutlet UIView *salesTargetPieChartView;
    IBOutlet UIView *staticMapView;
    IBOutlet UIView *orderHistoryView;
    IBOutlet UILabel *graphButtomLabel;
    IBOutlet UILabel *detailButtomLabel;
    
    IBOutlet UIButton *targetGraphButton;
    IBOutlet UIButton *targetDetailButton;
    IBOutlet UIView *salesVsTargetDetailView;
    
}
@property (strong, nonatomic) IBOutlet UITableView *orderHistoryTblView;

-(void)fetchCategories;
-(void)testMap;
-(void)fetchSalesStatistics;
- (void)displayActions:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *timegoneLbl;

@property (strong, nonatomic) IBOutlet UIView *chartView;
@property (strong, nonatomic) IBOutlet MedRepPanelTitle *salesTragetMonthLabel;


//@property(strong,nonatomic) IBOutlet     CustomerOrderHistoryView *customerOrderHistoryView;
@property (strong, nonatomic) IBOutlet UIView *customerOrderHistoryCustomView;
@property (strong, nonatomic) IBOutlet MapView *mapView;


@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UITableView *orderHistoryTbl;
@property (strong, nonatomic) IBOutlet UITableView *priceListTbl;
@property (strong, nonatomic) IBOutlet MKMapView *custMapView;
@property(strong,nonatomic)    CLLocationManager* locationManager;


@property (strong, nonatomic) IBOutlet UILabel *customerIdLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerTypeLbl;




//@property(strong,nonatomic)UUChart* chartView;
@property (strong, nonatomic) IBOutlet MIMBarGraph *myBarChart;

@property (strong, nonatomic) IBOutlet UILabel *addressLbl;
@property (strong, nonatomic) IBOutlet UILabel *poBoxLbl;
@property (strong, nonatomic) IBOutlet UILabel *cityLbl;
@property (strong, nonatomic) IBOutlet UILabel *locationLbl;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;



@property (strong, nonatomic) IBOutlet UILabel *avblBalanceLbl;

@property (strong, nonatomic) IBOutlet MedRepElementDescriptionLabel *creditLimitLbl;
@property (strong, nonatomic) IBOutlet UILabel *overDueLbl;
@property (strong, nonatomic) IBOutlet UILabel *pdcDueLbl;



@property (strong, nonatomic) IBOutlet UILabel *overallTargetLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalSalesLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalReturnsLbl;
@property (strong, nonatomic) IBOutlet UILabel *balanceToGoLbl;
@property (strong, nonatomic) IBOutlet UILabel *achievementLbl;


///customer category table view properties
- (IBAction)categoryBtnTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *agencyLbl;

@property (strong, nonatomic) IBOutlet UILabel *monthSalesLbl;
@property (strong, nonatomic) IBOutlet UILabel *monthBtgLbl;

@property (strong, nonatomic) IBOutlet UILabel *targetValueCatLbl;
@property (strong, nonatomic) IBOutlet UILabel *salesValueCatLbl;
@property (strong, nonatomic) IBOutlet UILabel *prevdaySalesCatLbl;
@property (strong, nonatomic) IBOutlet UITableView *targetTableView;

@property (strong, nonatomic) IBOutlet UILabel *achievementCatLbl;

@property (strong, nonatomic) IBOutlet XYPieChart *customerPieChart;

@property(nonatomic, strong) NSArray        *sliceColorsForActivities;
@property(strong,nonatomic) NSString* userCurrencyCode;
@property(nonatomic, strong) NSMutableArray *slices;

@property (strong, nonatomic) IBOutlet UITableView *salesStatisticsTblView;
@property(strong,nonatomic)NSMutableDictionary* customerDetailsDictionary;



@property (strong, nonatomic) IBOutlet UILabel *monthTargetLbl;

@property (strong, nonatomic) IBOutlet UILabel *totalTargetSalesStatsLbl;

@property (strong, nonatomic) IBOutlet UILabel *totalSalesStatsLbl;

@property (strong, nonatomic) IBOutlet UILabel *achSalesStatsLbl;

@property (strong, nonatomic) IBOutlet UILabel *prevDaySalesStatsLbl;

@property (strong, nonatomic) IBOutlet UILabel *btgSalesStatsLbl;


@property (strong, nonatomic) IBOutlet UITableView *graphTableViewCell;

@property (strong, nonatomic) IBOutlet UIView *agencyGraphView;

- (IBAction)launchMap:(id)sender;






@end
