


//
//  ReviewDocumentViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "ReviewDocumentViewController.h"
#import "SalesOrderNewViewController.h"
#import "CustomerOrderHistoryView.h"
#import "ReviewDocumentReportsViewController.h"
#import "ReviewDocReportsTableViewCell.h"
#import "ReviewDocumentFilterViewController.h"
@interface ReviewDocumentViewController ()

@end

@implementation ReviewDocumentViewController
@synthesize datePickerViewControllerDate,filterButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:kReviewDocumentsTitle];
    
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    self.datePickerViewControllerDate=datePickerViewController;
    
    datePickerViewController.isRoute=NO;
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    custType=@"All";
    
    //testing to date
    
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDate * currentDate = [NSDate date];
    NSDateComponents * comps = [[NSDateComponents alloc] init];
    [comps setDay:-1];
    
    minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
    
    reviewDocumentArray = [NSMutableArray array];
    
    fromDate=[formatter stringFromDate:minDate];
    toDate=[formatter stringFromDate:[NSDate date]];
    
    
    [self LoadDataFromDB];
    
    formatter=nil;
    usLocale=nil;
    reviewDocumentTableView.layer.cornerRadius = 8.0;
    reviewDocumentTableView.layer.masksToBounds = YES;
    
    if (@available(iOS 15.0, *)) {
        reviewDocumentTableView.sectionHeaderTopPadding = 0;
        filterTableView.sectionHeaderTopPadding = 0;
        gridView.tableView.sectionHeaderTopPadding = 0;
    }
}


-(void)LoadDataFromDB
{
    NSString *sQry =@"SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC";
    
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",minDate]];
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    NSLog(@"data one is %@", [sQry description]);
    [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
    
    NSLog(@"data array for collection did load %@", [reviewDocumentArray description]);
    
    NSString *sQry2 =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
    
    NSLog(@"toDate ---%@",fromDate);
    NSLog(@" fromDate---%@",toDate);
    NSString * tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
    NSString * tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
    sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
    
    NSLog(@"return query from did load %@",sQry2);
    NSLog(@"data array before adding return %@", [reviewDocumentArray description]);
    
    [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry2]];
    NSLog(@"data array after ading return %@", [reviewDocumentArray description]);
    
    NSString *sQry3 =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name, 'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
    
     tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
     tempToDate=[toDate stringByAppendingString:@" 23:59:59"];

    
    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
    sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
    NSLog(@"data array which has collection and return %@", [reviewDocumentArray description]);
    
    
    [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry3]];
    NSLog(@"complete data array %@", [reviewDocumentArray description]);
    
    
    NSString* multySqry=@"select TBL_Collection.Collection_Ref_NO ,TBL_Collection.Amount,TBL_Collection.Collected_On,TBL_Collection.Customer_ID, (TBL_Doc_Addl_Info.Custom_Attribute_5 *TBL_Collection.Amount) AS Converted_Amount  from TBL_Collection inner join TBL_Doc_Addl_Info on TBL_Collection.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO where TBL_Collection.Collected_On>='2015-04-13 00:00:00' AND TBL_Collection.Collected_On<='{2}'  ";
    multySqry = [multySqry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    
    NSLog(@"multi sqry is %@", multySqry);
    
    
    convertedAmountArray=[[SWDatabaseManager retrieveManager] dbGetDataForReport:multySqry];
    NSLog(@"test data array  %@", [reviewDocumentArray description]);
    [gridView reloadData];
}


-(void)viewWillAppear:(BOOL)animated
{
    Singleton * singleton=[Singleton retrieveSingleton];
    singleton.showCustomerDashboard=NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self viewReportAction];
}

- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
}

- (IBAction)filterButtonTapped:(id)sender {
    if (reviewDocumentArray.count > 0) {
        ReviewDocumentFilterViewController *popOverVC = [[ReviewDocumentFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.previousFilterParametersDict = previousFilteredParameters;
        popOverVC.customerDictionary = customerDict;
        popOverVC.reviewDocumentArray = reviewDocumentArray;
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController = filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 550) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

//testing touches began method

-(void)filteredReviewDocument:(id)filteredContent
{
    NSLog(@"filtered content in report is %@", filteredContent);
    
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterFilled.png"] forState:UIControlStateNormal];
    reviewDocumentArray = filteredContent;
    [reviewDocumentTableView reloadData];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
}

-(void)reviewDocumentFilterDidReset{
    previousFilteredParameters = [[NSMutableDictionary alloc]init];
    [self viewReportAction];
    [reviewDocumentTableView reloadData];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];
}

-(void)filterParametersReviewDocument:(id)filterParameter
{
    previousFilteredParameters=filterParameter;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touched");
}

- (void)dateChanged:(SWDatePickerViewController *)sender
{
    NSLog(@"from date after click %@", fromDate);
    
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        NSString * sampleDate=fromDate;
        
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            fromDate=sampleDate;
        }
        else
        {
            fromDate=[formatter stringFromDate:sender.selectedDate];
        }
    }
    else {
        NSString* sampleToDate=toDate;
        if ([formatter stringFromDate:sender.selectedDate]==nil) {
            toDate=sampleToDate;
        }
        else
        {
            toDate=[formatter stringFromDate:sender.selectedDate];
        }
    }
    [filterTableView reloadData];
    formatter=nil;
    usLocale=nil;
    [self viewReportAction];
}

- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    currencyTypeViewController = nil;
    currencyTypePopOver = nil;
}

- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andCustomer : (NSMutableDictionary *)customer
{
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID])
        {
            [SWDefaults setProductCategory:[row mutableCopy]];
            break;
        }
    }
    [dict setValue:[dict stringForKey:@"DocNo"] forKey:@"Orig_Sys_Document_Ref"];
    [dict setValue:[dict stringForKey:@"DocDate"] forKey:@"Schedule_Ship_Date"];
    
    
    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    newSalesOrder.parentViewString = @"MO";
    newSalesOrder.preOrderedItems=[NSMutableArray new];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
    newSalesOrder.isOrderConfirmed=@"confirmed";
    
    [self.navigationController pushViewController:newSalesOrder animated:YES];
}

-(IBAction)viewReportAction
{
    if (customerDict.count!=0)
    {
        if (custType.length!=0)
        {
            NSString *sQry;
            if([custType isEqualToString:NSLocalizedString(@"All", nil)])
            {
                sQry =@"SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC ";
                
                NSLog(@"squery is view report action of review document is %@", sQry);
              
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Customer_ID"]];
                NSLog(@"sQry 1 %@", sQry);
                
                
                [reviewDocumentArray setArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                
                NSLog(@"data from sqry 1 %@", [reviewDocumentArray description]);
                
                
                sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                NSLog(@"data from sqry 2 %@", [reviewDocumentArray description]);
                
                sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                NSLog(@"data from sqry 3 %@", [reviewDocumentArray description]);
                
                [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
                [gridView reloadData];
                
            }
            else if(![custType isEqualToString:NSLocalizedString(@"All", nil)])
            {
                if([custType isEqualToString:NSLocalizedString(@"Collection", nil)])
                {
                    sQry =@" SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC  ";
                    
                    
                    NSLog(@"to date if filter is all and collection %@ %@ ", fromDate,toDate);
                    
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                    
                }
                
                else if([custType isEqualToString:@"Sales Returns"])
                {
                    sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                    
                }
                else if([custType isEqualToString:NSLocalizedString(@"Sales Order", nil)])
                {
                    sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}' ORDER BY A.Orig_Sys_Document_Ref ASC";
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
                    sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
                }
                
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
                sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
                
                reviewDocumentArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
                [gridView reloadData];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select document type." withController:self];
            }
        }
    }
    else if([custType isEqualToString:NSLocalizedString(@"All", nil)])
    {
        reviewDocumentArray = [NSMutableArray array];
        NSString *sQry =@"SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC";
        
        
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        
        
        
        [reviewDocumentArray setArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry]];
        NSLog(@"data array at squery %@",[sQry description]);
        
        
        NSString *sQry2 =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
        
        
        NSLog(@"from date is %@", fromDate);
        
        
        NSString * tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
        NSString * tempToDate=[toDate stringByAppendingString:@" 23:59:59"];
        
        
        NSLog(@"to date is %@", toDate);
        
        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
        sQry2 = [sQry2 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
        [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry2]];
        
        //testing sqyery 3
        
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *components = [cal components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[[NSDate alloc] init]];
        
        [components setHour:-24];
        [components setMinute:0];
        [components setSecond:0];
        
        
        //this query is culprit which is not returning anything if to and from dates are same
        
        NSString *sQry3 =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount ,C.Customer_Name ,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Inv_To_Customer_ID = C.Customer_ID AND A.Inv_To_Site_ID  = C.Site_Use_ID    WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
        
        NSLog(@"todate is %@", toDate);
        tempfromDate=[fromDate stringByAppendingString:@" 00:00:00"];
        tempToDate=[toDate stringByAppendingString:@" 23:59:59"];
        
        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{1}" withString:tempfromDate];
        sQry3 = [sQry3 stringByReplacingOccurrencesOfString:@"{2}" withString:tempToDate];
        
        
        NSLog(@"sqry 3 is %@", sQry3);
        [reviewDocumentArray addObjectsFromArray:[[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry3]];
        
        NSLog(@"data from sqry3 %@", [reviewDocumentArray description]);
        [gridView reloadData];
    }
    else
    {
        NSString *sQry;
        if([custType isEqualToString:NSLocalizedString(@"Collection", nil)])
        {
            sQry =@"SELECT  A.Collection_Ref_No AS DocNo, A.Collected_On as DocDate,(TBL_Doc_Addl_Info.Custom_Attribute_5 *A.Amount)   AS Amount, C.Customer_Name AS Customer_Name ,'Collection' AS Doctype , CASE WHEN A.Status='N' THEN 'Pending' ELSE 'Processed' END As [ERP_Status] FROM TBL_Collection AS A LEFT OUTER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  LEFT OUTER JOIN TBL_Doc_Addl_Info on A.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO WHERE  A.Collected_On>='{1}' AND A.Collected_On<='{2}' ORDER BY A.Collection_Ref_No ASC ";
            
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        }
        else if([custType isEqualToString:@"Sales Returns"])
        {
            sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name ,'Return' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'R' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}'  ORDER BY A.Orig_Sys_Document_Ref ASC";
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry= [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        }
        else if([custType isEqualToString:NSLocalizedString(@"Sales Order", nil)])
        {
            sQry =@"SELECT  A.Orig_Sys_Document_Ref AS DocNo,A.Creation_Date As DocDate, A.Transaction_Amt AS Amount,C.Customer_Name ,'Order' AS Doctype , CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status] FROM TBL_Order_History AS A INNER JOIN TBL_Customer_Ship_Address AS C ON A.Ship_To_Customer_ID = C.Customer_ID AND A.Ship_To_Site_ID  = C.Site_Use_ID  WHERE A.Doc_Type = 'I' AND A.Creation_Date >='{1}' AND A.Creation_Date <='{2}' ORDER BY A.Orig_Sys_Document_Ref ASC";
            
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
            sQry= [sQry stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
        }
        reviewDocumentArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
        [reviewDocumentTableView reloadData];
    }
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionNSCalendarUnitSecond
{
    return reviewDocumentArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"salesSummary";
    ReviewDocReportsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ReviewDocReportsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.documentNoLabel.text = @"Document No";
    cell.documentTypeLabel.text = @"Document Type";
    cell.dateLabel.text = @"Date";
    cell.nameLabel.text = @"Name";
    cell.amountLabel.text = @"Amount (AED)";
    cell.statusLabel.text = @"Status";
    
    cell.documentNoLabel.textColor = TableViewHeaderSectionColor;
    cell.documentTypeLabel.textColor = TableViewHeaderSectionColor;
    cell.dateLabel.textColor = TableViewHeaderSectionColor;
    cell.nameLabel.textColor = TableViewHeaderSectionColor;
    cell.amountLabel.textColor = TableViewHeaderSectionColor;
    cell.statusLabel.textColor = TableViewHeaderSectionColor;
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"salesSummary";
    ReviewDocReportsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ReviewDocReportsTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data = [reviewDocumentArray objectAtIndex:indexPath.row];

    cell.documentNoLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"DocNo"]]];
    cell.documentTypeLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Doctype"]]];
    cell.dateLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data dateStringForKey:@"DocDate" withDateFormat:NSDateFormatterMediumStyle]]];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_Name"]]];
    cell.amountLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Amount"]]];
    cell.statusLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"ERP_Status"]]];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[[reviewDocumentArray objectAtIndex:indexPath.row] valueForKey:@"Doctype"]isEqualToString:@"Collection"]) {
        
        
    }
    
    else
    {
        
        int index;
        index = indexPath.row;
        ReviewDocumentReportsViewController* rdVc=[[ReviewDocumentReportsViewController alloc]init];
        
        NSLog(@"data array before removing %@", [reviewDocumentArray description]);
        
        
        
        for (NSInteger m =0; m<reviewDocumentArray.count; m++) {
            
            if ([[[reviewDocumentArray objectAtIndex:m] valueForKey:@"Doctype"]isEqualToString:@"Collection"]) {
                
                [reviewDocumentArray removeObjectAtIndex:m];
                index=index-1;
                
            }
            
        }
        
        NSLog(@"data array after removing %@", [reviewDocumentArray description]);
        rdVc.reportType=[[reviewDocumentArray objectAtIndex:indexPath.row]valueForKey:@"Doctype"];
        
        rdVc.selectedIndex=indexPath.row;
        rdVc.ordersArray=reviewDocumentArray ;
        rdVc.totalContentDataArray=reviewDocumentArray;
        [self.navigationController pushViewController:rdVc animated:YES];
    }
}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    //datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end
