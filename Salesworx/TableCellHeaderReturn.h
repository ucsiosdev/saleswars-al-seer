//
//  TblCellHeaderReturn.h
//  SalesWars
//
//  Created by Ravinder Kumar on 07/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableCellHeaderReturn : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnItemName;
//@property (strong, nonatomic) IBOutlet UIImageView *imgVIcon;

@end

NS_ASSUME_NONNULL_END
