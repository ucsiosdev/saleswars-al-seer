//
//  NewSurveyViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/22/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//
#import "SWAppDelegate.h"
#import "NewSurveyViewController.h"
#import "SWDatabaseManager.h"
#import "SurveyQuestionViewController.h"
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "DataType.h"
#import "FMDB/FMDBHelper.h"
#import "SWVisitOptionsViewController.h"
#import <IQKeyboardManager.h>

#define saveSurveyResponse @"insert into TBL_Survey_Cust_Responses(Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('%d','%d','%@','%d','%d','%d','%@','%@')"


@interface NewSurveyViewController (){
    int tempCountColor;
    BOOL setColorForDebugMode;
    int tempCountForButton;
    int countForQueryInsert;
}

@end

@implementation NewSurveyViewController
@synthesize surveyQuestionArray=_surveyQuestionArray,survetIDNew,surveyResponseArray=_surveyResponseArray,qID,containerView,scrollView,modifiedSurveyQuestionsArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    setColorForDebugMode = NO;
    
    /**** below line only for debugging purpose, do not un-comment this in production level *******/
    //[[SWDatabaseManager retrieveManager] testDeleteDataFromTable:@"TBL_Survey_Cust_Responses"];
    
    countForQueryInsert = 1;
    tempCountForButton = 0;
    tempCountColor = 0;
    
    // [self setShadowAndRoundCorner:containerView.layer];
    
    [self setShadowAndRoundCorner:scrollView.layer];
    
    //scrollView.layer.cornerRadius = 8.0;
    //scrollView.layer.masksToBounds = YES;
    
    modifiedSurveyQuestionsArray=[[NSMutableArray alloc]init];
    
    viewX = 13;
    // viewX = 20;
    viewY =13;
    viewWidth = containerView.frame.size.width;
    
    textAreaQuestionAnswerArray = [[NSMutableArray alloc]init];
    checkBoxQuestionAnswerArray = [[NSMutableArray alloc]init];
    radioQuestionAnswerArray = [[NSMutableArray alloc]init];
    
    
    
    
    textAreaQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    checkBoxQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    radioQuestionAnswerValidationArray=[[NSMutableArray alloc]init];
    textAreaAnswerDescriptionArray=[[NSMutableArray alloc]init];
    
    
    
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"survey type new is %@", appDelegate.SurveyType);
    
    
    
    
    UIBarButtonItem *Save = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(Save)];
    [Save setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = Save;
    
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnBackTapped:)];
    
    [btnBack setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     nil]
                           forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = btnBack;
    
    
    
    self.navigationItem.title=@"";
    
    scrollView.scrollEnabled=YES;
    
    
    scrollView.delegate=self;
    [scrollView setScrollEnabled:YES];
    
    [scrollView setContentSize: CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    
    
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    //    scrollView.pagingEnabled = YES;
    
    
    
    
    
    appDelegate=[DataSyncManager sharedManager];
    //self.title =appDelegate.surveyDetails.Survey_Title;
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:appDelegate.surveyDetails.Survey_Title];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(14)}];
    
    
    if(appDelegate.customerResponseArray)
        [appDelegate.customerResponseArray removeAllObjects];
    
    
    appDelegate.SurveyType = appDelegate.surveyDetails.Survey_Type_Code;
    
    NSLog(@"Survey type is %@",appDelegate.SurveyType);
    
    NSLog(@"survey type code is %@", appDelegate.surveyDetails.Survey_Type_Code);
    
    
    // Get survey question
    NSLog(@"survey id new %d", survetIDNew);
    
    
    NSLog(@"survey id in new %d", appDelegate.surveyDetails.Survey_ID);
    
    saveSurveyID=appDelegate.surveyDetails.Survey_ID;
    
    NSLog(@"saved survey id is %d", saveSurveyID);
    
    
    
    
    //new logs
    
    
    
    
    self.surveyQuestionArray = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];//survetIDNew];
    
    
    
    
    [self ModifyQuestionResponse];
    
    NSLog(@"new survey questions array count is %lu", (unsigned long)[modifiedSurveyQuestionsArray count]);
    
    if ([modifiedSurveyQuestionsArray count]>0) {
        [self CreateQuestionsView];
    }
    
    
    
    //got survey questions here
    for (int i=0; i<[self.surveyQuestionArray count]; i++) {
        surveyQuestionDetails = [_surveyQuestionArray objectAtIndex:i];
        NSLog(@"Survey questions new %@", surveyQuestionDetails.Question_Text);
        NSLog(@"Question id is %d", surveyQuestionDetails.Question_ID);
        
        
        //answer here
        self.surveyResponseArray=[[SWDatabaseManager retrieveManager] selectResponseType:surveyQuestionDetails.Question_ID];
        
        for (int j=0; j<[self.surveyResponseArray count]; j++) {
            info=[_surveyResponseArray objectAtIndex:j];
            
            NSLog(@"info tect is %@", info.Response_Text);
            
            NSLog(@"response type is %d", info.Response_Type_ID);
            
            
            
            
        }
        
        
        
        
        //try switch
        
        
        
        
        
        NSLog(@"response type id before view gets created is %@", [_surveyResponseArray valueForKey:@"Response_Type_ID"]);
        
        //        if (info.Response_Type_ID==1) {
        //            NSLog(@"radio button");
        //
        //            [self createViewForTextAreaQuestion:info withQuestionNumber:i];
        //
        //        }
        //
        //      if (info.Response_Type_ID==2)
        //        {
        //            [self createViewForRadioQuestion:info withQuestionNumber:i];
        //        }
        //
        //        if (info.Response_Type_ID==3)
        //        {
        //            [self createViewForCheckBoxQuestion:info withQuestionNumber:i];
        //        }
        //
    }
    
    NSLog(@"info count is %lu", (unsigned long)[self.surveyResponseArray count]);
    
    //  [self BuildScrollPage];
    
    //answers
    
    
    //    NSLog(@"question is old %d", surveyQuestionDetails.Question_ID);
    //
    //    self.surveyResponseArray  = [[SWDatabaseManager retrieveManager] selectResponseType:surveyQuestionDetails.Question_ID];
    //
    //    for (int j=0; j<[self.surveyResponseArray count]; j++) {
    //        info =[self.surveyResponseArray objectAtIndex:j];
    
    //    SurveyQuestionDetails* qdetails=[[SurveyQuestionDetails alloc]init];
    //
    
    //
    //
    
    //
    //
    //NSLog(@"new response %@", [[info valueForKey:@"Response_Text"]objectAtIndex:0]);
    //
    //    NSLog(@"surder q array new %@", [self.surveyQuestionArray description]);
    
    
    
    
    //    SurveyPageControlViewControl* sample=[[SurveyPageControlViewControl alloc]initWithSurveyQuestion:surveyQuestionDetails];
    //
    //    NSLog(@"q id fetched %@", [sample.surveyResponseArray description]);
    //
    //
    //    SurveyResponseDetails * resp=[sample.surveyResponseArray objectAtIndex:0];
    //
    //
    //
    //    NSLog(@"info here is %@", resp.Response_Text);
    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
    shadowView.layer.cornerRadius = 8.0;
    //    myview.layer.masksToBounds = YES;
}

/*
 - (void) animateTextField: (UITextView*) textField up: (BOOL) up
 {
 const int movementDistance = 220; // tweak as needed
 const float movementDuration = 0.3f; // tweak as needed
 
 int movement = (up ? -movementDistance : movementDistance);
 
 [UIView beginAnimations: @"anim" context: nil];
 [UIView setAnimationBeginsFromCurrentState: YES];
 [UIView setAnimationDuration: movementDuration];
 self.view.frame = CGRectOffset(self.view.frame, 0, movement);
 [UIView commitAnimations];
 }
 
 -(void)textViewDidBeginEditing:(UITextView *)textView{
 
 [self animateTextField:textView up:YES];
 }
 
 -(void)textViewDidEndEditing:(UITextView *)textView{
 
 [self animateTextField:textView up:NO];
 }
 */
-(void)setShadowAndRoundCorner:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    // txtview.keyboardDistanceFromTextField = 10;
    
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    /*
     [scrollView setContentSize: CGSizeMake(self.view.frame.size.width, containerView.frame.size.height)];
     NSLog(@"\n\n *** 1 containerView: %@  ****\n\n",containerView);
     NLog(@"\n\n *** 2 scrollView: %@  ****\n\n",scrollView);
     */
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self performSelector:@selector(handleScrollViewContentSizw) withObject:self afterDelay:1.2];
    
    
    
}


-(void)handleScrollViewContentSizw{
    [constrContainerViewHeight setConstant:viewY+40];
    [scrollView setContentSize: CGSizeMake(scrollView.frame.size.width, viewY+40)];
    
    /*
     //[containerView removeFromSuperview];
     
     
     int tempHeight = containerView.frame.size.height;
     
     containerView.frame = CGRectMake( containerView.frame.origin.x, containerView.frame.origin.y,containerView.frame.size.width,tempHeight);
     
     [scrollView addSubview:containerView];
     
     [scrollView bringSubviewToFront:self.view];
     
     */
    
}

/*
 -(void)viewDidLayoutSubviews
 {
 
 [super viewDidLayoutSubviews];
 
 // The scrollview needs to know the content size for it to work correctly
 self.scrollView.contentSize = CGSizeMake(
 self.containerView.frame.size.width,
 self.containerView.frame.size.height + 300
 );
 
 
 // self.scrollView.contentSize = CGSizeMake(2000, 2000);
 
 // [scrollView setContentSize: CGSizeMake(self.view.frame.size.width,viewY)];
 NSLog(@"\n\n *** 1 containerView: %@  ****\n\n",containerView);
 NSLog(@"\n\n *** 2 scrollView: %@  ****\n\n",scrollView);
 
 }*/

-(void)CreateQuestionsView
{
    
    for (int i=0;i<modifiedSurveyQuestionsArray.count;i++) {
        //SurveyQuestionDetails *question = [modifiedSurveyQuestionsArray objectAtIndex:i];
        
        SurveyQuestion* question=[modifiedSurveyQuestionsArray objectAtIndex:i];
        
        if ([question.Response_Type_ID isEqualToString:@"1"]) {
            [self createViewForTextAreaQuestion:question withQuestionNumber:i+1];
            
        }
        else if ([question.Response_Type_ID isEqualToString:@"2"])
        {
            [self createViewForRadioQuestion:question withQuestionNumber:i+1];
        }
        else if ([question.Response_Type_ID isEqualToString:@"3"])
        {
            [self createViewForCheckBoxQuestion:question withQuestionNumber:i+1];
        }
    }
}


-(void)ModifyQuestionResponse
{
    
    [modifiedSurveyQuestionsArray removeAllObjects];
    
    
    
    NSLog(@"int is %d", saveSurveyID);
    
    NSString *query = [NSString stringWithFormat:@"SELECT DISTINCT A.*, B.Response_Type_ID FROM TBL_Survey_Questions As A INNER JOIN TBL_Survey_Responses As B ON A.Question_ID=B.Question_ID WHERE A.Survey_ID= %d", saveSurveyID];
    
    NSLog(@"query to fetch questions is %@", query);
    
    
    
    
    NSArray *temp =[FMDBHelper executeQuery:query];
    
    
    
    NSLog(@"temp is %@", [temp description]);
    
    for (NSMutableDictionary *customerDic in temp) {
        SurveyQuestion *customer = [SurveyQuestion new];
        
        if ([[customerDic valueForKey:@"Question_ID"] isEqual: [NSNull null]]) {
            customer.Question_ID = @"";
        }else{
            customer.Question_ID = [customerDic valueForKey:@"Question_ID"];
        }
        
        if ([[customerDic valueForKey:@"Question_Text"] isEqual: [NSNull null]]) {
            customer.Question_Text = @"";
        }else{
            customer.Question_Text = [customerDic valueForKey:@"Question_Text"];
        }
        
        if ([[customerDic valueForKey:@"Survey_ID"] isEqual: [NSNull null]]) {
            customer.Survey_ID = @"";
        }else{
            customer.Survey_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Survey_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Default_Response_ID"] isEqual: [NSNull null]]) {
            customer.Default_Response_ID= @"";
        }else{
            customer.Default_Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Default_Response_ID"]];
        }
        
        if ([[customerDic valueForKey:@"Response_Type_ID"] isEqual: [NSNull null]]) {
            customer.Response_Type_ID = @"";
        }else{
            customer.Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        }
        
        [modifiedSurveyQuestionsArray addObject:customer];
        
    }
    
    NSLog(@"\n\n **** Hint: modifiedSurveyQuestionsArray : %@  **\n\n",modifiedSurveyQuestionsArray);
}










-(void)createViewForRadioQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY+13, viewWidth, 100)];
    // UIColor * color =[UIColor colorWithRed:233/255.0 green:232/255.0 blue:235/255.0 alpha:1];
    textQuestionView.backgroundColor = [UIColor clearColor];;
    textQuestionView.tag = questionNumber;
    
    //viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (0, viewHeightCalculator, viewWidth-15, 20)];
    NSString * QuestionText = [NSString stringWithFormat:@"%02d%@ %@",questionNumber,@".",question.Question_Text];
    //UIColor * questionColor =[UIColor colorWithRed:237/255.0 green:148/255.0 blue:0/255.0 alpha:1];
    questionLabel.textColor = UIColorFromRGB(0x2C394A);
    questionLabel.font = kFontWeblySleekSemiBold(16);
    
    if (setColorForDebugMode){
        questionLabel.backgroundColor = [UIColor purpleColor];
    }
    else {
        questionLabel.backgroundColor = [UIColor clearColor];
    }
    
    
    
    //QuestionText = question.Question_Text;
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    
    
    
    //    CGRect textRect = [QuestionText
    //                       boundingRectWithSize:maximumLabelSize
    //                       options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
    //                       attributes:@{NSFontAttributeName: questionLabel.font}
    //                       context:nil];
    
    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    [textQuestionView addSubview:questionLabel];
    
    //line 1
    int line_1 = questionLabel.frame.origin.y+questionLabel.frame.size.height+13;
    UILabel *lbl_1 = [[UILabel alloc]initWithFrame:CGRectMake(-13,line_1 , containerView.frame.size.width, 1)];
    lbl_1.text = @"";
    lbl_1.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [textQuestionView addSubview:lbl_1];
    
    
    
    
    
    //line 2
    
    //    int line_2 = textQuestionView.frame.origin.y+textQuestionView.frame.size.height;
    //    UILabel *lbl_2 = [[UILabel alloc]initWithFrame:CGRectMake(-13,line_2 , containerView.frame.size.width, 1)];
    //    lbl_2.text = @"";
    //    lbl_2.backgroundColor = [UIColor redColor];
    //    [textQuestionView addSubview:lbl_2];
    
    
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+20;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    for (NSMutableDictionary *customerDic in array) {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    for (int i= 0; i<SurveyResponseArray.count; i++) {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        int temp_2 = viewHeightCalculator+11;
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(0, temp_2, 32, 32);
        UIImage *btnImage = [UIImage imageNamed:@"radio_buttonNew"];
        UIImage *btnImage1 = [UIImage imageNamed:@"radio_button_activeNew"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClickedForRadio:) forControlEvents:UIControlEventTouchDown];
        
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@" image button tag is %d",imageButton.tag);
        NSLog(@"response id is %@", response.Response_ID);
        
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
            //imageButton.selected =YES;
        }
        
        [textQuestionView addSubview:imageButton];
        
        int temp_3 = 45;
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (temp_3, viewHeightCalculator+16, viewWidth-15, 20)];
        questionLabel.textColor =  UIColorFromRGB(0x6A6F7B);
        questionLabel.font = kFontWeblySleekSemiBold(14);
        NSString * QuestionText = response.Response_Text;
        //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
        CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        questionLabel.frame = questionLabelOldFrame;
        // questionLabel.backgroundColor = [UIColor redColor];
        /*
         if (setColorForDebugMode){
         questionLabel.backgroundColor = [UIColor redColor];
         }else {
         questionLabel.backgroundColor = [UIColor clearColor];
         }
         
         questionLabel.backgroundColor = [UIColor redColor];
         
         
         if (setColorForDebugMode){
         textQuestionView.backgroundColor =[UIColor blueColor];
         }else {
         textQuestionView.backgroundColor =[UIColor clearColor];
         }
         
         textQuestionView.backgroundColor =[UIColor blueColor];
         */
        
        
        [textQuestionView addSubview:questionLabel];
        
        viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height+25;
    }
    
    viewHeightCalculator = viewHeightCalculator+20;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 0;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    //}
    
    [self addDividerLine];
    
    
    
    // new special debug
    /*
     if (tempCountForButton==0){
     questionLabel.backgroundColor = [UIColor redColor];
     textQuestionView.backgroundColor =[UIColor orangeColor];
     }else if (tempCountForButton==1)  {
     questionLabel.backgroundColor = [UIColor blueColor];
     textQuestionView.backgroundColor =[UIColor greenColor];
     
     }else {
     questionLabel.backgroundColor = [UIColor blackColor];
     textQuestionView.backgroundColor =[UIColor yellowColor];
     
     }
     */
    tempCountForButton++;
    
    
    
    /*
     UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"horizontal_divider.png"]];
     imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
     [self.containerView addSubview:imageView];
     */
    
}


-(void)addDividerLine{
    
    UILabel *lblDivider = [[UILabel alloc]initWithFrame:CGRectMake(0, viewY, containerView.frame.size.width, 1)];
    lblDivider.text = @"";
    
    /*
     if (tempCountColor == 0){
     lblDivider.backgroundColor = [UIColor redColor];
     }else  if (tempCountColor==1){
     lblDivider.backgroundColor = [UIColor greenColor];
     }
     else  if (tempCountColor==2){
     lblDivider.backgroundColor = [UIColor yellowColor];
     }
     else  if (tempCountColor==3){
     lblDivider.backgroundColor = [UIColor blueColor];
     }
     else  if (tempCountColor==4){
     lblDivider.backgroundColor = [UIColor redColor];
     }
     
     tempCountColor++;
     */
    lblDivider.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [self.containerView addSubview:lblDivider];
    
    //[scrollView setContentSize: CGSizeMake(self.view.frame.size.width, viewY)];
    NSLog(@"\n\n *** 1 containerView: %@  ****\n\n",containerView);
    NSLog(@"\n\n *** 2 scrollView: %@  ****\n\n",scrollView);
}

-(void)createViewForTextAreaQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY, viewWidth, 100)];
    //UIColor * color =[UIColor colorWithRed:233/255.0 green:232/255.0 blue:235/255.0 alpha:1];
    textQuestionView.backgroundColor = [UIColor clearColor];
    textQuestionView.tag = questionNumber;
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (0, viewHeightCalculator, viewWidth-15, 20)];
    //UIColor * questionColor =[UIColor colorWithRed:237/255.0 green:148/255.0 blue:0/255.0 alpha:1];
    //questionLabel.textColor = questionColor;
    // questionLabel.font = LightFontOfSize(18.0);
    questionLabel.textColor = UIColorFromRGB(0x2C394A);
    questionLabel.font = kFontWeblySleekSemiBold(16);
    
    NSString * QuestionText = [NSString stringWithFormat:@"%02d%@ %@",questionNumber,@".",question.Question_Text];
    //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    questionLabel.frame = questionLabelOldFrame;
    
    
    
    //line 4
    int line_4 = questionLabel.frame.origin.y+questionLabel.frame.size.height+13;
    UILabel *lbl_4 = [[UILabel alloc]initWithFrame:CGRectMake(-13,line_4 , containerView.frame.size.width, 1)];
    lbl_4.text = @"";
    lbl_4.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [textQuestionView addSubview:lbl_4];
    
    
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+20;
    
    int tempWidthTV = containerView.frame.size.width-39;
    txtview =[[UITextView alloc]initWithFrame:CGRectMake(0,viewHeightCalculator+7,tempWidthTV,93)];
    
    if (setColorForDebugMode){
        txtview.backgroundColor = [UIColor lightGrayColor];
    }else {
        txtview.backgroundColor = [UIColor clearColor];
    }
    
    
    
    
    [txtview setReturnKeyType:UIReturnKeyDone];
    [textQuestionView addSubview:txtview];
    txtview.text = @"";
    txtview.textColor = UIColorFromRGB(0x2C394A);
    txtview.font = kFontWeblySleekSemiBold(14);
    txtview.tag = questionNumber * 100;
    txtview.delegate = self;
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame: CGRectMake(0,0,900,100)];
    imgView.image = [UIImage imageNamed: @"survey_comments.png"];
    [txtview addSubview: imgView];
    [txtview sendSubviewToBack: imgView];
    
    viewHeightCalculator = viewHeightCalculator+txtview.frame.size.height;
    
    viewHeightCalculator = viewHeightCalculator+20;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    
    
    if (setColorForDebugMode){
        textQuestionView.backgroundColor= [UIColor blueColor];
    }else {
        textQuestionView.backgroundColor= [UIColor clearColor];
    }
    
    
    
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 0;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY+100))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    // }
    
    [self addDividerLine];
    
    
    //[self setShadowOnUILayer:txtview.layer];
    
    txtview.layer.borderWidth=1.0;
    UIColor *borderColor = UIColorFromRGB(0xE3E4E6);
    txtview.layer.borderColor = borderColor.CGColor;
    txtview.layer.masksToBounds = YES;
    txtview.layer.cornerRadius = 8.0;
    
    
    
    
    
    /*
     UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"horizontal_divider.png"]];
     imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
     [self.containerView addSubview:imageView];
     */
}

-(void)createViewForCheckBoxQuestion :(SurveyQuestion *)question withQuestionNumber : (int)questionNumber{
    
    int viewHeightCalculator = 0;
    
    UIView *textQuestionView = [[UIView alloc] initWithFrame: CGRectMake ( viewX, viewY, viewWidth, 150)];
    textQuestionView.backgroundColor = [UIColor clearColor];;
    textQuestionView.tag = questionNumber;
    
    viewHeightCalculator = viewHeightCalculator+10;
    
    UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (0, viewHeightCalculator, viewWidth-15, 23)];
    questionLabel.textColor = UIColorFromRGB(0x2C394A);
    questionLabel.font =  kFontWeblySleekSemiBold(16);
    
    NSString * QuestionText = [NSString stringWithFormat:@"%02d%@ %@",questionNumber,@".",question.Question_Text];
    //QuestionText = question.Question_Text;
    questionLabel.text = QuestionText;
    questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionLabel.numberOfLines = 0;
    CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
    CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
    CGRect questionLabelOldFrame = questionLabel.frame;
    questionLabelOldFrame.size.height = expectedLabelSize.height;
    questionLabelOldFrame.size.width = expectedLabelSize.width;
    
    if (setColorForDebugMode){
        questionLabel.backgroundColor = [UIColor yellowColor];
    }else {
        questionLabel.backgroundColor = [UIColor clearColor];
    }
    
    
    
    
    questionLabel.frame = questionLabelOldFrame;
    
    
    //line 3
    int line_3 = questionLabel.frame.origin.y+questionLabel.frame.size.height+13;
    UILabel *lbl_3 = [[UILabel alloc]initWithFrame:CGRectMake(-13,line_3 , containerView.frame.size.width, 1)];
    lbl_3.text = @"";
    lbl_3.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [textQuestionView addSubview:lbl_3];
    
    
    
    [textQuestionView addSubview:questionLabel];
    
    viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height;
    viewHeightCalculator = viewHeightCalculator+110;
    
    NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
    NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
    NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
    for (NSMutableDictionary *customerDic in array) {
        SurveyResponse *customer = [SurveyResponse new];
        customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
        customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
        customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
        customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
        [SurveyResponseArray addObject: customer];
    }
    //button 1
    for (int i= 0; i<SurveyResponseArray.count; i++) {
        SurveyResponse * response = [SurveyResponseArray objectAtIndex:i];
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.frame = CGRectMake(0, viewHeightCalculator-80, 32, 34);
        UIImage *btnImage = [UIImage imageNamed:@"tick_unselectNew"];
        UIImage *btnImage1 = [UIImage imageNamed:@"tick_accessoryNew"];
        [imageButton setBackgroundImage:btnImage forState:UIControlStateNormal];
        [imageButton setBackgroundImage:btnImage1 forState:UIControlStateSelected];
        [imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
        imageButton.tag =questionNumber  * 100 + [response.Response_ID intValue];
        NSLog(@"%d",imageButton.tag);
        if ([question.Default_Response_ID isEqualToString:response.Response_ID]) {
            imageButton.selected =YES;
        }
        
        [textQuestionView addSubview:imageButton];
        
        int temp_4 = 45;
        
        UILabel *questionLabel = [[UILabel alloc]initWithFrame:CGRectMake (temp_4, viewHeightCalculator-72, viewWidth-15, 20)];
        questionLabel.textColor = UIColorFromRGB(0x6A6F7B);
        questionLabel.font =  kFontWeblySleekSemiBold(14);
        NSString * QuestionText = response.Response_Text;
        //QuestionText = @"HelohiiiiiiiiiiiHelohiiiiiiiiiiiHelohi";
        questionLabel.text = QuestionText;
        questionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        questionLabel.numberOfLines = 0;
        CGSize maximumLabelSize = CGSizeMake(viewWidth-15,9999);
        CGSize expectedLabelSize = [QuestionText sizeWithFont:questionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:questionLabel.lineBreakMode];
        CGRect questionLabelOldFrame = questionLabel.frame;
        questionLabelOldFrame.size.height = expectedLabelSize.height;
        questionLabelOldFrame.size.width = expectedLabelSize.width;
        
        
        
        if (setColorForDebugMode){
            questionLabel.backgroundColor = [UIColor greenColor];
        }else {
            questionLabel.backgroundColor = [UIColor clearColor];
        }
        
        
        questionLabel.frame = questionLabelOldFrame;
        
        
        if (setColorForDebugMode){
            textQuestionView.backgroundColor = [UIColor yellowColor];
        }else {
            textQuestionView.backgroundColor = [UIColor clearColor];
        }
        
        
        
        [textQuestionView addSubview:questionLabel];
        
        viewHeightCalculator = viewHeightCalculator+questionLabel.frame.size.height+25;
    }
    
    viewHeightCalculator = viewHeightCalculator-70;
    
    CGRect textQuestionViewOldFrame = textQuestionView.frame;
    textQuestionViewOldFrame.size.height = viewHeightCalculator;
    [textQuestionView setFrame:textQuestionViewOldFrame];
    [self.containerView addSubview:textQuestionView];
    
    viewY = viewY+ viewHeightCalculator + 0;
    
    [self.scrollView setContentSize:(CGSizeMake(self.scrollView.frame.size.width, viewY))];
    
    CGRect containerViewOldFrame = self.containerView.frame;
    //if (viewY > containerViewOldFrame.size.height) {
    containerViewOldFrame.size.height = viewY;
    [self.containerView setFrame:containerViewOldFrame];
    //}
    
    [self addDividerLine];
    /*
     UIImageView * imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"horizontal_divider.png"]];
     imageView.frame =CGRectMake ( viewX, viewY, viewWidth, 2);
     [self.containerView addSubview:imageView];
     */
}



/*
 -(void)setShadowOnUILayer:(CALayer*)viewLayer{
 
 viewLayer.borderWidth=1.0;
 UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
 viewLayer.borderColor = borderColor.CGColor;
 viewLayer.masksToBounds = NO;
 viewLayer.cornerRadius = 5.0;
 viewLayer.shadowColor = [UIColor blackColor].CGColor;
 viewLayer.shadowOffset = CGSizeMake(3, 3);
 viewLayer.shadowOpacity = 0.1;
 viewLayer.shadowRadius = 1.0;
 }
 */




- (void)buttonClicked:(UIButton*)button
{
    
    
    NSLog(@"button tag new  %ld",(long)button.tag);
    
    
    if (button.selected) {
        button.selected = NO;
    }else{
        button.selected = YES;
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        
        [self closeVisitForSurvey];
        
        NSLog(@"back button pressed in view will dis appear");
        
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    //start visit here
    
    [super viewWillAppear:animated];
    
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        
        [self startVisit];
        
        
        NSLog(@"current visit id in survey is %@", [SWDefaults currentVisitID]);
    }
    NSLog(@"visit id in  defaults at survey %@", currentVisitID);
    
    
}



#pragma mark Start Visit Methods

-(void)startVisit
{
    Singleton *single = [Singleton retrieveSingleton];
    single.currentCustomer = [[SWDefaults customer] mutableCopy];
    self.customer = single.currentCustomer;
    
    NSLog(@"customer details in survey %@", [self.customer description]);
    SWAppDelegate *appDelegate = [[SWAppDelegate alloc]init];
    
    [SWDefaults clearCurrentVisitID];
    visitID=[NSString createGuid];
    [SWDefaults setCurrentVisitID:visitID];
    
    
    [[SWDatabaseManager retrieveManager] saveVistWithCustomerInfo:self.customer andVisitID:visitID];
    [appDelegate saveLocationData:YES];
}

#pragma mark End Visit Methods
- (void)closeVisitForSurvey
{
    //routeSer.delegate=self;
    //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]];
    
    //clear visit id from defaults
    
    
    if ([SWDefaults currentVisitID]) {
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]]];
        [SWDefaults clearCurrentVisitID];
        
        
    }
    
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
    
}

- (void)getRouteServiceDidGetRoute:(NSArray *)r{
    
    
    Singleton *single = [Singleton retrieveSingleton];
    self.customer = single.currentCustomer;
    
    if(![r count]==0)
    {
        for (int i=0; i<[r count]; i++)
        {
            
            if([[self.customer stringForKey:@"Ship_Customer_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Customer_ID"]] && [[self.customer stringForKey:@"Ship_Site_Use_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Site_Use_ID"]])
            {
                //routeSer.delegate=self;
                [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[[r objectAtIndex:i] stringForKey:@"Planned_Visit_ID"]];
                
                NSString* tempVisitID=[SWDefaults currentVisitID];
                if (tempVisitID) {
                    [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
                }
            }
            
        }
    }
    if ([self.customer objectForKey:@"Planned_Visit_ID"])
    {
        [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[self.customer stringForKey:@"Planned_Visit_ID"]];
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    else
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    
    
    
    
    NSString* tempVisitID=[SWDefaults currentVisitID];
    
    if (tempVisitID) {
        
        
        NSMutableArray *locationDict = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Latitude , Longitude from TBL_FSR_Actual_Visits where Actual_Visit_ID='%@'",[SWDefaults currentVisitID]]];
        if ([[[locationDict objectAtIndex:0]stringForKey:@"Latitude"]isEqualToString:@"0"] || [[[locationDict objectAtIndex:0]stringForKey:@"Longitude"]isEqualToString:@"0"])
        {
            // #define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "
            NSString *latitudePoint,*longitudePoint;
            
            
            
            
            SWAppDelegate * appDeleage=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
            
            
            latitudePoint=[NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.latitude];
            longitudePoint=[NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.longitude];
            if (latitudePoint==nil) {
                
                latitudePoint=@"0.0";
                longitudePoint = @"0.0";
            }
            
            
            
            
            
            //        NSLog(@"latitude %@",latitudePoint);
            //        NSLog(@"longitude %@",longtitudePoint);
            [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@' Where Actual_Visit_ID='%@'",latitudePoint,longitudePoint,[SWDefaults currentVisitID]]];
        }
        
        
        
        
        SWVisitManager *visitManager = [[SWVisitManager alloc] init];
        [visitManager closeVisit];
        
        Singleton *single = [Singleton retrieveSingleton];
        if ([single.isCashCustomer isEqualToString:@"Y"]) {
            single.popToCash = @"Y";
        }
        else
        {
            single.popToCash = @"N";
            
        }
        r=nil;
        //        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        //        [self.navigationController  popViewControllerAnimated:YES];
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"went to visit options no activity done");
    }
}

- (void)buttonClickedForRadio:(UIButton*)button
{
    UIView *textQuestionView = button.superview;
    NSLog(@"Hi %ld",(long)textQuestionView.tag);
    SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:textQuestionView.tag-1];
    if ([question.Response_Type_ID isEqualToString:@"2"]){
        
        NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
        NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
        NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
        for (NSMutableDictionary *customerDic in array) {
            SurveyResponse *customer = [SurveyResponse new];
            customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
            customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
            customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
            customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
            [SurveyResponseArray addObject: customer];
            
            NSLog(@"new response %@", [SurveyResponseArray description]);
            
            UIButton * allButton  = (UIButton *)[textQuestionView viewWithTag:textQuestionView.tag * 100 + [customer.Response_ID intValue]];
            
            if (allButton.tag == button.tag) {
                allButton.selected = YES;
            }else{
                allButton.selected = NO;
            }
        }
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (sender.contentOffset.x != 0) {
        CGPoint offset = sender.contentOffset;
        offset.x = 0;
        sender.contentOffset = offset;
    }
}

- (void)btnBackTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)Save
{
    NSLog(@"%lu",(unsigned long)self.containerView.subviews.count);
    [textAreaQuestionAnswerArray removeAllObjects];
    [checkBoxQuestionAnswerArray removeAllObjects];
    [radioQuestionAnswerArray removeAllObjects];
    
    
    
    [textAreaQuestionAnswerValidationArray removeAllObjects];
    [checkBoxQuestionAnswerValidationArray removeAllObjects];
    [radioQuestionAnswerValidationArray removeAllObjects];
    
    
    
    for (UIView * subVw in self.containerView.subviews) {
        if (![subVw isKindOfClass:[UILabel class]] && ![subVw isKindOfClass:[UIImageView class]]) {
            SurveyQuestion * question = [modifiedSurveyQuestionsArray objectAtIndex:subVw.tag-1];
            if ([question.Response_Type_ID isEqualToString:@"1"]) {
                
                UITextView * text  = (UITextView *)[subVw viewWithTag:subVw.tag * 100];
                text.text = [text.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                
                NSLog(@"text %@",text.text);
                
                TextQuestionAnswer * answer = [TextQuestionAnswer new];
                answer.Question_ID = question.Question_ID;
                answer.Text = text.text;
                [textAreaQuestionAnswerArray addObject:answer];
                
            }else if ([question.Response_Type_ID isEqualToString:@"3"]){
                
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSMutableArray * idArray = [[NSMutableArray alloc]init];
                NSString *selectedAnsForCheckBoxTypeQ = @"";
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    if (button.selected) {
                        NSLog(@"Seletced");
                        [idArray addObject:customer.Response_ID];
                        NSString *newAns = customer.Response_Text;
                        if (selectedAnsForCheckBoxTypeQ.length==0){
                            selectedAnsForCheckBoxTypeQ = newAns;
                        }else {
                            selectedAnsForCheckBoxTypeQ = [selectedAnsForCheckBoxTypeQ stringByAppendingString:[NSString stringWithFormat:@", %@",newAns]];
                        }
                        

                    }else{
                        NSLog(@"NotSeletced");
                    }
                    
                }
                
                if (selectedAnsForCheckBoxTypeQ.length==0){
                    selectedAnsForCheckBoxTypeQ = @"N/A";
                }
                
                NSLog(@"\n\n *** Final selectedAnsForCheckBoxTypeQ: %@ ***\n\n",selectedAnsForCheckBoxTypeQ);
                
                CheckBoxQuestionAnswer * checkBoxAns = [CheckBoxQuestionAnswer new];
                checkBoxAns.Question_ID = question.Question_ID;
                checkBoxAns.ResponseIdArray = idArray;
                checkBoxAns.Text = selectedAnsForCheckBoxTypeQ;
                [checkBoxQuestionAnswerArray addObject:checkBoxAns];
                
            }else if ([question.Response_Type_ID isEqualToString:@"2"]){
                
                NSMutableArray *SurveyResponseArray = [[NSMutableArray alloc]init];
                NSString *queryForSurveyResponse =[NSString stringWithFormat:@"Select * from TBL_Survey_Responses WHERE Question_ID='%@'",question.Question_ID];
                NSMutableArray * array = [FMDBHelper executeQuery:queryForSurveyResponse];
                
                NSString *selectedAnsForRadioTypeQ = @"N/A";
                NSString * responseID;
                for (NSMutableDictionary *customerDic in array) {
                    SurveyResponse *customer = [SurveyResponse new];
                    customer. Response_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_ID"]];
                    customer. Response_Text = [customerDic valueForKey:@"Response_Text"];
                    customer. Question_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Question_ID"]];
                    customer. Response_Type_ID = [NSString stringWithFormat:@"%@",[customerDic valueForKey:@"Response_Type_ID"]];
                    [SurveyResponseArray addObject: customer];
                    
                    UIButton * button  = (UIButton *)[subVw viewWithTag:subVw.tag * 100 + [customer.Response_ID intValue]];
                    
                    if (button.selected) {
                        NSLog(@"Seletced");
                        responseID = customer.Response_ID;
                        selectedAnsForRadioTypeQ = customer.Response_Text;
                    }else{
                        NSLog(@"NotSeletced");
                    }
                }
                
                NSLog(@"\n\n *** Final selectedAnsForRadioTypeQ: %@ ***\n\n",selectedAnsForRadioTypeQ);
                radioQuestionAnswer * radioAnswer = [radioQuestionAnswer new];
                radioAnswer.Question_ID = question.Question_ID;
                radioAnswer.Response_ID =responseID;
                radioAnswer.Text =selectedAnsForRadioTypeQ;
                [radioQuestionAnswerArray addObject:radioAnswer];
                
            }
            
        }
    }
    
    
    NSInteger numberofQ = textAreaQuestionAnswerArray.count + checkBoxQuestionAnswerArray.count + radioQuestionAnswerArray.count;
    NSInteger numberofA = 0;
    
    if (textAreaQuestionAnswerArray.count > 0) {
        
        for (TextQuestionAnswer * answer in textAreaQuestionAnswerArray) {
            
            if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]]){}
            else
            {
                numberofA++;
            }
        }
    }
    if (checkBoxQuestionAnswerArray.count > 0) {
        
        for (CheckBoxQuestionAnswer *answer in checkBoxQuestionAnswerArray)
        {
            for (NSInteger i = 0; i< answer.ResponseIdArray.count; i++)
            {
                if ([answer.ResponseIdArray objectAtIndex:i]==nil || [[answer.ResponseIdArray objectAtIndex:i] isEqual:[NSNull null]] ||[[answer.ResponseIdArray objectAtIndex:i] isEqualToString:@""]){}
                else
                {
                    numberofA++;
                    break;
                }
            }
        }
    }
    
    if (radioQuestionAnswerArray.count > 0) {
        
        for (radioQuestionAnswer *answer in radioQuestionAnswerArray)
        {
            if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""]){}
            else
            {
                numberofA++;
            }
        }
    }
    
    if (numberofA == 0 || numberofA < numberofQ)
    {
        [self saveError];
    }
    else
    {
        if (textAreaQuestionAnswerArray.count > 0) {
            
            for (TextQuestionAnswer * answer in textAreaQuestionAnswerArray)
            {
                
                NSString *enteredTextByUser = @"";
                if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]]){
                     enteredTextByUser = @"N/A";
                }else{
                    enteredTextByUser = answer.Text;
                }
                
                [textAreaQuestionAnswerValidationArray addObject:enteredTextByUser];
                [self saveSurveyCustResponse:answer.Question_ID AndResponse:enteredTextByUser];
                
                
                /*
                if ([answer.Text isEqualToString:@""]|| answer.Text==nil ||[answer.Text isEqual:[NSNull null]])
                {
                    
                }
                else
                {
                    [textAreaQuestionAnswerValidationArray addObject:answer.Text];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Text];
                }
                */
            }
        }
        if (checkBoxQuestionAnswerArray.count > 0) {
            
            for (CheckBoxQuestionAnswer *answer in checkBoxQuestionAnswerArray)
            {
                //for (NSInteger i = 0; i< answer.ResponseIdArray.count; i++)
                //{
                    
                    [checkBoxQuestionAnswerValidationArray addObject:answer.Question_ID];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Text];
                    
                    /*
                    if ([answer.ResponseIdArray objectAtIndex:i]==nil || [[answer.ResponseIdArray objectAtIndex:i] isEqual:[NSNull null]] ||[[answer.ResponseIdArray objectAtIndex:i] isEqualToString:@""])
                    {
                        
                    }
                    else
                    {
                        [checkBoxQuestionAnswerValidationArray addObject:[answer.ResponseIdArray objectAtIndex:i]];
                        [self saveSurveyCustResponse:answer.Question_ID AndResponse:[answer.ResponseIdArray objectAtIndex:i]];
                    }
                    */
                //}
            }
        }
        
        if (radioQuestionAnswerArray.count > 0) {
            
            for (radioQuestionAnswer *answer in radioQuestionAnswerArray)
            {
                
                NSLog(@"radio answer before inserting %@", [radioQuestionAnswerValidationArray description]);
                [radioQuestionAnswerValidationArray addObject:answer.Response_ID];
                [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Text];
                
                /*
                if (answer.Response_ID==nil|| [answer.Response_ID isEqualToString:@""])
                {
                    
                }
                else
                {
                    NSLog(@"radio answer before inserting %@", [radioQuestionAnswerValidationArray description]);
                    [radioQuestionAnswerValidationArray addObject:answer.Response_ID];
                    [self saveSurveyCustResponse:answer.Question_ID AndResponse:answer.Response_ID];
                }
                */
            }
        }
        
        NSLog(@"arrays to be saved are %@,%@,%@",[textAreaQuestionAnswerArray description],[checkBoxQuestionAnswerArray description],[radioQuestionAnswerArray description]);
        
        [self saveAlert];
    }
}
-(void) saveSurveyCustResponse :(NSString *)questionID AndResponse :(NSString *)response
{
    NSDate * startDate = [NSDate date];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *startDateString =[formatter stringFromDate:startDate];
    
    
    NSString * Customer_Survey_ID =[NSString createGuid];
    
    NSString* survey_ID= [NSString stringWithFormat:@"%d" ,appDelegate.surveyDetails.Survey_ID];
    
    NSString* salesRep_ID= [NSString stringWithFormat:@"%d",appDelegate.surveyDetails.SalesRep_ID];
    
    NSString* empCode=[[SWDefaults userProfile]stringForKey:@"Emp_Code"];
    
    NSString* customer_ID=[[SWDefaults customer] stringForKey:@"Customer_ID"];
    
    NSString* site_Use_ID=[NSString stringWithFormat:@"%d",appDelegate.surveyDetails.Site_Use_ID ];
    
    NSString* Question_ID=[NSString stringWithFormat:@"%@", questionID];
    
    NSString* survey_By=[NSString stringWithFormat:@"%d", SurveyedBy];
   
    /*
    NSLog(@"survey by %@", survey_By);
    
    
    NSLog(@"new logs");
    NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
    
    
    NSLog(@"question id is %@", questionID);
    NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
    
    NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
    
    NSLog(@"Customer id is %@", [[SWDefaults customer] stringForKey:@"Customer_ID"]);
    
    //NSLog(@"customer survey ID %@", [NSString createGuid]);
    
    
    NSLog(@"new survey type is %@", appDelegate.SurveyType);
    */
    if ([appDelegate.SurveyType isEqualToString:@"N"]) {
        
        
        NSString *temp = @"insert into TBL_Survey_Cust_Responses(Customer_Survey_ID,Survey_ID,Question_ID,Response,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Survey_Timestamp) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')";
        
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
        
        
        
        if (!response || response==nil ||[response isEqual:[NSNull null]] || [response isEqualToString:@""]) {
            response=@" ";
            surveyIncomplete=YES;
            
            
            
            if (!saveError || ![[NSUserDefaults standardUserDefaults] boolForKey:@"alertShown"]) {
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"alertShown"];
                [self saveError];
                
            }
            
        }
        else{
            
            questionsCount=radioQuestionAnswerArray.count+checkBoxQuestionAnswerArray.count+textAreaQuestionAnswerArray.count;
            answersCount=radioQuestionAnswerValidationArray.count+checkBoxQuestionAnswerValidationArray.count+textAreaQuestionAnswerValidationArray.count;
            
            
            
           // NSLog(@"questions count is %lu", (unsigned long)questionsCount);
            //NSLog(@"answers count is %lu", (unsigned long)answersCount);
            
            
            // }
            
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:customer_ID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: site_Use_ID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:salesRep_ID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString: empCode];
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:startDateString];
            
            //NSLog(@"temp query is %@", temp);
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
            
            if (!saveSuccess ) {
                
                if (answersCount<questionsCount || answersCount==questionsCount ) {
                    
                    NSLog(@"\n\n\n **** Hint: Query count: %d\n\nrespone string is: %@ \n\nfor InsertdataCustomerResponse: %@  ***\n\n\n",countForQueryInsert,response,temp);
                    countForQueryInsert ++;
                    
                    [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];
                    //                    [self saveAlert];
                }
            }
        }
    }
    else{
        
        NSString * temp=@"insert into TBL_Survey_Audit_Responses(Audit_Survey_ID,Survey_ID,Question_ID,Response,SalesRep_ID,Emp_Code,Survey_Timestamp,Surveyed_By) Values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')";
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:Customer_Survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString: survey_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:Question_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:response];
        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:salesRep_ID];
        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString: empCode];
        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startDateString];
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:survey_By];
        
        [[SWDatabaseManager retrieveManager]InsertdataCustomerResponse:temp];
        
        if ([response isEqual:@""]) {
            NSLog(@"captured");
        }
    }
}


#pragma mark Alert View Delegate Method

-(void)saveError
{
    saveError=[[UIAlertView alloc]initWithTitle:@"Survey not complete" message:@"Please fill all details" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    saveError.tag=87952;
    [saveError show];
}

-(void)saveAlert
{
    saveSuccess=[[UIAlertView alloc]initWithTitle:@"Survey Completed" message:@"Survey Successfully saved. Do you wish to go with next survey?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    saveSuccess.tag=87760;
    [saveSuccess show] ;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==87760 ) {
        
        if (buttonIndex==1) {
            
            if ([self.surveyParentLocation isEqualToString:@"Visit"]) {
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
            }
            
            else
            {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else if (alertView.tag==87952)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"alertShown"];
    }
}


#pragma mark textView delegate methods

- (IBAction)DismissKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end


