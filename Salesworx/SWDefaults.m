//
//  SWDefaults.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDefaults.h"
#import "SWPlatform.h"



@implementation SWDefaults

+ (void)initializeDefaults {
    
    
    
    
    NSString *lastCollectionReference = [SWDefaults lastCollectionReference];
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    NSString *lastPerformaOrderReference = [SWDefaults lastPerformaOrderReference];
    NSString *lastReturnOrderReference = [SWDefaults lastReturnOrderReference];
    NSString *lastSyncDateString = [SWDefaults lastSyncDate];
    NSString *lastSyncStatusString = [SWDefaults lastSyncStatus];
    NSString *userName = [SWDefaults username];
    NSString *userNameForAct = [SWDefaults usernameForActivate];
    NSString *userPassword = [SWDefaults password];
    NSString *userPasswordForAct = [SWDefaults passwordForActivate];

    NSString *isActivationDone = [SWDefaults isActivationDone];

        if (!lastCollectionReference || lastCollectionReference.length == 0) {
        [SWDefaults setLastCollectionReference:@"M000C0000000000"];
    }   if (!lastOrderReference || lastOrderReference.length == 0) {
        [SWDefaults setLastOrderReference:@"M000S0000000000"];
    }   if (!lastPerformaOrderReference || lastPerformaOrderReference.length == 0) {
        [SWDefaults setLastPerformaOrderReference:@"M000D0000000000"];
    }   if (!lastReturnOrderReference || lastReturnOrderReference.length == 0) {
        [SWDefaults setLastReturnOrderReference:@"M000R0000000000"];
    }   if (!lastSyncDateString || lastSyncDateString.length == 0) {
        [SWDefaults setLastSyncDate:@"Never"];
    }   if (!lastSyncStatusString || lastSyncStatusString.length == 0) {
        [SWDefaults setLastSyncStatus:@"N/A"];
        [SWDefaults setLastSyncOrderSent:@"N/A"];
        [SWDefaults setLastSyncType:@"N/A"];
    }   if (!isActivationDone || isActivationDone.length == 0) {
        [SWDefaults setIsActivationDone:@"NO"];
    }   if (!userName || userName.length == 0) {
        [SWDefaults setUsername:@"demo"];//actual content
    }   if (!userNameForAct || userNameForAct.length == 0) {
        [SWDefaults setUsernameForActivate:@"demo"];
    }   if (!userPassword || userPassword.length == 0) {
        [SWDefaults setPassword:@"demo123"];
    }   if (!userPasswordForAct || userPasswordForAct.length == 0) {
        [SWDefaults setPasswordForActivate:@"demo123"];
    }
}

+ (NSString *)username {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kUsernameKey];
}

+ (void)setUsername:(NSString *)username {
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:kUsernameKey];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)password {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kPasswordKey];
}


+ (void)setPassword:(NSString *)password {
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPasswordKey];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)ClearLoginCredentials
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kPasswordKey];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kUsernameKey];
    
}

+ (NSMutableDictionary *)customer
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentCustomer"] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;

}
+ (void)setCustomer:(NSMutableDictionary *)customerDict
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:customerDict error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];   
}
+ (void)clearCustomer
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)locationFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kLocationFilterCustomerList];
}

+ (void)setLocationFilterForCustomerList:(NSString *)filter {
    [[NSUserDefaults standardUserDefaults] setObject:filter forKey:kLocationFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)filterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kFilterCustomerList];
}

+ (void)setFilterForCustomerList:(NSString *)cus_filter {
    [[NSUserDefaults standardUserDefaults] setObject:cus_filter forKey:kFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

////////
+ (NSString *)statusFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kStatusFilterForCustomerList];
}

+ (void)setStatusFilterForCustomerList:(NSString *)status_filter {
    [[NSUserDefaults standardUserDefaults] setObject:status_filter forKey:kStatusFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)paymentFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kPaymentFilterForCustomerList];
}

+ (void)setPaymentFilterForCustomerList:(NSString *)payment_filter {
    [[NSUserDefaults standardUserDefaults] setObject:payment_filter forKey:kPaymentFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
///////
+ (NSString *)codeFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kCodeFilterForCustomerList];
}

+ (void)setCodeFilterForCustomerList:(NSString *)code_filter {
    [[NSUserDefaults standardUserDefaults] setObject:code_filter forKey:kCodeFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)barCodeFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kBarCodeFilterForCustomerList];
}

+ (void)setbarCodeFilterForCustomerList:(NSString *)barcode_filter {
    [[NSUserDefaults standardUserDefaults] setObject:barcode_filter forKey:kBarCodeFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)nameFilterForCustomerList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kNameFilterForCustomerList];
}

+ (void)setNameFilterForCustomerList:(NSString *)name_filter {
    [[NSUserDefaults standardUserDefaults] setObject:name_filter forKey:kNameFilterForCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
//////
+ (NSString *)lastCollectionReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kLastCollectionRef];
}

+ (void)setLastCollectionReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:kLastCollectionRef];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)filterForProductList {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kFilterCustomerList];
}

+ (void)setFilterForProductList:(NSString *)pro_filter {
    [[NSUserDefaults standardUserDefaults] setObject:pro_filter forKey:kFilterCustomerList];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterBrand {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductBrandFilter];
}

+ (void)setProductFilterBrand:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductBrandFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterName {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductNameFilter];
}

+ (void)setProductFilterName:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductNameFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)productFilterProductID {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kProductProductIDFilter];
}

+ (void)setProductFilterProductID:(NSString *)value {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:kProductProductIDFilter];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)userProfile {

    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:kUserProfile] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;
}


+ (void)setUserProfile:(NSDictionary *)array {
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:array error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:kUserProfile];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)productCategory
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"ProductCategory11"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    
    return testServerDict;
    
}
+ (void)setProductCategory:(NSDictionary *)category
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:category error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"ProductCategory11"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)clearProductCategory
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentCustomer"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}
//[[NSUserDefaults standardUserDefaults] setObject:yourMutableArray forKey:@"Key"];
//NSMutableArray *array = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Key"]];

+ (NSString *)paymentType
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"PaymentType"];

}

+ (void)setPaymentType:(NSString *)value
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"PaymentType"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)currentVisitID;
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"VisitOnHold_ID"];

}

+ (void)clearCurrentVisitID
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"VisitOnHold_ID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setCurrentVisitID:(NSString *)visit_ID;
{
    [[NSUserDefaults standardUserDefaults] setObject:visit_ID forKey:@"VisitOnHold_ID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)orderAmount
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"OrderAmount"];

}

+ (NSString *)pdc_due
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"PDC_Due"];
    
}
+ (NSString *)Credit_Period
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"Credit_Period"];
    
}
+ (NSString *)Over_Due
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"Credit_Period"];
    
}
+ (void)setOrderAmount:(NSString *)orderAmount CreditLimit:(NSString *)credit_Limit PDCDUE:(NSString *)pdc_due Credit_Limit:(NSString *)credit_period OverDues:(NSString *)over_due
{
    [[NSUserDefaults standardUserDefaults] setObject:orderAmount forKey:@"OrderAmount"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:credit_Limit forKey:@"Credit_Limit"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:pdc_due forKey:@"PDC_Due"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:credit_period forKey:@"Credit_Period"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:over_due forKey:@"Over_Due"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)availableBalance
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"avl_bal"];

}
+ (void)setAvailableBalance:(NSString *)avl_bal
{
    [[NSUserDefaults standardUserDefaults] setObject:avl_bal forKey:@"avl_bal"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastOrderRef"];
}

+ (void)setLastOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastOrderRef"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastPerformaOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastPerformaOrderRef"];
}

+ (void)setLastPerformaOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastPerformaOrderRef"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastReturnOrderReference {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"LastReturnsOrder"];
}

+ (void)setLastReturnOrderReference:(NSString *)ref {
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"LastReturnsOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)serverArray
{
    
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"serverArray"];

}
+ (void)setServerArray:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"serverArray"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)selectedServerDictionary
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"serverDictionary"];

}
+ (void)setSelectedServerDictionary:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"serverDictionary"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)activationStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"actStats"];
    
}
+ (void)setActivationStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"actStats"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncdate"];

}


+ (void)setLastSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncdate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncstatus"];

}
+ (void)setLastSyncStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncstatus"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncType
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncType"];
}
+ (void)setLastSyncType:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncType"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastSyncOrderSent
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastSyncOrderSent"];
}

+ (void)setLastSyncOrderSent:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastSyncOrderSent"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSDictionary *)licenseKey
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"licenseKey"] dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    return testServerDict;
}
+ (void)setLicenseKey:(NSDictionary *)category
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:category error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"licenseKey"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}



+ (NSString *)usernameForActivate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"usernameForActivate"];

}
+ (void)setUsernameForActivate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"usernameForActivate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)passwordForActivate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"passwordForActivate"];
    
}
+ (void)setPasswordForActivate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"passwordForActivate"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)isActivationDone
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"isactivationdone"];
    
}
+ (void)setIsActivationDone:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"isactivationdone"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (int)collectionCounter
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"collectionScore"];
}
+ (void)setCollectionCounter:(int)ref
{
    [[NSUserDefaults standardUserDefaults] setInteger:ref forKey:@"collectionScore"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)licenseCustomerID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"licenseCustomerID"];
}
+ (void)setLicenseCustomerID:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"licenseCustomerID"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)licenseIDForInfo
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"licenseIDForInfo"];
}
+ (void)setLicenseIDForInfo:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"licenseIDForInfo"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)lastFullSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastFullSyncdate"];
}
+ (void)setLastFullSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastFullSyncdate"];
}

+ (NSString *)lastVTRXBackGroundSyncDate
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"lastVTRXBackgroundSyncdate"];
}
+ (void)setLastVTRXBackGroundSyncDate:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"lastVTRXBackgroundSyncdate"];
}

+(BOOL)isVTRXBackgroundSyncRequired
{
    AppControl *appcontrol=[AppControl retrieveSingleton];
    if([appcontrol.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode]) {
        
        
        // Get conversion to months, days, hours, minutes
        NSCalendar *sysCalendar = [NSCalendar currentCalendar];
        NSCalendarUnit unitFlags = NSCalendarUnitMinute;
        NSDateComponents *lastFullSyncTimeInfo = [sysCalendar components:unitFlags fromDate:[self convertNSStringtoNSDate:[self lastFullSyncDate]] toDate:[self convertNSStringtoNSDate:[self fetchCurrentDateTimeinDatabaseFormat]]  options:0];
        
        BOOL isBackgroundSyncTimeIntervelExceeded=YES;
        if([SWDefaults lastVTRXBackGroundSyncDate]!=nil)
        {
            NSDateComponents *BS_TimeInfo = [sysCalendar components:unitFlags fromDate:[self convertNSStringtoNSDate:[self lastVTRXBackGroundSyncDate]] toDate:[self convertNSStringtoNSDate:[self fetchCurrentDateTimeinDatabaseFormat]] options:0];
            if([BS_TimeInfo minute]<[[[AppControl retrieveSingleton] V_TRX_SYNC_INTERVAL] integerValue])
                isBackgroundSyncTimeIntervelExceeded=NO;
        }
        
        if([lastFullSyncTimeInfo minute] >= [[[AppControl retrieveSingleton] V_TRX_SYNC_INTERVAL] integerValue] && isBackgroundSyncTimeIntervelExceeded)
        {
            return YES;
        } else {
            NSLog(@"last V_Trx sync time less than the V_TRX_SYNC_INTERVAL");
        }
        
        return NO;
    }else{
        return NO;
    }
}

-(NSString *)getAppVersionWithBuild
{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@ (%@)", version,build]);
    
    NSString * AppVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
    return AppVersion;
}

+(NSDate*)convertNSStringtoNSDate:(NSString*)dateString
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* currentDate=[dateFormatter dateFromString:dateString];
    return currentDate;
}


+ (NSArray *)appControl
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [ [[NSUserDefaults standardUserDefaults] objectForKey:@"appControll"] dataUsingEncoding:NSUTF8StringEncoding];
    NSArray *testServerDict = [NSArray arrayWithArray:[djsonSerializer deserializeAsArray:dataDict error:&error]  ] ;
    
    return testServerDict;
}
+ (void)setAppControl:(NSArray *)array
{
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    NSError *error;
    NSData *dataDict = [jsonSerializer serializeObject:array error:&error];
    NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding];
    
    [[NSUserDefaults standardUserDefaults] setObject:stringDataDict forKey:@"appControll"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}


+(NSString*)checkMultiUOM

{
    
    
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check multi UOM %@",[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";
        
    }
    else if ([[[appCtrlFlag valueForKey:@"ENABLE_MULTI_UOM"] objectAtIndex:0] isEqualToString:@"Y"] ) {
        
        flag=@"Y";
        
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;
    
}

+(NSString*)checkTgtField

{
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";

    }
    
   
    
   else if ([[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0] isEqualToString:@"Y"] ) {

        flag=@"Y";
    
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;
    
}



+(NSString*)formatStringWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


+(NSString *)getValidStringValue:(id)inputstr
{
    if ([inputstr isEqual:(id)[NSNull null]] || inputstr==nil)
    {
        return [[NSString alloc]init];
    }
    if([inputstr isKindOfClass:[NSString class]] && ([inputstr isEqualToString:@"<null>"] || [inputstr isEqualToString:@"<Null>"]) )
    {
        return [[NSString alloc]init];
    }
    
    return [NSString stringWithFormat:@"%@",inputstr];
}



+(UILabel*)fetchTitleView:(NSString*)title
{
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = title;
    return label;

}

+(NSString*)checkTargetSalesDashBoard

{
    //"DASHBOARD_TYPE" = "TARGET-SALES";
    
    
    
    
    NSString *flag;
    NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"]);
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        return  flag=@"N";
        
    }
    
    
    
    else if ([[[appCtrlFlag valueForKey:@"DASHBOARD_TYPE"] objectAtIndex:0] isEqualToString:@"TARGET-SALES"] ) {
        
        flag=@"Y";
        
    }
    
    else
    {
        flag=@"N";
    }
    
    return flag;

    
    
    
    
    

    
}

+ (NSString *)versionNumber
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"appVersionNumber"];
}

+ (void)setVersionNumber:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"appVersionNumber"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)startDayStatus
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"startDayStatus"];
}
+ (void)setStartDayStatus:(NSString *)ref
{
    [[NSUserDefaults standardUserDefaults] setObject:ref forKey:@"startDayStatus"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSDictionary*)fetchBarAttributes
{
    NSDictionary *barattributes = [NSDictionary dictionaryWithObject:NavigationBarButtonItemFont
                                                              forKey:NSFontAttributeName];
    
    return barattributes;
    
}


+(UIView*)createNavigationBarTitleView:(NSString*)title
{
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = kFontWeblySleekSemiBold(19);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text =NSLocalizedString(title,nil);
    return label;
}



+ (NSMutableDictionary *)validateAvailableBalance:(NSArray *)orderAmmount
{
    NSString *tempPDC,*tempOD;
    if ([orderAmmount count]!=0)
    {
        NSMutableArray *pdcArray =[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT PDC_DUE,Over_Due  FROM TBL_Customer_Dues  WHERE Customer_ID= '%@' ",[[SWDefaults customer] stringForKey:@"Customer_ID"]]];
        
        if ([pdcArray count]!=0)
        {
            NSMutableDictionary *pdcDict = [pdcArray objectAtIndex:0];
            [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:[pdcDict stringForKey:@"PDC_Due"] Credit_Limit:@"" OverDues:[pdcDict stringForKey:@"Over_Due"]];
            
            tempPDC=[pdcDict stringForKey:@"PDC_Due"] ;
            tempOD=[pdcDict stringForKey:@"Over_Due"] ;
        }
        else
        {
            [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:@"0" Credit_Limit:@"" OverDues:@"0"];
            
            tempPDC=@"N/A" ;
            tempOD=@"N/A" ;
        }
        [SWDefaults setOrderAmount:[[orderAmmount objectAtIndex:0] stringForKey:@"OrderAmount"] CreditLimit:@"" PDCDUE:tempOD Credit_Limit:@"" OverDues:tempOD];
    }
    else
    {
        NSMutableArray *pdcArray =[[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT PDC_DUE,Over_Due  FROM TBL_Customer_Dues  WHERE Customer_ID= '%@' ",[[SWDefaults customer] stringForKey:@"Customer_ID"]]];
        
        if ([pdcArray count]!=0)
        {
            NSMutableDictionary *pdcDict = [pdcArray objectAtIndex:0];
            tempPDC=[pdcDict stringForKey:@"PDC_Due"] ;
            tempOD=[pdcDict stringForKey:@"Over_Due"] ;

        }
        else
        {
            tempPDC=@"N/A" ;
            tempOD=@"N/A" ;

        }
        [SWDefaults setOrderAmount:@"0" CreditLimit:@"" PDCDUE:tempOD Credit_Limit:@"" OverDues:tempOD];
    }
    
    float availableBalance = [[[SWDefaults customer] stringForKey:@"Avail_Bal"] floatValue]-[[SWDefaults orderAmount] floatValue];
    [[SWDefaults customer] setValue:[NSString stringWithFormat:@"%.02f",availableBalance] forKey:@"Avail_Bal"];
    [SWDefaults setAvailableBalance:[NSString stringWithFormat:@"%.02f",availableBalance]];
    
    
    NSMutableDictionary *temp = [SWDefaults customer];
    [temp setValue:tempPDC forKey:@"PDC_Due"];
    [temp setValue:tempOD forKey:@"Over_Due"];
    
    [SWDefaults setCustomer:temp];
    return [SWDefaults customer];
}

+ (void)clearEverything
{
    
}

+(NSString*)applicationDocumentsDirectory
{
    NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
    
    return [filePath path];
    
    
    
}

+(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}

+(NSString*)formatStringWithThousandSeparatorString:(NSString*)str
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSNumber *formattedNumber = [numberFormatter numberFromString:str];
    NSString *result = [numberFormatter stringFromNumber:formattedNumber];
    return result;
}



+(NSString*)fetchDashboardType

{
   NSArray* appCtrl=[SWDefaults appControl];
    
    
    NSString* dashboardType;
    
    if (appCtrl) {
        
        if ([[appCtrl valueForKey:@"CUST_DTL_SCREEN"]isEqualToString:@"DEFAULT"]) {
            
            
            dashboardType= @"DEFAULT";
        }
        
        else
        {
            dashboardType= @"DASHBOARD";
        }
    }
    
    return dashboardType;
    

}


+(void)setSignature:(id)signatureData

{
    [[NSUserDefaults standardUserDefaults]setValue:signatureData forKey:@"signature"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)clearSignature

{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"signature"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

+(id)fetchSignature
{
    id signature=[[NSUserDefaults standardUserDefaults]valueForKey:@"signature"];
    return signature;
    
}



+(void)hideDistributionCheckDate:(BOOL)option
{
    [[NSUserDefaults standardUserDefaults]setBool:option forKey:@"hideDistributionCheckDate"];
}

+(BOOL)fetchDistributionDateStatus
{
    BOOL status=[[NSUserDefaults standardUserDefaults]boolForKey:@"hideDistributionCheckDate"];
    return status;
}

+(void)setOnsiteVisitStatus:(BOOL)option
{
    [[NSUserDefaults standardUserDefaults]
     setBool:option forKey:@"isOnsiteVisit"];
    
    NSLog(@"onsite status in set defaults %hhd",[[NSUserDefaults standardUserDefaults]boolForKey:@"isOnsiteVisit"] );
    
    
}


+(BOOL)fetchOnsiteVisitStatus
{
    BOOL status=[[NSUserDefaults standardUserDefaults]boolForKey:@"isOnsiteVisit"];
    
    NSLog(@"onsite status in fetch defaults %hhd",status );

    
    return status;
}

+(void)setDistributionSaveStatus:(NSDictionary*)parametersDict

{
//    [[NSUserDefaults standardUserDefaults]
//     setBool:option forKey:@"isDistributionCheckSaved"];
    
    NSLog(@"received dict to save is %@", [parametersDict description]);
    
    NSString* custNumber=[parametersDict valueForKey:@"custNumber"];
    NSString* distCheck=[parametersDict valueForKey:@"distributionChecked"];
    
    if (custNumber && distCheck) {
        
        [[NSUserDefaults standardUserDefaults] setObject: parametersDict forKey:@"distributionSaved"];

    }
    
    
    
    
}

+(NSString*)fetchDistributionSaveStatus :(NSString*)customerID{
    
    
    NSDictionary* savedDefaultsDict=[[NSUserDefaults standardUserDefaults]valueForKey:@"distributionSaved"];
   
    NSString* custNumber=[savedDefaultsDict valueForKey:@"custNumber"];
    NSString* distCheck=[savedDefaultsDict valueForKey:@"distributionChecked"];
    
    if (custNumber&&distCheck) {
        
        if ([custNumber isEqualToString:customerID] ) {
            
           
            return distCheck;
            
            
        }
        
        else
        {
            return  nil;
        }
        
    }
    
    else
    {
        return  nil;
        
    }

    
    
    
 
}


+(void)isDCOptionalforOnsiteVisit:(BOOL)status
{
    [[NSUserDefaults standardUserDefaults]setBool:status forKey:@"isDCOptionalandOnsiteVisit"];
    
}

+(BOOL)fetchDCOptionalStatusforOnsiteVisit
{
    BOOL status;
    
    status=[[NSUserDefaults standardUserDefaults]boolForKey:@"isDCOptionalandOnsiteVisit"];
    return status;
    
}


+ (CGSize)screenSize {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    if ((NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        return CGSizeMake(screenSize.height, screenSize.width);
    }
    return screenSize;
}

#pragma mark UIAlertController Methods
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage, nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        [alert addAction:ok];
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController{
    [UIView animateWithDuration:0 animations: ^{
        [viewController.view endEditing:YES];
        
    } completion: ^(BOOL finished) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:NSLocalizedString(alertTitle,nil)
                                      message:NSLocalizedString(alertMessage,nil)
                                      preferredStyle:UIAlertControllerStyleAlert];
        for (NSInteger i=0; i<actionsArray.count; i++) {
            [alert addAction:[actionsArray objectAtIndex:i]];
        }
        [viewController presentViewController:alert animated:YES completion:nil];
    }];
    
}

+(NSString*)fetchCurrentDateTimeinDatabaseFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}

+(NSString*)fetchDatabaseDateFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}
+(NSString*)convertNSDatetoDataBaseDateTimeFormat:(NSDate *)inputDate
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    //NSDate* currentDate=inputDate;
    NSString * formattedDate=[dateFormatter stringFromDate:inputDate];
    return formattedDate;
    
    
}

+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;

    if ([date compare:endDate] == NSOrderedDescending)
        return NO;

    return YES;
}

-(void)AddBackButtonToViewcontroller:(UIViewController *)viewController
{
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSLog(@"language code %@",language);
    
    if([language isEqualToString:@"ar"])
    {
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:KBackButtonArrowImageName] style:UIBarButtonItemStylePlain target:viewController action:@selector(backButtonTapped:)];
        viewController.navigationItem.leftBarButtonItem = barButtonItem;
    }
    else
    {
        UIBarButtonItem * barButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:KBackButtonArrowImageName] style:UIBarButtonItemStylePlain target:viewController action:@selector(backButtonTapped:)];
        viewController.navigationItem.leftBarButtonItem = barButtonItem;
    }
    
}
+(NSPredicate*)fetchMultipartSearchPredicate:(NSString*)searchString withKey:(NSString*)keyString
{
    NSMutableArray *componentsPredicateArray=[[NSMutableArray alloc]init];
    NSPredicate *searchStringComponentsPredicate;
    NSString* filter = @"%K CONTAINS[cd] %@";
    
    NSArray *searchTextComponentsArray=[searchString componentsSeparatedByString:@"*"];
    NSLog(@"search text components are %@", searchTextComponentsArray);
    
    
    for (NSInteger i=0; i<searchTextComponentsArray.count; i++) {
        
        if([[[searchTextComponentsArray objectAtIndex:i]trimString]isEqualToString:@""]){
            
        }
        else{
            if([keyString isEqualToString:@""]){
                
                NSPredicate *componentesPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS [cd] %@", [searchTextComponentsArray objectAtIndex:i]];
                [componentsPredicateArray addObject:componentesPredicate];
                
            }else{
                NSPredicate *componentesPredicate = [NSPredicate predicateWithFormat:filter, keyString, [searchTextComponentsArray objectAtIndex:i]];
                [componentsPredicateArray addObject:componentesPredicate];
            }
            
        }
        
        NSLog(@"components predicate array %@", componentsPredicateArray);
        
        
    }
    
    searchStringComponentsPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:componentsPredicateArray];
    
    return searchStringComponentsPredicate;
}

+ (void)redirectLogToDocuments
{
    NSString *documentPathComponent=[[SWDefaults fetchPreviousDeviceLogsFileNames]objectAtIndex:0];
    NSString *logFilePath=[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent:documentPathComponent];
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

+(NSString*)fetchDeviceLogsFolderPath
{
    NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
    NSString* deviceLogsPath=[documentsDirectory stringByAppendingPathComponent:@"Device Logs"];
    BOOL isDir=YES;
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:deviceLogsPath isDirectory:&isDir]==NO) {
        [[NSFileManager defaultManager]createDirectoryAtPath:deviceLogsPath withIntermediateDirectories:NO attributes:nil error:nil];
        deviceLogsPath=[documentsDirectory stringByAppendingPathComponent:@"Device Logs"];
    }
    return deviceLogsPath;
}

+(void)deletePreviousMonthlyLog
{
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate: currentDate];
    
    long i = components.month;
    NSMutableString *documentsDirectory=[[SWDefaults applicationDocumentsDirectory] mutableCopy];
    NSString *documentPathComponent=[NSString stringWithFormat:@"DeviceLogs-%ld.txt",(long)i];
    NSString *fileSavePath=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
    
    if ([[NSFileManager defaultManager]fileExistsAtPath:fileSavePath]==YES) {
        //delete file
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:fileSavePath error:&error];
        if (success==YES) {
            NSLog(@"Previous monthly log file deleted on %@", [self fetchCurrentDateTimeinDatabaseFormat]);
        }
    }
}

+(void)deleteOldDeviceLogFiles
{
    //delete monthly log file
    [self deletePreviousMonthlyLog];
    
    
    //deleting previous all logs except for previous 3 days
    NSMutableArray* previousThreeFilesArray=[SWDefaults fetchPreviousDeviceLogsFileNames];
    
    NSError * error;
    NSMutableArray * totalFilesArray =  [[[NSFileManager defaultManager]
                                          contentsOfDirectoryAtPath:[SWDefaults fetchDeviceLogsFolderPath] error:&error] mutableCopy];
    
    for (NSInteger i=0; i<previousThreeFilesArray.count; i++) {
        if ([totalFilesArray containsObject:[previousThreeFilesArray objectAtIndex:i]]) {
            [totalFilesArray  removeObject:[previousThreeFilesArray objectAtIndex:i]];
        }
        else{
        }
    }
    
    //deleting files
    for (NSInteger i=0; i<totalFilesArray.count; i++) {
        [[NSFileManager defaultManager]removeItemAtPath:[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent:[totalFilesArray objectAtIndex:i]] error:nil];
    }
}

+(NSMutableArray*)fetchPreviousDeviceLogsFileNames
{
    NSMutableArray* fileNamesArray=[[NSMutableArray alloc]init];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear
                                    | NSCalendarUnitMonth | NSCalendarUnitDay
                                               fromDate:[NSDate date]];
    for (int i = 0; i < 7; ++i) {
        NSDate *date = [calendar dateFromComponents:components];
        // NSLog(@"%d days before today = %@", i, date);
        --components.day;
        
        NSString* dateString=   [[SWDefaults fetchLogsDateFormat] stringFromDate:date];
        NSString *DeviceLogsFileName=[NSString stringWithFormat:@"DeviceLogs-%@.txt",dateString];
        
        [fileNamesArray addObject:DeviceLogsFileName];
    }
    return fileNamesArray;
}

+(NSDateFormatter*)fetchLogsDateFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return dateFormatter;
}

@end
