//
//  CustomerStatementFilterViewController.m
//  SalesWars
//
//  Created by Prasann on 15/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import "CustomerStatementFilterViewController.h"
#import "SWDefaults.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepDefaults.h"
#import "AlSeerPopupViewController.h"
#import "NSPredicate+Distinct.h"

@interface CustomerStatementFilterViewController ()

@end

@implementation CustomerStatementFilterViewController
@synthesize customerDictionary, previousFilterParametersDict, customerNameTextField, toDateTextField, fromDateTextField, selectedTextFieldType, customerStatementArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    UINavigationBar *bar = [self.navigationController navigationBar];
    bar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
    bar.tintColor = [UIColor whiteColor];
    bar.translucent = NO;
    
    UIBarButtonItem * clearButton=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearButtontapped)];
    self.navigationItem.leftBarButtonItem=clearButton;
    
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    self.navigationItem.rightBarButtonItem=closeButton;
    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerDictionary = [[NSMutableDictionary alloc]init];
    if (previousFilterParametersDict.count>0) {
        
        filterParametersDict = previousFilterParametersDict;
    }
    else
    {
        previousFilterParametersDict=[[NSMutableDictionary alloc]init];
        
    }
    
    NSString *selectedCustomerName = [SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kCustomerNameTextField]];
    NSString *selectedTODate= [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kToDateTextField]]];
    NSString *selectedFromDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kFromDateTextField]]];
    
    if ([NSString isEmpty:selectedCustomerName] == NO) {
        customerNameTextField.text=[NSString stringWithFormat:@"%@",selectedCustomerName];
    }
    
    if ([NSString isEmpty:selectedFromDate] == NO) {
        fromDateTextField.text = [NSString stringWithFormat:@"%@",selectedFromDate];
        
    }
    
    if ([NSString isEmpty:selectedTODate] == NO) {
        toDateTextField.text = [NSString stringWithFormat:@"%@",selectedTODate];
    }
    
    selectedTextFieldType = [[NSString alloc]init];
    
}

-(void)clearButtontapped
{
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerNameTextField.text = @"";
    toDateTextField.text = @"";
    fromDateTextField.text = @"";
    
}
-(void)closeButtonTapped
{
    [self.filterPopOverController dismissPopoverAnimated:YES];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField == customerNameTextField) {
        selectedTextField = kCustomerNameTextField;
        selectedPredicateString = kCustomerNameTextField;
        titleText = kCustomerNameTitle;
//        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
//        return NO;

        
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        currencyTypeViewController.isFromReport = YES;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:customerNameTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
        return NO;
    }
    else if (textField == fromDateTextField){
        selectedTextFieldType = kFromDateTextField;
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = NSLocalizedString(@"Date", nil);
        popOverVC.datePickerMode=kTodoTitle;
        popOverVC.setMinimumDateCurrentDate=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController = datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:fromDateTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        return NO;
        
    }else if (textField == toDateTextField){
        selectedTextFieldType = kToDateTextField;
        
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = NSLocalizedString(@"Date", nil);
        popOverVC.datePickerMode=kTodoTitle;
        popOverVC.setMinimumDateCurrentDate=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController = [[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate = self;
        popOverVC.datePickerPopOverController = datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:toDateTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        return NO;
        
    }
    return NO;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    AlSeerPopupViewController *filterDescVC=[[AlSeerPopupViewController alloc]init];
    filterDescVC.selectedFilterDelegate = self;
    filterDescVC.descTitle = titleText;
    filterDescVC.filterNavController = self.filterNavController;
    filterDescVC.filterPopOverController = self.filterPopOverController;
    
    NSMutableArray *filterDescArray = [[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:@"CustomerName"];
    unfilteredArray = [[customerStatementArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    
    
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:[SWDefaults getValidStringValue:@"CustomerName"]];
    
    
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

-(void)selectedContent:(id)selectedObject
{
    if ([selectedTextField isEqualToString:kCustomerNameTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:@"Customer_Name"];
        customerNameTextField.text=[SWDefaults getValidStringValue:selectedObject];
    }
}

NSString *removeLastWord(NSString *str) {
    __block NSRange lastWordRange = NSMakeRange([str length], 0);
    NSStringEnumerationOptions opts = NSStringEnumerationByWords | NSStringEnumerationReverse | NSStringEnumerationSubstringNotRequired;
    [str enumerateSubstringsInRange:NSMakeRange(0, [str length]) options:opts usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        lastWordRange = substringRange;
        *stop = YES;
    }];
    return [str substringToIndex:lastWordRange.location];
}

- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDictionary = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterParametersDict setValue:[customerDictionary stringForKey:@"Customer_Name"] forKey:@"Customer_Name"];
    customerNameTextField.text = [SWDefaults getValidStringValue:[customerDictionary stringForKey:@"Customer_Name"]];
}

-(void)didSelectDate:(NSString *)selectedDate
{
    if ([selectedTextFieldType isEqualToString:kFromDateTextField] ) {
        [fromDateTextField setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];
        
        NSString *fromDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [filterParametersDict setValue:fromDate forKey:@"From_Date"];
    }else{
        [toDateTextField setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];
        
        NSString *toDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [filterParametersDict setValue:toDate forKey:@"To_Date"];
    }
}
- (IBAction)searchButtonTapped:(id)sender {
    NSMutableArray * filteredCustomerStatementArray = [[NSMutableArray alloc]init];
    NSMutableArray* predicateArray = [[NSMutableArray alloc]init];
    NSString *selectedCustomerName = [SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Customer_Name"]];
    NSString *selectedFromDate = [NSString stringWithFormat:@"%@ 00:00:00",[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"From_Date"]]];
    NSString *selectedToDate = [NSString stringWithFormat:@"%@ 23:59:59",[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"To_Date"]]];
    
    if (selectedCustomerName.length >0) {
        
        // Search from back to get the last space character
        NSRange range = [selectedCustomerName rangeOfString: @" " options: NSBackwardsSearch];
        // Take the first substring: from 0 to the space character
        NSString *str1 = [selectedCustomerName substringToIndex: range.location];
        
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"(%K CONTAINS[c] %@)",@"CustomerName",[NSString stringWithFormat:@"%@",str1]]];
    }
    if (selectedFromDate.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"(InvDate >= %@)",selectedFromDate]];
    }
    if (selectedToDate.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"(InvDate <= %@)",selectedToDate]];
    }
    
    NSLog(@"predicate array is %@", predicateArray);
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredCustomerStatementArray = [[customerStatementArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    
    if (predicateArray.count==0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Filter Criteria" andMessage:@"Please select filter criteria and try again" withController:self];
    }
    else  if (filteredCustomerStatementArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredCustomerStatement:)]) {
            previousFilterParametersDict=filterParametersDict;
            [self.delegate filteredCustomerStatement:filteredCustomerStatementArray];
            [self.delegate filterParametersCustomerStatement:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}


- (IBAction)resetButtonTapped:(id)sender {
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerNameTextField.text = @"";
    fromDateTextField.text = @"";
    toDateTextField.text = @"";
    
    if ([self.delegate respondsToSelector:@selector(customerStatementFilterDidReset)]) {
        [self.delegate customerStatementFilterDidReset];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
