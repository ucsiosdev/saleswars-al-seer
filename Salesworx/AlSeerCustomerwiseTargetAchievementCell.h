//
//  AlSeerCustomerwiseTargetAchievementCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerCustomerwiseTargetAchievementCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *btgLbl;

@end
