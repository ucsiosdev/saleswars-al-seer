//
//  AlseerSalesOrderProductListTableViewCell.h
//  SalesWars
//
//  Created by Prasann on 24/11/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AlseerSalesOrderProductListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *productDescriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *productItemCodeLabel;
@property (strong, nonatomic) IBOutlet UIView *statusIndicatorView;

@end

NS_ASSUME_NONNULL_END
