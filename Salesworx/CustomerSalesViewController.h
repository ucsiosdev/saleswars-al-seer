//
//  CustomerSalesViewController.h
//  Salesworx
//
//  Created by msaad on 6/3/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWPlatform.h"

@interface CustomerSalesViewController : SWViewController <  GridViewDataSource , GridViewDelegate>
{
    //
    ;
    GridView *gridView;
    NSMutableDictionary *customer;
    NSMutableDictionary *product;
    NSMutableArray *productSalesArray;
    ProductHeaderView *productHeaderView;
    int bucketSize;
    UILabel *discountLable;


}
- (id)initWithCustomer:(NSMutableDictionary *)customer andProduct:(NSMutableDictionary *)product;
-(void)updateViewDataWithProductID:(NSMutableDictionary *)productID;

@end
