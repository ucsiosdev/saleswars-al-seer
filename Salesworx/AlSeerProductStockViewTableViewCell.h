//
//  AlSeerProductStockViewTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerProductStockViewTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *expiryLbl;

@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@end
