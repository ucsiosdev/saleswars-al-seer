//
//  LineChartView.m
//  SWPlatform
//
//  Created by msaad on 2/4/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "LineChartView.h"

@implementation LineChartView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _lineChartView=nil;
        _lineChartView = [[PCLineChartView alloc] initWithFrame:CGRectMake(10,10,[self bounds].size.width-20,[self bounds].size.height-20)];
		[_lineChartView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
		_lineChartView.minValue = 0;
		_lineChartView.maxValue = 100;
		[self addSubview:_lineChartView];
        
        NSMutableArray *components = [NSMutableArray array];
        NSDictionary *point = [NSMutableDictionary dictionary];
        NSMutableArray *xPoints = [NSMutableArray array];
        
        for (int i= 1; i<=31; i++)
        {
            [xPoints addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc] init];
        [component setTitle:[point objectForKey:@"title"]];
        [component setPoints:[point objectForKey:@"data"]];
        [component setShouldLabelValues:NO];
        [component setColour:PCColorBlue];
        [components addObject:component];
        
		[_lineChartView setComponents:components];
		[_lineChartView setXLabels:xPoints];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
