//
//  SWOrderHistoryViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerOrderHistoryView.h"


@interface SWOrderHistoryViewController : SWViewController {
    
    IBOutlet MedRepElementDescriptionLabel *customerNameLabel;
    IBOutlet MedRepElementTitleLabel *customerNumberLabel;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;
    IBOutlet UITableView *orderHistoryTableView;
}

@property(strong,nonatomic) NSMutableDictionary * customerDictornary;
@property(strong, nonatomic) NSArray *orderHistoryArray;

@end
