//
//  AddFeedbackCommentViewController.m
//  SalesWars
//
//  Created by Neha Gupta on 2/5/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import "AddFeedbackCommentViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"

@interface AddFeedbackCommentViewController ()<UITextViewDelegate>

@end

@implementation AddFeedbackCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Add Feedback";
    self.navigationItem.titleView = label;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Add Feedback"];
 
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = closeButton;
    
    commentTextView.layer.borderColor = UITextViewBorderColor.CGColor;
    commentTextView.layer.borderWidth = 1.0;
    commentTextView.layer.cornerRadius = 3;
}

-(void)saveTapped
{
    [self.view endEditing:YES];
    if ([commentTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Data" andMessage:@"Please give your feedback to save" withController:self];
    } else {
        if ([self.AddFeedbackCommentDelegate respondsToSelector:@selector(selectFeedback:)]) {
            [self.AddFeedbackCommentDelegate selectFeedback:[commentTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    }
}

#pragma mark UITextView Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= FeedbackOtherReasonTextViewLimit;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
