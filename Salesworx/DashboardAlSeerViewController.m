



//
//  DashboardAlSeerViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "DashboardAlSeerViewController.h"
#import "ZUUIRevealController.h"
#import "SWDatabaseManager.h"
#import "ChartTableViewCell.h"
#import "DataTableViewCell.h"
#import "AlSeerDashboardChartTableViewCell.h"
#import "AlSeerTargetAchievementViewController.h"
#import "IdleTimeInfoViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface DashboardAlSeerViewController ()

@end

@implementation DashboardAlSeerViewController




@synthesize agencyArray,chartView,totalInvoicesLbl,previousDaySaleLbl,balancetoGoLbl,achLbl,targetArray,totalTargetLbl,salesTargetItemsArray,plannedLbl,completedLbl,outofRouteLbl,adherenceLbl,orderCount,returnCount,dailyTargetValue,targetAchievedLbl,timeGoneLbl,dashTblView, graphTableView,lblTotalTargetCurreny,lblTotalSalesCurrency,lblLastDaySalesCurrency,lblBalanceToGoCurrency;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    BOOL checkWelcome=[[NSUserDefaults standardUserDefaults]boolForKey:@"welcomeDisplayed"];
    
    if (checkWelcome==NO) {
        welcomeVC=[[AlSeerWelcomeViewController alloc]init];
        welcomeVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:welcomeVC animated:YES completion:nil];
        [self performSelector:@selector(hideWelcomeView) withObject:nil afterDelay:2.0];
    }
    
    NSString* userCurrencyCode = @"N/A";
    if ([[SWDefaults userProfile] valueForKey:@"Currency_Code"] != nil){
        userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
    }
    
    NSLog(@"user profile is %@", userCurrencyCode );
    
    //set currency
    lblTotalTargetCurreny.text = [NSString stringWithFormat:@"Total Target (%@)",userCurrencyCode];
    lblTotalSalesCurrency.text = [NSString stringWithFormat:@"Total Sales (%@)",userCurrencyCode];
    lblLastDaySalesCurrency.text = [NSString stringWithFormat:@"Last Day Sales (%@)",userCurrencyCode];
    lblBalanceToGoCurrency.text = [NSString stringWithFormat:@"Balance To Go (%@)",userCurrencyCode];
    
    
    [self setAllViewBorder];
    dashTblView.tableFooterView = [UIView new];
    
    NSArray *usernameArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select IFNULL(Emp_Name,'N/A') AS Username from TBL_User"];
    if (usernameArray.count > 0) {
        NSString *userName = [[usernameArray valueForKey:@"Username"] objectAtIndex:0];
        self.lblAppUserName.text=userName;
    }
    
    if ([SWDefaults lastSyncDate].length > 0 && ![[SWDefaults lastSyncDate] isEqualToString:@"N/A"]) {
        
        NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
        [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [customDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
        NSDate *lastSyncDate = [customDateFormatter dateFromString:[SWDefaults lastSyncDate]];
        
        [customDateFormatter setDateFormat:@"dd MMM, yyyy hh:mm a"];
        NSString *todayString = [NSString stringWithFormat:@"%@",[customDateFormatter stringFromDate:lastSyncDate]];
        
        [self.lastSyncLable setText:todayString];
    }
    
    [self.lastSyncStatusLable setText:[SWDefaults lastSyncStatus]];
    
    if (@available(iOS 15.0, *)) {
        dashTblView.sectionHeaderTopPadding = 0;
        graphTableView.sectionHeaderTopPadding = 0;
    }
}

-(void) setAllViewBorder{
    dashTblView.layer.cornerRadius = 8.0;
    dashTblView.layer.masksToBounds = YES;
    //self.targetVsSalesDetailsView.layer.cornerRadius = 8.0;
   // self.graphView.layer.cornerRadius = 8.0;
   // self.orderStatisticsView.layer.cornerRadius = 8.0;
   // self.visitStatisticsView.layer.cornerRadius = 8.0;
    self.plannedView.layer.cornerRadius = 4.0;
    self.completedView.layer.cornerRadius = 4.0;
    self.outOfRouteView.layer.cornerRadius = 4.0;
    self.adherenceView.layer.cornerRadius = 4.0;
    self.ordersView.layer.cornerRadius = 4.0;
    self.returnsView.layer.cornerRadius = 4.0;
    self.dailyReqView.layer.cornerRadius = 4.0;
    self.dayOrderView.layer.cornerRadius = 4.0;
    graphTableView.layer.cornerRadius = 8.0;
    graphTableView.layer.masksToBounds = YES;
//    self.targetVsSalesDetailsView.layer.masksToBounds = n;
   // self.graphView.layer.masksToBounds = YES;
   // self.orderStatisticsView.layer.masksToBounds = YES;
   // self.visitStatisticsView.layer.masksToBounds = YES;
    self.plannedView.layer.masksToBounds = YES;
    self.completedView.layer.masksToBounds = YES;
    self.outOfRouteView.layer.masksToBounds = YES;
    self.adherenceView.layer.masksToBounds = YES;
    self.ordersView.layer.masksToBounds = YES;
    self.returnsView.layer.masksToBounds = YES;
    self.dailyReqView.layer.masksToBounds = YES;
    self.dayOrderView.layer.masksToBounds = YES;
    
    NSMutableDictionary * salesTargetMonthDataDict=[[SWDatabaseManager retrieveManager] fetchSalesTargetMonthandYear];
    NSString* monthString=[NSString stringWithFormat:@"%@",[salesTargetMonthDataDict valueForKey:@"Month"]];
    if ([NSString isEmpty:monthString]==NO && salesTargetMonthDataDict.count>0) {
        NSString *yearString = [NSString stringWithFormat:@"%@",[salesTargetMonthDataDict valueForKey:@"Year"]];
        NSInteger monthNumber=[[NSString stringWithFormat:@"%@",[salesTargetMonthDataDict valueForKey:@"Month"]] integerValue];
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:(monthNumber-1)];
        self.monthLabel.text=[NSString stringWithFormat:@"- Agency Wise (%@ - %@)", monthName,yearString];
    }
}
- (void)viewDidAppear:(BOOL)animated {
    dashTblView.showsVerticalScrollIndicator = NO;
    [super viewDidAppear:animated];
    dashTblView.showsVerticalScrollIndicator = YES;
}


-(void)hideWelcomeView

{
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"welcomeDisplayed"];
        [self.dashTblView reloadData];
        [self viewDidLoad];
        [self viewWillAppear:YES];
    }];
}



-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


-(void)populateToplevelLabels

{
    //planned visits
    
    NSArray *temp1 = [[SWDatabaseManager retrieveManager] dbGetTotalPlannedVisits];
    if([temp1 count]==0)
    {
        plannedLbl.text = @"0";
    }
    else
    {
        plannedLbl.text = [[temp1 objectAtIndex:0] stringForKey:@"Total"];
    }

    
    
    //completed visits
    
    NSArray *temp5 = [[SWDatabaseManager retrieveManager] dbGetTotalCompletedVisits];
    if([temp5 count]==0)
    {
        completedLbl.text = @"0";
    }
    else
    {
        completedLbl.text = [[temp5 objectAtIndex:0] stringForKey:@"Total"];
    }

    
    
    
    
    //calculate adherence
    
    
    
    
    NSString * plannedStr= [NSString stringWithFormat:@"%@", [[temp1 valueForKey:@"Total"] objectAtIndex:0]];
    NSString * completedStr= [NSString stringWithFormat:@"%@", [[temp5 valueForKey:@"Total"] objectAtIndex:0]];

    
    
    
    double plannedVisits= [[[temp1 valueForKey:@"Total"] objectAtIndex:0] doubleValue];
    
    
    double completedVisits= [[[temp5 valueForKey:@"Total"] objectAtIndex:0] doubleValue];

    double Adherence= (completedVisits/plannedVisits)*100;
    
    if ([plannedStr isEqualToString:@"0"]|| [completedStr isEqualToString:@"0"]) {
       
        adherenceLbl.text=@"0";


    }
    
    else
    {
        adherenceLbl.text=[[NSString stringWithFormat:@"%0.0f", Adherence] stringByAppendingString:@"%"];

        
        
    }
    
    
    
    
    
    
    
    //out of route
    
    
    NSArray *temp6 = [[SWDatabaseManager retrieveManager] dbGetCustomerOutOfRoute];
    
    NSLog(@"out of route respone %@", [temp6 description]);

    if([temp6 count]==0)
    {
        outofRouteLbl.text = @"0";
    }
    else
    {
        outofRouteLbl.text = [temp6 objectAtIndex:0];
        
        NSLog(@"out of route respone %@", [temp6 description]);
        
        
    }
    
    
    //total order
    NSArray *temp7 = [[SWDatabaseManager retrieveManager] dbGetTotalOrder];
    if([temp7 count]==0)
    {
        orderCount.text = @"0";
    }
    else
    {
        orderCount.text = [[temp7 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    
    
    NSArray *temp8 = [[SWDatabaseManager retrieveManager] dbGetTotalReturn];
    if([temp8 count]==0)
    {
        returnCount.text = @"0";
    }
    else
    {
        returnCount.text = [[temp8 objectAtIndex:0] stringForKey:@"Total"];
    }
    
    
    
    
    
    
    
    
    
    
    //target achieved
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    if (targetdatArry.count==0) {
        
        //targetAchievedLbl.text=[NSString stringWithFormat:@"%@",[targetdatArry valueForKey:@"Target_Value"]];
        
    }
    
    
    
    
    
}

-(void)fetchSalesTargets

{

    NSString * salesTargetStr=@"SELECT Classification_1 ,SUM (IFNULL(Custom_Attribute_8,0)) AS Custom_Attribute_8,SUM(IFNULL(Custom_Attribute_7,0)) AS Custom_Attribute_7 ,SUM(IFNULL(Target_Value,0)) As [Target_Value],SUM(IFNULL(Sales_Value,0)) As [Sales_Value],SUM(IFNULL(Custom_Attribute_5,0)) AS LYTM,IFNULL(Custom_Attribute_4,0) AS Time_Gone from tbl_Sales_target_items GROUP BY Classification_1 ORDER BY Classification_1";
    
    
    NSLog(@"query for sales target items %@", salesTargetStr);
    
    
    salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetStr];
    
    
    if (salesTargetItemsArray.count>0) {
        
        NSLog(@"check sales target %@", [salesTargetItemsArray description]);
        
        
        
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];

    self.navigationController.navigationBar.translucent = NO;


    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];

    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:headerTitleFont, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];

    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    self.title=@"Dashboard";
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];
    
    [self fetchSalesTargets];
    
    
    [self populateToplevelLabels];
    
    //fetch total Target
    
    
    if (salesTargetItemsArray.count>0) {
        
        
        NSLog(@"check time gone %@", [salesTargetItemsArray valueForKey:@"Time_Gone"]);
        
        
        NSString* timeGoneStr=[NSString stringWithFormat:@"%@",[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:0]];
        
        NSMutableArray* temp=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<salesTargetItemsArray.count; i++) {
            [temp addObject:[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:i]];
            
        }
        
        
        NSNumber *maxTimeGone=[temp valueForKeyPath:@"@max.integerValue"];
        
        
        
        NSInteger timeGone=[timeGoneStr integerValue];
        
        
//        timeGoneLbl.text=[NSString stringWithFormat:@"%@", [timeGoneStr stringByAppendingString:@"%"]];
        
        timeGoneLbl.text=[[NSString stringWithFormat:@"%d", [maxTimeGone integerValue]] stringByAppendingString:@"%"];
        

    }
    
    
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    
    if (targetdatArry.count>0) {

        
        double tempTarget=[[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]]doubleValue] ;
        
        totalTargetLbl.text=[self thousandSaperatorforDouble:tempTarget];

        if ([[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
            
            
        }
        
        else
        {
        
        float totalTarget=[[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
        
            float achievedTarget=0.0;
            
            
            if ([[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
                
                
            }
            
            else
            {
            
         achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
                
            }
        //
        float balanceTogo= totalTarget-achievedTarget;
        
        float achievementPercent=(achievedTarget/totalTarget)*100;
            
            NSLog(@"achieved total : %f",achievementPercent);
            
            
            
            
            NSString* timeGoneStr=[NSString stringWithFormat:@"%@",[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:0]];
            
            NSMutableArray* temp=[[NSMutableArray alloc]init];
            for (NSInteger i=0; i<salesTargetItemsArray.count; i++) {
                [temp addObject:[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:i]];
                
            }
            
            
            NSNumber *maxTimeGone=[temp valueForKeyPath:@"@max.integerValue"];

            
            NSInteger timeGoneIntval=[maxTimeGone integerValue];
            
            
            NSLog(@"time gone value is %d", timeGoneIntval);
            
            float test= 12.34;
            NSLog(@"test logic %@",[NSString stringWithFormat:@"%0.0f",test]);
            
            
            
            
            NSInteger tempDIff=(achievementPercent/timeGoneIntval)*100;
            
            NSLog(@"ach percentage : %f Time gone: %ld difference: %ld", achievementPercent,(long)timeGoneIntval,(long)tempDIff);
            
            
            
            if (isnan(tempDIff) || isinf(tempDIff)) {
                
            }
            
            
            
            else
            {
                if (achievementPercent >= timeGoneIntval) {
                    
                    achLbl.textColor=[UIColor colorWithRed:(0.0/255.0) green:(192.0/255.0) blue:(115.0/255.0) alpha:1];//[UIColor greenColor];
                }
                else if (tempDIff >= 80)
                {
                    achLbl.textColor=[UIColor colorWithRed:(242.0/255.0) green:(197.0/255.0) blue:(117.0/255.0) alpha:1];
                }
                
                else
                {
                    achLbl.textColor=[UIColor redColor];
                }
            }
            
            
            if (isinf(achievementPercent)) {
                
                
            }
            
            else
            {
        
         achLbl.text=[[NSString stringWithFormat:@"%0.0f",achievementPercent] stringByAppendingString:@"%"];
        
            }
   
        
        
        NSInteger myInt = (NSInteger) balanceTogo;
        
        
        
        
        NSString* strNumber = [self formatWithThousandSeparator:myInt];
        
        //balancetoGoLbl.text=strNumber;
        
        
        }
        
        
        //fetch btg from table
        
        
        NSString* btgQry=@"select sum(custom_Attribute_7) AS Balance_To_Go from TBL_Sales_Target_Items";
        
        NSMutableArray* btgArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:btgQry];
        if (btgArray.count>0) {
            
            if ([[[btgArray objectAtIndex:0]valueForKey:@"Balance_To_Go"] isEqual:[NSNull null]]) {
                
                
            }
            
            else
            {
                
                
                
            
                
                double btgDouble=[[NSString stringWithFormat:@"%@",[[btgArray objectAtIndex:0]valueForKey:@"Balance_To_Go"]] doubleValue];
                balancetoGoLbl.text=[self thousandSaperatorforDouble:btgDouble];
            }

            
            
        }
        
        
        
        //balancetoGoLbl.text=[NSString stringWithFormat:@"%0.0f",balanceTogo];
        
        
        //calculate previous day sales
        
        
        NSString* prevDaySalesQuery=@"SELECT  sum(Custom_Attribute_6) As [Previous_Day_Sales]  FROM TBL_Sales_Target_Items";
        NSMutableArray* prevDaySales=[[SWDatabaseManager retrieveManager]fetchDataForQuery:prevDaySalesQuery];
        
        if (prevDaySales.count>0) {

            
            NSInteger tempTarget=[[NSString stringWithFormat:@"%@",[[prevDaySales valueForKey:@"Previous_Day_Sales"] objectAtIndex:0]]integerValue];

            previousDaySaleLbl.text=[self thousandSaperatorforDouble:tempTarget];
 
        }

        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString=[dateformate stringFromDate:[NSDate date]];
        
        NSLog(@"check time stamp %@", dateString);
        
        NSString* orderQry=[NSString stringWithFormat:@"SELECT IFNULL(SUM(Transaction_Amt),0) AS Target_Achieved FROM TBL_Order_History where Doc_Type='I' and Creation_Date like  '%%%@%%'",dateString];
        
        NSLog(@"order qry is %@", orderQry);
        
        NSMutableArray* achArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:orderQry];
        if (achArr.count==0|| [[achArr valueForKey:@"Target_Achieved"] isEqual:[NSNull null]]) {
            
        }
        
        {
            double temptargetVal=[[NSString stringWithFormat:@"%@",[[achArr valueForKey:@"Target_Achieved"] objectAtIndex:0]]doubleValue];
            targetAchievedLbl.text= [self thousandSaperatorforDouble:temptargetVal];
        }
        NSString* dsgQry=@"SELECT SUM(Custom_Attribute_8) As [Daily_Sales_To_Go], SUM(Sales_Value) AS Sales_Value FROM TBL_Sales_Target_Items";
        dsgArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:dsgQry];
        
        
        if (dsgArr.count>0) {
            
            if ([[[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0] isEqual:[NSNull null]]) {
            }
            else
            {
            NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0]]integerValue];
                NSInteger dailyTarget=[[NSString stringWithFormat:@"%@",[[dsgArr valueForKey:@"Daily_Sales_To_Go"] objectAtIndex:0]] integerValue];
            dailyTargetValue.text=[self thousandSaperatorforDouble:dailyTarget];
            dailyTargetValue.text= [[NSNumber numberWithInt:tempTargetDsg] descriptionWithLocale:[NSLocale currentLocale]];
            NSInteger tempTarget= [[NSString stringWithFormat:@"%@",[[dsgArr valueForKey:@"Sales_Value"] objectAtIndex:0]]integerValue];
                totalInvoicesLbl.text=[self thousandSaperatorforDouble:tempTarget];
            }
        }
    }
    
    
    NSMutableArray* agencyDetails=[[SWDatabaseManager retrieveManager] fetchAgencies];
    NSLog(@"Agancies are %@", [agencyDetails valueForKey:@"Classification_1"]);
    
    if (agencyDetails.count>0) {
        targetArray=[agencyDetails valueForKey:@"Target_Value"];
        agencyArray =[agencyDetails valueForKey:@"Agency"];
        NSLog(@"agency array is %@", [agencyArray description]);


    }
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            
            
            UIBarButtonItem* leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)];
            
            [self.navigationItem setLeftBarButtonItem:leftBtn];
            
        } else {
        }
    }
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"IdleTime_Icon"] style:UIBarButtonItemStylePlain target:self action:@selector(showIdleTimeView)];
    [rightBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    rightBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

-(void)showIdleTimeView
{
    IdleTimeInfoViewController *objVC = [[IdleTimeInfoViewController alloc]init];
    objVC.delegate = self;
    [self presentPopupViewController:objVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (agencyArray.count>0) {
        return agencyArray.count;
    }
    else
    {
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSLog(@"salestarget items array is %@", [[salesTargetItemsArray objectAtIndex:indexPath.row] valueForKey:@"Classification_1"]);
    
    if([stockPopOver isPopoverVisible])
    {
        [stockPopOver dismissPopoverAnimated:YES];
        return;
    }
    
    AlSeerTargetAchievementViewController * tgtVC=[[AlSeerTargetAchievementViewController alloc]init];
    
    tgtVC.selectedAgency=[[salesTargetItemsArray objectAtIndex:indexPath.row] valueForKey:@"Classification_1"];
    
    stockPopOver = [[UIPopoverController alloc]
                    initWithContentViewController:tgtVC];
    stockPopOver.popoverContentSize=CGSizeMake(630, 250);
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    CGRect rect=CGRectMake(cell.bounds.origin.x+420, cell.bounds.origin.y+10, 50, 30);
    
    tgtVC.popOver=stockPopOver;
    
    
    [stockPopOver presentPopoverFromRect:rect inView:cell permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ChartTableViewCell";
    static NSString *datacellIdentifier = @"dataTableViewCell";

    
    tableView.allowsSelection = YES;
    
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    
    
    self.dashTblView.showsHorizontalScrollIndicator=NO;
    self.dashTblView.showsVerticalScrollIndicator=NO;
    
    if (tableView.tag == 1) {
        
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            if ([[salesTargetItemsArray valueForKey:@"Classification_1"]isEqual:[NSNull null]] || [salesTargetItemsArray valueForKey:@"Classification_1"]==nil) {
                cell.agencyLbl.text=@"-";
            }
            else
            {
                cell.agencyLbl.text=
                [[salesTargetItemsArray valueForKey:@"Classification_1"] objectAtIndex:indexPath.row];
            }
            
            cell.contentView.userInteractionEnabled=YES;
            NSInteger dailyTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] integerValue];
            
            
            if (!dailyTarget) {
                
                cell.targetLbl.text=@"0";
                
            }
            
            else
            {
                cell.targetLbl.text= [self thousandSaperatorforDouble:dailyTarget];
            }
            
            
            if ([[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] isEqual:[NSNull null]]|| [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]==nil) {
                cell.MTDsalesLbl.text=@"0";
                cell.achPercentLbl.text=@"0";
                cell.dailySalesLbl.text=@"0";
                cell.btgLbl.text=@"0";
            }
            
            else
            {
                NSInteger dailySale=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]integerValue];
                cell.MTDsalesLbl.text= [self thousandSaperatorforDouble:dailySale];
                NSInteger lytmVal= [[[salesTargetItemsArray valueForKey:@"LYTM"] objectAtIndex:indexPath.row]integerValue];
                
                cell.lytmLbl.text=[self thousandSaperatorforDouble:lytmVal];
                
                
                float totalTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] floatValue];
                
                float achievedTarget=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] floatValue];
                
                float achievementPercent=(achievedTarget/totalTarget)*100;
                
                
                if (totalTarget==0) {
                    cell.achPercentLbl.text=@"0%";
                }
                
                else
                {
                    cell.achPercentLbl.text=[[NSString stringWithFormat:@"%0.0f", achievementPercent] stringByAppendingString:@"%"];
                }
                
                NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row]]integerValue];
                
                cell.btgLbl.text=[self thousandSaperatorforDouble:tempTargetDsg];
                NSInteger dailySalestoGo=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_8"] objectAtIndex:indexPath.row]]integerValue];
                cell.dailySalesLbl.text=[self thousandSaperatorforDouble:dailySalestoGo];
                NSMutableArray* temp=[[NSMutableArray alloc]init];
                for (NSInteger i=0; i<salesTargetItemsArray.count; i++) {
                    [temp addObject:[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:i]];
                }
                
                NSNumber *maxTimeGone=[temp valueForKeyPath:@"@max.integerValue"];
                NSInteger timeGoneIntval=[maxTimeGone integerValue];
                NSInteger achPercentVal= (NSInteger) achievementPercent;
                NSLog(@"ach percent and time gone are %ld %ld", (long)achPercentVal,(long)timeGoneIntval);
                
                
                float tempDifference= (achievementPercent/timeGoneIntval)*100;
                
                NSLog(@"percent difference %f", tempDifference);
                
                if (tempDifference>=100) {
                    cell.actualAchievementLbl.backgroundColor=[UIColor colorWithRed:(0.0/255.0) green:(192.0/255.0) blue:(115.0/255.0) alpha:1];//[UIColor greenColor];
                    NSLog(@"ach  for green is %ld", (long)achPercentVal);
                }
                
                else if (tempDifference>=80.0)
                {
                    cell.actualAchievementLbl.backgroundColor=[UIColor colorWithRed:(242.0/255.0) green:(197.0/255.0) blue:(117.0/255.0) alpha:1];
                }
                
                else
                {
                    cell.actualAchievementLbl.backgroundColor=[UIColor redColor];
                }
            }
        }
        return cell;
    }
    else
    {
        
        tableView.alwaysBounceVertical = NO;
        [tableView setShowsHorizontalScrollIndicator:NO];
        [tableView setShowsVerticalScrollIndicator:NO];
        AlSeerDashboardChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerDashboardChartTableViewCell" owner:nil options:nil] firstObject];
        }
        [cell configUI:indexPath];
        return cell;
        
        
    }
}


-(NSString*)thousandSaperatorforDouble:(NSInteger)number

{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 0) {
        return 169;
    }
    else
    {
        return 45;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag == 0) {
        return 0;
    }else{
        return 45;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (tableView.tag == 0) {
        return nil;
    }else{
        static NSString *datacellIdentifier = @"dataTableViewCell";
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            
            cell.agencyLbl.text=@"Agency";
            cell.targetLbl.text=@"Target";
            cell.MTDsalesLbl.text=@"MTD";
            cell.achPercentLbl.text=@"Achieved (%)";
            cell.btgLbl.text=@"BTG";
            cell.dailySalesLbl.text=@"Daily Req.";
            cell.lytmLbl.text=@"LYTM";
            
            [cell.agencyLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.targetLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.MTDsalesLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.achPercentLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.btgLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.dailySalesLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.lytmLbl setFont:EditableTextFieldSemiFontOfSize(14)];

            cell.agencyLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.targetLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.MTDsalesLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.achPercentLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.btgLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.dailySalesLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.lytmLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            
            cell.backgroundColor = [UIColor colorWithRed:(235.0/255.0) green:(251.0/255.0) blue:(249.0/255.0) alpha:1];
        }
        return cell;
    }
    
}



@end
