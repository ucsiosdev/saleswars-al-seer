//
//  SWCollectionCustListViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "SWViewController.h"
#import "SWLoadingView.h"


@interface SWCollectionCustListViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, ZBarReaderDelegate, UIImagePickerControllerDelegate, UISearchBarDelegate>
{
    NSArray *customerList;
    UITableView *tableView;
    UISearchBar *searchBar;
    NSMutableArray *searchData;
    UIPopoverController *locationFilterPopOver;
    SWLoadingView *loadingView;
    UILabel *infoLabel;
    
    ZBarReaderViewController *reader;
    SEL action;
    UIView *myBackgroundView;
    
    NSMutableDictionary *sectionList;
    NSMutableDictionary *sectionSearch;
    
    UIBarButtonItem *flexibleSpace ;
    UIBarButtonItem *totalLabelButton;
    UIBarButtonItem *displayActionBarButton ;
    UIBarButtonItem *displayMapBarButton ;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end
