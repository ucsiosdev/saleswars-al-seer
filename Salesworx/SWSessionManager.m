//
//  SWSessionManager.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSessionManager.h"
#import "SWSessionConstants.h"
#import "LicensingViewController.h"
#import "LoginViewController.h"
@interface SWSessionManager ()
- (void)login;
@end

@implementation SWSessionManager

@synthesize window;
@synthesize applicationViewController;

- (id)initWithWindow:(UIWindow *)w andApplicationViewController:(UIViewController *)avc {
    self = [super init];
    
    if (self)
    {
        //[self setWindow:w];
       // self.window = [[UIWindow alloc] init] ;
        self.window= w;

        //self.applicationViewController = [[UIViewController alloc] init] ;
        self.applicationViewController = avc;
        //[self setApplicationViewController:avc];
        
        [self.window setBackgroundColor:[UIColor whiteColor]];
        [self.window makeKeyAndVisible];
        
        [self login];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDone:) name:kSessionLoginDoneNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutDone:) name:kSessionLogoutNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(licenseError:) name:kSessionActivationErrorNotification object:nil];
    }
    
    return self;
}

- (void)login
{
    LoginViewController *loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginVC] ;
    [self.window setRootViewController:navigationController];
}

- (void)LicenseView
{
    LicensingViewController *sessionViewController = [[LicensingViewController alloc] init] ;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:sessionViewController] ;
    [self.window setRootViewController:navigationController];
    sessionViewController=nil;
}



#pragma mark Notifications
- (void)loginDone:(NSNotification *)notification
{    
    [self.window setRootViewController:self.applicationViewController];
}

- (void)logoutDone:(NSNotification *)notification   
{
    [SWDefaults setUsername:nil];
    [SWDefaults setPassword:nil];
    [self login];
}

- (void)licenseError:(NSNotification *)notification
{
    [SWDefaults setLicenseKey:nil];
    [SWDefaults setLicenseCustomerID:nil];
    [self LicenseView];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
