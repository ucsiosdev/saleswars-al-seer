//
//  AlSeerDashboardChartTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerDashboardChartTableViewCell.h"
#import "SWDatabaseManager.h"

@implementation AlSeerDashboardChartTableViewCell
@synthesize isCommingFromCustDash;
- (void)awakeFromNib {
    // Initialization code
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


//
//  ChartTableViewCell.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/11/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//


- (void)configUI:(NSIndexPath *)indexPath
{
    if (chartView) {
        [chartView removeFromSuperview];
        chartView = nil;
    }
    
    path = indexPath;
    
    
    if (isCommingFromCustDash == YES) {
        chartView = [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(8, 8, 992, 140)
                                                  withSource:self
                                                   withStyle:UUChartBarStyle];
    }else{
        chartView = [[UUChart alloc]initwithUUChartDataFrame:CGRectMake(10, 20, 1008, 170)
                                                  withSource:self
                                                   withStyle:UUChartBarStyle];
    }
    

    
    [chartView showInView:self.contentView];
    
    
}

- (NSArray *)getXTitles:(NSMutableArray*)num
{
    NSMutableArray *xTitles = [NSMutableArray array];
    for (int i=0; i<num.count; i++) {
        NSString * str = [num objectAtIndex:i];
        [xTitles addObject:str];
    }
    return xTitles;
}

#pragma mark - @required
//横坐标标题数组
- (NSArray *)UUChart_xLableArray:(UUChart *)chart
{
    //
    //    if (path.section==0) {
    //        switch (path.row) {
    //            case 0:
    //                return [self getXTitles:5];
    //            case 1:
    //                return [self getXTitles:11];
    //            case 2:
    //                return [self getXTitles:7];
    //            case 3:
    //                return [self getXTitles:7];
    //            default:
    //                break;
    //        }
    //    }else{
    //        switch (path.row) {
    //            case 0:
    //                return [self getXTitles:11];
    //            case 1:
    //                return [self getXTitles:7];
    //            default:
    //                break;
    //        }
    //    }
    
    
    
    if ([self.customerDetail isEqualToString:@"Dashboard"]) {
        
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDate *currDate = [NSDate date];
        NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                              fromDate:currDate];
        
        NSInteger month = [dComp month];
        
        
        NSLog(@"month is %d", month);
        
        
        
        
        NSString* salesTargetsQry=[NSString stringWithFormat:@"SELECT Classification_1 ,Custom_Attribute_8 ,Custom_Attribute_7 ,Target_Value ,Sales_Value ,Classification_2  FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%d' GROUP BY Classification_1 ORDER BY Classification_1",[self.customerDict valueForKey:@"Customer_No"],month];
        
        
        
        NSLog(@"sales statictics query is %@", salesTargetsQry);
        
        
        
        NSMutableArray* salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetsQry];
        
       
        
        
        NSLog(@"sales array in chart view is %@", [salesTargetItemsArray description]);
        
        
        
        return [self getXTitles:[salesTargetItemsArray valueForKey:@"Classification_1" ]];
        
        
        
        
        
        
        
        
    }
    
    else
    {
        
        
        NSMutableArray* tempAgencyArr=[[SWDatabaseManager retrieveManager] fetchAgencies];
        NSLog(@"Agancies are %@", [tempAgencyArr valueForKey:@"Classification_1"]);
        
        NSMutableArray* tempTitlesArray =[tempAgencyArr valueForKey:@"Agency"];
        
        NSLog(@"agency array is %@", [agencyArray description]);
        
        
    
        
        
        
        
        
        return [self getXTitles:tempTitlesArray];
        
    }
}
//数值多重数组
- (NSArray *)UUChart_yValueArray:(UUChart *)chart
{
    
    if ([self.customerDetail isEqualToString:@"Dashboard"]) {
        
        
        
        NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
        NSDate *currDate = [NSDate date];
        NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                              fromDate:currDate];
        
        NSInteger month = [dComp month];
        
        
        NSLog(@"month is %d", month);
        
        
        
        
        NSString* salesTargetsQry=[NSString stringWithFormat:@"SELECT Classification_1 As Agency,Custom_Attribute_8 ,Custom_Attribute_7 ,Target_Value ,Sales_Value ,Classification_2 FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%d' GROUP BY Classification_1 ORDER BY Classification_1",[self.customerDict valueForKey:@"Customer_No"],month];
        
        
        
        NSLog(@"sales statictics query is %@", salesTargetsQry);
        
        
        
        NSMutableArray* salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetsQry];
        
        
        
        NSLog(@"sales array in chart view is %@", [salesTargetItemsArray description]);
        
        
        
        NSMutableArray*  agencyDetailsArray= [salesTargetItemsArray valueForKey:@"Agency" ];
        
        //        agencyArray =[agencyDetails valueForKey:@"Classification_1"];
        //
        //        NSLog(@"agency array is %@", [agencyArray description]);
        
        
        
        NSMutableArray* targetArray=
        [salesTargetItemsArray valueForKey:@"Target_Value"];
        
        
        NSMutableArray* achievementArray=[salesTargetItemsArray valueForKey:@"Sales_Value"];
        
        
        NSLog(@"target array for customer dash board is %@", [targetArray description]);
        
        NSLog(@"target array for customer dash board is %@", [achievementArray description]);
        
        
        for(NSInteger i=0;i<targetArray.count;i++)
        {
            if([[targetArray objectAtIndex:i] isKindOfClass:[NSNull class]])
                [targetArray removeObjectAtIndex:i];
        }
        
        for(NSInteger i=0;i<achievementArray.count;i++)
        {
            if([[achievementArray objectAtIndex:i] isKindOfClass:[NSNull class]])
                [achievementArray removeObjectAtIndex:i];
            
            //handling negative values
            
            if ([[achievementArray objectAtIndex:i]hasPrefix:@"-"]) {
                
                [achievementArray replaceObjectAtIndex:i withObject:@"0"];
            }
            
            
        
            
        }
        
        NSLog(@"achievement array in dashboard chart %@", [achievementArray description]);
        
        
        
        //        [targetArray removeObjectIdenticalTo:[NSNull null]];
        //
        //
        //        [achievementArray removeObjectIdenticalTo:[NSNull null]];
        
        
        //get target and achievement here
        
        
        
        return @[targetArray,achievementArray];
        
        
        
        
        
        
        
        
    }
    
    else
    {
        
        
        
        
        
        
        NSMutableArray*  agencyDetailsArray=[[SWDatabaseManager retrieveManager] fetchAgencies];
        NSLog(@"Agancies are %@", [agencyDetails valueForKey:@"Classification_1"]);
        
        agencyArray =[agencyDetails valueForKey:@"Classification_1"];
        
        NSLog(@"agency array is %@", [agencyArray description]);
        
        
        
        NSMutableArray* targetArray=[[NSMutableArray alloc]initWithArray:
                                     [agencyDetailsArray valueForKey:@"Target_Value"]];
        
        
        NSMutableArray* achievementArray=[[NSMutableArray alloc]initWithArray:[agencyDetailsArray valueForKey:@"Sales_Value"]];
        
        
        
        NSLog(@"achievement array is %@", [achievementArray description]);
        
        
        if (achievementArray.count>0) {
            
       
        
        for(NSInteger i=0;i<achievementArray.count;i++)
        {
            if([[achievementArray objectAtIndex:i] isKindOfClass:[NSNull class]])
                [achievementArray removeObjectAtIndex:i];
            
            //handling negative values
            NSLog(@"achievement array in loop %@", [achievementArray description]);

            
             if (achievementArray.count>0) {
            
            NSString* tempStr=[NSString stringWithFormat:@"%@",[achievementArray objectAtIndex:i]];
            
            
            if ([tempStr hasPrefix:@"-"]) {
                
                [achievementArray replaceObjectAtIndex:i withObject:@"0"];
            }
            
             }
            
            
            
        }
        
        NSLog(@"achievement array in dashboard chart %@", [achievementArray description]);

        
        [targetArray removeObjectIdenticalTo:[NSNull null]];
        
        
        [achievementArray removeObjectIdenticalTo:[NSNull null]];
        
        
        }
        
        
        
        
        
        
        //get target and achievement here
        
        
        
        return @[targetArray,achievementArray];
        
        
        //    if (path.section==0) {
        //        switch (path.row) {
        //            case 0:
        //                return @[ary];
        //            case 1:
        //                return @[ary4];
        //            case 2:
        //                return @[ary1,ary2];
        //            default:
        //                return @[ary1,ary2,ary3];
        //        }
        //    }else{
        //        if (path.row) {
        //            return @[ary1,ary2];
        //        }else{
        //            return @[ary4];
        //        }
        //    }
    }
}

#pragma mark - @optional
//颜色数组
- (NSArray *)UUChart_ColorArray:(UUChart *)chart
{
    return @[UUYellow,UUGreen];
}
//显示数值范围
- (CGRange)UUChartChooseRangeInLineChart:(UUChart *)chart
{
           //check range based on highest target or sale value
        
        NSString* rangeStr=@"SELECT MAX(Target_Value) AS [Target], MAX(Sales_Value) AS [Sales] FROM (select Classification_1,SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] from TBL_sales_Target_Items GROUP BY Classification_1)";
        
        NSMutableArray* rangeArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:rangeStr];
        
        NSLog(@"check range %@", [[rangeArray valueForKey:@"Target"] objectAtIndex:0]);
        
        
        
        if ([[[rangeArray valueForKey:@"Target"] objectAtIndex:0] isEqual:[NSNull null]]||[[[rangeArray valueForKey:@"Sales"] objectAtIndex:0] isEqual:[NSNull null]]) {
            
            return CGRangeMake(0, 0);
            
        }
        
        
        else
        {
            
            
            
            
            
            NSInteger targetVar=[[[rangeArray valueForKey:@"Target"] objectAtIndex:0] integerValue];
            NSInteger salesVar=[[[rangeArray valueForKey:@"Sales"] objectAtIndex:0] integerValue];
            
            
            NSLog(@"check max %d", MAX(targetVar, salesVar));
            
            
            
            
            NSInteger maxRange=MAX(targetVar, salesVar);
            
            //    if (salesVar>maxRange) {
            //        maxRange=salesVar;
            //    }
            //
            maxRange=(maxRange/1000);
            maxRange=(maxRange+1)*1000;
            
            return CGRangeMake(maxRange, 0);
            
        }
        
   
    // NSArray *sorted = [numbers sortedArrayUsingSelector:@selector(compare:)];
    
    
    
    //return CGRangeMake(80000, 0);
    
    //    if (path.section==0 && (path.row==0|path.row==1)) {
    //        return CGRangeMake(60, 10);
    //    }
    //    if (path.section==1 && path.row==0) {
    //        return CGRangeMake(10000, 0);
    //    }
    //    if (path.row==2) {
    //        return CGRangeMake(100, 0);
    //    }
    //    return CGRangeZero;
}

#pragma mark 折线图专享功能

//标记数值区域
- (CGRange)UUChartMarkRangeInLineChart:(UUChart *)chart
{
    
    return CGRangeMake(25, 75);
    
    //    if (path.row==2) {
    //        return CGRangeMake(25, 75);
    //    }
    //    return CGRangeZero;
}

//判断显示横线条
- (BOOL)UUChart:(UUChart *)chart ShowHorizonLineAtIndex:(NSInteger)index
{
    return YES;
}

//判断显示最大最小值
- (BOOL)UUChart:(UUChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return path.row==2;
}


@end
