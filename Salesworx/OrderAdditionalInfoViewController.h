//
//  OrderAdditionalInfoViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "JBSignatureController.h"
#import "PPSSignatureView.h"
#import "SignatureViewController.h"
#import "PJRSignatureView.h"
#define kImagesFolder @"Signature"


@interface OrderAdditionalInfoViewController : SWViewController <JBSignatureControllerDelegate,EditableCellDelegate,UITextFieldDelegate,UITextViewDelegate , UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSMutableDictionary *salesOrder;
    NSMutableDictionary *info;
    JBSignatureController *signatureController;
    PPSSignatureView *signatureBaseView;
    NSDictionary *customerDict;
    UITableView *tableView;
    UIPopoverController *collectionTypePopOver;
    //
    CustomerHeaderView *customerHeaderView;
    NSString *performaOrderRef;
    UIImage *signatureSaveImage;
    NSString *wholeSaleString;
    NSString *consolidationString;
    NSString *isShipDateAllow;
    NSString *isSignatureOptional;
    NSString *returnTypeString;
    NSString *checkWholesaleOrder;
    NSString *ENABLE_ORDER_CONSOLIDATION;
    NSString *ALLOW_SKIP_CONSOLIDATION;
    
    BOOL isWholeSalesOrder;
    BOOL isEOConsolidation;
    BOOL isASConsolidation;
    UIView *myBackgroundView;
    
    
    PJRSignatureView* pjrSignatureView;
    
    BOOL isWSSelected;
    UIButton *saveButton;
    UIButton *saveAsTemplateButton;
    PPSSignatureView* ppSign; 
}


@property(strong,nonatomic)    NSMutableDictionary *form;

@property(nonatomic)    BOOL isSignature ;

@property(nonatomic,strong)NSString *performaOrderRef;
-(void)dismissPreview;

-(void)closeButtonTapped;

@property(strong,nonatomic) SignatureViewController* signVc;
@property (strong, nonatomic)  PPSSignatureView *glKitView;

@property(strong,nonatomic)NSMutableString* categoryLbl;
@property(strong,nonatomic)NSMutableString* descLbl;
@property(strong,nonatomic)NSMutableArray * warehouseDataArray;
@property(strong,nonatomic)NSMutableArray * itemsSalesOrder;
- (id)initWithOrder:(NSMutableDictionary *)so andCustomer:(NSDictionary *)customer;
@end
