//
//  MCQTypeQuestionView.m
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "MCQTypeQuestionView.h"

@implementation MCQTypeQuestionView

@synthesize tableViewController,checkedIndexPath;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setTableViewController:[[UITableView alloc] initWithFrame:self.frame style:UITableViewStyleGrouped]];
        [self.tableViewController setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.tableViewController setDelegate:self];
        [self.tableViewController setDataSource:self];
        self.tableViewController.backgroundView = nil;
        self.tableViewController.backgroundColor = [UIColor whiteColor];
        if (@available(iOS 15.0, *)) {
            self.tableViewController.sectionHeaderTopPadding = 0;
        }
        [self addSubview:self.tableViewController];
    }
    return self;
}



#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    
        return 2;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    if (section==0)
    {
        return 1;
    }
    else
    {
        return 3;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    


    if (indexPath.section==0 && indexPath.row == 0)
    {
        [cell.textLabel setText:@"Question"];
    }
    else if (indexPath.section==1 &&indexPath.row == 0)
    {
        [cell.textLabel setText:@"Answer 1"];
    }
    else if (indexPath.section==1 &&indexPath.row == 1)
    {
        [cell.textLabel setText:@"Anser 2 "];
    }
    else if (indexPath.section==1 &&indexPath.row == 2)
    {
        [cell.textLabel setText:@"Answer 3"];
    }
    
    if([self.checkedIndexPath isEqual:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
   
        if (section == 0)
        {
            return @"Question";
        }
        else
        {
            return @"Answer";
        }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

       
            return 50.0f;
    
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    if (s ==0)
    {
        
       GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Question"];
        return sectionHeader;
    }
    else {
       GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:@"Answer"];
        return sectionHeader;    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    if(self.checkedIndexPath)
    {
        UITableViewCell* uncheckCell = [self.tableViewController
                                        cellForRowAtIndexPath:self.checkedIndexPath];
        uncheckCell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell* cell = [self.tableViewController cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    self.checkedIndexPath = indexPath;
    
    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
