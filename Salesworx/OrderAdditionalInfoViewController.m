//
//  OrderAdditionalInfoViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "OrderAdditionalInfoViewController.h"
#import "ReturnTypeViewController.h"
#import "AlSeerSalesOrderPreviewViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SignatureViewController.h"

@interface OrderAdditionalInfoViewController ()

@end

@implementation OrderAdditionalInfoViewController
@synthesize performaOrderRef,categoryLbl,descLbl,warehouseDataArray,itemsSalesOrder,glKitView,signVc,form,isSignature;

- (id)initWithOrder:(NSMutableDictionary *)so andCustomer:(NSDictionary *)customer
{
    self = [super init];
    
    if (self) {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        salesOrder=[NSMutableDictionary dictionaryWithDictionary:so];
        
        NSLog(@" check discount percent  %@", [[[salesOrder valueForKey:@"OrderItems"] objectAtIndex:0]valueForKey:@"DiscountPercent"]);
        
        
        customerDict=[NSDictionary dictionaryWithDictionary:customer];
        form=[NSMutableDictionary dictionary];
        [form setObject:[NSDate date] forKey:@"ship_date"];
        
        customerHeaderView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customerDict ];
        [customerHeaderView setShouldHaveMargins:YES];
        [self.view addSubview:customerHeaderView];
        AppControl *appControl = [AppControl retrieveSingleton];
        ENABLE_ORDER_CONSOLIDATION = appControl.ENABLE_ORDER_CONSOLIDATION;
        ALLOW_SKIP_CONSOLIDATION = appControl.ALLOW_SKIP_CONSOLIDATION;
        checkWholesaleOrder = appControl.ENABLE_WHOLESALE_ORDER;
        if([checkWholesaleOrder isEqualToString:@"Y"])//ENABLE_WHOLESALE_ORDER
        {
            isWholeSalesOrder=YES;
        }
        else
        {
            isWholeSalesOrder = NO;
        }
        if([ENABLE_ORDER_CONSOLIDATION isEqualToString:@"Y"])//
        {
            isEOConsolidation=YES;
        }
        else
        {
            isEOConsolidation = NO;
        }
        if([ALLOW_SKIP_CONSOLIDATION isEqualToString:@"Y"])//
        {
            isASConsolidation=YES;
        }
        else
        {
            isASConsolidation = NO;
        }
        
        
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 60, 1014, 570) style:UITableViewStyleGrouped];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor whiteColor];
        if (@available(iOS 15.0, *)) {
            tableView.sectionHeaderTopPadding = 0;
        }
        [self.view addSubview:tableView];
        
        signatureBaseView =[[PPSSignatureView alloc] initWithFrame:CGRectMake(0, 10, 1024, 768)] ;
        signatureBaseView.alpha=0.0;
        signatureBaseView.backgroundColor=[UIColor redColor];
      //  [self.view addSubview:signatureBaseView];
        
        self.glKitView=[[PPSSignatureView alloc]init];
        self.glKitView.frame= CGRectMake(0, 10, self.view.frame.size.width-290, 350);
        self.glKitView.backgroundColor=[UIColor yellowColor];
        isSignature = NO;
        
        saveButton = [[UIButton alloc] init] ;
        [saveButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [saveButton setFrame:CGRectMake(tableView.frame.size.width-250,tableView.frame.size.height +80, 120, 42)];
        [saveButton addTarget:self action:@selector(saveOrder) forControlEvents:UIControlEventTouchUpInside];
        [saveButton bringSubviewToFront:self.view];
        [saveButton setTitle:NSLocalizedString(@"Close Order", nil) forState:UIControlStateNormal];
        saveButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
        [self.view addSubview:saveButton];
        
        saveAsTemplateButton = [[UIButton alloc] init] ;
        [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [saveAsTemplateButton setFrame:CGRectMake(tableView.frame.size.width-120,tableView.frame.size.height +80, 120, 42)];
        [saveAsTemplateButton addTarget:self action:@selector(confirmOrder) forControlEvents:UIControlEventTouchUpInside];
        [saveAsTemplateButton bringSubviewToFront:self.view];
        [saveAsTemplateButton setTitle:NSLocalizedString(@"Confirm Order", nil) forState:UIControlStateNormal];
        saveAsTemplateButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
        [self.view addSubview:saveAsTemplateButton];

    }
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    return self;
}

+ (CGRect)iOS7StyleScreenBounds {
    CGRect bounds = [UIScreen mainScreen].bounds;
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        bounds.size = CGSizeMake(bounds.size.height, bounds.size.width);
    }
    return bounds;
}


-(void)viewWillAppear:(BOOL)animated
{
    
    NSLog(@"check form for signature %@", [form description]);
    
    
    UIBarButtonItem *leftButtonClicked=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(presentCustomPreviewViewController)];
    
    [self.navigationItem setRightBarButtonItem:leftButtonClicked];
    
}

-(void)presentCustomPreviewViewController
{
    AlSeerSalesOrderPreviewViewController* previewVC=[[AlSeerSalesOrderPreviewViewController alloc]init];
    previewVC.salesOrderDict=salesOrder;
    previewVC.delegate=self;
    [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
}

-(void)dismissPreview
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"ware hopuse data reached destination safely %@", [warehouseDataArray description]);
    
    UILabel *warehouseLbl=[[UILabel alloc]initWithFrame:CGRectMake(350, 10, 100, 30)];
    UILabel *wareHouseDesc=[[UILabel alloc]initWithFrame:CGRectMake(350, 30, 100, 30)];
    warehouseLbl.backgroundColor=[UIColor clearColor];
    int selectedIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"SelectedIndex"];
    
    NSLog(@"selected Index %d", selectedIndex);
    NSLog(@"ware house data in sales order addl vc %@", [warehouseDataArray description]);
    NSLog(@"ware house data array count is %@", [[warehouseDataArray objectAtIndex:selectedIndex]valueForKey:@"Description"]);
    NSLog(@"items from sales order %@", [itemsSalesOrder description]);
    
    
    [self.view addSubview:wareHouseDesc];
    [self.view addSubview:warehouseLbl];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==40)
    {
        Singleton *single = [Singleton retrieveSingleton];
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            BOOL duesScreen=single.isDueScreen;
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            
            if (duesScreen==YES) {
                NSLog(@"coming from dues");
                
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
            }
            else
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
            }
        }
        else
        {
            NSLog(@"check view controllers on the stack %@", [self.navigationController.viewControllers description]);
            
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            if(single.isDueScreen)
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-5)] animated:YES];
            }
            else {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
            }
        }
    }
}
-(void)saveOrder
{
   Singleton *single = [Singleton retrieveSingleton];

    if([single.visitParentView isEqualToString:@"MO"])
    {
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
    }
    else
    {
        NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
        NSLog(@"check view controllers on the stack %@", [self.navigationController.viewControllers description]);
        
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
    }
}

- (NSString*) dbGetImagesDocumentPath
{
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

-(void)confirmOrder
{
    [self.view endEditing:YES];

    NSData* savedImageData=[SWDefaults fetchSignature];
    
    UIImage *image;
    
    if (savedImageData) {
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        image = [UIImage imageWithData:savedImageData scale:screenScale];
        [form setObject:savedImageData forKey:@"signature"];
    }
    
    if([self validateInput])
    {
        info=form;
        [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:performaOrderRef];
        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrder objectForKey:@"info"]] ;
        [infoOrder setValue:info forKey:@"extraInfo"];
        [infoOrder setValue:@"I" forKey:@"order_Status"];
        
        [salesOrder setValue:infoOrder forKey:@"info"];
        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveOrderWithCustomerInfo:customerDict andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        Singleton *single = [Singleton retrieveSingleton];
        
        if (wholeSaleString || wholeSaleString.length != 0)
        {
            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"WO" andAttValue:[wholeSaleString substringToIndex:1] andCust_Att_5:@""];
        }
        if (consolidationString || consolidationString.length != 0)
        {
            [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:orderRef andDocType:@"I" andAttName:@"SC" andAttValue:[consolidationString substringToIndex:1] andCust_Att_5:@""];
        }
        
        [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
        if([single.visitParentView isEqualToString:@"MO"])
        {
            UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Order Created Successfully", nil) andMessage:[NSString stringWithFormat:@"%@: \n %@",NSLocalizedString(@"Reference No", nil),orderRef] andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Order Created Successfully", nil) message:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),orderRef] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            alert.tag = 40;
            [alert show];
        }
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please provide 'Signature' with 'Name'." withController:self];
    }
}

#pragma mark UIView delegates

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(isWholeSalesOrder)
    {
        if (isEOConsolidation && isASConsolidation)
        {
            return 7;
        }
        else
        {
            return 6;
        }
    }
    else {
        if (isEOConsolidation && isASConsolidation)
        {
            return 6;
        }
        else
        {
            return 5;
        }
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableViews viewForHeaderInSection:(NSInteger)section {
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tableView.bounds.size.width text:@""] ;
    
    if (section == 0) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Customer reference", nil)];
    
    } else if (section == 1) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Ship Date", nil)];
    } else if (section == 2) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Comments", nil)];
    } else if (section == 3) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Customer Signature", nil)];
    } else if (section == 4) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Name", nil)];
    }
    else if (section == 5) {
        if(isWholeSalesOrder)
        {
            [sectionHeader.titleLabel setText:NSLocalizedString(@"Wholesale Order", nil)];
        }
        else
        {
            [sectionHeader.titleLabel setText:NSLocalizedString(@"Skip Consolidation", nil)];
        }
    }
    else if (section == 6) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Skip Consolidation", nil)];
    }
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *tableCell = nil;
    if (indexPath.section == 0)
    {
        static NSString *cellIdentifier = @"identifier1";
        SWTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:cellIdentifier] ;
        }
        [cell setKey:@"reference"];
        [cell setDelegate:self];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"reference"]];
        tableCell = cell;
    }
    else if (indexPath.section == 1)
    {
        static NSString *cellIdentifier2 = @"identifier2";
        SWDateFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        if (!cell)
        {
            [SWDefaults setPaymentType:@"CASH"];
            cell = [[SWDateFieldCell alloc] initWithReuseIdentifier:cellIdentifier2] ;
        }
        
        AppControl *appControl = [AppControl retrieveSingleton];
        isShipDateAllow = appControl.ALLOW_SHIP_DATE_CHANGE;
        if([isShipDateAllow isEqualToString:@"Y"])
        {
            cell.userInteractionEnabled=YES;
        }
        else
        {
            cell.userInteractionEnabled=NO;
        }
        [cell setKey:@"ship_date"];
        [cell setDelegate:self];
        tableCell = cell;
    }
    else if (indexPath.section == 2)
    {
        static NSString *cellIdentifier1 = @"identifier1";
        SWTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:cellIdentifier1] ;
        }
        [cell setKey:@"comments"];
        [cell setDelegate:self];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"comments"]];
        tableCell = cell;
    }
    else if (indexPath.section == 3)
    {
        static NSString *cellIdentifier3 = @"identifier3";
        EditableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
        }
        cell.textLabel.text = NSLocalizedString(@"Tap here for Signature", nil);
        cell.textLabel.textColor = [UIColor lightGrayColor];
        if(isSignature)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        tableCell  = cell;
    }
    else if (indexPath.section == 4)
    {
        static NSString *cellIdentifier1 = @"identifier1";
        SWTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:cellIdentifier1] ;
        }
        
        [cell setKey:@"name"];
        [cell setDelegate:self];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"name"]];
        tableCell = cell;
        
    }
    else if (indexPath.section == 5)
    {
        static NSString *cellIdentifier3 = @"identifier3";
        EditableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
        }
        
        if(isWholeSalesOrder)
        {
            cell.textLabel.text = wholeSaleString;
        }
        else
        {
            cell.textLabel.text = consolidationString;
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        tableCell = cell;
    }
    else if (indexPath.section == 6)
    {
        static NSString *cellIdentifier3 = @"identifier3";
        EditableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
        }
        
        cell.textLabel.text = consolidationString;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        tableCell = cell;
    }
    return tableCell;
    
}
- (void)tableView:(UITableView *)tableView3 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==3) {
        
        tableView.scrollEnabled=NO;
        signVc=[[SignatureViewController alloc]init];
        signVc.delegate=self;
        
        [self presentPopupViewController:signVc animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
        [tableView3 deselectRowAtIndexPath:indexPath animated:YES];
    }
    else  if (indexPath.section == 5)
    {
        if(isWholeSalesOrder)
        {
            isWSSelected=YES;
        }
        else
        {
            isWSSelected=NO;
        }
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithGoods];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(goodsCollected:)];
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        UITableViewCell *cell = [tableView3 cellForRowAtIndexPath:indexPath];
        [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else  if (indexPath.section == 6)
    {
        isWSSelected=NO;
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithGoods];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(goodsCollected:)];
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        UITableViewCell *cell = [tableView3 cellForRowAtIndexPath:indexPath];
        [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

-(void)close
{
    [pjrSignatureView removeFromSuperview];
}

- (void)goodsCollected:(NSString *)newType
{
    [collectionTypePopOver dismissPopoverAnimated:YES];
    if(isWSSelected)
    {
        wholeSaleString = newType;
        [form setValue:newType forKey:@"wholesaleOrder"];
        [tableView beginUpdates];
        [self reloadRowAtIndex:0 andSection:5];
        [tableView endUpdates];
    }
    else
    {
        consolidationString = newType;
        [form setValue:newType forKey:@"orderConsolidation"];
        [tableView beginUpdates];
        if(isWholeSalesOrder)
        {
            [self reloadRowAtIndex:0 andSection:6];
        }
        else {
            [self reloadRowAtIndex:0 andSection:5];
        }
        [tableView endUpdates];
    }
}

#pragma mark PPSIgnature methods

-(void)closeButtonTapped
{
    [signVc.view removeFromSuperview];
}

- (void)hideKeyboard
{
    int sections = [tableView numberOfSections];
    for (int i = 0; i < sections; i++) {
        for (int j = 0; j < [tableView numberOfRowsInSection:i]; j++)
        {
            [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]] resignResponder];
        }
    }
}
#pragma mark EditCell Delegate
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"name"]) {
        [self reloadRowAtIndex:0 andSection:3];
    }
    [form setObject:value forKey:key];
}

- (void)scrollTableViewToTextField:(NSIndexPath *)indexPath {
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)editableCellDidStartUpdate:(EditableCell *)cell
{
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];

    // now perform the scroll after the delay, and let the keyboardwill show animation complete
    [self performSelector:@selector(scrollTableViewToTextField:) withObject:indexPath afterDelay:0.1f];
}

-(void)hideTempView
{
    [UIView beginAnimations:@"Fade-in" context:NULL];
    [UIView setAnimationDuration:0.5];
    signatureBaseView.alpha=0.0;
    [self.view bringSubviewToFront:signatureBaseView];
    [UIView commitAnimations];
    tableView.scrollEnabled=YES;
}

/**
 * Example usage of signatureCancelled:
 * @author Jesse Bunch
 **/
-(void)signatureCancelled:(JBSignatureController *)sender {
	
	// close signature controller
    tableView.scrollEnabled=YES;
    [UIView beginAnimations:@"Fade-in" context:NULL];
    [UIView setAnimationDuration:0.5];
    signatureBaseView.alpha=0.0;
    [self.view bringSubviewToFront:signatureBaseView];
    [UIView commitAnimations];
}

#pragma mark UIKeyboard Notifications

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        [tableView setFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 550)];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        [tableView setFrame:f];
    }];
}

- (BOOL)validateInput
{
    [self hideKeyboard];
    AppControl *appControl = [AppControl retrieveSingleton];

    isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
    if([isSignatureOptional isEqualToString:@"Y"])
    {
        if ([form objectForKey:@"signature"])
        {
            if (![form objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[form objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
        else if ([form objectForKey:@"name"])
        {
            if (![form objectForKey:@"signature"])
            {
                return NO;
            }
            else if ([[form objectForKey:@"signature"] length] < 1)
            {
                return NO;
            }
        }
        return YES;
    }
    else
    {
        if (![form objectForKey:@"name"])
        {
            return NO;
        }
        else if ([[form objectForKey:@"name"] length] < 1)
        {
            return NO;
        }
        if (![form objectForKey:@"signature"])
        {
            return NO;
        }
        else if ([[form objectForKey:@"signature"] length] < 1)
        {
            return NO;
        }
        return YES;
    }
}

- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)viewDidDisappear:(BOOL)animated
{
    NSArray *viewsToRemove = [self.view subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    self.view=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    salesOrder=nil;
    info=nil;
    signatureController.delegate=nil;
    signatureController=nil;
    signatureBaseView=nil;
    form=nil;
    customerDict=nil;
    tableView.delegate=nil;
    tableView.dataSource=nil;
    tableView=nil;
    //single=nil;
    customerHeaderView=nil;
    performaOrderRef=nil;
    signatureSaveImage=nil;
    wholeSaleString=nil;
    isShipDateAllow=nil;
    isSignatureOptional=nil;
    collectionTypePopOver=nil;
    returnTypeString=nil;
    checkWholesaleOrder=nil;

}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    collectionTypePopOver = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
