//
//  LineChartView.h
//  SWPlatform
//
//  Created by msaad on 2/4/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCLineChartView.h"

@interface LineChartView : UIView
@property (nonatomic, strong) PCLineChartView *lineChartView;
@end
