//
//  AlSeerSalesOrderCalculationViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//
#import "SWPlatform.h"
#import "ProductOrderViewController.h"
#import "OrderAdditionalInfoViewController.h"
#import "AlSeerOrderAdditionalInfoViewController.h"

@protocol AlSeerSalesOrderViewControllerDelegate <NSObject>
- (void)selectedProduct:(NSMutableDictionary *)product;
@end

@interface AlSeerSalesOrderCalculationViewController : SWGridViewController <UIPopoverControllerDelegate, GridViewDataSource ,GridViewDelegate >
{
    float totalOrderAmount;
    float totalDiscountAmount;
    BOOL isManageOrder;
    double price ;
    double discountAmt ;
    
    NSMutableDictionary *customer;
    NSDictionary *category;
    UILabel *totalPriceLabel;
    UILabel *totalPriceValue;
    double totalPriceforLbl;
    
    double testPriceforLbl;
    double test1Price;
    
    
    
    
    NSMutableDictionary *selectedOrder;
    NSString *manageOrder;
    NSArray *categoryArray;
    NSString *savedCategoryID;
    NSMutableDictionary *productDict;
    ProductListViewController *productListViewController;
    UINavigationController *navigationControllerS;
    ProductOrderViewController *orderViewController;
    UINavigationController *navigationControllerR;
    OrderAdditionalInfoViewController *orderAdditionalInfoViewController;
    AlSeerOrderAdditionalInfoViewController * alseerOrderAdditionalInfoVC;
    
    NSMutableDictionary *dataItemArray;
    NSMutableDictionary *p;
    NSMutableDictionary *b;
    NSMutableDictionary *f;
    NSMutableArray *arrayTemp;
    NSString *title;
    NSString *amountString;
    
    NSMutableDictionary *deleteRowDict;
    NSMutableArray *tempSalesArray;
    
    NSString *ENABLE_TEMPLATES;
    UIPopoverController *popoverController;
    NSString *name_TEMPLATES;
    UIButton *saveButton;
    UIButton *saveAsTemplateButton;
    UIButton *mslOrderItem,*mslOrderItem1;
    
    UILabel *orderItemCountLabel;

}
@property (unsafe_unretained) id <AlSeerSalesOrderViewControllerDelegate> delegate;


- (void) productAdded:(NSMutableDictionary *)q;
- (id)initWithCustomer:(NSDictionary *)c andCategory:(NSDictionary *)category;
- (id)initWithCustomer:(NSDictionary *)c andOrders:(NSDictionary *)order andManage:(NSString *)manage;
- (id)initWithCustomer:(NSDictionary *)c andTemplate:(NSDictionary *)order andTemplateItem:(NSArray *)orderItems ;
- (void)updateTotal ;
- (void) dbGetSalesOrderServiceDiddbGetConfirmOrderItems:(NSArray *)performaOrder;
- (void) saveOrder:(id)sender;

@property(strong,nonatomic)NSMutableArray * wareHouseDataSalesOrder;

@property(strong,nonatomic)NSMutableArray * wareHouseDataOldSalesArray;

@end
