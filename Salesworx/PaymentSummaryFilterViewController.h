//
//  PaymentSummaryFilterViewController.h
//  SalesWars
//
//  Created by Prasann on 15/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "CustomersListViewController.h"
#define kCustomerNameTextField @"Customer_Name"
#define kFromDateTextField @"From_Date"
#define kToDateTextField @"To_Date"
#define kPaymentTypeTextField @"Payment_Type"

#define kCustomerNameTitle @"Customer Name"
#define kPaymentTypeTitle @"Payment Type"

@protocol PaymentSummaryFilterDelegate <NSObject>

-(void)filteredPaymentSummary:(id)filteredContent;
-(void)filterParametersPaymentSummary:(id)filterParameter;
-(void)paymentSummaryFilterDidReset;

@end

@interface PaymentSummaryFilterViewController : UIViewController<UIPopoverControllerDelegate, UITextFieldDelegate>{
    NSMutableDictionary *filterParametersDict;
    NSString *titleText;
    NSString* selectedTextField;
    NSString* selectedPredicateString;
    CustomersListViewController *currencyTypeViewController;
    UIPopoverController *currencyTypePopOver;
    UIPopoverController *datePickerPopOverController;
    NSArray *paymentTypeArray;

}
@property (strong, nonatomic) IBOutlet MedRepTextField *customerNameTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *toDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *paymentTypeTextField;
@property(strong,nonatomic) NSMutableDictionary *customerDictionary;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) NSString *selectedTextFieldType;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong, nonatomic) NSMutableArray *paymentSummaryArray;
@end

