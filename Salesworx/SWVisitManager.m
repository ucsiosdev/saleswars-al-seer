//
//  SWVisitManager.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

//78FC035B-88FB-44AB-AE7D-D7E2FA936E08

#import <objc/runtime.h>
#import "SWVisitManager.h"
#import "SWDefaults.h"
#import "Singleton.h"
#import "SWVisitOptionsViewController.h"
static SWVisitManager *instance;

@implementation SWVisitManager


@synthesize customer;
@synthesize parentViewController;
@synthesize visitID,routeDictionary;

- (id)init {
    self = [super init];
    
    if (self) {
       // visitStarted = NO;
    }

    return self;
}

- (void)startVisitWithCustomer:(NSDictionary *)c parent:(UIViewController *)parent andChecker:(NSString *)isParent
{
    [self setCustomer:[c mutableCopy]];
    [self setParentViewController:parent];
    Singleton *single = [Singleton retrieveSingleton] ;
    single.currentCustomer = [c mutableCopy] ;
    [SWDefaults setCustomer:[c mutableCopy] ];
    
    [self getVisitServiceDidGetOpenVisit:[[SWDatabaseManager retrieveManager] dbGetOpenVisiteFromCustomerID:[self.customer stringForKey:@"Ship_Customer_ID"] andSiteID:[self.customer stringForKey:@"Ship_Site_Use_ID"]] parent:parent];
    c=nil;
    parent=nil;
    isParent=nil;
}
- (void)getVisitServiceDidGetOpenVisit:(NSArray *)routes parent:(UIViewController *)parent;
{
    Singleton *single = [Singleton retrieveSingleton];
    single.isDistributionChecked = NO;
   
    if([routes count]!=0)
    {
        NSString *endDate = [[routes objectAtIndex:[routes count]-1] stringForKey:@"Visit_End_Date"];
        if ([endDate isEqualToString:@""]) {
            [self continueOpenVisit:[[routes objectAtIndex:[routes count]-1]stringForKey:@"Actual_Visit_ID"] parent:parent];
        }
        else {
            [self startNewVisit:parent];
        }
    }
    else {
        [self startNewVisit:parent];
    }
    routes=nil;
}
- (void)continueOpenVisit:(NSString *)openVisitID parent:(UIViewController *)parent {

    [SWDefaults clearCurrentVisitID];
    [SWDefaults setCurrentVisitID:openVisitID];
    
    Class module = NSClassFromString(@"SWVisitOptionsViewController");
    if (module != nil)
    {
        SWVisitOptionsViewController *viewController = [[SWVisitOptionsViewController alloc] init];
        viewController.customerDictornary = customer;
        [parent.navigationController pushViewController:viewController animated:YES];
        viewController=nil;
        visitStarted = YES;
    }
}
- (void)startNewVisit:(UIViewController *)parent
{
    NSLog(@"visit id before moving to sw visit options %@", [SWDefaults currentVisitID]);
    
    Class module = NSClassFromString(@"SWVisitOptionsViewController");
    if (module != nil) {

        SWVisitOptionsViewController *viewController = [[SWVisitOptionsViewController alloc] init];
        viewController.customerDictornary = customer;
        [parent.navigationController pushViewController:viewController animated:YES];
        visitStarted = YES;
        viewController=nil;
    }
}

- (void)closeVisit {
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isDistributionChecked = NO;
    
    [self setCustomer:nil];
    visitStarted = NO;
    [self setParentViewController:nil];
}

@end
