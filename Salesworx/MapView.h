//
//  MapViewController.h
//  Miller
//
//  Created by kadir pekel on 2/7/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
//#import "RegexKitLite.h"
#import "Place.h"
#import "CSMapAnnotation.h"
#import "FTPBaseViewController.h"
@interface MapView: FTPBaseViewController <MKMapViewDelegate> {

	MKMapView* mapView;
	UIImageView* routeView;
	NSArray* routes;
	UIColor* lineColor;
    NSMutableArray *RouteArray;
    MKCoordinateRegion region;
	MKCoordinateSpan span;

}

@property (nonatomic, strong) UIColor* lineColor;
@property (nonatomic, strong) MKMapView* mapView;
//- (void) showRouteFrom: (Place*) f to:(Place*) t;
- (void) showCurrentLocationAddress:(Place*)currentPlace; 
//- (void)addPolygonView:(Place *)place;
-(void)RemoveIT;

//- (void)DrawPolygonForArry:(NSArray *)LocArray;
@end
