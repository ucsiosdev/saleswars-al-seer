    //
//  CustomerOrderHistoryView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//self.detailScroller.bounds.size.width * 2

#import "CustomerOrderHistoryView.h"
#import "SWDefaults.h"
#import "AlSeerOrderHistoryPopupViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWDefaults.h"
#define landSpace 280
#define portSpace 280
#define labelWidth 200
#define labelHeight 30
#define labelOriginX 100
@implementation CustomerOrderHistoryView

@synthesize orderItemView,isToggled;

@synthesize gridView,testIndexPath,customerHeaderView;

- (id)initWithCustomer:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self)
    {
        
        customer=row;
        self.backgroundColor = [UIColor whiteColor];
        
        gridView=nil;
        
        
        
        CGSize screenSize=[SWDefaults screenSize];
        
        
        BOOL isHeaderHidden= [[NSUserDefaults standardUserDefaults]boolForKey:@"hideOrderHistoryHeader"];
        
        
        if (isHeaderHidden==YES) {
            
            
            customerHeaderView.hidden=YES;
            gridView=[[GridView alloc] initWithFrame:CGRectMake(8, 60, screenSize.width-16, screenSize.height - 136)]  ;
        }
        
        else
        {
            
            gridView=[[GridView alloc] initWithFrame:CGRectMake(8, 94, screenSize.width-16, screenSize.height - 176)]  ;
            
        }
        
        NSLog(@"gridviewframe is %@", NSStringFromCGRect(gridView.frame));
        
        gridView.backgroundColor = [UIColor whiteColor];
        gridView.layer.cornerRadius = 8.0;
        gridView.layer.masksToBounds = YES;
        
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        
        //check this header with app ctrl flag
        
        AppControl* appctrl=[AppControl retrieveSingleton];
        
        if ([appctrl.CUST_DTL_SCREEN isEqualToString:@"DASHBOARD"]) {
            
            customerHeaderView=nil;
            customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(8, 8, 1008, 80) andCustomer:customer  ];
            customerHeaderView.backgroundColor=[UIColor whiteColor];
            customerHeaderView.layer.cornerRadius = 8.0;
            customerHeaderView.layer.masksToBounds = YES;
            
            BOOL isHeaderHidden=[[NSUserDefaults standardUserDefaults]boolForKey:@"hideOrderHistoryHeader"];

            if (isHeaderHidden==YES) {
                customerHeaderView.hidden=YES;
            }
        }
        else
        {
            customerHeaderView=nil;
            customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(8, 8, 1008, 40) andCustomer:customer  ];
            customerHeaderView.backgroundColor=[UIColor whiteColor];
            customerHeaderView.companyNameLabel.textColor=[UIColor whiteColor];
        }
        [customerHeaderView setShouldHaveMargins:YES];
        
        
        [self addSubview:customerHeaderView];
        
        //check this header with app ctrl flag
        [self addSubview:gridView];
        [self getGetOrderHistory:[[SWDatabaseManager retrieveManager] dbGetRecentOrders:[customer objectForKey:@"Customer_No"]]];
        
    }
    
    orderDetailTableView.cellLayoutMarginsFollowReadableWidth = NO;
    gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    if (@available(iOS 15.0, *)) {
        orderDetailTableView.sectionHeaderTopPadding = 0;
        gridView.tableView.sectionHeaderTopPadding = 0;
    }
    
    return self;
}
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}


- (void) loadInvoiceView
{
    //salesSer.delegate=self;

    
    [self getSalesOrderServiceDidGetInvoice:[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:[[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"]]];
}
- (void) loadDetailView
{
    [self performSelector:@selector(loadInvoiceView) withObject:nil afterDelay:1.01];
    [self performSelector:@selector(loadOrders) withObject:nil afterDelay:1.01];

    //Bottom View
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    gridView.backgroundColor = [UIColor redColor];
    
    orderTitle=nil;
    orderTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, 100, 300, 50)]  ;
    orderTitle.font = EditableTextFieldSemiFontOfSize(16.0f);
    orderTitle.backgroundColor = [UIColor clearColor];
    orderTitle.textColor = [UIColor orangeColor];
    orderTitle.text = [[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"];

    orderBottomView=nil;
    orderBottomView = [[UIView alloc] init]  ;
    orderBottomView.frame= CGRectMake(0,440,orderItemView.frame.size.width,250);

    //main view
    orderItemView.backgroundColor = [UIColor whiteColor];
    [self addSubview:orderItemView];
    
    //customer header view
    subCustomerHeaderView = nil;
    subCustomerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(8, 8,screenRect.size.width, 60)  andCustomer:customer]  ;
    subCustomerHeaderView.backgroundColor = [UIColor whiteColor];
    [subCustomerHeaderView setShouldHaveMargins:YES];
    
    //hide and show button
    backButton=nil;
    backButton = [[UIButton alloc] init]  ;
    backButton=[UIButton buttonWithType:UIButtonTypeCustom];
   // NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//    if([language isEqualToString:@"ar"])
//    {
//        [backButton setImage:[UIImage imageNamed:@"order-list-AR.png" cache:NO] forState:UIControlStateNormal];
//    }
//    else
//    {
//        [backButton setImage:[UIImage imageNamed:@"order-list.png" cache:NO] forState:UIControlStateNormal];
//    }
    backButton.frame = CGRectMake(500,20,100,40);

    [backButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
    [backButton setTitle:NSLocalizedString(@"Order List", nil) forState:UIControlStateNormal];
    [backButton setTintColor:[UIColor redColor]];
    [backButton.titleLabel setFont:BoldSemiFontOfSize(14.0f)];
    [backButton addTarget:self action:@selector(itemsActions) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    UIButton* testButton=[[UIButton alloc]initWithFrame:CGRectMake(400, 20, 100, 40)];
    testButton.backgroundColor=[UIColor blackColor];
    [testButton addTarget:self action:@selector(testTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
    //Up buttom
    upButton=nil;
    upButton = [[UIButton alloc] init]  ;
    upButton=[UIButton buttonWithType:UIButtonTypeCustom];
    upButton.backgroundColor = [UIColor clearColor];
    [upButton setImage:[UIImage imageNamed:@"arrow-up.png" cache:NO] forState:UIControlStateNormal];
    [upButton addTarget:self action:@selector(upAction) forControlEvents:UIControlEventTouchUpInside];
  
    //down button
    downButton=nil;
    downButton = [[UIButton alloc] init]  ;
    downButton=[UIButton buttonWithType:UIButtonTypeCustom];
    downButton.backgroundColor = [UIColor clearColor];
    [downButton setImage:[UIImage imageNamed:@"arrow-down.png" cache:NO]forState:UIControlStateNormal];
    [downButton addTarget:self action:@selector(downAction) forControlEvents:UIControlEventTouchUpInside];
    if(orderIndexPath==0)
    {
        upButton.enabled=NO;
    }
    if (orderIndexPath == [orders count]-1) {
        downButton.enabled=NO;
    }
    //Segment controller
    segmentedControl=nil;
    segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:NSLocalizedString(@"Ordered Items", nil), NSLocalizedString(@"Invoiced Items", nil), nil]]  ;
    segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    segmentedControl.selectedSegmentIndex = 0;
    [segmentedControl addTarget:self action:@selector(valueChanged:) forControlEvents: UIControlEventValueChanged];
    segmentedControl.frame = CGRectMake(orderItemView.frame.size.width - 300, 120, 250, 30);
    segmentedControl.tintColor = UIColorFromRGB(0x4790D2);


    
    upButton.frame = CGRectMake(350,110,36,32);
    downButton.frame = CGRectMake(400,110,36,32);
    
    bacgroundBar=nil;
    bacgroundBar = [[UIImageView alloc] init];
    bacgroundBar.image = [UIImage imageNamed:@"background--customer-order-history-information.png" cache:NO];
    bacgroundBar.frame = CGRectMake(0, 0, orderItemView.frame.size.width, 660);

    borderView=nil;
    borderView = [[UIView alloc] initWithFrame:CGRectMake(33,80,self.bounds.size.width-72,578)];

//    borderView.layer.shadowOffset = CGSizeMake(4, 4);
//    borderView.layer.shadowRadius = 3;
//    borderView.layer.shadowOpacity = 0.2;
//    borderView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    borderView.layer.borderWidth = 1.0;
    borderView.backgroundColor = [UIColor whiteColor];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(33, 163, self.bounds.size.width-72, 2)];
    lineView.backgroundColor = [UIColor grayColor];
  
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(33, 437, self.bounds.size.width-72, 2)];
    lineView2.backgroundColor = [UIColor grayColor];
    
 
    //[orderItemView addSubview:bacgroundBar];
    [orderItemView addSubview:borderView];
//    [orderItemView addSubview:lineView];
//    [orderItemView addSubview:lineView2];
    [orderItemView addSubview:orderTitle];
    [orderItemView addSubview:segmentedControl];
    [orderItemView addSubview:downButton];
    [orderItemView addSubview:upButton];
    [self addSubview:backButton];
   // [self addSubview:testButton];
    [orderItemView addSubview:subCustomerHeaderView];


    [self LoadBottomView];
}

-(void)testTapped
{
    NSLog(@"test button tapped");
    
}
- (void)LoadBottomView
{
    orderDetailTableView=nil;
    orderDetailTableView=[[UITableView alloc] initWithFrame: CGRectMake(35,0,orderItemView.bounds.size.width-76,215) style:UITableViewStyleGrouped]  ;
    [orderDetailTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [orderDetailTableView setDelegate:self];
    [orderDetailTableView setDataSource:self];
    orderDetailTableView.backgroundView = nil;
    orderDetailTableView.backgroundColor = [UIColor whiteColor];
    orderDetailTableView.userInteractionEnabled = NO;
    orderDetailTableView.tag = 2;

    orderItemView.backgroundColor = [UIColor whiteColor];

    [orderBottomView addSubview:orderDetailTableView];
    [orderItemView addSubview:orderBottomView ];
}
- (void)valueChanged:(UISegmentedControl *)segment {
   
    if(segment.selectedSegmentIndex == 0 ) 
    {
        [orderInvoiceViewController removeFromParentViewController];
        orderInvoiceViewController.view.hidden=YES;
        orderInvoiceViewController=nil;
        
        orderTitle.text = [[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"];
        orderItemsViewController=nil;
        orderItemsViewController = [[OrderItemsViewController alloc] initWithDocumentReference:[[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"]];
        orderItemsViewController.view.frame = CGRectMake(35,165,orderItemView.bounds.size.width-76,portSpace-8);
   
        [orderDetailTableView reloadData];
        [orderItemView addSubview:orderItemsViewController.view];
    }
    else if(segment.selectedSegmentIndex == 1 )
    {
        [orderItemsViewController removeFromParentViewController];
        orderItemsViewController.view.hidden=YES;
        orderItemsViewController=nil;
        orderTitle.text = [[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"];
        orderInvoiceViewController=nil;
        orderInvoiceViewController = [[OrderInvoiceViewController alloc] initWithDocumentReference:[[orders objectAtIndex:orderIndexPath]  objectForKey:@"Orig_Sys_Document_Ref"]];
        orderInvoiceViewController.view.frame =CGRectMake(35,165,orderItemView.bounds.size.width-76,portSpace-8);
 
        [orderDetailTableView reloadData];
        [orderItemView addSubview:orderInvoiceViewController.view];
    }
}
- (void)itemsActions {

    if(!isToggled)
    {
        orderItemView.alpha = 0.0;
        [UIView beginAnimations:@"Fade-in" context:NULL];
        [UIView setAnimationDuration:0.5];
        orderItemView.alpha = 1.0;
        backButton.alpha=1.0;
        [UIView commitAnimations];
        isToggled = YES;
    }
    else 
    {
        [UIView beginAnimations:@"Fade-in" context:NULL];
        [UIView setAnimationDuration:0.5];
        orderItemView.alpha = 0.0;
        backButton.alpha=0.0;

        [UIView commitAnimations];
        isToggled = NO;
    }
}
- (void)upAction{
    if (orderIndexPath != 0)
    {
        upButton.enabled = YES;
        downButton.enabled = YES;
        orderIndexPath = orderIndexPath-1;
        //salesSer.delegate=self;

        
        [self getSalesOrderServiceDidGetInvoice:[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:[[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"]]];

        if (segmentedControl.selectedSegmentIndex==0)
        {
            segmentedControl.selectedSegmentIndex=0;
            [self valueChanged:segmentedControl];
        }
        else
        {
            segmentedControl.selectedSegmentIndex=1;
            [self valueChanged:segmentedControl];
        }
    }
    if(orderIndexPath==0)
    {
        upButton.enabled=NO;
    }
}
- (void)downAction{
    if (orderIndexPath!= ([orders count]-1))
    {
        orderIndexPath = orderIndexPath+1;
        //salesSer.delegate=self;

        [self getSalesOrderServiceDidGetInvoice:[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:[[orders objectAtIndex:orderIndexPath] objectForKey:@"Orig_Sys_Document_Ref"]]];

        downButton.enabled = YES;
        upButton.enabled = YES;
        if (segmentedControl.selectedSegmentIndex==0)
        {
            segmentedControl.selectedSegmentIndex=0;
            [self valueChanged:segmentedControl];
        }
        else
        {
            segmentedControl.selectedSegmentIndex=1;
            [self valueChanged:segmentedControl];
        }        
    }
    if (orderIndexPath == [orders count]-1)
    {
        downButton.enabled=NO;
    }
}
- (void)moveFrame:(UIView *)view withFrame:(CGRect)frame withDuration:(CFTimeInterval)duration{
    if (duration > 0) {
        [UIView beginAnimations:@"moveViewAnimation" context:nil];
        [UIView setAnimationDuration:duration];
        view.frame = frame;
        [UIView commitAnimations];
    }
    else {
        view.frame = frame;
    }
}
- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)loadOrders {
    
    //customerSer.delegate = self;
}
#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{ 
    return 0; 
} 
- (NSInteger)tableView:(UITableView *)tableViews numberOfRowsInSection:(NSInteger)section {
    if(tableViews.tag==1)
    {
        return ([orders count]+1);
    }
    else {
        return 6;
    }
}





- (CGFloat)tableView:(UITableView *)tableViews heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableViews.tag==1)
    {
        return 50;
    }
    else {
        return 30;
    }
}
    
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];

    if(tv.tag==1)
    {

        
    }
    else {
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
        }

        
        if (indexPath.row==0) {
            [cell.textLabel setText: NSLocalizedString(@"Order Date", nil)];


            [cell.detailTextLabel setText: [[orders objectAtIndex:orderIndexPath]  stringForKey:@"Creation_Date"]];
        }
        else if (indexPath.row==1) {
            [cell.textLabel setText:NSLocalizedString(@"Order Value", nil)];
            [cell.detailTextLabel setText: [[orders objectAtIndex:orderIndexPath]  currencyStringForKey:@"Transaction_Amt"]];


        }
        else if (indexPath.row==2) {
            [cell.textLabel setText:NSLocalizedString(@"Order Status", nil)];
            [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@", [[orders objectAtIndex:orderIndexPath] objectForKey:@"ERP_Status"]]];


        }
        else if (indexPath.row==3) {
            [cell.textLabel setText:NSLocalizedString(@"Invoice Date", nil)];
            [cell.detailTextLabel setText:@"N/A"];


        }
        else if (indexPath.row==4) {
            
           //Singleton *single = [Singleton retrieveSingleton];
            [cell.textLabel setText:NSLocalizedString(@"Invoice Value", nil)];
            [cell.detailTextLabel setText:[ invoinceTotal currencyString]];


        }
        else if (indexPath.row==5) {
            [cell.textLabel setText:NSLocalizedString(@"FSR", nil)];
            
            [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@", [[orders objectAtIndex:orderIndexPath] objectForKey:@"FSR"]]];
        }
        [cell.textLabel setFont:LightFontOfSize(14.0f)];
        [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];

    }
    return cell;

}
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
}
#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    
    NSLog(@"orders count in number of rows %d", orders.count);
    
    return orders.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 3;
}



- (float) gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 50.0f;
        case 1:
            return 25.0f;
        case 2:
            return 26.0f;
//        case 3:
//            return 26.0f;
                default:
            break;
    }
    return 10.0f;
}


- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = [NSString stringWithFormat:@"     %@",NSLocalizedString(@"Order No", nil)];
    } else if (column == 1) {
        title = NSLocalizedString(@"Order Date", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Status", nil);
    }
//    else if(column == 3) {
//        title = NSLocalizedString(@"Order Value", nil);
//    }
    
    return title;
   
}


-(NSString*)getDateStr:(NSString *)dateStr
{
    if([dateStr isEqualToString:@""] || dateStr.length<8)
        return @"";
    else
    {
        // Convert string to date object
//        dateStr=[dateStr substringToIndex:8];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        dateStr = [dateFormat stringFromDate:date];
        return dateStr;
    }
}



- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    
    NSDictionary *data = [orders objectAtIndex:row];
    
    NSLog(@"order count in cell for row %d", orders.count);
    
    NSLog(@"check date in dashboard %@", [data description]);
    //removing minutes and seconds from date
    
    
    NSString* dateStr=[data stringForKey:@"Creation_Date"];
    
    NSString* formattedDate=[self getDateStr:dateStr];
    
    
    NSLog(@"test date %@", [self getDateStr:dateStr]);
    
    
    
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Orig_Sys_Document_Ref"]];
    } else if (column == 1) {
        text = [NSString stringWithFormat:@"%@", formattedDate];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"ERP_Status"]];
    } else if (column == 3) {
        text = [NSString stringWithFormat:@"%@", [data currencyStringForKey:@"Transaction_Amt"]];
    }
    
    return text;
}

- (float)gridView:(GridView *)gridView 
                 :(int)columnIndex {
    if (columnIndex == 0) {
        return 30;
    } else if (columnIndex == 1) {
        return 30;
    } else if (columnIndex == 2) {
        return 30;
    }
    else if (columnIndex == 3) {
        return 30;
    }
    else
    {
        return 30;
    }
  
    
    
}

- (UIViewController *)viewController {
    if ([self.nextResponder isKindOfClass:UIViewController.class])
        return (UIViewController *)self.nextResponder;
    else
        return nil;
}


- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{
    

    
    NSDictionary *data = [orders objectAtIndex:rowIndex];
    
    NSLog(@"order count in did select  row %@", [data description]);
    
    
    UIViewController *viewController = [[UIViewController alloc]init];
    viewController.view.frame=CGRectMake(0, 8, 1008, 668);
    
    viewController.view = self;
    
    AlSeerOrderHistoryPopupViewController* salesHistoryPopUp=[[AlSeerOrderHistoryPopupViewController alloc]init];
    
   salesHistoryPopUp.preferredContentSize = CGSizeMake(894.0, 600.0);

    salesHistoryPopUp.modalPresentationStyle = UIModalPresentationFormSheet;

    salesHistoryPopUp.salesOrderArray=[orders mutableCopy];
    
    salesHistoryPopUp.selectedIndex=rowIndex;
    
    
    salesHistoryPopUp.salesOrderNumber=[NSString stringWithFormat:@"%@",[data valueForKey:@"Orig_Sys_Document_Ref"]];
    
    
    [viewController presentViewController:salesHistoryPopUp animated:YES completion:nil];
    
    
    
   // [gridView reloadData];
    
    
    /* restricting this for now because screen needs to be redesigned for Al Seer customer dashboard
    orderItemView=nil;
    orderItemView = [[UIView alloc] initWithFrame:self.bounds];
    [orderItemView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    orderItemView.alpha = 0.0;
    orderIndexPath = rowIndex;

    NSLog(@"check orders here %@,", [orders description]);
    
    
    [self loadDetailView];
    [self itemsActions];
    [self valueChanged:0];
     
     */
}


-(void)loadTestView
{
    
    orderItemView=nil;
    orderItemView = [[UIView alloc] initWithFrame:self.bounds];
    [orderItemView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    orderItemView.alpha = 0.0;
    orderIndexPath = testIndexPath;
    
    [self loadDetailView];
    [self itemsActions];
    
    [self valueChanged:0];

}

#pragma mark SWCustomerService Delegate
//- (void)customerServiceDidGetOrderHistory:(NSArray *)orderList {
- (void)getGetOrderHistory:(NSArray *)orderList
{
    orders= [NSArray arrayWithArray:orderList];
    NSLog(@"orders count in order history view %@", [orders description]);
    
    
    [gridView reloadData];
    orderList=nil;
}
#pragma mark Customer Order History Cell Delegate
- (void)customerOrderHistoryCellDidSelectItems:(CustomerOrderHistoryCell *)cell withInvoice:(NSDictionary *)invoice {

}
- (void)customerOrderHistoryCellDidSelectInvoice:(CustomerOrderHistoryCell *)cell withInvoice:(NSDictionary *)invoice {

}

- (void)getSalesOrderServiceDidGetInvoice:(NSArray *)invoice {
    items=[NSArray arrayWithArray:invoice];
    float invoiceTempValue = 0.0;

    if([items count] !=0)
    {
        for (int i =0 ; i <[items count]; i++) {
            @autoreleasepool{
            NSDictionary *data = [items objectAtIndex:i];
            
            float tempfloat = [[data stringForKey:@"Item_Value"] floatValue];
          // Singleton *single = [Singleton retrieveSingleton];
            invoiceTempValue = invoiceTempValue + tempfloat;
            invoinceTotal = [NSString stringWithFormat:@"%f",invoiceTempValue];
        }
        }
    }
    else
    {
        invoinceTotal = [NSString stringWithFormat:@"0"];

    }
  
    [orderDetailTableView reloadData];
   
    invoice=nil;
}

#pragma mark -

//OLA!

- (void)refreshOrderHistory
{
    [self getGetOrderHistory:[[SWDatabaseManager retrieveManager] dbGetRecentOrders:[customer objectForKey:@"Customer_No"]]];
}

@end
