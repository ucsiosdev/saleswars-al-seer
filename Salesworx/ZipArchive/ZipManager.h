//
//  ZipManager.h
//  EasyQuran
//
//  Created by Faizan Ali on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZipManager : NSObject
+(void)zipFile;
+(void)unzipFile;
+(void)unZipUpdatedFile;
+(void)convertSqliteToNSDATA;
+(void)convertNSDataToSQLite;
+(void)ZipUpdatedFile;
+(void)deleteDatabaseFromAppWithName:(NSString *)file;

@end
