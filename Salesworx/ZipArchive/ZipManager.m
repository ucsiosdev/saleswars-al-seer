//
//  ZipManager.m
//  EasyQuran
//
//  Created by Faizan Ali on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZipManager.h"
#import "ZipArchive.h"
//#import <zlib.h>
//#import "NSData+Gzip.h"
#import "ZipFile.h" 
#import "ZipWriteStream.h"
#import "GZIP.h"
#import "NSData+Compression.h"
#define CHUNK 16384

#import "SWDefaults.h"

@implementation ZipManager


+(void)zipFile
{
    
    //iOS 8 support
    NSString *dPath;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        dPath=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        dPath = [paths objectAtIndex:0];
    }

    
//    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString* dPath = [paths objectAtIndex:0];
    NSString* txtfile = [dPath stringByAppendingPathComponent:@"swx.sqlite"];
    NSString* zipfile = [dPath stringByAppendingPathComponent:@"swx.zip"];
    ZipArchive* zip = [[ZipArchive alloc] init];
    BOOL ret = [zip CreateZipFile2:zipfile];
    ret = [zip addFileToZip:txtfile newname:@"swx.sqlite"];//zip
    if( ![zip CloseZipFile2] )
    {
        zipfile = @"";
    }
   // //NSLog(@"The file has been zipped");
}
 
 
+(void)unzipFile
{
    
    NSString* dPath;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        dPath=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        dPath = [paths objectAtIndex:0];

    }
    
    
    
 
    NSString* zipfile = [dPath stringByAppendingPathComponent:@"swx.sqlite.gz"] ;
    NSString* unzipto = [dPath stringByAppendingPathComponent:@"swx1.sqlite"] ;
    ZipArchive* zip = [[ZipArchive alloc] init];
    if([zip UnzipOpenFile:zipfile] )
    {
        BOOL ret = [zip UnzipFileTo:unzipto overWrite:YES];
        if(NO == ret)
        {
        }
        [zip UnzipCloseFile];
    }
   // //NSLog(@"The file has been unzipped");
}



+(void)convertSqliteToNSDATA
{
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"swx.sqlite"];
    NSData *sqliteData = [[NSData alloc] initWithContentsOfFile:txtPath];
//    NSData *compressedData = [sqliteData gzippedData];
    NSData *compressedData = [sqliteData gzipDeflate];

    NSString *fileName = [NSString stringWithFormat:@"%@/swx.sqlite.gz",documentsDirectory];
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToFileOffset: 0];
	[fileHandler writeData:compressedData];
    [fileHandler closeFile];
}

+(void)convertNSDataToSQLite
{
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"swx.sqlite.gz"];
    NSData *sqliteData = [[NSData alloc] initWithContentsOfFile:txtPath];
    NSData *compressedData = [sqliteData gzipInflate];
    NSString *fileName = [NSString stringWithFormat:@"%@/swx.sqlite",documentsDirectory];
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToFileOffset: 0];
	[fileHandler writeData:compressedData];
    [fileHandler closeFile];
}






//////////////////////////.....................FOR ZIP FILE.....................//////////////////////////
+(void)ZipUpdatedFile
{
    /*
    ///////Copy Start
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *errorswx;
    NSArray *pathswx = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryswx = [pathswx objectAtIndex:0];
    
    NSString *txtPath = [documentsDirectoryswx stringByAppendingPathComponent:@"swx.sqlite"];
    NSString *txtPatha = [documentsDirectoryswx stringByAppendingPathComponent:@"swxCopy.sqlite"];
    [fileManager copyItemAtPath:txtPath toPath:txtPatha error:&errorswx];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    NSString *ZipLibrary = [paths objectAtIndex:0];
    NSString *fullPathToFile = [ZipLibrary stringByAppendingPathComponent:@"swx.sqlite.gz"];
    ZipFile *zipFile = [[ZipFile alloc]initWithFileName:fullPathToFile mode:ZipFileModeCreate];
    
    NSError *error = nil;
    NSArray *paths1 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory , NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths1 objectAtIndex:0];
    NSArray *files = [[NSFileManager defaultManager]contentsOfDirectoryAtPath:documentsDirectory error:&error];
    for(int i = 0;i<files.count;i++)
    {
        id myArrayElement = [files  objectAtIndex:i];
        if([myArrayElement rangeOfString:@"swx.sqlite" ].location !=NSNotFound)
        {
            NSString *path = [documentsDirectory stringByAppendingPathComponent:myArrayElement];
            NSDictionary *attributes = [[NSFileManager defaultManager]attributesOfItemAtPath:path error:&error];
            NSDate *Date = [attributes objectForKey:NSFileCreationDate];
            
            ZipWriteStream *streem = [zipFile writeFileInZipWithName:myArrayElement fileDate:Date compressionLevel:ZipCompressionLevelBest];
            NSData *data = [NSData dataWithContentsOfFile:path];
            
            [streem writeData:data];
            [streem finishedWriting];
        }
    }
    [zipFile close];
     */
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
//    
//    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [path objectAtIndex:0];
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"swx.sqlite"];
    NSData *data = [NSData dataWithContentsOfFile:txtPath options:NSDataReadingUncached error:nil];
    [data gzippedData];
    
}

//////////////////////////.....................FOR UNZIP FILE.....................//////////////////////////
+ (void)unZipUpdatedFile
{
//    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString* dPath = [paths objectAtIndex:0];

    
    
    //iOS 8 support
    NSString *dPath;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        dPath=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        dPath = [paths objectAtIndex:0];
    }
    
    
    
    NSString* zippedDBPath = [dPath stringByAppendingPathComponent:@"swx.sqlite.gz"] ;
    NSString *unzippedDBPath = [dPath stringByAppendingPathComponent:@"swx.sqlite"];
    
    gzFile file = gzopen([zippedDBPath UTF8String], "rb");
    FILE *dest = fopen([unzippedDBPath UTF8String], "w");
    
    unsigned char buffer[CHUNK];
    int uncompressedLength;
    while ((uncompressedLength = gzread(file, buffer, CHUNK)) )
    {
        // got data out of our file
        if(fwrite(buffer, 1, uncompressedLength, dest) != uncompressedLength || ferror(dest)) {
            // //NSLog(@"error writing data");
        }
    }
    fclose(dest);
    gzclose(file);
}


+ (void)deleteDatabaseFromAppWithName:(NSString *)file {
}

@end
