//
//  SWCollectionViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/20/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWCollectionViewController.h"
#import "CustomersListViewController.h"
#import "StatementViewController.h"
#import "CollectionTypeViewController.h"
#import "CurrencyViewController.h"
#import "SWVisitOptionsViewController.h"
#import "SWAppDelegate.h"

#define kKeyAmout   @"Amount"
#define kKeyChqDate @"ChqDate"
#define kKeyChqNo   @"ChqNumber"
#define kKeyBank    @"Bank"
#define kKeyBranch  @"Branch"

@interface SWCollectionViewController ()
- (BOOL)validateInput;
@end

@implementation SWCollectionViewController

- (id)init {
    self = [super init];
    if (self) {
        
        [self setTitle:NSLocalizedString(@"Take Collection", nil)];
        
        invoices=[NSArray array];
        
        collectionType=@"CURRENT CHEQUE";
        [SWDefaults setPaymentType:@"CHEQUE"];
        form=[NSMutableDictionary dictionary];
        [form setObject:collectionType forKey:@"CollectionType"];

        shouldPresentCustomerList = YES;
        [self startUpView];
    }
    return self;
}

- (id)initWithCustomer:(NSDictionary *)c {
    self = [self init];
    if (self) {
        customer=c;
        shouldPresentCustomerList = NO;
        [self startUpView];
    }
    return self;
}

-(void)startUpView
{
    appControl = [AppControl retrieveSingleton];
    noCustomerImageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"collections_placeholder.png"cache:NO]];
    [noCustomerImageView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    CGRect frame = CGRectMake((self.view.bounds.size.width / 2) - (262 / 2), 200, 262, 194);
    [noCustomerImageView setFrame:frame];
    
    selectCustomerButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [selectCustomerButton setTitle:NSLocalizedString(@"Select Customer", nil) forState:UIControlStateNormal];
    [selectCustomerButton addTarget:self action:@selector(openCustomers:) forControlEvents:UIControlEventTouchUpInside];
    [selectCustomerButton setFrame:CGRectMake((self.view.bounds.size.width / 2) - (175 / 2), 462, 175, 35)];
    
    
    tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor whiteColor];
    if (@available(iOS 15.0, *)) {
        tableView.sectionHeaderTopPadding = 0;
    }
    [self.view addSubview:tableView];
    
    // popover
    CollectionTypeViewController *collectionTypeViewController = [[CollectionTypeViewController alloc] init] ;
    [collectionTypeViewController setTarget:self];
    [collectionTypeViewController setAction:@selector(collectionTypeChanged:)];
    collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
    collectionTypePopOver.delegate=self;
    
    [self.view addSubview:noCustomerImageView];
    [self.view addSubview:selectCustomerButton];
    
    if (!customer) {
        [self openCustomers:nil];
    }
    
    customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 60) andCustomer:customer] ;
    [customerHeaderView setShouldHaveMargins:YES];
    [tableView setTableHeaderView:customerHeaderView];
    
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in collection is %@", [SWDefaults currentVisitID]);
    }
    NSLog(@"visit id in  defaults at collection %@", currentVisitID);
    
    currencyTypeViewController=nil;
    currencyTypePopOver=nil;
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES animated:YES];
    if (customer)
    {
        [Flurry logEvent:@"Collection  View"];

        [selectCustomerButton removeFromSuperview];
        [noCustomerImageView removeFromSuperview];
        [selectCustomerButton setHidden:YES];
        [noCustomerImageView setHidden:YES];
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Statement", nil) style:UIBarButtonItemStylePlain target:self action:@selector(statement:)]  animated:YES];
    }
    else {
        [selectCustomerButton setHidden:NO];
        [noCustomerImageView setHidden:NO];
    }
    
    [form setObject:[NSDate date] forKey:kKeyChqDate];
    [form setValue:[[SWDefaults userProfile] stringForKey:@"Currency_Code"] forKey:@"selectedCurrency"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillShow:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillHide:) 
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // back button was pressed.  We know this is true because self is no longer
        // in the navigation stack.
        
        [self closeVisitForCollection];
        NSLog(@"back button pressed in view will dis appear");
    }
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}


- (void)startOver:(UIBarButtonItem *)sender {
    [self openCustomers:nil];
}

- (void)customerSelected:(id)sender {
    customer=sender;
    
    collectionType=@"CURRENT CHEQUE";
    form=[NSMutableDictionary dictionary];
    [form setObject:collectionType forKey:@"CollectionType"];
    [SWDefaults setPaymentType:@"CHEQUE"];

    [tableView reloadData];
    
    // navigation bar buttons
    UIBarButtonItem *statementButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Statement", nil) style:UIBarButtonItemStylePlain target:self action:@selector(statement:)] ;
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(startOver:)] ;
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:statementButton, cancelButton, nil] animated:YES];
}

- (void)openCustomers:(id)sender {
    CustomersListViewController *customerListViewController = [[CustomersListViewController alloc] init] ;
    
    [customerListViewController setTarget:self];
    [customerListViewController setAction:@selector(customerSelected:)];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:customerListViewController] ;
    
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (void)statement:(id)sender {
    StatementViewController *statementViewController = [[StatementViewController alloc] initWithCustomer:customer] ;
    [self.navigationController pushViewController:statementViewController animated:YES];
}

- (void)currencyTypeChanged:(NSDictionary *)newType
{
    [currencyTypePopOver dismissPopoverAnimated:YES];
    double convertAmount = [[newType stringForKey:@"AString"] doubleValue]*[[newType stringForKey:@"CRate"] doubleValue];
    [form setValue:[NSString stringWithFormat:@"%.2f",convertAmount] forKey:kKeyAmout];
    [form setValue:[newType stringForKey:@"AString"] forKey:@"selectedAmount"];
    [form setValue:[newType stringForKey:@"CCode"] forKey:@"selectedCurrency"];
    [form setValue:[newType stringForKey:@"CRate"] forKey:@"currencyRate"];
    NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:1];
    NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
    [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)collectionTypeChanged:(NSString *)newType {
    [collectionTypePopOver dismissPopoverAnimated:YES];
    
    NSString *previousType = collectionType;
    
    collectionType=newType;
    [form setObject:collectionType forKey:@"CollectionType"];
    
    NSIndexPath *durPath = [NSIndexPath indexPathForRow:0 inSection:0];
    NSArray *paths = [NSArray arrayWithObject:durPath];
    
    
    [tableView beginUpdates];
    [tableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationAutomatic];
    
    NSIndexSet *removeIndexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(2, 4)];
    
    
    if ([collectionType isEqualToString:@"CASH"] && ![collectionType isEqualToString:previousType]) {
        [tableView deleteSections:removeIndexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (![collectionType isEqualToString:previousType] && [previousType isEqualToString:@"CASH"]) {
        [tableView insertSections:removeIndexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    if([collectionType isEqualToString:@"CURRENT CHEQUE"])
    {
        [SWDefaults setPaymentType:@"CHEQUE"];
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:2];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    else if([collectionType isEqualToString:@"PDC"])
    {
        [SWDefaults setPaymentType:@"PDC"];
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:0 inSection:2];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [tableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    }
    else {
        [SWDefaults setPaymentType:@"CASH"];
    }
    [tableView endUpdates];
}

- (BOOL)validateInput {
    [self hideKeyboard];
    if ([collectionType isEqual: @"CASH"]) {
        if (![form objectForKey:kKeyAmout]) {
            return NO;
        } else if ([[form objectForKey:kKeyAmout] length] < 1) {
            return NO;
        }
    } else {
        if (![form objectForKey:kKeyAmout]) {
            return NO;
        } else if ([[form objectForKey:kKeyAmout] length] < 1) {
            return NO;
        }
        
        if (![form objectForKey:kKeyChqNo]) {
            return NO;
        } else if ([[form objectForKey:kKeyChqNo] length] < 1) {
            return NO;
        }
        
        if (![form objectForKey:kKeyBank]) {
            return NO;
        } else if ([[form objectForKey:kKeyBank] length] < 1) {
            return NO;
        }
        
        if (![form objectForKey:kKeyBranch]) {
            return NO;
        } else if ([[form objectForKey:kKeyBranch] length] < 1) {
            return NO;
        }
    }
    
    return YES;
}

- (void)scrollTableViewToTextField:(NSIndexPath *)indexPath {
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)hideKeyboard {
    int sections = [tableView numberOfSections];
    for (int i = 0; i < sections; i++) {
        for (int j = 0; j < [tableView numberOfRowsInSection:i]; j++) {
            [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]] resignResponder];
        }
    }
}

- (void)collectionDone {
    
    // we come to collections view from customers
    // so no need to present the customer's list for selection
    if (!shouldPresentCustomerList) {
        collectionType=@"CURRENT CHEQUE";
        form=[NSMutableDictionary dictionary];
        [form setObject:collectionType forKey:@"CollectionType"];
        
        [tableView reloadData];
        return;
    }
    
    customer=nil;
    form=nil;
    
    [self performSelector:@selector(startOver:) withObject:nil afterDelay:0.4f];
}

#pragma mark UIKeyboard Notifications

- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        [tableView setFrame:self.view.bounds];
    }];    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        [tableView setFrame:f];
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (customer == nil) {
        return 0;
    }
    
    if ([collectionType isEqualToString:@"CASH"]) {
        return 3;
    }
    
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if (section == 6) {
        return @"Select invoices to settle against the collection";
    }
    
    return @"";
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)s {
    if (s == 6) {
        GroupSectionFooterView *footerView = [[GroupSectionFooterView alloc] initWithWidth:tv.bounds.size.width text:@"Select invoices to settle against the collection"] ;
        return footerView;    }
    
    else if(s==1)
    {
        if ([appControl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
        {
            GroupSectionFooterView *footerView = [[GroupSectionFooterView alloc] initWithWidth:tv.bounds.size.width text:[NSString stringWithFormat:@"Collected Currency : %@            Collected Amount : %@",[form stringForKey:@"selectedCurrency"],[self currencySelectedString:[form stringForKey:@"selectedAmount"]]]] ;
            footerView.titleLabel.textAlignment = NSTextAlignmentRight;
            return footerView;
        }
        else
        {
            return nil;
        }
    }
    else
    {
        return nil;
    }

}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    NSString *title = @"";
    
    if (section == 0) {
        title =  NSLocalizedString(@"Type", nil);
    } else if (section == 1) {
        title =  NSLocalizedString(@"Amount", nil);
    } else if (section == 2) {
        title =  NSLocalizedString(@"Cheque Date", nil);
    } else if (section == 3) {  
        title =  NSLocalizedString(@"Cheque Number", nil);
    } else if (section == 4) {
        title =  NSLocalizedString(@"Bank", nil);
    } else if (section == 5) {
        title =  NSLocalizedString(@"Branch", nil);
    } else if (section == 6) {
        title =  NSLocalizedString(@"Invoices", nil);
    }
    
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:title] ;
    return sectionHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section==1)
    {
        if ([appControl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
        {
            return 35;
        }
    }

    else if (section != 6) {
        return 0;
    }
        return 30;
}


//try 1
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *IDENT = @"CollectionCellIdent";
    EditableCell *cell = nil;//[tv dequeueReusableCellWithIdentifier:IDENT];
    if (indexPath.section == 0)
    {
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENT] ;
        }
        
        
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell.textLabel setText:collectionType];

        [cell setDelegate:self];
    }
    else if (indexPath.section == 1)
    {
        if ([appControl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
        {
            if (!cell)
            {
                cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENT] ;
            }
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            [cell.textLabel setText:[[form stringForKey:kKeyAmout] currencyString]];
            [cell setDelegate:self];
        }
        else
        {
            if (!cell) {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
            }
            
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            ((SWTextFieldCell *)cell).textField.returnKeyType = UIReturnKeyNext;

            [cell setKey:kKeyAmout];
            [cell setDelegate:self];
            
            if ([form objectForKey:kKeyAmout]) {
                [((SWTextFieldCell *)cell).textField setText:[form objectForKey:kKeyAmout]];
            }
        }

    }
    else if (indexPath.section == 2) {
        if (!cell) {
            cell = [[SWDateFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        }
        
        [cell setKey:kKeyChqDate];
        [cell setDelegate:self];
        
        if ([form objectForKey:kKeyChqDate]) {
            [((SWDateFieldCell *)cell) setDate:[form objectForKey:kKeyChqDate]];
        }

    } else if (indexPath.section == 3) {
        if (!cell) {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        }
        
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
        ((SWTextFieldCell *)cell).textField.returnKeyType = UIReturnKeyNext;
        [cell setKey:kKeyChqNo];
        [cell setDelegate:self];
        
        if ([form objectForKey:kKeyChqNo]) {
            [((SWTextFieldCell *)cell).textField setText:[form objectForKey:kKeyChqNo]];
        }
    } else if (indexPath.section == 4) {
        if (!cell) {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        }
        ((SWTextFieldCell *)cell).textField.returnKeyType = UIReturnKeyNext;
        [cell setKey:kKeyBank];
        [cell setDelegate:self];
        
        if ([form objectForKey:kKeyBank]) {
            [((SWTextFieldCell *)cell).textField setText:[form objectForKey:kKeyBank]];
        }
    } else if (indexPath.section == 5) {
        if (!cell) {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        }
        ((SWTextFieldCell *)cell).textField.returnKeyType = UIReturnKeyDone;
        [cell setKey:kKeyBranch];
        [cell setDelegate:self];
        if ([form objectForKey:kKeyBranch]) {
            [((SWTextFieldCell *)cell).textField setText:[form objectForKey:kKeyBranch]];
        }
    } else if (indexPath.section == 6) {
        cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENT] ;
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell.textLabel setText:NSLocalizedString(@"Select Invoices", nil)];
    } else {
        cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:IDENT] ;
    }
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
        [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    }
    else if (indexPath.section == 1) {
        if ([appControl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
        {
            if (!currencyTypeViewController) {
                currencyTypeViewController = [[CurrencyViewController alloc] init] ;
                [currencyTypeViewController setTarget:self];
                [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
                [currencyTypeViewController setPreferredContentSize:CGSizeMake(300,300)];
            }
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
            currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
            currencyTypePopOver.delegate=self;
            [currencyTypePopOver presentPopoverFromRect:CGRectMake(600, 190, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
        }
    }
    else if (indexPath.section == 6 || [collectionType isEqualToString:@"CASH"]) {
        
        if (![collectionType isEqualToString:@"CASH"]  )
        {
            if ([[form allKeys] containsObject:kKeyChqDate] )
            {
                if ([self validateInput])
                {
                    StatementViewController *statementViewController = [[StatementViewController alloc] initWithCustomer:customer andCollectionInfo:form] ;
                    [statementViewController setTarget:self];
                    [statementViewController setAction:@selector(collectionDone)];
                    [self.navigationController pushViewController:statementViewController animated:YES];
                } else {
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"All fields must be filled" withController:self];
                }
            }
            else {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Select Date", nil) withController:self];
            }
        }
        else
        {
            if ([self validateInput])
            {
                StatementViewController *statementViewController = [[StatementViewController alloc] initWithCustomer:customer andCollectionInfo:form] ;
                [statementViewController setTarget:self];
                [statementViewController setAction:@selector(collectionDone)];
                [self.navigationController pushViewController:statementViewController animated:YES];
            } else {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"All fields must be filled" withController:self];
            }
        }
    }
}

#pragma mark EditCell Delegate      

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key
{
    [form setObject:value forKey:key];
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    
    if (indexPath.section==3) {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:4] ;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:myIP];
        
        [((SWTextFieldCell *)cell).textField becomeFirstResponder];
    }
    else if (indexPath.section==4) {
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:5] ;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:myIP];
        
        [((SWTextFieldCell *)cell).textField becomeFirstResponder];
    }
}

- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    // now perform the scroll after the delay, and let the keyboardwill show animation complete
    [self performSelector:@selector(scrollTableViewToTextField:) withObject:indexPath afterDelay:0.1f];
}

- (NSString *)currencySelectedString:(NSString *)amount {
    NSScanner *scanner = [NSScanner scannerWithString:amount];
    
    double value = 0.2f;
    BOOL returnValue  = [scanner scanDouble:&value];
    
    if (returnValue) {
        return [self currencySelectStringWithDouble:value];
    } else {
        return [self currencySelectStringWithDouble:0.0f];
    }
}
- (NSString *)currencySelectStringWithDouble:(double)value {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setCurrencyCode:[form stringForKey:@"selectedCurrency"]];
    
    return [formatter stringFromNumber:[NSNumber numberWithDouble:value]];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


#pragma mark Start Visit Methods

-(void)startVisit
{
    Singleton *single = [Singleton retrieveSingleton] ;
    single.currentCustomer = [customer mutableCopy];
    self.customer = single.currentCustomer;
    
    NSLog(@"customer details in Collection %@", [self.customer description]);
    SWAppDelegate *appDelegate = [[SWAppDelegate alloc]init];
    
    [SWDefaults clearCurrentVisitID];
    visitID=[NSString createGuid];
    [SWDefaults setCurrentVisitID:visitID];
    
    if (self.customer==nil) {
        [[SWDatabaseManager retrieveManager] saveVistWithCustomerInfo:[SWDefaults customer] andVisitID:visitID];
        [appDelegate saveLocationData:YES];
    }
    else {
        [SWDefaults setCustomer:self.customer];
        [[SWDatabaseManager retrieveManager] saveVistWithCustomerInfo:self.customer andVisitID:visitID];
        [appDelegate saveLocationData:YES];
    }
}

#pragma mark End Visit Methods
- (void)closeVisitForCollection
{
    if ([SWDefaults currentVisitID]) {
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]]];
        [SWDefaults clearCurrentVisitID];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)getRouteServiceDidGetRoute:(NSArray *)r {
    
    Singleton *single = [Singleton retrieveSingleton];
    self.customer = single.currentCustomer;
    
    if([r count] != 0)
    {
        for (int i=0; i<[r count]; i++)
        {
            if([[self.customer stringForKey:@"Ship_Customer_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Customer_ID"]] && [[self.customer stringForKey:@"Ship_Site_Use_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Site_Use_ID"]])
            {
                [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[[r objectAtIndex:i] stringForKey:@"Planned_Visit_ID"]];
                
                NSString* tempVisitID=[SWDefaults currentVisitID];
                if (tempVisitID) {
                    [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
                }
            }
        }
    }
    if ([self.customer objectForKey:@"Planned_Visit_ID"])
    {
        [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[self.customer stringForKey:@"Planned_Visit_ID"]];
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    else
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    
    NSString* tempVisitID=[SWDefaults currentVisitID];
    if (tempVisitID) {
        
        NSMutableArray *locationDict = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Latitude , Longitude from TBL_FSR_Actual_Visits where Actual_Visit_ID='%@'",[SWDefaults currentVisitID]]];
        if ([[[locationDict objectAtIndex:0]stringForKey:@"Latitude"]isEqualToString:@"0"] || [[[locationDict objectAtIndex:0]stringForKey:@"Longitude"]isEqualToString:@"0"])
        {
            NSString *latitudePoint,*longtitudePoint;
            latitudePoint=@"";
            longtitudePoint = @"";
            
            for (int i= 0; i <=3; i++) {
                locationManager = [[CLLocationManager alloc] init];
                locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                locationManager.distanceFilter = kCLDistanceFilterNone;
                
                [locationManager startUpdatingLocation];
                CLLocation *location = [locationManager location];
                CLLocationCoordinate2D coordinate = [location coordinate];
                latitudePoint = [NSString stringWithFormat:@"%f", coordinate.latitude];
                longtitudePoint = [NSString stringWithFormat:@"%f", coordinate.longitude];
                [locationManager stopUpdatingLocation];
                if ([latitudePoint isEqualToString:@"0"] || [longtitudePoint isEqualToString:@"0"]) {
                }
                else
                {
                    break;
                }
            }
            [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@' Where Actual_Visit_ID='%@'",latitudePoint,longtitudePoint,[SWDefaults currentVisitID]]];
        }
        
        SWVisitManager *visitManager = [[SWVisitManager alloc] init];
        [visitManager closeVisit];
        
        Singleton *single = [Singleton retrieveSingleton];
        if ([single.isCashCustomer isEqualToString:@"Y"]) {
            single.popToCash = @"Y";
        }
        else
        {
            single.popToCash = @"N";
            
        }
        r=nil;
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"went to visit options no activity done");
    }
}


@end
