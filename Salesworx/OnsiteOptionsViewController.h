//
//  OnsiteOptionsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol updateVisitOptionButtons <NSObject>

-(void)selectedVisitOption:(NSString*)visitOption;

@end

@interface OnsiteOptionsViewController : UIViewController
{
    id delegate;
}

- (IBAction)segmentSwitch:(UISegmentedControl *)sender ;

@property (nonatomic, assign) id delegate;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@end
