//
//  SWDatabaseManager.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/7/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "SWSection.h"
#import "Singleton.h"
#import "CustomerShipAddressDetails.h"
#import <CoreLocation/CoreLocation.h>
#import "XMLWriter.h"
#import "SWPlatformDatabaseConstants.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>
#import "SWPlatform.h"

@class xswiViewController;

enum errorCodes {
	kDBNotExists,
	kDBFailAtOpen,
	kDBFailAtCreate,
	kDBErrorQuery,
	kDBFailAtClose
};


@interface SWDatabaseManager : NSObject <CLLocationManagerDelegate> {

//    NSMutableArray *resultsArray;
//    NSMutableDictionary *result;
    //SWSection *sectionItem;
   // NSArray *resultSet;
  //  NSString *columnName;
    BOOL databaseIsOpen;
    //
    
    //Product
    BOOL isFilter;

    //Dashboard
    //NSMutableArray *actualVisitArray;

    //Customer
   // NSMutableArray *sections;
    int currentCharacter;
    BOOL shouldStop;
    
    //Send Order
    NSMutableDictionary *mainDictionary;
    NSMutableDictionary *orderDictionary;
    NSMutableDictionary *itemDictionary ;
    NSMutableDictionary *lotDictionary ;
    NSMutableArray *orderArray1;
    NSMutableArray *itemArray;
    NSMutableArray *lotArray1;
    NSMutableArray *mainArray;
    //       id target;
    SEL action;
   // SWDatabaseManager *dataBase;
    //XMLWriter* xmlWriter;
    
    //Collection
    BOOL isCalled;
    
    //Route
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    
    
    
    ////FOR VISIT MANAGER

}


@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

+ (SWDatabaseManager *)retrieveManager;
+ (void)destroyMySingleton;
- (void)executeNonQuery:(NSString *)sql ;
- (NSMutableArray *)fetchDataForQuery:(NSString *)sql;
-(NSString*)fetchuserNamefromDB;

//////SQLFunction//////
- (void)InsertdataCustomerResponse:(NSString*)strquery;
- (NSArray*)selectResponseType:(int)QuestionId;
- (NSArray*)selectQuestionBySurveyId:(int)SurveyId;
- (NSArray*)selectSurveyWithCustomerID:(NSString *)customerID andSiteUseID:(NSString *)siteID;
- (int) dbGetCustomerResponseCount;
- (int) dbGetAuditResponseCount;

//Communication module
- (int) dbGetIncomingMessageCount;
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_ID:(NSString *)Customer_ID SiteUse_ID:(NSString *)SiteUse_ID;
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_Name:(NSString *)Customer_Name;

//- (NSArray*)dbGetPlannedVisitDetails:(NSString*)strQuery;
- (NSArray*)dbGetUsersList;
- (NSMutableArray*)dbGetFSRMessages;
- (NSArray*)dbGetFSRSentMessages;
-(void)updateReadMessagestatus:(NSString*)strQuery;
-(NSArray*)checkSurveyIsTaken:(NSString *)customerID andSiteUseID:(NSString *)siteID ansSurveyID:(NSString *)surveyID;
-(void)checkUnCompleteVisits;
-(void)updateEndTimeWithID:(NSString *)visitID;
-(NSString*)dbGetOrderCount;
-(NSString*)dbdbGetCollectionCount;
-(NSString*)dbGetReturnCount;
-(NSMutableDictionary*)dbGetPriceOrProductWithID:(NSString *)itemID;
-(void)deleteManageOrderWithRef:(NSString*)strQuery;
-(NSArray*)dbGetAppControl;
-(NSMutableDictionary *)dbGetBonusItemOfProduct:(NSString *)itemCode;
-(NSArray*)dbGetAllCategory;
-(NSArray*)dbGetBonusInfoOfProduct:(NSString *)product;
-(NSString*)dbGetOnOrderQuantityOfProduct:(NSString *)itemID;
-(void)deleteTemplateOrderWithRef:(NSString*)strQuery;
-(NSArray*)dbGetMultiCurrency;

//Reports Function
-(NSMutableArray*)dbGetDataForReport:(NSString*)sql;

//product Ser
- (NSArray *)dbGetProductCollection;
- (NSArray *)dbGetProductDetail:(NSString *)itemId organizationId:(NSString *)orgId;


//- (NSArray *)dbGetProductDetail:(NSString *)itemId organizationId:(NSString *)orgId with:(NSString*)itemUOM ;

- (NSArray *)dbGetStockInfo:(NSString *)itemId;
- (NSArray *)dbGetBonusInfo:(NSString *)itemId;
- (NSArray *)dbdbGetTargetInfo:(NSString *)itemId;
- (NSArray *)dbGetFilterByColumn:(NSString *)columnName;
- (NSArray *)dbGetCategoriesForCustomer:(NSDictionary *)customer;
- (NSArray *)dbGetProductsOfCategory:(NSDictionary *)category;
- (NSArray *)dbGetStockInfoForManage:(NSString *)itemId;
//- (NSMutableDictionary *)getProductsOfBonus:(NSString *)bname;
//- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID;
- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID :(NSString*)PriceListID;


-(NSArray*)fetchProductDetails:(NSString*)itemID organizationId:(NSString*)orgID UOMCode:(NSString*)itemUOM;


- (NSMutableDictionary *)dbdbGetPriceListProduct:(NSString *)items;


//Dashboard
-(NSArray *)dbGetListofCustomer;
-(NSArray *)dbGetTotalPlannedVisits;
-(NSArray *)dbGetTotalCompletedVisits;
-(NSArray *)dbGetTotalOrder;
-(NSArray *)dbGetTotalReturn;
-(NSArray *)dbGetTotalCollection;
-(NSArray *)dbGetSaleTrend;
-(NSArray *)dbGetCustomerStatus:(NSString *)customer;
-(NSArray *)dbGetCustomerSales:(NSString *)customer;
-(NSArray *)dbGetCustomerOutstanding:(NSString *)customer;
-(NSArray *)dbGetCustomerOutOfRoute;
-(NSArray *)dbGgtDetailofCustomer:(NSString *)customerID andSite:(NSString *)siteID;

////////Session
-(NSString *)verifyLogin:(NSString *)login andPassword:(NSString *)password;
-(NSString *) decrypt:(NSString *)encryptedtext andKey:(NSString *)keytext;
-(NSString *) encrypt:(NSString *)plaintext andKey:(NSString *)keytext;
-(void)writeXML;

//////Customer
- (NSArray *)dbGetCollection;
- (NSArray *)dbGetFilterByColumnCustomer:(NSString *)columnName;
- (NSArray *)dbGetTarget:(NSString *)customerCode;
- (NSArray *)dbGetRecentOrders:(NSString *)customerId;
- (NSArray *)dbGetPriceList:(NSString *)customerId;
- (NSArray *)dbGetPriceListGeneric:(NSString *)customerId;
- (NSArray *)dbGetOrderAmountForAvl_Balance:(NSString *)customerId;
- (NSArray *)dbGetCashCutomerList;
- (NSArray *)dbGetCustomerProductSalesWithCustomerID:(NSString *)custID andProductId:(NSString *)productID;
- (void)cancel;

////////////////////Distribution/////////////////////
- (NSArray *)dbGetDistributionCollection;
- (NSArray *)checkCurrentVisitInDC:(NSString *)visitID;
- (NSString *)saveDistributionCheckWithCustomerInfo:(NSMutableDictionary *)customerInfo andDistChectItemInfo:(NSMutableArray *)fetchDistriButionCheckLocations;
-(NSMutableArray *)fetchDistriButionCheckLocations;
-(NSMutableArray*)fetchDistributionCheckItemsForcustomer;
-(NSMutableArray*)fetchDCMinStockOfMSLItems;



///SalesOrder
- (NSString *)savePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (NSString *)saveOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (void)saveOrderHistoryWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderID:(NSString *)orderID andOrderRef:(NSString *)lastOrderReference;
- (void)deleteSalesPerformaOrderItems:(NSString *)refId;
- (NSString *)updatePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderRef:(NSString *)orderRef;
- (NSString *)saveReturnOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID;
- (void)saveTemplateWithName:(NSString *)name andOrderInfo:(NSMutableArray *)orderInfo;
- (void)saveDocAdditionalInfoWithDoc:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5;
- (NSArray *)dbGetReturnsType;
- (NSArray *)dbGetReasonCode;
- (NSArray *)dbGetTemplateOrder;
- (NSArray *)dbGetTemplateOrderItem:(NSString *)refId;
- (NSArray *)dbGetPerformaOrder:(NSString *)customerId withSiteId:(NSString *)siteId;
- (NSArray *)dbGetPerformaOrder;
- (NSArray *)dbGetConfirmOrder:(NSString *)customerId withSiteId:(NSString *)siteId;
- (NSArray *)dbGetSalesConfirmOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesPerformaOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesOrderItems:(NSString *)refId;
- (NSArray *)dbGetSalesOrderInvoice:(NSString *)refId;
- (NSArray *)dbGetInvoicesOfCustomer:(NSString *)customerId withSiteId:(NSString *)siteId andShipCustID:(NSString *)sCusId withShipSiteId:(NSString *)sSiteId andIsStatement:(BOOL)isStatement;
-(void)saveConfirmedOrder : (NSMutableDictionary *)order;
//- (void)testDeleteDataFromTable:(NSString *)tblName;

//SendOrder
- (void)dbGetConfirmOrder;
- (void)dbGetConfirmOrderItems;
- (void)dbGetConfirmOrderLots;
- (void)writeXMLString;
- (void)deleteSalesOrder;


//SendFeedback
- (void)writeFeedbackXMLString:(NSString *)customerID;

////////Collection
- (NSString *)saveCollection:(NSDictionary *)collectionInfo withCustomerInfo:(NSDictionary *)customerInfo andInvoices:(NSArray *)invoices;

//Route
- (NSArray *)dbGetCollectionFromDate:(NSDate *)date;
- (NSArray *)dbGetOpenVisiteFromCustomerID:(NSString *)customer_ID andSiteID:(NSString *)siteID;

- (void)saveVistWithCustomerInfo:(NSMutableDictionary *)customerInfo andVisitID:(NSString *)visitID;
- (void)saveVisitEndDateWithVisiteID:(NSString *)visitID;
- (NSArray *)dbGetDuesStaticWithCustomerID:(NSString *)customerID;
- (void)saveVisitStatusWithVisiteID:(NSString *)visitID;
- (void)insertProductAddInfo:(NSDictionary *)row;


//modify response

-(NSArray*)ModifyResponseType:(NSString*)query;


-(NSMutableArray*)fetchAgencies;
-(NSMutableArray*)fetchUOMforItem:(NSString*)itemCode;
//- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM;
- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM :(NSString*)priceListID;

- (NSArray *)dbGetProductDetailwithUOM:(NSString *)itemId organizationId:(NSString *)orgId;



+(NSMutableArray*)fetchConversionFactorforUOMitemCode:(NSString*)itemCode ItemUOM:(NSString*)itemUom;
+(NSMutableArray*)fetchOrderHistoryPopupData :(NSString*)docRefNumber;
+(NSString*)isCustomerBlocked:(NSString*)customerID;

- (NSArray *)fetchProductsforAllCategories;
-(NSMutableArray*)fetchSalesPerAgencyforAllCustomers;
-(NSMutableDictionary*)fetchSalesTargetMonthandYear;
-(NSMutableDictionary*)fetchSalesTargetMonthandYearforCustomer:(NSString*)customerNumber;
-(BOOL)saveCapturedImagestoVisitAdditionalInfo;
-(BOOL)updateCapturedImagesDatatoVisitAdditionalInfo;
-(BOOL)deleteCapturedImagesData;
-(NSMutableArray *)fetchProductForCategory:(NSString *)inventory_Item_ID;
-(NSMutableArray *) fetchCategoryArrayForAll;
-(BOOL)saveCapturedImagesToVisitImages:(NSArray *)imageArray;
-(BOOL)saveCapturedImagesWithMetaData:(NSMutableArray *)categoryArray product:(NSArray *)productArray;

//sendIdleReason
- (void)writeIdleReasonXMLString;

-(void)deleteOrdersAfterBackGroundSync:(NSString *)syncStr AndLastSyncTime:(NSString *)lastSync;

@end
