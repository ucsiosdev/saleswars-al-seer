//
//  Utils.m
//  EServices
//
//  Created by Charith Nidarsha on 5/29/12.
//  Copyright (c) 2013 Emaar Technologies LLC. All rights reserved.
//

#import "SharedUtils.h"

#import "SharedConstants.h"
//#import "DataLibSettingsManager.h"
#import "Reachability.h"
#import "NSData+Base64.h"
//#import "StringEncryption.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonDigest.h>

@implementation SharedUtils

static NSCalendar *gregorainCalendar = nil;
static NSLocale *usLocale = nil;
static NSTimeZone *gmtTimeZone = nil;

+ (void) initialize
{
    usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    gmtTimeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    
    gregorainCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorainCalendar setTimeZone:gmtTimeZone];
    [gregorainCalendar setLocale:usLocale];
}

+ (NSString*) readContentsFromFile:(NSString*)fileName type:(NSString*)fileType
{
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:path])
    {
        NSError *error = nil;
        return [[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return nil;
}

+ (BOOL) isConnectedToInternet
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    return ([reachability currentReachabilityStatus] != NotReachable);
}

+ (BOOL) checkInternetConnectionAndShowMessage:(BOOL)shouldShowMessage
{
    if([SharedUtils isConnectedToInternet])
    {
        return TRUE;
    }
    if(shouldShowMessage)
    {
        //[SharedUtils showNoInternetConnectionMessage];
    }
    return FALSE;
}

+ (BOOL) validateEmailAddress:(NSString*)email
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL) isStringIsNumeric:(NSString*)string
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return TRUE;
    }
    return FALSE;
}

+ (BOOL) isStringIsDecimal:(NSString*)string
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([string isEqualToString:@"."] || [string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9 and .
        return TRUE;
    }
    return FALSE;
}

+ (BOOL) isStringIsAlpha:(NSString*)string
{
    NSCharacterSet* notDigits = [[NSCharacterSet letterCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return TRUE;
    }
    return FALSE;
}

+ (BOOL) isStringIsAlphaNumeric:(NSString*)string
{
    NSCharacterSet *alphaSet = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    if ([string rangeOfCharacterFromSet:alphaSet].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return TRUE;
    }
    return FALSE;
}

+ (BOOL) isStringIsValidEmail:(NSString*)string
{
    BOOL isAlphaNumeric = [SharedUtils isStringIsAlphaNumeric:string];
    return isAlphaNumeric || [string isEqualToString:@"@"] || [string isEqualToString:@"-"] || [string isEqualToString:@"_"] 
    || [string isEqualToString:@"."];
}

+ (BOOL) isStringIsMobile:(NSString*)string
{
    BOOL isDigit = [SharedUtils isStringIsNumeric:string];
    return (isDigit || [string isEqualToString:@"+"] || [string isEqualToString:@"-"]);
}

+ (NSString*) stringForValue:(CGFloat)value withFormatter:(NSString*)formatter
{
    NSNumberFormatter *numformatter = [[NSNumberFormatter alloc] init];
    [numformatter setPositiveFormat:formatter];
    [numformatter setNegativeFormat:formatter];
    [numformatter setLocale:usLocale];
    return [numformatter stringFromNumber:@(value)];
}

+ (NSString*) stringForDate:(NSDate*)date withFormatter:(NSString*)formatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:gmtTimeZone];
    [dateFormatter setDateFormat:formatter];
    [dateFormatter setLocale:usLocale];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate*) dateForString:(NSString*)strDate withFormatter:(NSString*)formatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:gmtTimeZone];
    [dateFormatter setDateFormat:formatter];
    [dateFormatter setLocale:usLocale];
    NSDate *date = [dateFormatter dateFromString:strDate];
    return date;
}

+ (NSString*) urlEncodedString:(NSString*)string
{
    if([string length] <=0) return nil;
    
    CFStringRef urlString = CFURLCreateStringByAddingPercentEscapes(
                                                                    NULL,
                                                                    (CFStringRef)string,
                                                                    NULL,
                                                                    (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                    kCFStringEncodingUTF8 );
    NSString *result = (__bridge NSString*)urlString;
    CFRelease(urlString);
    //NSLog(@"url encoded value : %@", result);
    return result;
}

+ (NSDate*) convertSQLDateStringToDate:(NSString*)strDate
{
    if(strDate == nil || [strDate length] <=0)
    {
        return nil;
    }
    NSRange range = [strDate rangeOfString:@"+"];
    if(range.location == NSNotFound)
    {
        //NSLog(@"invalid");
        return nil;
    }
    strDate = [strDate substringToIndex:range.location];
    NSArray *array = [strDate componentsSeparatedByString:@"T"];
    if(array.count !=2) return nil;
    NSArray *dateArray = [array[0] componentsSeparatedByString:@"-"];
    NSArray *timeArray = [array[1] componentsSeparatedByString:@":"];
    if(dateArray.count !=3 || timeArray.count != 3) return nil;
    
    NSDateComponents *components = [gregorainCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[NSDate date]];
    
    [components setYear:[dateArray[0] intValue]];
    [components setMonth:[dateArray[1] intValue]];
    [components setDay:[dateArray[2] intValue]];
    [components setHour:[timeArray[0] intValue]];
    [components setMinute:[timeArray[1] intValue]];
    
    NSString *strSeconds = [timeArray[2] componentsSeparatedByString:@"."][0];
    int seconds = [strSeconds intValue];
    if(seconds >=0 && seconds <= 59)
    {
        [components setSecond:seconds];
    }
    NSDate *date = [gregorainCalendar dateFromComponents:components];
    return date;
}

+ (NSDate*) convertJsonDateStringToDate:(NSString*)strDate
{
    if(strDate == nil || [strDate length] <=0)
    {
        return 0;
    }
    
    NSArray *array = [strDate componentsSeparatedByString:@"T"];
    if(array.count !=2) return 0;
    NSArray *dateArray = [array[0] componentsSeparatedByString:@"-"];
    NSArray *timeArray = [array[1] componentsSeparatedByString:@":"];
    if(dateArray.count !=3 || timeArray.count != 3) return 0;
    
    NSDateComponents *components = [gregorainCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[NSDate date]];
    
    [components setYear:[dateArray[0] intValue]];
    [components setMonth:[dateArray[1] intValue]];
    [components setDay:[dateArray[2] intValue]];
    [components setHour:[timeArray[0] intValue]];
    [components setMinute:[timeArray[1] intValue]];
    
    NSString *strSeconds = [timeArray[2] componentsSeparatedByString:@"."][0];
    int seconds = [strSeconds intValue];
    if(seconds >=0 && seconds <= 59)
    {
        [components setSecond:seconds];
    }
    NSDate *date = [gregorainCalendar dateFromComponents:components];
    return date;
}

+ (NSString*) stringForDouble:(CGFloat)value
{
    static NSString *integerFormatString = @"###,###,###,##0.00";
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setPositiveFormat:integerFormatString];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:usLocale];
    [formatter setNegativeFormat:integerFormatString];
    NSString *returnString = [formatter stringFromNumber:@(value)];
    return returnString;
}

+ (iOSDeviceFamily) getCurrentDeviceFamily
{
    UIUserInterfaceIdiom idiom = UI_USER_INTERFACE_IDIOM();
    
    switch (idiom)
    {
        case UIUserInterfaceIdiomPhone:
        {
            if ([UIScreen mainScreen].scale == 2.0f)
            {
                CGSize result = [[UIScreen mainScreen] bounds].size;
                CGFloat scale = [UIScreen mainScreen].scale;
                result = CGSizeMake(result.width * scale, result.height * scale);
                
                if(result.height == 960)
                {
                    return iPhone4Retina;
                }
                if(result.height == 1136)
                {
                    return iPhone5;
                }
            }
            else
            {
                return iPhoneStandardOld;
            }
        }
            break;
            
        case UIUserInterfaceIdiomPad:
        {
            if ([UIScreen mainScreen].scale == 2.0f)
            {
                return iPadRetina;
            }
            else
            {
                return iPadStandard;
            }
        }
            break;
            
        default:
            break;
    }
    return iPhoneStandardOld;
}

+ (MajoriOSVersion) majorIOSVersion
{
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(version >=7)
    {
        return MajoriOSVersion7;
    }
    else if(version >=6)
    {
        return MajoriOSVersion6;
    }
    return MajoriOSVersionLessThan6;
}

+ (NSString*) getGUID
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString * uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    
    return uuidString;
}

+ (NSString*) getApplicationCachesDirectory
{
    NSString *path =  [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    return path;
}

+ (NSString*) applicationVersionNumber
{
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    return version;
}

+ (NSString*) applicationBuildNumber
{
    NSString *version = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    return version;
}

+ (NSString*) trimString:(NSString*)strInput
{
    return [strInput stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+ (NSString*) md5HexDigest2:(NSString*)input
{
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+ (NSString*) md5HexDigest:(NSString*)str
{
	const char *cStr = [str UTF8String];
	unsigned char result[16];
	CC_MD5( cStr, strlen(cStr), result);
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

+ (NSString*) sha1Digest:(NSString*)str
{
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

+ (NSString*) encryptPWDWithAES128:(NSString*)pwd usingKey:(NSString*)key
{
//    StringEncryption *crypto = [[StringEncryption alloc] init];
//    NSData *secretData = [pwd dataUsingEncoding:NSUTF8StringEncoding];
//    CCOptions padding = kCCOptionPKCS7Padding;
//    NSData *encryptedData = [crypto encrypt:secretData key:[key dataUsingEncoding:NSUTF8StringEncoding] padding:&padding];
//    NSString *result = ([encryptedData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]);
//    return result;
    return @"GTH";
}

+ (NSDate*) convertOracleJsonDateStringToDate:(NSString*)strDate
{
    if(strDate == nil || [strDate length] <=0)
    {
        return 0;
    }
    
    NSArray *array = [strDate componentsSeparatedByString:@" "];
    if(array.count !=2) return 0;
    NSArray *dateArray = [array[0] componentsSeparatedByString:@"-"];
    NSArray *timeArray = [array[1] componentsSeparatedByString:@":"];
    if(dateArray.count !=3 || timeArray.count != 3) return 0;
    
    NSDateComponents *components = [gregorainCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[NSDate date]];
    
    [components setYear:[dateArray[0] intValue]];
    [components setMonth:[dateArray[1] intValue]];
    [components setDay:[dateArray[2] intValue]];
    [components setHour:[timeArray[0] intValue]];
    [components setMinute:[timeArray[1] intValue]];
    
    NSString *strSeconds = [timeArray[2] componentsSeparatedByString:@"."][0];
    int seconds = [strSeconds intValue];
    if(seconds >=0 && seconds <= 59)
    {
        [components setSecond:seconds];
    }
    NSDate *date = [gregorainCalendar dateFromComponents:components];
    return date;
}

+ (NSString*) AES128EncryptedMessage:(NSString*)plainText withKey:(NSString*)key
{
    if(plainText == nil || key == nil) return nil;
    
    NSData *plain = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    NSData *cipher = [SharedUtils AES128Encryptdata:plain withKey:key];
    NSMutableString *stringToPass = [[NSMutableString alloc] initWithString:[[cipher description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]];
    
    [stringToPass replaceOccurrencesOfString:@" " withString:@"" options:1 range:NSMakeRange(0, [stringToPass length])];
    return [NSString stringWithString:stringToPass];
}

+ (NSData*) AES128Encryptdata:(NSData*)data withKey:(NSString*)key
{
	// 'key' should be 32 bytes for AES256, will be null-padded otherwise
	char keyPtr[kCCKeySizeAES128+1]; // room for terminator (unused)
	bzero(keyPtr, sizeof(keyPtr)); // fill with zeroes (for padding)
	
	// Initialization vector; dummy in this case 0's.
	uint8_t iv[kCCKeySizeAES128];
	memset((void *) iv, 0x0, (size_t) sizeof(iv));
	
	// fetch key data
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	//printf("keyPtr  ===  %s length = %d\n", keyPtr,strlen(keyPtr));
	NSUInteger dataLength = [data length];
	
	//See the doc: For block ciphers, the output size will always be less than or
	//equal to the input size plus the size of one block.
	//That's why we need to add the size of one block here
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
	
	size_t numBytesEncrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
										  keyPtr, kCCKeySizeAES128,
										  NULL /* initialization vector (optional) */,
										  [data bytes], dataLength, /* input */
										  buffer, bufferSize, /* output */
										  &numBytesEncrypted);
	if(cryptStatus == kCCSuccess)
    {
		//the returned NSData takes ownership of the buffer and will free it on deallocation
		return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
	}
	
	free(buffer); //free the buffer;
	return nil;
}

/// generates a random number between 0 and 1
+ (CGFloat) generateRandomRanumber
{
    NSLog(@"arc4random_uniform : %d", arc4random_uniform(100)); // generates a random number between 0 and 99
    srand48(time(0));
    double r = drand48();
    return r;
}

+ (NSUInteger) majoriOSVersion
{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

+ (NSString*) urlEncodedURLFromCompleteURL:(NSString*)fullURL
{
    if([fullURL length] <= 0) return nil;
    
    NSString *lastPath = [fullURL lastPathComponent];
    NSString *urlEncodedLastPath = [SharedUtils urlEncodedString:lastPath];
    if(lastPath && urlEncodedLastPath)
    {
        NSString *finalPath = [fullURL stringByReplacingOccurrencesOfString:lastPath withString:urlEncodedLastPath];
        return finalPath;
    }
    return nil;
}
@end
