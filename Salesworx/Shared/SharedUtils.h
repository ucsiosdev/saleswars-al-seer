//
//  Utils.h
//  EServices
//
//  Created by Charith Nidarsha on 5/29/12.
//  Copyright (c) 2013 Emaar Technologies LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SharedConstants.h"

@interface SharedUtils : NSObject

+ (NSString*) readContentsFromFile:(NSString*)fileName type:(NSString*)fileType;
+ (BOOL) isConnectedToInternet;
+ (BOOL) checkInternetConnectionAndShowMessage:(BOOL)shouldShowMessage;
+ (iOSDeviceFamily) getCurrentDeviceFamily;
+ (MajoriOSVersion) majorIOSVersion;

#pragma mark - validation methods related to UI
+ (BOOL) validateEmailAddress:(NSString*)email;
+ (BOOL) isStringIsNumeric:(NSString*)string;
+ (BOOL) isStringIsAlpha:(NSString*)string;
+ (BOOL) isStringIsAlphaNumeric:(NSString*)string;
+ (BOOL) isStringIsValidEmail:(NSString*)string;
+ (BOOL) isStringIsMobile:(NSString*)string;
+ (BOOL) isStringIsDecimal:(NSString*)string;

#pragma mark - string to number conversion and vise versa
+ (NSString*) stringForValue:(CGFloat)value withFormatter:(NSString*)formatter;
+ (NSString*) stringForDate:(NSDate*)date withFormatter:(NSString*)formatter;
+ (NSString*) stringForDouble:(CGFloat)value;

+ (NSString*) urlEncodedString:(NSString*)string;
+ (NSString*) urlEncodedURLFromCompleteURL:(NSString*)fullURL;

#pragma mark - datatime conversion related
+ (NSDate*) dateForString:(NSString*)strDate withFormatter:(NSString*)formatter;
+ (NSDate*) convertSQLDateStringToDate:(NSString*)strDate;
+ (NSDate*) convertJsonDateStringToDate:(NSString*)strDate;
+ (NSDate*) convertOracleJsonDateStringToDate:(NSString*)strDate;

+ (NSString*) getGUID;
+ (NSString*) getApplicationCachesDirectory;
+ (NSString*) applicationVersionNumber;
+ (NSString*) applicationBuildNumber;
+ (NSString*) trimString:(NSString*)strInput;

#pragma mark - Cyptography related
+ (NSString*) md5HexDigest:(NSString*)input;
+ (NSString*) sha1Digest:(NSString*)input;
+ (NSString*) encryptPWDWithAES128:(NSString*)pwd usingKey:(NSString*)key;
+ (NSString*) AES128EncryptedMessage:(NSString*)plainText withKey:(NSString*)key;

/// generates a random number between 0 and 1
+ (CGFloat) generateRandomRanumber;

@end
