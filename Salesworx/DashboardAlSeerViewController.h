//
//  DashboardAlSeerViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MIMBarGraph.h"
#import "MIMColor.h"
#import "UUChart.h"
#import "AlSeerWelcomeViewController.h"
#import "SWDefaults.h"
#import "MedRepElementDescriptionLabel.h"


@interface DashboardAlSeerViewController : UIViewController<BarGraphDelegate,UITabBarControllerDelegate,UITableViewDataSource,UITableViewDelegate>

{
    NSDictionary *barProperty;
    NSMutableArray * arrayForFirstMonth;
    NSMutableArray * arrayForSecondMonth;
    NSMutableArray * arrayForThirdMonth;
    NSArray *yValuesArray,*xValuesArray;
    NSArray *xTitlesArray;
    NSMutableArray* dsgArr;

    UIPopoverController* stockPopOver;
    
    
    AlSeerWelcomeViewController* welcomeVC;

}
@property (strong, nonatomic) IBOutlet UITableView *dashTblView;

@property(strong,nonatomic)NSMutableArray* dashBoardtblArray,*agencyArray,*targetArray;

@property(strong,nonatomic)UUChart* chartView;
@property (strong, nonatomic) IBOutlet UIView *shadowView;

@property (strong, nonatomic) IBOutlet UILabel *timeGoneLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalTargetLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalInvoicesLbl;
@property (strong, nonatomic) IBOutlet UILabel *previousDaySaleLbl;
@property (strong, nonatomic) IBOutlet UILabel *balancetoGoLbl;
@property (strong, nonatomic) IBOutlet UILabel *achLbl;

@property (strong, nonatomic) IBOutlet MIMBarGraph *myBarChart;
@property (strong, nonatomic) IBOutlet UIImageView *targetAndAcheivmentImageView;

@property (weak, nonatomic) IBOutlet UITableView *graphTableView;
@property(strong,nonatomic)NSMutableArray* salesTargetItemsArray;

//top level lables
@property (strong, nonatomic) IBOutlet UILabel *plannedLbl;
@property (strong, nonatomic) IBOutlet UILabel *completedLbl;
@property (strong, nonatomic) IBOutlet UILabel *outofRouteLbl;
@property (strong, nonatomic) IBOutlet UILabel *adherenceLbl;


@property (strong, nonatomic) IBOutlet UILabel *orderCount;
@property (strong, nonatomic) IBOutlet UILabel *returnCount;
@property (strong, nonatomic) IBOutlet UILabel *dailyTargetValue;
@property (strong, nonatomic) IBOutlet UILabel *targetAchievedLbl;

@property (strong, nonatomic) IBOutlet UIView *visitStatisticsView;
@property (strong, nonatomic) IBOutlet UIView *orderStatisticsView;
@property (strong, nonatomic) IBOutlet UIView *plannedView;
@property (strong, nonatomic) IBOutlet UIView *completedView;
@property (strong, nonatomic) IBOutlet UIView *outOfRouteView;
@property (strong, nonatomic) IBOutlet UIView *adherenceView;
@property (strong, nonatomic) IBOutlet UIView *ordersView;
@property (strong, nonatomic) IBOutlet UIView *returnsView;
@property (strong, nonatomic) IBOutlet UIView *dailyReqView;
@property (strong, nonatomic) IBOutlet UIView *dayOrderView;
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UIView *graphView;
@property (strong, nonatomic) IBOutlet UIView *targetVsSalesDetailsView;



@property (weak, nonatomic) IBOutlet MedRepElementDescriptionLabel *lblAppUserName;
@property (weak, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastSyncLable;
@property (weak, nonatomic) IBOutlet MedRepElementDescriptionLabel *lastSyncStatusLable;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalTargetCurreny;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalSalesCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblLastDaySalesCurrency;
@property (weak, nonatomic) IBOutlet UILabel *lblBalanceToGoCurrency;


@end
