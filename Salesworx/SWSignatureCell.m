//
//  SWSignatureCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSignatureCell.h"

@implementation SWSignatureCell

@synthesize signatureView;
@synthesize textField;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.textLabel setText:@"Sign here"];
        
        if (self.textField!=nil) {
            self.textField=nil;
        }
        [self setTextField:[[UITextField alloc] initWithFrame:CGRectZero]];
        [self.contentView addSubview:self.textField];
        [self.textField setInputView:self.signatureView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //[self.signatureView setFrame:CGRectMake(3, 3, self.contentView.bounds.size.width - 6, self.contentView.bounds.size.height - 6)];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        [self.textField becomeFirstResponder];
    }
}



#pragma mark UITableViewCell+Responder override
- (void)resignResponder {
    if ([self.textField isFirstResponder]) {
        [self.textField resignFirstResponder];
    }
}
#pragma mark SWSignatureView Delegate
- (void)signatureViewDidStart:(SWSignatureView *)signatureView {
    
}

- (void)signatureViewDidFinishWithImage:(UIImage *)image {
    [self.textField resignFirstResponder];
}

#pragma mark TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self setSignatureView:[[SWSignatureView alloc] initWithFrame:CGRectZero]];
    [self.signatureView setFrame:CGRectMake(0, 0, 400, 250)];
    [self.signatureView setDelegate:self];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(editableCellDidStartUpdate:)]) {
        [self.delegate editableCellDidStartUpdate:self];
    }
}
@end