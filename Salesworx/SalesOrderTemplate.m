//
//  SalesOrderTemplate.m
//  SalesWars
//
//  Created by Prashannajeet on 31/08/22.
//  Copyright © 2022 msaad. All rights reserved.
//

#import "SalesOrderTemplate.h"
#import "SalesOrderPDFTempleteTableViewCell.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "MedRepDefaults.h"

#define kImagesFolder @"Signature"

@implementation SalesOrderTemplate
@synthesize pageNumber;

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SalesOrderTemplate" owner:self options:nil];
        self = [subviewArray objectAtIndex:0];
        self.frame=frame;
        if (@available(iOS 15.0, *)) {
            self.pdfTableView.sectionHeaderTopPadding = 0;
        }
        
        self.lblRefNumber.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleCustomer.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleCustomerContact.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleCustomerNo.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleSalesman.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleSalesmanContactNo.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleOrderTime.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleSign.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleName.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblTitleComments.font = kSWX_FONT_SEMI_BOLD(14);
        
        self.lblCustomer.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblCustomerContact.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblCustomerNumber.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblSalesMan.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblSalesContactNo.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblOrderTime.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblSigneeName.font = kSWX_FONT_SEMI_BOLD(14);
        self.lblSigneeComments.font = kSWX_FONT_SEMI_BOLD(14);
    }
    return self;
}

-(void)updatePDF {
    
    if(self.isFromManageOrderOnSwipe)
    {
        self.lblRefNumber.text = [NSString stringWithFormat:@"Ref No. : %@",self.salesOrderDict[@"Orig_Sys_Document_Ref"]];
        self.lblCustomer.text = self.customerDict[@"Customer_Name"];
        
        NSString *ContactNo = self.customerDict[@"Phone"] ? self.customerDict[@"Phone"] : self.customerDict[@"Contact"];
        self.lblCustomerContact.text = ContactNo.length > 0 ? ContactNo : @"N/A";
        self.lblCustomerNumber.text = self.customerDict[@"Customer_No"];
        self.lblOrderTime.text = self.salesOrderDict[@"End_Time"];
        
        NSString *path = [self dbGetImagesDocumentPath];
        path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",self.salesOrderDict[@"Orig_Sys_Document_Ref"]]];
        self.signeeImageView.image = [UIImage imageWithContentsOfFile:path];
        
        self.lblSigneeName.text = self.salesOrderDict[@"Signee_Name"] ? self.salesOrderDict[@"Signee_Name"] : @"N/A";
        
        NSString *comments = [self.salesOrderDict[@"Packing_Instructions"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.lblSigneeComments.text = comments.length > 0 ? comments : @"N/A";
    } else {
        self.lblRefNumber.text = [NSString stringWithFormat:@"Ref No. : %@",self.salesOrderDict[@"orderRef"]];
        self.lblCustomer.text = self.customerDict[@"Customer_Name"];
        
        NSString *ContactNo = self.customerDict[@"Phone"] ? self.customerDict[@"Phone"] : self.customerDict[@"Contact"];
        self.lblCustomerContact.text = ContactNo.length > 0 ? ContactNo : @"N/A";
        self.lblCustomerNumber.text = self.customerDict[@"Customer_No"];
        self.lblOrderTime.text = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
        
        self.signeeImageView.image = [UIImage imageWithData:[SWDefaults fetchSignature]];
        self.lblSigneeName.text = self.orderAdditionalInfoDict[@"name"] ? self.orderAdditionalInfoDict[@"name"] : @"N/A";
        self.lblSigneeComments.text = self.orderAdditionalInfoDict[@"comments"] ? self.orderAdditionalInfoDict[@"comments"] : @"N/A";
    }
    
    NSDictionary *userProfile = [SWDefaults userProfile];
    self.lblSalesMan.text = [NSString stringWithFormat:@"%@ - %@",userProfile[@"Emp_Code"], userProfile[@"Emp_Name"]];
    self.lblSalesContactNo.text = userProfile[@"Emp_Phone"] ? userProfile[@"Emp_Phone"] : @"N/A";
    
    numberOfRowsInaPage = 10;
    StartIndexInCurrentPage = (pageNumber==1) ? 0 : (((pageNumber-1)*numberOfRowsInaPage));
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsSalesOrder.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"pdfTemplate";
    SalesOrderPDFTempleteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderPDFTempleteTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.lblArticleNumber.text = @"Article";
    cell.lblArticleDesc.text = @"Article Description";
    cell.lblbarcode.text = @"Bar Code No";
    cell.lblOUM.text = @"UOM";
    cell.lblOrderQty.text = @"Order Qty";
    cell.lblUOM.text = @"UOM";
    
    cell.lblArticleNumber.font = kSWX_FONT_SEMI_BOLD(14);
    cell.lblArticleDesc.font = kSWX_FONT_SEMI_BOLD(14);
    cell.lblbarcode.font = kSWX_FONT_SEMI_BOLD(14);
    cell.lblOUM.font = kSWX_FONT_SEMI_BOLD(14);
    cell.lblOrderQty.font = kSWX_FONT_SEMI_BOLD(14);
    cell.lblUOM.font = kSWX_FONT_SEMI_BOLD(14);
    
    return cell;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString* identifier=@"pdfTemplate";
    SalesOrderPDFTempleteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesOrderPDFTempleteTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSInteger currentRowArrayPostion = StartIndexInCurrentPage+indexPath.row;
    if(currentRowArrayPostion < self.itemsSalesOrder.count) {
        
        NSMutableDictionary *itemDic = [self.itemsSalesOrder objectAtIndex:currentRowArrayPostion];
        cell.lblArticleNumber.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"Item_Code"]];
        cell.lblArticleDesc.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"Description"]];
        
        NSString *barcode = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"EANNO"]];
        cell.lblbarcode.text = barcode.length > 0 ? barcode : @"N/A";
        cell.lblOUM.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"Order_Quantity_UOM"]];
        
        cell.lblUOM.text = @"N/A";
        if ([itemDic valueForKey:@"conversionfactor"]) {
            cell.lblUOM.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"conversionfactor"]];
        } else {
            NSMutableArray *conversionArray = [SWDatabaseManager fetchConversionFactorforUOMitemCode:[itemDic valueForKey:@"Item_Code"] ItemUOM:[itemDic valueForKey:@"Order_Quantity_UOM"]];
            if (conversionArray.count > 0) {
                NSInteger conversionVal = [[[conversionArray valueForKey:@"Conversion"]objectAtIndex:0] integerValue];
                cell.lblUOM.text = [NSString stringWithFormat:@"%ld",(long)conversionVal];
            }
        }
        
        if(self.isFromManageOrderOnSwipe)
        {
            cell.lblOrderQty.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"Ordered_Quantity"]];
        } else {
            cell.lblOrderQty.text = [NSString stringWithFormat:@"%@", [itemDic valueForKey:@"Qty"]];
        }
        
        cell.lblArticleNumber.font = kSWX_FONT_REGULAR(14);
        cell.lblArticleDesc.font = kSWX_FONT_REGULAR(14);
        cell.lblbarcode.font = kSWX_FONT_REGULAR(14);
        cell.lblOUM.font = kSWX_FONT_REGULAR(14);
        cell.lblOrderQty.font = kSWX_FONT_REGULAR(14);
        cell.lblUOM.font = kSWX_FONT_REGULAR(14);
    } else {
        cell.lblArticleNumber.hidden=YES;
        cell.lblArticleDesc.hidden=YES;
        cell.lblUOM.hidden=YES;
        cell.lblbarcode.hidden=YES;
        cell.lblOrderQty.hidden=YES;
        cell.lblOUM.hidden=YES;
    }
    return  cell;
}

- (NSString*) dbGetImagesDocumentPath
{
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

@end
