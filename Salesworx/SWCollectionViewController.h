//
//  SWCollectionViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/20/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CurrencyViewController.h"
#import "SWVisitOptionsViewController.h"

@interface SWCollectionViewController : SWViewController <UITableViewDelegate, UITableViewDataSource, EditableCellDelegate ,CLLocationManagerDelegate, UITextFieldDelegate,UIPopoverControllerDelegate> {
    NSDictionary *customer;
    UIImageView *noCustomerImageView;
    UITableView *tableView;
    UIButton *selectCustomerButton;
    UIPopoverController *collectionTypePopOver;
    NSString *collectionType;
    NSArray *invoices;
    CustomerHeaderView *customerHeaderView;
    NSMutableDictionary *form;
    UIPopoverController *popOverController;
    BOOL shouldPresentCustomerList;
    
    UIView *myBackgroundView;
    UIPopoverController *currencyTypePopOver;
    AppControl *appControl;
    CurrencyViewController *currencyTypeViewController;
    
    SWVisitOptionsViewController * swVisitOptionsVC;
    
    CLLocationManager *locationManager;
    
    NSString* visitID;

    
}
@property (nonatomic, strong) NSMutableDictionary *customer;


- (id)initWithCustomer:(NSDictionary *)customer;

@end
