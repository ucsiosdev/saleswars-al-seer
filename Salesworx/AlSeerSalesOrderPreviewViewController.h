//
//  AlSeerSalesOrderPreviewViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerSalesOrderPreviewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


{
    id delegate;
}
@property(strong,nonatomic)NSMutableDictionary* salesOrderDict;
@property(nonatomic) BOOL isReturn;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;



@property (nonatomic, assign) id delegate;

@end
