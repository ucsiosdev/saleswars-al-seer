//
//  OnsiteOptionsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "OnsiteOptionsViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWDefaults.h"

@interface OnsiteOptionsViewController ()

@end

@implementation OnsiteOptionsViewController


@synthesize segmentControl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [segmentControl setSelectedSegmentIndex:UISegmentedControlNoSegment];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)segmentAction:(UISegmentedControl *)segment
{
    
}


- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    
    NSString* title=  [sender titleForSegmentAtIndex:sender.selectedSegmentIndex];

    
    NSLog(@"selected title is %@", title);
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        
        //onsite  clicked
        
        [SWDefaults setOnsiteVisitStatus:YES];
        NSString *isDCFlag = [[SWDefaults userProfile] objectForKey:@"Is_DC_Optional"];
        
        if ([isDCFlag isEqualToString:@"Y"]) {
            [SWDefaults isDCOptionalforOnsiteVisit:YES];
        }
        
        if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
        {
            [self.delegate selectedVisitOption:title];
        }
        
        [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }
    else {
        [SWDefaults setOnsiteVisitStatus:NO];
        if([self.delegate respondsToSelector:@selector(selectedVisitOption:)])
        {
            [self.delegate selectedVisitOption:@"Telephonic"];
        }
        [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }
}

@end
