//
//  SWSignatureView.h
//  SWFoundation
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SWSignatureView;

@protocol SWSignatureViewDelegate <NSObject>
@optional
- (void)signatureViewDidStart:(SWSignatureView *)signatureView;
- (void)signatureViewDidFinishWithImage:(UIImage *)image;
- (void)signatureViewDidFinishWithCancle;

@end

@interface SWSignatureView : UIView {
    id < SWSignatureViewDelegate > delegate;
    
    CGPoint currentPoint;
    CGPoint previousPoint1;
    CGPoint previousPoint2;
    UIImage *curImage;
    UIToolbar *toolbar;
}

@property (unsafe_unretained) id < SWSignatureViewDelegate > delegate;
@property (nonatomic, strong) UIToolbar *toolbar;

@end