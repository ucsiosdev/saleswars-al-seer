//
//  JBSignatureController.h
//  JBSignatureController
//
//  Created by Jesse Bunch on 12/10/11.
//  Copyright (c) 2011 Jesse Bunch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>


@protocol JBSignatureControllerDelegate;
#import "FTPBaseViewController.h"


@interface JBSignatureController : FTPBaseViewController

// Allows you to set th background images in different states
@property(nonatomic,strong) UIImage *portraitBackgroundImage, *landscapeBackgroundImage;

// Buttons for confirm and cancel
@property(nonatomic,strong) UIButton *confirmButton, *cancelButton , *clearButton;

// Delegate
@property(unsafe_unretained) id<JBSignatureControllerDelegate> delegate;

// Clear the signature
-(void)clearSignature;
-(void) signatureSaveImage:(UIImage *)image withName:(NSString*)imageName;

@end



// Delegate Protocol
@protocol JBSignatureControllerDelegate <NSObject>

@required

// Called when the user clicks the confirm button
-(void)signatureConfirmed:(UIImage *)signatureImage signatureController:(JBSignatureController *)sender;

@optional

// Called when the user clicks the cancel button
-(void)signatureCancelled:(JBSignatureController *)sender;

// Called when the user clears their signature or when clearSignature is called.
-(void)signatureCleared:(UIImage *)clearedSignatureImage signatureController:(JBSignatureController *)sender;

@end