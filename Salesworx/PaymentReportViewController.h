
#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
#import "CustomerPopOverViewController.h"

@interface PaymentReportViewController : SWViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    
    UIPopoverController *currencyTypePopOver;
    UIPopoverController *datePickerPopOver;
    UIPopoverController *customPopOver;
    
    CustomersListViewController *currencyTypeViewController;
    CustomerPopOverViewController *customVC;
    
    NSMutableDictionary *customerDict;
    
    NSDate *selectedDate;
    
    
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    
    NSDate * minDate;
    
    
    IBOutlet UIView *backgroundView;
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    NSMutableArray *paymentSummaryArray;
    IBOutlet UIButton *filterButton;
    IBOutlet UITableView *paymentTableView;
    NSMutableDictionary * previousFilteredParameters;
    UIPopoverController *filterPopOverController;

}

@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@end

