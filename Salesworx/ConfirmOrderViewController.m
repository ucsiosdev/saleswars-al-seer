//
//  ConfirmOrderViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 11/17/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "ConfirmOrderViewController.h"
#import "SalesOrderNewViewController.h"
#import "UnConfirmedOrderTableViewCell.h"
#import "UnconfirmedHeaderTableViewCell.h"
@interface ConfirmOrderViewController ()

@end

@implementation ConfirmOrderViewController

@synthesize target;
@synthesize action;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:loadingView];
    loadingView.hidden=YES;
    self.title =NSLocalizedString( @"Unconfirmed Orders", nil);
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    selectedOrders = [NSMutableArray new];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
   // self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, screenWidth , screenHeight);
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    if (@available(iOS 15.0, *)) {
        self.tableView.sectionHeaderTopPadding = 0;
    }
    
   // self.tableView.editing=YES;	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getUnConfirmedOrder:  [[SWDatabaseManager retrieveManager] dbGetPerformaOrder]];
}
- (void)showActivate
{
    loadingView.hidden=NO;

    for (int i=0; i<selectedOrders.count; i++)
    {
        [[SWDatabaseManager retrieveManager] saveConfirmedOrder:[selectedOrders objectAtIndex:i]];
        [[SWDatabaseManager retrieveManager]deleteSalesPerformaOrderItems:[[selectedOrders objectAtIndex:i] stringForKey:@"Orig_Sys_Document_Ref"]];
    }
    if ([self.target respondsToSelector:self.action])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:nil];
#pragma clang diagnostic pop
    }
    loadingView.hidden=YES;
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    
}
- (void)getUnConfirmedOrder:(NSArray *)reason
{
    types=nil;
    types=[NSMutableArray arrayWithArray:reason];
    //NSLog(@"%@",types);
    [self.tableView reloadData];
}


#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [types count] ;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UnconfirmedHeaderTableViewCell *cell = (UnconfirmedHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"headerCell"];
    
    
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UnconfirmedHeaderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.custNameLbl.text= @"Customer Name";
    cell.orderAmountLbl.text=@"Order Amount";
    cell.docRefNumLbl.text=@"Doc Ref Number";
    cell.dateLbl.text=@"Date";
    return cell;
//
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Header"];
//    cell.textLabel.text = @"test";
//    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    @autoreleasepool
    {
        
        //        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
        //        cell=nil;
        //
        
        
       // SWTableViewCell *cell = (SWTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
        
//        NSMutableArray *leftUtilityButtons = nil;
//        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
//        
//        if (indexPath.row == 0) {
//            rightUtilityButtons = nil;
//        }
//        else
//        {
//            [rightUtilityButtons sw_addUtilityButtonWithColor:
//             [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
//                                                        title:@"Manage"];
//            [rightUtilityButtons sw_addUtilityButtonWithColor:
//             [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
//                                                        title:@"Delete"];
//
//        }
//         SWTableViewCell *cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
//                                      reuseIdentifier:@"MyIdentifier"
//                                  containingTableView:tableView // For row height and selection
//                                   leftUtilityButtons:leftUtilityButtons
//                                  rightUtilityButtons:rightUtilityButtons];
//        cell.delegate = self;
//        
//        //        if (cell == nil)
//        //        {
//        
//        // cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//       // UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, tableView.frame.size.width/4,tableView.rowHeight)]  ;
//        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 500,30)]  ;
//
//        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/4, 10, tableView.frame.size.width/4,tableView.rowHeight)]  ;
//        UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake((tableView.frame.size.width/4)*2, 10, tableView.frame.size.width/4,tableView.rowHeight)]  ;
//        UIView *view4 = [[UIView alloc] initWithFrame:CGRectMake((tableView.frame.size.width/4)*3, 10, tableView.frame.size.width/4,tableView.rowHeight)]  ;
//        
//        companyNameLabel=nil;
//       // companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, view1.frame.size.width-10,tableView.rowHeight)]  ;
//        
//        companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, 490,44)]  ;
//
//        [companyNameLabel setText:NSLocalizedString(@"Ref. No", nil)];
//        [companyNameLabel setBackgroundColor:[UIColor clearColor]];
//        
//        [view1 addSubview:companyNameLabel];
//        
//        companyNameLabel1=nil;
//        companyNameLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, view2.frame.size.width-10,tableView.rowHeight)]  ;
//        [companyNameLabel1 setText:NSLocalizedString(@"Customer Name", nil)];
//        [companyNameLabel1 setBackgroundColor:[UIColor clearColor]];
//        
//        [view2 addSubview:companyNameLabel1];
//        
//        companyNameLabel2=nil;
//        companyNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, view3.frame.size.width-10,tableView.rowHeight)]  ;
//        [companyNameLabel2 setText:NSLocalizedString(@"Order Amount", nil)];
//        [companyNameLabel2 setBackgroundColor:[UIColor clearColor]];
//        
//        [view3 addSubview:companyNameLabel2];
//        
//        companyNameLabel3=nil;
//        companyNameLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, view4.frame.size.width-10,tableView.rowHeight)]  ;
//        [companyNameLabel3 setText:NSLocalizedString(@"Date", nil)];
//        [companyNameLabel3 setBackgroundColor:[UIColor clearColor]];
//        
//        [view4 addSubview:companyNameLabel3];
//        
//        [cell.contentView addSubview:view1];
//        [cell.contentView addSubview:view2];
//        [cell.contentView addSubview:view3];
//        [cell.contentView addSubview:view4];
//        
//        //}
//        if (indexPath.row==0)
//        {
//            cell.contentView.backgroundColor = UIColorFromRGB(0xE0E5EC);
//            companyNameLabel.font = BoldSemiFontOfSize(16);
//            companyNameLabel1.font = BoldSemiFontOfSize(16);
//            companyNameLabel2.font = BoldSemiFontOfSize(16);
//            companyNameLabel3.font = BoldSemiFontOfSize(16);
//            
//            companyNameLabel.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel1.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel2.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel3.textAlignment = NSTextAlignmentLeft;
//            
//            [companyNameLabel setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//            [companyNameLabel1 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//            [companyNameLabel2 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//            [companyNameLabel3 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
//            
//        }
//        else
//        {
//            //NSLog(@"Row %@",[[types objectAtIndex:indexPath.row-1] stringForKey:@"Orig_Sys_Document_Ref"]);
//            cell.contentView.backgroundColor = [UIColor clearColor];
//            
//            [companyNameLabel setBackgroundColor:[UIColor clearColor]];
//            [companyNameLabel1 setBackgroundColor:[UIColor clearColor]];
//            [companyNameLabel2 setBackgroundColor:[UIColor clearColor]];
//            [companyNameLabel3 setBackgroundColor:[UIColor clearColor]];
//            
//            companyNameLabel.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel1.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel2.textAlignment = NSTextAlignmentLeft;
//            companyNameLabel3.textAlignment = NSTextAlignmentLeft;
//            
//            companyNameLabel.font = LightFontOfSize(14.0f);
//            companyNameLabel1.font = LightFontOfSize(14.0f);
//            companyNameLabel2.font = LightFontOfSize(14.0f);
//            companyNameLabel3.font = LightFontOfSize(14.0f);
//            
//            [companyNameLabel setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Orig_Sys_Document_Ref"]];
//            [companyNameLabel1 setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Customer_Name"]];
//            [companyNameLabel2 setText:[[[types objectAtIndex:indexPath.row-1] stringForKey:@"Order_Amt"] currencyString]];
//            [companyNameLabel3 setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Creation_Date"]];
//            
//        }
        
        //        UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, headerView.bounds.size.width, 35)]  ;
        //        [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        //        [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
        //        [companyNameLabel setBackgroundColor:[UIColor clearColor]];
        //        [companyNameLabel setTextColor:[UIColor darkGrayColor]];
        //        [companyNameLabel setShadowColor:[UIColor whiteColor]];
        //        [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
        //        [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];
        
        
        
        
        UnConfirmedOrderTableViewCell *cell = (UnConfirmedOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"unconfirmedCell"];
        
              
                    if(cell == nil)
                    {
                        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UnConfirmedOrderTableViewCell" owner:self options:nil];
                        cell = [nib objectAtIndex:0];
                    }

        cell.customerNameLbl.text= [[types objectAtIndex:indexPath.row] valueForKey:@"Customer_Name"];
        cell.orderAmount.text=[[[types objectAtIndex:indexPath.row]valueForKey:@"Order_Amt"]stringValue];
        cell.refNumberLbl.text=[[types objectAtIndex:indexPath.row]valueForKey:@"Orig_Sys_Document_Ref"];
        cell.dateLbl.text=[[types objectAtIndex:indexPath.row]valueForKey:@"Creation_Date"];
        
        return cell;
    }

    
    /*   @autoreleasepool
    {
       
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
//        cell=nil;
//        
           UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];

//        if (cell == nil)
//        {
        
           // cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width/4,tableView.rowHeight)]  ;
            UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.size.width/4, 0, tableView.frame.size.width/4,tableView.rowHeight)]  ;
            UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake((tableView.frame.size.width/4)*2, 0, tableView.frame.size.width/4,tableView.rowHeight)]  ;
            UIView *view4 = [[UIView alloc] initWithFrame:CGRectMake((tableView.frame.size.width/4)*3, 0, tableView.frame.size.width/4,tableView.rowHeight)]  ;
            
            companyNameLabel=nil;
            companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, view1.frame.size.width-10,tableView.rowHeight)]  ;
            [companyNameLabel setText:NSLocalizedString(@"Ref. No", nil)];
            [companyNameLabel setBackgroundColor:[UIColor clearColor]];

            [view1 addSubview:companyNameLabel];
            
            companyNameLabel1=nil;
            companyNameLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, view2.frame.size.width-10,tableView.rowHeight)]  ;
            [companyNameLabel1 setText:NSLocalizedString(@"Customer Name", nil)];
            [companyNameLabel1 setBackgroundColor:[UIColor clearColor]];

            [view2 addSubview:companyNameLabel1];
            
            companyNameLabel2=nil;
            companyNameLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, view3.frame.size.width-10,tableView.rowHeight)]  ;
            [companyNameLabel2 setText:NSLocalizedString(@"Order Amount", nil)];
            [companyNameLabel2 setBackgroundColor:[UIColor clearColor]];

            [view3 addSubview:companyNameLabel2];
            
            companyNameLabel3=nil;
            companyNameLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, view4.frame.size.width-10,tableView.rowHeight)]  ;
            [companyNameLabel3 setText:NSLocalizedString(@"Date", nil)];
            [companyNameLabel3 setBackgroundColor:[UIColor clearColor]];

            [view4 addSubview:companyNameLabel3];

            [cell.contentView addSubview:view1];
            [cell.contentView addSubview:view2];
            [cell.contentView addSubview:view3];
            [cell.contentView addSubview:view4];
           
        //}
        if (indexPath.row==0)
        {
            cell.contentView.backgroundColor = UIColorFromRGB(0xE0E5EC);
            companyNameLabel.font = BoldSemiFontOfSize(16);
            companyNameLabel1.font = BoldSemiFontOfSize(16);
            companyNameLabel2.font = BoldSemiFontOfSize(16);
            companyNameLabel3.font = BoldSemiFontOfSize(16);
            
            companyNameLabel.textAlignment = NSTextAlignmentLeft;
            companyNameLabel1.textAlignment = NSTextAlignmentLeft;
            companyNameLabel2.textAlignment = NSTextAlignmentLeft;
            companyNameLabel3.textAlignment = NSTextAlignmentLeft;
            
            [companyNameLabel setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
            [companyNameLabel1 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
            [companyNameLabel2 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];
            [companyNameLabel3 setBackgroundColor:UIColorFromRGB(0xE0E5EC)];

        }
        else
        {
            NSLog(@"Row %@",[[types objectAtIndex:indexPath.row-1] stringForKey:@"Orig_Sys_Document_Ref"]);
            cell.contentView.backgroundColor = [UIColor clearColor];
            
            [companyNameLabel setBackgroundColor:[UIColor clearColor]];
            [companyNameLabel1 setBackgroundColor:[UIColor clearColor]];
            [companyNameLabel2 setBackgroundColor:[UIColor clearColor]];
            [companyNameLabel3 setBackgroundColor:[UIColor clearColor]];
            
            companyNameLabel.textAlignment = NSTextAlignmentLeft;
            companyNameLabel1.textAlignment = NSTextAlignmentLeft;
            companyNameLabel2.textAlignment = NSTextAlignmentLeft;
            companyNameLabel3.textAlignment = NSTextAlignmentLeft;
            
            companyNameLabel.font = LightFontOfSize(14.0f);
            companyNameLabel1.font = LightFontOfSize(14.0f);
            companyNameLabel2.font = LightFontOfSize(14.0f);
            companyNameLabel3.font = LightFontOfSize(14.0f);
            
            [companyNameLabel setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Orig_Sys_Document_Ref"]];
            [companyNameLabel1 setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Customer_Name"]];
            [companyNameLabel2 setText:[[[types objectAtIndex:indexPath.row-1] stringForKey:@"Order_Amt"] currencyString]];
            [companyNameLabel3 setText:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Creation_Date"]];
            
        }

//        UILabel *companyNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, headerView.bounds.size.width, 35)]  ;
//        [companyNameLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
//        [companyNameLabel setText:NSLocalizedString(@"Filter", nil)];
//        [companyNameLabel setBackgroundColor:[UIColor clearColor]];
//        [companyNameLabel setTextColor:[UIColor darkGrayColor]];
//        [companyNameLabel setShadowColor:[UIColor whiteColor]];
//        [companyNameLabel setShadowOffset:CGSizeMake(0, -1)];
//        [companyNameLabel setFont:BoldSemiFontOfSize(18.0f)];

        
        return cell;
    }*/
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSMutableDictionary *row = [types objectAtIndex:indexPath.row];

    if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
        [selectedCell  setAccessoryType:UITableViewCellAccessoryCheckmark];
        [selectedOrders addObject:row];
    }
    else
    {
        [selectedOrders removeObject:row];
        [selectedCell  setAccessoryType:UITableViewCellAccessoryNone];
    }
    
}

- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict
{
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID])
        {
            [SWDefaults setProductCategory:row];
            
            break;
        }
    }
    
    

    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:cutomerDict];
    newSalesOrder.parentViewString = @"MO";
    newSalesOrder.preOrderedItems=[NSMutableArray new];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
    newSalesOrder.isOrderConfirmed=@"notconfirmed";

    [self.navigationController pushViewController:newSalesOrder animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
        {
            
            // Manage button was pressed
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            
            NSArray *tempArray = [[SWDatabaseManager retrieveManager]dbGetCustomerShipAddressDetailsByCustomer_ID:[[types objectAtIndex:indexPath.row-1]stringForKey:@"Ship_To_Customer_ID"] SiteUse_ID:[[types objectAtIndex:indexPath.row-1]stringForKey:@"Ship_To_Site_ID"]];
            //NSLog(@"Customer Array %@",tempArray);
            Singleton *single=[Singleton retrieveSingleton];
            single.visitParentView=@"MO";
            single.orderStartDate = [NSDate date];
            single.cashCustomerDictionary = [NSMutableDictionary new];
            [single.cashCustomerDictionary setValue:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Credit_Customer_ID"] forKey:@"Customer_ID"];
            [single.cashCustomerDictionary setValue:[[types objectAtIndex:indexPath.row-1] stringForKey:@"Credit_Site_ID"] forKey:@"Site_Use_ID"];
            //NSLog(@"Credit_Site_ID %@",single.cashCustomerDictionary);
            
            cutomerDict = [NSMutableDictionary dictionaryWithDictionary:[tempArray objectAtIndex:0]];

            NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionaryWithDictionary:[tempArray objectAtIndex:0]];
            NSString *shipCustomer = [tempDictionary stringForKey:@"Customer_ID"];
            NSString *shipSite = [tempDictionary stringForKey:@"Site_Use_ID"];
            [tempDictionary setValue:shipCustomer forKey:@"Ship_Customer_ID"];
            [tempDictionary setValue:shipSite forKey:@"Ship_Site_Use_ID"];
            NSString *savedCategoryID = [[types objectAtIndex:indexPath.row-1] stringForKey:@"Custom_Attribute_2"];
            [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:tempDictionary] andCategoryName:savedCategoryID andOrderDictionary:[types objectAtIndex:indexPath.row-1]];

        }
            break;
        case 1:
        {
            // Delete button was pressed
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            [[SWDatabaseManager retrieveManager] deleteManageOrderWithRef:[[types objectAtIndex:cellIndexPath.row-1] stringForKey:@"Orig_Sys_Document_Ref"]];
            [types removeObjectAtIndex:cellIndexPath.row-1];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath]
                                      withRowAnimation:UITableViewRowAnimationLeft];


        }
            break;
        default:
            break;
    }
}

@end
