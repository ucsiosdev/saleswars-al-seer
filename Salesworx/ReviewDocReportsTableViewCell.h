//
//  ReviewDocReportsTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface ReviewDocReportsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *documentNoLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *documentTypeLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *dateLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *amountLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *nameLabel;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *statusLabel;

@end
