//
//  CustomersListViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"
#import "SWLoadingView.h"
#import "ZBarSDK.h"
#import "Singleton.h"





@interface CustomersListViewController : SWViewController < UITableViewDataSource, UITableViewDelegate,  UIPopoverControllerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate,UISearchBarDelegate> {//UISearchDisplayDelegate, UISearchBarDelegate,
    
    
    
    NSMutableArray *customerList;
    
    UITableView *tableView;
    UISearchBar *searchBar;
    NSMutableArray *searchData;
    NSMutableDictionary *customerDict;
    
    UIPopoverController *locationFilterPopOver;
    SEL action;
    NSMutableDictionary *sectionList;
    NSMutableDictionary *sectionSearch;
    
    SWLoadingView *loadingView;
    UILabel *infoLabel;
    ZBarReaderViewController *reader;
    
    UIBarButtonItem *flexibleSpace ;
    UIBarButtonItem *totalLabelButton;
    UIBarButtonItem *displayActionBarButton ;
    UIBarButtonItem *displayMapBarButton ;
    UIView* myBackgroundView;
}

@property (nonatomic) BOOL isPendingNotificationTapped;
@property (nonatomic) BOOL isFromReport;

//@property (nonatomic, strong)  NSMutableDictionary *customerDict;

//@property (nonatomic, strong) NSMutableArray *customerList;
//@property (nonatomic, strong) NSMutableArray *searchData;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end

