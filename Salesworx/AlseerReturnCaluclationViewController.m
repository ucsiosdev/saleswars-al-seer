//
//  AlseerReturnCaluclationViewController.m
//  Salesworx
//
//  Created by Pavan Kumar Singamsetti on 2/26/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlseerReturnCaluclationViewController.h"
#import "AlSeerReturnAdditionalViewController.h"
#import "TblCellProductDetails.h"

@interface AlseerReturnCaluclationViewController (){
    NSString *currencyCode;
    NSString *totalPrice ;
    float temp1;
    float temp2;
    float temp3;
    float temp4;
    //float temp5;
}

@end

@implementation AlseerReturnCaluclationViewController
@synthesize delegate;
- (id)initWithCustomer:(NSDictionary *)customerDict andCategory:(NSDictionary *)categoryDict andViewController:(UIViewController *)returnVC
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:customerDict];
        category = [NSMutableDictionary dictionaryWithDictionary:categoryDict];
        [self setTitle:NSLocalizedString(@"Create Returns", nil)];
        Singleton *single = [Singleton retrieveSingleton];
        single.isSaveOrder = NO;
        productDict = [NSMutableDictionary dictionary ];
        
        
        productListViewController = [[ProductListViewController alloc] initWithCategory:category] ;
        [productListViewController setTarget:self];
        [productListViewController setAction:@selector(productSelected:)];
        navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
        
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(presentProductList:)] ];
        
        CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
        [headerView setShouldHaveMargins:YES];
        //[self.gridView.tableView setTableHeaderView:headerView];
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
        [self.gridView setShouldAllowDeleting:YES];
        
        //add Save Returns button
        UIBarButtonItem *btnSaveReturns = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save Returns", nil) style:UIBarButtonItemStyleDone target:self action:@selector(saveOrder:)];
        
        [btnSaveReturns setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                      nil]
                            forState:UIControlStateNormal];
        
        
        returnVC.navigationItem.rightBarButtonItem = btnSaveReturns;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    temp1 =  52.8f;;
    temp2 = 15.0;
    temp3 = 15.0;
    temp4 = 16.5f;
    
    
    
    currencyCode = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
    totalPrice = NSLocalizedString(@"Total Price", nil);
    self.view.backgroundColor = [UIColor clearColor];
    
    int xTemp = 0;
    int yTemp = 40;
    int widthTemp = self.view.bounds.size.width;
    int heightTemp = self.view.bounds.size.height-100;
    
    gridView.frame=CGRectMake(xTemp, yTemp, widthTemp, heightTemp);
    
    
    gridView.backgroundColor = [UIColor whiteColor];
    
    UILabel* bgLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 800, 40)];
    [bgLbl setBackgroundColor:[UIColor whiteColor]];//UIColorFromRGB(0xE0E5EC)];
    [self.view addSubview:bgLbl];
    
    
    UILabel *orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(13, 0, 133, 40)] ;
    //totalProduct=[[UILabel alloc] initWithFrame:CGRectMake(130, 0, 100, 40)] ;
    
    //UILabel *orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)] ;
    [orderItemLbl setText:@"Return Items"];
    [orderItemLbl setTextAlignment:NSTextAlignmentLeft];
    [orderItemLbl setBackgroundColor:[UIColor whiteColor]];//UIColorFromRGB(0xE0E5EC)];
    orderItemLbl.font = kFontWeblySleekSemiLight(16);
    orderItemLbl.textColor = UIColorFromRGB(0x2C394A);
    [self.view addSubview:orderItemLbl];
    
    totalProduct=[[UILabel alloc] initWithFrame:CGRectMake(109, 9.5, 33, 22)] ;
    totalProduct.backgroundColor = UIColorFromRGB(0x169C5C); //[UIColor colorWithRed:22.0/255.0 green:156.0/255.0 blue:92.0/255.0 alpha:1.0];
    totalProduct.layer.cornerRadius = 9.0;
    totalProduct.textAlignment = NSTextAlignmentCenter;
    totalProduct.layer.masksToBounds = YES;
    [totalProduct setTextColor:[UIColor whiteColor]];
    [totalProduct setFont:kFontWeblySleekSemiBold(14)];
    [totalProduct setText:@"0"];
    [self.view addSubview:totalProduct];
    
    int xTemp_2 = 439.40 ;
    int yTemp_2 = 4;
    int widthTemp_2 = 300;
    int heightTemp_2 = 30;
    
    
    totalPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(xTemp_2,yTemp_2, widthTemp_2, heightTemp_2)];
    totalPriceLabel.hidden = YES;
    totalPriceLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
   
    [bgLbl addSubview:totalPriceLabel];
 
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [Flurry logEvent:@"Return Order View"];
    
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: gridView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    gridView.layer.mask = maskLayer;
}

- (void)presentProductList:(id)sender
{
    [SWDefaults setProductCategory:category];
    productListViewController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
}

#pragma mark ProductListView Controller action
- (void)productSelected:(NSDictionary *)product {
    productDict = [NSMutableDictionary dictionaryWithDictionary:product];
    [self getProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[product stringForKey:@"ItemID"] :[customer stringForKey:@"Price_List_ID"]]];
}

- (void)presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"])
    {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }
    
    
    orderViewController = [[ProductReturnViewController alloc] initWithProduct:product] ;
    [orderViewController setTarget:self];
    [orderViewController setAction:@selector(productAdded:)];
    orderViewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:orderViewController animated:YES];
    orderViewController = nil;
}

- (void)getProductServiceDidGetCheckedPrice:(NSArray *)priceDetail
{
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customer stringForKey:@"Price_List_ID"]])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.0f];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            [self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.0f];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
    }
    priceDetail=nil;
}
- (void)productAdded:(NSMutableDictionary *)q{
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:q  ];
    
    for (int i = array.count - 1; i >= 0; i--)
    {
        @autoreleasepool{
            NSDictionary *data = [array objectAtIndex:i];
            if ([[data objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
            {
                [array removeObjectAtIndex:i];
            }
            else if ([[data objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
            {
                [array removeObjectAtIndex:i];
            }
        }
    }
    [array addObject:p];
    if ([p stringForKey:@"Bonus"].length > 0)
    {
        if ([[p objectForKey:@"Bonus"] intValue] > 0)
        {
            NSMutableDictionary *b = [NSMutableDictionary dictionaryWithDictionary:p];
            [b setValue:[NSString createGuid] forKey:@"Guid"];
            [b setValue:@"0" forKey:@"Price"];
            [b setValue:[p stringForKey:@"Bonus"] forKey:@"Qty"];
            [b setValue:@"0" forKey:@"Bonus"];
            [b removeObjectForKey:@"StockAllocation"];
            [p setValue:[b stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [b setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [array removeLastObject];
            [array addObject:p];
            [array addObject:b];
        }
    }
    [self setItems:array];
    [self.gridView reloadData];
    [self updateTotal];
    [totalProduct setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.items.count]];
    
    [gridView.tableView reloadData];
    NSInteger lastRowNumber = [gridView.tableView  numberOfRowsInSection:0] - 1;
    NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
    [gridView.tableView  scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)closeOrder {
    if([self.items count] > 0 )
    {
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isSaveOrder)
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
            [self.navigationController setToolbarHidden:YES animated:YES];
            
        }
        else
        {
            UIAlertAction *yesAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController  popViewControllerAnimated:YES];
                [self.navigationController setToolbarHidden:YES animated:YES];
            }];
            
            UIAlertAction *noAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Would you like to cancel the sales return without saving?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
        }
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

- (void)updateTotal {
    double price = 0.0f;
    for (int i = 0;i < self.items.count; i++)
    {
        @autoreleasepool{
            NSDictionary *data = [self.items objectAtIndex:i];
            if ([[data valueForKey:@"Price"] isEqualToString:@"0"]) {
                //bonus item
            }
            else
            {
                double currentPrice = [[data objectForKey:@"Net_Price"] doubleValue];
                currentPrice=currentPrice*[[data objectForKey:@"Qty"] doubleValue];
                price=price+currentPrice;
            }
        }
    }
    totalOrderAmount = price;
    [self setTotalPriceWithMutString:[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]];
    totalPriceLabel.hidden = NO;
}

-(void)setTotalPriceWithMutString:(NSString*)amountStr {
    
    if (amountStr==nil || amountStr.length==0){
        amountStr = [[NSString stringWithFormat:@"%.2f",0.00] currencyString];
    }
    
    UIColor *normalColor = UIColorFromRGB(0x6A6F7B);
    UIColor *highlightColor = UIColorFromRGB(0x2C394A);
    UIFont *font = kFontWeblySleekSemiBold(16);
    NSDictionary *normalAttributes = @{NSFontAttributeName:font, NSForegroundColorAttributeName:normalColor};
    NSDictionary *highlightAttributes = @{NSFontAttributeName:font, NSForegroundColorAttributeName:highlightColor};
    
    NSAttributedString *normalText = [[NSAttributedString alloc] initWithString:@"Total Price: " attributes:normalAttributes];
    
    NSAttributedString *highlightedText = [[NSAttributedString alloc] initWithString:amountStr attributes:highlightAttributes];
    
    NSMutableAttributedString *finalAttributedString = [[NSMutableAttributedString alloc] initWithAttributedString:normalText];
    [finalAttributedString appendAttributedString:highlightedText];
    totalPriceLabel.textAlignment = NSTextAlignmentRight;
    totalPriceLabel.attributedText = finalAttributedString;
}

- (void)saveOrder:(id)sender {
    
    if (self.items.count>0) {
        NSMutableDictionary *salesOrder = [NSMutableDictionary dictionary];
        [salesOrder setValue:self.items forKey:@"OrderItems"];
        
        NSMutableDictionary *info = [NSMutableDictionary dictionary];
        [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
        [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
        [salesOrder setValue:info forKey:@"info"];
        
        AlSeerReturnAdditionalViewController* returnAdditionalInfo=[[AlSeerReturnAdditionalViewController alloc]init];
        returnAdditionalInfo.salesOrderDict=salesOrder;
        returnAdditionalInfo.customerDict=customer;
        returnAdditionalInfo.arrReturnItem=[self.items mutableCopy];
        [self.navigationController pushViewController:returnAdditionalInfo animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please add item." withController:self];
    }
}
#pragma mark GridView DataSource
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return self.items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return temp1;
            break;
        case 1:
            return temp2;
            break;
        case 2:
            return temp3;
            break;
        case 3:
            return temp4;
            break;
            
        default:
            break;
    }
    return 10.0f;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"  Name", nil);
            break;
        case 1:
            title = NSLocalizedString(@"UOM", nil);
            break;

        case 2:
            title = NSLocalizedString(@"Quantity", nil);
            break;
        case 3:
            title = [NSString stringWithFormat:@"%@ (%@)",totalPrice,currencyCode];
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [self.items objectAtIndex:row];
    
    float netPrice =[[data stringForKey:@"Net_Price"] floatValue];
    float qty=[[data stringForKey:@"Qty"] floatValue];
    float totalReturnPrice=netPrice*qty;
    
    switch (column) {
        case 0:
            text = [NSString stringWithFormat:@"  %@",[data stringForKey:@"Description"]] ;
            break;
        case 1:
            text = [data stringForKey:@"SelectedUOM"];
            break;
        case 2:
            text = [data stringForKey:@"Qty"];
            break;
        case 3:
            if ([[data valueForKey:@"Price"] isEqualToString:@"0"]) {
                text=[NSString stringWithFormat:@"%0.2f",[[data valueForKey:@"Price"] floatValue]];
            }
            else {
                text=[NSString stringWithFormat:@"%0.2f", totalReturnPrice];
            }
            break;
        default:
            break;
    }
    return text;
}

#pragma mark GridView Delegate
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{
    if([[[self.items objectAtIndex:rowIndex] valueForKey:@"Price"] isEqualToString:@"0"])
    {
        if (![[self.items objectAtIndex:rowIndex-1] objectForKey:@"Guid"])
        {
            [[self.items objectAtIndex:rowIndex-1] setValue:[NSString createGuid] forKey:@"Guid"];
        }
        [self.delegate selectedProduct:[self.items objectAtIndex:rowIndex-1]];
    }
    else {
        if (![[self.items objectAtIndex:rowIndex] objectForKey:@"Guid"])
        {
            [[self.items objectAtIndex:rowIndex] setValue:[NSString createGuid] forKey:@"Guid"];
        }
        [self.delegate selectedProduct:[self.items objectAtIndex:rowIndex]];
    }
}

- (void)gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex {
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    NSDictionary *product = [array objectAtIndex:rowIndex];
    
    if(![[product valueForKey:@"Price"] isEqualToString:@"0"])
    {
        if ([product objectForKey:@"ParentGuid"])
        {
            for (int i = 0;i < array.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *data = [array objectAtIndex:i];
                    if ([[data objectForKey:@"Guid"] isEqualToString:[product objectForKey:@"ParentGuid"]])
                    {
                        [data setValue:@"0" forKey:@"Bonus"];
                        [array replaceObjectAtIndex:i withObject:data];
                        [self.gridView reloadRowAtIndex:i];
                        break;
                    }
                }
            }
        }
        else if ([product objectForKey:@"ChildGuid"])
        {
            for (int i = 0;i < array.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *data = [array objectAtIndex:i];
                    if ([[data objectForKey:@"Guid"] isEqualToString:[product objectForKey:@"ChildGuid"]])
                    {
                        [array removeObjectAtIndex:i];
                        [self setItems:array];
                        [self.gridView deleteRowAtIndex:i];
                        break;
                    }
                }
            }
        }
        [self.delegate selectedProduct:nil];
        [array removeObjectAtIndex:rowIndex];
        [self setItems:array];
        [gv deleteRowAtIndex:rowIndex];
        [self updateTotal];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus product can not be deleted" withController:self];
    }
    [totalProduct setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.items.count]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end
