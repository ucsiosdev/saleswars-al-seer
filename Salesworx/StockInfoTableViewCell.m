//
//  StockInfoTableViewCell.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/1/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "StockInfoTableViewCell.h"
#import "SWFoundation.h"

@implementation StockInfoTableViewCell
@synthesize lotNumberLbl,quantityLbl,wareHouseLbl,expiryLbl,allocatedTextField;
- (void)awakeFromNib
{
    // Initialization code
    
    //lotNumberLbl=[[UILabel alloc]init];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    
//    [allocatedLabel setTextColor:[UIColor whiteColor]];
//    [lotNumberLbl setFont:LightFontOfSize(14.0f)];
//   [lotNumberLbl setShadowColor:[UIColor blackColor]];
//    
//    
//    [quantityLbl setFont:LightFontOfSize(14.0f)];
//    [quantityLbl setShadowColor:[UIColor blackColor]];
//    
//    [wareHouseLbl setFont:LightFontOfSize(14.0f)];
//    [wareHouseLbl setShadowColor:[UIColor blackColor]];
//    
//    [expiryLbl setFont:LightFontOfSize(14.0f)];
//    [expiryLbl setShadowColor:[UIColor blackColor]];
    
    // Configure the view for the selected state
}



#pragma mark Text Field Delegate methods



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    /* for backspace */
    if([string length]==0){
        return YES;
    }
    
    /*  limit to only numeric characters  */
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            return YES;
        }
    }
    
    return NO;
}

@end
