//
//  ProductBrandFilterViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductBrandFilterViewController.h"
#import "SWDefaults.h"

@interface ProductBrandFilterViewController ()

@end

@implementation ProductBrandFilterViewController
//@synthesize serProduct;
@synthesize filters;
@synthesize target;
@synthesize action;

- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        [self setTitle:NSLocalizedString(@"Product Filter", nil)];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupView];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(selectionDone:)] ;
    
    [close setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = close;
    
    
    UIBarButtonItem *clear = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter:)];
    
    [clear setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = clear;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [self getProductServiceDidGetFilter:[[SWDatabaseManager retrieveManager] dbGetFilterByColumn:@"Brand_Code"]];
    Singleton *single = [Singleton retrieveSingleton];
    single.isBarCode = NO;
}

-(void)setupView{
   
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
}
- (void)selectionDone:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUD_IS_PRODUCT_LIST_FILTER_ON];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    Singleton *single = [Singleton retrieveSingleton];
    if(single.isBarCode)
    {
        [SWDefaults setFilterForProductList:NSLocalizedString(@"Item Code", nil)];
        NSString *filterTitle = single.valueBarCode;
        [SWDefaults setProductFilterProductID:filterTitle];
    }
    else 
    {
        if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)] || [[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)] )
        {
            [((SWTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]) resignResponder];
        }
    }
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;
}

- (void)clearFilter:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kUD_IS_PRODUCT_LIST_FILTER_ON];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SWDefaults setFilterForProductList:nil];
    [SWDefaults setProductFilterProductID:nil];
    [SWDefaults setProductFilterName:nil];
    [SWDefaults setProductFilterBrand:nil];
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action]; 
                #pragma clang diagnostic pop;

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 
    if (section == 0)
    {
        return 3;
    }
    else
    {
        if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
        {
            return [self.filters count];
        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
        {
            return 1;
        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
        {
            return 1;
        }
        else 
        {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                [cell.textLabel setText:NSLocalizedString(@"Item Name", nil)];
            }
            else if (indexPath.row == 1)
            {
                [cell.textLabel setText:NSLocalizedString(@"Item Code", nil)];
            }
            else if (indexPath.row == 2)
            {
                [cell.textLabel setText:NSLocalizedString(@"Brand", nil)];
            }
            
            if ([cell.textLabel.text isEqualToString:[SWDefaults filterForProductList]])
            {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
        else
        {
            if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
            {
                NSDictionary *row = [self.filters objectAtIndex:indexPath.row];
                NSString *filterTitle = [row objectForKey:@"Brand_Code"];
                [cell.textLabel setText:filterTitle];
                
                if ([filterTitle isEqualToString:[SWDefaults productFilterBrand]])
                {
                    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
                else {
                    [cell setAccessoryType:UITableViewCellAccessoryNone];
                }
            }
            else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
            {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
                [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Item Code", nil)];
                [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
                [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Item Code", nil)];
                [((SWTextFieldCell *)cell) setDelegate:self];
                [((SWTextFieldCell *)cell) setKey:NSLocalizedString(@"Item Code", nil)];
                [((SWTextFieldCell *)cell).textField setText:[SWDefaults productFilterProductID]];
            }
            else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
            {
                cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
                [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Item Name", nil)];
                [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
                [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Item Name", nil)];
                [((SWTextFieldCell *)cell) setDelegate:self];
                [((SWTextFieldCell *)cell) setKey:NSLocalizedString(@"Item Name", nil)];
                [((SWTextFieldCell *)cell).textField setText:[SWDefaults productFilterName]];
            }
        }
        cell.textLabel.font =  kFontWeblySleekSemiBold(14.0f);
        cell.textLabel.textColor =  UIColorFromRGB(0x2C394A);
        
        return cell;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
        
        NSString *filterTitle = cell.textLabel.text;
        [SWDefaults setFilterForProductList:filterTitle];
        if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
        {
            [SWDefaults setProductFilterBrand:nil];
            [SWDefaults setProductFilterProductID:nil];

        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
        {
            [SWDefaults setProductFilterProductID:nil];
            [SWDefaults setProductFilterName:nil];
        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
        {
            [SWDefaults setProductFilterName:nil];
            [SWDefaults setProductFilterBrand:nil];
        }
    }
    else
    {
        if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
        {
            NSDictionary *row = [self.filters objectAtIndex:indexPath.row];
            NSString *filterTitle = [row objectForKey:@"Brand_Code"];
            [SWDefaults setProductFilterBrand:filterTitle];
        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
        {
            UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            NSString *filterTitle = cell.textLabel.text;         
            [SWDefaults setProductFilterName:filterTitle];
        }
        else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
        {
            UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
            
            NSString *filterTitle = cell.textLabel.text;
            [SWDefaults setProductFilterProductID:filterTitle];
        }
    }
    
    [self.tableView reloadData];
}
- (void)scrollCell:(EditableCell *)cell {
    [self.tableView scrollRectToVisible:cell.frame animated:YES];
}

#pragma mark EditCell Delegate

- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    [self performSelector:@selector(scrollCell:) withObject:cell afterDelay:0.4f];
}

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        NSString *filterTitle = value; 
        [SWDefaults setProductFilterName:filterTitle];
    }
    else if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        NSString *filterTitle = value;
        [SWDefaults setProductFilterProductID:filterTitle];
    }
}

#pragma mark Service Delegate
- (void)getProductServiceDidGetFilter:(NSArray *)f {
    [self setFilters:f];
    [self.tableView reloadData];
    f=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
