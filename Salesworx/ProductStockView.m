//
//  ProductStockView.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "ProductStockView.h"
#import "ProductHeaderView.h"
#import "GroupSectionHeaderView.h"
#import "SWPlatform.h"

@implementation ProductStockView


@synthesize delegate = _delegate,items,gridView,editMode,invalidAllocation;

- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        
        product=[NSDictionary dictionaryWithDictionary:p];
        
        
        self.backgroundColor = [UIColor whiteColor];
        
        items=[[NSArray array] mutableCopy];
        
        
        gridView=[[GridView alloc] initWithFrame:CGRectZero];
        //[gridView setFrame:CGRectMake(0, 60, self.bounds.size.width, self.bounds.size.height)];
        
        gridView.tableView.scrollEnabled=YES;
        
        
        [gridView setFrame:CGRectMake(0, 45, 30, 230)];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        [self addSubview:gridView];
        
        ProductHeaderView *productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, 550, 44)] ;
        productHeaderView.headingLabel.font=headerFont;
        [productHeaderView.headingLabel setText:@"Stock Information"];
        productHeaderView.backgroundColor=[UIColor colorWithRed:(217.0/255.0) green:(223.0/255.0) blue:(231.0/255.0) alpha:1];
        [self addSubview:productHeaderView];
        
        discountLable = [[UILabel alloc] initWithFrame:CGRectMake(800,12, 200, 32)] ;
        [discountLable setTextAlignment:NSTextAlignmentCenter];
        [discountLable setBackgroundColor:[UIColor clearColor]];
        [discountLable setTextColor:[UIColor darkGrayColor]];
        [discountLable setFont:RegularFontOfSize(18.0)];
        
        AppControl *appControl = [AppControl retrieveSingleton];
        
        isOnOrder = appControl.SHOW_ON_ORDER;
        NSString *onOrderQty = [[SWDatabaseManager retrieveManager] dbGetOnOrderQuantityOfProduct:[product stringForKey:@"ItemID"]];
        
        if([isOnOrder isEqualToString:@"Y"])
        {
            if(onOrderQty.length !=0)
            {
                discountLable.text = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"On Order Quantity", nil),onOrderQty ];
            }
            else
            {
                discountLable.text = [NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"On Order Quantity", nil)];
            }
            discountLable.hidden = NO;
        }
        else
        {
            discountLable.hidden = YES;
            discountLable.text = [NSString stringWithFormat:@"%@ : N/A",NSLocalizedString(@"On Order Quantity", nil)];
        }
        [productHeaderView addSubview:discountLable];
        
        
    }
    gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    return self;
}

-(void)loadStock
{
    [self getProductServiceDiddbGetStockInfo:[[SWDatabaseManager retrieveManager] dbGetStockInfo:[product stringForKey:@"ItemID"]]];
}



#pragma mark Product Service Delegate
- (void)getProductServiceDiddbGetStockInfo:(NSArray *)info {
    items=[NSMutableArray arrayWithArray:info];
    Singleton *single = [Singleton retrieveSingleton];
    
    
    if(![single.manageOrderRefNumber isEqualToString:@"SO"])
    {
        //Saad Lots
        [self getProductServiceDidGetStockManage:[[SWDatabaseManager retrieveManager] dbGetStockInfoForManage:single.manageOrderRefNumber]];
    }
    else
    {
        [gridView reloadData];
        if ([self.delegate respondsToSelector:@selector(productStockViewLoaded)])
        {
            [self.delegate productStockViewLoaded];
        }
    }
    info=nil;
}

-(void)getProductServiceDidGetStockManage:(NSArray *)info
{
    
    for (int i=0; i < [info count]; i++)
    {
        @autoreleasepool{
            NSMutableDictionary *a = [NSMutableDictionary dictionaryWithDictionary:[info objectAtIndex:i]] ;
            for (int j=0; j < [items count]; j++)
            {
                @autoreleasepool{
                    NSMutableDictionary *b = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:j]] ;
                    if([[a stringForKey:@"Lot_Number"] isEqualToString:[b stringForKey:@"Lot_No"]])
                    {
                        [b setValue:[a objectForKey:@"Ordered_Quantity"] forKey:@"Allocated"];
                        [items removeObjectAtIndex:j];
                        [items addObject:b];
                    }
                }
            }
        }
    }
    
    [gridView reloadData];
    info=nil;
    if ([self.delegate respondsToSelector:@selector(productStockViewLoaded)]) {
        [self.delegate productStockViewLoaded];
    }
}
#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    if (editMode) {
        return 5;
    }
    
    return 3;
}

- (int)indexOfColumnForInput:(GridView *)gridView {
    return 3;
}
/*
 @discussion
 The returned float value must be under 100 and it is represented as percentage.
 And all columns should be divided into 100%.
 */
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex
{
        switch (columnIndex) {
            case 0:
                return 25.0f;
                break;
            case 1:
                return 10.0f;
                break;
            case 2:
                return 20.0f;
                break;
            case 3:
                return 20.0f;
                break;
            case 4:
                return 25.0f;
                break;
            default:
                break;
        }
    return 20.0f;

}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    if (column == 0) {
        title = NSLocalizedString(@"  Lot No", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Qty.", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Expiry", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Blocked", nil);
    } else if (column == 4) {
        title = NSLocalizedString(@"Quality Inspection", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [items objectAtIndex:row];
    NSLog(@"data in grid view for stock allocation %@", [data description]);
    
    
    if (column == 0) {
        text =[NSString stringWithFormat:@"     %@", [data objectForKey:@"Lot_No"]];
    } else if (column == 1) {
        text = [data dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle];
    } else if (column == 2) {
        
        //adding uom support
        
        
        
        
        
        NSString* selectedUOMReference=[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedUOMRef"];
        
        
        NSLog(@"check selected uom ref in product stock %@", selectedUOMReference);
        
        
        
        NSString *trimmedString = [selectedUOMReference stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        

        
        if ([trimmedString isEqualToString:@"CS"]) {
            
        
        
        NSString* itemCodeforUOM=[product stringForKey:@"Item_Code"];
        NSString* selectedUom=trimmedString;
        
        NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@' ",itemCodeforUOM,selectedUom];
        
        NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
        
        NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
        
        
        
        if (consersionArr.count>0) {
            
            NSInteger lotQty=[[data stringForKey:@"Lot_Qty"] integerValue];
            
            NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
            
            
            NSInteger updatedLotQty= round( lotQty/conversion);
            
            
            //[mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];
            
            
            text=[NSString stringWithFormat:@"%d", updatedLotQty];
            
            
        }
        }

        
        else
        {
        
               text = [data stringForKey:@"Lot_Qty"];
        }
    } else if (column == 3) {
        text = [data stringForKey:@"Org_ID"];
    } else if (column == 4) {
        text = [data stringForKey:@"Allocated"];
    }
    return text;
}

- (NSString *)gridView:(GridView *)gridView keyForRow:(int)rowIndex {
    return [NSString stringWithFormat:@"%d", rowIndex];
}

- (void)gridView:(GridView *)gridViews didFinishInputForKey:(NSString *)key andValue:(id)value {
    NSInteger orderedQty=[[product valueForKey:@"Qty"] integerValue];
    
    if ([value integerValue]>orderedQty) {
        
        UIAlertView* invalidAlert=[[UIAlertView alloc]initWithTitle:@"Invalid Allocation" message:@"Allocated quantity cannot be greater than ordered quantity" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        invalidAlert.tag=1000;
        invalidAllocation=YES;
        [invalidAlert show];
    }
    else
    {
        invalidAllocation=NO;
        
        NSScanner *scanner = [NSScanner scannerWithString:value];
        BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
        
        if (isNumeric)
        {
            if ([value integerValue] > 0)
            {
                if ([self.delegate respondsToSelector:@selector(productStockViewDidFinishedEditingForRowIndex:)]) {
                    int rowIndex = [key intValue];
                    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:rowIndex]];
                    int availableStock = [[data objectForKey:@"Lot_Qty"] intValue];
                    if (availableStock >= [value intValue]) {
                        [data setValue:value forKey:@"Allocated"];
                    } else {
                        [data setValue:[data objectForKey:@"Lot_Qty"] forKey:@"Allocated"];
                        [gridView reloadRow:rowIndex];
                    }
                    [items replaceObjectAtIndex:rowIndex withObject:data];
                    [self.delegate productStockViewDidFinishedEditingForRowIndex:rowIndex];
                }
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please provide positive value only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]   show];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please provide positive value only." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil]   show];
        }
    }
}

@end
