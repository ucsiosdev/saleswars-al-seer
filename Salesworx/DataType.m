//
//  DataType.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/26/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "DataType.h"

@implementation DataType

@end

@implementation SurveyQuestion
@synthesize Question_ID;
@synthesize Question_Text;
@synthesize Survey_ID;
@synthesize Default_Response_ID;
@synthesize Response_Type_ID;
@end

@implementation SurveyResponse
@synthesize Response_ID;
@synthesize Response_Text;
@synthesize Question_ID;
@synthesize Response_Type_ID;
@end


@implementation TextQuestionAnswer
@synthesize Question_ID;
@synthesize Text;
@end

@implementation CheckBoxQuestionAnswer
@synthesize Question_ID;
@synthesize ResponseIdArray;
@end

@implementation radioQuestionAnswer
@synthesize Question_ID;
@synthesize Response_ID;
@end




@implementation MediaFile

@synthesize  Media_File_ID;
@synthesize Entity_ID_1;
@synthesize Entity_ID_2;
@synthesize Entity_Type;
@synthesize Media_Type;
@synthesize Filename;
@synthesize Caption;
@synthesize Thumbnail;
@synthesize Is_Deleted;
@synthesize Download_Flag;

@end
