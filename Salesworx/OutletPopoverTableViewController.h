//
//  OutletPopoverTableViewController.h
//  JcTennisTournament
//
//  Created by Syed Ismail Ahamed on 1/21/15.
//  Copyright (c) 2015 ucs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StringPopOverDelegate <NSObject>
@required
-(void)selectedStringValue:(id)value;
@end

@interface OutletPopoverTableViewController : UITableViewController


@property (nonatomic, strong) NSMutableArray *colorNames;
@property (nonatomic, weak) id<StringPopOverDelegate> delegate;

@end
