//
//  SWAppDelegate.m
//  Salesworx
//
//  Created by msaad on 5/9/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWAppDelegate.h"
#import "SWSession.h"
#import "MenuViewController.h"
#import "Flurry.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "LoginViewController.h"
#import "SWDefaults.h"
#import "SSKeychain.h"
#import "SWNewActivationViewController.h"
#import <IQKeyboardManager.h>
#import "AppControl.h"
#import "FMDBHelper.h"
#import "SharedUtils.h"
#import "AGPushNoteView.h"
#import "Reachability.h"
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>
#import "SalesWorxBackgroundSyncManager.h"
#import "SWSessionConstants.h"

@import Firebase;

// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)



#import "DashboardAlSeerViewController.h"
static NSString *ClientVersion = @"1";

@implementation SWAppDelegate

@synthesize window = _window;
@synthesize sessionManager,activationManager,licenseVC,itemCodeString,selectedMenuIndexpath, currentExceutingDataUploadProcess,isBackgroundMode;



- (void)applicationWillTerminate:(UIApplication *)application
{
    NSString* visitID=[SWDefaults currentVisitID];
    NSLog(@"visit id in will terminate %@", visitID);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
#if DEBUG
    NSLog(@"I'm running in a DEBUG mode");
#else
    [SWDefaults redirectLogToDocuments];
#endif
    
    [SWDefaults deleteOldDeviceLogFiles];
    currentExceutingDataUploadProcess=KDataUploadProcess_None;
    
    NSLog(@"username and password for activate %@ %@", [SWDefaults usernameForActivate],[SWDefaults passwordForActivate]);
   
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:NO];
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar : NO];
    [[IQKeyboardManager sharedManager] setShouldShowToolbarPlaceholder: NO];
    [[IQKeyboardManager sharedManager]setLayoutIfNeededOnUpdate:YES];
    [FIRApp configure];
    
    [SSKeychain setAccessibilityType:kSecAttrAccessibleAlways];
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"version_string"];
    [self updateVersionInfo];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:headerTitleFont, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    if (@available(iOS 15, *)){
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        appearance.backgroundColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1];
        appearance.titleTextAttributes = attributes;
        [UINavigationBar appearance].standardAppearance = appearance;
        [UINavigationBar appearance].scrollEdgeAppearance = appearance;
    } else {
        [[UINavigationBar appearance]setTitleTextAttributes:attributes];
        [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]];
        [[UINavigationBar appearance] setTranslucent:NO];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        if (@available(iOS 11.0, *)) {
            [[UINavigationBar appearance]setPrefersLargeTitles:NO];
        }
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
        [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    }
    
    self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    self.locationManager.distanceFilter=kCLDistanceFilterNone;
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
    
    
    // Do any additional setup after loading the view from its nib.

    
    [SWDefaults ClearLoginCredentials];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    if (SYSTEM_VERSION_LESS_THAN(@"10.0")) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( !error ) {
                // required to get the app to do anything at all about push notifications
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
                NSLog( @"Push registration success." );
            } else {
                NSLog( @"Push registration FAILED" );
                NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
            }
        }];
    }
    

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
 
    
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification)
    {
        [self decrementOneBdge];
    }

    NSLog(@"local notification object in did finsh launching with options %@", notification);
    [self fetchPushNotificationContent:notification];
    
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isFullSyncDone = NO;
    single.isFullSyncCollection = NO;
    single.isFullSyncCustomer = NO;
    single.isFullSyncSurvey = NO;
    single.isFullSyncProduct = NO;
    single.isFullSyncDashboard = NO;
    single.isFullSyncMessage = NO;
    single.isDistributionItemGet=YES;
    single.showCustomerDashboard=YES;
    single.databaseIsOpen = 0;
    
    [SWDefaults initializeDefaults];
    [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    selectedMenuIndexpath=[NSIndexPath indexPathForRow:0 inSection:0];

    //-------Get app version
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    
    //-------Set app version, if first launch
    if (![SWDefaults versionNumber] || [SWDefaults versionNumber].length == 0)
    {
        [SWDefaults setVersionNumber:avid];
    }

    
    //iOS 8 support
    NSString *applicationDocumentsDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        applicationDocumentsDir=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        applicationDocumentsDir = [paths objectAtIndex:0];
    }
    
    NSString *storePatha = [applicationDocumentsDir stringByAppendingPathComponent:@"Activation_Log.txt"];
    [@"" writeToFile:storePatha atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    NSString *storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"Sync_Log.txt"];
    [@"" writeToFile:storePath atomically:YES encoding:NSUTF8StringEncoding error:NULL];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];

    //-------Check for license
    if ([[SWDefaults licenseKey] count] == 0)
    {
        //-------For no license, push license controller
        
        licenseVC = [[LicensingViewController alloc] init] ;
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:licenseVC] ;
        [licenseVC setTarget:self];
        [licenseVC setAction:@selector(licenceDone)];
        [self.window setRootViewController:self.mainNavigationController];
    }
    else {
        //-------Check for user activation
        if ([[SWDefaults isActivationDone] isEqualToString:@"NO"]){
            
            //-------For no activation, push activation controller
            
            [self setActivationManager:[[SWNewActivationViewController alloc] initWithNoDB] ];
            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:self.activationManager] ;
            [activationManager setTarget:self];
            [activationManager setAction:@selector(activationDone)];
            [self.window setRootViewController:self.mainNavigationController];
        }
        else {
            
            DashboardAlSeerViewController *dashboardAlSeerViewController;
            if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
                
                dashboardAlSeerViewController = [[DashboardAlSeerViewController alloc] init];
                self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardAlSeerViewController];
                
                MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
                _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
                
                SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
                
                [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
                
                splitViewController=nil;
                menuViewController=nil;
                dashboardAlSeerViewController=nil;
            }
            else
            {
                dashboardAlSeerViewController = [[DashboardAlSeerViewController alloc] init];

                self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardAlSeerViewController];
                
                MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
                _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
                
                SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
                
                [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
                
                splitViewController=nil;
                menuViewController=nil;
                dashboardAlSeerViewController=nil;
            }
        }
    }
    
    CJSONDeserializer *dejsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableArray *testserverArray = [NSMutableArray arrayWithArray:[dejsonSerializer deserializeAsArray:data error:&error]  ] ;
    if([testserverArray count] == 0)
    
    {
        CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
        NSMutableArray *serverArray = [NSMutableArray array] ;
        NSMutableDictionary *row1 = [NSMutableDictionary dictionary] ;
        [row1 setValue:@"Demo Server" forKey:@"serverName"];
        [row1 setValue:@"demoapps.ddns.net:12008" forKey:@"serverLink"];
        //[row1 setValue:@"demo.ucspromotions.com:10108" forKey:@"serverLink"];
        
        NSData *dataDict = [jsonSerializer serializeObject:row1 error:&error];
        NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
        [SWDefaults setSelectedServerDictionary:stringDataDict];
        
        [serverArray addObject:row1];
       /* for (int i=1; i<=10; i++){
            @autoreleasepool {
            NSMutableDictionary *row = [NSMutableDictionary dictionary] ;
            [row setValue:[NSString stringWithFormat:@"Location %d",i] forKey:@"serverName"];
            [row setValue:[NSString stringWithFormat:@"192.168.100.%d:10008",i] forKey:@"serverLink"];
            [serverArray addObject:row];
            }
        }*/
        NSData *datas = [jsonSerializer serializeObject:serverArray error:&error];
        NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding] ;
        [SWDefaults setServerArray:stringData];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [SWDefaults clearCurrentVisitID];
    ClientVersion = avid;
    if (![[AppControl retrieveSingleton].FSR_LOC_TRK_FREQ isEqualToString:@"0"] && [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ != nil) {
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"HH:mm"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [outputFormatter setLocale:usLocale];
        NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
        NSDate *currentTime = [outputFormatter dateFromString:newDateString];

        NSDate *startTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_START_TIME];
        NSDate *endTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_END_TIME];
        
        [FIRAnalytics logEventWithName:@"TrackingDuration"
                            parameters:@{
                                         @"Minutes": [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ,
                                         @"StartTime": [AppControl retrieveSingleton].LOC_TRK_START_TIME,
                                         @"EndTime": [AppControl retrieveSingleton].LOC_TRK_END_TIME,
                                         @"customerID": [self customerId]}];
        
        BOOL canCaptureLocationData = [SWDefaults date:currentTime isBetweenDate:startTime andDate:endTime];
        
        if (canCaptureLocationData) {
            float milliseconds = [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ.floatValue;
            float seconds = milliseconds / 1000.0;
            if (seconds < 30) {
                seconds = 30; // this is done to stop issue during full sync
            }
            sendLocationTimer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(saveLocationData:) userInfo:nil repeats:YES];
        }
        // Suppose start time = 8 am.  If user runs app before start time and time becomes 8 am then saveLocationData method should call.
        else{
            checkLocationStartTime = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkStartLocationTime:) userInfo:[NSNumber numberWithBool: YES] repeats:YES];
        }
    }
    
    /**Background Sync*/
    // Add an observer that will respond to enableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableVTRXBackGroundSync:)
                                                 name:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
    
    // Add an observer that will respond to disableBackGroundSync
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disableVTRXBackGroundSync:)
                                                 name:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
    
    return YES;
}

- (void)checkStartLocationTime:(NSTimer *)theTimer{
   
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [outputFormatter setLocale:usLocale];
    NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
    //NSDate *currentTime = [outputFormatter dateFromString:newDateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];

    NSDate *currentTime= [formatter dateFromString:newDateString];
    NSDate *locationStartTime = [formatter dateFromString:@"08:00:00"];

    NSComparisonResult result = [currentTime compare:locationStartTime];
    
    if(result == NSOrderedAscending){
        NSLog(@"Current time is smaller than start location time (LOC_TRK_START_TIME)");
    }
    else if(result == NSOrderedSame ||result == NSOrderedDescending){
        
        NSLog(@"\n\n *** Hint: Current time is same or greater than start location time (LOC_TRK_START_TIME), Handling location saving... ***\n\n");
        BOOL isTimerFromdidFinishLaunchingWithOptions = [theTimer.userInfo boolValue];
        
        //first stop location start time checker NSTimer
        if ([checkLocationStartTime isValid]) {
            [checkLocationStartTime invalidate];
        }
        checkLocationStartTime = nil;
        
        //now start same logic for save location data
        float milliseconds = [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ.floatValue;
        float seconds = milliseconds / 1000.0;
        
        if (isTimerFromdidFinishLaunchingWithOptions) {
            
            if (seconds < 30) {
                seconds = 30; // this is done to stop issue during full sync
            }
        }
        sendLocationTimer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(saveLocationData:) userInfo:nil repeats:YES];
    }
    
 }

- (void)updateVersionInfo
{
    //This method updates the Root settings to display current Version and Build No in Settings Bundle
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *appVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSString *appBuildNo = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSString *versionNumberInSettings = [NSString stringWithFormat:@"%@ Build %@", appBuildNo, appVersionNumber];
    
    [defaults setObject:versionNumberInSettings forKey:@"version"];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
   // NSLog(@" location from app delegate %@", [locations lastObject]);
    
    self.currentLocation = [locations lastObject];
//    if (self.isBackgroundMode == YES) {
//        [self updateLocationInBackground];
//    }
       CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
       [geocoder reverseGeocodeLocation:self.currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
        {
            if (!(error))
            {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                NSLog(@"\nCurrent Location Detected\n");
                NSLog(@"placemark %@",placemark);
                NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                NSString *Address = [[NSString alloc]initWithString:locatedAt];
                NSString *Area = [[NSString alloc]initWithString:placemark.locality];
                NSString *Country = [[NSString alloc]initWithString:placemark.country];
                NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
                NSLog(@"%@",CountryArea);
                NSString *valueToSave = [[Area stringByAppendingString:@" "] stringByAppendingString:Country];
                [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"preferenceName"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                NSLog(@"Geocode failed with error %@", error);
                NSLog(@"\nCurrent Location Not Detected\n");
                //return;
               
            
            }
           
          
        }];

}

#pragma mark Update And Send Location Data in Background
-(void)updateLocationInBackground {
    
    if (![[AppControl retrieveSingleton].FSR_LOC_TRK_FREQ isEqualToString:@"0"] && [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ != nil) {
        if (backgroundTimer == nil) {
            NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
            [outputFormatter setDateFormat:@"HH:mm"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [outputFormatter setLocale:usLocale];
            NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
            NSDate *currentTime = [outputFormatter dateFromString:newDateString];
            
            NSDate *startTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_START_TIME];
            NSDate *endTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_END_TIME];
            
            BOOL canCaptureLocationData = [SWDefaults date:currentTime isBetweenDate:startTime andDate:endTime];
            
            if (canCaptureLocationData) {
                float milliseconds = [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ.floatValue;
                float seconds = milliseconds / 1000.0;
                if (seconds < 30) {
                    seconds = 30; // this is done to stop issue during full sync
                }
                //backgroundTimer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(saveLocationDataBackground) userInfo:nil repeats:YES];
            }
        }
    }
}

-(void)saveLocationDataBackground {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [outputFormatter setLocale:usLocale];
    NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
    NSLog(@"Time:---%@",newDateString);
    NSDate *currentTime = [outputFormatter dateFromString:newDateString];

    NSDate *startTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_START_TIME];
    NSDate *endTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_END_TIME];
    
    BOOL canCaptureLocationData = [SWDefaults date:currentTime isBetweenDate:startTime andDate:endTime];
    
    if ([[SWDefaults isActivationDone] isEqualToString:@"YES"]) {
        if (canCaptureLocationData) {
            
            BOOL success;
            FMDatabase *db = [FMDBHelper getDatabase];
            [db open];
            NSString *salesRepID = [NSString stringWithFormat:@"%@", [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
            
            
            SWAppDelegate *appDelegate = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
            CLLocationCoordinate2D coordinate = [appDelegate.currentLocation coordinate];
            NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
            NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
            
            if ([latitude isEqualToString:@"0"] || [longitude isEqualToString:@"0"] || latitude == nil || longitude == nil) {
                latitude = @"0.0";
                longitude = @"0.0";
            }
            
            NSString *gpsStatus = @"";
            if ([CLLocationManager locationServicesEnabled]) {
                if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
                {
                    // authorized
                    gpsStatus = @"Y";
                } else {
                    // permission issue
                    gpsStatus = @"P";
                }
            } else {
                // no GPS
                gpsStatus = @"N";
            }
            
            
            CLLocationAccuracy locationAccuracy = self.currentLocation.horizontalAccuracy;
            NSString *accuracy = [NSString stringWithFormat:@"%f", locationAccuracy];
            NSString *loggedAt = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
            
            NSString *customerID = @"";
            NSString *siteUseID = @"";
            NSString *visitID = @"";
            
            @try {
                success = [db executeUpdate:@"INSERT into TBL_FSR_Locations(Row_ID, SalesRep_ID, Activity_Type, Latitude, Longitude, Status, Accuracy, Customer_ID, Site_Use_ID, Visit_ID, Logged_At) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [NSString createGuid], salesRepID, @"", latitude, longitude, gpsStatus, accuracy, customerID, siteUseID, visitID, loggedAt, nil];
                
                if (success == YES) {
                    
                    NSLog(@"TBL_FSR_Locations insert successfull");
                    
                    BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
                    if (checkInternetConnectivity==YES) {
                        [self sendLocationDataToServer];
                    }
                }
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while inserting FSR Location data in database: %@", exception.reason);
            }
            @finally {
                [db close];
            }
        } else {
            NSMutableArray *locationArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Locations"]];
            
            if(locationArray.count > 0)
            {
                BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
                if (checkInternetConnectivity==YES) {
                    [self sendLocationDataToServer];
                }
            }
        }
    }
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    Singleton *single= [Singleton retrieveSingleton];
    if (single.databaseIsOpen<2)
    {
        single.databaseIsOpen++;
        mach_port_t host_port;
        mach_msg_type_number_t host_size;
        vm_size_t pagesize;
        
        host_port = mach_host_self();
        host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
        host_page_size(host_port, &pagesize);
        
        vm_statistics_data_t vm_stat;
        
        if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
            NSLog(@"Failed to fetch vm statistics");
        
        /* Stats in bytes */
        natural_t mem_free = vm_stat.free_count * pagesize;
        natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
        natural_t mem_total = mem_used + mem_free;
        
        int iUsed = round(mem_used/1048576);
        int iFree = round(mem_free/1048576);
        int iTotal = round(mem_total/1048576);
        NSLog(@"applicationDidReceiveMemoryWarning used: %d free: %d total: %d", iUsed, iFree, iTotal);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"MEMORY WARNING!!!", nil) message:[NSString stringWithFormat:@" Used Memory : %dmb \nFree Memory : %dmb \n Please save your work then kill and relaunch application.",iUsed,iFree] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];

    }
}


- (void)licenceDone {
    
    licenseVC.target=nil;
    licenseVC.action=nil;
    licenseVC=nil;

    if ([[SWDefaults isActivationDone] isEqualToString:@"NO"])
    {
        
        [self setActivationManager:[[SWNewActivationViewController alloc] initWithNoDB] ];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:self.activationManager] ;
        [activationManager setTarget:self];
        [activationManager setAction:@selector(activationDone)];
        [self.window setRootViewController:self.mainNavigationController];
    }
    else
    {
        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
            
            DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        }
        else
        {
            DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
            self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        }

        
        MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
        _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
        
        SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController];
        
        [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
    }
}
- (void)activationDone
{
    self.activationManager.target=nil;
    self.activationManager.action=nil;
    
    self.activationManager=nil;
    
    if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
        
        DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    }
    
    else
    {
        DashboardAlSeerViewController *dashboardViewController = [[DashboardAlSeerViewController alloc] init];
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
    }

  

    MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
    _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;

    SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
    
    [self setSessionManager:[[SWSessionManager alloc] initWithWindow:self.window andApplicationViewController:splitViewController] ];
}
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //self.isBackgroundMode = NO;
    //[backgroundTimer invalidate];
    //backgroundTimer = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:kReloadRouteNotification object:nil];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"DownloadingDidComplete" object:nil];
}
-(void)applicationDidEnterBackground:(UIApplication *)application
{

    self.isBackgroundMode = YES;
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self beginBackgroundUpdateTask];
        
        self.locationManager.distanceFilter = 100;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
        
        NSLog(@"App staus: applicationDidEnterBackground");
        [self endBackgroundUpdateTask];
    });
}

- (void) beginBackgroundUpdateTask
{
    backgroundUpdateTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBackgroundUpdateTask];
    }];
}

- (void) endBackgroundUpdateTask
{
    [[UIApplication sharedApplication] endBackgroundTask: backgroundUpdateTask];
    backgroundUpdateTask = UIBackgroundTaskInvalid;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"did receive remote notification called");
    [self application:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:^(UIBackgroundFetchResult result) {
    }];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"did recieve remote notification fetch completion handler called");
    
   
    [self fetchPushNotificationContent:userInfo];
    
    
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)fetchPushNotificationContent:(NSDictionary*)launchOptions
{
    NSLog(@"fetching push notifications data");
    
    NSDictionary* userInfo = launchOptions;

    
    //    {
    //      aps =   {
    //        alert = “Test for SalesWars”;
    //        badge = 1;
    //        “content-available” = 1;
    //        sound = default;
    //      };
    //      od = “This is another test notification for SalesWars testing”;
    //      sn = Admin;
    //    }


    NSLog(@"push notification content is %@",userInfo);
    
    NSString *odString = [userInfo valueForKey:@"od"];
    NSData *asciiData = [[odString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""] dataUsingEncoding:NSASCIIStringEncoding];
    NSString *asciiString = [[NSString alloc] initWithData:asciiData encoding:NSASCIIStringEncoding];
    
    NSLog(@"HAS UNICODE  :%@", odString);
    NSLog(@"UNICODE AFTER:%@", asciiString);
    
    NSMutableArray * testArray=[[NSMutableArray alloc]initWithObjects:odString,asciiString, nil];
    NSLog(@"open data array with ascii char %@ \n", [testArray description]);
    NSString *convertedString;
    
    BOOL odHasAsciiString=[odString canBeConvertedToEncoding:NSASCIIStringEncoding];
    if (odHasAsciiString==YES) {
        
        // ASCII to NSString
        int asciiCode = 255;
        convertedString = [NSString stringWithFormat:@"%c", asciiCode]; // A
        
        NSArray *convertedArray = [[userInfo valueForKey:@"od"] componentsSeparatedByString:convertedString];
        NSLog(@"OD Array %@", convertedArray);
    }
    
 
    NSString* alertMessage=[[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
    [self incrementOneBadge];
    
    NSMutableArray *pushNotificationsContentArray = [[NSMutableArray alloc]init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    pushNotificationsContentArray= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];

    
    if ([NSString isEmpty:alertMessage]==NO) {
        
        FSRMessagedetails * currentMessage=[[FSRMessagedetails alloc]init];
        currentMessage.Message_Title=kPushNotificationTitle;
        currentMessage.Message_Content=alertMessage;
        currentMessage.Message_date=[SWDefaults fetchCurrentDateTimeinDatabaseFormat];
        currentMessage.isFromPushNotifications=YES;
        currentMessage.Message_Read=@"N";
        
        //split notification title and image url
        
        NSString *odString=[userInfo valueForKey:@"od"];
        
        if ([NSString isEmpty:odString]==NO) {
            //if od key is not available then alert message should be displayed as content
            currentMessage.Message_Title = alertMessage;
            currentMessage.Message_Content = odString;
            currentMessage.Push_Notification_ID=[NSString createGuid];
            
            NSLog(@"push notification id is %@", currentMessage.Push_Notification_ID);
        }
        
        
        if (pushNotificationsContentArray.count>0) {
            if (![pushNotificationsContentArray containsObject:currentMessage]) {
                [pushNotificationsContentArray addObject:currentMessage];
            }
        }
        else {
            pushNotificationsContentArray = [[NSMutableArray alloc]init];
            if (![pushNotificationsContentArray containsObject:currentMessage]) {
                [pushNotificationsContentArray addObject:currentMessage];
            }
        }
        
        NSLog(@"push notification content array being saved in defaults %@", pushNotificationsContentArray);
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushNotificationsContentArray];
        [defaults setObject:data forKey:@"PUSHED_MESSAGES"];
        [defaults synchronize];
        
        NSArray *array= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"PUSHED_MESSAGES"]];
        NSLog(@"notification content array  after archieving is %@",array);
        
    }
    NSLog(@"before calling notification in completion handler");
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"PUSHNOTIFICATIONARRIVED" object:nil userInfo:userInfo];
    
    NSLog(@"after calling notification in completion handler");
}

//Called when a notification is delivered to a foreground app.

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    
    if ([response.notification.request.content.title isEqualToString:@"Pending Feedback"]) {
        CustomersListViewController *objViewController = [[CustomersListViewController alloc] init];
        objViewController.isPendingNotificationTapped = YES;
        self.mainNavigationController = [[UINavigationController alloc] initWithRootViewController:objViewController];

        MenuViewController *menuViewController = [[MenuViewController alloc] init] ;
        _menuNavigationController = [[UINavigationController alloc] initWithRootViewController:menuViewController] ;
        
        SWSplitViewController *splitViewController = [[SWSplitViewController alloc] initWithFrontViewController:self.mainNavigationController rearViewController:_menuNavigationController] ;
        
        [self.window setRootViewController:splitViewController];
    }
    completionHandler();
}

-(void) incrementOneBadge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges +=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

-(void) decrementOneBdge{
    NSInteger numberOfBadges = [UIApplication sharedApplication].applicationIconBadgeNumber;
    numberOfBadges -=1;
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:numberOfBadges];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

-(void)registerforPushNotificationwithDeviceToken:(NSString*)deviceID
{
    NSString* tempUserName=[[SWDatabaseManager retrieveManager] fetchuserNamefromDB];
    NSString* userNameFromDB=[SWDefaults getValidStringValue:tempUserName];
    
    
    
    if ([NSString isEmpty:userNameFromDB]==NO ) {
        
        NSString *deviceCode = @"IPAD";
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        if (deviceToken.length==0) {
            deviceToken = @"";
        }
        
        //adding customer id with device id to distinguish customer
        
        NSString* licenseCustomerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        
        NSString *deviceUserId = [[NSString stringWithFormat:@"%@_",licenseCustomerID] stringByAppendingString:userNameFromDB];
        NSLog(@"device user id being saved is %@", deviceUserId);
        
        
        NSString *loggedTime = [self getUTCFormateDateforNotification:[NSDate date]];
        NSString *uKey =@"bca8863200aa18f96ef6f9b43d44e5778f338ee286bb56acf50aebb1d489238c";
        
        
        NSString *hashParam = [NSString stringWithFormat:@"DeviceCode=%@&DeviceID=%@&DeviceToken=%@&DeviceUserID=%@&LoggedAt=%@&UKey=%@",deviceCode,deviceID,deviceToken,deviceUserId,loggedTime,uKey];
        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *result =[self hmacWithKey:@"0f971d1b02062cc0a839126c7917027e80dd10c583a788504cf006801dc1ed02" andData:hashParam];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        
        if(networkStatus != NotReachable)
        {
            NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys: deviceCode ,@"DeviceCode", deviceID , @"DeviceID", deviceToken , @"DeviceToken", deviceUserId , @"DeviceUserID", loggedTime , @"LoggedAt", uKey, @"UKey", result, @"CData", nil];
            
            NSLog(@"registerforPushNotificationwithDeviceToken Parameter is %@", parameters);
            
            NSString *serverAPI=@"http://demoapps.ddns.net:10101/PNS/Data/RegisterDevice";
            
            AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:serverAPI]];
            [client setDefaultHeader:@"contentType" value:@"application/json; charset=utf-8"];
            client.parameterEncoding = AFJSONParameterEncoding;
            
            NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:@"" parameters:parameters];
            
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request,NSHTTPURLResponse *response, id JSON)
                                                 {
                                                     NSLog(@"register device JSON is %@", JSON);
                                                     
                                                     
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceToken"]forKey:@"deviceToken"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceID"]forKey:@"savedDeviceID"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                 }
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse*response, NSError *error, id JSON)
                                                 {
                                                     NSLog(@"Error%@",[error localizedDescription]);
                                                 }];
            [operation start];
        }
        else
        {
            
        }
        
    }else{
        [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"ToSaveDeviceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
 
    
    if (![[AppControl retrieveSingleton].FSR_LOC_TRK_FREQ isEqualToString:@"0"] && [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ != nil) {
        
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:@"HH:mm"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [outputFormatter setLocale:usLocale];
        NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
        NSDate *currentTime = [outputFormatter dateFromString:newDateString];

        NSDate *startTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_START_TIME];
        NSDate *endTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_END_TIME];
        
        BOOL canCaptureLocationData = [SWDefaults date:currentTime isBetweenDate:startTime andDate:endTime];
        
        if (canCaptureLocationData) {
            float milliseconds = [AppControl retrieveSingleton].FSR_LOC_TRK_FREQ.floatValue;
            float seconds = milliseconds / 1000.0;

            sendLocationTimer = [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(saveLocationData:) userInfo:nil repeats:YES];
        }
        // Suppose start time = 8 am.  If user runs app before start time and time becomes 8 am then saveLocationData method should call.
        else{
            checkLocationStartTime = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkStartLocationTime:) userInfo:[NSNumber numberWithBool: NO] repeats:YES];
        }
    }
}

+ (NSString *)stringFromDeviceToken:(NSData *)deviceToken {
    NSUInteger length = deviceToken.length;
    if (length == 0) {
        return nil;
    }
    const unsigned char *buffer = deviceToken.bytes;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(length * 2)];
    for (int i = 0; i < length; ++i) {
        [hexString appendFormat:@"%02x", buffer[i]];
    }
    return [hexString copy];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    NSString *deviceID = [SWAppDelegate stringFromDeviceToken:deviceToken];
    NSLog(@"Device Token %@", deviceToken);
    [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"deviceID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString* tempUserName=[[SWDatabaseManager retrieveManager] fetchuserNamefromDB];
    NSString* userNameFromDB=[SWDefaults getValidStringValue:tempUserName];
    
     NSLog(@"Device ID %@", deviceID);
    
    if ([NSString isEmpty:userNameFromDB]==NO ) {
        
        
        
        NSString *deviceCode = @"IPAD";
        
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
        if (deviceToken.length==0) {
            deviceToken = @"";
        }
        
        //adding customer id with device id to distinguish customer
        
        NSString* licenseCustomerID=[[NSUserDefaults standardUserDefaults] valueForKey:@"Customer_ID"];
        
        NSString *deviceUserId = [[NSString stringWithFormat:@"%@_",licenseCustomerID] stringByAppendingString:userNameFromDB];
        NSLog(@"device user id being saved is %@", deviceUserId);
        
        NSString *loggedTime = [self getUTCFormateDateforNotification:[NSDate date]];
        NSString *uKey =@"bca8863200aa18f96ef6f9b43d44e5778f338ee286bb56acf50aebb1d489238c";
        
        
        NSString *hashParam = [NSString stringWithFormat:@"DeviceCode=%@&DeviceID=%@&DeviceToken=%@&DeviceUserID=%@&LoggedAt=%@&UKey=%@",deviceCode,deviceID,deviceToken,deviceUserId,loggedTime,uKey];
        hashParam = [hashParam stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *result =[self hmacWithKey:@"0f971d1b02062cc0a839126c7917027e80dd10c583a788504cf006801dc1ed02" andData:hashParam];
        
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        
        if(networkStatus != NotReachable)
        {
            NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys: deviceCode ,@"DeviceCode", deviceID , @"DeviceID", deviceToken , @"DeviceToken", deviceUserId , @"DeviceUserID", loggedTime , @"LoggedAt", uKey, @"UKey", result, @"CData", nil];
            
            NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken Parameter is %@", parameters);
            
            NSString *serverAPI=@"http://demoapps.ddns.net:10101/PNS/Data/RegisterDevice";
            
            AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL: [NSURL URLWithString:serverAPI]];
            [client setDefaultHeader:@"contentType" value:@"application/json; charset=utf-8"];
            client.parameterEncoding = AFJSONParameterEncoding;
            
            NSMutableURLRequest *request = [client requestWithMethod:@"POST" path:@"" parameters:parameters];
            
            AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request,NSHTTPURLResponse *response, id JSON)
                                                 {
                                                     
                                                     NSLog(@"Register device response is %@", JSON);
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceToken"]forKey:@"deviceToken"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     [[NSUserDefaults standardUserDefaults]setValue:[JSON stringForKey:@"DeviceID"]forKey:@"savedDeviceID"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                 }
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse*response, NSError *error, id JSON)
                                                 {
                                                     NSLog(@"Error%@",[error localizedDescription]);
                                                 }];
            [operation start];
        }
        else
        {
            
        }
        
    }else{
        [[NSUserDefaults standardUserDefaults]setValue:deviceID forKey:@"ToSaveDeviceID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(NSString *)getUTCFormateDateforNotification:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocaleq];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}

- (NSString *)hmacWithKey:(NSString *)key andData:(NSString *)data
{
    const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    //NSString *s = [self base64String:[[hash.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    NSString* s = [SWAppDelegate base64forData:hash];
    //NSString *s = [[hash.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    // NSString *s = [hash base64EncodedString];
    NSLog(@"%@",s);
    return s;
}

+ (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] = table[(value >> 18) & 0x3F];
        output[theIndex + 1] = table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6) & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0) & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]; }
- (NSString *)base64String:(NSString *)str
{
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}


void uncaughtExceptionHandler(NSException *exception)
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    NSArray *tresdarray = [[NSThread callStackSymbols] objectAtIndex:0];
    NSString *message = [NSString stringWithFormat:@"iOS:%@  \n Other Detail:%@ ",version,tresdarray];
    [Flurry logError:@"Uncaught" message:message exception:exception];
}


#pragma mark Location data methods
-(void)saveLocationData:(BOOL)isVisitStarted {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [outputFormatter setLocale:usLocale];
    NSString *newDateString = [outputFormatter stringFromDate:[NSDate date]];
    NSDate *currentTime = [outputFormatter dateFromString:newDateString];

    NSDate *startTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_START_TIME];
    NSDate *endTime = [outputFormatter dateFromString:[AppControl retrieveSingleton].LOC_TRK_END_TIME];
    
    BOOL canCaptureLocationData = [SWDefaults date:currentTime isBetweenDate:startTime andDate:endTime];
    
    if ([[SWDefaults isActivationDone] isEqualToString:@"YES"]) {
        if (canCaptureLocationData || isVisitStarted) {
            
            BOOL success;
            FMDatabase *db = [FMDBHelper getDatabase];
            [db open];
            NSString *salesRepID = [NSString stringWithFormat:@"%@", [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
            
            
            SWAppDelegate *appDelegate = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
            CLLocationCoordinate2D coordinate = [appDelegate.currentLocation coordinate];
            NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
            NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
            
            if ([latitude isEqualToString:@"0"] || [longitude isEqualToString:@"0"] || latitude == nil || longitude == nil) {
                latitude = @"0.0";
                longitude = @"0.0";
            }
            
            NSString *gpsStatus = @"";
            if ([CLLocationManager locationServicesEnabled]) {
                if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
                {
                    // authorized
                    gpsStatus = @"Y";
                } else {
                    // permission issue
                    gpsStatus = @"P";
                }
            } else {
                // no GPS
                gpsStatus = @"N";
            }
            
            
            CLLocationAccuracy locationAccuracy = self.currentLocation.horizontalAccuracy;
            NSString *accuracy = [NSString stringWithFormat:@"%f", locationAccuracy];
            NSString *loggedAt = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
            
            NSString *customerID = @"";
            NSString *siteUseID = @"";
            NSString *visitID = @"";
            
            if (isVisitStarted) {
                visitID = [SWDefaults currentVisitID];
                NSMutableDictionary *customer = [SWDefaults customer];
                customerID = [customer stringForKey:@"Customer_ID"];
                siteUseID = [customer stringForKey:@"Site_Use_ID"];
            }
            
            @try {
                success = [db executeUpdate:@"INSERT into TBL_FSR_Locations(Row_ID, SalesRep_ID, Activity_Type, Latitude, Longitude, Status, Accuracy, Customer_ID, Site_Use_ID, Visit_ID, Logged_At) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [NSString createGuid], salesRepID, @"", latitude, longitude, gpsStatus, accuracy, customerID, siteUseID, visitID, loggedAt, nil];
                
                if (success == YES) {
                    
                    NSLog(@"TBL_FSR_Locations insert successfull");
                    [FIRAnalytics logEventWithName:@"CaptureLocation"
                                        parameters:@{
                                                     @"latitude": longitude,
                                                     @"longitude": longitude,
                                                     @"customerID": customerID,
                                                     @"gpsStatus": gpsStatus,
                                                     @"loggedAt": loggedAt
                                                     }];
                    
                    BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
                    if (checkInternetConnectivity==YES) {
                        [self sendLocationDataToServer];
                    }
                }
            }@catch (NSException *exception)
            {
                NSLog(@"Exception while inserting FSR Location data in database: %@", exception.reason);
            }
            @finally {
                [db close];
            }
        } else {
            NSMutableArray *locationArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Locations"]];
            
            if(locationArray.count > 0)
            {
                BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
                if (checkInternetConnectivity==YES) {
                    [self sendLocationDataToServer];
                }
            }
        }
    }
}

#pragma mark Send data to Server
-(void)sendLocationDataToServer
{
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        NSString *xmlParsedString = [self writeFSRLocationDataXMLString];
        NSString *XMLPassingString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)xmlParsedString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
        [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSave_FSR_Locations" andProcParam:@"GEOInfo" andProcValue: XMLPassingString];
    }
}

#pragma mark writeFSRLocationDataXML
- (NSString *)writeFSRLocationDataXMLString {

    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *locationArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Locations"]];
    
    if(locationArray.count > 0)
    {
        [xmlWriter writeStartElement:@"GEOLocations"];
        for (int i=0; i<[locationArray count]; i++)
        {
            [xmlWriter writeStartElement:@"L"];
            
            NSMutableDictionary *locationDictionary = [NSMutableDictionary dictionaryWithDictionary:[locationArray objectAtIndex:i]];
            
            if (![[locationDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"SalesRep_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"Activity_Type"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Activity_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Activity_Type"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[locationDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Status"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"Accuracy"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Accuracy"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Accuracy"]];
                [xmlWriter writeEndElement];
            }
            if (![[locationDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[locationDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Site_Use_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[locationDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[locationDictionary stringForKey:@"Logged_At"] isEqualToString:@"<null>"] && [locationDictionary stringForKey:@"Logged_At"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[locationDictionary stringForKey:@"Logged_At"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//L
        }
        [xmlWriter writeEndElement];//GEOLocations
    }
    return [xmlWriter toString];
}

- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData *dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *serverAPI = [NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    NSString *strurl = [serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];

    
    NSString *strDeviceID = [[DataSyncManager sharedManager]getDeviceID];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray *resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         
         if([procName isEqualToString:@"sync_MC_ExecSave_FSR_Locations"])
         {
             resultArray = [responseText JSONValue];
             NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
             if([ProcResponse isEqualToString:@"GEO Locations imported successfully"])
             {
                 [FIRAnalytics logEventWithName:@"SendLocationBO"
                                     parameters:@{
                                                  @"Messages": @"GEO Locations imported successfully",
                                                  @"customerID": [self customerId]}];
                 [[SWDatabaseManager retrieveManager] executeNonQuery:@"Delete from TBL_FSR_Locations"];
                 [self performSelector:@selector(stopAnimationofFSRLocation) withObject:nil afterDelay:0.1];
             }
             else {
                 [self performSelector:@selector(stopAnimationofFSRLocation) withObject:nil afterDelay:0.1];
             }
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [self performSelector:@selector(stopAnimationofFSRLocation) withObject:nil afterDelay:0.1];
     }];
    
    //call start on your request operation
    [operation start];
}

- (void)stopAnimationofFSRLocation
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
}

//#import "CJSONSerializer.h"
/*
 
 Steps to analyze crash report from apple:
 
 Copy the release .app file which was pushed to the appstore, the .dSYM file that was created at the time of release and the crash report receive from APPLE into a FOLDER.
 
 OPEN terminal application and go to the folder created above (using CD command)
 
 atos -arch armv7 -o YOURAPP.app/YOURAPP MEMORY_LOCATION_OF_CRASH. The memory location should be the one at which the app crashed as per the report.
 
 Ex:  atos -arch armv7  -o 'app name.app'/'app name' 0x0003b508
 
 This would show you the exact line, method name which resulted in crash.
 
 Ex: [classname functionName:]; -510
 

 */
//6   Salesworx                         0x000f4872 0x37000 + 776306

//#import "CJSONDeserializer.h"


// the function specified in the same class where we defined the addObserver
- (void)enableVTRXBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KENABLE_V_TRX_BS_NOTIFICATIONNameStr])
    {
        if(V_TRX_BS_Timer==nil)
        {
            AppControl *appcontrol = [AppControl retrieveSingleton];
            NSInteger timerValue = [appcontrol.V_TRX_SYNC_INTERVAL integerValue] * 60; // convert into minute
            NSLog(@"timer for visits sync %ld",timerValue);
            V_TRX_BS_Timer = [NSTimer timerWithTimeInterval:timerValue
                                                     target:self
                                                   selector:@selector(postVTRXDataToServer)
                                                   userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:V_TRX_BS_Timer forMode:NSRunLoopCommonModes];
        }
    }
}

-(void)postVTRXDataToServer
{
    if([SWDefaults isVTRXBackgroundSyncRequired] && ![NSString isEmpty:[SWDefaults username]] && [currentExceutingDataUploadProcess isEqualToString:KDataUploadProcess_None])
    {
        currentExceutingDataUploadProcess=KDataUploadProcess_VTRXBS;
        SalesWorxBackgroundSyncManager *salesWorxBackgroundSyncManager = [[SalesWorxBackgroundSyncManager alloc]init];
        [salesWorxBackgroundSyncManager postVisitDataToserver];
    }
}

// the function specified in the same class where we defined the addObserver
- (void)disableVTRXBackGroundSync:(NSNotification *)notification {
    if([notification.name isEqualToString:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr])
    {
        if (V_TRX_BS_Timer!=nil) {
            [V_TRX_BS_Timer invalidate];
            V_TRX_BS_Timer = nil;
        }
    }
}

-(NSString *)customerId {
    NSString *customerID = @"";
    NSMutableDictionary *customer = [SWDefaults customer];
    if (customer != nil) {
        customerID = [customer stringForKey:@"Customer_ID"];
    }
    return customerID;
}

@end
