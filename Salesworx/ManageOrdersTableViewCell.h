//
//  ManageOrdersTableViewCell.h
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 5/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
#import "EditableCell.h"
@interface ManageOrdersTableViewCell : EditableCell
@property (strong,nonatomic)IBOutlet SalesWorxSingleLineLabel * orderNumberLabel;
@property (strong,nonatomic)IBOutlet SalesWorxSingleLineLabel * orderamountLabel;
@property (strong,nonatomic)IBOutlet SalesWorxSingleLineLabel * OrderCreationdateLabel;
@property (strong,nonatomic)IBOutlet SalesWorxSingleLineLabel * OrderstatusLabel;

@end
