//
//  MenuItem.h
//  Salesworx
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject {
    NSString *title;
    NSString *className;
    NSString *imageName;
}

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *className;
@property (nonatomic, strong) NSString *imageName;

+ (MenuItem *)itemWithTitle:(NSString *)title className:(NSString *)className imageName:(NSString *)imageName;

@end
