//
//  SWProductListViewController.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "ProductDetailViewController.h"


@interface SWProductListViewController : ProductListViewController
{
    ProductDetailViewController *productDetailViewController;
}
@end