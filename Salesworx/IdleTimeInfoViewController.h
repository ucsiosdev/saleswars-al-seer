//
//  IdleTimeInfoViewController.h
//  SalesWars
//
//  Created by UshyakuMB2 on 25/08/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"

@interface IdleTimeInfoViewController : UIViewController
{
    IBOutlet UILabel *lblStartTime;
    IBOutlet MedRepTextField *txtStartTime;
    IBOutlet UILabel *lblEndTime;
    IBOutlet MedRepTextField *txtEndTime;
    IBOutlet UILabel *lblReason;
    IBOutlet MedRepTextField *txtReason;
    IBOutlet UIView *viewComments;
    IBOutlet UILabel *lblComments;
    IBOutlet UITextView *txtViewComments;
    
    MedRepTextField *selectedTextField;
    NSMutableArray *arrReason;
    NSString *selectedReasonCodeValue, *selectedStartTime, *selectedEndTime;
}
@property (nonatomic, assign) id delegate;

@end
