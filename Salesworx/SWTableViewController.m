//
//  SWTableViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWTableViewController.h"
#import "SWFoundation.h"

@interface SWTableViewController ()

@end

@implementation SWTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x169C5C);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x169C5C);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x169C5C)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x169C5C)];
    }
  
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          EditableTextFieldSemiFontOfSize(14), UITextAttributeFont,
                                                          nil] forState:UIControlStateNormal];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.textLabel.font = kFontWeblySleekSemiBold(14);
    cell.textLabel.textColor =  UIColorFromRGB(0x2C394A);
    
    cell.detailTextLabel.font = EditableTextFieldSemiFontOfSize(14);
    cell.detailTextLabel.textColor =  UIColorFromRGB(0x6A6F7B);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
