//
//  SWRouteViewController.h
//  SWRoute
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapView2.h"

//#import "RegexKitLite.h"
#import "Place2.h"
#import "PlaceMark.h"
#import <CoreLocation/CoreLocation.h>
#import "SWFoundation.h"
#import "SWPlatform.h"

@interface SWRouteViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate,MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate> {
    
    

    NSIndexPath *selectedIndexPath;
    NSArray *routes;
    UITableView *tableView;
    NSDate *selectedDate;
    UIPopoverController *datePickerPopOver;
    UIView *footerView;
    MapView2 *routeMapView;
    Place2 *place;
    CSMapAnnotation2 *Annotation;
    UILabel *visitedPlace;
    UILabel *remainingPlace;
    UILabel *coveragePlace;
    UIButton *minusDate;
    UIButton *addDate;
    UIImageView *bottomImage;
    NSMutableDictionary *customer;
    UIImageView* routeView;
    UIColor* lineColor;
    NSString * forCurrentLat;
    NSString * forCurrentLong;
    CLLocationManager * _locManager;
    CLLocation *myloa;
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentLocation;
    UIButton *Login_Btn;
    UIButton *locationButton;
    UIButton *customerMapButton;
    ZBarReaderViewController *reader;
    BOOL isFurtureDate;
    int visitedCustomer ;
    int remainningCustomer ;

    UIView *myBackgroundView;
    IBOutlet UIView *dateView;
    
    
    IBOutlet UIView *noDataView;
    NSMutableArray* annotationsArray;
    IBOutlet UIButton *showDateButton;
    IBOutlet UITableView *listTableView;
    IBOutlet MedRepElementDescriptionLabel *customerNameLabel;
    IBOutlet MedRepElementTitleLabel *customerCodeLabel;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;
    IBOutlet MKMapView *locationMapView;
    IBOutlet UILabel *plannedCountLabel;
    IBOutlet UILabel *completedCountLabel;
    IBOutlet UILabel *adherencePercentLabel;
    IBOutlet UIView *plannedBlock;
    IBOutlet UIView *completedBlock;
    IBOutlet UIView *adherenceBlock;

}


@end
