//
//  GridCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 6/28/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableCell.h"

@interface GridCell : EditableCell <UITextFieldDelegate> {
    NSArray *columnWidths;
    NSMutableArray *labels;
    int inputColumnIndex;
    UITextField *textFieldView;
    UILabel *labelView;
}

@property (nonatomic, strong) NSArray *columnWidths;
@property (nonatomic, strong) NSMutableArray *labels;
@property(strong,nonatomic)UILabel *labelView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier columnWidths:(NSArray *)widths andInputColumnIndex:(int)columnIndex;
- (void)setText:(NSString *)text forColumn:(int)column isTextCenterAlign:(BOOL)isCenterAlign;
@end
