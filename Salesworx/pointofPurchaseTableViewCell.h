//
//  pointofPurchaseTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pointofPurchaseTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *brandNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *skuLbl;
@property (strong, nonatomic) IBOutlet UILabel *remarksButton;
@property (strong, nonatomic) IBOutlet UILabel *rangeLbl;
@property (strong, nonatomic) IBOutlet UILabel *shelfLbl;
@property (strong, nonatomic) IBOutlet UILabel *priceLbl;
@property (strong, nonatomic) IBOutlet UILabel *promotionLbl;
@property (strong, nonatomic) IBOutlet UILabel *mslLbl;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;

@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UILabel *remarksLbl;

- (IBAction)infoButtonTapped:(id)sender;


@property (strong, nonatomic) IBOutlet UITextView *remarksTxtView;

@property (strong, nonatomic) IBOutlet UILabel *remarksPlaceholderLbl;












































@end
