//
//  SWDuesView.h
//  SWCustomer
//
//  Created by Irfan on 12/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"


@interface SWDuesView : FTPBaseViewController<UITableViewDataSource,UITableViewDelegate >
{
    
    NSArray *duesItems;
    UITableView *duesTableView;
    NSDictionary *duesDict;
    NSMutableDictionary *customer;
    
    UIButton *continueVisitButton;
    UIButton *stopVisitButton;
    UILabel *noteLable;
    CustomerHeaderView *customerHeaderView;

    
    

    
}

@property(strong,nonatomic)NSArray* categoriesArrayFromVisitOptions;



- (id)initWithCustomer:(NSDictionary *)dueItem;


@end
