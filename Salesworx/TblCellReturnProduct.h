//
//  TblCellReturnProduct.h
//  SalesWars
//
//  Created by Ravinder Kumar on 07/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TblCellReturnProduct : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblProdcutCode;
@property (strong, nonatomic) IBOutlet UIView *viewProdcutStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblProductName;
@end

NS_ASSUME_NONNULL_END
