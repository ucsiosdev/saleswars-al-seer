//
//  CustomerPriceListCell.h
//  SWCustomer
//
//  Created by Irfan on 10/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerPriceListCell : UITableViewCell
{
    UILabel *refLabelP;
    UILabel *dateLabelP;
    UILabel *amountLabelP;
    UILabel *statusLabelP;
    UILabel *priceLabelP;

    NSDictionary *invoiceP;

}
@property (nonatomic, strong) UILabel *refLabelP;
@property (nonatomic, strong) UILabel *dateLabelP;
@property (nonatomic, strong) UILabel *amountLabelP;
@property (nonatomic, strong) UILabel *statusLabelP;
@property (nonatomic, strong) UILabel *priceLabelP;
@property (nonatomic, strong) NSDictionary *invoiceP;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)bindInvoicePrice:(NSDictionary *)invoice;


@end
