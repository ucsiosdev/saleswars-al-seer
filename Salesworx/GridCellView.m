//
//  GridCellView.m
//  SWFoundation
//
//  Created by Irfan Bashir on 6/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "GridCellView.h"
#import "SWFoundation.h"
#import <QuartzCore/QuartzCore.h>
#import "SWDefaults.h"

@implementation GridCellView

@synthesize borderType;
@synthesize borderColor;
@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
        
        self.borderType = GridCellBorderTypeRight;
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setBorderColor:[UIColor colorWithRed:0.9f green:0.9f blue:0.9f alpha:1.0f]];
        
        if (self.titleLabel) {
            self.titleLabel=nil;
        }
        [self setTitleLabel:[[UILabel alloc] init]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
//        [self.titleLabel setFont:BoldSemiFontOfSize(14)];
        [self.titleLabel setFont:headerTitleFont];

        
        [self.titleLabel setHighlightedTextColor:[UIColor whiteColor]];

        
        [self.titleLabel setText:@"Hello"];
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)setBorderType:(GridCellBorderType)bt {
    borderType = bt;
    
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    const CGFloat *c = CGColorGetComponents(self.borderColor.CGColor);
    CGFloat red = c[0];
    CGFloat green = c[1];
    CGFloat blue = c[2];
    
    
    CGContextSetRGBStrokeColor(ctx, red, green, blue, 1.0f);
    CGContextSetRGBFillColor(ctx, red, green, blue, 1.0f);
    
    if ((GridCellBorderTypeAll & self.borderType) == GridCellBorderTypeAll) {
        CGContextStrokeRect(ctx, self.bounds);
    } else {
        if ((GridCellBorderTypeRight & self.borderType) == GridCellBorderTypeRight) {
            CGContextBeginPath(ctx);
            CGContextMoveToPoint(ctx, self.bounds.size.width - 1, 0.0f);
            CGContextAddLineToPoint(ctx, self.bounds.size.width - 1, self.bounds.size.height);
            CGContextStrokePath(ctx);
        }
        if ((GridCellBorderTypeLeft & self.borderType) == GridCellBorderTypeLeft) {
            CGContextBeginPath(ctx);
            CGContextMoveToPoint(ctx, 1, 0.0f);
            CGContextAddLineToPoint(ctx, 1, self.bounds.size.height);
            CGContextStrokePath(ctx);
        }
        if ((GridCellBorderTypeBottom & self.borderType) == GridCellBorderTypeBottom) {
            CGContextBeginPath(ctx);
            CGContextMoveToPoint(ctx, 0, self.bounds.size.height);
            CGContextAddLineToPoint(ctx, self.bounds.size.width, self.bounds.size.height);
            CGContextStrokePath(ctx);
        }
        if ((GridCellBorderTypeTop & self.borderType) == GridCellBorderTypeTop) {
            CGContextBeginPath(ctx);
            CGContextMoveToPoint(ctx, 0, 0);
            CGContextAddLineToPoint(ctx, self.bounds.size.width, 0);
            CGContextStrokePath(ctx);
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.titleLabel setFrame:CGRectInset(self.bounds, 5, 5)];
}


@end
