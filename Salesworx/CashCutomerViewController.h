//
//  CashCutomerViewController.h
//  SWPlatform
//
//  Created by msaad on 1/8/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"


@class GenCustListViewController;


@interface CashCutomerViewController : SWViewController <UITableViewDataSource , UITableViewDelegate ,UITextFieldDelegate , UIPopoverControllerDelegate>
{
    NSMutableDictionary *customer;
    UIPopoverController *popoverController;
    UITableView *tableViewController;
    CustomerHeaderView *customerHeaderView;
    NSString *name;
    NSString *phone;
    NSString *contact;
    NSString *address;
    NSString *fax;
    NSMutableDictionary *dataDict;
    GenCustListViewController *listView;
    BOOL isDropDownSelected;
    BOOL isTextFieldSelected;
    //
    

}


- (id)initWithCustomer:(NSDictionary *)customer;

@end
