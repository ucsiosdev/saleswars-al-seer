//
//  AlseerSalesOrderProductListTableViewCell.m
//  SalesWars
//
//  Created by Prasann on 24/11/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import "AlseerSalesOrderProductListTableViewCell.h"

@implementation AlseerSalesOrderProductListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   
    [super setSelected:selected animated:animated];
    if (self.tag==1){
        self.statusIndicatorView.backgroundColor = [UIColor redColor];
    }else {
        self.statusIndicatorView.backgroundColor = [UIColor clearColor];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [super setHighlighted:highlighted animated:animated];
    
   
}

@end
