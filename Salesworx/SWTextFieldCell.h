//
//  SWTextFieldCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "EditableCell.h"

/*!
 @discussion
 This class will customize the UITableViewCell to have a text field for input.
 If there's no label required then text field will take all of the space.
 */
@interface SWTextFieldCell : EditableCell < UITextFieldDelegate > {

    UILabel *label;
    UILabel *secondLabel;
    UITextField *textField;
}

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *secondLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end