//
//  LocationFilterViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWTableViewController.h"
#import "SWPlatform.h"

@interface LocationFilterViewController : SWTableViewController <EditableCellDelegate>
{
    NSArray *filters;
    SEL action;
    NSMutableArray *paymentOption;
    NSMutableArray *customerStatus;
    UIView *myBackgroundView;
}

@property (nonatomic, strong) NSArray *filters;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

- (void)selectionDone:(id)sender;
- (void)clearFilter:(id)sender ;
@end
