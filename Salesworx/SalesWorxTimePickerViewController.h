//
//  SalesWorxTimePickerViewController.h
//  SalesWars
//
//  Created by UshyakuMB2 on 26/08/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SalesWorxTimePickerDelegate <NSObject>

-(void)didSelectTime:(NSString*)selectedTime;

@end


@interface SalesWorxTimePickerViewController : UIViewController<UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIDatePicker *salesWorxDatePicker;
@property(strong,nonatomic) id didSelectTimeDelegate;
@property(strong,nonatomic) NSString *titleString;

@end
