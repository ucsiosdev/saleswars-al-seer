



//
//  SWDatabaseManager.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/7/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDatabaseManager.h"
///
#import "SWAppDelegate.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "AllUserList.h"
#import "FSRMessagedetails.h"
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "SurveyDetails.h"
#import "PlannedVisitDetails.h"
#import "CustomerShipAddressDetails.h"
#import "AppConstantsList.h"
#import "DataSyncManager.h"
//#import "ILAlertView.h"
#import "SWDefaults.h"
#import "SWFoundation.h"
#import "StatementViewController.h"
#import "FMDBHelper.h"
#import "SalesWorxCustomClass.h"
#import "MedRepDefaults.h"



#define ksampleQuery select Customer_Name from TBL_Customer_Ship_Address;

@implementation SWDatabaseManager

@synthesize target,action;



#pragma cache_size =1

static SWDatabaseManager *sharedSingleton=nil;
//static sqlite3 *db;

+ (SWDatabaseManager *)retrieveManager{
    @synchronized(self)
    {
        if (!sharedSingleton)
        {
            //NSLog(@"DATABASE MANAGER ALLOCS");
            sharedSingleton = [[SWDatabaseManager alloc] init];
        }
        return sharedSingleton;
    }
    
}
+ (void)destroyMySingleton
{
    sharedSingleton = nil;
   // [self performSelector:@selector(cleanObject) withObject:nil afterDelay:0.0];

}

-(void)cleanObject
{
//    resultsArray=nil;
//    result=nil;
//    resultSet=nil;
//    columnName=nil;
    //single=nil;
    
    //Product
    
    //Dashboard
   // actualVisitArray=nil;
    
    //Customer
    //sections=nil;
    
    //Send Order
//    mainDictionary=nil;
//    orderDictionary=nil;
//    itemDictionary =nil;
//    lotDictionary =nil;
//    orderArray1=nil;
//    itemArray=nil;
//    lotArray1=nil;
//    mainArray=nil;
    //       id target;
    action=nil;
    //xmlWriter=nil;
    
    //Collection
    
    //Route
    //locationManager=nil;
    latitude=nil;
    longitude=nil;
}
- (id)init{
    if ((self = [super init]))
    {

    }
    return self;
}

- (FMDatabase *)getDatabase
{
    
    
    
    
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }

    
//    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    
    return db;
}


- (BOOL)executeNonQueryOne:(NSString *)sqlQuery
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sqlQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}
- (void)executeNonQuery:(NSString *)sql {
    //[self doQuery:sql];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sql];
        NSLog(@"insert successfull %hhd", success);
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    //return success;

    
}










- (NSMutableArray *)fetchDataForQuery:(NSString *)sql
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableArray *newsItems = [NSMutableArray new];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            [newsItems addObject:[results resultDictionary]];            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        
        [db close];
    }
    
    return newsItems;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(NSString*)fetchuserNamefromDB
{
    
    NSMutableArray*    tblUserArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:@"select Username from tbl_user"];
    if (tblUserArray.count>0) {
        
        return [[tblUserArray objectAtIndex:0] valueForKey:@"Username"];
    }
    else
    {
        return nil;
    }
    

}

- (NSMutableArray *)GetBonusProductWithDescription:(NSString *)pname
{
	NSMutableArray *GameInfo =[NSMutableArray array];
    NSString *sqlQuery = [NSString stringWithFormat:@"SELECT * FROM TBL_Product where Description = '%@'",pname];
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sqlQuery];
        while ([results next])
        {
            [GameInfo addObject:[results resultDictionary]];

        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
	return GameInfo;
}
- (NSArray*)selectSurveyWithCustomerID:(NSString *)customerID andSiteUseID:(NSString *)siteID;{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey Where Customer_ID='%@' and Site_Use_ID='%@'",customerID,siteID];
    FMDatabase *db = [self getDatabase];
    [db open];

    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int  Survey_ID =[[results stringForColumnIndex:0] intValue];
            NSString * Survey_Title  = [results stringForColumnIndex:1];
            int  Customer_ID=[[results stringForColumnIndex:2] intValue];
            int  Site_Use_ID =[[results stringForColumnIndex:3] intValue];
            int  SalesRep_ID  =[[results stringForColumnIndex:4] intValue];
            
            NSString * Survey_Type_Code  = [results stringForColumnIndex:5];
            NSString * Start_Time  =[results stringForColumnIndex:6];
            NSString *  End_Time  = [results stringForColumnIndex:7];
            
            SurveyDetails *info=[[SurveyDetails alloc] initWithSurveyId:Survey_ID Customer_ID:Customer_ID Site_Use_ID:Site_Use_ID SalesRep_ID:SalesRep_ID Survey_Title:Survey_Title Start_Time:Start_Time End_Time:End_Time Survey_Type_Code:Survey_Type_Code];
            [retVal addObject:info];

        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)checkSurveyIsTaken:(NSString *)customerID andSiteUseID:(NSString *)siteID ansSurveyID:(NSString *)surveyID;{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT Survey_ID,Survey_Timestamp FROM TBL_Survey_Cust_Responses Where Customer_ID='%@' and Site_Use_ID='%@' and Survey_ID='%@'",customerID,siteID,surveyID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0 ];
            
            NSString * Start_Time  = [results stringForColumnIndex:1];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:Survey_ID forKey:@"Survey_ID"];
            [tempDict setValue:Start_Time forKey:@"Start_Time"];
            
            
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)selectQuestionBySurveyId:(int)SurveyId{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey_Questions where Survey_ID=%d",SurveyId];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:0] intValue];
            
            int Survey_ID=[[results stringForColumnIndex:2] intValue];
            int  Default_Response_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Question_Text = [results stringForColumnIndex:1];
            
            SurveyQuestionDetails *info=[[SurveyQuestionDetails alloc] initWithUniqueQuestionId:Question_ID Survey_ID:Survey_ID Default_Response_ID:Default_Response_ID Question_Text:Question_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
    
}



-(NSArray*)ModifyResponseType:(NSString*)query
{
    NSMutableArray *retVal =[NSMutableArray array];
    FMDatabase *db=[self getDatabase];
    
    [db open];
    @try
    {
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:0] intValue];
            
            int Survey_ID=[[results stringForColumnIndex:2] intValue];
            int  Default_Response_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Question_Text = [results stringForColumnIndex:1];
            
            SurveyQuestionDetails *info=[[SurveyQuestionDetails alloc] initWithUniqueQuestionId:Question_ID Survey_ID:Survey_ID Default_Response_ID:Default_Response_ID Question_Text:Question_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
}



- (NSArray*)selectResponseType:(int)QuestionId{
    
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Survey_Responses where Question_Id =%d",QuestionId];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int Question_ID=[[results stringForColumnIndex:2] intValue];
            
            int Response_ID=[[results stringForColumnIndex:0] intValue];
            int Response_Type_ID=[[results stringForColumnIndex:3] intValue];
            NSString *Response_Text = [results stringForColumnIndex:1];
            
            SurveyResponseDetails *info=[[SurveyResponseDetails alloc] initWithUniqueQuestionId:Question_ID Response_ID:Response_ID Response_Type_ID:Response_Type_ID Response_Text:Response_Text];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }


           return retVal;
}
- (int) dbGetCustomerResponseCount{
    int count = 0;
    NSString *sql = @"SELECT COUNT(*) FROM TBL_Survey_Cust_Responses";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (int) dbGetAuditResponseCount{

    int count = 0;
    NSString *sql = @"SELECT COUNT(*) FROM TBL_Survey_Audit_Responses";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (void)InsertdataCustomerResponse:(NSString*)strquery{
    DataSyncManager *appDelegate =[DataSyncManager sharedManager];
    
    NSString *selectSQL =strquery;
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];

    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }

    if(appDelegate.alertMessageShown==0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"InsertSurveyNotification"object:self];
        appDelegate.alertMessageShown=1;
    }
    
}
//Communication Module
- (NSMutableArray*)dbGetFSRMessages{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Messages"];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            int Message_ID=[[results stringForColumnIndex:0] intValue];
            
            int SalesRep_ID=[[results stringForColumnIndex:1] intValue];
            
            NSString *Message_Title = [results stringForColumnIndex:2];
            NSString *Message_Content = [results stringForColumnIndex:3];
            NSString *Message_date = [results stringForColumnIndex:4];
            NSString *Message_Expiry_date = [results stringForColumnIndex:5];
            NSString *Message_Read = [results stringForColumnIndex:6];
            NSString *Message_Reply = [results stringForColumnIndex:7];
            NSString *Emp_Code = [results stringForColumnIndex:8];
            NSString *Reply_Date = [results stringForColumnIndex:9];
            
            
            
            FSRMessagedetails *info=[[FSRMessagedetails alloc] initWithMessage_Id:Message_ID SalesRep_ID:SalesRep_ID Message_Title:Message_Title Message_Content:Message_Content Message_date:Message_date Message_Expiry_date:Message_Expiry_date Message_Read:Message_Read Message_Reply:Message_Reply Emp_Code:Emp_Code Reply_Date:Reply_Date];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSString*)dbGetOrderCount{
    
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_Order "];
    FMDatabase *db = [self getDatabase];
    [db open];

    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
    
}
- (NSMutableDictionary *)dbGetBonusItemOfProduct:(NSString *)itemCode{
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
    NSString *sql =  [NSString stringWithFormat:@"select * from TBL_Product where Item_Code = (SELECT  A.Get_Item  FROM TBL_BNS_Promotion AS A INNER JOIN TBL_Product AS B ON A.Item_Code = B.Item_Code   WHERE   A.Item_Code='%@')",itemCode];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            tempDict =[[results resultDictionary] mutableCopy];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    
    return tempDict;
    
}
- (NSString*)dbGetReturnCount
{
    
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_RMA "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
   
    
    
}
- (NSString*)dbdbGetCollectionCount{
    NSString *sql = [NSString stringWithFormat:@"SELECT Count(*) FROM  TBL_Collection "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            Message_Title =[results stringForColumnIndex:0];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return Message_Title;
    
}

- (NSArray*)dbGetFSRSentMessages{
    
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT A.Message_Title,A.Message_Content,A.Message_Date , B.Username FROM TBL_Incoming_Messages As A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID GROUP BY A.Message_Date "];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            NSString *Message_Title = [results stringForColumnIndex:0];
            NSString *Message_Content =[results stringForColumnIndex:1];
            NSString *Message_date =[results stringForColumnIndex:2];
            
            FSRMessagedetails *info=[[FSRMessagedetails alloc] initWithMessage_Id:0 SalesRep_ID:0 Message_Title:Message_Title Message_Content:Message_Content Message_date:Message_date Message_Expiry_date:@"" Message_Read:@"" Message_Reply:@"" Emp_Code:@"" Reply_Date:@""];
            [retVal addObject:info];
            
            
            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)dbGetUsersList{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = [NSString stringWithFormat:@"SELECT User_id,Username FROM TBL_Users_ALL"];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            int User_ID=[[results stringForColumnIndex:0] intValue];
            NSString *name = [results stringForColumnIndex:1];
            AllUserList *info =[[AllUserList alloc] initWithUserId:User_ID User:name];
            [retVal addObject:info];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
}
- (void)updateReadMessagestatus:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:strQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"AddResponseNotification"object:self];

    
}
- (int) dbGetIncomingMessageCount
{
    int count = 0;

    NSString *sql= @"SELECT COUNT(*) FROM TBL_Incoming_Messages";
    FMDatabase *db = [self getDatabase];
    [db open];

    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            count = [[results stringForColumnIndex:0] intValue];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return count;
}
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_ID:(NSString *)Customer_ID SiteUse_ID:(NSString *)SiteUse_ID{
    
    
    NSString * sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Customer_Ship_Address where Customer_ID =%@ and Site_Use_ID =%@",Customer_ID,SiteUse_ID];
    //NSLog(@"Query %@",sql);
    
    NSArray *temp = [self fetchDataForQuery:sql];
    return temp;
    
}
- (NSArray*)dbGetCustomerShipAddressDetailsByCustomer_Name:(NSString *)Customer_Name{
    
    
    NSString * sql = [NSString stringWithFormat:@"SELECT * FROM TBL_Customer_Ship_Address where Customer_Name='%@'",Customer_Name];
    //NSLog(@"Query %@",sql);
    
    NSArray *temp = [self fetchDataForQuery:sql];
    return temp;
    
}
- (void)UserAlert:(NSString *)Message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [alert show];
}

- (void)checkUnCompleteVisits{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date == ''";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0];
            [retVal addObject:Survey_ID];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    if([retVal count]!=0)
    {
        for(int is = 0 ; is<[retVal count] ; is++)
        {
            [[SWDatabaseManager retrieveManager] updateEndTimeWithID:[retVal objectAtIndex:is]];
        }
    }
    
}
- (void)updateEndTimeWithID:(NSString *)visitID{
    
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    NSString *selectSQL =[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='%@' WHERE Actual_Visit_ID='%@'",dateString,visitID];
   
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:selectSQL];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }

    
    formatter=nil;
    usLocale=nil;
    
}
- (NSMutableDictionary*)dbGetPriceOrProductWithID:(NSString *)itemID{
    
    
    NSMutableDictionary *retVal =[NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"SELECT Unit_Selling_Price,Unit_List_Price from  TBL_Price_List  WHERE  Is_Generic = 'Y' AND Inventory_Item_ID = '%@'",itemID];
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            
            NSString *sellingPrice = [results stringForColumnIndex:0];
            NSString *listPrice = [results stringForColumnIndex:1];
            
            
            [retVal setValue:sellingPrice forKey:@"sellingPrice"];
            [retVal setValue:listPrice forKey:@"listPrice"];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return retVal;
    
}
- (void)deleteManageOrderWithRef:(NSString*)strQuery{
    

   
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    [self deleteManageOrderLiteItemsWithRef:strQuery];

}
- (void)deleteManageOrderLiteItemsWithRef:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order_Line_Items WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    [self deleteManageOrderLotWithRef:strQuery];
    
    
}
- (void)deleteManageOrderLotWithRef:(NSString*)strQuery{
    

    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Proforma_Order_Lots WHERE Orig_Sys_Document_Ref = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    
}
- (NSArray*)dbGetAppControl{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"SELECT Control_Key,Control_Value from TBL_App_Control";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Survey_ID =[results stringForColumnIndex:0];
            NSString * Start_Time  = [results stringForColumnIndex:1];
            [tempDict setValue:Start_Time forKey:Survey_ID];
        }
        [retVal addObject:tempDict];
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (void)deleteTemplateOrderWithRef:(NSString*)strQuery{
    
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Order_Template WHERE Order_Template_ID = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }

    [self deleteTemplateOrderLiteItemsWithRef:strQuery];
    
}
- (void)deleteTemplateOrderLiteItemsWithRef:(NSString*)strQuery{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM TBL_Order_Template_Items WHERE Order_Template_ID = '%@'",strQuery]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}
- (NSArray*)dbGetAllCategory;{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString * sql = @"SELECT DISTINCT Category, Item_No FROM TBL_Product Group BY  Item_No ";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * category_Name =[results stringForColumnIndex:1];
            
            NSString * category_Code  = [results stringForColumnIndex:0];
            
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            
            
            [tempDict setValue:category_Name forKey:@"category_Name"];
            [tempDict setValue:category_Code forKey:@"category_Code"];
            
            
            [retVal addObject:tempDict];
            

        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return retVal;
    
}
- (NSArray*)dbGetBonusInfoOfProduct:(NSString *)product;{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql =[NSString stringWithFormat:@"SELECT A.Prom_Qty_From, A.Prom_Qty_To, A.Price_Break_Type_Code, A.Get_Item, A.Get_Qty , B.Description  FROM TBL_BNS_Promotion AS A INNER JOIN TBL_Product AS B ON A.Item_Code = B.Item_Code   WHERE   A.Item_Code='%@' ORDER BY A.Prom_Qty_From ASC",product];
    FMDatabase *db = [self getDatabase];
    [db open];

    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSString * Prom_Qty_From =[results stringForColumnIndex:0];
            NSString * Prom_Qty_To  = [results stringForColumnIndex:1];
            NSString * Price_Break_Type_Code =[results stringForColumnIndex:2];
            NSString * Get_Item  = [results stringForColumnIndex:3];
            NSString * Get_Qty =[results stringForColumnIndex:4];
            NSString * Description  = [results stringForColumnIndex:5];
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            [tempDict setValue:Prom_Qty_From forKey:@"Prom_Qty_From"];
            [tempDict setValue:Prom_Qty_To forKey:@"Prom_Qty_To"];
            [tempDict setValue:Price_Break_Type_Code forKey:@"Price_Break_Type_Code"];
            [tempDict setValue:Get_Item forKey:@"Get_Item"];
            [tempDict setValue:Get_Qty forKey:@"Get_Qty"];
            [tempDict setValue:Description forKey:@"Description"];
            [retVal addObject:tempDict];

        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }

    
    return retVal;
    
}
- (NSString*)dbGetOnOrderQuantityOfProduct:(NSString *)itemID{
    NSString *sql = [NSString stringWithFormat:@"SELECT Attrib_Value FROM TBL_Product_Addl_Info where  Attrib_Name = 'ON_ORDER_QTY' AND Inventory_Item_ID = '%@' ",itemID];
    FMDatabase *db = [self getDatabase];
    [db open];
    NSString *Message_Title;
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            Message_Title = [results stringForColumnIndex:0];

            
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }

    
    
    
    return Message_Title;
    
    
}
- (NSArray*)dbGetMultiCurrency{
    NSMutableArray *retVal =[NSMutableArray array];
    NSString *sql = @"SELECT * from TBL_Currency";
    FMDatabase *db = [self getDatabase];
    [db open];
    
    
    @try
    {
        FMResultSet *results = [db executeQuery:sql];
        while ([results next])
        {
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            NSString * CCode =[results stringForColumnIndex:0];
            NSString * CDescription  = [results stringForColumnIndex:1];
            NSString * CRate =[results stringForColumnIndex:2];
            NSString * CDigits  = [results stringForColumnIndex:3];
            
            [tempDict setValue:CCode forKey:@"CCode"];
            [tempDict setValue:CDescription forKey:@"CDescription"];
            [tempDict setValue:CRate forKey:@"CRate"];
            [tempDict setValue:CDigits forKey:@"CDigits"];
            [retVal addObject:tempDict];
        }
        [results close];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }

    return retVal;
    
}
#pragma Report Functions
- (NSMutableArray*)dbGetDataForReport:(NSString*)sql{

    return [[self fetchDataForQuery:sql] mutableCopy];
    
}

/////////////////Product/////////////////////
- (NSArray *)dbGetProductCollection {
    
    NSString *filter = @"";
    
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
    {
        if ([[SWDefaults productFilterBrand] length] > 0)
        {
            filter = [NSString stringWithFormat:@"WHERE Brand_Code = '%@'", [SWDefaults productFilterBrand]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        if ([[SWDefaults productFilterProductID] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        if ([[SWDefaults productFilterName] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
        }
    }
    
    NSLog(@"check query for product %@",[NSString stringWithFormat:kSQLProductListNonAlphabet, filter] );
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListNonAlphabet, filter]];
    // query=nil;
    return temp;
}

- (NSArray *)dbGetProductDetail:(NSString *)itemId organizationId:(NSString *)orgId {

//check these for MSL
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetail, itemId, orgId]];
    
    
    NSLog(@"product detail query is %@", [NSString stringWithFormat:kSQLProductDetail, itemId, orgId]);
    
    
    NSLog(@"product detail qry description %@", [temp description]);
    
    return temp;
}



-(NSArray*)fetchProductDetails:(NSString*)itemID organizationId:(NSString*)orgID UOMCode:(NSString*)itemUOM

{
   
    NSString *trimmedString = [itemUOM stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
   
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetailWithUOM, itemID, orgID,trimmedString]];
    
    
    NSLog(@"product detail query with uom %@", [NSString stringWithFormat:kSQLProductDetailWithUOM, itemID, orgID,trimmedString]);
    
    
    NSLog(@"product detail qry with uom description %@", [temp description]);
    
    return temp;

}




- (NSArray *)dbGetProductDetailwithUOM:(NSString *)itemId organizationId:(NSString *)orgId {
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductDetail, itemId, orgId]];
    return temp;
}



- (NSArray *)dbGetStockInfo:(NSString *)itemId {


    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductStock, itemId]];
    return temp;
}

- (NSArray *)dbGetStockInfoForManage:(NSString *)itemId {


    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Lots Where Orig_Sys_Document_Ref = '%@'", itemId]];
    return temp;
}

- (NSArray *)dbGetBonusInfo:(NSString *)itemId {


    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductBonus, itemId]];
    return temp;
}

- (NSArray *)dbdbGetTargetInfo:(NSString *)itemId {
    NSDate *date = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSMonthCalendarUnit fromDate:date];
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:KSWLProductTarget, itemId, [comps month]]];
    return temp;
}

- (NSArray *)dbGetFilterByColumn:(NSString *)columnName1 {
    NSArray *temp = [self fetchDataForQuery:[kSQLProductFilterBrand stringByReplacingOccurrencesOfString:@"{0}" withString:columnName1]];
    return temp;
}

- (NSArray *)dbGetCategoriesForCustomer:(NSDictionary *)customer {
    NSString*testqry= [NSString stringWithFormat:kSQLProductCategories, [customer stringForKey:@"Ship_Customer_ID"], [customer stringForKey:@"Ship_Site_Use_ID"]];
    NSLog(@"test qry %@",testqry);
    
    NSArray *temp = [self fetchDataForQuery:testqry];
    return temp;
}

- (NSArray *)dbGetProductsOfCategory:(NSDictionary *)category {
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLProductListFromCategoriesNA, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]];
    
    NSLog(@"query for fetching products in sales order %@",[NSString stringWithFormat:kSQLProductListFromCategoriesNA, [category stringForKey:@"Org_ID"], [category stringForKey:@"Category"]]);
    
    return temp;
}

- (NSArray *)checkGenericPriceOfProduct:(NSString *)productID :(NSString*)PriceListID
{
    NSLog(@"query for checking price of product %@", [NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID='%@' AND  (Price_List_ID='%@' OR Is_Generic='Y') ORDER BY Is_Generic ASC ",productID,PriceListID]);
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID='%@' AND  (Price_List_ID='%@' OR Is_Generic='Y') ORDER BY Is_Generic ASC ",productID,PriceListID]];
    NSLog(@"tmp is %@", [temp description]);
    
    return temp;
}



- (NSArray *)checkGenericPriceOfProductwithUOM:(NSString *)productID UOM:(NSString*)SelectedUOM :(NSString*)priceListID
{
    NSString *trimmedString = [SelectedUOM stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];

    NSLog(@"trimmied str in generic price %@", trimmedString);
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID = '%@' and Item_UOM ='%@' and (Price_List_ID='%@' OR Is_Generic='Y')",productID,trimmedString,priceListID]];
    
    NSLog(@"query for fetching generic price %@",[NSString stringWithFormat:@"select * from TBL_Price_List where Inventory_Item_ID = '%@' and Item_UOM ='%@' and (Price_List_ID='%@' OR Is_Generic='Y')",productID,SelectedUOM,priceListID] );
    
    
    NSLog(@"price details from price list table %@", [temp description]);

    
    return temp;
}

- (NSMutableDictionary *)dbdbGetPriceListProduct:(NSString *)items
{
    return [self dbGetPriceOrProductWithID:items];
}

////////////////Dashboard////////////
-(NSArray *)dbGetTotalPlannedVisits{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    

    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    NSString* dummytimeStr=@" 00:00:00";
    
    NSString *dateString=[[dateformate stringFromDate:[NSDate date]] stringByAppendingString:dummytimeStr];
    
    
    NSLog(@"check time stamp %@", dateString);
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*)AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]]];
    
    formatter=nil;
    usLocale=nil;
    
    return temp;
}
-(NSArray *)dbGetTotalCompletedVisits{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total FROM TBL_FSR_Planned_Visits where Visit_Date Like '%%%@%%' AND Visit_Status = 'Y'", [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    
    return temp ;
}
-(NSArray *)dbGetTotalOrder{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString=[dateformate stringFromDate:[NSDate date] ];
    NSLog(@"check time stamp %@", dateString);

    
    NSString* totalOrderQry=[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where  Creation_Date like  '%%%@%%'  and Doc_Type='I'", dateString];
    NSArray *temp= [self fetchDataForQuery:totalOrderQry];
    
    formatter=nil;
    usLocale=nil;
    
    return temp;
}

-(NSArray *)dbGetTotalReturn {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    
    [dateformate setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString=[dateformate stringFromDate:[NSDate date]];
    NSLog(@"check time stamp %@", dateString);
    
    
    NSArray *temp= [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT  COUNT(*) AS Total from TBL_Order_History Where Creation_Date like  '%%%@%%' and Doc_Type='R'", dateString]];
    formatter=nil;
    usLocale=nil;
    return temp;
}

-(NSArray *)dbGetTotalCollection{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(*) AS Total  FROM TBL_Collection Where strftime('%%m', `Collected_On`) = '%@'", [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    return temp;
}

-(NSArray *)dbGetSaleTrend {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"MM"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];

    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(Row_ID)  AS Total , DATE(Creation_Date) ,SUM(Transaction_Amt) As Ammount FROM TBL_Order_History Where strftime('%%m', `Creation_Date`) = '%@' AND Doc_Type ='I' GROUP BY DATE(Creation_Date)", [formatter stringFromDate:[NSDate date]]]];
    
    formatter=nil;
    usLocale=nil;
    return temp;
}
-(NSArray *)dbGetCustomerStatus:(NSString *)customer {
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"Select Credit_Hold , Cust_Status , Cash_Cust from TBL_Customer where Customer_ID =   '%@'",customer]];
    return temp;
}

-(NSArray *)dbGetCustomerSales:(NSString *)customer {
    NSArray *temp= [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT COUNT(Row_ID)  AS Total FROM TBL_Sales_History Where Inv_To_Customer_ID =  '%@'",customer]];
    return temp;
}
-(NSArray *)dbGetCustomerOutstanding:(NSString *)customer {
    NSArray *temp=  [self fetchDataForQuery:[NSString stringWithFormat:@"Select Total_Due AS Total from TBL_Customer_Dues where Customer_ID = '%@'",customer]];
    return temp;
}
-(NSArray *)dbGetListofCustomer{
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT B.Customer_Name ,A.Customer_ID , A.Site_Use_ID,A.Start_Time AS Visit_Start_Time  FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID AND A.Site_Use_ID = B.Site_Use_ID   WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , B.Customer_Name ASC", [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    return temp;
}

-(NSArray *)dbGgtDetailofCustomer:(NSString *)customerID andSite:(NSString *)siteID {
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.*,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.* ,A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  WHERE A.Customer_ID = '%@' AND A.Site_Use_ID='%@'",customerID,siteID ]];
    return temp;
}

-(NSArray *)dbGetCustomerOutOfRoute {
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSLog(@"out of route query %@",[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]] );
    

    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:@"SELECT Customer_ID , Site_Use_ID FROM TBL_FSR_Actual_Visits where Visit_Start_Date Like '%%%@%%'", [formatter stringFromDate:[NSDate date]]]];
    
    
    formatter=nil;
    usLocale=nil;
    return [self CalculateOutOfRouteVisits:temp];
}

-(NSMutableArray *)CalculateOutOfRouteVisits:(NSArray *)arrayResult
{
    NSMutableArray *actualVisitArray = [NSMutableArray arrayWithArray:arrayResult];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    

    
    NSLog(@"check out of route query %@",[NSString stringWithFormat:kSQLRoute, [formatter stringFromDate:[NSDate date]]] );
    
    
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLRoute, [formatter stringFromDate:[NSDate date]]]];
    formatter=nil;
    usLocale=nil;
    
    int countValue=0;
    if(actualVisitArray.count > 0)
    {
        for (int i=0; i<[temp count]; i++)
        {
            @autoreleasepool{
                NSDictionary *PV = [NSDictionary dictionaryWithDictionary:[temp objectAtIndex:i]];
                for (int j=0; j<[actualVisitArray count]; j++)
                {
                    @autoreleasepool{
                        NSDictionary *AV = [NSDictionary dictionaryWithDictionary:[actualVisitArray objectAtIndex:j]];
                        if([[PV stringForKey:@"Customer_ID"] isEqualToString:[AV stringForKey:@"Customer_ID"]] && [[PV stringForKey:@"Site_Use_ID"] isEqualToString:[AV stringForKey:@"Site_Use_ID"]])
                        {
                            countValue++;
                        }
                    }
                }
            }
        }
        int resultCount = [actualVisitArray count]-countValue;
        [actualVisitArray removeAllObjects];
        [actualVisitArray addObject:[NSString stringWithFormat:@"%d",resultCount]];
        return actualVisitArray;
    }
    else
    {
        return actualVisitArray;
    }
}

///////////////////////Session/////////////////
- (NSString *)verifyLogin:(NSString *)login andPassword:(NSString *)password
{
    NSString *shaPass = [self digest:password];
    NSLog(@" user : %@ SHA : %@",login,[shaPass uppercaseString]);
    NSString *query = [NSString stringWithFormat:kSQLLogin, login, [shaPass uppercaseString]];

    NSArray *temp =[self fetchDataForQuery:query];
    if (temp.count == 0)
    {
        return @"Invalid login or password.";
        //return @"Done";

    }
    else
    {
        [SWDefaults setUserProfile:[temp objectAtIndex:0]];
        return @"Done";
    }
}

-(NSString *) encrypt:(NSString *)plaintext andKey:(NSString *)keytext
{
    NSData *data;
    NSData *key;
    
    int BLOCK_SIZE = 6 ;
    int data_lenght,key_lenght ;
    
    data = [plaintext dataUsingEncoding:NSUTF8StringEncoding];
    key = [keytext dataUsingEncoding:NSUTF8StringEncoding];
    
    data_lenght = [data length];
    key_lenght = [key length];
    
    NSMutableData *Mdata = [data mutableCopy];
    NSMutableData *Mkey = [key mutableCopy];
    
    char *dataBytes = (char *) [Mdata mutableBytes];
    char *keyBytes = (char *) [Mkey mutableBytes];
    
    int y =0;
    int i = 0 ;
    
    
    while (i <= data_lenght) {
        for (int x=0 ; x <= BLOCK_SIZE; x++)
        {
            if (i < data_lenght) {
                dataBytes[i] =  dataBytes[i] ^ keyBytes[y];
            }
            i+=1;
        }
        
        y+=1;
        if (y== key_lenght)
        {
            y=0;
        }
        
    }
    
    NSData *objData = [NSData dataWithBytes:dataBytes length:data_lenght];
    return [objData base64EncodedString];
}
-(NSString *) decrypt:(NSString *)encryptedtext andKey:(NSString *)keytext
{
    NSData *data;
    NSData *key;
    
    int BLOCK_SIZE = 6 ;
    int data_lenght,key_lenght ;
    
    key = [keytext dataUsingEncoding:NSUTF8StringEncoding];
    key_lenght = [key length];
    NSMutableData *Mkey = [key mutableCopy];
    char *keyBytes = (char *) [Mkey mutableBytes];
    
    data = [NSData dataFromBase64String:encryptedtext];
    data_lenght = [data length];
    NSMutableData *Mdata = [data mutableCopy];
    char *dataBytes = (char *) [Mdata mutableBytes];
    
    int y =0;
    int i = 0 ;
    
    while (i <= data_lenght) {
        for (int x=0 ; x <= BLOCK_SIZE; x++)
        {
            if (i < data_lenght) {
                dataBytes[i] =  dataBytes[i] ^ keyBytes[y];
            }
            i+=1;
        }
        
        y+=1;
        if (y== key_lenght)
        {
            y=0;
        }
        
    }
    NSData *objData = [NSData dataWithBytes:dataBytes length:data_lenght];
    NSString* newStr = [[NSString alloc] initWithData:objData encoding:NSUTF8StringEncoding];
    return newStr;
}

-(NSString*) digest:(NSString*)input
{
    NSString *hashkey = input;
    // PHP uses ASCII encoding, not UTF
    const char *s = [hashkey cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData;
    if (s != NULL) {
        keyData = [NSData dataWithBytes:s length:strlen(s)];
    } else {
        return @"";
    }
    
    // This is the destination
    uint8_t digest[CC_SHA1_DIGEST_LENGTH] = {0};
    // This one function does an unkeyed SHA1 hash of your hash data
    CC_SHA1(keyData.bytes, keyData.length, digest);
    
    // Now convert to NSData structure to make it usable again
    NSData *out = [NSData dataWithBytes:digest length:CC_SHA1_DIGEST_LENGTH];
    // description converts to hex but puts <> around it and spaces every 4 bytes
    NSString *hash = [out debugDescription];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    [hash uppercaseString];
    return hash;
}

-(void)writeXML
{
    float x = 0.0, y = 1.0, z = 2.0;
    NSString *name = @"Ansasri";
    
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver setOutputFormat:NSPropertyListXMLFormat_v1_0];
    [archiver encodeFloat:x forKey:@"x"];
    [archiver encodeFloat:y forKey:@"y"];
    [archiver encodeFloat:z forKey:@"z"];
    [archiver encodeObject:name forKey:@"name"];
    [archiver finishEncoding];
    //BOOL result = [data writeToFile:@"MyFile" atomically:YES];
    
    
    
    
    
    
    
    
    //iOS 8 support
    NSString *storePath;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        storePath=[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"sample.xml"];
    }
    
    else
    {
        NSString *applicationDocumentsDir =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        storePath = [applicationDocumentsDir stringByAppendingPathComponent:@"sample.xml"];
    }

    
    
    
    
   
    
    // write to file atomically (using temp file)
    [data writeToFile:storePath atomically:TRUE];
    //[archiver release];
}

///////Customer//////////////////////

- (void)cancel
{
    shouldStop=YES;
}

- (NSArray *)dbGetRecentOrders:(NSString *)customerId
{

    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerOrderHistory, customerId]];
    
    NSLog(@"customers are  %@", [temp description]);
    
    
    NSLog(@"check recent orders count %d", temp.count);
    
    return temp;
}
- (NSArray *)dbGetCashCutomerList
{

    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLCashCustomerListAll]];
    return  temp;
}
#define KSQLSumOfOrderAmount @"SELECT SUM(Order_Amt) AS OrderAmount FROM TBL_Order  WHERE Inv_To_Customer_ID='{0}'  "

- (NSArray *)dbGetOrderAmountForAvl_Balance:(NSString *)customerId
{

    NSArray *temp = [self fetchDataForQuery:[KSQLSumOfOrderAmount stringByReplacingOccurrencesOfString:@"{0}" withString:customerId]];
    return  temp;
}

- (NSArray *)dbGetPriceList:(NSString *)customerId
{
    NSString *filter = @"";
    
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
    {
        if ([[SWDefaults productFilterBrand] length] > 0)
        {
            filter = [NSString stringWithFormat:@"AND Brand_Code = '%@'", [SWDefaults productFilterBrand]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        if ([[SWDefaults productFilterProductID] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        if ([[SWDefaults productFilterName] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
        }
    }

    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerPriceList, customerId,filter]];
    return temp;
}

- (NSArray *)dbGetPriceListGeneric:(NSString *)customerId
{
    NSString *filter = @"";
    
    if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Brand", nil)])
    {
        if ([[SWDefaults productFilterBrand] length] > 0)
        {
            filter = [NSString stringWithFormat:@"AND Brand_Code = '%@'", [SWDefaults productFilterBrand]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Code", nil)])
    {
        if ([[SWDefaults productFilterProductID] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Item_Code LIKE '%%%@%%'", [SWDefaults productFilterProductID]];
        }
    }
    else  if ([[SWDefaults filterForProductList]isEqualToString:NSLocalizedString(@"Item Name", nil)])
    {
        if ([[SWDefaults productFilterName] length] > 0) {
            filter = [NSString stringWithFormat:@"AND Description LIKE '%%%@%%'", [SWDefaults productFilterName]];
        }
    }
    

    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerPriceListGeneric, customerId,filter]];
    return temp;
}

- (NSArray *)dbGetCollection {
    
    NSString *filter = @"";
    NSString *tableName = @"TBL_Customer_Ship_Address";
    
    if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Name", nil)])
    {
        
        if ([[SWDefaults nameFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Name LIKE '%%%@%%'", [SWDefaults nameFilterForCustomerList]];
            
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Code", nil)])
    {
        
        if ([[SWDefaults codeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_No LIKE '%%%@%%'", [SWDefaults codeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:@"BarCode"])
    {
        
        if ([[SWDefaults barCodeFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.Customer_Barcode = '%@'", [SWDefaults barCodeFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Location", nil)])
    {
        
        if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
            filter = [NSString stringWithFormat:@"WHERE A.City = '%@'", [SWDefaults locationFilterForCustomerList]];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Status", nil)])
    {
        if ([[SWDefaults statusFilterForCustomerList] length] > 0) {
            NSString *queryParam = @"";
            if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Open"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults statusFilterForCustomerList] isEqualToString:@"Blocked"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE A.Cust_Status = '%@'",queryParam];
        }
    }
    else if ([[SWDefaults filterForCustomerList]isEqualToString:NSLocalizedString(@"Payment", nil)])
    {
        
        if ([[SWDefaults paymentFilterForCustomerList] length] > 0)
        {
            NSString *queryParam = @"" ;
            if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Cash"])
            {
                queryParam = @"Y";
            }
            else if([[SWDefaults paymentFilterForCustomerList] isEqualToString:@"Credit"])
            {
                queryParam = @"N";
            }
            filter = [NSString stringWithFormat:@"WHERE B.Cash_Cust = '%@'",queryParam];
            NSLog(@"filter is %@", filter);
        }
    }
    
    
    
    
    
    
    
    //handling null lat longs
    
    
    NSString* custListQry=@"SELECT A.*,IFNULL(A.Cust_Lat,0)AS Lat,IFNULL(A.Cust_Long,0)AS Long,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone, B.Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, B.Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM %@ AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  %@ ORDER BY A.Customer_Name";
    
    
    
    
    NSLog(@"check query %@", [NSString stringWithFormat:custListQry,tableName ,filter]);
    

    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:custListQry,tableName ,filter]];
    return temp;
}

- (NSArray *)dbGetFilterByColumnCustomer:(NSString *)columnName {

    NSArray *temp = [self fetchDataForQuery:kSQLCUstomerLocationFilter];
    return  temp;
}

- (NSArray *)dbGetTarget:(NSString *)customerCode {
    NSDate *date = [NSDate date];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSMonthCalendarUnit fromDate:date];
    NSArray *temp;
    
    
   NSArray* appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"]);
    
    
    
    NSString* tgtStr=[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0];
    
    
    if (tgtStr==(id)[NSNull null]|| tgtStr.length==0) {
        
    
    
    }

    
    
   else if ([[[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"] objectAtIndex:0] isEqualToString:@"Y"] )
    
    {
        
        NSLog(@"This is Al seer");
        
        
        NSLog(@"check query %@",[NSString stringWithFormat:kSQLCustomerTargetAlseer, customerCode, [comps month]] );
        
        
        temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerTargetAlseer, customerCode, [comps month]]];

    }
    
    else
    {
        temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLCustomerTargetGeneral, customerCode, [comps month]]];

    }
    
    return temp;
}

- (NSArray *)dbGetCustomerProductSalesWithCustomerID:(NSString *)custID andProductId:(NSString *)productID
{

    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Customer_Sales_Data where Cust_ID='%@' and Item_ID='%@'", custID,productID]];
    return temp;
}

//////////////////////Distribution/////////////
#define kSQLDictributionChecktInfo @"INSERT INTO TBL_Distribution_Check (DistributionCheck_ID, Customer_ID, Site_Use_ID, SalesRep_ID, Emp_Code, Checked_On, Visit_ID, Status) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}')"

#define kSQLDictributionItemInfo @"INSERT INTO TBL_Distribution_Check_Items(DistributionCheckLine_ID, DistributionCheck_ID, Inventory_Item_ID, Is_Available, Qty, Expiry_Dt, Display_UOM, Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Reorder) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}', '{9}', '{10}')"


- (NSArray *)dbGetDistributionCollection {
    //[query setQuery:kSQLDistributionCollection];
    //[query setTag:kQueryTypeCollection];

    NSArray *temp = [self fetchDataForQuery:kSQLDistributionCollection];
    
    
    
    NSLog(@"query for fetching distribution check %@", kSQLDistributionCollection);
    
    
    NSMutableArray *results = [NSMutableArray array];
    
    NSLog(@"distribution check results %@", [results description]);
    
    for (NSDictionary *row in temp) {
        @autoreleasepool{
            NSMutableDictionary *newROW = [NSMutableDictionary dictionaryWithDictionary:row];
            [newROW setValue:@"" forKey:@"Qty"];
            [newROW setValue:@"" forKey:@"Avl"];
            [newROW setValue:@"" forKey:@"ExpDate"];
            [results addObject:newROW];
        }
    }
    
    
    NSMutableArray * DCLineItemArray=[[NSMutableArray alloc]init];
    NSMutableDictionary* customerDict=[SWDefaults customer];
    
    NSString* customerID=[customerDict valueForKey:@"Customer_ID"];
    
    
    NSString* customerMSlQry=[NSString stringWithFormat:@"select IFNULL(C.Sequence,'0') AS Sequence,P.Item_Code,P.Description,P.Inventory_Item_ID, P.Primary_UOM_Code,P.Organization_ID from TBL_Product AS P  inner join TBL_Customer_MSL AS C on P.Inventory_Item_ID = C.Inventory_Item_ID  and C.Organization_ID=P.Organization_ID where C.Customer_ID='%@' Order by C.Sequence ASC",customerID ];
    
    NSLog(@"new query for fetching distribution check %@", customerMSlQry);
    
    
    
    NSMutableArray* distResp=[[SWDatabaseManager retrieveManager]fetchDataForQuery:customerMSlQry];
    
    if (distResp.count>0) {
        for (NSInteger i=0; i<distResp.count; i++)
        {
            //fetch product details
            NSDictionary * DCLineItem=[distResp objectAtIndex:i];
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[DCLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[DCLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                DistriButionCheckItem * DCItem=[[DistriButionCheckItem alloc]init];
                DCItem.DcProduct=[productDetails objectAtIndex:0];
                [DCLineItemArray addObject:DCItem];
                
            }
        }
        return DCLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
    //return temp;
}
#define kSQLcheckCurrentVisitID @"SELECT DistributionCheck_ID FROM TBL_Distribution_Check WHERE Visit_ID = '%@'"
- (NSArray *)checkCurrentVisitInDC:(NSString *)visitID{

    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLcheckCurrentVisitID,visitID]];
    return temp;
}

-(NSMutableArray*)fetchDCMinStockOfMSLItems
{
    NSMutableArray *DCLineItemArray = [[NSMutableArray alloc]init];
    
    NSMutableArray *DCMinStockArray = [self fetchDataForQuery:@"select IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Organization_ID, 'N/A') AS Organization_ID, IFNULL(Attrib_Value,'N/A') AS Attrib_Value from TBL_Product_Addl_Info where Attrib_Name = 'DC_MIN_STOCK'"];
    if (DCMinStockArray.count>0) {
        for (NSInteger i=0; i<DCMinStockArray.count; i++)
        {
            //fetch product details
            NSDictionary *DCLineItem = [DCMinStockArray objectAtIndex:i];
            
            DistriButionCheckMinStock *DCItem = [[DistriButionCheckMinStock alloc]init];
            DCItem.InventoryItemID = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Inventory_Item_ID"]];
            DCItem.organizationID = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Organization_ID"]];
            DCItem.attributeValue = [SWDefaults getValidStringValue:[DCLineItem valueForKey:@"Attrib_Value"]];
            
            [DCLineItemArray addObject:DCItem];
        }
        return DCLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}
-(NSMutableArray*)fetchDistributionCheckItemsForcustomer
{
    NSMutableArray * DCLineItemArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* DCLineItemsArray=[self fetchDataForQuery:@"select * from TBL_Product_MSL"];
    if (DCLineItemsArray.count>0) {
        for (NSInteger i=0; i<DCLineItemsArray.count; i++)
        {
            //fetch product details
            NSDictionary * DCLineItem=[DCLineItemsArray objectAtIndex:i];
            NSMutableArray* productDetails=[self fetchProductDetailsForInventoryItemId:[DCLineItem valueForKey:@"Inventory_Item_ID"] AndOrganization_Id:[DCLineItem valueForKey:@"Organization_ID"]] ;
            if(productDetails.count>0)
            {
                DistriButionCheckItem * DCItem=[[DistriButionCheckItem alloc]init];
                DCItem.DcProduct=[productDetails objectAtIndex:0];
                [DCLineItemArray addObject:DCItem];
                
            }
        }
        return DCLineItemArray;
    }
    else
    {
        return [[NSMutableArray alloc]init];
    }
}
-(NSMutableArray*)fetchProductDetailsForInventoryItemId:(NSString*)inventory_Item_ID AndOrganization_Id:(NSString*)productOrganizationId
{
    NSString* productDetailsQry=[NSString stringWithFormat:@"Select IFNULL(D.Get_Qty,'0')AS Bonus,E.* from (SELECT A.Primary_UOM_Code As UOM,A.Inventory_Item_ID,IFNULL(A.Discount,'0') AS Discount,IFNULL(SUM(B.Lot_Qty),'0')AS Lot_Qty, A.Item_Code,A.Category,A.Agency,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'    ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'    ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A  left join TBL_Product_Stock AS B on A.Inventory_Item_ID=B.Item_ID LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID   GROUP by   A.Inventory_Item_ID order by A.Description) AS E left join (SELECT * FROM TBL_BNS_Promotion GROUP BY Item_Code) AS D on E.Item_Code=D.Item_Code  where E.Inventory_Item_ID='%@' AND orgID='%@'  group by E.Item_Code order by E.Description asc", inventory_Item_ID,productOrganizationId];
    NSMutableArray* selectedProductArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* tempProductArray=[self fetchDataForQuery:productDetailsQry];
    
    if (tempProductArray.count>0) {
        
        for (NSInteger i=0; i<tempProductArray.count;i++ ) {
            
            NSMutableDictionary * currentProductDict=[tempProductArray objectAtIndex:i];
            
            
            
            Products * currentProduct=[[Products alloc]init];
            currentProduct.Brand_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.selectedUOM=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"UOM"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[NSString stringWithFormat:@"%@",[currentProductDict valueForKey:@"Inventory_Item_ID"]];
            
            currentProduct.bonus=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Bonus"]];
            currentProduct.stock=[NSString stringWithFormat:@"%@", [currentProductDict valueForKey:@"Lot_Qty"]];
            
            [selectedProductArray addObject:currentProduct];
            
        }
        
    }
    return selectedProductArray;
    
}
-(NSMutableArray *)fetchDistriButionCheckLocations;
{
    NSMutableArray * DCLocationsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray* DClocArray=[self fetchDataForQuery:@"Select * from TBL_App_Codes  where code_type ='DIST_CHECK_LOCATIONS'"];
    if (DClocArray.count>0) {
        for (NSInteger i=0; i<DClocArray.count; i++)
        {
            //fetch product details
            NSDictionary * DCloc=[DClocArray objectAtIndex:i];
            DistriButionCheckLocation * DCLocation=[[DistriButionCheckLocation alloc]init];
            DCLocation.LocationID=[DCloc valueForKey:@"Code_Value"];
            DCLocation.LocationName=[DCloc valueForKey:@"Code_Description"];
            [DCLocationsArray addObject:DCLocation];
        }
        return DCLocationsArray;
    }
    else
    {
        
        DistriButionCheckLocation * DCLocation=[[DistriButionCheckLocation alloc]init];
        DCLocation.LocationID=@"S";
        DCLocation.LocationName=@"Shelf";
        [DCLocationsArray addObject:DCLocation];
        
        return DCLocationsArray;
    }
}


- (NSString *)saveDistributionCheckWithCustomerInfo:(NSMutableDictionary *)customerInfo andDistChectItemInfo:(NSMutableArray *)fetchDistriButionCheckLocations
{
    NSString *DistributionCheckID = [NSString createGuid];
    NSDictionary *FSR = [SWDefaults userProfile];
    
    NSString *temp = kSQLDictributionChecktInfo;
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:DistributionCheckID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[self fetchCurrentDateTimeinDatabaseFormat]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[SWDefaults currentVisitID]];
    /*pavan:24-02-2015 status should be always "N" while saving the data(//test/backoffice)*/
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:@"N"];
    
    
    [self executeNonQuery:temp];
    
    
    for (DistriButionCheckLocation *DCLocation in fetchDistriButionCheckLocations) {
        for (DistriButionCheckItem *DCItem in DCLocation.dcItemsArray)
        {
            for (int i =0; i<[DCItem.dcItemLots count]; i++) {
                DistriButionCheckItemLot *DCItemLot = [DCItem.dcItemLots objectAtIndex:i];
                
                if (DCItem.imageName.length >0 && i == 0) {
                    DCItemLot.DistriButionCheckItemLotId = DCItem.imageName;
                }
                else{
                    DCItemLot.DistriButionCheckItemLotId = [NSString createGuid];
                }
                
                NSString *sql = kSQLDictributionItemInfo;
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:DCItemLot.DistriButionCheckItemLotId];
                sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:DistributionCheckID];
                sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[SWDefaults getValidStringValue:DCItem.DcProduct.Inventory_Item_ID]];
                
                if ([[AppControl retrieveSingleton].ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
                {
                    if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItemLot.itemAvailability]];
                    } else {
                        if (DCItemLot.Quntity.length > 0) {
                            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:@"Y"];
                        }
                        else{
                            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:@"N"];
                        }
                    }
                }
                else{
                    if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItemLot.itemAvailability]];
                    } else {
                        sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[SWDefaults getValidStringValue:DCItem.itemAvailability]];
                    }
                }
                
                if ([[AppControl retrieveSingleton].DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]) {
                  sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[SWDefaults getValidStringValue:DCItem.Quntity].length == 0 ? @"0" : [SWDefaults getValidStringValue:DCItem.Quntity]];
                }else{
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[SWDefaults getValidStringValue:DCItemLot.Quntity].length == 0 ? @"0" : [SWDefaults getValidStringValue:DCItemLot.Quntity]];
                }
                NSString *refinedExpiryDate;
               if ([[AppControl retrieveSingleton].FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                   refinedExpiryDate = [MedRepDefaults refineDateFormat:kDatabaseFormatDC destFormat:kDatabseDefaultDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:DCItemLot.expiryDate]];
                   
               }else{
                   if ([[AppControl retrieveSingleton].DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]){
                       refinedExpiryDate = DCItem.expiryDate;
                   }else{
                       refinedExpiryDate = DCItemLot.expiryDate;
                   }
               }
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[SWDefaults getValidStringValue:refinedExpiryDate]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[SWDefaults getValidStringValue:DCItem.DcProduct.primaryUOM]];
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[SWDefaults getValidStringValue:DCItemLot.LotNumber]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:DCLocation.LocationID];
                
                if (DCItem.imageName.length >0) {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@"Y"];
                } else {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@"N"];
                }
                
                if ([[AppControl retrieveSingleton].FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[SWDefaults getValidStringValue:DCItemLot.reOrder]];
                } else {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:@"N"];
                }
                
                [self executeNonQuery:sql];
            }
        }
    }
    
    return DistributionCheckID;
}


////////////////////SalesOrder/////////////////
#define kSQLSaveOrder @"INSERT INTO TBL_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number,Shipping_Instructions,Custom_Attribute_4) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}' ,'{23}','{24}','{25}' ,'{26}','{27}','{28}','{29}','{30}')"

#define kSQLSaveOrderLineItem @"INSERT INTO TBL_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID, Unit_Selling_Price, Def_Bonus,Calc_Price_Flag, Custom_Attribute_3,Custom_Attribute_1,Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}','{9}' ,'{10}','{11}','{12}')"

#define kSQLSaveOrderLotItem @"INSERT INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

#define kSQLSavePerformaOrder @"INSERT INTO TBL_Proforma_Order (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Start_Time, Emp_Code,Order_Amt,Visit_ID , Custom_Attribute_2,Credit_Customer_ID,Credit_Site_ID,Custom_Attribute_3,Last_Update_Date,Last_Updated_By) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}' ,'{10}','{11}' ,'{12}','{13}','{14}','{15}')"

//TODO: change this also. OLA!!
#define kSQLSavePerfomrmaLineItem @"INSERT INTO TBL_Proforma_Order_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Order_Quantity_UOM, Display_UOM, Ordered_Quantity, Inventory_Item_ID,Unit_Selling_Price,Def_Bonus,Calc_Price_Flag,Custom_Attribute_3,Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}' , '{9}', '{10}','{11}' )"

#define kSQLSavePerfomrmaLotItem @"INSERT INTO TBL_Proforma_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

#define kSQLSaveOrderHistory @"INSERT INTO TBL_Order_History (Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{14}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}','{23}' )"

//#define kSQLSaveOrderHistoryLineItem @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' ,'{8}')"

/*modified on 25-02-2015 : adding lot no and expiry date*/
#define kSQLSaveOrderHistoryLineItem @"INSERT INTO TBL_Order_History_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number, Inventory_Item_ID, Order_Quantity_UOM, Ordered_Quantity, Calc_Price_Flag, Unit_Selling_Price ,Item_Value,Lot_No,Expiry_Date) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' ,'{8}','{9}' ,'{10}')"


#define kSQLdbGetTemplateOrder @"Select * from TBL_Order_Template"
#define kSQLdbGetTemplateOrderItems @"SELECT A.*, B.* FROM TBL_Order_Template_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Order_Template_ID='%@'"
#define kSQLDeletePerformaOrder @"DELETE FROM TBL_Proforma_Order WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLDeletePerformaOrderItems @"DELETE FROM TBL_Proforma_Order_Line_Items WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLDeletePerformaOrderLots @"DELETE FROM TBL_Proforma_Order_Lots WHERE Orig_Sys_Document_Ref='{0}'"
#define kSQLdbGetConfirmOrder @"Select * from TBL_Order where Ship_To_Customer_ID = '%@' AND Ship_To_Site_ID = '%@'"

// TODO: change this. OLA!!
#define kSQLPerformaOrderItems @"SELECT A.*, B.* FROM TBL_Proforma_Order_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLConfirmOrderItems @"SELECT A.*, B.* FROM TBL_Order_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLInsertTemplateOrder  @"INSERT INTO TBL_Order_Template (Order_Template_ID, Template_Name, SalesRep_ID, Custom_Attribute_1, Custom_Attribute_2) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')"

#define kSQLInsertTemplateOrderItem @"INSERT INTO TBL_Order_Template_Items (Template_Line_ID, Order_Template_ID, Inventory_Item_ID, Organization_ID, Quantity) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')"

#define kSQLSaveReturnOrder @"INSERT INTO TBL_RMA (Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID,Internal_Notes, Customer_Ref_No, Order_Status, Start_Time, End_Time,Last_Update_Date ,Last_Updated_By, Signee_Name, Order_Amt, Visit_ID ,Emp_Code,Credit_Customer_ID , Credit_Site_ID ,Invoice_Ref_No ,Reason_Code , Custom_Attribute_2,Custom_Attribute_4,Custom_Attribute_1,Custom_Attribute_3) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{14}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')"

#define kSQLSaveReturnLineItem @"INSERT INTO TBL_RMA_Line_Items (Row_ID, Orig_Sys_Document_Ref, Line_Number,Return_Quantity_UOM, Display_UOM, Returned_Quantity, Inventory_Item_ID,Unit_Selling_Price,Custom_Attribute_2,Custom_Attribute_1) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}','{8}','{9}')"

#define kSQLSaveReturnLotItem @"INSERT INTO TBL_RMA_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Inventory_Item_ID,Returned_Quantity , Expiry_Dt ,Lot_Type) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}' )"

#define kSQLdbGetPerformaOrder @"Select * from TBL_Proforma_Order where Ship_To_Customer_ID = '%@' AND Ship_To_Site_ID = '%@'"
#define kSQLGetAllPerformaOrder @"Select A.*,B.Customer_Name,B.Customer_ID,B.Site_Use_ID from TBL_Proforma_Order AS A INNER JOIN TBL_Customer As B ON A.Ship_To_Customer_ID=B.Customer_ID"



static NSString *BonusCA1GUID;
- (NSArray *)dbGetSalesOrderItems:(NSString *)refId {
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLOrderInvoiceItems, refId]];
    return temp;
}

- (NSArray *)dbGetSalesOrderInvoice:(NSString *)refId {
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLOrderInvoice, refId]];
    return temp;
}

- (NSArray *)dbGetInvoicesOfCustomer:(NSString *)customerId withSiteId:(NSString *)siteId andShipCustID:(NSString *)sCusId withShipSiteId:(NSString *)sSiteId andIsStatement:(BOOL)isStatement {
    
    NSString *q = kSQLOrderAllInvoices;
    q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:customerId];
    q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:siteId];
    q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:sCusId];
    q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:sSiteId];

    NSArray *temp = [self fetchDataForQuery:q];
    
    NSLog(@"paid amt description %@", [temp valueForKey:@"PaidAmt"]);
    //deduction total
    
    NSMutableArray * totalArray=[[temp valueForKey:@"PaidAmt"]mutableCopy];
                                 
                                 
   
    [totalArray removeObject:[NSNull null]];
    
    NSLog(@"total array after removing null objects %@",[totalArray description]);
    
    
    double sum=0;
    
    for (int i=0; i<[totalArray count]; i++) {
    
        
        sum=sum+[[totalArray objectAtIndex:i]doubleValue];
        
        
    }
    
    NSLog(@"grand sum %f", sum);
    
    
    [[NSUserDefaults standardUserDefaults]setDouble:sum forKey:@"totalAmountPaid"];
    
    
    
   
    
    
    
    
    
    
    
    if (isStatement) {
        return temp;
    }
    else
    {
    NSMutableArray *filteredResultSet = [NSMutableArray array];
    for (NSDictionary *row in temp) {
        @autoreleasepool{
            int foundat = -1;
            for (int i = 0; i < filteredResultSet.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *r = [filteredResultSet objectAtIndex:i];
                    if ([[r stringForKey:@"C.Invoice_Ref_No"] isEqualToString:[row stringForKey:@"C.Invoice_Ref_No"]]) {
                        double netamount = [[r stringForKey:@"PaidAmt"] doubleValue];
                        double paid = [[row stringForKey:@"PaidAmt"] doubleValue];
                        netamount = netamount + paid;
                        [r setValue:[NSNumber numberWithDouble:netamount] forKey:@"PaidAmt"];
                        [filteredResultSet replaceObjectAtIndex:i withObject:r];
                        foundat = i;
                        break;
                    }
                }
            }
            
            if (foundat > -1) {
            } else {
                [filteredResultSet addObject:row];
            }
            
        }
    }
    
    for (int i = filteredResultSet.count - 1; i >= 0; i--) {
        @autoreleasepool{
            NSDictionary *r = [filteredResultSet objectAtIndex:i];
            double netamount = [[r stringForKey:@"NetAmount"] doubleValue];
            double paid = [[r stringForKey:@"PaidAmt"] doubleValue];
            
            
//            if (netamount <= paid )
            if (netamount <= paid || netamount < 0) {
                [filteredResultSet removeObjectAtIndex:i];
            }
        }
    }
    
    //    if ([self.delegate respondsToSelector:@selector(salesOrderServiceDidGetAllInvoices:)]) {
    //        [self.delegate salesOrderServiceDidGetAllInvoices:filteredResultSet];
    //    }
    
    
    return filteredResultSet;
    }
}

- (NSArray *)dbGetPerformaOrder:(NSString *)customerId withSiteId:(NSString *)siteId
{
    NSLog(@"proforma order qry is %@", [NSString stringWithFormat:kSQLdbGetPerformaOrder, customerId , siteId]);
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetPerformaOrder, customerId , siteId]];
    return temp;
}
- (NSArray *)dbGetPerformaOrder{
    NSArray *temp = [self fetchDataForQuery:kSQLGetAllPerformaOrder];
    return temp;
}

- (NSArray *)dbGetConfirmOrder:(NSString *)customerId withSiteId:(NSString *)siteId
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetConfirmOrder, customerId , siteId]];
    return temp;
}

- (NSArray *)dbGetSalesConfirmOrderItems:(NSString *)refId
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLConfirmOrderItems, refId]];
    return temp;
}
- (NSArray *)dbGetSalesPerformaOrderItems:(NSString *)refId
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLPerformaOrderItems, refId]];
    return temp;
}

- (NSArray *)dbGetReturnsType
{
    NSArray *temp = [self fetchDataForQuery:@"select * from TBL_RMA_Lot_Types"];
    return temp;
}

- (NSArray *)dbGetReasonCode
{
    NSArray *temp = [self fetchDataForQuery:@"select * from TBL_Reason_Codes where Purpose = 'Returns'"];
    return temp;
}

- (NSArray *)dbGetTemplateOrder
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetTemplateOrder]];
    return temp;
}
- (NSArray *)dbGetTemplateOrderItem:(NSString *)refId
{
    NSArray *temp = [self fetchDataForQuery:[NSString stringWithFormat:kSQLdbGetTemplateOrderItems, refId]];
    return temp;
}

- (void)filterInvoices:(NSArray *)result {
}


- (NSString *)saveOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    NSString *orderID = [NSString createGuid];
    
    Singleton *single = [Singleton retrieveSingleton];

    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    
    NSLog(@"check nuserID %@", nUserId);
    
    
    if(nUserId.length == 1)
    {nUserId = [NSString stringWithFormat:@"00%@",nUserId];}
    else if(nUserId.length == 2)
    {nUserId = [NSString stringWithFormat:@"0%@",nUserId];}
    
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 6] longLongValue];
    lastRefId = lastRefId + 1;
    lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
    
    
    NSLog(@"check the required number %@", lastOrderReference);
    
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    //NSArray *lotArray = [NSArray aray] ;
    NSDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    
    NSString *temp = kSQLSaveOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}"  withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}"  withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:stringComments];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:dateString];
    
    NSLog(@"check extra info %@", [extraInfo description]);
    
    
    
    
    
    //hiding shipdate to insertcurrent date
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:dateString];

    
//    if ([extraInfo objectForKey:@"ship_date"]) {
//        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[formatter stringFromDate:[extraInfo objectForKey:@"ship_date"]]];
//    }
//    
//    else
//    {
//        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@"-"];
//    }
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:dateTimeString];
    
    
    //fetch signature from defaults
    
    id signatureInfo=[SWDefaults fetchSignature];
    
    
    if(signatureInfo)
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:Y]"];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:N]"];
    }
    //temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:[extraInfo stringForKey:@"signature"]];
    if([extraInfo objectForKey:@"name"])
    {
        NSString *stringName = [extraInfo stringForKey:@"name"];
        stringName = [stringName stringByReplacingOccurrencesOfString:@"'" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:stringName];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[infoOrder stringForKey:@"productCategory"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    
    //why is discount harded coded as 0.0
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:@"0.0"]; // discount amt
    
    
    NSString *stringReference = [extraInfo stringForKey:@"reference"];
    
    stringReference = [stringReference stringByReplacingOccurrencesOfString:@"'" withString:@""];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{28}" withString:stringReference];
    
    
    
    //inserting 0 in shipping instruction s as suggested by param
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{29}" withString:@"0"];

    
    
    
    //saving the order type, onsite/telephone in shipping instructions of tbl_order
    
    
    BOOL isOnsite=[SWDefaults fetchOnsiteVisitStatus];
    
    if (isOnsite==YES) {
        temp = [temp stringByReplacingOccurrencesOfString:@"{30}" withString:@"ONSITE"];
        
        
        
    }
    
    else
    {        temp = [temp stringByReplacingOccurrencesOfString:@"{30}" withString:@"TELEPHONIC"];
        
        
    }
    
    
    
    NSLog(@"check customer po number %@", stringReference);
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    

    
    [self executeNonQuery:temp];
    int line;
    for (int i = 0; i < orderArray.count; i++)
    {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            //NSLog(@"TEsting %@",row);
            NSArray *lotArray = [NSArray arrayWithArray:[row objectForKey:@"StockAllocation"]] ;
            
            //lotArray = [row objectForKey:@"StockAllocation"];
            line=i+1;
            
            
            NSString *sql = kSQLSaveOrderLineItem;
            
            
            
            
            
            
            
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];

            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
            
            
            
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
                
                //UOM based on UOM picker al Seer
                
                
                NSString* uomStr=[row stringForKey:@"SelectedUOM"];
                
                if ([[row stringForKey:@"SelectedUOM"]isEqual:[NSNull null]]|| uomStr.length==0) {
                    
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];

                    NSLog(@"inserted uom qry %@", sql);
                    
                    
                }
                else
                {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                    NSLog(@"inserted uom qry %@", sql);

                }
            }
            
            
            else
            {
            
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                
                NSLog(@"inserted uom qry %@", sql);

            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                NSLog(@"inserted uom qry %@", sql);

            }
                
            }
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
             
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
                            }
            
             NSString *zeroString=@"0";
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
            }
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
            
            NSString *stringPercent = [[row stringForKey:@"DiscountPercent"]stringByReplacingOccurrencesOfString:@"%" withString:@""];
            double discountPercent = 0.0;
            discountPercent = [stringPercent doubleValue] /(double)100.0;
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[NSString stringWithFormat:@"%.02f",discountPercent]];
            
            if([row objectForKey:@"ChildGuid"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else if([row objectForKey:@"ParentGuid"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[NSString createGuid]];
            }
        
            
            
            
            //adding line item notes(text view comments while capturing order)

            
            NSLog(@"add line item notes %@", [row valueForKey:@"lineItemNotes"]);
            
            
           if ([row valueForKey:@"lineItemNotes"]==nil ||[row stringForKey:@"lineItemNotes"].length==0) {
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
               
                
            }
            
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:[row valueForKey:@"lineItemNotes"]];
            }
        
            
            NSLog(@"check for notes %@", sql);
            
            
          [self executeNonQuery:sql];
            
//            BOOL checkStatus=[self executeNonQueryOne:sql];
//            
//            if (checkStatus==YES) {
//                
//                NSLog(@"db success");
//                
//                
//            }
            
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSaveOrderLotItem;
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                    
                        
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
            lotArray =nil;
        }
    }
    
    [self saveOrderHistoryWithCustomerInfo:customerInfo andOrderInfo:orderInfo andFSRInfo:userInfo andVisitID:visitID andOrderID:orderID andOrderRef:lastOrderReference];
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocaleq=nil;
    extraInfo=nil;
    infoOrder=nil;
    orderArray=nil;
    dict=nil;
    return lastOrderReference;
}
- (void)saveOrderHistoryWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderID:(NSString *)orderID andOrderRef:(NSString *)lastOrderReference{
    
    
    
    
    
   Singleton *single = [Singleton retrieveSingleton];
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString * dummyTimeStr=@" 00:00:00";
    
    NSString *dateString =  [formatter stringFromDate:[NSDate date]] ;
    dateString=[dateString stringByAppendingString:dummyTimeStr];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocaleq];
    
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    NSString *startTime =  [formatterTime stringFromDate:single.orderStartDate];
    
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    
    NSDateComponents *components=[gregCalendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    
    
    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    // NSArray *lotArray = [NSArray aray] ;
    NSMutableDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    if ([extraInfo objectForKey:@"ship_date"])
    {
        //NSLog(@"ShipDate");
    }
    else
    {
        [extraInfo setObject:[NSDate date] forKey:@"ship_date"];
        //NSLog(@"NO Shipdte");
    }
    NSString *temp = kSQLSaveOrderHistory;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[infoOrder stringForKey:@"order_Status"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[formatter stringFromDate:[extraInfo objectForKey:@"ship_date"]]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:dateTimeString];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:stringComments];
    temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:lastDateMonthString];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];

    
    
    
    NSLog(@"order item query to check transaction amount in return%@", [temp description]);
    
    
    //NSLog(@"Return %@",temp);
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            
            line=i+1;
            
            NSString *sql = kSQLSaveOrderHistoryLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            
            if([row stringForKey:@"lot"]==nil||[row stringForKey:@"lot"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:@""];

            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"lot"]];

            }
            
            NSLog(@"%@",[row stringForKey:@"EXPDate"]);
            if([row stringForKey:@"EXPDate"]==nil||[row stringForKey:@"EXPDate"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:@""];

                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"EXPDate"]];
                
            }

            
             if([row stringForKey:@"ItemID"]==nil||[row stringForKey:@"ItemID"].length==0) /**return items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Inventory_Item_ID"]];

             }
            else /**sales order or manage order items*/
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"ItemID"]];

            }
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {

             //save uom based on uom selection Al seer
                
                NSString* uomStr=[row stringForKey:@"SelectedUOM"];
                
                if ([[row stringForKey:@"SelectedUOM"]isEqual:[NSNull null]]|| uomStr.length==0) {
                    
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
                }
                
                else
                {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"SelectedUOM"]];
                }
                
            }
            
            else
            {
            
            
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];

            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }

            
            }
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql =  [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
                double iTemValue = [[row stringForKey:@"Qty"] doubleValue] * [[row stringForKey:@"Price"] doubleValue];
                
                NSLog(@"check item val in 1st item %f", iTemValue);
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.1f",iTemValue]];
                
                
                
                NSLog(@"check discounted price here before inserting %@", [row stringForKey:@"Discounted_Price"]);
                
                
                //adding discounted price to item_value which was not added earlier
                
                
               // sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Discounted_Price" ]];

                
                NSLog(@"check the item value %@", sql);
                
                
                if ([row objectForKey:@"priceFlag"])
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"priceFlag"]];
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:@"F"];
                }

            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
                
                
                double iTemValue = [[row stringForKey:@"Qty"] doubleValue] * [[row stringForKey:@"Net_Price"] doubleValue];
                NSLog(@"check the item value %@", sql);

                
                //taking discount into consideration
                
//                double discount=[[row stringForKey:@"Discounted_Price"]doubleValue];
//                
//                double totalVal=iTemValue-discount;
                double qty=[[row stringForKey:@"Qty"] doubleValue];
                
                double usp=[[row stringForKey:@"Net_Price"] doubleValue];
                
                double discountPrice=[[row stringForKey:@"DiscountPercent"] doubleValue];
                NSLog(@"check discount percent %f", discountPrice);
                
                
                
                double tempTotal= qty*usp;
                
                discountPrice=discountPrice/100;
                
                double totalMO=tempTotal*(1-discountPrice);
                
                NSLog(@"check total val %f", totalMO);
                
                
                //adding discounted price to item_value which was not added earlier

                NSLog(@"check discounted price here before inserting %@", [row stringForKey:@"Discounted_Price"]);

               sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.1f",totalMO]];
//                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Discounted_Price"] ];
                
                if ([row objectForKey:@"priceFlag"])
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"priceFlag"]];
                }
                else
                {
                    sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
                }
                
            }
        
            
            NSLog(@"check query %@", sql);
            
            
            [self executeNonQuery:sql];
        }
    }
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocaleq=nil;
    gregCalendar=nil;
    components=nil;
    infoOrder=nil;
    extraInfo=nil;
    orderArray=nil;
    dict=nil;
}
- (void)saveTemplateWithName:(NSString *)name andOrderInfo:(NSMutableArray *)orderInfo{
    NSString *orderID = [NSString createGuid];
    
    NSString *temp = kSQLInsertTemplateOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:name];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults productCategory] stringForKey:@"Category"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[[SWDefaults productCategory] stringForKey:@"Description"]];

    
    [self executeNonQuery:temp];
    
    for (int i = 0; i < orderInfo.count; i++)
    {
        @autoreleasepool
        {
            NSString *itemID = [NSString createGuid];
            NSDictionary *row = [orderInfo objectAtIndex:i];
            NSString *temp = kSQLInsertTemplateOrderItem;
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:itemID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"ItemID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults productCategory]stringForKey:@"Org_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Qty"]];
        
            
            [self executeNonQuery:temp];
        }
    }
}
- (void)deleteSalesPerformaOrderItems:(NSString *)refId{
    NSString *temp = kSQLDeletePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:refId];

    
    [self executeNonQuery:temp];
    
    NSString *temp1 = kSQLDeletePerformaOrderItems;
    temp1 = [temp1 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];

    
    [self executeNonQuery:temp1];
    
    NSString *temp2 = kSQLDeletePerformaOrderLots;
    temp2 = [temp2 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];

    
    [self executeNonQuery:temp2];
    
}
- (void)kSQLDeletePerformaOrderItemsFunction:(NSString *)refId{
    
    NSString *temp1 = kSQLDeletePerformaOrderItems;
    temp1 = [temp1 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];

    
    [self executeNonQuery:temp1];
    
    [self performSelector:@selector(kSQLDeletePerformaOrderLotsFunction:) withObject:refId afterDelay:1.0f];
    
}
- (void)kSQLDeletePerformaOrderLotsFunction:(NSString *)refId{
    NSString *temp2 = kSQLDeletePerformaOrderLots;
    temp2 = [temp2 stringByReplacingOccurrencesOfString:@"{0}" withString:refId];

    
    [self executeNonQuery:temp2];
}
- (NSString *)savePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    
    NSString *orderID = [NSString createGuid];
    
    NSString *lastPerformaOrderReference = [SWDefaults lastPerformaOrderReference];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastPerformaOrderReference substringFromIndex: [lastPerformaOrderReference length] - 6] longLongValue];
    lastRefId = lastRefId + 1;
    
    
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%006lld", nUserId, lastRefId];
    if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
    {
        [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%006lld", nUserId, lastRefId];
        if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastPerformaOrderReference]])
        {
            [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastPerformaOrderReference = [NSString stringWithFormat:@"M%@D%006lld", nUserId, lastRefId];
            [SWDefaults setLastPerformaOrderReference:lastPerformaOrderReference];
        }
    }
    
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
   Singleton *single = [Singleton retrieveSingleton];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocale];
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    
    
    NSString *temp = kSQLSavePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[infoOrder stringForKey:@"productCategory"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    
    
    [self executeNonQuery:temp];
    
    int line;
   //NSLog(@"OLA!!******* inserting in DB : orderItems %@", orderArray);
    
    for (int i = 0; i < orderArray.count; i++)
    {
        @autoreleasepool
        {
            NSDictionary *row = [orderArray objectAtIndex:i];
            //NSLog(@"TET %@",row);
            NSArray *lotArray = [NSArray arrayWithArray:[row objectForKey:@"StockAllocation"]] ;
            line=i+1;
            NSString *sql = kSQLSavePerfomrmaLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];}
            else{sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];}
            
            NSString *zeroString=@"0";
            
            
            //adding selected UOM in tbl proforma order
            
            
            //check for multi uom
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
               
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                
                
                NSLog(@"uom inserted to proforma order %@", sql);
                
                
                
            }
            
            
          else  if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
                
            }
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"DiscountPercent"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[row stringForKey:@"lineItemNotes"]];
            
            
            
            [self executeNonQuery:sql];
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool
                {
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSavePerfomrmaLotItem;
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
            lotArray=nil;
        }
    }
    
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    orderArray=nil;
    infoOrder=nil;
    dict=nil;
    return lastPerformaOrderReference;
}
- (NSString *)updatePerformaOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID andOrderRef:(NSString *)orderRef{
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    
    
   Singleton *single = [Singleton retrieveSingleton];
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocale1];
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    NSString *temp = kSQLSavePerformaOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[userInfo stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[infoOrder stringForKey:@"productCategory"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[infoOrder stringForKey:@"Org_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    
   //Singleton *single = [Singleton retrieveSingleton];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:@"0"];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }

    
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            NSArray *lotArray = [row objectForKey:@"StockAllocation"];
            
            line=i+1;
            
            NSString *sql = kSQLSavePerfomrmaLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];

            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"ItemID"]];
           
            
            
            
            
            
            
            
            
            //adding selected UOM in tbl proforma order
            
            
            //check for multi uom
            
            
            if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
                
                
                
                Singleton *single = [Singleton retrieveSingleton];
                
                NSLog(@"check for order type? %@", single.visitParentView);
                
                
                if ([single.visitParentView isEqualToString:@"MO"]) {
                    
                    
                    
                    
                    
                   // NSString* orderQtyUom=[[[orderInfo valueForKey:@"OrderItems"] valueForKey:@"Order_Quantity_UOM"] objectAtIndex:0];
                    
                    NSString* orderQtyUom=  [ NSString stringWithFormat:@"%@",[[[orderInfo valueForKey:@"OrderItems"]objectAtIndex:i] valueForKey:@"Order_Quantity_UOM"]];
                    NSString* displayUOMStr= [NSString stringWithFormat:@"%@", [[[orderInfo valueForKey:@"OrderItems"]objectAtIndex:i] valueForKey:@"Display_UOM"]];

                    
                    
                   // NSString* displayUOMStr=[[[orderInfo valueForKey:@"OrderItems"] valueForKey:@"Display_UOM"] objectAtIndex:0];
                    
                    
                    sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:orderQtyUom];
                    sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:displayUOMStr];
                    
                }
                
                else
                {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
                }
                
                NSLog(@"uom inserted to proforma order %@", sql);
                
                
                
            }
            
            
            else  if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            

            
            
            /**
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"UOM"]];
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
             
             
             **/
            
            
            if([[row stringForKey:@"priceFlag"] isEqualToString:@"N"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
  
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Price"]];
             
            }
             NSString *zeroString=@"0";
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Qty"]];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:zeroString];
            }
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"priceFlag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"DiscountPercent"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:[row stringForKey:@"lineItemNotes"]];
        
            
            
            [self executeNonQuery:sql];
            
            for (int i = 0; i < lotArray.count; i++)
            {
                @autoreleasepool{
                    NSDictionary *row = [lotArray objectAtIndex:i];
                    if([row objectForKey:@"Allocated"])
                    {
                        NSString *temp = kSQLSavePerfomrmaLotItem;
                        
                        temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:orderRef];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_No"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Allocated"]];
                        temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:@"N"];
                        
                        temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
                    
                        
                        [self executeNonQuery:temp];
                    }
                }
            }
        }
    }
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocale1=nil;
    orderArray=nil;
    infoOrder=nil;
   // lotArray=nil;
    dict=nil;
    return orderRef;
}
- (NSString *)saveReturnOrderWithCustomerInfo:(NSDictionary *)customerInfo andOrderInfo:(NSMutableDictionary *)orderInfo andFSRInfo:(NSDictionary *)userInfo andVisitID:(NSString *)visitID{
    
    
    NSLog(@"check order info in tbl method %@", [orderInfo description]);
    
    
    NSString* inventoryItemID=[[[orderInfo valueForKey:@"OrderItems"] objectAtIndex:0] valueForKey:@"Inventory_Item_ID"];
    
    NSLog(@"check inventory item id in begin %@", inventoryItemID);
    
    NSString *orderID = [NSString createGuid];
    NSString *lastPerformaOrderReference = [SWDefaults lastReturnOrderReference];
    NSString *nUserId = [userInfo stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastPerformaOrderReference substringFromIndex: [lastPerformaOrderReference length] - 6] longLongValue];
    lastRefId = lastRefId + 1;
    
    
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
   Singleton *single = [Singleton retrieveSingleton];
    
    
    
    
    lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%006lld", nUserId, lastRefId];
    //[SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
    
    if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastReturnOrderReference]])
    {
        [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%006lld", nUserId, lastRefId];
        if (![lastPerformaOrderReference isEqualToString:[SWDefaults lastReturnOrderReference]])
        {
            [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastPerformaOrderReference = [NSString stringWithFormat:@"M%@R%006lld", nUserId, lastRefId];
            [SWDefaults setLastReturnOrderReference:lastPerformaOrderReference];
        }
    }
    
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale1 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setLocale:usLocale1];
    NSString *dateTimeString =  [formatterTime stringFromDate:[NSDate date]];
    
    NSString *startTime = [formatterTime stringFromDate:single.orderStartDate];
    
    NSArray *orderArray = [NSArray arrayWithArray:[orderInfo objectForKey:@"OrderItems"]] ;
    NSDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[orderInfo objectForKey:@"info"]] ;
    NSDictionary *extraInfo = [NSMutableDictionary dictionaryWithDictionary:[infoOrder objectForKey:@"extraInfo"]];
    
    
    NSString *temp = kSQLSaveReturnOrder;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[userInfo stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[customerInfo stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    NSString *stringComments = [extraInfo stringForKey:@"comments"];
    stringComments = [stringComments stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:stringComments];
    NSString *stringReference = [extraInfo stringForKey:@"reference"];
    stringReference = [stringReference stringByReplacingOccurrencesOfString:@"'" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:stringReference];
    //  temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"Y"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:dateTimeString];
    //  temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:[extraInfo stringForKey:@"signature"]];
    
    
    
    id signatureInfo=[SWDefaults fetchSignature];

    if(signatureInfo)
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:@"[SIGN:Y]"];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:@"[SIGN:N]"];
    }
    if([extraInfo objectForKey:@"name"])
    {
        NSString *stringName = [extraInfo stringForKey:@"name"];
        stringName = [stringName stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:stringName];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[infoOrder stringForKey:@"orderAmt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:[userInfo stringForKey:@"Emp_Code"]];
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary] ;
    
    if([single.isCashCustomer isEqualToString:@"N"] || [dict objectForKey:@"cash_Name"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:@""];
    }
    else{
        temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:[dict stringForKey:@"Customer_ID"]];
        temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[dict stringForKey:@"Site_Use_ID"]];
    }
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[extraInfo stringForKey:@"invoice_no"]];
    NSString *stringReturns =[extraInfo stringForKey:@"returnType"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:stringReturns];
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[infoOrder stringForKey:@"productCategory"]];
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[extraInfo stringForKey:@"goodCollected"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:[[SWDefaults productCategory] stringForKey:@"Org_ID"]];
    

    
    NSLog(@"check rma query %@", temp);
    
    
    [self executeNonQuery:temp];
    
    int line;
    for (int i = 0; i < orderArray.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [orderArray objectAtIndex:i];
            
            line=i+1;
            
            NSString *sql = kSQLSaveReturnLineItem;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Qty"]];
            
            NSString* inventoryItemID=[row stringForKey:@"Inventory_Item_ID"];
            NSLog(@"check inventory item id in end %@", inventoryItemID);
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Inventory_Item_ID"]];
           
            
            /*pavan 26-02-2015:multiple UOM implementation */

            if([row objectForKey:@"SelectedUOM"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"SelectedUOM"]];

            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@""]];

            }
            
            if ([row objectForKey:@"Primary_UOM_Code"]) {
                sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Primary_UOM_Code"]];
                
            }
            else
            {
                  sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"UOM"]];
            }
            
            if([[row stringForKey:@"Price"] isEqualToString:@"0"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:@"F"];
                 sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:@"0"];
                
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:@"N"];
                 sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Net_Price"]];
            
            }
            
            if([row objectForKey:@"ChildGuid"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:BonusCA1GUID];
            }
            else if([row objectForKey:@"ParentGuid"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:BonusCA1GUID];
            }
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[NSString createGuid]];
            }
        
            NSLog(@"query for saving rma line items %@", [temp description]);

            
            
            [self executeNonQuery:sql];
            //for (int i = 0; i < orderArray.count; i++)
            //{
            //  @autoreleasepool{
            NSDictionary *row1 = [orderArray objectAtIndex:i];
            NSString *temp = kSQLSaveReturnLotItem;
            
            NSDateFormatter *expformatter = [NSDateFormatter new] ;
            [expformatter setDateFormat:@"MMM dd, yyyy"];
            NSLocale *expLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [expformatter setLocale:expLocale];

            
            //echeck exp date here
            NSDate *expDate = [expformatter dateFromString:[row1 stringForKey:@"EXPDate"]];
            NSString *expStr = [formatter stringFromDate:expDate];
            if (expStr==nil) {
                //check what if date is nill
                
                expStr=[formatter stringFromDate:[NSDate date]];
            }
            
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastPerformaOrderReference];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row1 stringForKey:@"lot"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:inventoryItemID];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row1 stringForKey:@"Qty"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:expStr];
            NSString *stringReturns =[row1 stringForKey:@"RMA_Lot_Type"];
            temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:stringReturns];
        
            
            NSLog(@"check query for lots %@", temp);
            
            
            [self executeNonQuery:temp];
            
            expformatter=nil;
            expLocale=nil;
            //   }
            //}
        }
    }
    
    [self saveOrderHistoryWithCustomerInfo:customerInfo andOrderInfo:orderInfo andFSRInfo:userInfo andVisitID:visitID andOrderID:orderID andOrderRef:lastPerformaOrderReference];
    formatter=nil;
    usLocale=nil;
    formatterTime=nil;
    usLocale1=nil;
    orderArray=nil;
    infoOrder=nil;
    extraInfo=nil;
    dict=nil;
    
    return lastPerformaOrderReference;
}
- (void)saveDocAdditionalInfoWithDoc:(NSString *)docNumber andDocType:(NSString *)docType andAttName:(NSString *)attName andAttValue:(NSString *)attVale andCust_Att_5:(NSString *)cust_Att_5{
    NSString *temp = @"INSERT INTO TBL_Doc_Addl_Info (Row_ID, Doc_No, Doc_Type, Attrib_Name, Attrib_Value,Custom_Attribute_5) VALUES ('{0}', '{1}', '{2}', '{3}','{4}','{5}')";
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:docNumber];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:docType];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:attName];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:attVale];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:cust_Att_5];

    
    [self executeNonQuery:temp];
}
///////////////Send Orders
#define kSQLdbGetConfirmOrderS @"Select * from TBL_Order"

#define kSQLdbGetConfirmOrderItems @"SELECT * FROM TBL_Order_Line_Items  ORDER BY Orig_Sys_Document_Ref ASC"

#define kSQLdbGetConfirmOrderLots @"SELECT * FROM TBL_Order_Lots  ORDER BY Orig_Sys_Document_Ref ASC"

- (void)dbGetConfirmOrder
{
    
//    mainDictionary = [NSMutableDictionary dictionary];
//    mainArray=[NSMutableArray array];
//   // orderArray1 = [NSMutableArray array];
//    lotArray1 = [NSMutableArray array];
//    itemArray = [NSMutableArray array];
    
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderS];
    
    if (orderArray1)
    {
        orderArray1=nil;
    }
    orderArray1 = [NSMutableArray arrayWithArray:temp];
    if([orderArray1 count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:@"Please take order first." delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
        
    }
    else
    {
        [self dbGetConfirmOrderItems];
    }
}
- (void)dbGetConfirmOrderItems
{
    //2
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderItems];
    if (itemArray)
    {
        itemArray=nil;
    }
    itemArray = [NSMutableArray arrayWithArray:temp];
    
    [self dbGetConfirmOrderLots];
}
- (void)dbGetConfirmOrderLots
{
    //3
    
    NSArray *temp = [self fetchDataForQuery:kSQLdbGetConfirmOrderLots];
    if (lotArray1) {
        lotArray1=nil;
    }
    lotArray1 = [NSMutableArray arrayWithArray:temp];
    [self writeXMLString];
}

#define kSQLDeletePerformaOrder1 @"DELETE FROM TBL_Order "
#define kSQLDeletePerformaOrderItems1 @"DELETE FROM TBL_Order_Line_Items"
#define kSQLDeletePerformaOrderLots1 @"DELETE FROM TBL_Order_Lots"

- (void)deleteSalesOrder
{
    NSString *temp = kSQLDeletePerformaOrder1;
    
    [self executeNonQuery:temp];
    
    NSString *temp1 = kSQLDeletePerformaOrderItems1;
    
    [self executeNonQuery:temp1];
    
    NSString *temp2 = kSQLDeletePerformaOrderLots1;
    
    [self executeNonQuery:temp2];
}


-(void)writeXMLString{
    //    if (xmlWriter) {
    //        xmlWriter=nil;
    //    }
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    
    if(![orderArray1 count]==0)
    {
        [xmlWriter writeStartElement:@"Orders"];
        
        for (int o=0; o<[orderArray1 count]; o++)
        {
            if (orderDictionary) {
                orderDictionary=nil;
            }
            orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderArray1 objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            
            
            NSLog(@"check dict in full sync %@", [orderDictionary description]);
            
            
            [xmlWriter writeStartElement:@"Order"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_PO_Number"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_PO_Number"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_PO_Number"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Status"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Update_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Update_Date"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Update_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Discount_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Discount_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Discount_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    if (itemDictionary) {
                        itemDictionary=nil;
                    }
                    itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Def_Bonus"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Def_Bonus"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Def_Bonus"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if([lotArray1 count]!=0)
                        {
                            [xmlWriter writeStartElement:@"Lots"];
                            for (int l=0 ; l<[lotArray1 count] ; l++)
                            {
                                if (lotDictionary) {
                                    lotDictionary=nil;
                                }
                                lotDictionary = [NSMutableDictionary dictionaryWithDictionary:[lotArray1 objectAtIndex:l]];
                                if([orderRef isEqualToString:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]] && [itemLine isEqualToString:[lotDictionary stringForKey:@"Line_Number"]]){
                                    [xmlWriter writeStartElement:@"Lot"];
                                    
                                    [xmlWriter writeStartElement:@"C1"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Row_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C2"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C3"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Line_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C4"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C5"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Ordered_Quantity"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C6"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Is_FIFO"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C7"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Org_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeEndElement];//lot
                                }
                            }
                            [xmlWriter writeEndElement];//lots
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            
            [xmlWriter writeEndElement];//Orders
            
        }
        [xmlWriter writeEndElement];//Order
    }
   Singleton *single = [Singleton retrieveSingleton];
    single.xmlParsedString = [xmlWriter toString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"done-01"];
#pragma clang diagnostic pop
    });
    
}


-(NSString*)getcodeforkeys:(NSString*)str_code
{
    str_code = [str_code stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
    str_code = [str_code stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    return str_code;
}

///////////Collection///////////////
- (NSString *)saveCollection:(NSDictionary *)collectionInfo withCustomerInfo:(NSDictionary *)customerInfo andInvoices:(NSArray *)invoices {
    // Create CollectionId
    NSString *collectionId = [NSString createGuid];
    
    // setup date formatter
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    // Collection date and time
    NSDate *dateOfCollection = [NSDate date];
    
    // Make new Last Collection Reference and save it to NSDefaults
    NSString *lastCollectionReference = [SWDefaults lastCollectionReference];
    NSString *nUserId = [[SWDefaults userProfile] stringForKey:@"Emp_Code"];
    
    unsigned long long lastRefId = [[lastCollectionReference substringFromIndex: [lastCollectionReference length] - 6] longLongValue];
    lastRefId = lastRefId + 1;
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    
    lastCollectionReference = [NSString stringWithFormat:@"M%@C%006lld", nUserId, lastRefId];
    if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
    {
        [SWDefaults setLastCollectionReference:lastCollectionReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastCollectionReference = [NSString stringWithFormat:@"M%@C%006lld", nUserId, lastRefId];
        if (![lastCollectionReference isEqualToString:[SWDefaults lastCollectionReference]])
        {
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastCollectionReference = [NSString stringWithFormat:@"M%@C%006lld", nUserId, lastRefId];
            [SWDefaults setLastCollectionReference:lastCollectionReference];
        }
    }
    //[SWDefaults setLastCollectionReference:lastCollectionReference];
    
    // setup collection type
    NSString *collectionType = @"CASH";
    
    // check date
    NSString *collectionChqDate = @"";
    
    if ([[collectionInfo objectForKey:@"CollectionType"] isEqual:@"PDC"]) {
        collectionType = @"POST-CHQ";
        NSDate *tempDate = [collectionInfo objectForKey:@"ChqDate"];
        collectionChqDate = [dateFormatter stringFromDate:tempDate];
    } else if ([[collectionInfo objectForKey:@"CollectionType"] isEqual:@"CURRENT CHEQUE"]) {
        collectionType = @"CURR-CHQ";
        NSDate *tempDate = [collectionInfo objectForKey:@"ChqDate"];
        collectionChqDate = [dateFormatter stringFromDate:tempDate];
    }
    
    // Construct SQL Statement
    NSString *temp = kSQLCollectionInsertInfo;
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:lastCollectionReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[dateFormatter stringFromDate:dateOfCollection]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:collectionType];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[customerInfo stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[customerInfo stringForKey:@"Site_Use_ID"]];
    if ([[AppControl retrieveSingleton].ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[collectionInfo stringForKey:@"selectedAmount"]];
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[collectionInfo stringForKey:@"Amount"]];
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[collectionInfo stringForKey:@"ChqNumber"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:collectionChqDate];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[collectionInfo stringForKey:@"Bank"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[collectionInfo stringForKey:@"Branch"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[[SWDefaults userProfile] stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[dateFormatter stringFromDate:dateOfCollection]];
    
    
    [self executeNonQuery:temp];
    
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool {
            NSDictionary *row = [invoices objectAtIndex:i];
            
            NSString *sql = kSQLCollectionInsertInvoice;
            
            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:collectionId];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:[row stringForKey:@"C.Invoice_Ref_No"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"InvDate"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Paid"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:@"N"];
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[dateFormatter stringFromDate:dateOfCollection]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[NSString createGuid]];
            
            [self executeNonQuery:sql];
        }
    }
    
    dateFormatter=nil;
    usLocale=nil;
    return lastCollectionReference;
}

//////////////////////////Route//////////////
#pragma  Route Service
- (NSArray *)dbGetCollectionFromDate:(NSDate *)date {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    

    
NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLRoute, [formatter stringFromDate:date]]];
    
    

    
    NSString* strDate=[formatter stringFromDate:date];
    
    NSLog(@"test string %@", strDate);
    formatter=nil;
    usLocale=nil;
    return temp;
}

#pragma  Visit Service

#define kSQLOpenVisit @"SELECT A.Actual_Visit_ID,A.Customer_ID,A.Visit_Start_Date,A.Visit_End_Date FROM  TBL_FSR_Actual_Visits AS A  WHERE  A.Customer_ID = '%@' AND A.Site_Use_ID = '%@' AND A.Visit_End_Date =''"

- (NSArray *)dbGetOpenVisiteFromCustomerID:(NSString *)customer_ID andSiteID:(NSString *)siteID
{
    
    NSArray *temp=[self fetchDataForQuery:[NSString stringWithFormat:kSQLOpenVisit,customer_ID,siteID]];

    return temp;
}

#define kSQLSaveNewVisit @"INSERT INTO TBL_FSR_Actual_Visits (Actual_Visit_ID,Customer_ID,Site_Use_ID,SalesRep_ID,Emp_Code,Latitude,Longitude,Visit_End_Date ,Cash_Customer_ID , Cash_Site_ID ,CC_Name ,CC_TelNo, Visit_Start_Date, Odo_Reading, FSR_Plan_Detail_ID, Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Custom_Attribute_4, Custom_Attribute_5, Custom_Attribute_6, Sync_Timestamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}' ,'{8}', '{9}', '{10}','{11}','{12}','{13}','{14}', '{15}','{16}','{17}','{18}','{19}','{20}', '{21}')"

- (void)saveVistWithCustomerInfo:(NSMutableDictionary *)customerInfo andVisitID:(NSString *)visitID
{
    
    
    SWAppDelegate* appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    
    latitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.latitude];
    longitude=[NSString stringWithFormat:@"%f",appDel.currentLocation.coordinate.longitude];

    if (latitude==nil) {
        latitude=@"0.0";
        longitude=@"0.0";
    }
    
    
    
    NSLog(@"lat while saving visit info is %@", latitude);
    NSLog(@"long while saving visit info is %@", longitude);

    
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
  Singleton *single = [Singleton retrieveSingleton];
    
    NSDictionary *tempCust = [NSMutableDictionary dictionaryWithDictionary:customerInfo];
    NSDictionary *FSR = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults userProfile]  ];
    NSString *temp = kSQLSaveNewVisit;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:visitID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[tempCust stringForKey:@"Ship_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[tempCust stringForKey:@"Ship_Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[FSR stringForKey:@"SalesRep_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[FSR stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:latitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:longitude];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:@"0"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:dateString];
    
    
    //add visit option (ON site on telephonic);
    
    
    BOOL isOnsite=[SWDefaults fetchOnsiteVisitStatus];
    
    if (isOnsite==YES) {
        temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:@"ONSITE"];
        

        
    }
    
    else
    {        temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:@"TELEPHONIC"];

        
    }
    
    
    
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:@""];
    
    if (single.planDetailId==nil) {
        temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:@"0"];

    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{14}" withString:single.planDetailId];

    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:single.cashCustomerDictionary];
    
    formatter=nil;
    usLocale=nil;
    
    if([single.isCashCustomer isEqualToString:@"Y"] )
    {
        if([dict objectForKey:@"cash_Name"])
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@"0"];
            temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"0"];
            temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:[dict stringForKey:@"cash_Name"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:[dict stringForKey:@"cash_Phone"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:[dict stringForKey:@"cash_Contact"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:[dict stringForKey:@"cash_Address"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[dict stringForKey:@"cash_Fax"]];
        }
        else
        {
            temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[dict stringForKey:@"Customer_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[dict stringForKey:@"Site_Use_ID"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
            temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
        }
    }
    else
    {
        temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:@"0"];
        temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@""];
        temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:@""];
    }
    
    
    NSLog(@"query in visit fsr actual visits %@", temp);
    
    
    [self executeNonQuery:temp];
    tempCust=nil;
    FSR=nil;
    dict=nil;
}
#define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "

- (void)saveVisitEndDateWithVisiteID:(NSString *)visitID
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSString *temp = kSQLUpdateEndDate;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:visitID];
    
    [self executeNonQuery:temp];
    formatter=nil;
    usLocale=nil;
}

#define kSQLUpdatePlannedVisitStatus @"UPDATE TBL_FSR_Planned_Visits SET Visit_Status='{0}' WHERE Planned_Visit_ID='{1}'"

- (void)saveVisitStatusWithVisiteID:(NSString *)visitID
{
    NSString *temp = kSQLUpdatePlannedVisitStatus;
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:@"Y"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:visitID];
    
    [self executeNonQuery:temp];
}

#define kSQLDuesStatics @"SELECT Customer_ID, M0_Due ,M1_Due ,M2_Due ,M3_Due ,M4_Due ,Prior_Due ,Total_Due ,PDC_Due FROM TBL_Customer_Dues WHERE Customer_ID = '%@'"

-(NSArray *)dbGetDuesStaticWithCustomerID:(NSString *)customerID
{
    
    NSArray *temp =[self fetchDataForQuery:[NSString stringWithFormat:kSQLDuesStatics,customerID]];
    return  temp;
}

- (void)currentLocation
{
    for (int i= 0; i <=3; i++) {
        //NSLog(@"Location captureing ...ellloooo!!!!");
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        //locationManager.delegate=self;
        
        [locationManager startUpdatingLocation];
        CLLocation *location = [locationManager location];
        // Configure the new event with information from the location
        CLLocationCoordinate2D coordinate = [location coordinate];
        latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
        longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
        //locationManager=nil;
        [locationManager stopUpdatingLocation];
        if ([latitude isEqualToString:@"0"] || [longitude isEqualToString:@"0"]) {
        }
        else
        {
            break;
        }
    }
    //  Latitude = "25.304524";
//    Longitude = "55.373513";
//    NSLog(@"latitude %@",latitude);
//    NSLog(@"longitude %@",longitude);
}


//-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//
//{
//    [manager stopUpdatingLocation];
//    NSString *lati = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.latitude];
//    NSLog(@"address:%@",lati);
//    
//    NSString *longi = [[NSString alloc] initWithFormat:@"%+.6f", newLocation.coordinate.longitude];
//    NSLog(@"address:%@",longi);
//    
//    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
//
//    [geoCoder reverseGeocodeLocation: newLocation completionHandler: ^(NSArray *placemarks, NSError *error)
//     {
//         //Get nearby address
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//         
//         //String to hold address
//         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//         
//         //Print the location to console
//         NSLog(@"I am currently at %@",locatedAt);
//         
//         NSString *address = [[NSString alloc]initWithString:locatedAt];
//         NSLog(@"address:%@",address);
//     }];
//    
//}
-(void)saveConfirmedOrder : (NSMutableDictionary *)order
{
    //kSQLSaveOrder
    //NSLog(@"Orer %@",order);x
    
  
    NSString *orderID = [NSString createGuid];
    NSString *lastOrderReference = [SWDefaults lastOrderReference];
    NSString *nUserId = [order stringForKey:@"Emp_Code"];
    unsigned long long lastRefId = [[lastOrderReference substringFromIndex: [lastOrderReference length] - 6] longLongValue];
    lastRefId = lastRefId + 1;
    if(nUserId.length == 1)
    {
        nUserId = [NSString stringWithFormat:@"00%@",nUserId];
    }
    else if(nUserId.length == 2)
    {
        nUserId = [NSString stringWithFormat:@"0%@",nUserId];
    }
    lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
    
    if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
    {
        [SWDefaults setLastOrderReference:lastOrderReference];
    }
    else
    {
        lastRefId = lastRefId + 1;
        lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
        if (![lastOrderReference isEqualToString:[SWDefaults lastOrderReference]])
        {
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
        else
        {
            lastRefId = lastRefId + 1;
            lastOrderReference = [NSString stringWithFormat:@"M%@S%006lld", nUserId, lastRefId];
            [SWDefaults setLastOrderReference:lastOrderReference];
        }
    }
    NSDateFormatter *formatter = [NSDateFormatter new] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *formatterTime = [NSDateFormatter new] ;
    NSLocale *usLocaleq = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatterTime setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [formatterTime setLocale:usLocaleq];
    NSString *startTime =  [formatterTime stringFromDate:[NSDate date]];
    
    
    NSString *temp =@"INSERT INTO TBL_Order ( Row_ID, Orig_Sys_Document_Ref, Creation_Date, Created_By, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, Packing_Instructions, Request_Date, Schedule_Ship_Date, Order_Status, Start_Time, End_Time, Signee_Name, Custom_Attribute_1, Emp_Code, Order_Amt, Visit_ID , System_Order_Date,Credit_Customer_ID,Credit_Site_ID,Last_Update_Date ,Last_Updated_By,Custom_Attribute_2,Custom_Attribute_3,Discount_Amt,Customer_PO_Number, ,Shipping_Instructions,Custom_Attribute_4) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' , '{7}', '{8}', '{9}', '{10}', '{11}' , '{12}', '{13}', '{15}', '{16}' , '{17}', '{18}' , '{19}','{20}','{21}','{22}' ,'{23}','{24}','{25}' ,'{26}','{27}','{28}','{29}','{30}')";

    temp = [temp stringByReplacingOccurrencesOfString:@"{0}"  withString:orderID];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}"  withString:lastOrderReference];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}"  withString:[order stringForKey:@"Creation_Date"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}"  withString:[order stringForKey:@"Created_By"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}"  withString:[order stringForKey:@"Ship_To_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}"  withString:[order stringForKey:@"Ship_To_Site_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}"  withString:[order stringForKey:@"Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}"  withString:[order stringForKey:@"Site_Use_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}"  withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}"  withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{10}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{11}" withString:@"N"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{12}" withString:[order stringForKey:@"Start_Time"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{13}" withString:startTime];
    temp = [temp stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{16}" withString:@"[SIGN:N]"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{17}" withString:[order stringForKey:@"Emp_Code"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{18}" withString:[order stringForKey:@"Order_Amt"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{19}" withString:[order stringForKey:@"Visit_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{20}" withString:dateString];
    temp = [temp stringByReplacingOccurrencesOfString:@"{21}" withString:[order stringForKey:@"Credit_Customer_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{22}" withString:[order stringForKey:@"Credit_Site_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{23}" withString:[order stringForKey:@"Last_Update_Date"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{24}" withString:[order stringForKey:@"Last_Updated_By"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{25}" withString:[order stringForKey:@"Custom_Attribute_2"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{26}" withString:[order stringForKey:@"Custom_Attribute_3"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{27}" withString:@"0.0"]; // discount amt
    
    
    //insert customer doc reference here
    temp = [temp stringByReplacingOccurrencesOfString:@"{28}" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"{29}" withString:@"0"];
    temp = [temp stringByReplacingOccurrencesOfString:@"{30}" withString:@""];
    
    NSMutableArray *arrVisitStatus = [self fetchDataForQuery:[NSString stringWithFormat:@"select Custom_Attribute_4 from TBL_FSR_Actual_Visits where Actual_Visit_ID = '%@'", [order stringForKey:@"Visit_ID"]]];
    if (arrVisitStatus.count > 0) {
        NSString *visitStatus = [arrVisitStatus[0]valueForKey:@"Custom_Attribute_4"];
        if (visitStatus.length > 0) {
            temp = [temp stringByReplacingOccurrencesOfString:@"{30}" withString:visitStatus];
        }
    }

    [self executeNonQuery:temp];
    
//#define kSQLSaveOrderHistory Row_ID, Orig_Sys_Document_Ref, Doc_Type, Creation_Date  , Created_By, System_Order_Date, Request_Date, Schedule_Ship_Date, Ship_To_Customer_ID, Ship_To_Site_ID, Inv_To_Customer_ID, Inv_To_Site_ID, BO_Status, Start_Time, End_Time, Packing_Instructions, Emp_Code,ERP_Status, Transaction_Amt,Due_Date,Credit_Customer_ID,Credit_Site_ID, Visit_ID,Sync_Timestamp
    
    NSString *tempHistory = kSQLSaveOrderHistory;
    NSCalendar *gregCalendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
    
    NSDateComponents *components=[gregCalendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger month=[components month];
    NSInteger year=[components year];
    
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    

    NSDate *lastDateofMonth= [[gregCalendar dateFromComponents:components] dateByAddingTimeInterval:0];
    NSString *lastDateMonthString =  [formatter stringFromDate:lastDateofMonth];

    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{1}" withString:lastOrderReference];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{2}" withString:@"I"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{3}" withString:dateString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{4}" withString:[order stringForKey:@"Created_By"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{5}" withString:dateString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{6}" withString:dateString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{7}" withString:dateString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{8}" withString:[order stringForKey:@"Ship_To_Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{9}" withString:[order stringForKey:@"Ship_To_Site_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{10}" withString:[order stringForKey:@"Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{11}" withString:[order stringForKey:@"Site_Use_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{12}" withString:@"N"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{13}" withString:[order stringForKey:@"Start_Time"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{14}" withString:startTime];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{15}" withString:@""];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{16}" withString:[order stringForKey:@"Emp_Code"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{17}" withString:@"N"];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{18}" withString:[order stringForKey:@"Order_Amt"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{19}" withString:lastDateMonthString];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{20}" withString:[order stringForKey:@"Credit_Customer_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{21}" withString:[order stringForKey:@"Credit_Site_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{22}" withString:[order stringForKey:@"Visit_ID"]];
    tempHistory = [tempHistory stringByReplacingOccurrencesOfString:@"{23}" withString:dateString];

    [self executeNonQuery:tempHistory];
    
    
    
    
    [self saveConfirmedOrderLineItems:[order stringForKey:@"Orig_Sys_Document_Ref"] andConfirmedOrderRef:lastOrderReference];

}
-(void)saveConfirmedOrderLineItems : (NSString *)orderRef andConfirmedOrderRef:(NSString *)cOrderRef
{
    NSMutableArray *tempArray =   [[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",orderRef]] mutableCopy];
    
    int line;

    for (int i = 0; i < tempArray.count; i++)
    {
        @autoreleasepool{
            NSDictionary *row = [tempArray objectAtIndex:i];
            NSString *sql = kSQLSaveOrderLineItem;
            line=i+1;

            sql = [sql stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql = [sql stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Display_UOM"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Def_Bonus"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql = [sql stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"Custom_Attribute_3"]];
            if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
            {
                BonusCA1GUID=[NSString createGuid];
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            else if([[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"F"] || [[row objectForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{11}" withString:BonusCA1GUID];
            }
            
            if ([row valueForKey:@"lineItemNotes"]==nil ||[row stringForKey:@"lineItemNotes"].length==0){
                
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:@""];
                
                
            }
            
            else
            {
                sql = [sql stringByReplacingOccurrencesOfString:@"{12}" withString:[row valueForKey:@"lineItemNotes"]];
            }


            [self executeNonQuery:sql];
            
            
            //Order HISTORY
            NSString *sql1 = kSQLSaveOrderHistoryLineItem;
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Inventory_Item_ID"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Order_Quantity_UOM"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Ordered_Quantity"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Calc_Price_Flag"]];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Unit_Selling_Price"]];
            double iTemValue = [[row stringForKey:@"Ordered_Quantity"] doubleValue] * [[row stringForKey:@"Unit_Selling_Price"] doubleValue];
            sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{8}" withString:[NSString stringWithFormat:@"%.1f",iTemValue]];
            
            if([row stringForKey:@"lot"]==nil||[row stringForKey:@"lot"].length==0) /**return items*/
            {
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{9}" withString:@""];
            }
            else {
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"lot"]];
            }
            
            if([row stringForKey:@"EXPDate"]==nil||[row stringForKey:@"EXPDate"].length==0) /**return items*/
            {
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{10}" withString:@""];
            }
            else {
                sql1 = [sql1 stringByReplacingOccurrencesOfString:@"{10}" withString:[row stringForKey:@"EXPDate"]];
                
            }
            
            //NSLog(@"Query %@",sql1);
            [self executeNonQuery:sql1];

        }
    }

    [self saveConfirmedOrderLots:orderRef andConfirmedOrderRef:cOrderRef];
    
}
-(void)saveConfirmedOrderLots : (NSString *)orderRef andConfirmedOrderRef:(NSString *)cOrderRef
{
    NSMutableArray *tempArray =   [[self fetchDataForQuery:[NSString stringWithFormat:@"Select * from TBL_Proforma_Order_Lots where Orig_Sys_Document_Ref='%@'",orderRef]] mutableCopy];
    //NSLog(@"Temsp %@",tempArray);
    
    int line;

    for (int i = 0; i < tempArray.count; i++)
    {
        @autoreleasepool
        {
            NSDictionary *row = [tempArray objectAtIndex:i];
            line=i+1;
            //#define kSQLSaveOrderLotItem @"INSERT   INTO TBL_Order_Lots (Row_ID, Orig_Sys_Document_Ref, Line_Number, Lot_Number, Ordered_Quantity, Is_FIFO, Org_ID) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')"

            NSString *temp = kSQLSaveOrderLotItem;
            temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString createGuid]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:cOrderRef];
            temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[NSString stringWithFormat:@"%d",line]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Lot_Number"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Ordered_Quantity"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Is_FIFO"]];
            temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Org_ID"]];
            
            [self executeNonQuery:temp];
        }
    }
}

-(void)insertProductAddInfo:(NSDictionary *)row
{
    NSString *temp= @"INSERT INTO TBL_Product_Addl_Info (Attrib_Name, Attrib_Value, Custom_Attribute_1, Custom_Attribute_2, Custom_Attribute_3, Custom_Attribute_4, Custom_Attribute_5,Inventory_Item_ID,Organization_ID,Sync_Timestamp) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')";
    
    temp = [temp stringByReplacingOccurrencesOfString:@"{0}" withString:[row stringForKey:@"Attrib_Name"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{1}" withString:[row stringForKey:@"Attrib_Value"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{2}" withString:[row stringForKey:@"Custom_Attribute_1"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{3}" withString:[row stringForKey:@"Custom_Attribute_2"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{4}" withString:[row stringForKey:@"Custom_Attribute_3"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{5}" withString:[row stringForKey:@"Custom_Attribute_4"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{6}" withString:[row stringForKey:@"Custom_Attribute_5"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{7}" withString:[row stringForKey:@"Inventory_Item_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{8}" withString:[row stringForKey:@"Organization_ID"]];
    temp = [temp stringByReplacingOccurrencesOfString:@"{9}" withString:[row stringForKey:@"Sync_Timestamp"]];
    NSLog(@"Hello %@",temp);
    [self executeNonQuery:temp];

}

-(NSMutableArray*)fetchAgencies
{
    
//    NSString* agencyQuery=@"SELECT Classification_1 FROM tbl_sales_Target_Items";
    
    NSString* agencyQuery=@"SELECT IFNULL(Classification_1,0) As [Agency],SUM(IFNULL(Target_Value,0)) As [Target_Value],SUM(IFNULL(Sales_Value,0)) As [Sales_Value] FROM TBL_Sales_Target_Items GROUP BY Classification_1 ORDER BY Classification_1";

    
    NSMutableArray* agencies=[self fetchDataForQuery:agencyQuery];
    
    return agencies;
    
    
    
    
}

-(NSMutableArray*)fetchUOMforItem:(NSString*)itemCode

{
    NSString* UOMQuery=[NSString stringWithFormat:@"SELECT Item_UOM FROM TBL_Item_UOM where Item_Code ='%@'",itemCode];
    
    
    
    NSMutableArray* UOM=[self fetchDataForQuery:UOMQuery];
    
    return UOM;
    

}



-(NSMutableArray*)fetchLYTMLabel
{
    NSString* lytmQry=[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_4,0) AS Custom_Attribute_4,IFNULL(Custom_Attribute_5,0) AS Custom_Attribute_5 from TBL_Sales_Target_Meta "];
    NSArray* lytmArray=[self fetchDataForQuery:lytmQry];
    if (lytmArray.count>0) {
        return [lytmArray mutableCopy];
        
        
        
        
    }
    
    else
    {
        return nil;
        
    }
}

+(NSMutableArray*)fetchConversionFactorforUOMitemCode:(NSString*)itemCode ItemUOM:(NSString*)itemUom
{
    NSString* conversionFactorQry=[NSString stringWithFormat:@"select IFNULL(Conversion,0) AS Conversion from TBL_ITEM_UOM where Item_Code='%@' and Item_UOM ='%@' ",itemCode,itemUom];
    
    NSLog(@"query for fetching conversion %@", conversionFactorQry);
    
    
    NSMutableArray* conversionArray=[FMDBHelper executeQuery:conversionFactorQry ];
    if (conversionArray.count>0) {
        
        NSLog(@"conversion factor is %@", [conversionArray description]);
        
        return conversionArray;
        
    }
    else
    {
        return 0;
    }
    
}


+(NSMutableArray*)fetchOrderHistoryPopupData :(NSString*)docRefNumber
{
    NSString* orderHistoryPopUpDataQry=[NSString stringWithFormat:@"SELECT IFNULL(SWX_Doc_No,0) AS SWX_Doc_Number, IFNULL(Doc_Type,0) AS Doc_Typ, IFNULL(ERP_DOC_NO_1,0) AS PO_Number, IFNULL(ERP_DOC_NO_2,0) AS SO_Number,  IFNULL(ERP_DOC_NO_3,0) AS Delivery_Number,  IFNULL(ERP_DOC_NO_4,0) AS Invoice_Number,   IFNULL(ERP_DOC_NO_5,0) AS Trip_Number, IFNULL(ERP_Date_1,0) AS PO_Date,IFNULL(ERP_Date_2,0) AS SO_Date, IFNULL(ERP_Date_3,0) AS Delivery_Date, IFNULL(ERP_Date_4,0) AS Invoice_Date , IFNULL(ERP_Date_5,0) AS Trip_Date FROM TBL_ERP_DOC_Status where SWX_Doc_Number='%@'", docRefNumber];
    
    
    NSLog(@"order history query is %@", orderHistoryPopUpDataQry);
    
    
    NSMutableArray* orderHistoryArray=[FMDBHelper executeQuery:orderHistoryPopUpDataQry ];

    if (orderHistoryArray.count>0) {
        
        NSLog(@"order history array is %@", [orderHistoryArray description]);
        
        return orderHistoryArray;
        
    }
    
    else
    {
        return 0;
    }
    
    
    
}

+(NSString*)isCustomerBlocked:(NSString*)customerID

{
    
    NSString* status;
    
    NSString* blockedQry=[NSString stringWithFormat:@"select Cust_Status from TBL_Customer where Customer_ID='%@'",customerID];
    
    NSMutableArray* statusArray=[FMDBHelper executeQuery:blockedQry];
    
    if (statusArray.count>0) {
        status=[[statusArray valueForKey:@"Cust_Status"] objectAtIndex:0];
        return status;
        
    }
    
    else
    {
        return @"";
    }
    
}


#pragma mark New Reports methods


-(NSString*)fetchCurrentDateTimeinDatabaseFormat
{
    NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:usLocale];
    NSDate* currentDate=[NSDate date];
    NSString * formattedDate=[dateFormatter stringFromDate:currentDate];
    return formattedDate;
}

- (NSArray *)fetchProductsforAllCategories {
    
    
    NSMutableArray *productsArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *tempProductsArray=[[NSMutableArray alloc]init];
    
    
    NSLog(@"Starting execution %@", [self fetchCurrentDateTimeinDatabaseFormat]);
    
    tempProductsArray= [self fetchDataForQuery:kSQLFetchAllCategoriesWithProducts];
    

   // NSLog(@"products of category is %@", [tempProductsArray description]);
    
    if (tempProductsArray.count>0) {
        
        for (NSMutableDictionary *currentProductDict in tempProductsArray) {
        
//        for (NSInteger i=0; i<tempProductsArray.count;i++) {
//             currentProductDict=[tempProductsArray objectAtIndex:i];
//            
           // NSLog(@"current product dict is %@", currentProductDict);
            
            Products * currentProduct=[[Products alloc]init];
            currentProduct.Brand_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Brand_Code"]];
            currentProduct.selectedUOM=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Primary_UOM_Code"]];
            currentProduct.Discount=[NSString stringWithFormat:@"%0.2f",[[currentProductDict valueForKey:@"Discount"] doubleValue]];
            currentProduct.Agency=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Agency"]];
            currentProduct.Category=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Category"]];
            currentProduct.Description=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Description"]];
            currentProduct.IsMSL=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"IsMSL"]];
            currentProduct.ItemID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.Item_Code=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Item_Code"]];
            currentProduct.OrgID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"OrgID"]];
            currentProduct.Sts=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Sts"]];
            currentProduct.Inventory_Item_ID=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"ItemID"]];
            currentProduct.bonus= [SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Bonus"]]];
            currentProduct.stock=[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Lot_Qty"]];
            
            
            currentProduct.formattedStock=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue: [currentProductDict valueForKey:@"Lot_Qty"]]];
            
            
            currentProduct.nearestExpiryDate=[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Expiry_Date"]];
            currentProduct.isSellingPriceFieldEditable=[[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Control_1"]]isEqualToString:@"Y"]?YES:NO;
            currentProduct.Blocked_Stock=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"Blocked_Stock"]]];
            currentProduct.QC_Stock=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentProductDict valueForKey:@"QC_Stock"]]];
            
            [productsArray addObject:currentProduct];
        }
    }
    
    NSLog(@"finished execution %@", [self fetchCurrentDateTimeinDatabaseFormat]);

    
    
    return productsArray;
}


-(NSMutableArray*)fetchSalesPerAgencyforAllCustomers
{
//    NSString* salesPerAgencyQry=@"select IFNULL(A.Customer_No,'N/A') AS Customer_No,IFNULL(D.Attrib_Value, 'N/A') AS Chain_Customer,C.Customer_Name,C.Chain_Customer_Code,IFNULL(A.Item_Code,'N/A') AS Item_Code,IFNULL(B.Agency,'N/A') AS Agency,IFNULL(SUM(LY_YTD_Qty),'0') AS LY_YTD_Qty, IFNULL(SUM(LY_YTD_Val),'0') AS LY_YTD_Val,IFNULL(SUM(CY_YTD_Qty),'0') AS CY_YTD_Qty,IFNULL(SUM(CY_YTD_Val),'0') AS CY_YTD_Val,IFNULL(SUM(LY_MTD_Qty),'0') AS LY_MTD_Qty,IFNULL(SUM(LY_MTD_Val),'0') AS LY_MTD_Val,IFNULL(SUM(CY_MTD_Val),'0') AS CY_MTD_Val,IFNULL(SUM(CY_MTD_Qty),'0') AS CY_MTD_Qty,IFNULL(SUM(CY_M1_Qty),'0') AS CY_M1_Qty,IFNULL(SUM(CY_M1_Val),'0') AS CY_M1_Val,IFNULL(SUM(CY_M2_Qty),'0') AS CY_M2_Qty,IFNULL(SUM(CY_M2_Val),'0') AS CY_M2_Val,IFNULL(SUM(CY_M3_Qty),'0') AS CY_M3_Qty,IFNULL(SUM(CY_M3_Val),'0') AS CY_M3_Val  from TBL_Customer_Product_Sales AS A inner join TBL_Product AS B on A.Item_Code =B.Item_Code and A.Org_ID=B.Organization_ID inner join TBL_customer AS C on A.Customer_NO=C.Customer_NO  inner join  TBL_Customer_Addl_info as D on C.Customer_ID = D.Customer_ID and C.Site_use_ID=D.Site_Use_ID where D.Attrib_name='EXT'   group by B.Agency";
//    

    
    NSString* salesPerAgencyQry=@"select x.*,D.Attrib_Value AS Chain_Customer,C.Customer_Name,C.Chain_Customer_Code from (select A.Customer_No, IFNULL(B.Agency,'N/A') AS Agency,IFNULL(SUM(LY_YTD_Qty),'0') AS LY_YTD_Qty, IFNULL(SUM(LY_YTD_Val),'0') AS LY_YTD_Val,IFNULL(SUM(CY_YTD_Qty),'0') AS CY_YTD_Qty,IFNULL(SUM(CY_YTD_Val),'0') AS CY_YTD_Val,IFNULL(SUM(LY_MTD_Qty),'0') AS LY_MTD_Qty,IFNULL(SUM(LY_MTD_Val),'0') AS LY_MTD_Val,IFNULL(SUM(CY_MTD_Val),'0') AS CY_MTD_Val,IFNULL(SUM(CY_MTD_Qty),'0') AS CY_MTD_Qty,IFNULL(SUM(CY_M1_Qty),'0') AS CY_M1_Qty,IFNULL(SUM(CY_M1_Val),'0') AS CY_M1_Val,IFNULL(SUM(CY_M2_Qty),'0') AS CY_M2_Qty,IFNULL(SUM(CY_M2_Val),'0') AS CY_M2_Val,IFNULL(SUM(CY_M3_Qty),'0') AS CY_M3_Qty,IFNULL(SUM(CY_M3_Val),'0') AS CY_M3_Val  from TBL_Customer_Product_Sales AS A inner join TBL_Product AS B on A.Item_Code =B.Item_Code and A.Org_ID=B.Organization_ID group by b.agency,A.Customer_No) as x inner join TBL_customer AS C on x.Customer_NO=C.Customer_NO  inner join  TBL_Customer_Addl_info as D on C.Customer_ID = D.Customer_ID and C.Site_use_ID=D.Site_Use_ID and D.Attrib_name='EXT'";
    

    
    
    NSMutableArray * salesPerAgencyArray=[[NSMutableArray alloc]init];
    
    salesPerAgencyArray=[self fetchDataForQuery:salesPerAgencyQry];
    
    NSLog(@"sales per agecy responde %@",[salesPerAgencyArray description]);
    
    NSMutableArray* salesPerAgencyObjectsArray=[[NSMutableArray alloc]init];
    
    
    if (salesPerAgencyArray.count>0) {
        
    for (NSMutableDictionary * currentCustomer in salesPerAgencyArray) {
            
        SalesPerAgency * salesPerAgency=[[SalesPerAgency alloc]init];
        salesPerAgency.Customer_No= [SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_No"]];
        salesPerAgency.Customer_Name=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Customer_Name"]];
        salesPerAgency.Agency=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Agency"]];
        
        salesPerAgency.Item_Code=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Item_Code"]];
        
        salesPerAgency.Chain_Customer=[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Chain_Customer"]];
        
        salesPerAgency.LY_YTD_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_YTD_Qty"]]];
                                   
         salesPerAgency.LY_YTD_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_YTD_Val"]]];
                                    
        salesPerAgency.LY_MTD_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_MTD_Qty"]]];
        
        salesPerAgency.LY_MTD_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_MTD_Val"]]];
        
         salesPerAgency.CY_YTD_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_YTD_Qty"]]];
        
        salesPerAgency.CY_YTD_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_YTD_Val"]]]];
        
        salesPerAgency.CY_MTD_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_MTD_Qty"]]];
        
        salesPerAgency.CY_MTD_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_MTD_Val"]]];
        
         salesPerAgency.CY_M1_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M1_Qty"]]];
        
         salesPerAgency.CY_M1_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M1_Val"]]];
        
        salesPerAgency.CY_M2_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M2_Qty"]]];
        
        salesPerAgency.CY_M2_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M2_Val"]]];
        
        salesPerAgency.CY_M3_Qty=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M3_Qty"]]];
        
         salesPerAgency.CY_M3_Val=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_M3_Val"]]];
        
        salesPerAgency.Chain_Customer_Code=[SWDefaults formatStringWithThousandSeparatorString:[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"Chain_Customer_Code"]]];
        
        double ytdVariance=0.0;
        double lytd=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_YTD_Val"]] doubleValue];
        double cytd=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_YTD_Val"]] doubleValue];
        
        ytdVariance=((cytd-lytd)/lytd)*100;
        if (isinf(ytdVariance)) {
            ytdVariance=0.0;
        }
        
        
        if (isnan(ytdVariance)) {
            ytdVariance=0.0;

        }

        
        salesPerAgency.ytdVariancePercentage=[NSString stringWithFormat:@"%0.2f",ytdVariance];
        NSLog(@"ytd variance is lytd %0.2f cytd %0.2f variance %0.2f",lytd,cytd,ytdVariance);

        
        double mtdVariance=0.0;
        double lymtd=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_MTD_Val"]] doubleValue];
        double cymtd=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_MTD_Val"]] doubleValue];
        
        mtdVariance=((cymtd-lymtd)/lymtd)*100;
        
        if (isinf(mtdVariance)) {
            mtdVariance=0.0;
        }
        
        
        if (isnan(mtdVariance)) {
            mtdVariance=0.0;
            
        }
        
        salesPerAgency.mtdVariancePercentage=[NSString stringWithFormat:@"%0.2f",mtdVariance];
        
        
        //qty variance
        
        double ytdQtyVariance=0.0;
        double lytdQty=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_YTD_Qty"]] doubleValue];
        double cytdQty=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_YTD_Qty"]] doubleValue];
        
        ytdQtyVariance=((cytdQty-lytdQty)/lytdQty)*100
        ;
        if (isinf(ytdQtyVariance)) {
            ytdQtyVariance=0.0;
        }
        
        if (isnan(ytdQtyVariance)) {
            ytdQtyVariance=0.0;
            
        }

        
        
        salesPerAgency.ytdQtyVariancePercentage=[NSString stringWithFormat:@"%0.2f",ytdQtyVariance];
        
        
        double mtdQtyVariance=0.0;
        double lymtdQty=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"LY_MTD_Qty"]] doubleValue];
        double cymtdQty=[[SWDefaults getValidStringValue:[currentCustomer valueForKey:@"CY_MTD_Qty"]] doubleValue];
        
        mtdQtyVariance=((cymtdQty-lymtdQty)/lymtdQty)*100;
        
        if (isinf(mtdQtyVariance)) {
            mtdQtyVariance=0.0;
        }
        
        
        if (isnan(mtdQtyVariance)) {
            mtdQtyVariance=0.0;
            
        }
        
        salesPerAgency.mtdQtyVariancePercentage=[NSString stringWithFormat:@"%0.2f",mtdQtyVariance];
        

        
        
        
        
        [salesPerAgencyObjectsArray addObject:salesPerAgency];
    
    }
    }
    if (salesPerAgencyObjectsArray.count>0) {
        
        return salesPerAgencyObjectsArray;
    }
    else
    {
        return nil;
    }
    
    
}
/*
- (void)testDeleteDataFromTable:(NSString*)tblName{
    
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:[NSString stringWithFormat:@"DELETE FROM '%@'",tblName]];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}
*/


-(NSMutableDictionary*)fetchSalesTargetMonthandYear
{
    NSString* dateMonthQuery=@"select IFNULL(A.Target_Year,'N/A') AS Year, IFNULL(B.Month,'N/A')AS Month   from TBL_Sales_Target AS A inner join TBL_Sales_target_Items AS B on A.Sales_Target_ID=B.Sales_Target_ID";
    
    NSMutableArray* targetDataArray=[FMDBHelper executeQuery:dateMonthQuery];
    if (targetDataArray.count>0) {
        
        NSMutableDictionary* targetDict=[targetDataArray objectAtIndex:0];
        
        return targetDict;
        
    }
    else
    {
        return nil;
    }
}


-(NSMutableDictionary*)fetchSalesTargetMonthandYearforCustomer:(NSString*)customerNumber
{
    NSString* dateMonthQuery=@"select IFNULL(A.Target_Year,'N/A') AS Year, IFNULL(B.Month,'N/A')AS Month   from TBL_Sales_Target AS A inner join TBL_Sales_target_Items AS B on A.Sales_Target_ID=B.Sales_Target_ID";
    
    NSMutableArray* targetDataArray=[FMDBHelper executeQuery:dateMonthQuery];
    if (targetDataArray.count>0) {
        
        NSMutableDictionary* targetDict=[targetDataArray objectAtIndex:0];
        
        return targetDict;
        
    }
    else
    {
        return nil;
    }
}

-(BOOL)deleteCapturedImagesData
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    NSString* attribName=[NSString stringWithFormat:@"VISIT_IMG"];
    
    BOOL success=NO;
    @try {
        success =  [database executeUpdate:@"Delete TBL_Visit_Addl_Info  WHERE Visit_ID = ? and Attrib_Name = ?",  [SWDefaults currentVisitID],attribName, nil];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [database commit];
    [database close];
    return success;
}

-(BOOL)updateCapturedImagesDatatoVisitAdditionalInfo
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    NSString* createdAt=[SWDefaults fetchCurrentDateTimeinDatabaseFormat];
    NSString* createdBy=[[SWDefaults userProfile] valueForKey:@"User_ID"];
    NSString* rowID=[NSString createGuid];
    NSString* attribName=[NSString stringWithFormat:@"VISIT_IMG"];
    
    NSMutableString *documentsDirectory=[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] mutableCopy];
    
    NSError * fileContentsError;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&fileContentsError];
    NSArray *jpgFiles = [files filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        return [evaluatedObject hasSuffix:@".jpg"];
    }]];
    NSString* attributeValue=[jpgFiles componentsJoinedByString:@","];
    BOOL success=NO;
    @try {
        success =  [database executeUpdate:@"UPDATE TBL_Visit_Addl_Info SET  Visit_ID = ?, Attrib_Name = ?, Attrib_Value = ?, Created_At = ?, Created_By = ? WHERE Visit_ID = ?",  [SWDefaults currentVisitID],attribName,attributeValue,createdAt,createdBy,[SWDefaults currentVisitID], nil];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [database commit];
    [database close];
    return success;
}

-(BOOL)saveCapturedImagestoVisitAdditionalInfo
{
    FMDatabase *database = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    NSString* createdAt=[SWDefaults fetchCurrentDateTimeinDatabaseFormat];
    NSString* createdBy=[[SWDefaults userProfile] valueForKey:@"User_ID"];
    NSString* rowID=[NSString createGuid];
    NSString* attribName=[NSString stringWithFormat:@"VISIT_IMG"];
    
    NSMutableString *documentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] mutableCopy];
    
    NSError * fileContentsError;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:&fileContentsError];
    NSArray *jpgFiles = [files filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(NSString *evaluatedObject, NSDictionary *bindings) {
        return [evaluatedObject hasSuffix:@".jpg"];
    }]];
    NSString* attributeValue;
    if (jpgFiles.count>0) {
        if (jpgFiles.count==1) {
            attributeValue =[jpgFiles objectAtIndex:0];
        }
        else{
       attributeValue =[jpgFiles componentsJoinedByString:@","];
        }
    }
    BOOL success=NO;
    @try {
        success=[database executeUpdate:@"INSERT OR Replace INTO TBL_Visit_Addl_Info (Row_ID,Visit_ID, Attrib_Name,Attrib_Value,Created_At,Created_By)  VALUES (?,?,?,?,?,?)",rowID,[SWDefaults currentVisitID],attribName,attributeValue,createdAt,createdBy, nil];
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    
    [database commit];
    [database close];
    return success;
}
-(BOOL)saveCapturedImagesToVisitImages:(NSArray *)imageArray
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    BOOL success=NO;
    for (int i =0; i< [imageArray count]; i++) {
        NSString* createdAt=[SWDefaults fetchCurrentDateTimeinDatabaseFormat];
        NSString* imageID = [[imageArray valueForKey:@"imageID"] objectAtIndex:i];
        NSString* imageName = [[imageArray valueForKey:@"imageName"] objectAtIndex:i];
        NSString* status = [[imageArray valueForKey:@"status"] objectAtIndex:i];
        
        NSString *query = @"INSERT OR Replace INTO TBL_Visit_Images (Image_ID,Visit_ID, File_Type,File_Name,Captured_At,Status)  VALUES (?,?,?,?,?,?)";
        NSMutableArray *insertParameters=[[NSMutableArray alloc]initWithObjects:
                                          imageID,
                                          [SWDefaults currentVisitID],
                                          @".jpg",
                                          imageName,
                                          createdAt ,
                                          status ,
                                          nil];
        
        @try {
            success=[database executeUpdate:query withArgumentsInArray:insertParameters];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
    }
    
    
    [database commit];
    [database close];
    return success;
}
-(BOOL)saveCapturedImagesWithMetaData:(NSMutableArray *)categoryArray product:(NSArray *)productArray
{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"swx.sqlite"]];
    [database open];
    [database beginTransaction];
    BOOL success=NO;
    // NSMutableArray *array = [self fetchCategoryArrayForAll];
    for(int i=0;i<[categoryArray count]; i++){
        NSString *rowID;
        NSString *agencyCode;
        NSString *categoryCode;
        NSString *brandCode;
        NSString *selectedImageID;
        NSString *inventoryItemId;
        NSString *loggedAt;
        NSString *lastUpdatedAt;
        if([[categoryArray objectAtIndex:i] isKindOfClass:[VisitImagesProducts class]]){
            VisitImagesProducts * array = [categoryArray objectAtIndex:i];
            // for (VisitImagesProducts *product in array) {
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.inventory_Item_ID ==[cd] %@",array.inventory_Item_ID];
            NSMutableArray *filteredCategory = [[productArray filteredArrayUsingPredicate:bPredicate] mutableCopy];
            for(VisitImagesCategories *category in filteredCategory){
                rowID = [NSString createGuid];
                agencyCode = category.agency;
                categoryCode = category.category_Code;
                brandCode = category.brand_Code;
                selectedImageID = array.selectedImageID;
                inventoryItemId = array.inventory_Item_ID;
                loggedAt = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
                lastUpdatedAt = @"";
                
            }
            // }
        }else{
            // for(VisitImagesCategories *category in [categoryArray objectAtIndex:i]){
            VisitImagesCategories * category = [categoryArray objectAtIndex:i];
            rowID = [NSString createGuid];
            agencyCode = category.agency;
            categoryCode = category.category_Code;
            brandCode = category.brand_Code;
            selectedImageID = category.selectedImageID;
            inventoryItemId = @"";
            loggedAt = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
            lastUpdatedAt = @"";
            // }
            
        }
        NSString *query = @"INSERT OR Replace INTO TBL_Visit_Image_Metadata (Row_ID,Image_ID,Agency_Code, Brand_Code,Category_Code,Inventory_Item_ID,Logged_At,Last_Updated_At)  VALUES (?,?,?,?,?,?,?,?)";
        NSMutableArray *insertParameters=[[NSMutableArray alloc]initWithObjects:
                                          rowID,
                                          selectedImageID,
                                          agencyCode,
                                          brandCode,
                                          categoryCode ,
                                          inventoryItemId ,
                                          loggedAt,
                                          lastUpdatedAt,
                                          nil];
        @try {
            success=[database executeUpdate:query withArgumentsInArray:insertParameters];
            
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        
    }
    [database commit];
    [database close];
    return success;
}

-(NSMutableArray *)fetchCategoryList{
    
    NSArray *categoryArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"Select IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID, IFNULL(Agency,'N/A') AS Agency,IFNULL(Brand_Code,'N/A') AS Brand_Code,IFNULL(Prod_Category,'N/A') AS Prod_Category from TBL_Product ORDER By Prod_Category"];
    if(categoryArray.count > 0)
    {
        for (NSMutableDictionary *dict in categoryArray) {
            [dict setValue:KAppControlsNOCode forKey:@"isSelected"];
        }
        return categoryArray;
    }
    else
    {
        return nil;
    }
}

-(NSMutableArray *) fetchCategoryArrayForAll{
    NSMutableArray *categoryArray = [[NSMutableArray alloc]init];
    NSMutableArray *tempcategoryArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select distinct P.Inventory_Item_ID as inventory_item_id,Agency,(select  Code_Description  as Category_Desc from TBL_App_Codes where Code_Type ='PROD_Agency' and Code_Value=P.Agency) Prod_Agency_Desc, Brand_Code,(select  Code_Description  as Category_Desc from TBL_App_Codes where Code_Type ='PROD_BRAND_CODE' and Code_Value=P.Brand_Code) Prod_Brand_Desc,Prod_Category,(select  Code_Description  as Category_Desc from TBL_App_Codes where Code_Type ='PROD_CATEGORY' and Code_Value=P.Prod_Category) Prod_Category_Desc  from TBL_Product P where not( ifnull(Agency,'')='' or ifnull(Brand_Code,'')='' or ifnull(Prod_Category,'')='')"];
    if (tempcategoryArray.count > 0) {
        for(NSMutableDictionary *cat in tempcategoryArray){
            VisitImagesCategories *category = [[VisitImagesCategories alloc]init];
            category.isSelected =  @"N";
            category.agency = [cat valueForKey:@"Agency"];
            category.inventory_Item_ID = [cat valueForKey:@"inventory_item_id"];
            category.agencyDesc = [cat valueForKey:@"Prod_Agency_Desc"];
            category.brand_Code = [cat valueForKey:@"Brand_Code"];
            category.brandDesc = [cat valueForKey:@"Prod_Brand_Desc"];
            category.category_Code = [cat valueForKey:@"Prod_Category"];
            category.categoryDesc = [cat valueForKey:@"Prod_Category_Desc"];
            category.selectedImageID = @"";
            
            [categoryArray addObject:category];
        }
        return categoryArray;
    }else{
        return [[NSMutableArray alloc]init];
    }
}

-(NSMutableArray *)fetchProductForCategory:(NSString *)inventory_Item_ID{
    NSMutableArray *tempProductArray = [[NSMutableArray alloc]init];
    NSMutableArray *productArray = [[NSMutableArray alloc]init];
    if ([inventory_Item_ID isEqualToString:@""]) {
        tempProductArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID,description from TBL_Product order by description"]];
        if (tempProductArray.count > 0) {
            for(NSMutableDictionary *products in tempProductArray){
                VisitImagesProducts *visitProducts = [[VisitImagesProducts alloc]init];
                visitProducts.inventory_Item_ID = [products valueForKey:@"Inventory_Item_ID"];
                visitProducts.productDescription = [products valueForKey:@"Description"];
                visitProducts.selectedImageID = @"";
                visitProducts.isSelected = @"N";
                [productArray addObject:visitProducts];
            }
        }
        
    }else{
        tempProductArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select IFNULL(Inventory_Item_ID,'N/A') AS Inventory_Item_ID,IFNULL(Description,'N/A') AS Description from TBL_Product where Prod_Category = '%@' order by Description",inventory_Item_ID]];
        if (tempProductArray.count > 0) {
            for(NSMutableDictionary *products in tempProductArray){
                VisitImagesProducts *visitProducts = [[VisitImagesProducts alloc]init];
                visitProducts.inventory_Item_ID = [products valueForKey:@"Inventory_Item_ID"];
                visitProducts.productDescription = [products valueForKey:@"Description"];
                visitProducts.selectedImageID = @"";
                visitProducts.isSelected = @"N";
                [productArray addObject:visitProducts];
            }
        }
    }
    return productArray;
    
}

///////////////Send Feedbacks

- (void)writeFeedbackXMLString:(NSString *)customerID {

    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *feedbackArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FeedBack_Request where Customer_ID = '%@'",customerID]];
    
    if(feedbackArray.count > 0)
    {
        [xmlWriter writeStartElement:@"FeedBacks"];
        
        for (int i=0; i<[feedbackArray count]; i++)
        {
            NSMutableDictionary *feedbackDictionary = [NSMutableDictionary dictionaryWithDictionary:[feedbackArray objectAtIndex:i]];
            
            NSMutableArray *arrFeedbackItem = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_FeedBack_Request_Items where FeedBack_ID = '%@' and FeedBack is Not NULL",[feedbackDictionary stringForKey:@"FeedBack_ID"]]];
            
            if (arrFeedbackItem.count > 0) {
                [xmlWriter writeStartElement:@"Feedback"];
                
                if (![[feedbackDictionary stringForKey:@"FeedBack_ID"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"FeedBack_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C1"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"FeedBack_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Customer_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C2"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Customer_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Site_Use_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C3"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Site_Use_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"SalesRep_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C4"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"SalesRep_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Emp_Code"].length !=0){
                    [xmlWriter writeStartElement:@"C5"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Emp_Code"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Created_At"].length !=0){
                    [xmlWriter writeStartElement:@"C6"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Created_At"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Status"].length !=0){
                    [xmlWriter writeStartElement:@"C7"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Status"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Last_Updated_At"].length !=0){
                    [xmlWriter writeStartElement:@"C8"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Last_Updated_At"]];
                    [xmlWriter writeEndElement];
                }
                
                if (![[feedbackDictionary stringForKey:@"Confirmation_Date"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Confirmation_Date"].length !=0){
                    [xmlWriter writeStartElement:@"C9"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Confirmation_Date"]];
                    [xmlWriter writeEndElement];
                } else {
                    [xmlWriter writeStartElement:@"C9"];
                    [xmlWriter writeCharacters:@""];
                    [xmlWriter writeEndElement];
                }
                
                if (![[feedbackDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                    [xmlWriter writeStartElement:@"C10"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Custom_Attribute_1"]];
                    [xmlWriter writeEndElement];
                } else {
                    [xmlWriter writeStartElement:@"C10"];
                    [xmlWriter writeCharacters:@""];
                    [xmlWriter writeEndElement];
                }
                
                if (![[feedbackDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                    [xmlWriter writeStartElement:@"C11"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Custom_Attribute_2"]];
                    [xmlWriter writeEndElement];
                } else {
                    [xmlWriter writeStartElement:@"C11"];
                    [xmlWriter writeCharacters:@""];
                    [xmlWriter writeEndElement];
                }
                
                if (![[feedbackDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                    [xmlWriter writeStartElement:@"C12"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Custom_Attribute_3"]];
                    [xmlWriter writeEndElement];
                } else {
                    [xmlWriter writeStartElement:@"C12"];
                    [xmlWriter writeCharacters:@""];
                    [xmlWriter writeEndElement];
                }
                
                
                if (![[feedbackDictionary stringForKey:@"Request_ID"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Request_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C13"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Request_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Expiry_Date"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Expiry_Date"].length !=0){
                    [xmlWriter writeStartElement:@"C14"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Expiry_Date"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Closed_Date"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Closed_Date"].length !=0){
                    [xmlWriter writeStartElement:@"C15"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Closed_Date"]];
                    [xmlWriter writeEndElement];
                }
                if (![[feedbackDictionary stringForKey:@"Last_Received_Date"] isEqualToString:@"<null>"] && [feedbackDictionary stringForKey:@"Last_Received_Date"].length !=0){
                    [xmlWriter writeStartElement:@"C16"];
                    [xmlWriter writeCharacters:[feedbackDictionary stringForKey:@"Last_Received_Date"]];
                    [xmlWriter writeEndElement];
                }
                
                if([arrFeedbackItem count] > 0)
                {
                    [xmlWriter writeStartElement:@"LineItems"];
                    for (int i=0; i<[arrFeedbackItem count]; i++)
                    {
                        NSMutableDictionary *feedbackItemDictionary = [NSMutableDictionary dictionaryWithDictionary:[arrFeedbackItem objectAtIndex:i]];
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[feedbackItemDictionary stringForKey:@"FeedBackLine_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"FeedBackLine_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"FeedBackLine_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"FeedBack_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"FeedBack_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"FeedBack_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Organization_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Organization_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Organization_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Item_Type"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Item_Type"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Item_Type"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"FeedBack"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"FeedBack"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"FeedBack"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Response_Date"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Response_Date"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Response_Date"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if (![[feedbackItemDictionary stringForKey:@"Comments"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Comments"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Comments"]];
                            [xmlWriter writeEndElement];
                        } else {
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:@""];
                            [xmlWriter writeEndElement];
                        }
                        
                        if (![[feedbackItemDictionary stringForKey:@"Created_At"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Created_At"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Created_At"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Visit_ID"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Visit_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Visit_Date"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Visit_Date"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Visit_Date"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Lat_Value"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Lat_Value"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Lat_Value"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Long_Value"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Long_Value"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Long_Value"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Status"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C14"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Status"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Last_Updated_At"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Last_Updated_At"].length !=0){
                            [xmlWriter writeStartElement:@"C15"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Last_Updated_At"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Request_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Request_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C16"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Request_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Request_Line_ID"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Request_Line_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C17"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Request_Line_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C18"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C19"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[feedbackItemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [feedbackItemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C20"];
                            [xmlWriter writeCharacters:[feedbackItemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                    [xmlWriter writeEndElement];//lineItems
                }
            }
            [xmlWriter writeEndElement];//Feedback
        }
        [xmlWriter writeEndElement];//FeedBacks
    }
    Singleton *single = [Singleton retrieveSingleton];
    single.xmlParsedString = [xmlWriter toString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"done-02"];
#pragma clang diagnostic pop
    });
}

#pragma mark writeIdleReasonXML
- (void)writeIdleReasonXMLString {

    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *reasonArray = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Idle_Time_Reason"]];
    
    if(reasonArray.count > 0)
    {
        [xmlWriter writeStartElement:@"FSRIdleReasons"];
        for (int i=0; i<[reasonArray count]; i++)
        {
            [xmlWriter writeStartElement:@"R"];
            
            NSMutableDictionary *idleReasonDictionary = [NSMutableDictionary dictionaryWithDictionary:[reasonArray objectAtIndex:i]];
            
            if (![[idleReasonDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[idleReasonDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"SalesRep_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[idleReasonDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[idleReasonDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[idleReasonDictionary stringForKey:@"Reason_Code"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Reason_Code"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Reason_Code"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[idleReasonDictionary stringForKey:@"Comments"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Comments"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Comments"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[idleReasonDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[idleReasonDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [idleReasonDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[idleReasonDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            [xmlWriter writeEndElement];//R
        }
        [xmlWriter writeEndElement];//FSRIdleReasons
    }
    Singleton *single = [Singleton retrieveSingleton];
    single.xmlParsedString = [xmlWriter toString];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:@"done-03"];
#pragma clang diagnostic pop
    });
}

#pragma mark sync_data_delete methods
-(void)deleteOrdersAfterBackGroundSync:(NSString *)syncStr AndLastSyncTime:(NSString *)lastSync
{
    FMDatabase *db = [self getDatabase];
    [db open];
    [db beginTransaction];
    BOOL success = NO;
    NSMutableArray * bsDeleteQueriesarray=[[NSMutableArray alloc]initWithObjects:
                                           [NSString stringWithFormat:K_BS_SQLdbGetDeleteOrderLots,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbGetDeleteOrderLineItems,lastSync,syncStr],
                                           [NSString stringWithFormat:K_BS_SQLdbDeleteOrders,lastSync,syncStr],
                                           nil];
    
    for (NSInteger i=0; i<bsDeleteQueriesarray.count; i++) {
        @try
        {
            success =  [db executeUpdate:[bsDeleteQueriesarray objectAtIndex:i]];
            if(success)
                NSLog(@"Order line items deleted");
        }
        @catch (NSException *exception)
        {
            break;
            NSLog(@"Exception while deleting events from database: %@", exception.reason);
        }
        @finally {
           // [db close];
        }
    }
    
    [db commit];
    [db close];
}

@end
