//
//  SalesWorxFeedbackViewController.m
//  SalesWars
//
//  Created by Neha Gupta on 1/30/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import "SalesWorxFeedbackViewController.h"
#import "MedRepDefaults.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxFeedbackTableViewCell.h"
#import "SWAppDelegate.h"

@interface SalesWorxFeedbackViewController ()<UITextFieldDelegate>

@end

static NSString *ClientVersion = @"1";

@implementation SalesWorxFeedbackViewController

- (id)initWithCustomer:(NSDictionary *)c andVisitID:(NSString *)currentVisitID {
    self = [super init];
    
    if (self) {
        customer = c;
        VisitID = currentVisitID;
        [self setTitle:NSLocalizedString(@"Feedback", nil)];
        [self.navigationController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self setupView];
    [self setNavigBar];
    
    if ([customer objectForKey:@"Customer_Name"]) {
       lblName.text = [customer valueForKey:@"Customer_Name"];
    }
    if ([customer objectForKey:@"Customer_No"]) {
         lblNumber.text = [customer valueForKey:@"Customer_No"];
    }
    if ([customer objectForKey:@"Avail_Bal"]) {
        
        NSString *availBal = [NSString stringWithFormat:@"%@",[customer valueForKey:@"Avail_Bal"]];
        if (availBal != nil && availBal.length>0){
            lblAvaialbleBal.text = [availBal currencyString];
        }
    }
   
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    ClientVersion =avid;
    
    SWAppDelegate * appDeleage=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    latitudePoint=[NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.latitude];
    longitudePoint=[NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.longitude];
    if (latitudePoint==nil) {
        latitudePoint=@"0.0";
        longitudePoint = @"0.0";
    }
    
    
    indexPathArray = [[NSMutableArray alloc]init];
    itemsArray = [[NSMutableArray alloc]init];
    NSMutableArray *tempItemsArray = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"select P.Item_Code,P.Description,P.Inventory_Item_ID, I.FeedBackLine_ID, I.FeedBack_ID, I.FeedBack, I.Comments, I.Item_Type from TBL_Product AS P inner join TBL_FeedBack_Request_Items AS I on P.Inventory_Item_ID = I.Inventory_Item_ID and I.Organization_ID=P.Organization_ID inner join TBL_FeedBack_Request as R ON R.FeedBack_ID = I.FeedBack_ID where R.Customer_ID='%@' and Expiry_Date > datetime('now', 'localtime') Order by P.Description ASC",[customer valueForKey:@"Customer_ID"]]];
    
    for (NSDictionary *dic in tempItemsArray) {
        VisitOption_Feedback *objFeedback = [[VisitOption_Feedback alloc]init];
        objFeedback.feedbackID = [SWDefaults getValidStringValue:[dic valueForKey:@"FeedBack_ID"]];
        objFeedback.feedbackLineID = [SWDefaults getValidStringValue:[dic valueForKey:@"FeedBackLine_ID"]];
        objFeedback.itemCode = [SWDefaults getValidStringValue:[dic valueForKey:@"Item_Code"]];
        objFeedback.itemDescription = [SWDefaults getValidStringValue:[dic valueForKey:@"Description"]];
        objFeedback.inventoryItemID = [SWDefaults getValidStringValue:[dic valueForKey:@"Inventory_Item_ID"]];
        objFeedback.itemType = [SWDefaults getValidStringValue:[dic valueForKey:@"Item_Type"]];
        
        NSString *feedback = [SWDefaults getValidStringValue:[dic valueForKey:@"FeedBack"]];
        NSString *comments = [SWDefaults getValidStringValue:[dic valueForKey:@"Comments"]];
        
        if (feedback.length > 0) {
            objFeedback.feedbackStatus = feedback;
            objFeedback.comments = comments;
            objFeedback.isSave = YES;
        } else {
            objFeedback.feedbackStatus = @"";
            objFeedback.comments = @"";
            objFeedback.isSave = NO;
        }
        
        [itemsArray addObject:objFeedback];
    }
    
    
    statusArray = [[NSMutableArray alloc]init];
    NSMutableArray *appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select Code_Description from TBL_App_Codes where Code_Type = 'FEEDBACK_LIST'"];
    if (appCodeArray.count>0)
    {
        for (NSDictionary *dic in appCodeArray) {
            [statusArray addObject:[dic valueForKey:@"Code_Description"]];
        }
    }
    
    if ([itemsArray count] == 0)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    
    if (@available(iOS 15.0, *)) {
        itemTableView.sectionHeaderTopPadding = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Feedback View"];
}

-(void)setupView{
    
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    viewParentAvilableBal.backgroundColor = [UIColor clearColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    
    itemTableView.layer.cornerRadius = 4.0;
    itemTableView.layer.masksToBounds = YES;
    
    imgEnableTickMark = @"tick" ;
    imgDisableTickMark = @"tickDisable" ;
}

-(void)setNavigBar{
   
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [close setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = close;
    
    UIBarButtonItem *confirm = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm", nil) style:UIBarButtonItemStyleDone target:self action:@selector(ConfirmButtonTapped:)];
    
    
    [confirm setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     nil]
                           forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = confirm;
    
}

- (void) handleBack:(id)sender
{
    for (VisitOption_Feedback *objFeedback in itemsArray) {
        if (!objFeedback.isSave && objFeedback.feedbackStatus.length > 0) {
            
            anyUpdated = YES;
            break;
        }
    }
    
    if(anyUpdated)
    {
        UIAlertAction *yesAction = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                                 [self.navigationController popViewControllerAnimated:YES];
                             }];
        
        UIAlertAction *noAction = [UIAlertAction
                             actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                             }];
   
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Would you like to close the form without giving the feedback?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
    }
    else {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return itemsArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44.0)];
    
    tabelHeaderView.backgroundColor = UIColorFromRGB(0xEBFBF9); //kHeaderbackgroundColor;
   // [tabelHeaderView.layer setBorderWidth: 1.0];
    //[tabelHeaderView.layer setMasksToBounds:YES];
    //[tabelHeaderView.layer setBorderColor:UIColorFromRGB(0xE3E4E6).CGColor];
    [view addSubview:tabelHeaderView];
    
    return view;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"cell";
    
    SalesWorxFeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxFeedbackTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if ([indexPathArray containsObject:indexPath]) {
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblInventoryItemID.textColor = [UIColor whiteColor];
        cell.statusTextField.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
    }
    else {
        cell.lblCode.textColor = UIColorFromRGB(0x2C394A); //MedRepDescriptionLabelFontColor;
        cell.lblDescription.textColor = UIColorFromRGB(0x2C394A); //MedRepDescriptionLabelFontColor;
        cell.lblInventoryItemID.textColor = UIColorFromRGB(0x2C394A); //MedRepDescriptionLabelFontColor;
        cell.statusTextField.textColor = UIColorFromRGB(0x2C394A); //MedRepDescriptionLabelFontColor;
        [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    }
    
    VisitOption_Feedback *objFeedback = [itemsArray objectAtIndex:indexPath.row];
    
    cell.lblCode.text = objFeedback.itemCode;
    cell.lblDescription.text = objFeedback.itemDescription;
    cell.lblInventoryItemID.text = objFeedback.itemType;
    
    cell.statusTextField.text = objFeedback.feedbackStatus;
    cell.statusTextField.tag = indexPath.row;
    cell.statusTextField.delegate = self;
    
    if (objFeedback.isSave) {
        cell.btnSave.userInteractionEnabled = NO;
        [cell.btnSave setImage:[UIImage imageNamed:imgDisableTickMark] forState:UIControlStateNormal];
    } else {
        cell.btnSave.userInteractionEnabled = YES;
        [cell.btnSave setImage:[UIImage imageNamed:imgEnableTickMark] forState:UIControlStateNormal];
       
    }
    cell.btnSave.tag = indexPath.row;
    [cell.btnSave addTarget:self action:@selector(btnSaveFeedback:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.lblCode.font=kFontWeblySleekSemiBold(14);
    cell.lblDescription.font=kFontWeblySleekSemiBold(14);
    cell.lblInventoryItemID.font=kFontWeblySleekSemiBold(14);
    cell.statusTextField.font=kFontWeblySleekSemiBold(14);
    return cell;
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(MedRepTextField *)textField;
{
    selectedFeedback = [itemsArray objectAtIndex:textField.tag];
    selectedTextField = textField;
    [self btnDropDown:textField];
    return NO;
}

- (IBAction)btnDropDown:(id)sender
{
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = statusArray;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    popOverVC.popoverType = @"Visit_Option_Feedback";
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.popOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark
- (void)btnSaveFeedback:(UIButton *) sender {

    selectedFeedback = [itemsArray objectAtIndex:sender.tag];
    if ([selectedFeedback.feedbackStatus isEqualToString:@""]) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select status to save" withController:self];
    } else {
        selectedFeedback.isSave = YES;
        SalesWorxFeedbackTableViewCell *cell = [itemTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
        cell.btnSave.userInteractionEnabled = NO;
        [cell.btnSave setImage:[UIImage imageNamed:imgDisableTickMark] forState:UIControlStateNormal];

        NSString *currentDate = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
        
        if (selectedFeedback.comments.length == 0) {
            [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_FeedBack_Request_Items set FeedBack = '%@', Response_Date = '%@', Visit_ID = '%@', Status = 'Y', Last_Updated_At = '%@', Visit_Date = '%@', Lat_Value = '%@', Long_Value = '%@' where FeedBackLine_ID = '%@'",selectedFeedback.feedbackStatus, currentDate, VisitID, currentDate, currentDate, latitudePoint, longitudePoint, selectedFeedback.feedbackLineID]];
        } else {
            [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_FeedBack_Request_Items set FeedBack = '%@', Response_Date = '%@', Comments = '%@', Visit_ID = '%@', Status = 'Y', Last_Updated_At = '%@', Visit_Date = '%@', Lat_Value = '%@', Long_Value = '%@' where FeedBackLine_ID = '%@'",selectedFeedback.feedbackStatus, currentDate, selectedFeedback.comments, VisitID, currentDate, currentDate, latitudePoint, longitudePoint, selectedFeedback.feedbackLineID]];
        }
        
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_FeedBack_Request set Last_Updated_At = '%@' where Customer_ID = '%@'",currentDate, [customer valueForKey:@"Customer_ID"]]];
        
        NSMutableArray *feedbackArray = [[NSMutableArray alloc]init];
        [feedbackArray addObject:selectedFeedback];
        
        [self sendFeedback:feedbackArray];
    }
}

- (void)ConfirmButtonTapped:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.feedbackStatus.length > 0"];
    NSArray *filteredArray = [itemsArray filteredArrayUsingPredicate:predicate];
    
    if (filteredArray.count == itemsArray.count) {

        NSString *currentDate = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];
        for (VisitOption_Feedback *objFeedback in itemsArray) {
            
            if (!objFeedback.isSave) {
                [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_FeedBack_Request_Items set FeedBack = '%@', Response_Date = '%@', Comments = '%@', Visit_ID = '%@', Status = 'Y', Last_Updated_At = '%@', Visit_Date = '%@', Lat_Value = '%@', Long_Value = '%@' where FeedBackLine_ID = '%@'",objFeedback.feedbackStatus, currentDate, objFeedback.comments, VisitID, currentDate, currentDate, latitudePoint, longitudePoint, objFeedback.feedbackLineID]];
            }
        }
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"update TBL_FeedBack_Request set Status = 'Y', Last_Updated_At = '%@', Confirmation_Date = '%@' where Customer_ID = '%@'",currentDate, currentDate, [customer valueForKey:@"Customer_ID"]]];
        
        [self sendFeedback:itemsArray];
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(@"Feedback saved successfully", nil) withController:self];
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please give feedback for all", nil) withController:self];
    }
}

#pragma mark UIPopOverDelegate
-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
//    anyUpdated = YES;
    NSString *selectedStatus = [statusArray objectAtIndex:selectedIndexPath.row];
    if (![selectedStatus isEqualToString:selectedTextField.text]) {
        SalesWorxFeedbackTableViewCell *cell = [itemTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedTextField.tag inSection:0]];
        cell.btnSave.userInteractionEnabled = YES;
        selectedFeedback.isSave = NO;
        [cell.btnSave setImage:[UIImage imageNamed:imgEnableTickMark] forState:UIControlStateNormal];
        
        selectedFeedback.feedbackStatus = [statusArray objectAtIndex:selectedIndexPath.row];
        selectedTextField.text = selectedFeedback.feedbackStatus;
    }
}
-(void)didSaveFeedbackForOtherReason:(NSString *)selectedStatus {
//    anyUpdated = YES;

    SalesWorxFeedbackTableViewCell *cell = [itemTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedTextField.tag inSection:0]];
    cell.btnSave.userInteractionEnabled = YES;
    selectedFeedback.isSave = NO;
    [cell.btnSave setImage:[UIImage imageNamed:imgEnableTickMark] forState:UIControlStateNormal];
    selectedFeedback.feedbackStatus = selectedFeedback.feedbackStatus = @"OTHER REASON";;
    selectedFeedback.comments = selectedStatus;
    selectedTextField.text = @"OTHER REASON";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)sendFeedback:(NSMutableArray *)feedbackArray
{
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.isFromSync = 2;
        single.isActivated = YES;
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [[SWDatabaseManager retrieveManager] writeFeedbackXMLString:[customer valueForKey:@"Customer_ID"]];
        [[SWDatabaseManager retrieveManager] setTarget:self];
        [[SWDatabaseManager retrieveManager] setAction:@selector(xmlFeedbackDone:)];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"You license has been expired or contact your administrator for further assistance" withController:self];
    }
}

-(void)xmlFeedbackDone:(NSString *)st
{
    if([st isEqualToString:@"done-02"])
    {
        Singleton *single = [Singleton retrieveSingleton];
        NSString *XMLPassingString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)single.xmlParsedString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
        [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecFSRFeedBack" andProcParam:@"FeedBackList" andProcValue: XMLPassingString];
    }
    else
    {
        //hide local notification
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
        [self stopAnimationofFeedback];
        [self performSelector:@selector(stopAnimationofFeedback) withObject:nil afterDelay:0.1];
    }
}
- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData *dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *serverAPI = [NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    NSString *strurl = [serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];

    
    NSString *strDeviceID = [[DataSyncManager sharedManager]getDeviceID];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray *resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         
         if([procName isEqualToString:@"sync_MC_ExecFSRFeedBack"])
         {
             resultArray = [responseText JSONValue];
             NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
             if([ProcResponse isEqualToString:@"Feedback imported successfully"])
             {
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofFeedback) withObject:nil afterDelay:0.1];
             }
             else
             {
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofFeedback) withObject:nil afterDelay:0.1];
                 [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:ProcResponse withController:self];
             }
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
        [[DataSyncManager sharedManager] hideCustomIndicator];
        [self performSelector:@selector(stopAnimationofFeedback) withObject:nil afterDelay:0.1];
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Server unreachable at the moment" withController:self];
    }];
    
    //call start on your request operation
    [operation start];
}

- (void)stopAnimationofFeedback
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
}

@end
