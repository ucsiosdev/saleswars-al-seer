//
//  SWRouteViewController.m
//  SWRoute
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWRouteViewController.h"
#import "CashCutomerViewController.h"
#import "LocationFilterViewController.h"
#import "SWAppDelegate.h"
#import "SWRouteTableViewCell.h"
#import "SWSessionConstants.h"

@interface SWRouteViewController (){
    UIBarButtonItem *startVisitButton;
    UIBarButtonItem *barCodeButton;
}
@end

@implementation SWRouteViewController

- (void)viewDidLoad {
    
    NSLog(@"did load called");
    
    [super viewDidLoad];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Route"];
    
     NSString* identifier=@"SWRouteTableViewCell";
    [listTableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];

    
    barCodeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarCode_Scan"] style:UIBarButtonItemStylePlain target:self action:@selector(barCodeButtonTapped:)];
    //set up pop over
    startVisitButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Start_Visit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitTapped:)];
    
   // self.navigationItem.rightBarButtonItems=@[startVisitButton,barCodeButton];
    
    isFurtureDate = NO;
    
    selectedDate=[NSDate date];
    customer = [NSMutableDictionary dictionary];
    listTableView.tableFooterView = [UIView new];
    
    locationMapView.layer.cornerRadius = 8.0f;
    locationMapView.layer.masksToBounds = YES;
    locationMapView.layer.borderWidth = 2.0;
    locationMapView.layer.borderColor = [UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1.0].CGColor;
    plannedBlock.layer.cornerRadius = 8.0f;
    plannedBlock.layer.masksToBounds = YES;
    completedBlock.layer.cornerRadius = 8.0f;
    completedBlock.layer.masksToBounds = YES;
    adherenceBlock.layer.cornerRadius = 8.0f;
    adherenceBlock.layer.masksToBounds = YES;

    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
    if (routes.count>0) {
        [listTableView reloadData];
        [self PlaceAnnotationOnMapView];
    }
    noDataView.hidden = false;
    dateView.layer.cornerRadius = 8.0;
    dateView.layer.borderWidth = 1.0;
    dateView.layer.borderColor = [UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1].CGColor;
    dateView.layer.masksToBounds = YES;
    listTableView.layer.cornerRadius = 8.0;
    listTableView.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDate:) name:kReloadRouteNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isMovingToParentViewController) {
        isFurtureDate = NO;
        selectedDate=[NSDate date];
        customer = [NSMutableDictionary dictionary];
    }
    
    [Flurry logEvent:@"Route View"];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [showDateButton setTitle:[NSString stringWithFormat:@"%@",[formatter stringFromDate:selectedDate]] forState:UIControlStateNormal];
    
    
    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
    [listTableView reloadData];
}


- (void)updateDate:(NSNotification *)notification
{
    NSLog(@"application enterted foreground notification received");
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [showDateButton setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
    

    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
    
    [listTableView reloadData];
    
    NSLog(@"selected date refreshed due to notification %@",[selectedDate description]);
}

-(void)navigatetodeals
{
    [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
}



- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
{
    if (annotation == mapView.userLocation)
    {
        return nil;
    }
    else
    {
        MKAnnotationView *pin = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: @"VoteSpotPin"];
        if (pin == nil)
        {
            pin = [[MKAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"TestPin"];
        }
        else
        {
            pin.annotation = annotation;
        }

        if([[annotation title] isEqualToString:[customer stringForKey:@"Customer_Name"]])
        {
            [pin setImage:[UIImage imageNamed:@"Map_Marker"]];
        } else {
            [pin setImage:[UIImage imageNamed:@"Un_Map_Marker"]];
        }
        
        pin.canShowCallout = YES;
        return pin;
    }
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![view.annotation.title isEqualToString:[customer stringForKey:@"Customer_Name"]]) {
        SWAppDelegate * appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        double currentLat=appDelegate.currentLocation.coordinate.latitude;
        double currentLong=appDelegate.currentLocation.coordinate.longitude;
        
        
        float custLat = [[customer stringForKey:@"Cust_Lat"] floatValue];
        float custLong = [[customer stringForKey:@"Cust_Long"] floatValue];
        
        
        NSString *browserUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f",   currentLat, currentLong,custLat,custLong];
        NSString *googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f", currentLat, currentLong,custLat,custLong];
        BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
        
        if (canHandle) {
            // Google maps installed
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
            
            
        } else {
            // Use Apple maps?
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:browserUrlString]];
        }
        
    } else {
        
        MKPointAnnotation *selectedAnnotation = (MKPointAnnotation *)view.annotation;
        [selectedAnnotation setTitle:[customer stringForKey:@"Customer_Name"]];
        [locationMapView addAnnotation:selectedAnnotation];
    }
}
- (void)locationAction{
    
    
    SWAppDelegate* appDel=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    CLLocationCoordinate2D custLocation;
    custLocation.latitude=appDel.currentLocation.coordinate.latitude;
    custLocation.longitude=appDel.currentLocation.coordinate.longitude;
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (custLocation, 1000, 1000);
    [locationMapView setRegion:regionCustom animated:NO];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 return NO;
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {}

- (void)PlaceAnnotationOnMapView
{
    
    if([routes count]==0)
    {
        noDataView.hidden = false;
        self.navigationItem.rightBarButtonItems=nil;
    }
    else
    {
        self.navigationItem.rightBarButtonItems=@[startVisitButton,barCodeButton];
        noDataView.hidden = true;
        locationMapView.showsUserLocation = YES;
        [locationMapView removeAnnotations:locationMapView.annotations];
        [self performSelector:@selector(locationAction) withObject:nil afterDelay:0.1];
        
        visitedCustomer = 0;
        remainningCustomer = 0;
        
        
        annotationsArray=[[NSMutableArray alloc]init];
        
        for (int i=0; i<[routes count]; i++)
        {
            @autoreleasepool {
                place = [[Place2 alloc] init]  ;
                place.latitude= [[[routes objectAtIndex:i] objectForKey:@"Cust_Lat"] floatValue];
                place.longitude =  [[[routes objectAtIndex:i] objectForKey:@"Cust_Long"] floatValue];
                place.name= [[routes objectAtIndex:i] objectForKey:@"Customer_Name"];
                
                NSString *detailString = [[routes objectAtIndex:i] objectForKey:@"City"];
                NSString *timeString = [[routes objectAtIndex:i] objectForKey:@"Visit_Start_Time"];
                
                
                
                
                
                if ([[routes objectAtIndex:i] objectForKey:@"Visit_Start_Time"] != [NSNull null])
                {
                    NSString *newStr = [timeString substringFromIndex:11];
                    detailString = [detailString stringByAppendingString:[NSString stringWithFormat:@"  Time:%@",newStr] ];
                    place.otherDetail = detailString;
                }
                Annotation=[[CSMapAnnotation2 alloc]initWithPlace:place];
                
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                //[annotation setCoordinate:centerCoordinate];
                
                CLLocationCoordinate2D coord;
                
                coord.latitude=place.latitude;
                coord.longitude=place.longitude;
                
                [annotation setCoordinate:coord];
                
                NSString * annotationTitle=[[routes objectAtIndex:i] objectForKey:@"Customer_Name"];
                [annotation setTitle:annotationTitle];
                [locationMapView addAnnotation:annotation];
                [annotationsArray addObject:annotation];
                
                NSLog(@"check routes for titles %@", [routes description]);
                
                NSString *isVisit = [[routes objectAtIndex:i] objectForKey:@"Visit_Status"];
                if([isVisit isEqualToString:@"N"])
                {
                    remainningCustomer = remainningCustomer +1;
                }
                else
                {
                    visitedCustomer= visitedCustomer + 1;
                }
            }
        }
        double a = ((double)visitedCustomer/(double) [routes count])*100;
        plannedCountLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[routes count]];
        completedCountLabel.text = [NSString stringWithFormat:@"%d", visitedCustomer];
        adherencePercentLabel.text = [NSString stringWithFormat:@"%.0f%@ ", a , @"%"];
    }
}
- (void)centerSelectedCustomerOnMapWithLatitude:(NSString *)lat andLongitude:(NSString *)longt{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.07f, 0.07f);
    CLLocationCoordinate2D coordinate = {[lat floatValue],[longt floatValue]};
    MKCoordinateRegion region = {coordinate, span};
    MKCoordinateRegion regionThatFits = [locationMapView regionThatFits:region];
   [locationMapView setRegion:regionThatFits animated:YES];
    
    [locationMapView setSelectedAnnotations:annotationsArray];
    
}



-(void)displayAnnotationwithTitle:(NSString*)lat andLong:(NSString*)longitude withIndex:(NSInteger)selectedIndex
{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.07f, 0.07f);
    CLLocationCoordinate2D coordinate = {[lat floatValue],[longitude floatValue]};
    MKCoordinateRegion region = {coordinate, span};
    MKCoordinateRegion regionThatFits = [locationMapView regionThatFits:region];
    [locationMapView selectAnnotation:[annotationsArray objectAtIndex:selectedIndex] animated:YES];
    [locationMapView setRegion:regionThatFits animated:YES];

}
- (IBAction)previousButtonTapped:(id)sender {
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *previousDate = [selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
    NSString *preDateString =[formatter stringFromDate:previousDate];
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    if([todayDate isEqualToString:preDateString])
    {
        minusDate.enabled=NO;
        [showDateButton setTitle:[formatter stringFromDate:[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)]] forState:UIControlStateNormal];

        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
        [self dateChanged:nil];
    }
    else{
        [showDateButton setTitle:[formatter stringFromDate:[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)]] forState:UIControlStateNormal];

        selectedDate=[selectedDate dateByAddingTimeInterval:(-1.0*24*60*60)];
        [self dateChanged:nil];
    }
    formatter=nil;
    usLocale=nil;
}

- (IBAction)dateButtonTapped:(id)sender {
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=YES;
    [datePickerViewController.picker setDate:selectedDate];
    datePickerPopOver.delegate=self;
    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    navigationController.navigationBar.translucent = NO;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    [datePickerPopOver presentPopoverFromRect:showDateButton.frame inView:showDateButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
- (IBAction)nextButtonTapped:(id)sender {
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSDate *previousDate = [selectedDate dateByAddingTimeInterval:(1.0*24*60*60)];
    
    NSString *preDateString =[formatter stringFromDate:previousDate];
    
    
    NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
    
    if(![todayDate isEqualToString:preDateString])
    {
        minusDate.enabled=YES;
    }
    [showDateButton setTitle:[formatter stringFromDate:previousDate] forState:UIControlStateNormal];

    selectedDate=previousDate;
    [self dateChanged:nil];
    formatter=nil;
    usLocale=nil;
}
- (void)showInbox:(UIButton *)sender {

    
}
- (void)dateChanged:(SWDatePickerViewController *)sender {
    
    if (sender == nil)
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        [showDateButton setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];
        [listTableView reloadData];
    }
    else
    {
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        NSString *todayDate =  [formatter stringFromDate:[NSDate date]];
        
        NSString *preDateString =[formatter stringFromDate:sender.selectedDate];

        if(![todayDate isEqualToString:preDateString])
        {
            minusDate.enabled=YES;
        }
        else
        {
            minusDate.enabled=NO;

        }
        selectedDate = sender.selectedDate;
      
        [showDateButton setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:selectedDate]];

        [listTableView reloadData];
        formatter=nil;
        usLocale=nil;
    }
    
    NSDate* enteredDate = selectedDate;
    NSDate * today = [NSDate date];
    NSComparisonResult result = [today compare:enteredDate];
    switch (result) 
    {
        case NSOrderedAscending:
            isFurtureDate = YES;
            break;
        case NSOrderedDescending:
            isFurtureDate = NO;
            break;

        case NSOrderedSame:
            isFurtureDate = NO;
            break;

        default:
            isFurtureDate = NO;
            break;
    }
}
#pragma mark SWRouteService Delegate
- (void)getRouteServiceDidGetRoute:(NSArray *)r{
    routes=r;
    [listTableView reloadData];
    [self PlaceAnnotationOnMapView];
    [datePickerPopOver dismissPopoverAnimated:YES];
    r=nil;
}

#pragma mark UITableView Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return routes.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"SWRouteTableViewCell";
    SWRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *data = [routes objectAtIndex:indexPath.row];
    cell.customerNameLabel.numberOfLines = 0;
    cell.customerNameLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_Name"]]];
    cell.customerCodeLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_No"]]];
    
   
//    if (selectedIndexPath == indexPath) {
//        [cell.contentView setBackgroundColor:[UIColor colorWithRed:(255.0/255.0) green:(240.0/255.0) blue:(195.0/255.0) alpha:1]];
//    }else{
//        [cell.contentView setBackgroundColor:[UIColor clearColor]];
//    }
    if([[data stringForKey:@"Visit_Status"] isEqualToString:@"Y"])
    {
        cell.tag = 1;
        [cell.statusView setBackgroundColor:[UIColor colorWithRed:243.0 / 255.0 green:150.0 / 255.0 blue:0.0 / 255.0 alpha:1.0f]];
    }
    else
    {
        cell.tag = 0;
        [cell.statusView setBackgroundColor:[UIColor clearColor]];
    }
    
    
   // cell.backgroundColor = [UIColor colorWithRed:249.0/255 green:249.0/255  blue:249.0/255  alpha:1.0];
    //cell.contentView.backgroundColor = [UIColor colorWithRed:249.0/255 green:249.0/255  blue:249.0/255  alpha:1.0];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    return cell;

}


#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    noDataView.hidden = true;
    selectedIndexPath = indexPath;
    
    NSDictionary *row = [routes objectAtIndex:indexPath.row];
    customer=[row mutableCopy];
    Singleton *single = [Singleton retrieveSingleton];
    single.planDetailId = [row stringForKey:@"FSR_Plan_Detail_ID"];
    
    [locationMapView removeAnnotations:locationMapView.annotations];
    [locationMapView addAnnotations:annotationsArray];
    
    
    [self displayAnnotationwithTitle:[customer stringForKey:@"Cust_Lat"] andLong:[customer stringForKey:@"Cust_Long"] withIndex:indexPath.row];
    customerNameLabel.text = [SWDefaults getValidStringValue:[customer valueForKey:@"Customer_Name"]];
    customerCodeLabel.text = [SWDefaults getValidStringValue:[customer valueForKey:@"Customer_No"]];
    double availBal = [[customer stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
}
- (void)startVisitTapped:(id)sender{
    
    if (selectedIndexPath == nil ){
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select route from list then try again" withController:self];
        return;
    }
    else {
        Singleton *single = [Singleton retrieveSingleton];
        if([[customer stringForKey:@"Cash_Cust"]isEqualToString:@"Y"])
        {
            single.isCashCustomer = @"Y";
            CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customer ];
            [self.navigationController pushViewController:cashCustomer animated:YES];
        }
        else
        {
            if(!isFurtureDate)
            {
                [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[customer stringForKey:@"Customer_ID"]]];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"You can't start visit for future date." withController:self];
            }
        }
    }
}

#pragma Barcode Functions
- (void)barCodeButtonTapped:(id)sender {
    
    Singleton *single = [Singleton retrieveSingleton];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    
    reader.wantsFullScreenLayout = NO;

    reader.readerView.zoom = 1.0;
    
    reader.showsZBarControls=NO;
    
    [self presentViewController: reader animated: YES completion:nil];
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil] ;
}
- (void)imagePickerController: (UIImagePickerController*) reader2 didFinishPickingMediaWithInfo: (NSDictionary*) info{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    [reader dismissViewControllerAnimated: YES completion:nil];
 
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init];
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        single.isBarCode = NO;
    }

}
- (void)filterChangedBarCode {
    [self.navigationController setToolbarHidden:YES animated:YES];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
#pragma mark Customer List service delegate
//- (void)customerServiceDidGetList:(NSArray *)cl{
- (void)getListFunction:(NSArray *)cl
{
    if(![cl count]==0)
    {
        SWSection *section = [cl objectAtIndex:0];
        NSDictionary *row = [section.rows objectAtIndex:0];
        customer=[row mutableCopy] ;
        BOOL isInList=NO;
        for (int i=0; i<[routes count]; i++)
        {
            @autoreleasepool {
            NSDictionary *row1 = [routes objectAtIndex:i];
        
            if ([[row1 stringForKey:@"Customer_Name"] isEqualToString:[row stringForKey:@"Customer_Name"]]) {
                isInList=YES;
            }
            }
        
        }
        if (isInList){
            [self performSelector:@selector(Login_Btn_Clicked:) withObject:nil afterDelay:1.0];
            //[self Login_Btn_Clicked:self];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:[NSString stringWithFormat:@"%@ is not in today's route list",[row stringForKey:@"Customer_Name"]] withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No customer found" withController:self];
    }
    cl=nil;
    
}
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
  
    [SWDefaults setCustomer:customer];
    customer = [SWDefaults validateAvailableBalance:orderAmmount];

   Singleton *single = [Singleton retrieveSingleton];
    single.isCashCustomer = @"N";
    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
    [visitManager startVisitWithCustomer:customer parent:self andChecker:@"R"];
    orderAmmount=nil;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    datePickerPopOver = nil;
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [showDateButton setTitle:[formatter stringFromDate:selectedDate] forState:UIControlStateNormal];
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
