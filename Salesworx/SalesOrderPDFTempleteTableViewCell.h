//
//  SalesOrderPDFTempleteTableViewCell.h
//  SalesWars
//
//  Created by Prashannajeet on 31/08/22.
//  Copyright © 2022 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SalesOrderPDFTempleteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblArticleNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblArticleDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblUOM;
@property (weak, nonatomic) IBOutlet UILabel *lblbarcode;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderQty;
@property (weak, nonatomic) IBOutlet UILabel *lblOUM;

@end

NS_ASSUME_NONNULL_END
