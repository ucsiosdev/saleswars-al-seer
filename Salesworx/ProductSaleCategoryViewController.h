//
//  ProductSaleCategoryViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface ProductSaleCategoryViewController : SWTableViewController  {
    
    SEL action;
    NSDictionary *customer;
    NSArray *items;
    BOOL isPopover;
    UIView *myBackgroundView;
    NSMutableArray *totalOrderArray;
    int indexPathTemplate;
    BOOL isFromVisit;
    NSString *orderID;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


- (id)initWithCustomer:(NSDictionary *)customer;

@end
