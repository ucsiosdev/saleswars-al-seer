//
//  CollectionTypeViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ReturnTypeViewController.h"

@interface ReturnTypeViewController ()

@end

@implementation ReturnTypeViewController

@synthesize target;
@synthesize action;

- (id)initWithEXP {
    self = [super init];
    
    if (self) {
        

        //salesSer.delegate=self;
       
        [self getSalesOrderServiceDiddbGetReturnsType: [[SWDatabaseManager retrieveManager] dbGetReturnsType]];
        [self setPreferredContentSize:CGSizeMake(250, 130)];
        parentType = @"EXP";
    }
    
    return self;
}

- (id)initWithReturnType {
    self = [super init];
    
    if (self) {
        

        //salesSer.delegate=self;
      
        [self getSalesOrderServiceDidGetReasonType:  [[SWDatabaseManager retrieveManager] dbGetReasonCode]];
        [self setPreferredContentSize:CGSizeMake(250, 130)];
        parentType = @"RTN";

    }
    
    return self;
}
- (id)initWithGoods {
    self = [super init];
    
    if (self) {
        
        types=[NSArray arrayWithObjects:@"YES", @"NO", nil];
        [self setPreferredContentSize:CGSizeMake(250, 130)];
        parentType = @"GDC";
        
    }
    
    return self;
}


#pragma Sales Orrder Service Delegate
- (void)getSalesOrderServiceDiddbGetReturnsType:(NSArray *)types2;
{
    types=[NSArray arrayWithArray:types2];
    [self.tableView reloadData];
    types2=nil;
}

- (void)getSalesOrderServiceDidGetReasonType:(NSArray *)reason
{
    types=[NSArray arrayWithArray:reason];
    [self.tableView reloadData];
    reason=nil;
}


#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [types count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if([parentType isEqualToString:@"EXP"] || [parentType isEqualToString:@"RTN"])
    {
        [cell.textLabel setText:[[types objectAtIndex:indexPath.row] stringForKey:@"Description"]];
    }
    else
    {
        [cell.textLabel setText:[types objectAtIndex:indexPath.row]];
    }
    return cell;
}
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([parentType isEqualToString:@"EXP"] || [parentType isEqualToString:@"RTN"])
    {
        NSDictionary *selectedType = [types objectAtIndex:indexPath.row];
        if ([self.target respondsToSelector:self.action]) {
             #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
[self.target performSelector:self.action withObject:selectedType]; 
                #pragma clang diagnostic pop
        }
    }
    else
    {
        NSString *selectedType = [types objectAtIndex:indexPath.row];
        if ([self.target respondsToSelector:self.action]) {
             #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
[self.target performSelector:self.action withObject:selectedType]; 
                #pragma clang diagnostic pop
        }
    }
  
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end