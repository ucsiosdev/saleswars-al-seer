//
//  AlSeerSalesPerAgencyViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlSeerSalesPerAgencyTableViewCell.h"
#import "SalesWorxCustomClass.h"

#define kSalesPerAgencyVarienceLessThanZeroColor [UIColor colorWithRed:(255.0/255.0) green:(114.0/255.0) blue:(71.0/255.0) alpha:1]
#define kSalesPerAgencyVarianceGreaterThanZeroColor [UIColor colorWithRed:(71.0/255.0) green:(114.0/255.0) blue:(210.0/255.0) alpha:1]
#define kSalesPerAgencyCustomerNameColor [UIColor colorWithRed:(89.0/255.0) green:(114.0/255.0) blue:(153.0/255.0) alpha:1]


@interface AlSeerSalesPerAgencyViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>


{
    NSMutableArray * salesPerAgencyArray;
    IBOutlet UIButton *filterButton;
    NSMutableArray* filteredSalesPerAgencyArray;
    NSMutableArray* salesAgencyArray;
    NSMutableArray* groupedAgencyArray;
    NSMutableArray* filteredGroupedAgencyArray;
    
    NSMutableArray* contentArray;
    NSMutableArray* filteredContentArray;
    NSMutableArray* agencyArray;
    NSMutableArray * contentArrayGroupedByAgency;
    NSMutableArray * filteredContentArrayGroupedByAgency;
    
    NSMutableDictionary *previousFilteredParameters;
    
    UIPopoverController *filterPopOverController;
    
    BOOL showQuantityData;
    
    
    
    UIBarButtonItem * filterBarButtonItem;
    
    NSInteger cellCount;
    
    IBOutlet UIScrollView *headerScrollView;
    
    NSInteger currentTopVisibleSection;
   
    
}
- (IBAction)filterButtontapped:(id)sender;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *bgView;

@property (strong, nonatomic) IBOutlet UITableView *salesPerAgencyTableView;
@end
