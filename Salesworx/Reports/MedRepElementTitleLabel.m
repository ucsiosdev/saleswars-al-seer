//
//  MedRepElementTitleLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepElementTitleLabel.h"
#import "SWDefaults.h"
#import "SWFoundation.h"

@implementation MedRepElementTitleLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib{
    [super awakeFromNib];
    self.font=kFontWeblySleekSemiBold(14);
    self.textColor= UIColorFromRGB(0x6A6F7B); // Drak Grey. Taken as SalesWorx > SalesWorxTitleLabel
}

@end
