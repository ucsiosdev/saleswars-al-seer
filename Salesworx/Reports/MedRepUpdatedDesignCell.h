//
//  MedRepUpdatedDesignCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/8/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MedRepUpdatedDesignCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *cellDividerImg;
@property (strong, nonatomic) IBOutlet UIImageView *accessoryImageView;

@property (strong, nonatomic) IBOutlet UILabel *descLbl;
//@property (strong, nonatomic) IBOutlet MedRepView *statusView;

@end
