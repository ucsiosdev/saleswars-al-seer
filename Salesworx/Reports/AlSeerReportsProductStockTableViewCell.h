//
//  AlSeerReportsProductStockTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface AlSeerReportsProductStockTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *stockLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *blockedStockLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *qualityInspectionStockLbl;

@end
