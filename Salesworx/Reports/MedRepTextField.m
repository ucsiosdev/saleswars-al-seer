//
//  MedRepTextField.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepTextField.h"
#import "SWDefaults.h"
#import "SWFoundation.h"
@implementation MedRepTextField
@synthesize dropdownImageView,DropDownImage,readOnlyLayer;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    
    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
//    self.leftView = paddingView;
//    paddingView.userInteractionEnabled=NO;
//    self.leftViewMode = UITextFieldViewModeAlways;
    
    if(self.frame.size.height>35)
    {
        self. font=FilterButtonFont;
    }
    else
    {
        self. font=[UIFont fontWithName:@"WeblySleekUISemibold" size:15];
    }
        self.textColor=SalesWarsMenuTitleDescFontColor;
    
    self.layer.borderWidth=1.0f;
    self.layer.borderColor = UIColorFromRGB(0xE3E4E6).CGColor;;
    self.layer.masksToBounds = NO;

    
    self.layer.cornerRadius=5.0;

    if (self.isMandatory==YES) {
        UILabel *mandatoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
        mandatoryLabel.textColor=[UIColor redColor];
        mandatoryLabel.font=FilterButtonFont;
        mandatoryLabel.text=@"*";
        self.rightView = mandatoryLabel;
        self.rightViewMode = UITextFieldViewModeAlways;
    }

    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
    self.leftView = leftPaddingView;
    leftPaddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;

    if(self.textAlignment==NSTextAlignmentRight)
    {
        UIView *rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];/* 26 is padding +imageView Width*/
        self.rightView = rightPaddingView;
        rightPaddingView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }

}

-(void)setisMandatory:(BOOL)status
{
    
}
-(void)setDropDownImage:(NSString *)ImageName
{
    if(ImageName.length>0)
    {
        UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 16)];/* 26 is padding +imageView Width*/
        dropdownImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,16,16)];
        [dropdownImageView setImage:[UIImage imageNamed:ImageName/*@"MedRepDropdownArrow.png"*/]];
        [dropDownView addSubview:dropdownImageView];
        self.rightView = dropDownView;
        dropDownView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else
    {
      
    }
   
}
-(void)setIsReadOnly:(BOOL)status
{
    if(status==YES)
    {
        [self setEnabled:NO];
        self.textColor=SalesWorxReadOnlyTextFieldTextColor;
        self.backgroundColor=SalesWorxReadOnlyTextFieldColor;
    }
    else
    {
        self.textColor=SalesWarsMenuTitleDescFontColor;
        self.backgroundColor=[UIColor whiteColor];
        [self setEnabled:YES];
    }
}
-(void)showTextfieldAsLabel
{
    UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];/* 26 is padding +imageView Width*/
    self.rightView = dropDownView;
    self.rightViewMode = UITextFieldViewModeAlways;
    self.leftView = dropDownView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    self.layer.borderColor=(__bridge CGColorRef)([UIColor clearColor]);
    
    self.textAlignment=NSTextAlignmentLeft;
}
-(void)showTextfieldAsBox
{
    if(self.frame.size.height>35)
    {
        self. font=FilterButtonFont;
    }
    else
    {
        self. font=[UIFont fontWithName:@"WeblySleekUISemibold" size:14];
    }
    self.textColor=SalesWarsMenuTitleDescFontColor;
    
    self.layer.borderWidth=1.0f;
    self.layer.borderColor=[kFilterButtonBorderColor CGColor];
    self.layer.masksToBounds = NO;
    
    
    self.layer.cornerRadius=5.0;
    
    if (self.isMandatory==YES) {
        UILabel *mandatoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
        mandatoryLabel.textColor=[UIColor redColor];
        mandatoryLabel.font=FilterButtonFont;
        mandatoryLabel.text=@"*";
        self.rightView = mandatoryLabel;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    
    UIView *leftPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];
    self.leftView = leftPaddingView;
    leftPaddingView.userInteractionEnabled=NO;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    if(self.textAlignment==NSTextAlignmentRight)
    {
        UIView *rightPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height*0.2, self.frame.size.height)];/* 26 is padding +imageView Width*/
        self.rightView = rightPaddingView;
        rightPaddingView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    
    if(DropDownImage.length>0)
    {
        UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 16)];/* 26 is padding +imageView Width*/
        dropdownImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,16,16)];
        [dropdownImageView setImage:[UIImage imageNamed:DropDownImage/*@"MedRepDropdownArrow.png"*/]];
        [dropDownView addSubview:dropdownImageView];
        self.rightView = dropDownView;
        dropDownView.userInteractionEnabled=NO;
        self.rightViewMode = UITextFieldViewModeAlways;
    }

}

@end
