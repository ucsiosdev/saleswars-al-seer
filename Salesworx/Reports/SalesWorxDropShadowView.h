//
//  SalesWorxDropShadowView.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxDropShadowView : UIView
@property (nonatomic,strong) IBInspectable UIColor *shadowColor;

@end
