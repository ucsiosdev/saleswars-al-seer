//
//  AlSeerSalesPerAgencyViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerSalesPerAgencyViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "AlSeerSalesPerAgencyFilterViewController.h"
#import "AlSeerSalesPerAgencyTitleHeaderTableViewCell.h"

@interface AlSeerSalesPerAgencyViewController ()

@end

@implementation AlSeerSalesPerAgencyViewController
@synthesize salesPerAgencyTableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
            NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"AlSeerSalesPerAgencyViewController"
                                                              owner:self
                                                            options:nil];
    
    NSLog(@"nob arrauys %@", nibViews);
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Sales Per Agency";
    self.navigationItem.titleView=label;

    salesPerAgencyArray=[[NSMutableArray alloc]init];
    filteredSalesPerAgencyArray=[[NSMutableArray alloc]init];
    groupedAgencyArray=[[NSMutableArray alloc]init];
    salesAgencyArray=[[NSMutableArray alloc]init];
    filteredGroupedAgencyArray=[[NSMutableArray alloc]init];
    
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    
    
     filterBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ReportsFilter.png"] style:UIBarButtonItemStylePlain target:self action:@selector(filterButtontapped:)];
    

    contentArray=[[NSMutableArray alloc]init];
    filteredContentArray=[[NSMutableArray alloc]init];
    agencyArray=[[NSMutableArray alloc]init];
    contentArrayGroupedByAgency=[[NSMutableArray alloc]init];
    filteredContentArrayGroupedByAgency=[[NSMutableArray alloc]init];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        /*filtering products by brand code*/
        
        contentArray=[[[SWDatabaseManager retrieveManager]fetchSalesPerAgencyforAllCustomers] mutableCopy];
        
        filteredContentArray=contentArray;
        agencyArray = [[NSMutableArray alloc]initWithArray:[filteredContentArray valueForKeyPath:@"@distinctUnionOfObjects.Agency"]];
        /** removing invalid brandcodes*/
        [agencyArray removeObject:@""];
    
        
        for (NSString *productBrandCode in agencyArray) {
            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
                                                @"Agency=%@",productBrandCode];
            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[contentArray filteredArrayUsingPredicate:brandCodePredicate]];
            [contentArrayGroupedByAgency addObject:tempProductsArray];
        }
        
        
        filteredContentArrayGroupedByAgency=contentArrayGroupedByAgency;
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

            [salesPerAgencyTableView reloadData];

            [headerScrollView setContentSize:CGSizeMake(salesPerAgencyTableView.contentSize.width, headerScrollView.contentSize.height)];
            
            
            [salesPerAgencyTableView setContentSize:CGSizeMake(1378,salesPerAgencyTableView.contentSize.height)];
            

            
        });
    });
    
    
   
    
    
    
    
    // Do any additional setup after loading the view from its nib.
    
    [salesPerAgencyTableView registerNib:[UINib nibWithNibName:@"AlSeerSalesPerAgencyTableViewCell" bundle:nil] forCellReuseIdentifier:@"salesPerAgencyCell"];

    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterEmpty.png"] forState:UIControlStateNormal];
    
    currentTopVisibleSection=0;
    
    
    salesPerAgencyTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    salesPerAgencyTableView.layer.cornerRadius = 8.0;
    salesPerAgencyTableView.layer.masksToBounds = YES;
    
    if (@available(iOS 15.0, *)) {
        salesPerAgencyTableView.sectionHeaderTopPadding = 0;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{

    NSLog(@"sections in dragging %@",[self indexesOfVisibleSections]);
    
    NSMutableArray *visibleSections = [NSMutableArray array];
    
    for (UITableViewCell * cell in [salesPerAgencyTableView visibleCells]) {
        if (![visibleSections containsObject:[NSNumber numberWithInteger:[salesPerAgencyTableView indexPathForCell:cell].section]]) {
            [visibleSections addObject:[NSNumber numberWithInteger:[salesPerAgencyTableView indexPathForCell:cell].section]];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    
    
    
    UIView * headerView=[self fetchHeader];
    headerView.backgroundColor=[UIColor colorWithRed:(235.0/255.0) green:(251.0/255.0) blue:(249.0/255.0) alpha:1];
    
    [headerScrollView setShowsVerticalScrollIndicator:NO];
    [headerScrollView setShowsHorizontalScrollIndicator:NO];


    [headerScrollView addSubview:headerView];
    
    
    salesPerAgencyTableView.bounces=NO;
    
    NSLog(@"index %@", [self indexesOfVisibleSections]);
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if([scrollView isEqual:salesPerAgencyTableView]) {
        CGPoint offset = headerScrollView.contentOffset;
        offset.x = salesPerAgencyTableView.contentOffset.x;
        [headerScrollView setContentOffset:offset];
    } else {
        CGPoint offset = salesPerAgencyTableView.contentOffset;
        offset.x = headerScrollView.contentOffset.x;
        [salesPerAgencyTableView setContentOffset:offset];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==salesPerAgencyTableView) {
        
        //return [(NSArray*)[filteredContentArrayGroupedByAgency objectAtIndex:section] count];
        
        return [(NSArray*)[filteredContentArrayGroupedByAgency objectAtIndex:section] count];
    }
    else{
        return 0;
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==salesPerAgencyTableView) {
        return [filteredContentArrayGroupedByAgency  count];
    }
    else{
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString* identifier=@"salesPerAgencyCell";
    AlSeerSalesPerAgencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesPerAgencyTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:(239.0/255.0) green:(243.0/255.0) blue:(246.0/255.0) alpha:1]];

    
    
    cell.backgroundColor = [UIColor colorWithRed:(239.0/255.0) green:(243.0/255.0) blue:(246.0/255.0) alpha:1];
    
    
    
    cell.customerNameLbl.textColor = kSalesReportPerAgencyHeaderTitleColor;
    cell.customerNameLbl.font=kSalesReportPerAgencyHeaderTitleFont;
    
    cell.customerNameLbl.text = [NSString stringWithFormat:@"%@", [agencyArray objectAtIndex:section]];
    cell.lytdLbl.text=@"";
    cell.ytdlbl.text=@"";
    cell.mtdLbl.text=@"";
    
    cell.ytdVariancePercentLbl.text=@"";
    cell.lymtdLbl.text=@"";
    cell.mtdVariancePercentage.text=@"";
    cell.m1SalesLbl.text=@"";
    cell.m2SalesLbl.text=@"";
    cell.m3SalesLbl.text=@"";
    return cell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 44;

}

- (NSArray *)indexesOfVisibleSections {
    
    NSMutableArray *visibleSections = [NSMutableArray array];
    
    for (UITableViewCell * cell in [salesPerAgencyTableView visibleCells]) {
        if (![visibleSections containsObject:[NSNumber numberWithInteger:[salesPerAgencyTableView indexPathForCell:cell].section]]) {
            [visibleSections addObject:[NSNumber numberWithInteger:[salesPerAgencyTableView indexPathForCell:cell].section]];
        }
    }
    
    return visibleSections;
}



-(UIView*)fetchHeader
{
    static NSString* identifier=@"titleHeaderCell";
    AlSeerSalesPerAgencyTitleHeaderTableViewCell *cell = [salesPerAgencyTableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesPerAgencyTitleHeaderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return cell;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"salesPerAgencyCell";
    AlSeerSalesPerAgencyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesPerAgencyTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

    }
    
    
    
    NSLog(@"frame in cell for row %@", NSStringFromCGRect(cell.frame));
    if (indexPath.section==0) {
        
    }
    
    NSMutableArray* currentAgencyArray=[filteredContentArrayGroupedByAgency objectAtIndex:indexPath.section];
    
//    cell.layer.borderColor = [UIColor colorWithRed:227.0/255.0 green:228.0/255.0 blue:230.0/255.0 alpha:1.0].CGColor;
//    cell.layer.borderWidth = 1.0;
    SalesPerAgency * salesPerAgency=[currentAgencyArray objectAtIndex:indexPath.row];
    
    if (showQuantityData==YES) {
        cell.lytdLbl.text=salesPerAgency.LY_YTD_Qty;
        cell.ytdlbl.text=salesPerAgency.CY_YTD_Qty;
        cell.mtdLbl.text=salesPerAgency.CY_MTD_Qty;
        
        double ytdVariancePercentage=[salesPerAgency.ytdQtyVariancePercentage doubleValue];
        double mtdVariancePercentage =[salesPerAgency.mtdQtyVariancePercentage doubleValue];
        
        
        
        if (ytdVariancePercentage<=0) {
            cell.ytdVariancePercentLbl.textColor=kSalesPerAgencyVarienceLessThanZeroColor;
        }
        else{
            cell.ytdVariancePercentLbl.textColor=kSalesPerAgencyVarianceGreaterThanZeroColor;
        }
        
        if (mtdVariancePercentage<=0) {
            cell.mtdVariancePercentage.textColor=kSalesPerAgencyVarienceLessThanZeroColor;
        }
        else
        {
            cell.mtdVariancePercentage.textColor=kSalesPerAgencyVarianceGreaterThanZeroColor;
        }
        
        cell.ytdVariancePercentLbl.text=salesPerAgency.ytdQtyVariancePercentage;
        cell.mtdVariancePercentage.text=salesPerAgency.mtdQtyVariancePercentage;
        cell.m1SalesLbl.text=salesPerAgency.CY_M1_Qty;
        cell.m2SalesLbl.text=salesPerAgency.CY_M2_Qty;
        cell.m3SalesLbl.text=salesPerAgency.CY_M3_Qty;
        cell.lymtdLbl.text=salesPerAgency.LY_MTD_Qty;
        cell.customerNameLbl.text=[NSString stringWithFormat:@"%@ (%@)",salesPerAgency.Customer_Name,salesPerAgency.Customer_No];
    }
    else
    {
        
        double ytdVariancePercentage=[salesPerAgency.ytdVariancePercentage doubleValue];
        double mtdVariancePercentage =[salesPerAgency.mtdVariancePercentage doubleValue];
        
        
        if (ytdVariancePercentage<=0) {
            cell.ytdVariancePercentLbl.textColor=kSalesPerAgencyVarienceLessThanZeroColor;
        }
        else{
            cell.ytdVariancePercentLbl.textColor=kSalesPerAgencyVarianceGreaterThanZeroColor;
        }
        
        if (mtdVariancePercentage<=0) {
            cell.mtdVariancePercentage.textColor=kSalesPerAgencyVarienceLessThanZeroColor;
        }
        else
        {
            cell.mtdVariancePercentage.textColor=kSalesPerAgencyVarianceGreaterThanZeroColor;
        }
        
        cell.lytdLbl.text=salesPerAgency.LY_YTD_Val;
        cell.ytdlbl.text=salesPerAgency.CY_YTD_Val;
        cell.mtdLbl.text=salesPerAgency.CY_MTD_Val;
        cell.ytdVariancePercentLbl.text=salesPerAgency.ytdVariancePercentage;
        cell.mtdVariancePercentage.text=salesPerAgency.mtdVariancePercentage;
        cell.m1SalesLbl.text=salesPerAgency.CY_M1_Val;
        cell.m2SalesLbl.text=salesPerAgency.CY_M2_Val;
        cell.m3SalesLbl.text=salesPerAgency.CY_M3_Val;
        cell.lymtdLbl.text=salesPerAgency.LY_MTD_Val;

        cell.customerNameLbl.text=[NSString stringWithFormat:@"%@ (%@)",salesPerAgency.Customer_Name,salesPerAgency.Customer_No];
    }
    
    
    
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Filter Button Methods
-(void)salesReportFilterDidReset
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    filteredContentArray=contentArray;
    showQuantityData=NO;
    [self updateReportsOnFilter];
    
    
    
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];

}


- (IBAction)filterButtontapped:(id)sender {
    
    if (agencyArray.count>0) {
        
        AlSeerSalesPerAgencyFilterViewController * popOverVC=[[AlSeerSalesPerAgencyFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.contentArray=contentArray;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 530) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        
        UIBarButtonItem *item = filterBarButtonItem ;
        
        UIView *view = [item valueForKey:@"view"];
        CGRect frame=view.frame;
        
        NSLog(@"filter framer is %@", NSStringFromCGRect(frame));
        
        [filterPopOverController presentPopoverFromRect:CGRectMake(973, frame.origin.y+40, frame.size.width, frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}


-(void)filteredSalesReport:(id)filteredContent
{
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
    filteredContentArray=filteredContent;
    [self updateReportsOnFilter];
}

-(void)filterParametersSalesReport:(id)filterParameter
{
    previousFilteredParameters=filterParameter;
    
    NSLog(@"filter parameters are %@", previousFilteredParameters);
    
    NSString *filterByParameter=[previousFilteredParameters valueForKey:@"Filter_By"];
    
    if ([NSString isEmpty:filterByParameter]==NO) {
        
        if ([filterByParameter isEqualToString:@"Quantity"]) {
            showQuantityData=YES;
        }
        else if ([filterByParameter isEqualToString:@"Value"])
        {
            showQuantityData=NO;
        }
    }
    
}

-(void)updateReportsOnFilter
{
    agencyArray = [[NSMutableArray alloc]initWithArray:[filteredContentArray valueForKeyPath:@"@distinctUnionOfObjects.Agency"]];
    /** removing invalid brandcodes*/
    [agencyArray removeObject:@""];
    filteredContentArrayGroupedByAgency=[[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        /*filtering products by brand code*/
        for (NSString *productBrandCode in agencyArray) {
            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
                                                @"Agency=%@",productBrandCode];
            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[filteredContentArray filteredArrayUsingPredicate:brandCodePredicate]];
            [filteredContentArrayGroupedByAgency addObject:tempProductsArray];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [salesPerAgencyTableView reloadData];
            [salesPerAgencyTableView setContentSize:CGSizeMake(1378,salesPerAgencyTableView.contentSize.height)];
            

        });
    });
}

@end
