//
//  AlSeerSalesPerAgencyTableViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerSalesPerAgencyTableViewCell.h"

@implementation AlSeerSalesPerAgencyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setFrame:(CGRect)frame {
    
    if (self.superview) {
        float cellWidth = 1378;
        frame.size.width = cellWidth;
    }
    
    [super setFrame:frame];
}


@end
