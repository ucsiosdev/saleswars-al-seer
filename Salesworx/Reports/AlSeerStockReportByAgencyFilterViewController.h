//
//  AlSeerStockReportByAgencyFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepButton.h"
#import "MedRepTextField.h"
#import "SalesWorxCustomClass.h"
#import "AlSeerPopupViewController.h"

#define kAgencyTitle @"Agency"
#define kProductNameTitle @"Product Name"
#define kProductCodeTitle @"Product Code"

@protocol StockReportFilterDelegate <NSObject>

-(void)filteredStockReport:(id)filteredContent;

-(void)filterParametersStockReport:(id)filterParameter;

-(void)filterDidReset;

@end


@interface AlSeerStockReportByAgencyFilterViewController : UIViewController<UITextFieldDelegate>

{
    
    IBOutlet MedRepTextField *productCodeTextField;
    IBOutlet MedRepTextField *productNameTextField;
    IBOutlet MedRepTextField *agencyTextField;
    
    IBOutlet UISegmentedControl *stockSegment;
    
    IBOutlet UISegmentedControl *blockedStockSegment;
    
    IBOutlet UISegmentedControl *qcStockSegment;
    
    id delegate;
    
    NSString* selectedPredicateString;
    NSString* selectedTextField;
    
    
    Products* filterParameterProduct;
    
    NSMutableDictionary * filterParametersDict;
}
@property(nonatomic) id delegate;


- (IBAction)stockSegmentTapped:(id)sender;
- (IBAction)blockedStockSegmentTapped:(id)sender;
- (IBAction)qcStockSegmentTapped:(id)sender;

- (IBAction)ResetButtonTapped:(id)sender;

- (IBAction)searchButtonTapped:(id)sender;
@property(strong,nonatomic) NSMutableArray * productsArray;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) Products * selectedProduct;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) Products * filteredParmeterProduct;
@end

