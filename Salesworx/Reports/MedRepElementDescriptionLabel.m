//
//  MedRepElementDescriptionLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/26/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepElementDescriptionLabel.h"
#import "SWDefaults.h"
#import "SWFoundation.h"

@implementation MedRepElementDescriptionLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib{
    [super awakeFromNib];
    self.font=kFontWeblySleekSemiBold(16);
    self.textColor= UIColorFromRGB(0x2C394A); // Black. Taken as SalesWorx > SalesWorxDescriptionLabel


}

@end
