//
//  SalesWorxVisitOptionsCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesWorxVisitOptionsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UIView *layerView;
@property (strong, nonatomic) IBOutlet UILabel *optionTitleLbl;
@end
