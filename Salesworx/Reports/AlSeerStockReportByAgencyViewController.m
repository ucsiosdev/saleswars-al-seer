//
//  AlSeerStockReportByAgencyViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerStockReportByAgencyViewController.h"
#import "MedRepUpdatedDesignCell.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SalesWorxCustomClass.h"
#import "AlSeerReportsProductStockTableViewCell.h"
@interface AlSeerStockReportByAgencyViewController ()

@end

@implementation AlSeerStockReportByAgencyViewController
@synthesize agencyTableView,productStockTableView,filterButton,startDate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Stock Report Per Agency";
    self.navigationItem.titleView=label;
    
     productStockTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, productStockTableView.bounds.size.width, 0.01f)];
    
    filterBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ReportsFilter.png"] style:UIBarButtonItemStylePlain target:self action:@selector(filterTapped)];
    productStockTableView.layer.cornerRadius = 8.0;
    productStockTableView.layer.masksToBounds = YES;
    if (@available(iOS 15.0, *)) {
        productStockTableView.sectionHeaderTopPadding = 0;
        agencyTableView.sectionHeaderTopPadding = 0;
    }
}

- (void)closeOrder {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    filterButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 24, 24)];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"ReportsFilter.png"] forState:UIControlStateNormal];
    [filterButton addTarget:self action:@selector(filterTapped) forControlEvents:UIControlEventTouchUpInside];
    filterBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:filterButton];
}


-(void)viewDidAppear:(BOOL)animated
{
    startDate=[NSDate date];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self fetchStockReports];
        
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            
            NSLog(@"Seconds --------> %f",[[NSDate date] timeIntervalSinceDate: self.startDate]);
            
            timeLbl.text=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSinceDate: self.startDate] ];
            
            if (filteredProductsArrayGroupedByBrandCode.count>0) {
                [productStockTableView reloadData];
            }
        });
    });
    
    selectedCategoryIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [filterPopOverButton setBackgroundImage:[UIImage imageNamed:@"filterEmpty.png"] forState:UIControlStateNormal];
    NSLog(@"content count is %lu",(unsigned long)productsArrayGroupedByBrandCode.count );
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fetchStockReports
{
    productsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    filteredProductsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    
    NSMutableArray* contentArray=[[[SWDatabaseManager retrieveManager]fetchProductsforAllCategories] mutableCopy];
    
    if (contentArray.count>0) {
        
        NSPredicate * stockPredicate=[NSPredicate predicateWithFormat:@"self.Blocked_Stock.intValue > 0 or self.QC_Stock.intValue > 0 or self.stock.intValue >0"];
        
        
        productsArray=[[contentArray filteredArrayUsingPredicate:stockPredicate] mutableCopy];
        NSLog(@"stock count %lu", (unsigned long)productsArray.count);
        
        
        filteredProductsArray=productsArray;
        productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Agency"]];
        [productsBrandCodesArray removeObject:@""];
        
        for (NSString *productBrandCode in productsBrandCodesArray) {
            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
                                                @"Agency=%@",productBrandCode];
            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[productsArray filteredArrayUsingPredicate:brandCodePredicate]];
            [productsArrayGroupedByBrandCode addObject:tempProductsArray];
        }
        filteredProductsArrayGroupedByBrandCode=productsArrayGroupedByBrandCode;
    }
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==productStockTableView) {
       
        return [(NSArray*)[filteredProductsArrayGroupedByBrandCode objectAtIndex:section] count];
    }
    else{
        return productsBrandCodesArray.count;

    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==agencyTableView) {
        selectedCategoryIndexPath=indexPath;
        selectedProductIndexpath=indexPath;
        [agencyTableView reloadData];
        [productStockTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView==productStockTableView) {
    return [filteredProductsArrayGroupedByBrandCode  count];
    }
    else{
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"stockCell";
    AlSeerReportsProductStockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerReportsProductStockTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.backgroundColor = [UIColor colorWithRed:(239.0/255.0) green:(243.0/255.0) blue:(246.0/255.0) alpha:1];
    
    cell.productLbl.textAlignment=NSTextAlignmentLeft;
    cell.stockLbl.textAlignment=NSTextAlignmentRight;
    cell.blockedStockLbl.textAlignment=NSTextAlignmentRight;
    cell.qualityInspectionStockLbl.textAlignment=NSTextAlignmentRight;
    

    
    cell.productLbl.textColor = kSalesReportPerAgencyHeaderTitleColor;
    cell.productLbl.font=kSalesReportPerAgencyHeaderTitleFont;
    
    cell.productLbl.text=[NSString stringWithFormat:@"%@", [productsBrandCodesArray objectAtIndex:section]];
    cell.stockLbl.text=@"";
    cell.blockedStockLbl.text=@"";
    cell.qualityInspectionStockLbl.text=@"";
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (tableView==productStockTableView) {
       
       

        static NSString* identifier=@"stockCell";
        AlSeerReportsProductStockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerReportsProductStockTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }

        NSMutableArray* currentCategoryProductsArray=[filteredProductsArrayGroupedByBrandCode objectAtIndex:indexPath.section];
        Products * currentProduct=[currentCategoryProductsArray objectAtIndex:indexPath.row];

        cell.productLbl.textAlignment=NSTextAlignmentLeft;
        cell.stockLbl.textAlignment=NSTextAlignmentRight;
        cell.blockedStockLbl.textAlignment=NSTextAlignmentRight;
        cell.qualityInspectionStockLbl.textAlignment=NSTextAlignmentRight;

        
        cell.productLbl.text=[NSString stringWithFormat:@"%@",currentProduct.Description];
        cell.stockLbl.text=[NSString stringWithFormat:@"%@",currentProduct.formattedStock];
        cell.blockedStockLbl.text=[NSString stringWithFormat:@"%@",currentProduct.Blocked_Stock];
        cell.qualityInspectionStockLbl.text=[NSString stringWithFormat:@"%@",currentProduct.QC_Stock];
        
        
        
        return cell;
        
    }
    else
    {
    
    static NSString* identifier=@"updatedDesignCell";
    MedRepUpdatedDesignCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"MedRepUpdatedDesignCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if(![selectedProductIndexpath isEqual:indexPath] || selectedProductIndexpath==nil)
    {
        cell.cellDividerImg.hidden=NO;
        cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
        cell.titleLbl.textColor=SalesWarsMenuTitleFontColor;
        cell.descLbl.textColor=SalesWarsMenuTitleDescFontColor;
        //cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewUnSelectedCellrightArrow];
        
    }
    else
    {
        cell.cellDividerImg.hidden=YES;
        cell.contentView.backgroundColor=UITableviewSelectedCellBackgroundColor;
        cell.titleLbl.textColor=[UIColor whiteColor];
        cell.descLbl.textColor=[UIColor whiteColor];
        //cell.accessoryImageView.image=[UIImage imageNamed:KUITableViewSelectedCellrightArrow];
        
    }

    
    cell.titleLbl.text=[NSString stringWithFormat:@"%@",[productsBrandCodesArray objectAtIndex:indexPath.row]];
    
    return cell;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark Filter Methods

-(IBAction)filterTapped:(id)sender
{
    if (productsBrandCodesArray.count>0) {
        
        AlSeerStockReportByAgencyFilterViewController * popOverVC=[[AlSeerStockReportByAgencyFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        popOverVC.productsArray=productsArray;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict=previousFilteredParameters;
            
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 550) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        UIBarButtonItem *item = filterBarButtonItem ;
        UIView *view = [item valueForKey:@"view"];
        
        if(view){
            CGRect frame=view.frame;
            [filterPopOverController presentPopoverFromRect:CGRectMake(977, frame.origin.y+25, frame.size.width, frame.size.height) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

#pragma mark Filter Delegate methods

-(void)filteredStockReport:(id)filteredContent
{
    NSLog(@"filtered content in report is %@", filteredContent);
 
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterFilled.png"] forState:UIControlStateNormal];
    filteredProductsArray=filteredContent;
    
    [self updateProductTableViewOnFilter];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
}


-(void)filterParametersStockReport:(id)filterParameter
{
    NSLog(@"filter parameter stock report is %@", filterParameter);
    previousFilteredParameters=filterParameter;
}

-(void)filterDidReset
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    filteredProductsArray=productsArray;
    [self updateProductTableViewOnFilter];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];
}

-(void)updateProductTableViewOnFilter
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    productsBrandCodesArray = [[NSMutableArray alloc]initWithArray:[filteredProductsArray valueForKeyPath:@"@distinctUnionOfObjects.Agency"]];
    /** removing invalid brandcodes*/
    [productsBrandCodesArray removeObject:@""];
    filteredProductsArrayGroupedByBrandCode=[[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        /*filtering products by brand code*/
        for (NSString *productBrandCode in productsBrandCodesArray) {
            NSPredicate * brandCodePredicate = [NSPredicate predicateWithFormat:
                                                @"Agency=%@",productBrandCode];
            NSMutableArray *tempProductsArray= [[NSMutableArray alloc]initWithArray:[filteredProductsArray filteredArrayUsingPredicate:brandCodePredicate]];
            [filteredProductsArrayGroupedByBrandCode addObject:tempProductsArray];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [productStockTableView reloadData];
        });
    });
}

- (BOOL) popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    if (popoverController == filterPopOverController) {
        
        return NO;
    }
    else
    {
        return YES;
    }
    
    
}


@end
