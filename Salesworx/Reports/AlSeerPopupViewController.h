//
//  AlSeerPopupViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectedFilterDelegate <NSObject>

-(void)selectedContent:(id)selectedObject;

@end

@interface AlSeerPopupViewController : UIViewController <UITableViewDataSource, UITabBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
{
    id selectedFilterDelegate;
}
@property (strong, nonatomic) IBOutlet UITableView *popUpTableView;
@property(strong,nonatomic) id selectedFilterDelegate;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;
@property(strong,nonatomic) NSMutableArray* filterDescArray;
@property(strong,nonatomic) NSMutableArray* refinedFilterDescArray;
@property(strong,nonatomic) NSString* descTitle;

@property (strong, nonatomic) UISearchController *searchController;

@end
