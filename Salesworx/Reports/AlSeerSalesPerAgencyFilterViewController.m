//
//  AlSeerSalesPerAgencyFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/22/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerSalesPerAgencyFilterViewController.h"
#import "SWDefaults.h"
#import "AlSeerPopupViewController.h"
#import "NSPredicate+Distinct.h"
#import "NSString+Additions.h"

@interface AlSeerSalesPerAgencyFilterViewController ()

@end

@implementation AlSeerSalesPerAgencyFilterViewController

@synthesize customerNametextField,customerCodeTextFieldd,chainCustomerCodeTextField,agencyTextField,filterBySegment,contentArray,previousFilterParametersDict;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Filter";
    self.navigationItem.titleView=label;
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }

    filterBySegment.selectedSegmentIndex=UISegmentedControlNoSegment;
    
    UIBarButtonItem * clearButton=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearButtontapped)];
    self.navigationItem.leftBarButtonItem=clearButton;
    
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    self.navigationItem.rightBarButtonItem=closeButton;
    

    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    NSLog(@"previous filter parameters dict is %@",previousFilterParametersDict);
    
    
    if (previousFilterParametersDict.count>0) {
        
        filterParametersDict=previousFilterParametersDict;
    }
    else
    {
        previousFilterParametersDict=[[NSMutableDictionary alloc]init];
        
    }
    
    NSString* selectedAgency=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kAgency]];
    NSString* selectedCustomer=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kCustomerNameTextField]];
    NSString* selectedCustomerCode=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kCustomerCodeTextField]];
    
    NSString* selectedChainCustomerCode=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kChainCustomerCodeTextField]];
    
    NSString* filterByStr=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"Filter_By"]];
    
    customerNametextField.enabled=YES;
    customerCodeTextFieldd.enabled=YES;
    chainCustomerCodeTextField.enabled=YES;
    
    if ([NSString isEmpty:selectedAgency]==NO) {
        agencyTextField.text=selectedAgency;
    }
    
    if ([NSString isEmpty:selectedCustomer]==NO) {
        customerNametextField.text=selectedCustomer;
        customerCodeTextFieldd.enabled=NO;
        chainCustomerCodeTextField.enabled=NO;

        
    }
    if ([NSString isEmpty:selectedCustomerCode]==NO) {
        customerCodeTextFieldd.text=selectedCustomerCode;
        customerNametextField.enabled=NO;
        chainCustomerCodeTextField.enabled=NO;

    }
    if ([NSString isEmpty:selectedChainCustomerCode]==NO) {
        chainCustomerCodeTextField.text=selectedChainCustomerCode;
        customerNametextField.enabled=NO;
        customerCodeTextFieldd.enabled=NO;
    }
    
    if ([NSString isEmpty:filterByStr]==NO) {
        
        if ([filterByStr isEqualToString:@"Quantity"]) {
            
            [filterBySegment setSelectedSegmentIndex:0];
        }
        else if ([filterByStr isEqualToString:@"Value"])
        {
            [filterBySegment setSelectedSegmentIndex:1];

        }
    }
    chainCustomerCodeTextField.text=selectedChainCustomerCode;

    
}

-(void)clearButtontapped
{
    customerNametextField.enabled=YES;
    customerCodeTextFieldd.enabled=YES;
    chainCustomerCodeTextField.enabled=YES;
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerNametextField.text=@"";
    customerCodeTextFieldd.text=@"";
    chainCustomerCodeTextField.text=@"";
    agencyTextField.text=@"";
    filterBySegment.selectedSegmentIndex=UISegmentedControlNoSegment;

}

-(void)closeButtonTapped
{
    [self.filterPopOverController dismissPopoverAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)filterBySegmentTapped:(UISegmentedControl *)sender {
    
    UISegmentedControl * currentStockSegment=sender;
    
    if (currentStockSegment.selectedSegmentIndex==0) {
        
        [filterParametersDict setValue:@"Quantity" forKey:@"Filter_By"];
    }else
    {
        [filterParametersDict setValue:@"Value" forKey:@"Filter_By"];
        
    }
    
}

-(void)selectedContent:(id)selectedObject
{
    NSLog(@"selected object is %@", selectedObject);
    
    
    if ([selectedTextField isEqualToString:kAgency]) {
        [filterParametersDict setValue:selectedObject forKey:kAgency];
        agencyTextField.text=[SWDefaults getValidStringValue:selectedObject];
        
    }
    else if ([selectedTextField isEqualToString:kCustomerNameTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:kCustomerNameTextField];
        customerNametextField.text=[SWDefaults getValidStringValue:selectedObject];
//        customerCodeTextFieldd.enabled=NO;
//        chainCustomerCodeTextField.enabled=NO;

    }
    else if ([selectedTextField isEqualToString:kCustomerCodeTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:kCustomerCodeTextField];
        customerCodeTextFieldd.text=[SWDefaults getValidStringValue:selectedObject];
//        customerNametextField.enabled=NO;
//        chainCustomerCodeTextField.enabled=NO;


    }
    else if ([selectedTextField isEqualToString:kChainCustomerCodeTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:kChainCustomerCodeTextField];
        
        chainCustomerCodeTextField.text=[SWDefaults getValidStringValue:selectedObject];
//        customerCodeTextFieldd.enabled=NO;
//        customerNametextField.enabled=NO;
    }
    else{
        customerCodeTextFieldd.enabled=YES;
        customerNametextField.enabled=YES;
        chainCustomerCodeTextField.enabled=YES;
    }
}


- (IBAction)searchButtonTapped:(id)sender {
    
    NSMutableArray * filteredProductsArray=[[NSMutableArray alloc]init];
    NSMutableArray* predicateArray=[[NSMutableArray alloc]init];
    
    
    NSString* selectedAgency=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:kAgency]];
    NSString* selectedCustomer=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:kCustomerNameTextField]];
    NSString* selectedCustomerCode=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:kCustomerCodeTextField]];
    
    NSString* selectedChainCustomerCode=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:kChainCustomerCodeTextField]];
    
    NSString* filterByStr=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Filter_By"]];
    
    
    if ([NSString isEmpty:selectedAgency]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Agency ==[cd] %@",selectedAgency]];
    }
    if ([NSString isEmpty:selectedCustomer]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Customer_Name ==[cd] %@",selectedCustomer]];
    }
    if ([NSString isEmpty:selectedCustomerCode]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Customer_No ==[cd] %@",selectedCustomerCode]];
    }
    
    if ([NSString isEmpty:selectedChainCustomerCode]==NO) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Chain_Customer ==[cd] %@ AND SELF.Chain_Customer!='' ",selectedChainCustomerCode]];
    }
    if ([NSString isEmpty:filterByStr]==NO) {
        
    }

    

    
    NSLog(@"predicate array is %@", predicateArray);
    NSLog(@"content array is %@",contentArray);
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredProductsArray=[[contentArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    NSLog(@"filtered products are %@", filteredProductsArray);
    
    if (predicateArray.count==0 && [NSString isEmpty:filterByStr]==YES) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Filter Criteria" andMessage:@"Please select filter criteria and try again" withController:self];
    }
    else  if (filteredProductsArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredSalesReport:)]) {
            
            previousFilterParametersDict=filterParametersDict;
            [self.delegate filterParametersSalesReport:filterParametersDict];

            [self.delegate filteredSalesReport:filteredProductsArray];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

- (IBAction)resetButtonTapped:(id)sender {
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    agencyTextField.text=@"";
    customerCodeTextFieldd.text=@"";
    customerNametextField.text=@"";
    chainCustomerCodeTextField.text=@"";
    
    if ([self.delegate respondsToSelector:@selector(salesReportFilterDidReset)]) {
        
        [self.delegate salesReportFilterDidReset];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
    

    
}

#pragma mark UITextField Methods
-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    
    
    
    AlSeerPopupViewController * filterDescVC=[[AlSeerPopupViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=titleText;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[contentArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    
    
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:[SWDefaults getValidStringValue:predicateString]];
    
    NSLog(@"filter desc array is %@", filterDescArray);
    //if in case agency is selected then display products for that agency only

    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==customerNametextField) {
        selectedTextField=kCustomerNameTextField;
        selectedPredicateString=kCustomerNameTextField;
        titleText=kCustomerNameTitle;
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else if (textField==customerCodeTextFieldd)
    {
        selectedTextField=kCustomerCodeTextField;
        selectedPredicateString=kCustomerCodeTextField;
        titleText=kCustomerCodeTitle;

        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else if (textField==chainCustomerCodeTextField)
    {
        selectedTextField=kChainCustomerCodeTextField;
        selectedPredicateString=kChainCustomerCodeTextField;
        titleText=kChainCustomerCodeTitle;

        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField==agencyTextField)
    {
        selectedTextField=kAgency;
        selectedPredicateString=kAgency;
        titleText=kAgencyTitle;

        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    return NO;
}


@end
