//
//  AlSeerReportsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerReportsViewController.h"
#import "SalesSummaryViewController.h"
#import "BlockedCustomerViewController.h"
#import "StatementReportViewController.h"
#import "PaymentReportViewController.h"
#import "ReviewDocumentReportsViewController.h"
#import "AlSeerStockReportByAgencyViewController.h"
#import "SalesWorxVisitOptionsCollectionViewCell.h"
#import "ReviewDocumentViewController.h"
#import "AlSeerSalesPerAgencyViewController.h"
#import "SWDefaults.h"
#import "AlSeerReportsCollectionViewCell.h"


@interface AlSeerReportsViewController ()

@end

@implementation AlSeerReportsViewController
@synthesize reportsCollectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
        // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor=UIViewControllerBackGroundColor;
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"Reports";
    titleLabel.font = headerTitleFont;
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
    
    [reportsCollectionView registerClass:[AlSeerReportsCollectionViewCell class] forCellWithReuseIdentifier:@"reportsCollectionViewCell"];
    
    reportsArray=[[NSMutableArray alloc]init];
    
    
    
    NSMutableDictionary * salesPerAgencyDict=[[NSMutableDictionary alloc]init];
    [salesPerAgencyDict setValue:kSalesReportTitle forKey:@"Title"];
    [salesPerAgencyDict setValue:kSalesReportDescription forKey:@"Description"];
    [salesPerAgencyDict setValue:[UIImage imageNamed:@"Sales_Per_Agency"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:salesPerAgencyDict];
    
    
    NSMutableDictionary * stockReportDict=[[NSMutableDictionary alloc]init];
    [stockReportDict setValue:kStockReportTitle forKey:@"Title"];
    [stockReportDict setValue:kStockReportDescription forKey:@"Description"];
    [stockReportDict setValue:[UIImage imageNamed:@"Stock_Report"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:stockReportDict];
    

    
    
    NSMutableDictionary * salesSummaryDict=[[NSMutableDictionary alloc]init];
    [salesSummaryDict setValue:kSalesSummaryTitle forKey:@"Title"];
    [salesSummaryDict setValue:kSalesSummaryDescription forKey:@"Description"];
    [salesSummaryDict setValue:[UIImage imageNamed:@"Sales_Summary"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:salesSummaryDict];
    

    
    
    
    NSMutableDictionary * reviewDocsDict=[[NSMutableDictionary alloc]init];
    [reviewDocsDict setValue:kReviewDocumentsTitle forKey:@"Title"];
    [reviewDocsDict setValue:kReviewDocumentsDescription forKey:@"Description"];
    [reviewDocsDict setValue:[UIImage imageNamed:@"Review_Document"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:reviewDocsDict];
    
    
    NSMutableDictionary * blockedCustomersDict=[[NSMutableDictionary alloc]init];
    [blockedCustomersDict setValue:kBlockedCustomersTitle forKey:@"Title"];
    [blockedCustomersDict setValue:kBlockedCustomersDescription forKey:@"Description"];
    [blockedCustomersDict setValue:[UIImage imageNamed:@"Blocked_Customer"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:blockedCustomersDict];
    
    
    
    
    
    NSMutableDictionary * customerStatementDict=[[NSMutableDictionary alloc]init];
    [customerStatementDict setValue:kCustomerStatementTitle forKey:@"Title"];
    [customerStatementDict setValue:kCustomerStatementDescription forKey:@"Description"];
    [customerStatementDict setValue:[UIImage imageNamed:@"Customer_Statement"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:customerStatementDict];
    
    
    
    NSMutableDictionary * paymentSummaryDict=[[NSMutableDictionary alloc]init];
    [paymentSummaryDict setValue:kPaymentSummaryTitle forKey:@"Title"];
    [paymentSummaryDict setValue:kPaymentSummaryDescription forKey:@"Description"];
    [paymentSummaryDict setValue:[UIImage imageNamed:@"Payment_Summarry"] forKey:@"ThumbNailImage"];
    [reportsArray addObject:paymentSummaryDict];

    
    
    
    
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
          
            
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  reportsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(300, 130);

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"reportsCollectionViewCell";
    AlSeerReportsCollectionViewCell *cell = (AlSeerReportsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.titleLbl.text=[[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Title"] uppercaseString] ;
    cell.descLbl.text=[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Description"] ;
    cell.optionImageView.image=[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"ThumbNailImage"];
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* cellTitle=[[reportsArray objectAtIndex:indexPath.row] valueForKey:@"Title"];
    
    if ([cellTitle isEqualToString:kReviewDocumentsTitle]) {
        ReviewDocumentViewController * reviewDocumentVC=[[ReviewDocumentViewController alloc]init];
        [self.navigationController pushViewController:reviewDocumentVC animated:YES];
    }
   else if ([cellTitle isEqualToString:kBlockedCustomersTitle])
   {
       BlockedCustomerViewController *blockedCustomerVC=[[BlockedCustomerViewController alloc]init];
       [self.navigationController pushViewController:blockedCustomerVC animated:YES];
   }
   else if ([cellTitle isEqualToString:kCustomerStatementTitle])
   {
       StatementReportViewController *statementVC=[[StatementReportViewController alloc]init];
       [self.navigationController pushViewController:statementVC animated:YES];
   }
   else if ([cellTitle isEqualToString:kPaymentSummaryTitle])
   {
       PaymentReportViewController *paymentVC=[[PaymentReportViewController alloc]init];
       [self.navigationController pushViewController:paymentVC animated:YES];
   }
   else if ([cellTitle isEqualToString:kSalesSummaryTitle])
   {
       SalesSummaryViewController *salesSummaryVC=[[SalesSummaryViewController alloc]init];
       [self.navigationController pushViewController:salesSummaryVC animated:YES];
   }
   else if ([cellTitle isEqualToString:kStockReportTitle])
   {
       AlSeerStockReportByAgencyViewController *stockReportVC=[[AlSeerStockReportByAgencyViewController alloc]init];
       [self.navigationController pushViewController:stockReportVC animated:YES];
   }
   else if ([cellTitle isEqualToString:kSalesReportTitle])
   {
       AlSeerSalesPerAgencyViewController *salesVC=[[AlSeerSalesPerAgencyViewController alloc]init];
       [self.navigationController pushViewController:salesVC animated:YES];
   }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
