//
//  AlSeerSalesPerAgencyTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface AlSeerSalesPerAgencyTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *lytdLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ytdlbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *mtdLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *ytdVariancePercentLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *lymtdLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *mtdVariancePercentage;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *m1SalesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *m2SalesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *m3SalesLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *customerNameLbl;

@end
