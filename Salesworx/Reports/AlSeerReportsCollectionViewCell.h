//
//  AlSeerReportsCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/25/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWDefaults.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"


@interface AlSeerReportsCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UITextView *descTxtView;
@property (strong, nonatomic) IBOutlet UIImageView *optionImageView;
@property (strong, nonatomic) IBOutlet UIView *layerView;
@property(strong,nonatomic) IBOutlet MedRepElementTitleLabel * titleLbl;
@property(strong,nonatomic) IBOutlet MedRepElementDescriptionLabel * descLbl;

@end
