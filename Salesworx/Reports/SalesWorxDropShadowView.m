//
//  SalesWorxDropShadowView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxDropShadowView.h"
#import "SWFoundation.h"

@implementation SalesWorxDropShadowView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    
    [super awakeFromNib];
    
    // drop shadow
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;
    self.layer.masksToBounds = NO;
    
    //round corner
    [self.layer setCornerRadius:8.0f];
    
    
}

@end
