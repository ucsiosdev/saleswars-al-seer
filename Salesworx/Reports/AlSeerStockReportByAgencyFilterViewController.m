//
//  AlSeerStockReportByAgencyFilterViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerStockReportByAgencyFilterViewController.h"
#import "SWFoundation.h"
#import "NSPredicate+Distinct.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"

@interface AlSeerStockReportByAgencyFilterViewController ()

@end

@implementation AlSeerStockReportByAgencyFilterViewController
@synthesize productsArray,filteredParmeterProduct,previousFilterParametersDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Filter";
    self.navigationItem.titleView=label;
    

    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor = UIColorFromRGB(0x169C5C);
        self.navigationController.toolbar.translucent = NO;
        
    }

    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
    filterParametersDict=[[NSMutableDictionary alloc]init];

    
    if (previousFilterParametersDict.count>0) {
        
        NSLog(@"previosu filter parameters are %@", previousFilterParametersDict);
        
        filterParametersDict=previousFilterParametersDict;
        
        NSString* filterParameterAgency=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"Agency"]];
        NSString* filterParameterProductName=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"Description"]];
        NSString* filterParameterProductCode=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"Item_Code"]];
        NSString* stockStr=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"stock"]];
        NSString* blockedStockStr=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"Blocked_Stock"]];
        NSString* qcStockStr=[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:@"QC_Stock"]];
        
        if ([NSString isEmpty:filterParameterAgency]==NO) {
            agencyTextField.text=[NSString stringWithFormat:@"%@",filterParameterAgency];
            
        }
        if ([NSString isEmpty:filterParameterProductName]==NO) {
            productNameTextField.text=[NSString stringWithFormat:@"%@",filterParameterProductName];
            
            

            
        }
        if ([NSString isEmpty:filterParameterProductCode]==NO) {
            productCodeTextField.text=[NSString stringWithFormat:@"%@",filterParameterProductCode];
            
        }
        if ([NSString isEmpty:stockStr]==NO) {
            
            if ([stockStr isEqualToString:@"NO"]) {
                [stockSegment setSelectedSegmentIndex:1];
            }
            else if ([stockStr isEqualToString:@"YES"]) {
                [stockSegment setSelectedSegmentIndex:0];
            }
            else{
                [stockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
            }
        }
        else{
            [stockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];

        }
        if ([NSString isEmpty:blockedStockStr]==NO) {
            
            if ([blockedStockStr isEqualToString:@"NO"]) {
                [blockedStockSegment setSelectedSegmentIndex:1];
            }
           else if ([blockedStockStr isEqualToString:@"YES"]) {
                [blockedStockSegment setSelectedSegmentIndex:0];
            }
            else{
                [blockedStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
            }
        }
        else{
            [blockedStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];

        }
        if ([NSString isEmpty:qcStockStr]==NO) {
            
            if ([qcStockStr isEqualToString:@"NO"]) {
                [qcStockSegment setSelectedSegmentIndex:1];
            }
            else if ([qcStockStr isEqualToString:@"YES"])
            {
                [qcStockSegment setSelectedSegmentIndex:0];
            }
            else{
                [qcStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
            }
        }
        else{
            [qcStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];

        }
    }
    else{
        [stockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
        [blockedStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
        [qcStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    }
    
    
    
    

    
    
    UIBarButtonItem * clearButton=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearButtontapped)];
    self.navigationItem.leftBarButtonItem=clearButton;

    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    self.navigationItem.rightBarButtonItem=closeButton;
    

    
    
    
   
    
    // Do any additional setup after loading the view from its nib.
}

-(void)clearButtontapped
{
    filterParametersDict=[[NSMutableDictionary alloc]init];
    selectedTextField=@"";
    agencyTextField.text=@"";
    productNameTextField.text=@"";
    productCodeTextField.text=@"";
    [stockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [blockedStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [qcStockSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    
    
}
-(void)closeButtonTapped
{
    [self.filterPopOverController dismissPopoverAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)selectedContent:(id)selectedObject
{
    NSLog(@"selected object is %@", selectedObject);
    
    
    if ([selectedTextField isEqualToString:kAgencyTitle]) {
        filteredParmeterProduct.Agency=selectedObject;
        [filterParametersDict setValue:selectedObject forKey:@"Agency"];
        agencyTextField.text=[SWDefaults getValidStringValue:selectedObject];
        
    }
    
   
    else if ([selectedTextField isEqualToString:kProductNameTitle])
    {
        filteredParmeterProduct.Description=selectedObject;
        [filterParametersDict setValue:selectedObject forKey:@"Description"];

        productNameTextField.text=[SWDefaults getValidStringValue:selectedObject];
        
    }
    else if ([selectedTextField isEqualToString:kProductCodeTitle])
    {
        filteredParmeterProduct.Item_Code=selectedObject;
        [filterParametersDict setValue:selectedObject forKey:@"Item_Code"];

        productCodeTextField.text=[SWDefaults getValidStringValue:selectedObject];
        
    }
    
    
}


-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    
    

    AlSeerPopupViewController * filterDescVC=[[AlSeerPopupViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray* filterDescArray=[[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray=[[productsArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
        //now sort by asc
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    
    
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:[SWDefaults getValidStringValue:predicateString]];
    
    
    //if in case agency is selected then display products for that agency only
    
    NSString* agencyString=[SWDefaults getValidStringValue:filteredParmeterProduct.Agency];
    
    
    
//    if ([NSString isEmpty:agencyString]==NO) {
//        NSPredicate *agencyPredicate=[NSPredicate predicateWithFormat:@"SELF.Agency == [cd] %@",agencyString];
//         filterDescArray=[[sortedArray filteredArrayUsingPredicate:agencyPredicate] mutableCopy];
//        filterDescArray =[filterDescArray valueForKey:[SWDefaults getValidStringValue:predicateString]];
//    
//    }
    
    
    
    

    
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==agencyTextField) {
        selectedTextField=kAgencyTitle;
        selectedPredicateString=@"Agency";
        filteredParmeterProduct.Agency=@"";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else if (textField==productNameTextField)
    {
        selectedTextField=kProductNameTitle;
        selectedPredicateString=@"Description";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    else if (textField==productCodeTextField)
    {
        selectedTextField=kProductCodeTitle;
        selectedPredicateString=@"Item_Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    return NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)stockSegmentTapped:(id)sender {
    
    UISegmentedControl * currentStockSegment=sender;
    
    if (currentStockSegment.selectedSegmentIndex==0) {
        
        [filterParametersDict setValue:@"YES" forKey:@"stock"];
    }else
    {
        [filterParametersDict setValue:@"NO" forKey:@"stock"];

    }
    
}

- (IBAction)blockedStockSegmentTapped:(id)sender {
    
    UISegmentedControl * currentStockSegment=sender;
    
    if (currentStockSegment.selectedSegmentIndex==0) {
        
        [filterParametersDict setValue:@"YES" forKey:@"Blocked_Stock"];
    }else
    {
        [filterParametersDict setValue:@"NO" forKey:@"Blocked_Stock"];
        
    }
}

- (IBAction)qcStockSegmentTapped:(id)sender {
    
    UISegmentedControl * currentStockSegment=sender;
    
    if (currentStockSegment.selectedSegmentIndex==0) {
        
        [filterParametersDict setValue:@"YES" forKey:@"QC_Stock"];
    }else
    {
        [filterParametersDict setValue:@"NO" forKey:@"QC_Stock"];
        
    }
}

- (IBAction)ResetButtonTapped:(id)sender {
    
    filteredParmeterProduct=[[Products alloc]init];
    filterParametersDict=[[NSMutableDictionary alloc]init];
    
    agencyTextField.text=@"";
    productNameTextField.text=@"";
    productCodeTextField.text=@"";
    
    if ([self.delegate respondsToSelector:@selector(filterDidReset)]) {
        
        [self.delegate filterDidReset];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];

}

- (IBAction)searchButtonTapped:(id)sender {
    
    NSMutableArray * filteredProductsArray=[[NSMutableArray alloc]init];
    NSMutableArray* predicateArray=[[NSMutableArray alloc]init];
    
    
    NSString* selectedAgency=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Agency"]];
    NSString* selectedproduct=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Description"]];
    NSString* selectedBrand=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Item_Code"]];
                             
    NSString* stockStr=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"stock"]];
    NSString* blockedStockStr=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Blocked_Stock"]];
    NSString* qcStockStr=[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"QC_Stock"]];
    
    
    if (selectedAgency.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Agency ==[cd] %@",selectedAgency]];
    }
    if (selectedproduct.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Description ==[cd] %@",selectedproduct]];
    }
    if (selectedBrand.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",selectedBrand]];
    }
    if ([NSString isEmpty:stockStr]==NO) {
        
        if ([stockStr isEqualToString:@"YES"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.stock.intValue > 0"]];
  
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.stock.intValue == 0"]];

        }
        
    }
    
    if ([NSString isEmpty:blockedStockStr]==NO) {
        
        if ([blockedStockStr isEqualToString:@"YES"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Blocked_Stock.intValue > 0"]];
 
        }
        else{
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Blocked_Stock.intValue == 0"]];

        }
    }
    
    if ([NSString isEmpty:qcStockStr]==NO) {
        
        if ([qcStockStr isEqualToString:@"YES"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.QC_Stock.intValue > 0"]];
        }
        else
        {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.QC_Stock.intValue == 0"]];
        }
        
    }
    NSLog(@"predicate array is %@", predicateArray);
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredProductsArray=[[productsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];

    NSLog(@"filtered products are %@", filteredProductsArray);
    
    if (predicateArray.count==0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Filter Criteria" andMessage:@"Please select filter criteria and try again" withController:self];
    }
    else if (filteredProductsArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredStockReport:)]) {
            
            previousFilterParametersDict=filterParametersDict;
            [self.delegate filteredStockReport:filteredProductsArray];
            [self.delegate filterParametersStockReport:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

@end
