//
//  MedRepButton.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepButton.h"
#import <QuartzCore/QuartzCore.h>
#import "SWDefaults.h"

@implementation MedRepButton

-(void)awakeFromNib
{
    [super awakeFromNib];
    if(self.tag==1881)
    {
        
    }
    else if (self.ShadowColor==NO) {
        [self.titleLabel setFont:FilterButtonFont];
        [self.titleLabel setTextColor:[UIColor redColor]];
        
        
        self.layer.borderWidth=1.0f;
        self.layer.borderColor=[kFilterButtonBorderColor CGColor];
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.1f;
        self.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        self.layer.shadowRadius = 1.0f;
    }
    else
    {
        NSLog(@"shadow color on");
        
        [self.titleLabel setFont:FilterButtonFont];
        [self.titleLabel setTextColor:[UIColor whiteColor]];
        self.backgroundColor=[UIColor colorWithRed:(87.0/255.0) green:(190.0/255.0) blue:(135.0/255.0) alpha:1];
        self.layer.cornerRadius=5.0f;
        
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height, self.frame.size.width, 4.0f);
        bottomBorder.cornerRadius=5.0f;
        bottomBorder.backgroundColor = [UIColor yellowColor].CGColor;
        self.layer.shadowColor = [[UIColor colorWithRed:(13.0/255.0) green:(136.0/255.0) blue:(96.0/255.0) alpha:1] CGColor];
        
        if (self.frame.size.height==40) {
            self.layer.shadowOffset = CGSizeMake(0.0, 3.0);
        }
        else
        {
            self.layer.shadowOffset = CGSizeMake(0.0, 3.0);
        }
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowRadius = 0.0;
    }
}

@end
