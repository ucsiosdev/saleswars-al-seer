//
//  AlSeerReportsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWViewController.h"

@interface AlSeerReportsViewController : SWViewController
{
    NSMutableArray * reportsArray;
}
@property (strong, nonatomic) IBOutlet UICollectionView *reportsCollectionView;

@end
