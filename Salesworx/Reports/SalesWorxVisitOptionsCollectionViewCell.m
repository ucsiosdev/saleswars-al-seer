//
//  SalesWorxVisitOptionsCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/30/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxVisitOptionsCollectionViewCell.h"
#import "SWDefaults.h"
@implementation SalesWorxVisitOptionsCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"SalesWorxVisitOptionsCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        }
    
    return self;
    
}

- (void)awakeFromNib {
    // Shadow and Radius
    [super awakeFromNib];
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowOpacity = 0.1;
    self.layer.shadowRadius = 1.0;
    self.layer.masksToBounds = NO;
    
    //round corner
    [self.layer setCornerRadius:8.0f];
}
@end
