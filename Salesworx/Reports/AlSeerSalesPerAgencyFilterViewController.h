//
//  AlSeerSalesPerAgencyFilterViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/22/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "SalesWorxCustomClass.h"
#import "AlSeerPopupViewController.h"
#define kCustomerNameTextField @"Customer_Name"
#define kCustomerCodeTextField @"Customer_No"
#define kChainCustomerCodeTextField @"Chain_Customer"
#define kAgency @"Agency"


#define kCustomerNameTitle @"Customer Name"
#define kCustomerCodeTitle @"Customer No"
#define kChainCustomerCodeTitle @"Chain Customer"
#define kAgencyTitle @"Agency"



@protocol SalesReportFilterDelegate <NSObject>

-(void)filteredSalesReport:(id)filteredContent;

-(void)filterParametersSalesReport:(id)filterParameter;

-(void)salesReportFilterDidReset;

@end



@interface AlSeerSalesPerAgencyFilterViewController : UIViewController<SelectedFilterDelegate>

{
    id delegate;
    NSString* selectedTextField;
    NSString* selectedPredicateString;
    NSMutableDictionary *filterParametersDict;
    NSString* titleText;
    
}
@property(nonatomic) id delegate;


@property (strong, nonatomic) IBOutlet MedRepTextField *customerNametextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *customerCodeTextFieldd;
@property (strong, nonatomic) IBOutlet MedRepTextField *chainCustomerCodeTextField;

@property (strong, nonatomic) IBOutlet UISegmentedControl *filterBySegment;
- (IBAction)filterBySegmentTapped:(UISegmentedControl *)sender;
- (IBAction)searchButtonTapped:(id)sender;

- (IBAction)resetButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet MedRepTextField *agencyTextField;


@property(strong,nonatomic) NSMutableArray * contentArray;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) SalesPerAgency * salesPerAgency;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;


@end
