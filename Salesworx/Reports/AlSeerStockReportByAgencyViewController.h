//
//  AlSeerStockReportByAgencyViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDropShadowView.h"
#define kSpaceString @"  "
#import "AlSeerStockReportByAgencyFilterViewController.h"
@interface AlSeerStockReportByAgencyViewController : UIViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    NSIndexPath * selectedProductIndexpath;
    
    NSMutableArray *productsArrayGroupedByBrandCode;
    IBOutlet UILabel *timeLbl;
    NSMutableArray * filteredProductsArrayGroupedByBrandCode;
    NSMutableArray * filteredProductsArray;
    NSMutableArray * productsArray;
    NSMutableArray* productsBrandCodesArray;
    
     UIButton *filterButton;
    NSIndexPath* selectedCategoryIndexPath;
    
    NSMutableDictionary * previousFilteredParameters;
    
    UIPopoverController *filterPopOverController;
    
    Products * filterParamaterProduct;
    
    UIBarButtonItem * filterBarButtonItem;
    
    UIButton * filterPopOverButton;
    
    
    
    
}
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet UICollectionView *reportsCollectionView;
@property (strong, nonatomic) IBOutlet UITableView *agencyTableView;
@property (strong, nonatomic) IBOutlet UIButton *filterButton;
- (IBAction)filterButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *productStockTableView;

@property (nonatomic, strong) NSDate *startDate;

- (IBAction)filterTapped:(id)sender;
@end
