//
//  AlSeerPopupViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "AlSeerPopupViewController.h"
#import "SWDefaults.h"
#import "SalesWorxCustomClass.h"
@interface AlSeerPopupViewController ()

@end



@implementation AlSeerPopupViewController
@synthesize filterDescArray, refinedFilterDescArray, descTitle, popUpTableView, selectedFilterDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:  [UIFont fontWithName:@"OpenSans" size:16], NSFontAttributeName, nil];
    CGSize requestedTitleSize = [self.descTitle sizeWithAttributes:textTitleOptions];
    CGFloat titleWidth = MIN(self.view.frame.size.width, requestedTitleSize.width);
    CGRect frame = CGRectMake(0, 0, titleWidth, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = self.descTitle;
    self.navigationItem.titleView = label;

  
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) { /// iOS 7 or above
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    refinedFilterDescArray = [filterDescArray mutableCopy];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate=self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.popUpTableView.tableHeaderView = self.searchController.searchBar;
    
    [popUpTableView reloadData];
    self.definesPresentationContext = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;

    
    popUpTableView.estimatedRowHeight = 44.0;
    popUpTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableView DataSource Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return refinedFilterDescArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.font=headerFont;
    cell.textLabel.textColor=fontColor;
    
    cell.textLabel.text=[SWDefaults getValidStringValue:[refinedFilterDescArray objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    
    Products *selectedProduct = [refinedFilterDescArray objectAtIndex:indexPath.row];
    
    tableView.userInteractionEnabled=NO;
    self.searchController.active=NO;
    if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedContent:)]) {
        
        [self.selectedFilterDelegate selectedContent:selectedProduct];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text scope:@""];
}

- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;
    refinedFilterDescArray = [filterDescArray mutableCopy];
    [popUpTableView reloadData];
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if(searchText.length == 0)
    {
        refinedFilterDescArray = [filterDescArray mutableCopy];
    } else {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    [popUpTableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
