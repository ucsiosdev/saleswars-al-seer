//
//  SalesWarsCustomClass.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWarsCustomClass : NSObject

@end

@interface PriceList : NSObject
@property(strong,nonatomic) NSString *Price_List_ID,*Inventory_Item_ID,*Organization_ID,*Item_UOM,*Unit_Selling_Price,*Unit_List_Price,*Is_Generic;
@end

@interface Stock : NSObject
@property(strong,nonatomic) NSString *Org_ID,*Lot_Qty,*Lot_No,*Expiry_Date,*Stock_ID,*On_Order_Qty,*warehouse,*lotNumberWithExpiryDate;
@end

@interface ProductMediaFile : NSObject

@property(strong,nonatomic) NSString *Media_File_ID,*Entity_ID_1,*Entity_ID_2,*Entity_Type,*Media_Type,*Filename,*Caption,*Thumbnail,*Is_Deleted,*Download_Flag,*Custom_Attribute_1,*Custom_Attribute_2,*Custom_Attribute_3,*Custom_Attribute_4,*File_Path;


@end


@interface Products  : NSObject
@property(strong,nonatomic) NSString * Agency;
@property(strong,nonatomic) NSString*Inventory_Item_ID;
@property(strong,nonatomic) NSString * Brand_Code;
@property(strong,nonatomic) NSString * Category;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * IsMSL;
@property(strong,nonatomic) NSString * ItemID;
@property(strong,nonatomic) NSString * Lot_Qty;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * OrgID;
@property(strong,nonatomic) NSString * Sts;
@property(strong,nonatomic) NSString * Discount;
@property(strong,nonatomic) NSString * selectedUOM;
@property(nonatomic) NSInteger  conversionValue;
@property(strong,nonatomic) NSMutableArray* productStockArray;
@property(strong,nonatomic) NSMutableArray* productBonusArray;
@property(strong,nonatomic) NSMutableArray* productPriceListArray;
@property(nonatomic,strong) NSString* stock;
@property(nonatomic,strong) NSString* bonus;
@property(strong,nonatomic) NSMutableArray* productImagesArray;
@property(strong,nonatomic) ProductMediaFile* productMediaFile;
@property(strong,nonatomic) Stock *productStock;
@property(strong,nonatomic)PriceList *productPriceList;
@property(strong,nonatomic)NSString *nearestExpiryDate;
@property(nonatomic)BOOL isSellingPriceFieldEditable;
@property(nonatomic,strong) NSString* specialDicount;
@property(nonatomic,strong) NSString* Blocked_Stock;
@property(nonatomic,strong) NSString* QC_Stock;
@property(nonatomic,strong) NSString* formattedStock;


@end



@interface ProductCategories : NSObject

@property(strong,nonatomic)NSString* Category;

@property(strong,nonatomic)NSString* Description;

@property(strong,nonatomic)NSString* Org_ID;

@property(strong,nonatomic)NSString* Item_No;

@property(strong,nonatomic) NSString * isSelected;
@end

@interface SalesPerAgency : NSObject
@property(strong,nonatomic)NSString* Customer_No;
@property(strong,nonatomic)NSString* Customer_Name;
@property(strong,nonatomic)NSString* Chain_Customer;
@property(strong,nonatomic)NSString* Agency;
@property(strong,nonatomic)NSString* Item_Code;
@property(strong,nonatomic)NSString* LY_YTD_Qty;
@property(strong,nonatomic)NSString* LY_YTD_Val;
@property(strong,nonatomic)NSString* CY_YTD_Qty;
@property(strong,nonatomic)NSString* CY_YTD_Val;
@property(strong,nonatomic)NSString* LY_MTD_Qty;
@property(strong,nonatomic)NSString* LY_MTD_Val;
@property(strong,nonatomic)NSString* CY_MTD_Qty;
@property(strong,nonatomic)NSString* CY_MTD_Val;
@property(strong,nonatomic)NSString* CY_M1_Qty;
@property(strong,nonatomic)NSString* CY_M1_Val;
@property(strong,nonatomic)NSString* CY_M2_Qty;
@property(strong,nonatomic)NSString* CY_M2_Val;
@property(strong,nonatomic)NSString* CY_M3_Qty;
@property(strong,nonatomic)NSString* CY_M3_Val;
@property(strong,nonatomic)NSString* ytdVariancePercentage;
@property(strong,nonatomic)NSString* mtdVariancePercentage;

@property(strong,nonatomic)NSString* ytdQtyVariancePercentage;
@property(strong,nonatomic)NSString* mtdQtyVariancePercentage;

@property(strong,nonatomic) NSString* Chain_Customer_Code;


@end


