//
//  MedRepTextField.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/25/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface MedRepTextField : UITextField<UITextFieldDelegate>
{
    
}
@property (strong,nonatomic) UIImageView *dropdownImageView;
//@property (nonatomic,setter=setIsDropDown:) IBInspectable BOOL isDropDown;
@property (nonatomic,strong,setter=setDropDownImage:) IBInspectable NSString *DropDownImage;
@property (nonatomic,setter=setIsReadOnly:) IBInspectable BOOL isReadOnly;
@property (nonatomic,setter=setIsReadOnly:) IBInspectable BOOL isMandatory;
@property (strong,nonatomic) CALayer *readOnlyLayer;
-(void)showTextfieldAsLabel;

@end
