//
//  AlSeerProductDetailViewController_2.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerProductDetailViewController_2.h"
#import "SWDatabaseManager.h"
#import "AlSeerProductStockViewTableViewCell.h"
#import "AlSeerProductStockViewHeaderTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "FMDBHelper.h"
#import "DataType.h"
#import "AlSeerProductStockHeaderTableViewCell.h"
#import "AlSeerSalesOrderProductStockTableViewCell.h"
#import "FullScreenProductDetailsViewController.h"
#import "TblCellProductDetails.h"

@interface AlSeerProductDetailViewController_2 ()

@end

@implementation AlSeerProductDetailViewController_2

@synthesize productDict,itemDescLbl,itemNumberLbl,brandLbl,productImagesScrollView,priceDict,baseUOMLbl,totalStockLbl,productPageControl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupView];
    [self setNavigBar];
    
    
    allMediaFilesArray=[[NSMutableArray alloc]init];
    imagesArray=[[NSMutableArray alloc]init];
    
    
    [self fetchMediaFilesFromDocuments];
    [self seperateMediaFilesIntoArrays];

    NSInteger numberOfPages = imagesArray.count;

    productPageControl.numberOfPages=imagesArray.count;
    productPageControl.currentPage=0;
    
    
    productImagesScrollView.pagingEnabled = YES;
    productImagesScrollView.backgroundColor=[UIColor whiteColor];
    productImagesScrollView.contentSize = CGSizeMake(numberOfPages * productImagesScrollView.frame.size.width, productImagesScrollView.frame.size.height);
    productImagesScrollView.delegate=self;
    
    
    for (int i = 0; i < numberOfPages; i++) {
        UIImageView *tmpImg = [[UIImageView alloc] initWithFrame:CGRectMake(i * productImagesScrollView.frame.size.width,
                                                                      0,
                                                                      productImagesScrollView.frame.size.width,
                                                                       productImagesScrollView.frame.size.height)];
        tmpImg.contentMode=UIViewContentModeScaleAspectFit;
        
        
        if (imagesArray.count>0) {
            
        
        
            MediaFile* mediaFile=[imagesArray objectAtIndex:i];
            // Get dir
            NSString *documentsDirectory = nil;
            
            
            
            //iOS 8 support
            
            
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            else
            {
                
                
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            
            NSString* imagePath=[documentsDirectory stringByAppendingPathComponent:mediaFile.Filename];
            
            NSLog(@"image path is %@", imagePath);
            
            
        tmpImg.image = [UIImage imageWithContentsOfFile:imagePath];
        [productImagesScrollView addSubview:tmpImg];
        }else {
            UIImageView *tmpImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                                0,
                                                                                productImagesScrollView.frame.size.width ,
                                                                                 productImagesScrollView.frame.size.height)];
            tmpImg.contentMode=UIViewContentModeScaleAspectFit;
            tmpImg.image = [UIImage imageNamed:@"product-default"];
            [productImagesScrollView addSubview:tmpImg];
            
        }

    }
    
    if (numberOfPages == 0)  {
        UIImageView *tmpImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,
                                                                            0,
                                                                            productImagesScrollView.frame.size.width,
                                                                            productImagesScrollView.frame.size.height)];
        tmpImg.contentMode=UIViewContentModeScaleAspectFit;
        tmpImg.image = [UIImage imageNamed:@"product-default"];
        [productImagesScrollView addSubview:tmpImg];
        
    }


    
    //fetching product stock data
    
    
    NSString* productStockQry=[NSString stringWithFormat:@"SELECT  IFNULL(Lot_Qty,0) AS Lot_Qty ,IFNULL(Lot_No,0)AS Lot_No, IFNULL(Expiry_Date,0) AS Expiry_Date ,IFNULL(Custom_Attribute_1,0)AS QC,  IFNULL(Custom_Attribute_2,0)AS Blocked FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY  Expiry_Date ASC",[productDict stringForKey:@"ItemID"]];
    

    
//    NSString* productStockQry=[NSString stringWithFormat:@"SELECT  IFNULL(Lot_Qty,0) AS Lot_Qty ,IFNULL(Lot_No,0)AS Lot_No, IFNULL(Expiry_Date,0) AS Expiry_Date  FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY  Expiry_Date ASC",[productDict valueForKey:@"ItemID"]];
    NSLog(@"product stock qry is %@", productStockQry);
     productStockArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:productStockQry];
    if (productStockArray.count>0) {
        NSLog(@"product stock array is %@", [productStockArray description]);
       
    }
    
    
    
    
    priceDict=[[SWDatabaseManager retrieveManager] dbdbGetPriceListProduct:[productDict valueForKey:@"ItemID"]];
    
    NSLog(@"price dict data %@", [priceDict description]);
    
    if (priceDict.count>0) {
        
       NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

        self.unitPriceLbl.text=[NSString stringWithFormat:@"%@ %@", userCurrencyCode,[priceDict valueForKey:@"sellingPrice"]];
        
    }
    
    
    
    NSLog(@"check product dict in product details screen %@",[productDict description] );
    
    
    
    //fetch agency
    
    NSString* agencyQry=[NSString stringWithFormat:@"select IFNULL(Agency,0) AS Agency from TBL_Product where Inventory_Item_ID='%@' and Item_Code='%@' ", [productDict valueForKey:@"Inventory_Item_ID"],[productDict valueForKey:@"Item_Code"]];
    NSLog(@"query for fetching agency %@", agencyQry);
    NSMutableArray* agencyArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:agencyQry];
    NSLog(@"agencyArray details %@", [agencyArray description]);
    if (agencyArray.count>0) {
        self.agencyLbl.text=[NSString stringWithFormat:@"%@",[[agencyArray valueForKey:@"Agency"] objectAtIndex:0]];
    }
    
    
    
    //fetch sum
    NSString* sumofLotsQty=[NSString stringWithFormat:@"SELECT IFNULL(Sum(Lot_Qty),0) AS Total from TBL_Product_Stock WHERE Item_ID='%@' ",[productDict valueForKey:@"ItemID"]];
    NSLog(@"product stock qry is %@", sumofLotsQty);
  NSMutableArray*  sumArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:sumofLotsQty];
    
    if (sumArray.count>0) {
        NSLog(@"sumArray array is %@", [sumArray description]);
        totalStock=[NSString stringWithFormat:@"%@",[[sumArray valueForKey:@"Total"] objectAtIndex:0]];
    }
    
    
    doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapTwice:)];
    doubleTapGesture.numberOfTapsRequired = 1;
    [productImagesScrollView addGestureRecognizer:doubleTapGesture];
    
    
    // Do any additional setup after loading the view from its nib.

    if (@available(iOS 15.0, *)) {
        self.stockTableVIew.sectionHeaderTopPadding = 0;
    }
}

-(void)setNavigBar{
 
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [back setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = back;
}

- (void)handleBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
     self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.stockTableVIew.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.stockTableVIew.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.stockTableVIew.layer.mask = maskLayer;
    
}
-(void)setupView{
    
   
    self.viewAlignToX.hidden = true;
    self.viewParentScroolView.backgroundColor = [UIColor whiteColor];
    self.viewParentProductDetails.backgroundColor = [UIColor whiteColor];
    self.viewParentTbl.backgroundColor = [UIColor whiteColor];
    
    
   // self.stockTableVIew.layer.cornerRadius = 8.0;
   // self.stockTableVIew.layer.masksToBounds = YES;
    
    
    
    //self.viewParentScroolView.layer.cornerRadius = 8.0;
   // self.viewParentScroolView.layer.masksToBounds = YES;
    
    self.productImagesScrollView.layer.cornerRadius = 6.0;
    self.productImagesScrollView.layer.masksToBounds = YES;
    
   // self.viewParentProductDetails.layer.cornerRadius = 8.0;
   // self.viewParentProductDetails.layer.masksToBounds = YES;
    
   // self.viewParentTbl.layer.cornerRadius = 8.0;
    //self.viewParentTbl.layer.masksToBounds = YES;
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView==fullScreenproductImagesScrollView) {
        CGFloat pageWidth = fullScreenproductImagesScrollView.frame.size.width;
        float fractionalPage = fullScreenproductImagesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        fullScreenProductImagesPageControl.currentPage = page;

    }
    
    else
    {
    
    CGFloat pageWidth = productImagesScrollView.frame.size.width;
    float fractionalPage = productImagesScrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    productPageControl.currentPage = page;
    }
}


- (void)tapOnce:(id)tapOnce {
//    [UIView transitionWithView:self.view
//                      duration:0.7
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^ {
//                        [fullscreenProductImageView removeFromSuperview];
//                    }
//                    completion:nil];
    
    
    
    
//    [UIView animateWithDuration:0.7
//                          delay:0.0
//                        options: UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         fullscreenProductImageView.alpha = 0;
//                     }completion:^(BOOL finished){
//                         [fullscreenProductImageView removeFromSuperview];
//                     }];
    
    
//    [[self.view.subviews lastObject]removeFromSuperview];
//    
//    
//    UIView *topMost = [[self.view subviews] firstObject];
//
//    
//    [topMost removeFromSuperview];
    
    
  
   // [bgView removeFromSuperview];

    [fullScreenproductImagesScrollView removeFromSuperview];
    [fullScreenProductImagesPageControl removeFromSuperview];
    
//    [UIView animateWithDuration:0.7
//                          delay:0.0
//                        options: UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         fullScreenproductImagesScrollView.alpha = 0;
//                         fullScreenProductImagesPageControl.alpha=0;
//                         bgView.alpha=0;
//                     }completion:^(BOOL finished){
//                         
//                         
//
//                         [fullScreenproductImagesScrollView removeFromSuperview];
//                         [fullScreenProductImagesPageControl removeFromSuperview];
//                         [bgView removeFromSuperview];
//                     }];
}
- (void)tapTwice:(id)tapTwice {
    
    
    NSLog(@"tapped twice");
    
    if (imagesArray == nil || imagesArray.count == 0){
     
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Alert", nil) andMessage:@"No Media data found." withController:self];
        return;
    }
    
    FullScreenProductDetailsViewController * fullScreenVC= [[FullScreenProductDetailsViewController alloc]init];
    fullScreenVC.imagesArray=imagesArray;
    [self presentViewController:fullScreenVC animated:YES completion:nil];
}
-(void)collapseImages {
  
}

-(void)viewWillAppear:(BOOL)animated
{
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    [self.navigationController setToolbarHidden:YES animated:YES];
    brandLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Brand_Code"]];
    itemNumberLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Item_Code"]];
    itemDescLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Description"]];
    
    NSLog(@"product dict is %@", productDict);
    
    NSString* barcodeStr=[NSString stringWithFormat:@"%@",[productDict valueForKey:@"BarCode"]];
    
    if ([NSString isEmpty:barcodeStr]==NO) {
        barcodeLbl.text=[SWDefaults getValidStringValue:barcodeStr];
    }
    else{
        barcodeLbl.text=@"N/A";
    }
    
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

    
    self.navigationController.navigationBar.translucent = NO;
    
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:kFontWeblySleekSemiBold(19), NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    self.title=@"Product Details";
    
    
    //fetch base UOM
    //fetch agency
    
    NSString* baseUOMQry=[NSString stringWithFormat:@"SELECT IFNULL(Item_UOM,0)AS  Item_UOM  FROM TBL_Item_UOM where Item_Code ='%@' and Conversion ='1' order by Sync_Timestamp desc ", [productDict valueForKey:@"Item_Code"]];
    NSLog(@"query for fetching base uom %@", baseUOMQry);
    NSMutableArray* baseUOMArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:baseUOMQry];
    NSLog(@"base UOM details %@", [baseUOMArray description]);
    if (baseUOMArray.count>0) {
        self.baseUOMLbl.text=[NSString stringWithFormat:@"%@",[[baseUOMArray valueForKey:@"Item_UOM"] objectAtIndex:0]];
    }
    

    
    //fetch conversion
    
    
    NSArray* conversionArray=[SWDatabaseManager fetchConversionFactorforUOMitemCode:itemNumberLbl.text ItemUOM:@"CS"];
    
    NSLog(@"test stock %@", [conversionArray description]);
    
    if (conversionArray.count>0) {
        
         conversionRate=[[[conversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        
//        NSInteger tempStock=[[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Item_Code"]] integerValue];
        
        
        NSInteger tempStock=[totalStock integerValue];
        
        
        NSInteger convertedStock= tempStock/conversionRate;
        
        NSLog(@"converted stock %d", convertedStock);
        
        
        totalStockLbl.text=[[NSString stringWithFormat:@"%d", convertedStock] stringByAppendingString:@"(CS)"];
        
    }
    //test qty
//    
//    NSString* itemCodeforUOM=itemNumberLbl.text;
//    
//    NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@' ",itemCodeforUOM,@"CS"];
//    
//    NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
//    
//    NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
//    
//    
//    
//    if (consersionArr.count>0) {
//        
//        NSInteger lotQty=[[data stringForKey:@"Lot_Qty"] integerValue];
//        
//        NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
//        
//        
//        NSInteger updatedLotQty= round( lotQty/conversion);
//        
//        
//        //[mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];
//        
//    }


    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma  mark UITableView data source methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (productStockArray.count>0) {
        return productStockArray.count;
    }
    else
    {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    /*
    static NSString* stockViewCell=@"customHeader";
    AlSeerProductStockHeaderTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    */
    
    static NSString* stockViewCell=@"TblCellProductDetails";
    TblCellProductDetails * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    if (cell==nil) {
        
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"TblCellProductDetails" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.contentView.backgroundColor = UIColorFromRGB(0xEBFBF9);
    }
    
    cell.expiryLbl.textColor =  UIColorFromRGB(0x6A6F7B);
    cell.lotNumberLbl.textColor =  UIColorFromRGB(0x6A6F7B);
    cell.qtyLbl.textColor =  UIColorFromRGB(0x6A6F7B);
    cell.qualityInspectionLbl.textColor =  UIColorFromRGB(0x6A6F7B);
    cell.blockedStockLbl.textColor =  UIColorFromRGB(0x6A6F7B);
    
    cell.expiryLbl.font=kFontWeblySleekSemiBold(14);
    cell.lotNumberLbl.font=kFontWeblySleekSemiBold(14);
    cell.qtyLbl.font=kFontWeblySleekSemiBold(14);
    cell.qualityInspectionLbl.font=kFontWeblySleekSemiBold(14);
    cell.blockedStockLbl.font=kFontWeblySleekSemiBold(14);
   
    /*
    [cell.contentView.layer setBorderColor:UIColorFromRGB(0xE3E4E6).CGColor];
    [cell.contentView.layer setBorderWidth:1.0f];
    */
    
    /*
    UIView *topLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 1)];
    topLineView.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [cell.contentView addSubview:topLineView];


    UIView *bottomLineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height, self.view.bounds.size.width, 1)];
    bottomLineView.backgroundColor = UIColorFromRGB(0xE3E4E6);
    [cell.contentView addSubview:bottomLineView];
    */
    return cell;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    /*
    static NSString* stockViewCell=@"stockCell";
    AlSeerProductStockViewTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    
    if (cell==nil) {
        
       NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"AlSeerProductStockViewTableViewCell" owner:self options:nil];
        
        cell= [cellArray objectAtIndex:0];
        
        
    }
    
    //cell.qtyLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]];
    
    cell.lotNumberLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_No"] objectAtIndex:indexPath.row]];
    
    
    NSInteger tempQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]] integerValue];
    
    NSInteger convertedQty= tempQty/conversionRate;
    
    cell.qtyLbl.text=[NSString stringWithFormat:@"%d", convertedQty];
    
    
    
    //
   
    
    
    
    

    
    // Convert string to date object
  NSString*  dateStr=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Expiry_Date"] objectAtIndex:indexPath.row]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateStr = [dateFormat stringFromDate:date];

    
    NSLog(@"desired date format %@", dateStr);
    cell.expiryLbl.text=dateStr;
    
    
    return cell;
     */
    
    /*
     static NSString* stockViewCell=@"soStockCell";
     AlSeerSalesOrderProductStockTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    */
    
    static NSString* stockViewCell=@"TblCellProductDetails";
    TblCellProductDetails * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"TblCellProductDetails" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
    }
    
    //cell.qtyLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]];
    
    cell.lotNumberLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_No"] objectAtIndex:indexPath.row]];
    NSInteger tempQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]] integerValue];
    
    NSInteger convertedQty= tempQty/conversionRate;
    cell.qtyLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedQty];

    
    NSInteger blockedQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Blocked"] objectAtIndex:indexPath.row]] integerValue];
    NSInteger convertedBlockedQty= blockedQty/conversionRate;
   
    
   NSInteger qc=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"QC"] objectAtIndex:indexPath.row]] integerValue];
   NSInteger convertedQC= qc/conversionRate;
   cell.qualityInspectionLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedBlockedQty];
   cell.blockedStockLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedQC];
    
    // Convert string to date object
    NSString*  dateStr=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Expiry_Date"] objectAtIndex:indexPath.row]];
    
    if (dateStr != nil && ![dateStr isEqualToString:@"0"]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        dateStr = [dateFormat stringFromDate:date];
        cell.expiryLbl.text=dateStr;
     }else {
        cell.expiryLbl.text=@"N/A";
     }
    
   
    
    cell.lblTopLine.hidden = YES;
    cell.lblBottomLine.hidden = YES;
    
    cell.expiryLbl.textColor =  UIColorFromRGB(0x2C394A);
    cell.lotNumberLbl.textColor =  UIColorFromRGB(0x2C394A);
    cell.qtyLbl.textColor =  UIColorFromRGB(0x2C394A);
    cell.qualityInspectionLbl.textColor =  UIColorFromRGB(0x2C394A);
    cell.blockedStockLbl.textColor =  UIColorFromRGB(0x2C394A);
    
    cell.expiryLbl.font=kFontWeblySleekSemiBold(14);
    cell.lotNumberLbl.font=kFontWeblySleekSemiBold(14);
    cell.qtyLbl.font=kFontWeblySleekSemiBold(14);
    cell.qualityInspectionLbl.font=kFontWeblySleekSemiBold(14);
    cell.blockedStockLbl.font=kFontWeblySleekSemiBold(14);
    
    return cell;
}

+(NSString*)getDateStr:(NSString *)dateStr
{
    if([dateStr isEqualToString:@""] || dateStr.length<8)
        return @"";
    else
    {
        // Convert string to date object
        dateStr=[dateStr substringToIndex:8];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd MMM, YYYY"];
        dateStr = [dateFormat stringFromDate:date];
        return dateStr;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark fetching images from documents directory



-(void)fetchMediaFilesFromDocuments {
    
    [allMediaFilesArray removeAllObjects];
    
    
    
    NSString* inventoryItemID=[productDict stringForKey:@"Inventory_Item_ID"];
    
    NSString *query = [NSString stringWithFormat:@"Select * from TBL_Media_Files WHERE Download_Flag='N' AND Entity_ID_1 = '%@'",inventoryItemID];

    NSLog(@"query for media in product details %@", query);
    
    
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:query]];
    
    NSLog(@"media content %@", [array description]);
    
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Item_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [allMediaFilesArray addObject:customer];
    }
    
}

-(void)seperateMediaFilesIntoArrays {
    
    [imagesArray removeAllObjects];
    [pdfArray removeAllObjects];
    [videoArray removeAllObjects];
    [pptArray removeAllObjects];
    
    
    for (MediaFile * mfile in allMediaFilesArray) {
        
        NSLog(@"check media type %@", mfile.Media_Type);
        
        
        if ([mfile.Media_Type isEqualToString:@"Image"]) {
            [imagesArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Video"]) {
            [videoArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Brochure"]) {
            [pdfArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Powerpoint"]) {
            [pptArray addObject:mfile];
        }
        
        
        else{
            
        }
    }
}


@end
