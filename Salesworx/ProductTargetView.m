//
//  ProductTargetView.m
//  SWPlatform
//
//  Created by Irfan on 11/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductTargetView.h"

@implementation ProductTargetView
//@synthesize serProduct;
- (id)initWithProduct:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        customer=row;
        self.backgroundColor = [UIColor whiteColor];

        
        //saperator view
      UIView*   saperatorView=[[UIView alloc]initWithFrame:CGRectMake(280, 50, 2, 600)];
        saperatorView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"main-background.png"]];
        
        [self addSubview:saperatorView];

        
        
        
        

        customerHeaderView=[[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, 500, 60)] ;
        [customerHeaderView.headingLabel setText:[customer stringForKey:@"Description"]];
        [customerHeaderView.detailLabel setText:[customer stringForKey:@"Item_Code"]];
        [self addSubview:customerHeaderView];
        
        
        backGroundView = [[UIView alloc] initWithFrame:CGRectMake(300,70,20,768)] ;
//        backGroundView.layer.masksToBounds = NO;
//        backGroundView.layer.shadowOffset = CGSizeMake(-4, 8);
//        backGroundView.layer.shadowRadius = 3;
//        backGroundView.layer.shadowOpacity = 0.2;
//        backGroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        backGroundView.layer.borderWidth = 1.0;
    //  backGroundView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        categoryTableView=[[UITableView alloc]initWithFrame:CGRectMake(20, 70, 250, 560) style:UITableViewStyleGrouped] ;
        [categoryTableView setDelegate:self];
        [categoryTableView setDataSource:self];
        [categoryTableView setTag:2];
//        categoryTableView.layer.masksToBounds = YES;
//        categoryTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        categoryTableView.layer.borderWidth = 1.0;
        categoryTableView.backgroundView = nil;
        categoryTableView.backgroundColor = [UIColor whiteColor];
        
        targetTableView=[[UITableView alloc] initWithFrame:CGRectMake(300,400, 700,230) style:UITableViewStyleGrouped];
        [targetTableView setDelegate:self];
        [targetTableView setDataSource:self];
        [targetTableView setTag:1];
//        targetTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        targetTableView.layer.borderWidth = 1.0;
        targetTableView.backgroundView = nil;
        targetTableView.backgroundColor = [UIColor whiteColor];
        targetTableView.hidden=YES;
        
        if (@available(iOS 15.0, *)) {
            targetTableView.sectionHeaderTopPadding = 0;
            categoryTableView.sectionHeaderTopPadding = 0;
        }
        [self addSubview:targetTableView];
        
    }
    
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)loadTarget {
  //  [self.service dbGetTarget:[self.customer objectForKey:@"Customer_No"]];
    
    //serProduct.delegate=self;
    
    
       
    
    [self getProductServiceDiddbdbGetTargetInfo:[[SWDatabaseManager retrieveManager] dbdbGetTargetInfo:[customer stringForKey:@"Category"]]];
}
- (void)loadPieChartWithAchievement:(NSDictionary *)pie;{
    NSString *achieve = [pie valueForKey:@"Achieved_Target"] ;
    NSString *remain = [pie valueForKey:@"Remainning_Value"] ;
    
    pieChartNew = [[PCPieChart alloc] initWithFrame:CGRectMake(150, 10, 400, 350)] ;
    [pieChartNew setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
    
    [pieChartNew setDiameter:240];
    [pieChartNew setSameColorLabel:YES];
    components = [NSMutableArray array] ;
    PCPieComponent *component = [PCPieComponent pieComponentWithTitle:@"Remaining Target" value:[remain floatValue]];
    PCPieComponent *component2 = [PCPieComponent pieComponentWithTitle:@"Sales" value:[achieve floatValue]];
    
    [component2 setColour:PCColorOrange];
    [component setColour:PCColorBlue];
    
    [components addObject:component2];
    [components addObject:component];
    [pieChartNew setComponents:components];
    
    [backGroundView addSubview: pieChartNew];
    
}
#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(tableView.tag==1)
    {
        return 1;
    }
    else
    {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag==1)
    {
        if ([targets count] == 0)
        {
            return 1;
        }
        else
        {
            return 4;
        }
        
    }
    else
    {
        return [targets count];
        
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }

    if(tv.tag==1)
    {
        
        
        if ([targets count] == 0)
        {
            [cell.textLabel setText:NSLocalizedString(@"No target or achievement found", nil)];
            [cell.detailTextLabel setText:@""];

        }
        
        else
        {
            //NSDictionary *row = [targets objectAtIndex:indexPath.section];
            ;
            
            if (indexPath.row == 0) {
                [cell.textLabel setText:@"Target Value"];
                [cell.detailTextLabel setText:[ customerPie currencyStringForKey:@"Target_Value"]];
            }
            else if (indexPath.row == 1) {
                [cell.textLabel setText:@"FSR Target"];
                [cell.detailTextLabel setText:[ customerPie currencyStringForKey:@"FSR_Target"]];
            }
            else if (indexPath.row == 2) {
                [cell.textLabel setText:@"Sales Value"];
                [cell.detailTextLabel setText:[ customerPie currencyStringForKey:@"Sales_Value"]];
            }
            else if (indexPath.row == 3)
            {
                [cell.textLabel setText:@"Achievement"];
                NSString *achievPercent = [[ customerPie objectForKey:@"Achieved_Target"]stringValue];
                achievPercent = [achievPercent stringByAppendingString:@"%"];
                [cell.detailTextLabel setText:achievPercent];
            }
        }
        
    }
    
    else
    {   NSDictionary *row = [targets objectAtIndex:indexPath.row];
        
        [cell.textLabel setText:[row objectForKey:@"Category"]];
        // cell.textLabel.textColor = UIColorForPlainTable();
    }
        
    return cell;
    
}
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView.tag==1)
    {
        if ([targets count] == 0)
        {
            return nil;
        }
        else
        {
            return [customerPie objectForKey:@"Category"];
        }
    }
    else
    {
        return @"Brand/Category";
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag==1)
    {
        if ([targets count] == 0)
        {
            return 0.0f;
        }
        else
        {
            return 20.0f;
        }
    }
    else
    {
        return 40.0f;
    }
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    if (tv.tag==1)
    {
        
        if ([targets count] == 0)
        {
            return nil;
        }
        
      GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:[customerPie objectForKey:@"Category"]] ;
        return sectionHeader;
    }
    else {
        return nil;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tv.tag==1)
    {
        [tv deselectRowAtIndexPath:indexPath animated:YES];
    }
    else
    {
        customerPie=[targets objectAtIndex:indexPath.row];
        customerPie = [targets objectAtIndex:indexPath.row];
        [pieChartNew removeFromSuperview];
        [self loadPieChartWithAchievement:customerPie];
        [targetTableView reloadData];
        
        
        
    
        
        
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}
#pragma mark Product Service Delegate
- (void)getProductServiceDiddbdbGetTargetInfo:(NSArray *)info {
  
    
    
    
    
    //calculate total sales value and target value
    
    double sampleSalesVal,sampleTargetVal;
    
    
    sampleSalesVal=0;
    sampleTargetVal=0;

    
    
    
    for (NSInteger i=0; i<info.count; i++) {
        
        NSLog(@"sales val being added %f", [[[info valueForKey:@"Sales_Value"] objectAtIndex:i] doubleValue]);
        sampleSalesVal=sampleSalesVal+[[[info valueForKey:@"Sales_Value"] objectAtIndex:i] doubleValue];
        sampleTargetVal=sampleTargetVal+[[[info valueForKey:@"Target_Value"] objectAtIndex:i] doubleValue];
        NSLog(@"sales val after adding %f", sampleSalesVal);
        
    }
    NSLog(@"total sample sales value is %f", sampleSalesVal);
    NSLog(@"total target sales value is %f", sampleTargetVal);

    
    
    //[self setTargets:[info mutableCopy]];
    targets = [NSMutableArray arrayWithArray:info] ;
    if ([targets count] == 0)
    {
        targetTableView.frame = CGRectMake(20, 70, self.frame.size.width-40,self.frame.size.height-80);

      //  targetTableView.frame=self.bounds;
    }
    else
    {
        int targetValue;
        int saleValue;
        for (int i=1; i<=[targets count];i++)
        {
            customerPie = [NSMutableDictionary dictionaryWithDictionary:[targets objectAtIndex:i-1]];
            targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
            if([customerPie objectForKey:@"Sales_Value"]==(id) [NSNull null] ||  [[customerPie objectForKey:@"Sales_Value"]isEqual:@""])
            {saleValue = 0;}
            else
            {
            saleValue = [[customerPie objectForKey:@"Sales_Value"] intValue];}
            achievmentValue = saleValue * 100 /targetValue ;
            float remainingValue = 100 - achievmentValue ;
            [customerPie setObject:[NSNumber numberWithFloat:achievmentValue] forKey:@"Achieved_Target"];
            [customerPie setObject:[NSNumber numberWithFloat:remainingValue] forKey:@"Remainning_Value"];
            [targets replaceObjectAtIndex:i-1 withObject:customerPie];
            
        }
        
        customerPie = [targets objectAtIndex:0];
        targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
        NSNumber *num = [NSNumber numberWithInt:targetValue];
        if((num == nil) || [num intValue] == 0)
        {
        }
        else
        {
            [self addSubview:backGroundView];
            [self addSubview:categoryTableView];
            slices = [NSMutableArray arrayWithCapacity:2] ;
        }
        [self loadPieChartWithAchievement:customerPie];
        
    }
    
    [targetTableView reloadData];
    [categoryTableView reloadData];
    targetTableView.hidden=NO;
    info=nil;
}
- (void)loadPieChart
{
    
}


@end
