//
//  Singleton.m
//  Chyea
//
//  Created by Usman Ahmed on 5/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Singleton.h"
#import "Reachability.h"
//#import "ILAlertView.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
@implementation Singleton

@synthesize frameWidth,frameHeight, shouldPresentCustomerList,customerIndexPath,mainScreenWidth,mainScreenHeight,invoiceDate,invoiceValue,isCashCustomer,cashCustomerDictionary,FOC_Product_Dict,isFromSync,isForSyncUpload;
@synthesize pieChartHiegth,pieChartX,pieChartY,pieChartWidht,isBarCode,valueBarCode,paymentType,popToCash,isDueScreen;
@synthesize currentCustomer,isSaveOrder,stockAvailble,productBarPopOver,visitParentView,savedPerformaCurrentOrder,cashCustomerList,lastSyncDate,isManageOrder,manageOrderRefNumber,planDetailId,isSendOrderDone,xmlParsedString,isActivated,savedOrderArray,orderStartDate,savedOrderTemplateArray,databaseIsOpen,isFullSyncDone,isFullSyncCollection,isFullSyncCustomer,isFullSyncDashboard,isFullSyncProduct,isFullSyncSurvey,isFullSyncMessage,isDistributionChecked,distributionRef,isDistributionItemGet,isSurveyTaken,showCustomerDashboard;

static Singleton *sharedSingleton = nil;

+ (Singleton*) retrieveSingleton {  
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [[Singleton alloc] init];
		}
	}
	return sharedSingleton;
}

+ (id) allocWithZone:(NSZone *) zone {
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}
+ (void) destroyMySingleton
{
    sharedSingleton = nil;
}
+ (BOOL)connectedToInternet
{
    Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];    
    BOOL result = NO;
    if (internetStatus == ReachableViaWiFi )
    {
        result = YES;    
    } 
    else if(internetStatus == ReachableViaWWAN){
        result = YES;
    }
    return result;
}

+(void) dbGetFreeMemory {
    
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        NSLog(@"Failed to fetch vm statistics");
    
    /* Stats in bytes */
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
    natural_t mem_total = mem_used + mem_free;
    
       /* Stats in bytes */
    NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
    //size_t size = mem_free - 2048;
    void *allocation = malloc(mem_free - 10240);
   // bzero(allocation, size);
    free(allocation);
    
    int iUsed = round(mem_used/1048576);
    int iFree = round(mem_free/1048576);
    int iTotal = round(mem_total/1048576);
    NSLog(@"used: %d free: %d total: %d", iUsed, iFree, iTotal);
    
//    [ILAlertView showWithTitle:@"MEMORY WARNING!!!"
//                       message:[NSString stringWithFormat:@"Free Memory : %dmb \n Used Memory : %dmb \n Please save your work then kill and relaunch application.",iFree,iUsed]
//              closeButtonTitle:NSLocalizedString(@"OK", nil)
//             secondButtonTitle:nil
//           tappedButtonAtIndex:nil];
    
}

@end
