//
//  DetailBaseView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "DetailBaseView.h"
#import "SWFoundation.h"
#import "SWSection.h"

@interface DetailBaseView (PRIVATE)
- (UIView *)getHeaderView;
@end

@implementation DetailBaseView

@synthesize tableView,productImage;
@synthesize parent;
@synthesize headerView;
@synthesize dataSource;



- (id)initWithParent:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        [self setParent:row];
        self.backgroundColor = [UIColor whiteColor];

        self.tableView=nil;
        [self setTableView:[[UITableView alloc] initWithFrame:CGRectMake(10, 150, 660, 395) style:UITableViewStylePlain]];
        //[self.tableView setAutoresizingMask: UIViewAutoresizingFlexibleHeight];
        [self.tableView setDelegate:self];
        [self.tableView setDataSource:self];
        self.tableView.backgroundColor=[UIColor clearColor];
        [self.tableView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.tableView.layer setBorderWidth:1.0f];
        if (@available(iOS 15.0, *)) {
            self.tableView.sectionHeaderTopPadding = 0;
        }
        [self addSubview:self.tableView];
        self.tableView.userInteractionEnabled=NO;
        
        UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(5+self.tableView.frame.size.width,150, self.frame.size.width-5-660,395)];
        [tempView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [tempView.layer setBorderWidth:1.0f];
        
      [self setHeaderView:[self getHeaderView]];
        [self addSubview:headerView];
        _IsSectionedView = NO;
        
        productImage=nil;
        productImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noImageAvailable.png" cache:NO]];
        productImage.frame = CGRectMake(5+self.tableView.frame.size.width+15,180, 330,330);
        [productImage.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [productImage.layer setBorderWidth:1.0f];
        productImage.contentMode = UIViewContentModeScaleAspectFit;
        productImage.clipsToBounds = YES;
        

        [self addSubview:productImage];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setDataSource:(NSArray *)ds {
    if (dataSource) {
       // [dataSource release];
    }
    dataSource = nil;
    
    dataSource = ds;
    
    if (dataSource.count > 0) {
        if ([[dataSource objectAtIndex:0] isKindOfClass:[SWSection class]]) {
            _IsSectionedView = YES;
        }
    }
    
    [self.tableView reloadData];
}

#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_IsSectionedView) {
        return self.dataSource.count;
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_IsSectionedView) {
        SWSection *s = [self.dataSource objectAtIndex:section];
        return [s.rows count];
    }
    
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
           
    return cell;
}
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (_IsSectionedView) {
        SWSection *s = [self.dataSource objectAtIndex:section];
        return s.sectionName;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (_IsSectionedView) {
        return 25.0f;
    }
    
    return 0;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}

@end
