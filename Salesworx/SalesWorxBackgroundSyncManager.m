//
//  SalesWorxBackgroundSyncManager.m
//  SalesWars
//
//  Created by Neha Gupta on 25/01/21.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "SalesWorxBackgroundSyncManager.h"
#import "SWDatabaseManager.h"
#import "SWAppDelegate.h"
#import "SWDefaults.h"
#import <Foundation/Foundation.h>
#import "ReachabilitySync.h"

@implementation SalesWorxBackgroundSyncManager
-(void)postVisitDataToserver
{
    [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendTrxAndVisitData" andProcDic:[self getProcedureDictionary]];
}

-(NSDictionary *)getProcedureDictionary
{
    if([SWDefaults lastVTRXBackGroundSyncDate]==nil)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else if ([[SWDefaults convertNSStringtoNSDate:[SWDefaults lastFullSyncDate]] compare:[SWDefaults convertNSStringtoNSDate:[SWDefaults lastVTRXBackGroundSyncDate]]] == NSOrderedDescending)
    {
        lastSynctimeStr=[SWDefaults lastFullSyncDate];
    }
    else {
        lastSynctimeStr=[SWDefaults lastVTRXBackGroundSyncDate];
    }
    
    currentSyncStarttimeStr = [SWDefaults fetchCurrentDateTimeinDatabaseFormat];

    /*** Visits*/
    NSMutableArray *vistsArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetVisits, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSString *visitsXmlString=[self prepareXMLStringForVisits:vistsArray];
    NSString  *visitsXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)visitsXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    /*** Orders*/
    NSMutableArray *ordersArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrders, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *orderLineItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderLineItems, lastSynctimeStr,currentSyncStarttimeStr]]];

    NSMutableArray *orderLineItemLotsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderLots,lastSynctimeStr,currentSyncStarttimeStr]]];

    
    NSDictionary *ordersData=[NSDictionary dictionaryWithObjectsAndKeys:ordersArray,@"orderArray",orderLineItemsarray,@"itemArray",orderLineItemLotsarray,@"lotArray", nil];
    NSString *ordersXmlString=[self prepareXMLStringForOrders:ordersData];
    NSString  *ordersXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)ordersXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));

    
    /** Order History*/
    NSMutableArray *orderHistoryArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderHistory, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSMutableArray *orderHistoryItemsarray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetOrderHistoryLineItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    

    NSDictionary *orderHistorysData=[NSDictionary dictionaryWithObjectsAndKeys:orderHistoryArray,@"orderHistoryArray",orderHistoryItemsarray,@"itemArray", nil];
    NSString *orderHistoryXmlString=[self prepareXMLStringForOrderHistory:orderHistorysData];
    NSString  *orderHistoryXMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)orderHistoryXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    

    
    /***Distribution Check*/
    NSMutableArray *distributionCheckArray=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDistributionCheck, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSLog(@"%@",distributionCheckArray);
    NSMutableArray *distributionCheckItems=[[NSMutableArray alloc]initWithArray:[[[SWDatabaseManager alloc]init] fetchDataForQuery:[NSString stringWithFormat:K_BS_SQLdbGetDistributionCheckItems, lastSynctimeStr,currentSyncStarttimeStr]]];
    NSLog(@"%@",distributionCheckItems);
    
    NSDictionary *distributionCheckData=[NSDictionary dictionaryWithObjectsAndKeys:distributionCheckArray,@"DistributionCheckData",distributionCheckItems,@"DistributionCheckItem",nil];
    NSString *distributionXmlString=[self prepareXMLStringForDistribution:distributionCheckData];
    NSString *distributionXMLPassingString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)distributionXmlString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
    
    
    /***Final Dictionary*/
    NSArray *procedureParametersArray = [[NSMutableArray alloc]initWithObjects:@"VisitsData", @"DistributionCheckData", @"OrderList", @"OrderHistoryData", nil];
    NSArray *procedureValuesArray=[[NSMutableArray alloc]initWithObjects:visitsXMLPassingString, distributionXMLPassingString, ordersXMLPassingString, orderHistoryXMLPassingString, nil];
    
    
    return [[NSDictionary alloc]initWithObjects:procedureValuesArray forKeys:procedureParametersArray];
}

- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcDic:(NSDictionary *)procDic
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *serverAPI = [NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSLog(@"Request sent at %@",[NSDate date]);
    NSString *strParams =[[NSString alloc] init];
    NSString *strName=procName;
    NSArray *procedureParametersArray=[[NSMutableArray alloc]initWithObjects:@"VisitsData", @"DistributionCheckData", @"OrderList", @"OrderHistoryData", nil];
    
    for (NSInteger i=0; i<procedureParametersArray.count; i++) {
        strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",[procedureParametersArray objectAtIndex:i]];
        strParams=[strParams stringByAppendingFormat:@"&ProcValues=%@",[procDic valueForKey:[procedureParametersArray objectAtIndex:i]]];

    }
    
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",@"SalesRep_ID"];
    strParams=[strParams stringByAppendingFormat:@"&ProcValues=%d",[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue]];
    
    
    NSString *strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    NSString *ClientVersion=[[[SWDefaults alloc]init] getAppVersionWithBuild];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&SyncType=%@&SyncLocation=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,@"Send_TrxAndVisitData_V1",[testServerDict stringForKey:@"name"],strName,strParams];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    SWAppDelegate *appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication] delegate];

    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray* resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         NSLog(@"Sync response time %@",[NSDate date]);
         NSLog(@"background sync Response %@",resultArray);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None;
        
         if(resultArray.count>0 && [[[resultArray objectAtIndex:0] valueForKey:@"ErrorCode"] doubleValue]==0)
         {
             [[SWDatabaseManager retrieveManager]deleteOrdersAfterBackGroundSync:currentSyncStarttimeStr AndLastSyncTime:lastSynctimeStr];
             [SWDefaults setLastVTRXBackGroundSyncDate:currentSyncStarttimeStr];
        }
     }
      failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         NSLog(@"background sync error %@",error);
         appDelegate.currentExceutingDataUploadProcess=KDataUploadProcess_None;
     }];
    
    //call start on your request operation
    [operation start];
}

#pragma mark Visits methods
-(NSString *)prepareXMLStringForVisits:(NSMutableArray *)VisitsArray {
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    [xmlWriter writeStartElement:@"ActualVisits"];

    if(!([VisitsArray count]==0))
    {
        for (NSInteger v=0; v<[VisitsArray count]; v++)
        {
            NSMutableDictionary * visitDictionary = [NSMutableDictionary dictionaryWithDictionary:[VisitsArray objectAtIndex:v]];

            [xmlWriter writeStartElement:@"Visit"];
            
            if (![[visitDictionary stringForKey:@"Actual_Visit_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Actual_Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Actual_Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"FSR_Plan_Detail_ID"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"FSR_Plan_Detail_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Site_Use_ID"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Site_Use_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Customer_Barcode"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Customer_Barcode"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Customer_Barcode"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Visit_Start_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_Start_Date"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_Start_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Visit_End_Date"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Visit_End_Date"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Visit_End_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Text_Notes"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Text_Notes"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Text_Notes"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"SalesRep_ID"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"SalesRep_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Emp_Code"].length !=0  ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];}
            if (![[visitDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Scanned_Closing"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Scanned_Closing"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Scanned_Closing"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Odo_Reading"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Odo_Reading"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Odo_Reading"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Cash_Customer_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Cash_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Cash_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Cash_Site_ID"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Cash_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Cash_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"CC_Name"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"CC_Name"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"CC_Name"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"CC_TelNo"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"CC_TelNo"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"CC_TelNo"]];
                [xmlWriter writeEndElement];
            } else {
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:@""];
                [xmlWriter writeEndElement];
            }
            
            if (![[visitDictionary stringForKey:@"Latitude"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Latitude"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Latitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Longitude"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Longitude"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Longitude"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[visitDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [visitDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[visitDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            [xmlWriter writeEndElement];//Visit
        }
    }
    [xmlWriter writeEndElement];//Visits
    NSLog(@"Visits = %@",xmlWriter.toString);
    return [xmlWriter toString];
}

#pragma mark DistributionCheck methods
-(NSString *)prepareXMLStringForDistribution:(NSDictionary *)distributionData {
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *distributionArray=[distributionData valueForKey:@"DistributionCheckData"];
    NSMutableArray *distributionCheckItemArray=[distributionData valueForKey:@"DistributionCheckItem"];
    [xmlWriter writeStartElement:@"DistributionChecks"];
    if(!([distributionArray count]==0))
    {
        for (int o=0; o<[distributionArray count]; o++)
        {
            NSMutableDictionary * distributionCheckDictionary = [NSMutableDictionary dictionaryWithDictionary:[distributionArray objectAtIndex:o]];
            NSString *orderRef = [distributionCheckDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            [xmlWriter writeStartElement:@"DistributionCheck"];
            
            if (![[distributionCheckDictionary stringForKey:@"DistributionCheck_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"DistributionCheck_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"DistributionCheck_ID"]];
                [xmlWriter writeEndElement];
                
                if (![[distributionCheckDictionary stringForKey:@"Customer_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Customer_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C2"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Customer_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Site_Use_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Site_Use_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C3"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Site_Use_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"SalesRep_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"SalesRep_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C4"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"SalesRep_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Emp_Code"].length !=0){
                    [xmlWriter writeStartElement:@"C5"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Emp_Code"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Checked_On"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Checked_On"].length !=0){
                    [xmlWriter writeStartElement:@"C6"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Checked_On"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Visit_ID"].length !=0){
                    [xmlWriter writeStartElement:@"C7"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Visit_ID"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Status"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Status"].length !=0){
                    [xmlWriter writeStartElement:@"C8"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Status"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                    [xmlWriter writeStartElement:@"C9"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_1"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                    [xmlWriter writeStartElement:@"C10"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_2"]];
                    [xmlWriter writeEndElement];
                }
                if (![[distributionCheckDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [distributionCheckDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                    [xmlWriter writeStartElement:@"C11"];
                    [xmlWriter writeCharacters:[distributionCheckDictionary stringForKey:@"Custom_Attribute_3"]];
                    [xmlWriter writeEndElement];
                }
                
                if([distributionCheckItemArray count]!=0)
                {
                    [xmlWriter writeStartElement:@"LineItems"];
                    for (int i=0; i<[distributionCheckItemArray count]; i++)
                    {
                        NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[distributionCheckItemArray objectAtIndex:i]];
                        if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                        {
                            [xmlWriter writeStartElement:@"LineItem"];
                            if (![[itemDictionary stringForKey:@"DistributionCheckLine_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"DistributionCheckLine_ID"].length !=0){
                                [xmlWriter writeStartElement:@"C1"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"DistributionCheckLine_ID"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"DistributionCheck_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"DistributionCheck_ID"].length !=0){
                                [xmlWriter writeStartElement:@"C2"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"DistributionCheck_ID"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                                [xmlWriter writeStartElement:@"C3"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Is_Available"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Is_Available"].length !=0){
                                [xmlWriter writeStartElement:@"C4"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Is_Available"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Qty"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Qty"].length !=0){
                                [xmlWriter writeStartElement:@"C5"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Qty"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Expiry_Dt"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Expiry_Dt"].length !=0){
                                [xmlWriter writeStartElement:@"C6"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Expiry_Dt"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                                [xmlWriter writeStartElement:@"C7"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                                [xmlWriter writeStartElement:@"C8"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                                [xmlWriter writeStartElement:@"C9"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                                [xmlWriter writeStartElement:@"C10"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                                [xmlWriter writeEndElement];
                            }
                            if (![[itemDictionary stringForKey:@"Reorder"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Reorder"].length !=0){
                                [xmlWriter writeStartElement:@"C11"];
                                [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Reorder"]];
                                [xmlWriter writeEndElement];
                            }
                        }
                        [xmlWriter writeEndElement];
                    }
                    [xmlWriter writeEndElement];
                }
            }
            [xmlWriter writeEndElement];
        }
    }
    [xmlWriter writeEndElement];
    NSLog(@"Distribution check = %@",xmlWriter.toString);
    return [xmlWriter toString];
}

#pragma mark SalesOrder methods
-(NSString *)prepareXMLStringForOrders:(NSDictionary *)ordersData {
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderArray=[ordersData valueForKey:@"orderArray"];
    NSMutableArray *lotArray=[ordersData valueForKey:@"lotArray"];
    NSMutableArray *itemArray=[ordersData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"Orders"];

    if(!([orderArray count]==0))
    {
        for (int o=0; o<[orderArray count]; o++)
        {
            NSMutableDictionary * orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"Order"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0 ){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Customer_PO_Number"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Customer_PO_Number"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Customer_PO_Number"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Status"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Signee_Name"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Signee_Name"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Signee_Name"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_1"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_2"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_3"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Update_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Update_Date"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Update_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Last_Updated_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Last_Updated_By"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Last_Updated_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Discount_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Discount_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Discount_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Print_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Print_Status"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Print_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C27"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Order_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Order_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C28"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Order_Amt"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C29"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C30"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C31"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_4"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_4"].length !=0){
                [xmlWriter writeStartElement:@"C32"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_4"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_5"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_5"].length !=0){
                [xmlWriter writeStartElement:@"C33"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_5"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Custom_Attribute_6"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Custom_Attribute_6"].length !=0){
                [xmlWriter writeStartElement:@"C34"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Custom_Attribute_6"]];
                [xmlWriter writeEndElement];
            }
            
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    NSString *itemLine = [itemDictionary stringForKey:@"Line_Number"];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Display_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Display_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Display_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Def_Bonus"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Def_Bonus"].length !=0){
                            //NSLog(@"Def_Bonus");
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Def_Bonus"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_1"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_1"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_1"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_2"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_2"].length !=0){
                            [xmlWriter writeStartElement:@"C12"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_2"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Custom_Attribute_3"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Custom_Attribute_3"].length !=0){
                            [xmlWriter writeStartElement:@"C13"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Custom_Attribute_3"]];
                            [xmlWriter writeEndElement];
                        }
                        
                        if([lotArray count]!=0)
                        {
                            [xmlWriter writeStartElement:@"Lots"];
                            for (int l=0 ; l<[lotArray count] ; l++)
                            {
                                NSMutableDictionary * lotDictionary = [NSMutableDictionary dictionaryWithDictionary:[lotArray objectAtIndex:l]];
                                if([orderRef isEqualToString:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]] && [itemLine isEqualToString:[lotDictionary stringForKey:@"Line_Number"]]){
                                    [xmlWriter writeStartElement:@"Lot"];
                                    
                                    [xmlWriter writeStartElement:@"C1"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Row_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C2"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C3"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Line_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C4"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Lot_Number"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C5"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Ordered_Quantity"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C6"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Is_FIFO"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeStartElement:@"C7"];
                                    [xmlWriter writeCharacters:[lotDictionary stringForKey:@"Org_ID"]];
                                    [xmlWriter writeEndElement];
                                    
                                    [xmlWriter writeEndElement];//lot
                                }
                            }
                            [xmlWriter writeEndElement];//lots
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            [xmlWriter writeEndElement];//Orders
        }
    }
    [xmlWriter writeEndElement];//Order
    NSLog(@"Orders = %@",xmlWriter.toString);
    return [xmlWriter toString];
}

#pragma mark OrderHistory Methods
-(NSString *)prepareXMLStringForOrderHistory:(NSDictionary *)OrderHistoryData {
    
    XMLWriter *xmlWriter = [[XMLWriter alloc]init];
    NSMutableArray *orderHistoryArray=[OrderHistoryData valueForKey:@"orderHistoryArray"];
    NSMutableArray *itemArray=[OrderHistoryData valueForKey:@"itemArray"];
    [xmlWriter writeStartElement:@"SalesHistory"];

    if(!([orderHistoryArray count]==0))
    {
        for (int o=0; o<[orderHistoryArray count]; o++)
        {
            NSMutableDictionary *orderDictionary = [NSMutableDictionary dictionaryWithDictionary:[orderHistoryArray objectAtIndex:o]];
            NSString *orderRef = [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"];
            
            [xmlWriter writeStartElement:@"Trx"];
            
            if (![[orderDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Row_ID"].length !=0){
                [xmlWriter writeStartElement:@"C1"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Row_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                [xmlWriter writeStartElement:@"C2"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Doc_Type"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Doc_Type"].length !=0){
                [xmlWriter writeStartElement:@"C3"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Doc_Type"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Creation_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Creation_Date"].length !=0){
                [xmlWriter writeStartElement:@"C4"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Creation_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Created_By"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Created_By"].length !=0){
                [xmlWriter writeStartElement:@"C5"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Created_By"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"System_Order_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"System_Order_Date"].length !=0){
                [xmlWriter writeStartElement:@"C6"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"System_Order_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Request_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Request_Date"].length !=0){
                [xmlWriter writeStartElement:@"C7"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Request_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Schedule_Ship_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Schedule_Ship_Date"].length !=0){
                [xmlWriter writeStartElement:@"C8"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Schedule_Ship_Date"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Customer_ID"].length !=0 ){
                
                [xmlWriter writeStartElement:@"C9"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Ship_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Ship_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C10"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Ship_To_Site_ID"]];
                [xmlWriter writeEndElement];}
            if (![[orderDictionary stringForKey:@"Inv_To_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C11"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Inv_To_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Inv_To_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C12"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Inv_To_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"BO_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"BO_Status"].length !=0){
                [xmlWriter writeStartElement:@"C13"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"BO_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Start_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Start_Time"].length !=0){
                [xmlWriter writeStartElement:@"C14"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Start_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"End_Time"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"End_Time"].length !=0){
                [xmlWriter writeStartElement:@"C15"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"End_Time"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Shipping_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Shipping_Instructions"].length !=0){
                [xmlWriter writeStartElement:@"C16"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Shipping_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Packing_Instructions"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Packing_Instructions"].length !=0){
                [xmlWriter writeStartElement:@"C17"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Packing_Instructions"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Emp_Code"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Emp_Code"].length !=0){
                [xmlWriter writeStartElement:@"C18"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Emp_Code"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"ERP_Ref_No"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"ERP_Ref_No"].length !=0){
                [xmlWriter writeStartElement:@"C19"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"ERP_Ref_No"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"ERP_Status"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"ERP_Status"].length !=0){
                [xmlWriter writeStartElement:@"C20"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"ERP_Status"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Transaction_Amt"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Transaction_Amt"].length !=0){
                [xmlWriter writeStartElement:@"C21"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Transaction_Amt"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Due_Date"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Due_Date"].length !=0){
                [xmlWriter writeStartElement:@"C22"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Due_Date"]];
                [xmlWriter writeEndElement];
            }
            
            if (![[orderDictionary stringForKey:@"Credit_Customer_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Customer_ID"].length !=0){
                [xmlWriter writeStartElement:@"C23"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Customer_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Credit_Site_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Credit_Site_ID"].length !=0){
                [xmlWriter writeStartElement:@"C24"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Credit_Site_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Visit_ID"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Visit_ID"].length !=0){
                [xmlWriter writeStartElement:@"C25"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Visit_ID"]];
                [xmlWriter writeEndElement];
            }
            if (![[orderDictionary stringForKey:@"Sync_Timestamp"] isEqualToString:@"<null>"] && [orderDictionary stringForKey:@"Sync_Timestamp"].length !=0){
                [xmlWriter writeStartElement:@"C26"];
                [xmlWriter writeCharacters:[orderDictionary stringForKey:@"Sync_Timestamp"]];
                [xmlWriter writeEndElement];
            }
            
            if([itemArray count]!=0)
            {
                [xmlWriter writeStartElement:@"LineItems"];
                for (int i=0; i<[itemArray count]; i++)
                {
                    NSMutableDictionary *itemDictionary = [NSMutableDictionary dictionaryWithDictionary:[itemArray objectAtIndex:i]];
                    if([orderRef isEqualToString:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]])
                    {
                        [xmlWriter writeStartElement:@"LineItem"];
                        if (![[itemDictionary stringForKey:@"Row_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Row_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C1"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Row_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Orig_Sys_Document_Ref"].length !=0){
                            [xmlWriter writeStartElement:@"C2"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Orig_Sys_Document_Ref"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Line_Number"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Line_Number"].length !=0){
                            [xmlWriter writeStartElement:@"C3"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Line_Number"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Inventory_Item_ID"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Inventory_Item_ID"].length !=0){
                            [xmlWriter writeStartElement:@"C4"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Inventory_Item_ID"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Lot_No"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Lot_No"].length !=0){
                            [xmlWriter writeStartElement:@"C5"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Lot_No"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Expiry_Date"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Expiry_Date"].length !=0){
                            [xmlWriter writeStartElement:@"C6"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Expiry_Date"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Order_Quantity_UOM"].length !=0){
                            [xmlWriter writeStartElement:@"C7"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Order_Quantity_UOM"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Ordered_Quantity"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Ordered_Quantity"].length !=0){
                            //NSLog(@"Unit_Selling_Price");
                            [xmlWriter writeStartElement:@"C8"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Ordered_Quantity"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Calc_Price_Flag"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Calc_Price_Flag"].length !=0){
                            [xmlWriter writeStartElement:@"C9"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Calc_Price_Flag"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Unit_Selling_Price"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Unit_Selling_Price"].length !=0){
                            [xmlWriter writeStartElement:@"C10"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Unit_Selling_Price"]];
                            [xmlWriter writeEndElement];
                        }
                        if (![[itemDictionary stringForKey:@"Item_Value"] isEqualToString:@"<null>"] && [itemDictionary stringForKey:@"Item_Value"].length !=0){
                            [xmlWriter writeStartElement:@"C11"];
                            [xmlWriter writeCharacters:[itemDictionary stringForKey:@"Item_Value"]];
                            [xmlWriter writeEndElement];
                        }
                        [xmlWriter writeEndElement];//lineItem
                    }
                }
                [xmlWriter writeEndElement];//lineItems
            }
            [xmlWriter writeEndElement];//TRX
        }
    }
    [xmlWriter writeEndElement];//SalesHistory
    NSLog(@"Order history = %@",xmlWriter.toString);
    return [xmlWriter toString];
}

@end
