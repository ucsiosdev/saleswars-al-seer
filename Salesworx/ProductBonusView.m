//
//  ProductBonusView.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "ProductHeaderView.h"
#import "ProductBonusView.h"
#import "SWPlatform.h"
@implementation ProductBonusView
//@synthesize serProduct;

@synthesize gridView;
- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        product= [NSDictionary dictionaryWithDictionary:p];
        self.backgroundColor =[UIColor whiteColor];

        
        gridView=[[GridView alloc] initWithFrame:CGRectZero] ;
        [gridView setFrame:CGRectMake(0, 60, 800, 600)];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        [self addSubview:gridView];
        
        ProductHeaderView *productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60)] ;
        [productHeaderView.headingLabel setText:[product stringForKey:@"Description"]];
        [productHeaderView.detailLabel setText:[product stringForKey:@"Item_Code"]];
        [self addSubview:productHeaderView];
        
        discountLable = [[UILabel alloc] initWithFrame:CGRectMake(800,12, 200, 32)] ;
        [discountLable setTextAlignment:NSTextAlignmentCenter];
        [discountLable setBackgroundColor:[UIColor clearColor]];
        [discountLable setTextColor:[UIColor darkGrayColor]];
        [discountLable setFont:RegularFontOfSize(18.0)];
        [productHeaderView addSubview:discountLable];
     
        gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    return self;
}

-(void)loadBonus
{
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[product stringForKey:@"Item_Code"]]];
}


#pragma mark ProductService delegate
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info {
    items = nil;
    items= [NSArray arrayWithArray:info];
    if([items count] > 0)
    {
        discountRate = [product stringForKey:@"Discount"];
        if(discountRate.length == 0)
        {
            discountLable.text =@"";

        }
        else{
            discountRate= [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"Discount Applicable", nil),discountRate];
            discountLable.text = [discountRate stringByAppendingString:@"%"];
        }

    }
    [gridView reloadData];
}

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (items.count == 0) {
        [gridView.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        return 0;
    }
    [gridView.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"GridViewItemsCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    return cell;
}
}

#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    if (column == 0) {
        title = NSLocalizedString(@"From", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"To", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Bonus", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Type", nil);
    } else if (column == 4) {
        title = NSLocalizedString(@"Bonus Item", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [items objectAtIndex:row];
    if (column == 0) {
        text =[NSString stringWithFormat:@"     %@", [data objectForKey:@"Prom_Qty_From"]];
    } else if (column == 1) {
        text = [data stringForKey:@"Prom_Qty_To"];
    } else if (column == 2) {
        text = [data stringForKey:@"Get_Qty"];
    } else if (column == 3) {
        text = [data stringForKey:@"Price_Break_Type_Code"];
    } else if (column == 4) {
        text = [data stringForKey:@"Description"];
    }
    return text;
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 10;
    } else if (columnIndex == 1) {
        return 10;
    } else if (columnIndex == 2) {
        return 10;
    }
    else if (columnIndex == 3) {
        return 10;
    }
    else if (columnIndex == 4) {
        return 40;
    }
    return 0;
}
@end
