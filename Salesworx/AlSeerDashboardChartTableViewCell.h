//
//  AlSeerDashboardChartTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UUChart.h"

@interface AlSeerDashboardChartTableViewCell : UITableViewCell<UUChartDataSource>


{
    NSIndexPath *path;
    UUChart *chartView;
    NSMutableArray* agencyArray;
    NSMutableArray* agencyDetails;
}

@property(strong,nonatomic)NSString* customerDetail;
@property(strong,nonatomic)NSMutableDictionary* customerDict;
@property(nonatomic) BOOL isCommingFromCustDash;
- (void)configUI:(NSIndexPath *)indexPath;
@end
