//
//  SalesOrderTemplate.h
//  SalesWars
//
//  Created by Prashannajeet on 31/08/22.
//  Copyright © 2022 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SalesOrderTemplate : UIView<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger numberOfRowsInaPage;
    NSInteger StartIndexInCurrentPage;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitleCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCustomerContact;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleCustomerNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSalesman;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSalesmanContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleOrderTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSign;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleComments;


@property (nonatomic) NSInteger pageNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblRefNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomer;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerContact;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomerNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblSalesMan;
@property (weak, nonatomic) IBOutlet UILabel *lblSalesContactNo;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTime;
@property (weak, nonatomic) IBOutlet UIImageView *signeeImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblSigneeName;

@property (weak, nonatomic) IBOutlet UILabel *lblSigneeComments;
@property (weak, nonatomic) IBOutlet UITableView *pdfTableView;
@property(strong,nonatomic)NSMutableDictionary *salesOrderDict, *customerDict, *orderAdditionalInfoDict;
@property(strong,nonatomic)NSMutableArray * itemsSalesOrder;

@property(nonatomic) BOOL isFromManageOrderOnSwipe;

-(void)updatePDF;
@end

NS_ASSUME_NONNULL_END
