//
//  DistributionUpdateViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "DistributionUpdateViewController.h"

@interface DistributionUpdateViewController ()

@end

@implementation DistributionUpdateViewController

@synthesize target;
@synthesize action;

- (id)initWithDistributionItem:(NSDictionary *)item {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
       distributionItem=[NSMutableDictionary dictionaryWithDictionary:item];
       // [self setTitle:@"Update"];
        
        self.title=@"Update";
        
        CGRect frame = CGRectMake(0, 0, [self.title sizeWithFont:bodySemiBold].width, 44);
        UILabel *label = [[UILabel alloc] initWithFrame:frame];
        label.backgroundColor = [UIColor clearColor];
        label.textColor=[UIColor blackColor];
        label.font = bodySemiBold;
        label.textAlignment = NSTextAlignmentCenter;
        self.navigationItem.titleView = label;
        label.text = self.title;
        
        self.navigationController.navigationItem.titleView=label;
        
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView=nil;
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = self.view.bounds;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStyleDone target:self action:@selector(done:)] ];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)] ];
}

- (void)done:(id)sender {
    
    [self.view endEditing:YES];
    if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
        [((SWTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]) resignResponder];
        [((SWDateFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]]) resignResponder];
    }
    [((SWDateFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]]) resignResponder];
    
    NSLog(@"check distribution data in done action %@", [distributionItem description]);
    
    //save the shelf price of dictribution check in custom_Attribute_1 for tbl_Distribution_check
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:distributionItem];
#pragma clang diagnostic pop
}

- (void)cancel:(id)sender {
     #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:nil]; 
                #pragma clang diagnostic pop
}

- (void)scroll:(EditableCell *)cell {
    [self.tableView scrollRectToVisible:cell.frame animated:YES];
}
#pragma mark UITableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"] && section==1) {
        return 3;
    }
    else
    {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    NSLog(@"distribution update data %@",[distributionItem description]);
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:NSLocalizedString(@"Available", nil)];
            
            if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"Y"]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
        else if(indexPath.row==1)
        {
            [cell.textLabel setText:@"Unavailable"];
            if ([[distributionItem objectForKey:@"Avl"] isEqualToString:@"N"]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
        }
    }
    else if (indexPath.section==1)
    {
        if (indexPath.row == 0) {
            cell=nil;
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Quantity", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell) setKey:@"Qty"];
            [((SWTextFieldCell *)cell).textField setText:[distributionItem stringForKey:@"Qty"]];
            [((SWTextFieldCell *)cell).textField setDelegate:self];
        } else if (indexPath.row == 1) {
            
            [SWDefaults setPaymentType:@"NO"];
            cell=nil;
            cell = [[SWDateFieldCell alloc] initWithReuseIdentifier:@"Date"] ;
            
            [((SWDateFieldCell *)cell).titleLabel setText:NSLocalizedString(@"Expiry Date", nil)];
            [((SWDateFieldCell *)cell) setKey:@"ExpDate"];
            [((SWDateFieldCell *)cell) setDelegate:self];
            [((SWDateFieldCell *)cell) setDate:[distributionItem objectForKey:@"ExpDate"]];
        }
        else if (indexPath.row==2)
        {
            //update the custom attribute_1 text from tbl_Distribution check
            cell=nil;
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
            [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Shelf Price", nil)];
            [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeNumberPad];
            [((SWTextFieldCell *)cell).textField setTag:100];
            [((SWDateFieldCell *)cell) setKey:@"ShelfPrice"];
            
            [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Enter shelf price", nil)];
            [((SWTextFieldCell *)cell) setDelegate:self];
            [((SWTextFieldCell *)cell).textField setText:[distributionItem stringForKey:@"ShelfPrice"]];
            [((SWTextFieldCell *)cell).textField setDelegate:self];
        }
    }
    return cell;
}

#pragma mark UITable View delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [distributionItem setValue:@"Y" forKey:@"Avl"];
           // [distributionItem setValue:[NSDate date] forKey:@"ExpDate"];
        } else if (indexPath.section == 0 && indexPath.row == 1) {
            [distributionItem setValue:@"N" forKey:@"Avl"];
            [distributionItem setValue:@"" forKey:@"Qty"];
            [distributionItem setValue:@"" forKey:@"ExpDate"];
            [distributionItem setValue:@"" forKey:@"ShelfPrice"];
        }

        [self.tableView reloadData];
        [self setPreferredContentSize:self.tableView.contentSize];
    }
}

#pragma mark EditCell Delegate
- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    [self performSelector:@selector(scroll:) withObject:cell afterDelay:0.4f];
}

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    [distributionItem setValue:value forKey:key];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate methods


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"check qty text field text  %@", textField.text);
    
    if (textField.tag==100) {
        [distributionItem setValue:textField.text forKey:@"ShelfPrice"];
        NSLog(@"shelf price set %@", [distributionItem description]);
        [textField resignFirstResponder];
    }
    else {
        [distributionItem setValue:textField.text forKey:@"Qty"];
    }
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *allowedCharacters;
    if ([textField.text length]>0)//no decimal point yet, hence allow point
        allowedCharacters = @"0123456789.";
    else
        allowedCharacters = @"123456789";//first input character cannot be '0'
    
    if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
    {
        return NO;
    }
    return YES;
}

@end
