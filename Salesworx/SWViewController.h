//
//  SWViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTPBaseViewController.h"

@interface SWViewController : FTPBaseViewController

- (void)changeUISegmentFont:(UIView *)aView;
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)setupToolbar:(NSArray *)buttons;

@end