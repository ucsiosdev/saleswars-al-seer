//
//  SalesOrderNewViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 11/19/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SalesOrderNewViewController.h"
#import <QuartzCore/QuartzCore.h>

#define NUMERIC                 @"1234567890"

@interface SalesOrderNewViewController ()

@end

@implementation SalesOrderNewViewController
@synthesize parentViewController,customerDict,preOrdered,preOrderedItems,isOrderConfirmed,wareHouseDataSalesOrderArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title= @"Sales Order";
    
    //table view crash animation reported by client visit fixed
    productTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.view.backgroundColor = [UIColor whiteColor ];
    isPinchDone = NO;
    if ([self.parentViewString isEqualToString:@"SO"])
    {
        
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andCategory:[SWDefaults productCategory]] ;
    }
    else if ([self.parentViewString isEqualToString:@"MO"]) {
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andOrders:self.preOrdered andManage:self.isOrderConfirmed];
        
        
       // oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
        
        NSLog(@"ware house data in old sales order %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
        
        //OLA!!
        if ([self.isOrderConfirmed isEqualToString:@"confirmed"])
        {
            [addButton setHidden:YES];
            [txtDefBonus setUserInteractionEnabled:NO];
            [txtDiscount setUserInteractionEnabled:NO];
            [txtfldWholesalePrice setUserInteractionEnabled:NO];
            [txtFOCQty setUserInteractionEnabled:NO];
            [txtNotes setUserInteractionEnabled:NO];
            [txtProductQty setUserInteractionEnabled:NO];
            [txtRecBonus setUserInteractionEnabled:NO];
            [focProductBtn setUserInteractionEnabled:NO];
        }
    }
    else if ([self.parentViewString isEqualToString:@"TO"]) {
        oldSalesOrderVC = [[SalesOrderViewController alloc] initWithCustomer:self.customerDict andTemplate:self.preOrdered andTemplateItem:self.preOrderedItems] ;
    }
    oldSalesOrderVC.view.frame = bottomOrderView.bounds;
    oldSalesOrderVC.view.clipsToBounds = YES;
    
    
    // moving ware house data to old sales order
    
    NSLog(@"data before moving %@", [wareHouseDataSalesOrderArray description]);
    oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
    
    NSLog(@"data after moving %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
    
    
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];
    
    
    productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    
    NSLog(@"section headings %@", [finalProductArray description]);
    
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    [self borderView:bonusView];
    [self borderView:stockView];
    [self borderView:salesHistoryView];
    [self borderView:dragParentView];
    // [self borderView:dragButton];
    
    //changed to [customerDict stringForKey:@"Customer_Name"]
    //    NSLog(@"OLA!!******customer name %@",[customerDict stringForKey:@"Customer_Name"]);
    //    NSLog(@"OLA!!******customer name %@",[[SWDefaults customer] stringForKey:@"Customer_Name"]);
    lblCustomerLabel.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerLabel.font = RegularFontOfSize(26);
    lblProductName.font = BoldSemiFontOfSize(16);
    lblProductCode.font =BoldSemiFontOfSize(12);
    lblProductBrand.font=BoldSemiFontOfSize(12);
    lblWholesalePrice.font=BoldSemiFontOfSize(12);
    txtfldWholesalePrice.font=BoldSemiFontOfSize(12);
    lblRetailPrice.font=BoldSemiFontOfSize(12);
    lblUOMCode.font=BoldSemiFontOfSize(12);
    lblAvlStock.font=BoldSemiFontOfSize(12);
    lblExpDate.font=BoldSemiFontOfSize(12);
    lblPriceLabel.font=BoldSemiFontOfSize(12);
    
    //    txtProductQty.font=LightFontOfSize(14);
    //    txtDefBonus.font=LightFontOfSize(14);
    //    txtRecBonus.font=LightFontOfSize(14);
    //    txtDiscount.font=LightFontOfSize(14);
    //    txtFOCQty.font=LightFontOfSize(14);
    //    txtNotes.font=LightFontOfSize(14);
    
    lblCustomerLabel.textColor = UIColorFromRGB(0x4A5866);
    lblProductName.textColor = UIColorFromRGB(0x4687281);
    lblProductCode.textColor =UIColorFromRGB(0x4687281);
    lblProductBrand.textColor=UIColorFromRGB(0x4687281);
    lblWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    txtfldWholesalePrice.textColor=UIColorFromRGB(0x4687281);
    lblRetailPrice.textColor=UIColorFromRGB(0x4687281);
    lblUOMCode.textColor=UIColorFromRGB(0x4687281);
    lblAvlStock.textColor=UIColorFromRGB(0x4687281);
    lblExpDate.textColor=UIColorFromRGB(0x4687281);
    lblPriceLabel.textColor=UIColorFromRGB(0x4687281);
    
    txtProductQty.textColor=UIColorFromRGB(0x4687281);
    txtDefBonus.textColor=UIColorFromRGB(0x4687281);
    txtRecBonus.textColor=UIColorFromRGB(0x4687281);
    txtDiscount.textColor=UIColorFromRGB(0x4687281);
    txtFOCQty.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor=UIColorFromRGB(0x4687281);
    txtNotes.textColor = [UIColor darkTextColor ];
    
    
    candySearchBar.backgroundColor = UIColorFromRGB(0xF6F7FB);
    
    lblTitleProductCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleWholesalePrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleRetailPrice.textColor=UIColorFromRGB(0x4A5866);
    lblTitleUOMCode.textColor=UIColorFromRGB(0x4A5866);
    lblTitleAvlStock.textColor=UIColorFromRGB(0x4A5866);
    lblTitleExpDate.textColor=UIColorFromRGB(0x4A5866);
    txtTitleProductQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDefBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleRecBonus.textColor=UIColorFromRGB(0x4A5866);
    txtTitleDiscount.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCQty.textColor=UIColorFromRGB(0x4A5866);
    txtTitleFOCItem.textColor=UIColorFromRGB(0x4A5866);
    txtTitleNotes.textColor=UIColorFromRGB(0x4A5866);
    
    lblTitleProductCode.font=LightFontOfSize(14);
    lblTitleWholesalePrice.font=LightFontOfSize(14);
    lblTitleRetailPrice.font=LightFontOfSize(14);
    lblTitleUOMCode.font=LightFontOfSize(14);
    lblTitleAvlStock.font=LightFontOfSize(14);
    lblTitleExpDate.font=LightFontOfSize(14);
    txtTitleProductQty.font=LightFontOfSize(14);
    txtTitleDefBonus.font=LightFontOfSize(14);
    txtTitleRecBonus.font=LightFontOfSize(14);
    txtTitleDiscount.font=LightFontOfSize(14);
    txtTitleFOCQty.font=LightFontOfSize(14);
    txtTitleFOCItem.font=LightFontOfSize(14);
    txtTitleNotes.font=LightFontOfSize(14);
    
    lblSelectProduct.font=BoldSemiFontOfSize(20);
    
    dragParentView.backgroundColor = UIColorFromRGB(0x343F5F);
    focProductBtn.titleLabel.font =BoldSemiFontOfSize(14);
    
    [txtNotes.layer setCornerRadius:7.0f];
    [txtNotes.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [txtNotes.layer setBorderWidth:1.0f];
    
    //    [txtfldWholesalePrice setPlaceholder:[NSString stringWithFormat:@"(in %@)",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]]];
    [txtfldWholesalePrice setDelegate:self];
    [txtfldWholesalePrice setKeyboardType:UIKeyboardTypeDecimalPad];
    [txtfldWholesalePrice setHidden:YES];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)] ];

    oldSalesOrderVC.gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    if (@available(iOS 15.0, *)) {
        productTableView.sectionHeaderTopPadding = 0;
        focProductTableView.sectionHeaderTopPadding = 0;
        oldSalesOrderVC.gridView.tableView.sectionHeaderTopPadding = 0;
    }
}

- (void)closeVisit:(id)sender {
    
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        
    }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Alert", nil) andMessage:NSLocalizedString(@"Would you like to close this order?", nil) andActions:actionsArray withController:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"ware house data in sales order %@", [wareHouseDataSalesOrderArray description]);
    
    OrderAdditionalInfoViewController * orderVC=[[OrderAdditionalInfoViewController alloc]init];
    orderVC.warehouseDataArray=wareHouseDataSalesOrderArray ;
    
    NSLog(@"ware house data being pushed to final destination %@", [orderVC.warehouseDataArray description]);
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate = self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    oldSalesOrderVC.delegate = nil;
    txtfldWholesalePrice.delegate=nil;
    productStockViewController.delegate=nil;
}

-(void)borderView:(UIView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
    [view.layer setBorderColor:UIColorFromRGB(0xE6E6E6).CGColor];
    [view.layer setBorderWidth:2.0f];
}

#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            return 1;
        }
        else
        {
            return [finalProductArray count];
        }
    }
    else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return [filteredCandyArray count];
        } else {
            NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
            return [_collapsedSections containsObject:@(section)] ? [tempDict count] :0 ;
        }
    }
    else {
        return [productArray count];
    }
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else {
            return 40.0;
        }
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=BoldSemiFontOfSize(16);
            [result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            
            return headerView;
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines=2;
        cell.textLabel.font=RegularFontOfSize(5);
        cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
        cell.textLabel.textColor=UIColorFromRGB(0x687281);
        cell.textLabel.textColor=[UIColor darkTextColor ];
        cell.textLabel.font  =  LightFontOfSize(14) ;
    }

    
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [row stringForKey:@"Description"];
        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            cell.textLabel.textColor = [UIColor redColor];
        }
        else
        {
            cell.textLabel.textColor=[UIColor darkTextColor ];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    if (tableView.tag==1)
    {
        
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtRecBonus.text=@"";
        txtDiscount.text=@"";
        txtFOCQty.text=@"";
        txtNotes.text=@"";
        [addButton setTitle:@"Add" forState:UIControlStateNormal];
        
        NSDictionary * row ;
        if (bSearchIsOn)
        {
            row = [filteredCandyArray objectAtIndex:indexPath.row];
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            row= [objectsForCountry objectAtIndex:indexPath.row];
        }
        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
        mainProductDict=nil;
        
        
        if (produstDetail.count>0) {
            
            
            
            mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
            NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
            [mainProductDict setValue:itemID forKey:@"ItemID"];
            
            isDidSelectStock=YES;
            [stockBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
            [stockBtn setSelected:YES];
            
            if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
                [lblWholesalePrice setHidden:YES];
                [txtfldWholesalePrice setHidden:NO];
            }
            else {
                [lblWholesalePrice setHidden:NO];
                [txtfldWholesalePrice setHidden:YES];
            }
            
            [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
            [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
        }
        else
        {
            NSDictionary * row = [productArray objectAtIndex:indexPath.row];
            if (![row objectForKey:@"Guid"])
            {
                [row setValue:[NSString createGuid] forKey:@"Guid"];
            }
            if([fBonusProduct objectForKey:@"Qty"])
            {
                [row setValue:[fBonusProduct objectForKey:@"Qty"] forKey:@"Qty"];
            }
            fBonusProduct=[row mutableCopy];
            [focProductBtn setTitle:[row stringForKey:@"Description"] forState:UIControlStateNormal];
            [self buttonAction:focProductBtn];
        }
    }
}
#pragma mark -

- (void)selectedProduct:(NSMutableDictionary *)product
{
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    mainProductDict=nil;
    mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
    txtfldWholesalePrice.text = @"";
    
    if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
        [lblWholesalePrice setHidden:YES];
        [txtfldWholesalePrice setHidden:NO];
    }
    else {
        [lblWholesalePrice setHidden:NO];
        [txtfldWholesalePrice setHidden:YES];
    }
    
    
    if ([mainProductDict objectForKey:@"Bonus_Product"]) {
        bonusProduct=[mainProductDict objectForKey:@"Bonus_Product"];
    }
    
    if ([mainProductDict objectForKey:@"FOC_Product"]) {
        fBonusProduct=[mainProductDict objectForKey:@"FOC_Product"];
        
    }
    NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
    [mainProductDict setValue:itemID forKey:@"ItemID"];
    
    
    NSString* updatedWholesalePrice;
    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        NSLog(@"check is order confirmed %@", self.isOrderConfirmed);
        NSLog(@"check net price in confrimed order %@", [mainProductDict valueForKey:@"Unit_Selling_Price"]);
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Unit_Selling_Price"];
    }
    else {
        updatedWholesalePrice= [mainProductDict valueForKey:@"Net_Price"] ;
    }
    
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    NSLog(@"quantity text is %@",txtProductQty.text);
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    txtDiscount.text=[mainProductDict stringForKey:@"Discount"];
    txtFOCQty.text=[fBonusProduct stringForKey:@"Qty"];
    txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
        NSLog(@"updated wholase price case 2 %@", updatedWholesalePrice);
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,[updatedWholesalePrice doubleValue]]];
    }
    [addButton setTitle:@"Update" forState:UIControlStateNormal];
}

- (void) dbGetSelectedProductServiceDidGetCheckedPrice:(NSArray *)priceDetail
{
    NSArray *stockInfo = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]];
    
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Lot_Qty"] forKey:@"Lot_Qty"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Expiry_Date"] forKey:@"Expiry_Date"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"UOM"] forKey:@"UOM"];
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
                    [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [txtfldWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
    [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    
    lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
}

- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Unit_Selling_Price"];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    
    NSString* currencyCode=[[SWDefaults userProfile]valueForKey:@"Currency_Code"];
    [txtfldWholesalePrice setText:[ NSString stringWithFormat:@"%@%@",currencyCode,[mainProductDict stringForKey:@"Net_Price"]]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@"N/A"];
    }
    else
    {
        [lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }
}

- (void)stockAllocated:(NSMutableDictionary *)p {
    mainProductDict = nil;
    mainProductDict=[NSMutableDictionary dictionaryWithDictionary:p];
}

-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    
    
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        NSInteger collapedSectionsCount=_collapsedSections.count;

        
        for (NSInteger i=0; i<collapedSectionsCount; i++) {
            
            NSArray *myArray = [_collapsedSections allObjects];
            
            NSInteger sectionVal=[[myArray objectAtIndex:i] integerValue];
            int numOfRows = [productTableView numberOfRowsInSection:sectionVal];
            NSArray* indexPaths = [self indexPathsForSection:sectionVal withNumberOfRows:numOfRows];
            [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [_collapsedSections removeObject:@(sectionVal)];
        }
        
        NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
        int numOfRows = [tempDict count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
}

#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [candySearchBar resignFirstResponder];
    NSUInteger len = [ [candySearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = candySearchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearch = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearch allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each isDualBonusAppControlsection array
            for (NSString *key in [sectionSearch allKeys])
            {
                [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            [productTableView reloadData];
        }
        
    }
    else
    {
        [ candySearchBar resignFirstResponder ];
    }
}

#pragma mark - Button Action
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info{
    isBonusRowDeleted=NO;
    bonusInfo=nil;
    bonusInfo=[NSArray arrayWithArray:info];
    AppControl *appControl = [AppControl retrieveSingleton];
    isBonusAppControl = appControl.ALLOW_BONUS_CHANGE;
    isFOCAppControl = appControl.ENABLE_MANUAL_FOC;
    isDiscountAppControl = appControl.ALLOW_DISCOUNT;
    isDualBonusAppControl = appControl.ALLOW_DUAL_FOC;
    isFOCItemAppControl=appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE;
    
    if([isFOCAppControl isEqualToString:@"Y"])
    {
        isFOCAllow=YES;
        checkFOC=YES;
    }
    else
    {
        if ([isDualBonusAppControl isEqualToString:@"N"])
        {
            isFOCAllow=NO;
            checkFOC=NO;
        }
    }
    
    if([isDiscountAppControl isEqualToString:@"Y"])
    {
        isDiscountAllow=YES;
        checkDiscount = YES;
    }
    else
    {
        isDiscountAllow=YES;
        checkDiscount = NO;
    }
    
    if([isFOCItemAppControl isEqualToString:@"Y"])
    {
        isFOCItemAllow=YES;
    }
    else
    {
        isFOCItemAllow=NO;
    }
    
    if([isDualBonusAppControl isEqualToString:@"Y"])
    {
        isDualBonusAllow=YES;
    }
    else
    {
        isDualBonusAllow=NO;
    }
    
    if (bonusInfo.count > 0)
    {
        isBonus = YES;
        if([isBonusAppControl isEqualToString:@"Y"] || [isBonusAppControl isEqualToString:@"I"] || [isBonusAppControl isEqualToString:@"D"])
        {
            isBonusAllow = YES;
            checkBonus = YES;
        }
        else
        {
            isBonusAllow = NO;
            checkBonus = NO;
        }
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        isBonus = NO;
        bonus=0;
        isBonusAllow = NO;
        checkBonus = NO;
    }
    bonusEdit = @"N";
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    if([mainProductDict objectForKey:@"FOC_Product"])
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"FOC_Product"]];
    }
    else
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:mainProductDict];
        [fBonusProduct removeObjectForKey:@"Qty"];
        [fBonusProduct removeObjectForKey:@"Bonus_Product"];
        [fBonusProduct removeObjectForKey:@"StockAllocation"];
    }
    
    if([mainProductDict objectForKey:@"Bonus_Product"])
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"Bonus_Product"]];
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[mainProductDict stringForKey:@"Item_Code"]]];
        [bonusProduct removeObjectForKey:@"Qty"];
        [bonusProduct removeObjectForKey:@"FOC_Product"];
        [bonusProduct removeObjectForKey:@"StockAllocation"];
    }
    [focProductBtn setTitle:[fBonusProduct stringForKey:@"Description"] forState:UIControlStateNormal];
    
    //focProductBtn.titleLabel.text = [fBonusProduct stringForKey:@"Description"];
    //[tableView reloadData];
    info=nil;
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0)
        {
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
    }
}

- (void)calculateBonus {
    
    int quantity = 0;
    bonus = 0;
    NSDictionary *tempBonus;
    BOOL isBonusGroup = YES;
    if ([[AppControl retrieveSingleton].ENABLE_BONUS_GROUPS isEqualToString:@"Y"])
    {
        NSString *query = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Promotion AS A INNER JOIN TBL_Customer_Addl_Info As B ON B.Attrib_Value=A.BNS_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",[customerDict stringForKey:@"Customer_ID"],[customerDict stringForKey:@"Site_Use_ID"]];
        if ([[SWDatabaseManager retrieveManager] fetchDataForQuery:query].count==0)
        {
            isBonusGroup = NO;
        }
        else
        {
            isBonusGroup = YES;
        }
    }
    else
    {
        isBonusGroup = YES;
    }
    
    if (isBonusGroup) {
        if ([mainProductDict objectForKey:@"Qty"])
        {
            quantity = [[mainProductDict objectForKey:@"Qty"] intValue];
            for(int i=0 ; i<[bonusInfo count] ; i++ )
            {
                NSDictionary *row = [bonusInfo objectAtIndex:i];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    
                    if(checkBonus)
                    {
                        isBonusAllow = YES;
                    }
                    if ([[AppControl retrieveSingleton].BONUS_MODE isEqualToString:@"DEFAULT"])
                    {
                        if(i == [bonusInfo count]-1)
                        {
                            if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                            {
                                int dividedValue = quantity / rangeStart ;
                                bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                                tempBonus=row;
                            }
                            
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                            {
                                int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                                bonus = floor(dividedValue) ;
                                tempBonus=row;
                                
                            }
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"POINT"])
                            {
                                bonus = [[row objectForKey:@"Get_Qty"] intValue];
                                tempBonus=row;
                                
                            }
                        }
                        else
                        {
                            bonus = [[row objectForKey:@"Get_Qty"] intValue];
                            tempBonus=row;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue] + floorf(([[mainProductDict objectForKey:@"Qty"] intValue] - rangeStart) *[[row objectForKey:@"Get_Add_Per"] floatValue]);
                        tempBonus=row;
                    }
                    break;
                }
                else
                {
                    if(checkBonus)
                    {
                        isBonusAllow = NO;
                    }
                }
            }
        }
        
        if (bonus > 0 && bonusInfo)
        {
            if(![[tempBonus stringForKey:@"Get_Item"] isEqualToString:[bonusProduct stringForKey:@"Item_Code"]])
            {
                NSArray *temBonusArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:[NSString stringWithFormat:@"select * from TBL_Product where Item_Code ='%@'",[tempBonus stringForKey:@"Get_Item"]]];
                bonusProduct=nil;
                bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[temBonusArray objectAtIndex:0]];
                [bonusProduct removeObjectForKey:@"Qty"];
                [bonusProduct removeObjectForKey:@"FOC_Product"];
                [bonusProduct removeObjectForKey:@"StockAllocation"];
            }
            
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Def_Qty"];
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Qty"];
            
            if(!isDualBonusAllow)
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct removeObjectForKey:@"Def_Qty"];
                
            }
            if(isDiscountAllow)
            {
                [mainProductDict setValue:@"0" forKey:@"Discount"];
            }
        }
        else
        {
            [bonusProduct removeObjectForKey:@"Qty"];
            [bonusProduct removeObjectForKey:@"Def_Qty"];
            
        }
        
    }
    
}

- (NSString*)stringByKeepingOnlyCharactersInString:(NSString *)string {
    #define ACCEPTABLE_CHARACTERS @"0123456789."
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];

    
    return filtered;
    
}
- (void)updatePrice{
    
    //NSLog(@"OLA!!******* mainProduct at begin updatePrice %@",mainProductDict);
    
    if ([mainProductDict stringForKey:@"Qty"].length > 0)
    {
        int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            
            NSMutableString *wholesalePrice;
            
            
            if (txtfldWholesalePrice.text) {
                if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""]){
                    
                    
                    //unit price issue 1,000 to 1 fixed  by syed (Comma issue)
                    NSString * stringValue = txtfldWholesalePrice.text;
                    NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    
                    
                    
                    // All the characters to remove
                    NSMutableCharacterSet *characterSet = [NSMutableCharacterSet letterCharacterSet];
                    [characterSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    // Build array of components using specified characters as separtors
                    NSArray *arrayOfComponents = [stringValue1 componentsSeparatedByCharactersInSet:characterSet];
                    
                    // Create string from the array components
                    NSString *strOutput = [arrayOfComponents componentsJoinedByString:@""];
                    
                    NSLog(@"New string: %@", strOutput);
//
                    
                    
                    
                    
                    
                    
                    
                    NSString *filtered = [self stringByKeepingOnlyCharactersInString:stringValue1];

                    
                    
                    
                    
                    
//                    NSString *newString= [self stringByKeepingOnlyCharactersInString:stringValue1];
                    
                    
                    NSLog(@"updated new string is %@", filtered);
                    
                    
                    wholesalePrice=[NSMutableString stringWithFormat:@"%@", filtered];
                    
                   // wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
                }
                else
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            }
            else
                wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            
            
            NSLog(@"updated price in case 1 %@", [mainProductDict stringForKey:@"Net_Price"]);
            
            
            
            float totalPrice = (double)qty * ([wholesalePrice floatValue]);
            
            NSLog(@"total val %f",[wholesalePrice doubleValue]);
            //            NSLog(@"Price %f",[[mainProductDict stringForKey:@"Net_Price"] doubleValue]);
            //            NSLog(@"Price %f",(double)qty);
            //            NSLog(@"Price %f",totalPrice);
            if(bonus<=0 || !checkDiscount)
            {
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                
                if ([self.parentViewString isEqualToString:@"MO"]) {
                    
                    
//                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),[[mainProductDict valueForKey:@"Discounted_Price"] floatValue] ,NSLocalizedString(@"Discounted Amount", nil), [[mainProductDict valueForKey:@"Discounted_Amount"] floatValue]]];
                    
                    
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Net_Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                }
                

                else
                {
                
                
                
                
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                    
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
            }
            }
            
            else
            {
//                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
//                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Discounted_Price"];
//                
//                //why si discount hardcoded as 0.0?
//                
//                [mainProductDict setValue:@"0.00" forKey:@"Discounted_Amount"];
//                
//                [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
                
                
                
                
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                
                
                NSLog(@"check discount amount in main product dict %@", [mainProductDict valueForKey:@"Discounted_Amount"]);
                
                
                
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                
                
            }
        }
        else{
            [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
            
        }
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [lblPriceLabel setText:@""];
        //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    
    //NSLog(@"OLA!!******* mainProduct at end updatePrice %@",mainProductDict);
}

-(void)saveAsTemplateAction:(id)sender
{
    NSLog(@"Save As Template");
}
-(IBAction)resetAction:(id)sender
{
    lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    mainProductDict = nil;
    bonusProduct = nil;
    fBonusProduct = nil;
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
}



-(void)ResetData
{
}


-(IBAction)saveOrderAction:(id)sender
{
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    
    if ([self.isOrderConfirmed isEqualToString:@"confirmed"]) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Confirmed orders can't be edited." withController:self];
        return;
    }
    
    [txtProductQty resignFirstResponder];
    [txtDefBonus resignFirstResponder];
    [txtRecBonus resignFirstResponder];
    [txtDiscount resignFirstResponder];
    [txtFOCQty resignFirstResponder];
    [txtNotes resignFirstResponder];
    
    if (![txtProductQty.text isEqualToString:@""]) {
        
        [txtfldWholesalePrice resignFirstResponder];

        [self saveProductOrder];
        
        [oldSalesOrderVC productAdded:mainProductDict];
        mainProductDict=nil;
        fBonusProduct= nil;
        bonusProduct = nil;
        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
        
        [productTableView deselectRowAtIndexPath:[productTableView indexPathForSelectedRow] animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please enter quantity." withController:self];
    }
}

- (void)saveProductOrder {
    
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    
    isSaveOrder = YES;
    int totalBonus = 0 ;
    [mainProductDict removeObjectForKey:@"FOC_Product"];
    [mainProductDict removeObjectForKey:@"Bonus_Product"];
    
    [mainProductDict setValue:bonusProduct forKey:@"Bonus_Product"];
    [mainProductDict setValue:fBonusProduct forKey:@"FOC_Product"];
    
    BOOL isBothBonus = NO;
    if([bonusProduct objectForKey:@"Qty"])
    {
        totalBonus = [[bonusProduct stringForKey:@"Qty"] intValue];
        if(!isDualBonusAllow)
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
        else if([fBonusProduct objectForKey:@"Qty"])
        {
            isBothBonus = YES;
            totalBonus = totalBonus + [[fBonusProduct stringForKey:@"Qty"] intValue];
        }
        else
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
    }
    else
    {
        [mainProductDict removeObjectForKey:@"Bonus_Product"];
    }
    
    if([fBonusProduct objectForKey:@"Qty"] && !isBothBonus)
    {
        totalBonus = [[fBonusProduct stringForKey:@"Qty"] intValue];
    }
    else if(!isBothBonus)
    {
        [mainProductDict removeObjectForKey:@"FOC_Product"];
    }
    
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%d",totalBonus] forKey:@"Def_Bonus"];
    [mainProductDict setValue:[mainProductDict stringForKey:@"Discount"] forKey:@"DiscountPercent"];
    
    if(![mainProductDict objectForKey:@"StockAllocation"])
    {
        // [mainProductDict setValue:stockItems forKey:@"StockAllocation"];
    }
    else
    {
    }
    //NSLog(@"Product %@",mainProductDict);
    //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    //[self.navigationController  popViewControllerAnimated:YES];
    
    
    if(txtfldWholesalePrice.text)
    {
        
        NSMutableString *wholesalePrice;
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""])
        {
            
             NSLog(@"OLA!!***** WP 2 %@",txtfldWholesalePrice.text );
            
            //unit price fix 1,000 to 1 fixed by syed (comma issue)
            NSString * stringValue = txtfldWholesalePrice.text;
            NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            
            NSLog(@"wholesale price before string removal is %@", stringValue1);

            
            
            
            //this guy has used substringfrom index 3 if its AED11250.00 it shows 11245, if its $11250 it shows 250 so changing the logic here to check if string has alphabets then remove them so that it wont effect for currency signs like $
            
            
            
            NSRegularExpression *regex = [[NSRegularExpression alloc]
                                           initWithPattern:@"[a-zA-Z]" options:0 error:NULL];
            
            // Assuming you have some NSString `myString`.
            NSUInteger matches = [regex numberOfMatchesInString:stringValue1 options:0
                                                          range:NSMakeRange(0, [stringValue1 length])];
            
            if (matches > 0) {
                // `myString` contains at least one English letter.
                
                
            }
            else
            {
                //this string dones not have alphabets in prefix ex AED, AQ etc
                
                
                
            }
            
            
            
            //removing alphabets from price
            
//            
//            NSString *newString = [[stringValue1 componentsSeparatedByCharactersInSet:
//                                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                                   componentsJoinedByString:@""];
//            
//            
//            
//            
////            wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
//            
//
//            
//            NSLog(@"wholesale price after string removal is %@", wholesalePrice);
//            
            
            
            
            NSString *filtered = [self stringByKeepingOnlyCharactersInString:stringValue1];

            
            wholesalePrice=[NSMutableString stringWithFormat:@"%@", filtered];

            
            
            //NSString *wholesalePrice = [txtfldWholesalePrice.text substringFromIndex:3];
            [mainProductDict setObject:wholesalePrice forKey:@"Net_Price"];
        }
        
    }
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
}

-(IBAction)buttonAction:(id)sender
{
    [self.view endEditing:YES];
    
    //    if (mainProductDict==nil) {
    //
    //
    //    ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
    //    [stockViewController setDelegate:self];
    //    [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
    //    [stockViewController setTarget:self];
    //    [stockViewController setAction:@selector(stockAllocated:)];
    //
    //    UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
    //    modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    //    [self presentViewController:modalViewNavController animated:YES completion:nil];
    //
    //    }
    
    
    
    int tag = ((UIButton *)sender).tag;
    switch (tag)
    {
        case 1:
            if (isPproductDropDownToggled)
            {
                isPproductDropDownToggled=NO;
                focProductTableView.hidden=YES;
                
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,0) withDuration:0.2];
            }
            else
            {
                isPproductDropDownToggled=YES;
                focProductTableView.hidden=NO;
                productDropDown.frame=CGRectMake(599, 133, 241, 340);

                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,383) withDuration:0.2];
            }
            break;
        case 11:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"Stock");
                //productStockViewController=nil;
                ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
                [stockViewController setDelegate:self];
                [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
                [stockViewController setTarget:self];
                [stockViewController setAction:@selector(stockAllocated:)];
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 12:
        {
            if (mainProductDict.count!=0)
            {
                ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:bonusViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 13:
        {
            if (mainProductDict.count!=0)
            {
                CustomerSalesViewController *salesHistory = [[CustomerSalesViewController alloc] initWithCustomer:customerDict andProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:salesHistory];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 5:
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)textViewDidEndEditing:(UITextView *)textView{
    //if ([key isEqualToString:@"lineItemNotes"])
    if(textView==txtNotes)
    {
        [mainProductDict setValue:textView.text forKey:@"lineItemNotes"];
        txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
        isErrorSelectProduct = YES;
        [self performSelector:@selector(resignedTextField:) withObject:textField afterDelay:0.0];
    }
    
    if (textField==txtDiscount) {
        textField.text=@"";
    }
    
    return YES;
}
-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSLog(@"main product dic desc %@", [mainProductDict description]);
    
    [mainProductDict removeObjectForKey:@"StockAllocation"];
    
    NSLog(@"removed %@",[mainProductDict description] );
    
    [textField resignFirstResponder];
    NSCharacterSet *unacceptedInput = nil;
    NSString *zeroString=@"0";
    
    // textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if (textField==txtFOCQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            if ([textField.text length]!=0)
            {
                [fBonusProduct setValue:textField.text forKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
            else
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
            [fBonusProduct removeObjectForKey:@"Qty"];
            [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
        }
    }
    if (textField==txtRecBonus)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [bonusProduct setValue:textField.text forKey:@"Qty"];
            if([isBonusAppControl isEqualToString:@"Y"])
            {
                bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                bonusEdit = @"Y";
            }
            else if([isBonusAppControl isEqualToString:@"I"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] >= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can increase only." withController:self];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            else if([isBonusAppControl isEqualToString:@"D"])
            {
                if([[bonusProduct stringForKey:@"Qty"] intValue] <= bonus)
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else
                {
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can decrease only." withController:self];
                    [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                }
            }
            
            if (bonus==0)
            {
                [bonusProduct removeObjectForKey:@"Qty"];
            }
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
            [bonusProduct removeObjectForKey:@"Qty"];
        }
    }
    if (textField==txtDiscount)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            if ([textField.text intValue]>100) {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Discount (%) should not be more than 100%" withController:self];
                [mainProductDict removeObjectForKey:@"Discount"];
            }
            else
            {
                [mainProductDict setValue:textField.text forKey:@"Discount"];
            }
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Discount (%) value should have only numeric" withController:self];
            [mainProductDict removeObjectForKey:@"Discount"];
        }
    }
    if (textField==txtProductQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            
            [mainProductDict setValue:textField.text forKey:@"Qty"];
            if (bonusInfo.count > 0 || ![[mainProductDict stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if([bonusEdit isEqualToString:@"N"])
                {
                    [self calculateBonus];
                }
                else
                {
                    [self calculateBonus];
                    
                    bonusEdit = @"N";
                }
            }
        }
        else
        {
            if (!isErrorSelectProduct) {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Quantity value should have only numeric" withController:self];
            }
            isErrorSelectProduct=NO;
            [mainProductDict setValue:@"" forKey:@"Qty"];
        }
    }
    
    
    
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    
    
    
    
    //Bonus
    if(![fBonusProduct objectForKey:@"Qty"]  || isDualBonusAllow)
    {
        txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    }
    
    
    //OLA!!
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0){
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
        
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
        
        
    }
    
    
    //MFOC
    if(![bonusProduct objectForKey:@"Qty"] || isDualBonusAllow)
    {
        [txtFOCQty setText:[fBonusProduct stringForKey:@"Qty"]];
    }
    //[((SWTextFieldCell *)cell).secondLabel setText:[fBonusProduct stringForKey:@"Description"]];
    
    if(isFOCAllow)//APP CONTROL
    {
        [txtFOCQty setUserInteractionEnabled:YES];
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        [txtFOCQty setUserInteractionEnabled:NO];
        focProductBtn.userInteractionEnabled=NO;
    }
    
    if(isFOCItemAllow)
    {
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        focProductBtn.userInteractionEnabled=YES;
    }
    else
    {
        focProductBtn.userInteractionEnabled=NO;
    }
    //Discount
    NSString *discountString = [mainProductDict stringForKey:@"Discount"];
    
    [txtDiscount setText: discountString];
    
    if([isDiscountAppControl isEqualToString:@"Y"])//APP CONTROL
    {
        [txtDiscount setUserInteractionEnabled:YES];
    }
    else
    {
        [txtDiscount setUserInteractionEnabled:NO];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"OLA!!***** WP %@",txtfldWholesalePrice.text );
    
    [textField resignFirstResponder];
    return YES;
}


//restrictiong text view notes to 50 characters

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    else if([[textView text] length] > 49 )
    {
        return NO;
    }
    return YES;
    //return textView.text.length + (text.length - range.length) <= 140;
}


//restrict characters here
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"You have entered wrong input" withController:self];
        return NO;
    }
    
    if([textField.text hasPrefix:@"0"])
    {
        textField.text=@"";
    }
    
    if ([textField isEqual:txtRecBonus]) {
        if ([txtProductQty.text isEqualToString:@""] || [txtProductQty.text isEqual:[NSNull null]]) {
            return NO;
        }
    }
    
    if ([textField isEqual:txtProductQty]) {
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
            // Game on: when you return YES from this, your field will be empty
            [txtRecBonus setText:@""];
            [txtDefBonus setText:@""];
            [txtRecBonus setUserInteractionEnabled:NO];
        }
    }
    
    if ([textField isEqual:txtfldWholesalePrice]) {
      
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"resulting string would be: %@", resultString);
        NSString *prefixString = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
        NSRange prefixStringRange = [resultString rangeOfString:prefixString];
        
        if (prefixStringRange.location == 0) {
            // prefix found at the beginning of result string
            
            NSString *allowedCharacters;
            if ([textField.text componentsSeparatedByString:@"."].count >= 2)//no decimal point yet, hence allow point
                allowedCharacters = @"01234567890,.";
            else
                allowedCharacters = @"0123456789.,";
            
            if ([[string stringByReplacingOccurrencesOfString:prefixString withString:@""] rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
            {
                return NO;
            }
        }
        else
            return NO;
    }
    else if ([textField isEqual:txtDiscount])
    {
        NSString *allowedCharacters = @"0123456789";
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
    }
    return YES;
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        
        if (!isPinchDone)
        {
            isPinchDone=YES;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(251, 8, 773, 690) withDuration:0.5];
        }
        else
        {
            isPinchDone=NO;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(258, 324, 756, 374) withDuration:0.5];
            
            recognizer.view.frame = CGRectMake(258, 324, 756, 374);
        }
    }
}

#pragma mark - ProductStockViewControllerDelegate methods

-(void)didTapSaveButton
{
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
}

@end

