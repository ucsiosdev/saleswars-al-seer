//
//  OrderItemsViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "OrderItemsViewController.h"
#import "SWDefaults.h"
#import "SWPlatform.h"
@interface OrderItemsViewController ()

@end

@implementation OrderItemsViewController

@synthesize documentReference;
@synthesize orderItems;
@synthesize gridView;

- (id)initWithDocumentReference:(NSString *)ref {
    self = [super init];
    
    if (self) {
        [self setDocumentReference:ref];
        [self setTitle:@"Order Items"];
        

        //salesSer.delegate=self;

    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setGridView:[[GridView alloc] initWithFrame:self.view.bounds]];
    [self.gridView setDataSource:self];
    [self.gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.gridView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getSalesOrderServiceDidGetItems: [[SWDatabaseManager retrieveManager] dbGetSalesOrderItems:self.documentReference]];
}

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.orderItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
        NSString *CellIdentifier = @"OrderItemsCell";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        return cell;
    }
}

#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return self.orderItems.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = NSLocalizedString(@"Item Code", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Description", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Quantity", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"Unit Price", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"Total", nil);
    }
    
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    
    NSDictionary *data = [self.orderItems objectAtIndex:row];
    
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Item_Code"]];
    } else if (column == 1) {
        text = [data objectForKey:@"Description"];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Ordered_Quantity"]];
    } else if (column == 3) {
        text = [data currencyStringForKey:@"Unit_Selling_Price"];
    }else if (column == 4) {
        NSString* itemValQuery=[NSString stringWithFormat:@"select Item_Value from TBL_Order_History_line_Items where Orig_Sys_Document_Ref ='%@'", self.documentReference];
        NSMutableArray* ItemValueArr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:itemValQuery];
        NSLog(@"check item val %@", [ItemValueArr description]);

        id ItemVarId= [[ItemValueArr valueForKey:@"Item_Value"] objectAtIndex:row];
        NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
        if ([ItemVarId isEqual:@""]) {
            text=[NSString stringWithFormat:@"%@0.0", userCurrencyCode];
        }
        else {
            text=[NSString stringWithFormat:@"%@%@",userCurrencyCode, ItemVarId];
        }
    }
    return text;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 20;
    } else if (columnIndex == 1) {
        return 40;
    } else if (columnIndex == 2) {
        return 10;
    }
    else if (columnIndex == 3) {
        return 15;
    }
    else if (columnIndex == 4) {
        return 15;
    }
    else
        return 0;
}

#pragma mark SalesOrderService Delegate
- (void)getSalesOrderServiceDidGetItems:(NSArray *)items {
    [self setOrderItems:items];
    [self.gridView reloadData];
    
    if (self.orderItems.count > 0) {
        [self setPreferredContentSize:CGSizeMake(420, self.gridView.tableView.contentSize.height)];
    }
    items=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
