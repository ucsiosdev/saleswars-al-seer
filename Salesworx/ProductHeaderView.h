//
//  ProductHeaderView.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductHeaderView : UIView {
    UILabel *headingLabel;
    UILabel *detailLabel;
}

@property (nonatomic, strong) UILabel *headingLabel;
@property (nonatomic, strong) UILabel *detailLabel;

@end