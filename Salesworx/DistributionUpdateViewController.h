//
//  DistributionUpdateViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface DistributionUpdateViewController : SWTableViewController <EditableCellDelegate, UITextFieldDelegate> {
    NSMutableDictionary *distributionItem;
//       id target;
    SEL action;
   // UIView *myBackgroundView;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

- (id)initWithDistributionItem:(NSDictionary *)item;



@end