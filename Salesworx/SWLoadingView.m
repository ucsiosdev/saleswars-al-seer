//
//  SWLoadingView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWLoadingView.h"
#import "SWFoundation.h"

@implementation SWLoadingView

@synthesize loadingLabel;
@synthesize activityIndicatorView;


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor=[UIColor clearColor];
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.6]];
        [self setLoadingLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self setActivityIndicatorView:[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
        
        [self.loadingLabel setText:@"Loading"];
        [self.loadingLabel setTextAlignment:NSTextAlignmentCenter];
        [self.loadingLabel setBackgroundColor:[UIColor clearColor]];
        [self.loadingLabel setTextColor:[UIColor darkGrayColor]];
        [self.loadingLabel setFont:RegularFontOfSize(22.0)];
        [self.loadingLabel setShadowColor:[UIColor whiteColor]];
        [self.loadingLabel setShadowOffset:CGSizeMake(0, -1)];        
        [self.activityIndicatorView setHidesWhenStopped:YES];
        
        [self addSubview:self.loadingLabel];
        [self addSubview:self.activityIndicatorView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.activityIndicatorView.frame;
    frame.origin.x = (self.frame.size.width / 2) - frame.size.width / 2;
    frame.origin.y = (self.frame.size.height / 2) - frame.size.height / 2;
    [self.activityIndicatorView setFrame:frame];
    
    [self.activityIndicatorView startAnimating];
    
    [self.loadingLabel setFrame:CGRectMake(self.frame.size.width / 2 - 100, frame.origin.y + frame.size.height + 20, 200, 25)];
}



@end
