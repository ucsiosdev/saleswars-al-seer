
//
//  ViewController.m
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//            [self.request setShouldContinueWhenAppEntersBackground:YES];
#import "AppConstantsList.h"
//#import "RequestResponseManager.h"
#import "SynViewController.h"
#import "DataSyncManager.h"
#import "JSON.h"
#import "Status.h"
#import "SWDefaults.h"
#import "CJSONDeserializer.h"
#import "SBJsonParser.h"
#import "ZipManager.h"
#import "ConfirmOrderViewController.h"
#import "DataType.h"
#import "FMDBHelper.h"
#import "AFJSONRequestOperation.h"
#import "AFImageRequestOperation.h"
#import "AlSeerSalesOrderPreviewViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "AlSeerSettingsViewController.h"
#import "SWVisitOptionsViewController.h"
#import "SWNewActivationViewController.h"
#import "SWAppDelegate.h"
#import "Reachability.h"

@interface SynViewController ()
{
    SWAppDelegate *appDel;
}
@end
@implementation SynViewController
@synthesize target,action,request;
@synthesize delegate= _delegate;

static NSString *ClientVersion = @"1";
static NSString *synchReference = @"";

- (id)init
{
    self = [super init];
    if (self){
        self.target = nil;
        self.action = nil;
        self.delegate = nil;
        
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibName bundle:(NSBundle *)bundle
{
    self = [super initWithNibName:nibName bundle:bundle];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDel=(SWAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    [self setAllUIBorderAndRound];
    
    [self setFontAndColor];
    
    
    mediaFilesToDeleteArray=[[NSMutableArray alloc]init];
    allMediaFilesArray=[[NSMutableArray alloc]init];

    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    ClientVersion =avid;
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    
    
    NSInteger collectionCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"collectionCount"];
    
    if (collectionCount) {
        collectionCountString=[NSString stringWithFormat:@"%ld",(long)collectionCount];
    }
    else {
        collectionCountString=@"0";
    }
    
    [self setTitle:NSLocalizedString(@"Data Synchronization", nil)];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    
    appData=[DataSyncManager sharedManager];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    //NSLog(@"SERVER NAME : %@",serverAPI);
    uploadFileCount=0;
    procParamArray=[NSMutableArray arrayWithObjects:@"inpu1",@"input2", nil];
    procValueArray=[NSMutableArray arrayWithObjects:@"1234",@"5678", nil];
    strDeviceID=[[DataSyncManager sharedManager]getDeviceID];
    status_ =[[Status alloc] init];
    
    NSData* dataDicts = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDicts = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDicts error:&error]];
     [lastSyncLable setText:[SWDefaults lastSyncDate]];
     [lastSyncStatusLable setText:[SWDefaults lastSyncStatus]];
     [lastSyncType setText:[SWDefaults lastSyncType]];
     [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
     [pendingOrder setText:orderCountString];
     [pendingCollection setText:collectionCountString];
     [pendingReturn setText:returnCountString];
    
    if ([[SWDefaults userProfile] stringForKey:@"Username"] != nil && ![[[SWDefaults userProfile] stringForKey:@"Username"]  isEqual: @""] ){
        [lblAppUserName setText: [[SWDefaults userProfile] stringForKey:@"Username"]];
    }
    if ([testServerDicts stringForKey:@"serverName"] != nil && ![[testServerDicts stringForKey:@"serverName"]  isEqual: @""]){
        [lblServerName setText:[testServerDicts stringForKey:@"serverName"] ];
    }
    
    /** setting the tap gestures*/
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startSyncOrder)];
    tapRecognizer.numberOfTapsRequired=1;
   [SendOrdersButtonImageView addGestureRecognizer:tapRecognizer];
   
    UITapGestureRecognizer *tapRecognizer_2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startSync)];
    tapRecognizer_2.numberOfTapsRequired=1;
    [fullSyncButtonImageView addGestureRecognizer:tapRecognizer_2];
    isSuccessfull = NO;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadPieChart];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
}

-(void)loadPieChart{
    [pieChart removeFromSuperview];
    pieChart = [[PNCircleChart alloc] initWithFrame:pieChart.frame total:[NSNumber numberWithInt:1] current:0 clockwise:YES shadow:YES shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:NO lineWidth:@15.0f];
    [pieChart.countingLabel setTextColor:[UIColor blackColor] ];
    [pieChart.countingLabel setFont:kFontWeblySleekSemiBold(28)];
    
    pieChart.backgroundColor = [UIColor clearColor];
    [pieChart setStrokeColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [pieChart setStrokeColorGradientStart:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [pieChart strokeChart];
    [viewSyncProgress addSubview:pieChart];
}

-(void)setAllUIBorderAndRound {
    
    viewParentStatistics.layer.cornerRadius = 8.0;
    viewSyncProgress.layer.cornerRadius = 8.0;
    viewLastSyncInfo.layer.cornerRadius = 8.0;
    
    viewOrder.layer.cornerRadius = 4.0;
    viewOrder.layer.masksToBounds = YES;
    
    viewReturns.layer.cornerRadius = 4.0;
    viewReturns.layer.masksToBounds = YES;
    
    viewCollections.layer.cornerRadius = 4.0;
    viewCollections.layer.masksToBounds = YES;
}

// set font color on Special UI which are not set from Xib
-(void)setFontAndColor
{
    lblOrderStatic.textColor = [UIColor whiteColor];
    lblReturnsStatic.textColor = [UIColor whiteColor];
    lblCollectionStatic.textColor = [UIColor whiteColor];
    
    pendingOrder.font = kFontWeblySleekSemiBold(29);
    pendingReturn.font = kFontWeblySleekSemiBold(29);
    pendingCollection.font = kFontWeblySleekSemiBold(29);
}

-(void)StartFullSyncAnimation {
    
    fullSyncButtonImageView.layer.sublayers=nil;
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0  ];
    rotationAnimation.repeatCount = INFINITY;
    rotationAnimation.speed=0.2;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(40,13,32,32)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.clipsToBounds = YES;
    [imgView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [fullSyncButtonImageView.layer addSublayer:[imgView layer]];
}

-(void)StopFullSyncAnimation
{
    [self.view setUserInteractionEnabled:YES];
    [fullSyncButtonImageView.layer removeAllAnimations];
    fullSyncButtonImageView.layer.sublayers=nil;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(40,13,32,32)];
    imgView.image = [UIImage imageNamed:@"Sync_Rotator"];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.clipsToBounds = YES;
    [fullSyncButtonImageView.layer addSublayer:[imgView layer]];
}

-(void)showSettings
{
    AlSeerSettingsViewController* previewVC=[[AlSeerSettingsViewController alloc]init];
    previewVC.delegate=self;
    [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES animated:animated];
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    [pendingOrder setText:orderCountString];
    [self StopFullSyncAnimation];
}

- (void)selectServerButton
{
    SeverEditLocation *serverSelectionViewController = [[SeverEditLocation alloc] init] ;
    [serverSelectionViewController setTarget:self];
    [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
    [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController] ;
    popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    [popoverController setPopoverContentSize:CGSizeMake(200, 520)];
    
    
    [popoverController setDelegate:self];
    [popoverController presentPopoverFromRect:selectServer.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
}
- (void)serverChanged:(NSMutableDictionary *)d
{
    serverDict=nil;
    serverDict=[NSMutableDictionary dictionaryWithDictionary:d];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:serverDict  ] ;
    serverAPI=[NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    //NSLog(@"SERVER NAME : %@",serverAPI);
    [popoverController dismissPopoverAnimated:YES];
    [selectServer setTitle:[serverDict stringForKey:@"serverName"] forState:UIControlStateNormal];
}

//this is send order action
- (void)startSyncOrder
{
    NSInteger count = [[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT COUNT(*) FROM TBL_Proforma_Order"]objectAtIndex:0] stringForKey:@"COUNT(*)"] integerValue];
    if (count>0)
    {
        ConfirmOrderViewController *orderConfirm = [[ConfirmOrderViewController alloc]init];
        [orderConfirm setTarget:self];
        [orderConfirm setAction:@selector(orderConfirmedSendOrder)];
        [self.navigationController pushViewController:orderConfirm animated:YES];
    }
    else {
        [self orderConfirmedSendOrder];
    }
}

-(void)orderConfirmedSendOrder
{
    [self setTarget:nil];
    [self setAction:nil];
    self.target = nil;
    self.action = nil;
    
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.isFromSync = 2;
        single.isActivated = YES;
        isCompleted = NO;
        logTextView.text = @"";
        [SWDefaults setLastSyncType:NSLocalizedString(@"Send Orders", nil)];
        [lastSyncType setText:[SWDefaults lastSyncType]];
        [self finalLogPrint:@"Starting order upload"];
        orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
        [self finalLogPrint: [NSString stringWithFormat:@"Orders to be uploaded : %@",orderCountString]];
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [self StartOrderSyncAnimation];
        //sendOrderSer.delegate=self;
        [[SWDatabaseManager retrieveManager] dbGetConfirmOrder];
        [[SWDatabaseManager retrieveManager] setTarget:self];
        [[SWDatabaseManager retrieveManager] setAction:@selector(xmlDone:)];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"You license has been expired or contact your administrator for further assistance" withController:self];
    }
    
}
-(void)xmlDone:(NSString *)st
{
    if([st isEqualToString:@"done-01"])
    {
        Singleton *single = [Singleton retrieveSingleton];
        NSString  *XMLPassingString= (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)single.xmlParsedString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
        [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSendOrders" andProcParam:@"OrderList" andProcValue: XMLPassingString];
    }
    else
    {
        //hide local notification
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
        [self stopAnimationofSendOrder];
        [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
    }
}
- (void)startSync
{
    int count = [[[[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT COUNT(*) FROM TBL_Proforma_Order"]objectAtIndex:0] stringForKey:@"COUNT(*)"] intValue];
    if (count>0)
    {
        ConfirmOrderViewController *orderConfirm = [[ConfirmOrderViewController alloc]init];
        [orderConfirm setTarget:self];
        [orderConfirm setAction:@selector(orderConfirmed)];
        [self.navigationController pushViewController:orderConfirm animated:YES];
    }
    else {
        [self orderConfirmed];
    }
}

-(void)orderConfirmed
{
    [self setTarget:nil];
    [self setAction:nil];
    self.target = nil;
    self.action = nil;
    Singleton *single = [Singleton retrieveSingleton];
    single.isActivated = YES;
    isCompleted = NO;
    appData=[DataSyncManager sharedManager];
    [[SWDatabaseManager retrieveManager] checkUnCompleteVisits];
   // [self StartSyncAnimation];
    [self StartFullSyncAnimation];
    
    statusSynString = @"100";
    progressBar.progress = 0.0;
    logTextView.text = @"";
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        [self setRequestForSyncUpload];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"You license has been expired or contact your administrator for further assistance" withController:self];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==1000) {
        alertView=nil;
    }
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ActivationError" object:nil];
    }
}
- (void)setRequestForSyncActivateWithUserName:(NSString *)username andPassword:(NSString *)password
{
    SWNewActivationViewController * newActivation=[[SWNewActivationViewController alloc]initWithNoDB];
    newActivation.alertDisplayed=NO;
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isActivated = NO;
    isCompleted=NO;
    if(appData.statusDict)
    {
        [appData.statusDict removeAllObjects];
        appData.statusDict=nil;
    }
    if (self.request)
    {
        self.request = nil;
    }
    
    self.request = [[HttpClient sharedManager] initWithBaseURL:[NSURL URLWithString:@"http://google.com"]];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [self finalLogPrint:@"Connection Failed"];
        NSLog(@"\n\n**************** WARNING: Connection Failed  *************\n\n");
        
        [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Connection Failed, Please check User Name, Password and Server URL and try again." withController:self];
        return;
    } else {
        if (!isSuccessfull)
        {
            isSuccessfull=YES;
            NSLog(@"\n\n*** Hint: connection successfully *****\n\n");
            [self finalLogPrint:@"Connection successfully"];
        }
    }
    
    passwordActivate =[[DataSyncManager sharedManager] sha1:password];
    usernameActivate = username;
    isByteSent = NO;
    NSString *strDatabaseInfo= [[DataSyncManager sharedManager] dbGetDatabasePath];
    if([strDatabaseInfo isEqualToString:@"Database does not exist"]){}
    else{[[DataSyncManager sharedManager] deleteDatabaseFromApp];}
    isFromSync=1;
    single.isFromSync = 1;
    [self finalLogPrint:@"Connecting..."];
    
    [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
    [self sendRequestForActivate];
}

- (void)setRequestForSyncUpload
{
    [ZipManager convertSqliteToNSDATA];
    [SWDefaults setLastSyncType:NSLocalizedString(@"Full Sync", nil)];
    [lastSyncType setText:[SWDefaults lastSyncType]];
    if(appData.statusDict){
        [appData.statusDict removeAllObjects];
        appData.statusDict=nil;
    }
    Singleton *single = [Singleton retrieveSingleton];
    isByteSent = NO;
    isForSyncUpload=1;
    isFromSync=2;
    single.isFromSync = 2;
    single.isForSyncUpload=1;
    [self finalLogPrint:@"Connecting..."];
    
    usernameActivate = [[SWDefaults userProfile] stringForKey:@"Username"];
    passwordActivate = [[SWDefaults userProfile] stringForKey:@"Password"];
    
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:[NSURL URLWithString:@"http://google.com"]];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"\n\n**************** WARNING: Connection Failed  *************\n\n");
        [self finalLogPrint:@"Connection Failed"];
        [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
        
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Connection Failed, Please check User Name, Password and Server URL and try again." withController:self];
        return;
    }
    else
    {
        if (!isSuccessfull)
        {
            NSLog(@"\n\n*** Hint: connection successfully *****\n\n");
            isSuccessfull=YES;
            [self finalLogPrint:@"Connection successfully"];
        }
    }

    
    NSString *strDatabaseInfo= [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    if([strDatabaseInfo isEqualToString:@"Database does not exist"])
    {
        [[DataSyncManager sharedManager]UserAlert:@"Please activate the process first"];
    }
    else
    {
        [self.view setUserInteractionEnabled:NO];
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [[DataSyncManager sharedManager] hideCustomIndicator];
        [self sendRequestForUploadData];
    }
}
- (void)sendRequestForActivate
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *strurl =[serverAPI stringByAppendingString:@"Activate"];
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    //check the reachability of activation url
    BOOL isReachable=[self urlIsReachable:[testServerDict stringForKey:@"serverName"]];
    if (isReachable==YES) {
        NSLog(@"server url is reachable");
    }
    else {
        NSLog(@"server url unreachable");
    }
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            @"Activation", @"SyncType",
                            [testServerDict stringForKey:@"serverName"], @"SyncLocation",
                            nil];
    NSLog(@"chek request %@", self.request);
    NSLog(@"check parameters %@", [params description]);
    
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSLog(@"check response %@", [responseObject description]);
         
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
             synchReference = strSyncNO;
             if(![strSyncNO length]==0)
             {
                 [self sendRequestForInitiate];
             }
             else
             {
                 [self stopAnimation];
                 [[DataSyncManager sharedManager] UserAlert:[NSString stringWithFormat:@"Error:%@",responseString]];
                 if(!single.isActivated)
                 {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                     [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                 }
             }
         }
     } failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [self.view setUserInteractionEnabled:YES];
         [self stopAnimation];
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Connection Failed, Please check User Name, Password and Server URL and try again." withController:self];
         
         NSLog(@"[HTTPClient Error]: %@", error.debugDescription);
         
         SWNewActivationViewController* newActivationVC=[[SWNewActivationViewController alloc]initWithNoDB];
         logTextView.text=@"";
         newActivationVC.alertDisplayed=NO;
     }];
}

-(BOOL) urlIsReachable:(NSString*)urlStr {
    
    NSString *url = urlStr;
    
    NSURLRequest* reachableRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5.0];
    NSHTTPURLResponse* response = nil;
    NSError* error = nil;
    [NSURLConnection sendSynchronousRequest:reachableRequest returningResponse:&response error:&error];
    NSLog(@"statusCode = %d", [response statusCode]);
    
    if ([response statusCode] == 404)
        return NO;
    else
        return YES;
}

- (void)sendRequestForUploadData
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    [self finalLogPrint:@"Uploading database..."];
    //Saadi Progress
    
    [DataSyncManager sharedManager].progressHUD.labelText=@"Uploading database...";
    lblSyncProgressStatus.text = @"Uploading database...";
    
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeDeterminate;
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadDataGZ"];
    NSString *path = [[DataSyncManager sharedManager] dbGetZipDatabasePath];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            @"Full Sync", @"SyncType",
                            [testServerDict stringForKey:@"serverName"], @"SyncLocation",
                            nil];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
        [formData appendPartWithFileData:postData
                                    name:@"swx"
                                fileName:@"swx"
                                mimeType:@"application/x-gzip"];
    }
    ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
             [DataSyncManager sharedManager].progressHUD.progress = 0.0;
         }
         else
         {
             progressBarFloatValue = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
             [DataSyncManager sharedManager].progressHUD.progress = progressBarFloatValue;
             progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue*100] ;
             progressDetail =[progressDetail stringByAppendingString:@"%"];
             [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
             
             NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
             [pieChart updateChartByCurrent:progress];
             
             if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
             {
                 [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
             }
             [DataSyncManager sharedManager].progressHUD.progress = totalBytesWritten * 1.0 / totalBytesExpectedToWrite;
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
        appData.statusDict = [ operationQ.responseString JSONValue];
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isFromSync==1)
        {
            
        }
        else if(single.isFromSync==2)
        {
            [DataSyncManager sharedManager].progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png" cache:NO]];
            [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeCustomView;
            [DataSyncManager sharedManager].progressHUD.labelText = @"Uploaded";
            lblSyncProgressStatus.text =  @"Uploaded";
            
            if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
            {
                [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
            }
            
            NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
            synchReference = strSyncNO;
            
            if(![strSyncNO length]==0)
            {
                [self sendRequestForInitiate];
            }
            else
            {
                [self.view setUserInteractionEnabled:YES];
                [self stopAnimation];
                [[DataSyncManager sharedManager] UserAlert:@"Please start the process again require Syncrefrenceno."];
                if(!single.isActivated)
                {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                    [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                }
            }
        }
        
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
        
        [self.view setUserInteractionEnabled:YES];
        NSLog(@"error calling send request for complete 893 %@", error);
        [self sendRequestCompleteWithError:[operationQ responseString]];
    }];
    
    [operation start];
}
- (void)sendRequestForStatus
{
    if(isCompleted)
    {
        [self ProcessCompleted];
        [DataSyncManager sharedManager].progressHUD.progress = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
    }
    else {
        NSString *strUrl =[serverAPI stringByAppendingFormat:@"status/%@",[appData.statusDict objectForKey:@"SyncReferenceNo"]];
        NSURL *url = [NSURL URLWithString:strUrl];
        if (self.request)
        {
            self.request = nil;
        }
        self.request = [[HttpClient sharedManager] initWithBaseURL:url];
        [self.request getPath:nil parameters:nil success:^(AFHTTPRequestOperation *operationQ, id responseObject)
         {
            NSString *responseString = operationQ.responseString;
            appData.statusDict = [responseString JSONValue];
            Singleton *single = [Singleton retrieveSingleton];
            if(single.isFromSync==1)
            {
                if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"D"])
                {
                    [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                    [self sendRequestForDownload];
                }
                else if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"F"])
                {
                    if (!single.isActivated)
                    {
                        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:[appData.statusDict stringForKey:@"ProcessResponse"] withController:self];
                        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                        [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                    }
                }
                else
                {
                    [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
                }
            }
            else
            {
                if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"D"])
                {
                    [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                    [self sendRequestForDownload];
                }
                else if([[appData.statusDict objectForKey:@"SyncStatus"] isEqualToString:@"F"])
                {
                    [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"] ];
                    [self performSelector:@selector(stopAnimation) withObject:nil afterDelay:0.0];
                    [[DataSyncManager sharedManager] UserAlert:@"Sync process failed. Please try again or contact your administrator for assistance "];
                }
                else {
                    [self performSelector:@selector(sendRequestForStatus) withObject:nil afterDelay:1.0];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
            [self.view setUserInteractionEnabled:YES];
            NSLog(@"error calling send request for complete  996 %@", error);
            [self sendRequestCompleteWithError:[operationQ responseString]];
        }];
        
        Singleton *single = [Singleton retrieveSingleton];
        if(![statusSynString isEqualToString:[appData.statusDict stringForKey:@"SyncStatus"]])
        {
            statusSynString = [appData.statusDict stringForKey:@"SyncStatus"];
            if([statusSynString isEqualToString:@"N"])
            {
                [self finalLogPrint:@"Sync Status : Pending"];
            }
            if([statusSynString isEqualToString:@"C"])
            {
                if(single.isFromSync==2)
                {
                    [self finalLogPrint:@"Database has been uploaded successfully"];
                }
                [self finalLogPrint:[NSString stringWithFormat:@"Sync Reference # %@",[appData.statusDict stringForKey:@"SyncReferenceNo"]]];
                [self finalLogPrint:[NSString stringWithFormat:@"Byte Sent : %@ bytes",[appData.statusDict stringForKey:@"BytesReceived"]]];
                [self finalLogPrint:@"Sync Status : Copying"];
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = [NSString stringWithFormat:@"Byte Sent : %@ bytes",[appData.statusDict stringForKey:@"BytesReceived"]];
                
                NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
                [pieChart updateChartByCurrent:progress];
                
                [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeDeterminate;
                [DataSyncManager sharedManager].progressHUD.labelText = @"Copying";
                lblSyncProgressStatus.text = @"Copying";
            }
            if([statusSynString isEqualToString:@"S"])
            {
                [self finalLogPrint:@"Sync Status : Synchronizing"];
                [DataSyncManager sharedManager].progressHUD.labelText = @"Synchronizing";
                lblSyncProgressStatus.text = @"Synchronizing";
            }
            if([statusSynString isEqualToString:@"D"])
            {
                [self finalLogPrint:@"Sync Status : Downloaded"];
                [DataSyncManager sharedManager].progressHUD.labelText = @"Please Wait...";
                lblSyncProgressStatus.text = @"Please Wait...";
                
                [self finalLogPrint:[NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]]];
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = [NSString stringWithFormat:@"Byte Received : %@ bytes",[appData.statusDict stringForKey:@"BytesSent"]];
                
                NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
                [pieChart updateChartByCurrent:progress];
            }
            if([statusSynString isEqualToString:@"F"])
            {
                [self stopAnimation];
                [self finalLogPrint:[appData.statusDict stringForKey:@"ProcessResponse"]];
                [self finalLogPrint:@"Sync Status : Failed"];
                Singleton *single = [Singleton retrieveSingleton];
                if (!single.isActivated)
                {
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:[appData.statusDict stringForKey:@"ProcessResponse"] withController:self];
                    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                    [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                }
            }
            if([statusSynString isEqualToString:@"P"])
            {
                [self finalLogPrint:@"Sync Status : Preparing output"];
            }
            if([[appData.statusDict objectForKey:@"CurrentProgress"] isEqual:@"1"])
            {
                [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
            }
        }
        if([[appData.statusDict objectForKey:@"CurrentProgress"] isEqual:@"1"])
        {
            [DataSyncManager sharedManager].progressHUD.labelText = @"PLease wait...";
            lblSyncProgressStatus.text = @"PLease wait...";
            [DataSyncManager sharedManager].progressHUD.detailsLabelText = @"Linking with application";
        }
        progressBarFloatValue = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
        progressBar.progress=progressBarFloatValue;
        progressBarFloatValue = progressBarFloatValue * 100.0;
        progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue] ;
        progressDetail =[progressDetail stringByAppendingString:@"%"];
        [DataSyncManager sharedManager].progressHUD.progress = [[appData.statusDict objectForKey:@"CurrentProgress"] floatValue];
        [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
       
        NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
        [pieChart updateChartByCurrent:progress];
    }
    
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
}
- (void)sendRequestForInitiate
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Initiate"];
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            [appData.statusDict objectForKey:@"SyncReferenceNo"], @"SyncReferenceNo",
                            nil];
    
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         appData.statusDict = [responseString JSONValue];
         Singleton *single = [Singleton retrieveSingleton];
         if(single.isFromSync==1)
         {
             NSString *strSyncNO=[appData.statusDict objectForKey:@"SyncReferenceNo"];
             if(![strSyncNO length]==0)
             {
                 [self sendRequestForStatus];
             }
             else
             {
                 [self stopAnimation];
                 [[DataSyncManager sharedManager] UserAlert:@"Please start the process again require Syncrefrenceno."];
                 if (!single.isActivated)
                 {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                     [self.target performSelector:self.action withObject:@"error"];
#pragma clang diagnostic pop
                 }
             }
         }
         else if(single.isFromSync==2)
         {
             [self sendRequestForStatus];
         }
    }
                   failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
        [self.view setUserInteractionEnabled:YES];
        NSLog(@"error calling send request for complete 1155 %@", error);
        [self sendRequestCompleteWithError:[operationQ responseString]];
    }];
}

- (void)sendRequestForDownload
{
    [DataSyncManager sharedManager].progressHUD.labelText = @"Downloading";
    lblSyncProgressStatus.text = @"Downloading";
    [self finalLogPrint:@"Sync Status : Downloading database..."];
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    
    NSString *strUrl =[serverAPI stringByAppendingFormat:@"DownloadGZ/%@",[appData.statusDict objectForKey:@"SyncReferenceNo"]];
    NSString *resourceDocPath;
    
    
    //iOS 8 support
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    NSString *pdfName = @"swx.sqlite.gz";
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:request123] ;
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setDownloadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
        if (totalBytesExpectedToWrite == 0)
        {
            [DataSyncManager sharedManager].progressHUD.progress = 0.0;
        }
        else
        {
            progressBarFloatValue = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
            [DataSyncManager sharedManager].progressHUD.progress = progressBarFloatValue;
            progressDetail = [NSString stringWithFormat:@"%.0f",progressBarFloatValue*100] ;
            progressDetail =[progressDetail stringByAppendingString:@"%"];
            [DataSyncManager sharedManager].progressHUD.detailsLabelText = progressDetail;
            
            NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
            [pieChart updateChartByCurrent:progress];
            
            if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
            {
                [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
            }
        }
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        [self finalLogPrint:@"Sync Status : Database Downloaded"];
        
        NSString *responseString = [operationQ responseString];
        appData.statusDict = [responseString JSONValue];
        [DataSyncManager sharedManager].progressHUD.progress = 1.0;
        Singleton *single = [Singleton retrieveSingleton];
        [ZipManager convertNSDataToSQLite];
        
        
        if(single.isFromSync==1)
        {
            BOOL success;
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *resourceDocPath;
            
            //iOS 8 support
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                resourceDocPath=[SWDefaults applicationDocumentsDirectory];
            }
            else {
                resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
            }
            
            NSString *pdfName = databaseName;
            NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
            success = [fileManager fileExistsAtPath:filePath];
            if (success)
            {
                [self sendRequestForComplete];
                [self downloadMedia];
            }
            else
            {
                [[DataSyncManager sharedManager] UserAlert:@"Databse can not be downloaded. Please try again"];
                [self.view setUserInteractionEnabled:YES];
            }
        }
        else
        {
            NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
            NSArray *DistributionImageArray=[[DataSyncManager sharedManager] dbGetDistributionImageFilePath];
            NSArray *visitImageArray=[[DataSyncManager sharedManager] getVisitImagesFilePath];
            
            if([signatureArray count]>=1)
            {
                [self sendRequestForUploadFile];
            }
            else if (DistributionImageArray.count>=1)
            {
                uploadDistributionImageFileCount=0;
                [self sendRequestForUploadDistributionImageFile];
            }
            else if (visitImageArray.count>=1)
            {
                uploadVisitImagesFileCount=0;
                [self sendRequestForUploadVisitImages];
            }
            else
            {
                NSLog(@"send request for complete 1316");
                [self sendRequestForComplete];
            }
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
        NSLog(@"error calling send request for complete 1320 %@", error);
        [self.view setUserInteractionEnabled:YES];
        [self sendRequestCompleteWithError:[operationQ responseString]];
    }];
    [operation start];
}
#pragma mark Media files downloading methods


-(NSMutableArray *) fetchMediaFilesToDelete {
    
    [mediaFilesToDeleteArray removeAllObjects];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Is_Deleted='Y'"]];
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [mediaFilesToDeleteArray addObject:customer];
    }
    return mediaFilesToDeleteArray;
}


-(NSMutableArray *) fetchMediaFilesFromDocuments {
    
    [allMediaFilesArray removeAllObjects];
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:@"Select * from TBL_Media_Files WHERE Download_Flag='Y' AND Is_Deleted='N'"]];
    
    //ppt file got downloaded
    
    NSLog(@"downloaded media files %@", [array description]);
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [allMediaFilesArray addObject:customer];
    }
    return allMediaFilesArray;
}


-(void)downloadMedia {
    
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeIndeterminate;
    [DataSyncManager sharedManager].progressHUD.detailsLabelText =@"";
    
    NSNumber *progress = [NSNumber numberWithFloat:[DataSyncManager sharedManager].progressHUD.progress];
    [pieChart updateChartByCurrent:progress];
    
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    
    //Delete Media Files for is_delted= Y
    NSMutableArray * arrayToDelete = [self fetchMediaFilesToDelete];
    for (MediaFile * mToDelete in arrayToDelete) {
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        
        // Get dir
        NSString *resourceDocPath;
        
        
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        NSString *fileName =mToDelete.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:fileName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSError *error = nil;
            if(![fileManager removeItemAtPath: filePath error:&error]) {
                NSLog(@"Delete failed:%@", error);
            } else {
                NSLog(@"image removed: %@", filePath);
                //Delete from Table
                NSString *updateMediaFile =[NSString stringWithFormat:@"DELETE FROM TBL_Media_Files WHERE Media_File_ID='%@'",mToDelete.Media_File_ID];
                BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
                if (status) {
                }
            }
        }
    }
    
    NSMutableArray * arrayOfMediaFiles = [self fetchMediaFilesFromDocuments];
    if (arrayOfMediaFiles.count > 0) {
        [DataSyncManager sharedManager].progressHUD.labelText = @"Downloading";
        lblSyncProgressStatus.text = @"Downloading";
        [self finalLogPrint:@"Sync Status : Downloading Media Files..."];
    }
    
    for (MediaFile * mfile in arrayOfMediaFiles) {
        if ([mfile.Media_Type isEqualToString:@"Image"]) {
            [self downloadProductImageWithName:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Video"]) {
            [self downloadVideoOrPdf:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Brochure"]) {
            [self downloadVideoOrPdf:mfile];
        }else if([mfile.Media_Type isEqualToString:@"Powerpoint"]){
            NSLog(@"got ppt ");
            [self downloadPowerpointFile:mfile];
        }
    }
}

-(void)downloadProductImageWithName:(MediaFile *)mfile
{
    NSString *strUrlForImage =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForImage] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1200];
    
    AFImageRequestOperation *operationImag = [AFImageRequestOperation imageRequestOperationWithRequest:request123 imageProcessingBlock:nil
                                                                                               success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        // Get dir
        NSString *documentsDirectory = nil;
        
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            documentsDirectory=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            documentsDirectory = [paths objectAtIndex:0];
        }
        
        NSString *pathString = [NSString stringWithFormat:@"%@/%@",documentsDirectory, mfile.Filename];
        // Save Image
        NSData *imageData = UIImageJPEGRepresentation(image, 90);
        [imageData writeToFile:pathString atomically:YES];
        
        //Update Table
        NSLog(@"DownloadComplete Image %@",mfile.Media_File_ID);
        
        NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
        BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
        if (status) {
        }
        
    }
                                                                                               failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        [self.view setUserInteractionEnabled:YES];
        NSLog(@"Test ...... %@", [error localizedDescription]);
    }];
    
    [operationImag start];
}

-(void)downloadVideoOrPdf:(MediaFile *)mfile
{
    NSString *strUrlForImage =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    
    NSLog(@"media file ID for video is %@", mfile.Media_File_ID);
    
    // Get dir
    NSString *resourceDocPath;
    
    //iOS 8 support
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    
    NSString *pdfName = mfile.Filename;
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    NSURLRequest *request123 = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForImage] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1000];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:request123] ;
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *resourceDocPath;
        
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        NSString *pdfName =mfile.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSLog(@"DownloadComplete Video Or PDF %@ :",mfile.Media_File_ID);
            //Update Table
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
            }
        }
        else
        {
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
        [self.view setUserInteractionEnabled:YES];
        NSLog(@"failed to download file : %@", error);
        //[self sendRequestCompleteWithError:[[operationQ error] localizedDescription]];
        single = [Singleton retrieveSingleton];
        
    }];
    
    [operation start];
}


-(void)downloadPowerpointFile:(MediaFile*)mfile
{
    NSLog(@"server api for ppt is %@", serverAPI);
    NSString *strUrlForPpt =[serverAPI stringByAppendingString:[NSString stringWithFormat:@"Media/%@?FileType=%@",mfile.Media_File_ID,@"M"]];
    NSLog(@"media file ID is %@", mfile.Media_File_ID);
    NSLog(@"string url is %@", strUrlForPpt);
    NSString *resourceDocPath;
    
    //iOS 8 support
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        resourceDocPath=[SWDefaults applicationDocumentsDirectory];
    }
    else
    {
        resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
    }
    
    NSString *pdfName = mfile.Filename;
    NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
    
    NSURLRequest *requestPPT = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlForPpt] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1000];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:requestPPT] ;
    
    NSLog(@"operation url is %@", operation.request.URL);
    
    NSLog(@"file path is %@", filePath);
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject) {
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *resourceDocPath;
        
        
        
        //iOS 8 support
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            resourceDocPath=[SWDefaults applicationDocumentsDirectory];
        }
        else
        {
            resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle]  resourcePath] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Documents"]];
        }
        
        
        NSString *pdfName =mfile.Filename;
        NSString *filePath = [resourceDocPath stringByAppendingPathComponent:pdfName];
        success = [fileManager fileExistsAtPath:filePath];
        if (success)
        {
            NSLog(@"DownloadComplete ppt");
            //Update Table
            NSString *updateMediaFile =[NSString stringWithFormat:@"UPDATE TBL_Media_Files SET Download_Flag='%@' WHERE Media_File_ID='%@'",@"N",mfile.Media_File_ID];
            BOOL status = [FMDBHelper executeNonQuery:updateMediaFile];
            if (status) {
            }
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
        [self.view setUserInteractionEnabled:YES];
        NSLog(@"Error: %@", error);
        single = [Singleton retrieveSingleton];
    }];
    [operation start];
}


#pragma mark media file download complete

- (void)sendRequestForComplete
{
    NSString *strurl =[serverAPI stringByAppendingString:@"Complete"];
    NSURL *url = [NSURL URLWithString:strurl];
    NSLog(@"send request for complete url %@",url);
    
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            synchReference, @"SyncReferenceNo",
                            nil];
    
    NSLog(@"send request for response params %@", params);
    [self.request postPath:nil parameters:params success:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
        NSString *responseString = [operationQ responseString];
        appData.statusDict = [responseString JSONValue];
        NSLog(@"send request for complete response %@", appData.statusDict);
        
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isFromSync==1)
        {
            isCompleted=YES;
            [self stopAnimation];
            [self sendRequestForStatus];
        }
        else if(single.isFromSync==2)
        {
            isCompleted=YES;
            isForSyncUpload =0;
            single.isForSyncUpload=0;
            [self sendRequestForStatus];
        }
    }
                   failure:^(AFHTTPRequestOperation *operationQ, NSError *error) {
        [self.view setUserInteractionEnabled:YES];
        //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        NSLog(@"error calling send request for complete 1911 %@", error);
        
        [self sendRequestCompleteWithError:[operationQ responseString]];
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isFromSync==1)
        {
            
        }
        else
        {
            
        }
    }];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    
    NSString *directory;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
    }
    
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
    }
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}
- (void)sendRequestForUploadFile
{
    [self finalLogPrint:@"Uploading Files"];
    
    NSArray *signatureImageArray= [[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = [signatureImageArray objectAtIndex:uploadFileCount];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            FileType, @"FileType",
                            nil];
    
    
    NSLog(@"upload file parameters %@", [params description]);
    
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    if (self.request)
    {
        self.request = nil;
    }
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
        [formData appendPartWithFileData:postData
                                    name:[path lastPathComponent]
                                fileName:[path lastPathComponent]
                                mimeType:@"image/jpeg"];
    }
    ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
        if (totalBytesExpectedToWrite == 0)
        {
        }
        else
        {
        }
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
        NSString *responseString = [operationQ responseString];
        appData.statusDict = [responseString JSONValue];
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isFromSync==1)
        {
            
        }
        else
        {
            NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
            NSString *strImageName =[signatureArray objectAtIndex:uploadFileCount];
            [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
            if(uploadFileCount != [signatureArray count]-1)
            {
                uploadFileCount++;
                [self sendRequestForUploadFile];
            }
            
            else
            {
                NSLog(@"send request for complete 2051");
                [self sendRequestForComplete];
            }
        }
    }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
        NSLog(@"error calling send request for complete 2056 %@", error);
        [self.view setUserInteractionEnabled:YES];
        [self sendRequestCompleteWithError:[operationQ responseString]];
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isFromSync==1)
        {
            
        }
        else
        {
            
        }
    }];
    [operation start];
}


#pragma mark Visit Images

-(void)sendRequestForUploadVisitImages
{
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
     [self prepareStausDictionary:KSync_UploadingVisitImagesStatusStr Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    //uploading signature here
    
    NSArray *visitImagesarray= [[DataSyncManager sharedManager] getVisitImagesFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = [visitImagesarray objectAtIndex:uploadVisitImagesFileCount];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ResponseFormat, @"ResponseFormat",
                            [[SWDefaults userProfile] stringForKey:@"Username"], @"Username",
                            [[SWDefaults userProfile] stringForKey:@"Password"], @"Password",
                            strDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            @"VISIT_IMG", @"FileType",
                            nil];
    
    NSLog(@"upload file parameters for visit file is %@", [params description]);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [request multipartFormRequestWithMethod:@"POST"
                                                                        path:nil
                                                                  parameters:params
                                                   constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:KJPEGMIMEFormat];
                                      }
                                      ];
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
         }
         else
         {
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         NSArray *DistributionImagesarray=[[DataSyncManager sharedManager] getVisitImagesFilePath];
         NSString *strImageName =[DistributionImagesarray objectAtIndex:uploadVisitImagesFileCount];
         [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
          [self prepareStausDictionary:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]] Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         
         if(uploadVisitImagesFileCount != [DistributionImagesarray count]-1)
         {
             uploadVisitImagesFileCount++;
             [self sendRequestForUploadVisitImages];
         }
         else
         {
             [self deleteVisitImages];
             NSLog(@"send request for complete 2154");
             
             [self sendRequestForComplete];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [self.view setUserInteractionEnabled:YES];
         [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
     }];
    
    [operation start];
}

-(void)deleteVisitImages
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory;
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Visit Images/"];
    }
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Visit Images/"];
    }
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}

#pragma mark UploadDistributionFiles

- (void)sendRequestForUploadDistributionImageFile
{
    [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
     [self prepareStausDictionary:KSync_UploadingDistributionImagesStatusStr Progress:0.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
    
    //uploading signature here
    
    NSArray *DistributionImagesarray= [[DataSyncManager sharedManager] dbGetDistributionImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSString *path = [DistributionImagesarray objectAtIndex:uploadDistributionImageFileCount];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            ResponseFormat, @"ResponseFormat",
                            [[SWDefaults userProfile] stringForKey:@"Username"], @"Username",
                            [[SWDefaults userProfile] stringForKey:@"Password"], @"Password",
                            strDeviceID, @"DeviceID",
                            [self getAppVersionWithBuild], @"ClientVersion",
                            @"DIST_CHECK", @"FileType",
                            nil];
    
    NSLog(@"upload file parameters for distribution check file is %@", [params description]);
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [request multipartFormRequestWithMethod:@"POST"
                                                                        path:nil
                                                                  parameters:params
                                                   constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:KJPEGMIMEFormat];
                                      }
                                      ];
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
         }
         else
         {
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         NSString *responseString = [operationQ responseString];
         ////NSLog(@"Upload response %@", appData.statusDict);
         
         NSArray *DistributionImagesarray=[[DataSyncManager sharedManager] dbGetDistributionImageFilePath];
         NSString *strImageName =[DistributionImagesarray objectAtIndex:uploadDistributionImageFileCount];
         
         //  [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
         [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
          [self prepareStausDictionary:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]] Progress:1.0 ShowAlert:KSync_NoCode WithTitle:@"" Message:@""]];
         
         if(uploadDistributionImageFileCount != [DistributionImagesarray count]-1)
         {
             uploadDistributionImageFileCount++;
             [self sendRequestForUploadDistributionImageFile];
         }
         else
         {
             [self deleteDistributionImages];
             NSLog(@"send request for complete 2154");

             [self sendRequestForComplete];
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [self.view setUserInteractionEnabled:YES];
         [[NSNotificationCenter defaultCenter]postNotificationName:KNewSyncNotificationNameStr object:
          [self prepareStausDictionary:KSync_ErrorAlertTitleStr Progress:0.0 ShowAlert:KSync_YesCode WithTitle:KSync_ErrorAlertTitleStr Message:[NSString stringWithFormat:@"%@,\n %@ (%d)",KSync_URLRequestFailureAlertMessageStr,error.localizedDescription,error.code]]];
     }];
    
    [operation start];
    
    
}

-(NSDictionary *)prepareStausDictionary:(NSString*)statusStr Progress:(float)Progress ShowAlert:(NSString *)ShowAlert WithTitle:(NSString *)Title Message:(NSString *)Message
{
    NSDictionary *statusDic=[[NSDictionary alloc]initWithObjectsAndKeys:statusStr,KSyncDic_SyncStausStr,[NSString stringWithFormat:@"%f",Progress],KSyncDic_SyncProgressStr,ShowAlert,KSyncDic_ShowAlertStr,Title,KSync_AlertTitleStr,Message,KSync_AlertMessageStr,nil];
    return statusDic;
}

#pragma Delete Distribution images
-(void)deleteDistributionImages
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory;
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Distribution check images/"];
    }
    
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Distribution check images/"];
    }
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}

-(NSString *)getAppVersionWithBuild
{
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    NSLog(@"updating client version %@", [NSString stringWithFormat:@"%@(%@)", version,build]);
    
    NSString * AppVersion =[NSString stringWithFormat:@"%@(%@)", version,build];
    
    return AppVersion;
}

- (void)sendRequestCompleteWithError:(NSString *)error
{
    NSLog(@"Error in send request for complete: %@",error);
    
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    Singleton *single = [Singleton retrieveSingleton];
    if(single.isFromSync==1)
    {
        UIAlertView *ErrorAlert;
        
        if (!ErrorAlert) {
            
            
            ErrorAlert= [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:@"Please check server settings or Internet connection and try again."] delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", nil) otherButtonTitles:nil];
            ErrorAlert.tag=1000;
            [ErrorAlert show];
            
        }
    }
    else
    {
        [self finalLogPrint:[NSString stringWithFormat:@"Error.Please check server settings or Internet connection and try again."]];
    }
    [self stopAnimation];
}
- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    
    
    if([procName isEqualToString:@"sync_MC_GetLastDocumentReferenceAll"])
    {
        [self finalLogPrint:@"Retrieving orders references number"];
    }
    
    
    NSString *strurl =[serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];
    if (self.request)
    {
        self.request = nil;
    }
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [self.request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         // //NSLog(@"Result Array %@",resultArray);
         
         if([procName isEqualToString:@"sync_MC_GetLastDocumentReferenceAll"])
         {
             
             
             resultArray = [responseText JSONValue];
             //NSLog(@"Result Array %@",resultArray);
             [self finalLogPrint:@"Order reference has been retrieved"];
             Singleton *single = [Singleton retrieveSingleton];
             single.isFullSyncDone = YES;
             single.isFullSyncCollection = YES;
             single.isFullSyncCustomer = YES;
             single.isFullSyncSurvey = YES;
             single.isFullSyncProduct = YES;
             single.isFullSyncDashboard = YES;
             single.isFullSyncMessage = YES;
             for (int i =0 ; i <[resultArray count]; i++)
             {
                 @autoreleasepool {
                     NSDictionary *data = [resultArray objectAtIndex:i];
                     if([[data stringForKey:@"Doc_Type"] isEqualToString:@"COLLECTION"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastCollectionReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastCollectionReference:@"M000C0000000000"];
                         }
                     }
                     
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastOrderReference:@"M000S0000000000"];
                         }
                     }
                     else  if([[data stringForKey:@"Doc_Type"] isEqualToString:@"PROFORMA_ORDER"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastPerformaOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastPerformaOrderReference:@"M000D0000000000"];
                         }
                     }
                     else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"RMA"])
                     {
                         if ( ![[data stringForKey:@"Doc_Reference_No"] isEqualToString:@"<null>"])
                         {
                             [SWDefaults setLastReturnOrderReference:[data stringForKey:@"Doc_Reference_No"]];
                         }
                         else
                         {
                             [SWDefaults setLastReturnOrderReference:@"M000R0000000000"];
                         }
                     }
                     
                 }
             }
             
             //             NSLog(@"lastCollectionReference %@",[SWDefaults lastCollectionReference]);
             //             NSLog(@"lastOrderReference %@",[SWDefaults lastOrderReference]);
             //             NSLog(@"lastPerformaOrderReference %@",[SWDefaults lastPerformaOrderReference]);
             //             NSLog(@"lastReturnOrderReference %@",[SWDefaults lastReturnOrderReference]);
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastCollectionReference %@",[SWDefaults lastCollectionReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastOrderReference %@",[SWDefaults lastOrderReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastPerformaOrderReference %@",[SWDefaults lastPerformaOrderReference]]];
             //             [self writeActivationLogToTextFile:[NSString stringWithFormat:@"lastReturnOrderReference %@",[SWDefaults lastReturnOrderReference]]];
             
             
             
             //Singleton *single = [Singleton retrieveSingleton];
             if (!single.isActivated)
             {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                 [self.target performSelector:self.action withObject:nil];
#pragma clang diagnostic pop
             }
         }
         if([procName isEqualToString:@"sync_MC_ExecSendOrders"])
         {
             resultArray = [responseText JSONValue];
             ////NSLog(@"Result Array %@",resultArray);
             NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
             if([ProcResponse isEqualToString:@"Order imported successfully"])
             {
                 [self finalLogPrint:@"Order sent successfully"];
                 [[SWDatabaseManager retrieveManager] deleteSalesOrder];
                 NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
                 if([signatureArray count]!=0)
                 {
                     [self uploadFile];
                 }
                 else
                 {
                     NSDateFormatter *formatter = [NSDateFormatter new];
                     [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                     NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                     [formatter setLocale:usLocale];
                     NSString *dateString =  [formatter stringFromDate:[NSDate date]];
                     
                     [SWDefaults setLastSyncOrderSent:orderCountString];
                     [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
                     
                     orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
                     [pendingOrder setText:orderCountString];
                     
                     [SWDefaults setLastSyncDate:dateString];
                     lastSyncLable.text = [SWDefaults lastSyncDate];
                     
                     [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
                     lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
                     
                     [self finalLogPrint:@"Order upload successfully completed"];
                     
                     
                     //add a notification here to check the status in visit options, send order button action
                     
                     
                     NSMutableDictionary* sendOrderStatusDict=[[NSMutableDictionary alloc]init];
                     
                     
                     [sendOrderStatusDict setObject:orderCountString forKey:@"OrderCount"];
                     
                     [sendOrderStatusDict setObject:lastSyncLable.text forKey:@"Time"];
                     
                     [sendOrderStatusDict setObject:lastSyncStatusLable.text forKey:@"Status"];
                     
                     
                     
                     
                     NSDictionary *sendOrder = [NSDictionary dictionaryWithObject:sendOrderStatusDict forKey:@"sendOrderStatus"];
                     
                     
                     SWVisitOptionsViewController * swVisitOptionsVC=[[SWVisitOptionsViewController alloc]init];
                     
                     
                    

                     NSLog(@"notification added");

                     
                     
                     [[NSNotificationCenter defaultCenter] addObserver:swVisitOptionsVC selector:@selector(notificationReceived:) name:@"ordersSentSuccessfully" object:lastSyncStatusLable.text];
                     
                     
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"ordersSentSuccessfully" object:lastSyncStatusLable.text];
                     
                     


                     
                     
                     [[DataSyncManager sharedManager] hideCustomIndicator];
                     formatter=nil;
                     usLocale=nil;
                     [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
                     
                 }
             }
             else
             {
                 [self finalLogPrint:@"Order uplaod failed"];
                 [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
                 [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:ProcResponse withController:self];
             }
         }
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
        [self.view setUserInteractionEnabled:YES];
        [self finalLogPrint: [NSString stringWithFormat:@"Process failed.%@",error.localizedDescription]];
        [[DataSyncManager sharedManager] hideCustomIndicator];
        [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
        [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
        lastSyncStatusLable.text=[SWDefaults lastSyncStatus];
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Server unreachable at the moment" withController:self];
    }];
    
    //call start on your request operation
    [operation start];
}
-(void)uploadFile
{
    NSArray *signatureImageArray= [[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
    NSString *strUrl =[serverAPI stringByAppendingString:@"UploadFile"];
    NSURL *url = [NSURL URLWithString: strUrl];
    
    NSString *path = [signatureImageArray objectAtIndex:uploadFileCount];
    [self finalLogPrint:[NSString stringWithFormat:@"Files to be uploaded : %d",[signatureImageArray count]-uploadFileCount ]];
    [self finalLogPrint:@"Uploading Files"];
    
    usernameActivate = [[SWDefaults userProfile] stringForKey:@"Username"];
    passwordActivate = [[SWDefaults userProfile] stringForKey:@"Password"];
    
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            ResponseFormat, @"ResponseFormat",
                            usernameActivate, @"Username",
                            passwordActivate, @"Password",
                            strDeviceID, @"DeviceID",
                            ClientVersion, @"ClientVersion",
                            FileType, @"FileType",
                            nil];
    
    
    NSData *postData = [[NSData alloc] initWithContentsOfFile:path];
    
    if (self.request)
    {
        self.request = nil;
    }
    // self.request = [[AFHTTPClient alloc] initWithBaseURL:url];
    self.request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSMutableURLRequest *afRequest = [self.request multipartFormRequestWithMethod:@"POST"
                                                                             path:nil
                                                                       parameters:params
                                                        constructingBodyWithBlock:^(id < AFMultipartFormData > formData)
                                      {
                                          [formData appendPartWithFileData:postData
                                                                      name:[path lastPathComponent]
                                                                  fileName:[path lastPathComponent]
                                                                  mimeType:@"image/jpeg"];
                                      }
                                      ];
    if (operation)
    {
        operation = nil;
    }
    operation = [[AFHTTPRequestOperation alloc] initWithRequest:afRequest];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
     {
         if (totalBytesExpectedToWrite == 0)
         {
         }
         else
         {
         }
     }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operationQ, id responseObject)
     {
         //NSString *responseString =[self.request responseString];
         // //NSLog(@"Upload response %@", responseString);
         NSArray *signatureArray=[[DataSyncManager sharedManager] dbGetSignatureImageFilePath];
         NSString *strImageName =[signatureArray objectAtIndex:uploadFileCount];
         [self finalLogPrint:[NSString stringWithFormat:@"%@ uploaded successfully",[strImageName lastPathComponent]]];
         
         if(uploadFileCount != [signatureArray count]-1)
         {
             uploadFileCount++;
             [self uploadFile];
         }
         else
         {
             [self finalLogPrint:@"Order upload successfully completed"];
             NSFileManager *fm = [NSFileManager defaultManager];
             
             
             
             
             NSString *directory;
             
             
             //iOS 8 support
             
             if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                 directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Signature/"];
             }
             
             else
             {
                 directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature/"];
             }
             
             
             
             
             
             
             NSError *error = nil;
             
             for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
             {
                 @autoreleasepool {
                     
                     BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
                     if (!success || error)
                     {
                         // it failed.
                         //NSLog(NSLocalizedString(@"Error", nil));
                     }
                 }
             }
             [[DataSyncManager sharedManager] hideCustomIndicator];
             
             NSDateFormatter *formatter = [NSDateFormatter new];
             [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
             NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
             [formatter setLocale:usLocale];
             NSString *dateString =  [formatter stringFromDate:[NSDate date]];
             
             [SWDefaults setLastSyncOrderSent:orderCountString];
             [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
             
             orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
             [pendingOrder setText:orderCountString];
             
             [SWDefaults setLastSyncDate:dateString];
             lastSyncLable.text = [SWDefaults lastSyncDate];
             
             [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
             lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
             
             
             
             
             formatter=nil;
             usLocale=nil;
             [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
         }
         
         
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [self.view setUserInteractionEnabled:YES];
         orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
         [pendingOrder setText:orderCountString];
         
         NSDateFormatter *formatter = [NSDateFormatter new];
         [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
         NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
         [formatter setLocale:usLocale];
         NSString *dateString =  [formatter stringFromDate:[NSDate date]];
         [SWDefaults setLastSyncDate:dateString];
         lastSyncLable.text = [SWDefaults lastSyncDate];
         
         [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
         lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
         
         [self finalLogPrint:[[operationQ error] localizedDescription]];
         [[DataSyncManager sharedManager] hideCustomIndicator];
         formatter=nil;
         usLocale=nil;
         [self performSelector:@selector(stopAnimationofSendOrder) withObject:nil afterDelay:0.1];
         //NSLog(@" Error - Statistics file upload failed: \"%@\"",[[operation error] localizedDescription]);
     }];
    
    [operation start];
}
- (void)backtoActivation
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    
    [[DataSyncManager sharedManager] hideCustomIndicator];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:@"done"];
#pragma clang diagnostic pop
}
- (void)ProcessCompleted
{
    [self.view setUserInteractionEnabled:YES];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isFullSyncDone = YES;
    single.isFullSyncCollection = YES;
    single.isFullSyncCustomer = YES;
    single.isFullSyncSurvey = YES;
    single.isFullSyncProduct = YES;
    single.isFullSyncDashboard = YES;
    single.isFullSyncMessage = YES;
    
    if(!single.isActivated)
    {
        [SWDatabaseManager destroyMySingleton];
    }
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    [activityImageView stopAnimating];
    [self StopFullSyncAnimation ];
    
    [SWDefaults setLastSyncOrderSent:orderCountString];
    [lastSyncOrderSent setText:[SWDefaults lastSyncOrderSent]];
    [self finalLogPrint:@"Sync Status : Completed"];
    [DataSyncManager sharedManager].progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png" cache:NO]];
    [DataSyncManager sharedManager].progressHUD.mode = MBProgressHUDModeCustomView;
    [DataSyncManager sharedManager].progressHUD.labelText = @"Completed";
    lblSyncProgressStatus.text = @"Completed";
    if([self.delegate respondsToSelector:@selector(updateProgressDelegateWithType:andTitle:andDetail:andProgress:)])
    {
        [self.delegate updateProgressDelegateWithType:[DataSyncManager sharedManager].progressHUD.mode andTitle:[DataSyncManager sharedManager].progressHUD.labelText andDetail:[DataSyncManager sharedManager].progressHUD.detailsLabelText andProgress:[DataSyncManager sharedManager].progressHUD.progress];
    }
    [[DataSyncManager sharedManager].progressHUD hide:YES afterDelay:0.0];
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [SWDefaults setLastSyncDate:dateString];
    [SWDefaults setLastFullSyncDate:[SWDefaults fetchCurrentDateTimeinDatabaseFormat]];
    [SWDefaults setLastSyncStatus:NSLocalizedString(@"Successful", nil)];
    lastSyncLable.text = [SWDefaults lastSyncDate];
    lastSyncStatusLable.text = [SWDefaults lastSyncStatus];
    progressBar.progress=1.0;
    
    [SWDefaults setCollectionCounter:0];
    
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    // collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    collectionCountString=nil;
    collectionCountString = @"0";
    [pendingOrder setText:orderCountString];
    [pendingReturn setText:returnCountString];
    [pendingCollection setText:collectionCountString];
    [pendingCollection setText:@"0"];
    
    
    //update the nsuser defauls for collection count as well because tbl collection will have data, even after full sync, it will show all the collections
    
    [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:@"collectionCount"];
    
    
    [[DataSyncManager sharedManager]hideCustomIndicator];
    //NSLog(@"hideCustomIndicator 9");
    //Singleton *single = [Singleton retrieveSingleton];
    if(!single.isActivated)
    {
        
        [self performSelector:@selector(backtoActivation) withObject:nil afterDelay:0.0];
    }
    else
    {
        
    }
    [SWDefaults setAppControl:[[SWDatabaseManager retrieveManager] dbGetAppControl]];
    [AppControl destroyMySingleton];
    //[Singleton destroyMySingleton];
    
    // NSLog(@"App Controll %@",[SWDefaults appControl]);
    formatter=nil;
    usLocale=nil;
}
- (void)finalLogPrint:(NSString *)text
{
    Singleton *single = [Singleton retrieveSingleton];
    
    
    logTextView.font = kFontWeblySleekSemiBold(14);
    // //NSLog(@"addLogToTextView Calls with : %@",text);
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , text]];
    
    logTextView.text = [logTextView.text stringByAppendingString:slashN];
    CGPoint p = [logTextView contentOffset];
    [logTextView setContentOffset:p animated:YES];
    [logTextView scrollRangeToVisible:NSMakeRange([logTextView.text length], 0)];
    
    if(single.isFromSync==1)
    {
        //[self writeActivationLogToTextFile:text];
        if([self.delegate respondsToSelector:@selector(updateActivationTextDelegate:)])
        {
            [self.delegate updateActivationTextDelegate:slashN];
        }
    }
    else
    {
        //[self writeSyncLogToTextFile:text];
        if([self.delegate respondsToSelector:@selector(updateActivationTextDelegate:)])
        {
            [self.delegate updateActivationTextDelegate:slashN];
        }
    }
    formatter=nil;
    usLocale=nil;
}
-(void) writeActivationLogToTextFile:(NSString *)textString
{
    //get the documents directory:
    
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , textString]];
    
    
    
    
    NSString *documentsDirectory;
    
    
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Activation_Log.txt",documentsDirectory];
    
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandler closeFile];
    formatter=nil;
    usLocale=nil;
    
}
-(void) writeSyncLogToTextFile:(NSString *)textString
{
    //get the documents directory:
    
    NSString *slashN = @"\n";
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    slashN = [slashN stringByAppendingString:[NSString stringWithFormat:@"%@ : %@",dateString , textString]];
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    
    
    
    //	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //	NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/Sync_Log.txt",documentsDirectory];
    
    NSFileHandle *fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:fileName];
    [fileHandler seekToEndOfFile];
    [fileHandler writeData:[slashN dataUsingEncoding:NSUTF8StringEncoding]];
    [fileHandler closeFile];
    formatter=nil;
    usLocale=nil;
    
}
- (void)stopAnimation
{
    [self.view setUserInteractionEnabled:YES];
    [self StopFullSyncAnimation];
    
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    
    if(timerObj)
    {
        [timerObj invalidate];
        timerObj=nil;
    }
    [SWDefaults setLastSyncStatus:NSLocalizedString(@"Failed", nil)];
    progressBar.progress = 0.0;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSString *dateString =  [formatter stringFromDate:[NSDate date]];
    [SWDefaults setLastSyncDate:dateString];
    
    orderCountString = [[SWDatabaseManager retrieveManager] dbGetOrderCount];
    returnCountString = [[SWDatabaseManager retrieveManager] dbGetReturnCount];
    
    
    //data from tbl collection will not be cleared even after full sync so do it with a flag set it to 0 when full sync is done
    
    
    
    
    //    collectionCountString = [[SWDatabaseManager retrieveManager] dbdbGetCollectionCount];
    
    
    
    NSInteger collectionCount=[[NSUserDefaults standardUserDefaults]integerForKey:@"collectionCount"];
    
    if (collectionCount) {
        collectionCountString=[NSString stringWithFormat:@"%d",collectionCount];
        
    }
    
    
    [pendingOrder setText:orderCountString];
    [pendingReturn setText:returnCountString];
    [pendingCollection setText:collectionCountString];
    
    [activityImageView stopAnimating];
    [self StopFullSyncAnimation];
    [[DataSyncManager sharedManager]hideCustomIndicator];
    
    formatter=nil;
    usLocale=nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    if(!single.isActivated)
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        
        
        @try
        {
           // [self.target performSelector:self.action withObject:@"error"];

        }
         @catch (NSException *exception) {
            
             NSLog(@"exception is %@", exception);
         }
        
        
        
#pragma clang diagnostic pop
    }
}
- (void)stopAnimationofSendOrder
{
    
    
    //add a notification here to check whether order has been send to check status from visit options screen
    
    
    
    
    
    
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    // [Singleton destroyMySingleton];
    //    [DashboardService destroyMySingleton];
    //    [SWSessionService destroyMySingleton];
    //    [SWCustomerService destroyMySingleton];
    //    [ProductService destroyMySingleton];
    //    [DistributionService destroyMySingleton];
    //    [SalesOrderService destroyMySingleton];
    //    [SendOrderService destroyMySingleton];
    //    [SWCustomerService destroyMySingleton];
    //    [SWRouteService destroyMySingleton];
    
    [activityImageView2 stopAnimating];
}
- (NSData *)dataFromHexString:(NSString *)string
{
    string = [string lowercaseString];
    NSMutableData *data= [NSMutableData new];
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i = 0;
    int length = string.length;
    while (i < length-1) {
        char c = [string characterAtIndex:i++];
        if (c < '0' || (c > '9' && c < 'a') || c > 'f')
            continue;
        byte_chars[0] = c;
        byte_chars[1] = [string characterAtIndex:i++];
        whole_byte = strtol(byte_chars, NULL, 16);
        [data appendBytes:&whole_byte length:1];
        
    }
    return data;
}
- (NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces :(NSData *)data
{
    const unsigned char* bytes = (const unsigned char*)[data bytes];
    NSUInteger nbBytes = [data length];
    //If spaces is true, insert a space every this many input bytes (twice this many output characters).
    static const NSUInteger spaceEveryThisManyBytes = 4UL;
    //If spaces is true, insert a line-break instead of a space every this many spaces.
    static const NSUInteger lineBreakEveryThisManySpaces = 4UL;
    const NSUInteger lineBreakEveryThisManyBytes = spaceEveryThisManyBytes * lineBreakEveryThisManySpaces;
    NSUInteger strLen = 2*nbBytes + (spaces ? nbBytes/spaceEveryThisManyBytes : 0);
    
    NSMutableString* hex = [[NSMutableString alloc] initWithCapacity:strLen];
    for(NSUInteger i=0; i<nbBytes; ) {
        [hex appendFormat:@"%02X", bytes[i]];
        //We need to increment here so that the every-n-bytes computations are right.
        ++i;
        
        if (spaces) {
            if (i % lineBreakEveryThisManyBytes == 0) [hex appendString:@"\n"];
            else if (i % spaceEveryThisManyBytes == 0) [hex appendString:@" "];
        }
    }
    return hex;
}
- (void)StartOrderSyncAnimation
{
    
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag==1000) {
        alertView=nil;
    }
    
    //do whatever with the result
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    
}
- (void)RequestFailureAlert:(NSString *)Message
{
    [self.view setUserInteractionEnabled:YES];
    if(Message.length!=0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:@"Start",nil];
        alertType=requestFailureAlert;
        [ErrorAlert show];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    self.request=nil;
    operation= nil;
    logTextView.text=nil;
    
    
}
-(void)cancelHTTPRequest
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
    [[self.request operationQueue] cancelAllOperations];
    self.request=nil;
    operation=nil;
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
