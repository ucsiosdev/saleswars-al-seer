

//
//  AlSeerSalesOrderViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSalesOrderViewController.h"
#import "AnimationUtility.h"
#import "SWDatabaseManager.h"
#import "SWVisitOptionsViewController.h"
#import "StockInfoSalesHistoryViewController.h"
#import "AlseerSalesOrderProductListTableViewCell.h"
#import "TableCellHeaderSalesOrder.h"

@interface AlSeerSalesOrderViewController ()

@end

@implementation AlSeerSalesOrderViewController

@synthesize parentViewController,customerDict,preOrdered,preOrderedItems,isOrderConfirmed,wareHouseDataSalesOrderArray,uomButton,targetLbl,salesLbl,balanceToGoLbl,achievementPercentLbl,infoButton,clearShadowView,addButtonShadowView,popoverController,uomConversionFactorLbl,viewUOMCode;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title= @"Sales Order";
    
    
    NSString *cellID = @"AlseerSalesOrderProductListTableViewCell";
    [productTableView registerNib:[UINib nibWithNibName:cellID bundle:nil] forCellReuseIdentifier:cellID];
    
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addButton.titleLabel setFont:bodySemiBold];
    
    viewUOMCode.backgroundColor = [UIColor whiteColor];
    [self setShadowOnUILayer:viewUOMCode.layer];

    
    [addButton setTitle:@"Add to Order" forState:UIControlStateNormal];
    
//    lblUOMCode.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
//    lblUOMCode.layer.borderWidth = 1.0;

    txtProductQty.backgroundColor = [UIColor whiteColor];
    lblWholesalePrice.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    lblWholesalePrice.layer.borderWidth = 1.0;

    
    txtProductQty.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    txtProductQty.layer.borderWidth = 1.0;

    txtProductQty.layer.sublayerTransform = CATransform3DMakeTranslation(3.0f, 0.0f, 0.0f);

    
    
    lblAvlStock.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    lblAvlStock.layer.borderWidth = 1.0;
    
    
    [[ UILabel appearanceWhenContainedIn:[AlSeerSalesOrderCalculationViewController class], nil]setFont:headerFont];
    
    
    
    //adding drop shadow
    

    clearShadowView.layer.shadowOffset = CGSizeMake(1, 1);
    clearShadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
    clearShadowView.layer.shadowRadius = 2.0f;
    clearShadowView.layer.shadowOpacity = 0.80f;
    clearShadowView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:clearShadowView.layer.bounds] CGPath];
    
    
    addButtonShadowView.layer.shadowOffset = CGSizeMake(1, 1);
    addButtonShadowView.layer.shadowColor = [[UIColor blackColor] CGColor];
    addButtonShadowView.layer.shadowRadius = 2.0f;
    addButtonShadowView.layer.shadowOpacity = 0.80f;
    addButtonShadowView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:addButtonShadowView.layer.bounds] CGPath];

    
    
    
    
   // infoButton.backgroundColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];
    
    
    [infoButton setTitleColor:[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1] forState:UIControlStateNormal];
    
    
    /*for (UIView *i in self.view.subviews){
        if([i isKindOfClass:[UIView class]]){
            UIView *newLbl = (UIView *)i;
            if(newLbl.tag == 1|| newLbl.tag == 2||newLbl.tag == 3||newLbl.tag == 4||newLbl.tag == 5 ||newLbl.tag == 6||newLbl.tag == 7){
                /// Write your code
                
                
                
                CALayer *layer = newLbl.layer;
                layer.shadowOffset = CGSizeMake(1, 1);
                layer.shadowColor = [[UIColor blackColor] CGColor];
                layer.shadowRadius = 2.0f;
                layer.shadowOpacity = 0.80f;
                layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
                
            }
        }
        
        
        
    }*/

    
    
    noPricesAvailableStatus=NO;
    productTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    isPinchDone = NO;
    //check if app control has multi UOM selected
    
    if ([[SWDefaults checkMultiUOM]isEqualToString:@"N"]) {
        
        
        [uomButton setHidden:YES];
    }
    NSLog(@"check customer dict in Al seer view did load %@", [customerDict description]);
    
    Singleton *single = [Singleton retrieveSingleton];
    
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
        
    {
        [uomButton setHidden:YES];
        
    }
    
    
    if ([self.parentViewString isEqualToString:@"SO"])
    {
        
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andCategory:[SWDefaults productCategory]] ;
    }
    else if ([self.parentViewString isEqualToString:@"MO"]) {
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andOrders:self.preOrdered andManage:self.isOrderConfirmed];
        
        
        
        // oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
        
        NSLog(@"ware house data in old sales order %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
        
        //OLA!!
        if ([self.isOrderConfirmed isEqualToString:@"confirmed"])
        {
            [addButton setHidden:YES];
            [txtDefBonus setUserInteractionEnabled:NO];
            [txtDiscount setUserInteractionEnabled:NO];
            [txtfldWholesalePrice setUserInteractionEnabled:NO];
            [txtFOCQty setUserInteractionEnabled:NO];
            [txtNotes setUserInteractionEnabled:NO];
            [txtProductQty setUserInteractionEnabled:NO];
            [txtRecBonus setUserInteractionEnabled:NO];
            [focProductBtn setUserInteractionEnabled:NO];
        }
    }
    else if ([self.parentViewString isEqualToString:@"TO"]) {
        oldSalesOrderVC = [[AlSeerSalesOrderCalculationViewController alloc] initWithCustomer:self.customerDict andTemplate:self.preOrdered andTemplateItem:self.preOrderedItems] ;
    }
    
    NSLog(@"check parent view string %@", self.parentViewString);
    
    oldSalesOrderVC.view.frame = bottomOrderView.bounds;
    oldSalesOrderVC.view.clipsToBounds = YES;
    
    
    // moving ware house data to old sales order
    
    NSLog(@"data before moving %@", [wareHouseDataSalesOrderArray description]);
    oldSalesOrderVC.wareHouseDataSalesOrder=wareHouseDataSalesOrderArray;
    
    NSLog(@"data after moving %@", [oldSalesOrderVC.wareHouseDataSalesOrder description]);
    
    
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];
    
    
    
    NSLog(@"check product category %@", [SWDefaults productCategory]);
    
    
    
    if ([[SWDefaults productCategory] count]>0) {
        productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];

    }
    else
    {
        
        
        NSMutableDictionary* categoryDict=[[NSMutableDictionary alloc]init];
        
        //setting categories dict here for customers with dues
        
        
       if ([self.parentViewString isEqualToString:@"MO"]) {
           
       }
        
        else
        {
            [categoryDict setObject:[[wareHouseDataSalesOrderArray objectAtIndex:0]valueForKey:@"Category"] forKey:@"Category"];
            
            [categoryDict setObject:[[wareHouseDataSalesOrderArray objectAtIndex:0]valueForKey:@"Org_ID"] forKey:@"Org_ID"];
            
            productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:categoryDict]];
            
            
            NSLog(@"check product array for product UB03843 %@ ", [productArray description]);
            
        }
        
        
        
        
        
        

        
    }
    
    NSLog(@"check product array %@", [productArray description]);
    
    
        filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Agency"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Agency"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    [self borderView:bonusView];
    [self borderView:stockView];
    [self borderView:salesHistoryView];
    [self borderView:dragParentView];
    // [self borderView:dragButton];
    
    //changed to [customerDict stringForKey:@"Customer_Name"]
    //    NSLog(@"OLA!!******customer name %@",[customerDict stringForKey:@"Customer_Name"]);
    //    NSLog(@"OLA!!******customer name %@",[[SWDefaults customer] stringForKey:@"Customer_Name"]);
    lblCustomerLabel.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerLabel.font = headerTitleFont;
    lblProductName.font =headerTitleFont;
//    lblProductCode.font =BoldSemiFontOfSize(12);
//    lblProductBrand.font=BoldSemiFontOfSize(12);
//    lblWholesalePrice.font=BoldSemiFontOfSize(12);
//    txtfldWholesalePrice.font=BoldSemiFontOfSize(12);
//    lblRetailPrice.font=BoldSemiFontOfSize(12);
//    lblUOMCode.font=BoldSemiFontOfSize(12);
//    lblAvlStock.font=BoldSemiFontOfSize(12);
//    lblExpDate.font=BoldSemiFontOfSize(12);
//    lblPriceLabel.font=BoldSemiFontOfSize(12);
    
    //    txtProductQty.font=LightFontOfSize(14);
    //    txtDefBonus.font=LightFontOfSize(14);
    //    txtRecBonus.font=LightFontOfSize(14);
    //    txtDiscount.font=LightFontOfSize(14);
    //    txtFOCQty.font=LightFontOfSize(14);
    //    txtNotes.font=LightFontOfSize(14);
    
//    lblCustomerLabel.textColor = UIColorFromRGB(0x4A5866);
//   // lblProductName.textColor = UIColorFromRGB(0x4687281);
//    lblProductCode.textColor =UIColorFromRGB(0x4687281);
//    lblProductBrand.textColor=UIColorFromRGB(0x4687281);
//    lblWholesalePrice.textColor=UIColorFromRGB(0x4687281);
//    txtfldWholesalePrice.textColor=UIColorFromRGB(0x4687281);
//    lblRetailPrice.textColor=UIColorFromRGB(0x4687281);
//    lblUOMCode.textColor=UIColorFromRGB(0x4687281);
//    lblAvlStock.textColor=UIColorFromRGB(0x4687281);
//    lblExpDate.textColor=UIColorFromRGB(0x4687281);
//    lblPriceLabel.textColor=UIColorFromRGB(0x4687281);
//    
//    txtProductQty.textColor=UIColorFromRGB(0x4687281);
//    txtDefBonus.textColor=UIColorFromRGB(0x4687281);
//    txtRecBonus.textColor=UIColorFromRGB(0x4687281);
//    txtDiscount.textColor=UIColorFromRGB(0x4687281);
//    txtFOCQty.textColor=UIColorFromRGB(0x4687281);
//    txtNotes.textColor=UIColorFromRGB(0x4687281);
//    txtNotes.textColor = [UIColor darkTextColor ];
    
    
    candySearchBar.backgroundColor = UIColorFromRGB(0xF6F7FB);
//    
//    lblTitleProductCode.textColor=UIColorFromRGB(0x4A5866);
//    lblTitleWholesalePrice.textColor=UIColorFromRGB(0x4A5866);
//    lblTitleRetailPrice.textColor=UIColorFromRGB(0x4A5866);
//    lblTitleUOMCode.textColor=UIColorFromRGB(0x4A5866);
//    lblTitleAvlStock.textColor=UIColorFromRGB(0x4A5866);
//    lblTitleExpDate.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleProductQty.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleDefBonus.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleRecBonus.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleDiscount.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleFOCQty.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleFOCItem.textColor=UIColorFromRGB(0x4A5866);
//    txtTitleNotes.textColor=UIColorFromRGB(0x4A5866);
    
//    lblTitleProductCode.font=LightFontOfSize(14);
//    lblTitleWholesalePrice.font=LightFontOfSize(14);
//    lblTitleRetailPrice.font=LightFontOfSize(14);
//    lblTitleUOMCode.font=LightFontOfSize(14);
//    lblTitleAvlStock.font=LightFontOfSize(14);
//    lblTitleExpDate.font=LightFontOfSize(14);
//    txtTitleProductQty.font=LightFontOfSize(14);
//    txtTitleDefBonus.font=LightFontOfSize(14);
//    txtTitleRecBonus.font=LightFontOfSize(14);
//    txtTitleDiscount.font=LightFontOfSize(14);
//    txtTitleFOCQty.font=LightFontOfSize(14);
//    txtTitleFOCItem.font=LightFontOfSize(14);
//    txtTitleNotes.font=LightFontOfSize(14);
//    
//    lblSelectProduct.font=BoldSemiFontOfSize(20);
    
    dragParentView.backgroundColor = UIColorFromRGB(0x343F5F);
    //focProductBtn.titleLabel.font =BoldSemiFontOfSize(14);
    
    [txtNotes.layer setCornerRadius:7.0f];
    [txtNotes.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [txtNotes.layer setBorderWidth:1.0f];
    
    //    [txtfldWholesalePrice setPlaceholder:[NSString stringWithFormat:@"(in %@)",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]]];
    [txtfldWholesalePrice setDelegate:self];
    [txtfldWholesalePrice setKeyboardType:UIKeyboardTypeDecimalPad];
    [txtfldWholesalePrice setHidden:YES];
    
    lblCustomerName.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerNo.text = [customerDict stringForKey:@"Customer_No"];
    lblAvailableBalance.text = [[customerDict stringForKey:@"Avail_Bal"] currencyString];
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)];
    
    [close setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = close;
    
    UIBarButtonItem *btnSaveOrder = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save Order", nil) style:UIBarButtonItemStyleDone target:self action:@selector(saveOrder:)];
    
    [btnSaveOrder setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    
    
   
    
    
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        
        self.navigationItem.rightBarButtonItem = nil;
    }else{
         self.navigationItem.rightBarButtonItem = btnSaveOrder;
    }
    
    [self setAllViewBorder];
    [self setUpView];
    [self addShadowOnUI];
    
    if (@available(iOS 15.0, *)) {
        productTableView.sectionHeaderTopPadding = 0;
        focProductTableView.sectionHeaderTopPadding = 0;
        self.salesOrderTableView.sectionHeaderTopPadding = 0;
    }
}

-(void)addShadowOnUI{
    [self setShadowOnUILayer:viewUOMCode.layer];
    [self setShadowOnUILayer:txtProductQty.layer];    
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [candySearchBar layoutSubviews];
    
    //handle case: Sometimes search bar's cancel button overcomes on search bar's text filed
    if (candySearchBar.showsCancelButton){
        return;
    }
    

    
    for(UIView *view in candySearchBar.subviews)
    {
        for(UITextField *textfield in view.subviews)
        {
            if ([textfield isKindOfClass:[UITextField class]]) {
                textfield.frame = CGRectMake(13.0f, 13.0f, 220, 30.0f);
            }
        }
    }
    /*
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.path = [UIBezierPath bezierPathWithRoundedRect: productListView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight |  UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    productListView.layer.mask = layer;
     */
    
    /*
    CAShapeLayer *layer1 = [CAShapeLayer layer];
    layer1.path = [UIBezierPath bezierPathWithRoundedRect: orderItemContainerView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight |  UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    orderItemContainerView.layer.mask = layer1;
     */
}

-(void)setUpView{
    candySearchBar.barTintColor = [UIColor colorWithRed:235.0/255 green:251.0/255 blue:249.0/255 alpha:1.0];
    
    //set border of search bar text filed as per UI/Design
    for (id object in [[[candySearchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            
            textFieldObject.layer.borderColor = [UIColor lightGrayColor].CGColor;
            textFieldObject.layer.borderWidth = 0.5;
            textFieldObject.layer.cornerRadius = 4.5;
            break;
        }
    }
    
    
    candySearchBar.layer.borderWidth = 1;
    candySearchBar.layer.borderColor = UIColorFromRGB(0xE1E8ED).CGColor;
}

-(void) setAllViewBorder{
    
    //customerDetailView.layer.cornerRadius = 8.0;
    productDetailView.layer.cornerRadius = 8.0;
    agencyTargetInfoView.layer.cornerRadius = 8.0;
   // orderItemContainerView.layer.cornerRadius = 8.0;
  //  bottomOrderView.layer.cornerRadius = 8.0;

    placeHolderView.layer.cornerRadius = 8.0;
 //   productListView.layer.cornerRadius = 8.0;
   // productListView.layer.cornerRadius = 8.0;


    txtProductQty.layer.cornerRadius = 8.0;
    lblWholesalePrice.layer.cornerRadius = 8.0;
    lblAvlStock.layer.cornerRadius = 8.0;
   // lblUOMCode.layer.cornerRadius = 8.0;

    addButton.layer.cornerRadius = 8.0;
    clearButton.layer.cornerRadius = 8.0;

//    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:lblUOMCode.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(8.0, 8.0)];
//
//    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//    maskLayer.frame = lblUOMCode.bounds;
//    maskLayer.path  = maskPath.CGPath;
//    lblUOMCode.layer.mask = maskLayer;
    
    //uomConversionFactorLbl
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:uomConversionFactorLbl.bounds byRoundingCorners:(UIRectCornerTopRight| UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = uomConversionFactorLbl.bounds;
    maskLayer1.path  = maskPath1.CGPath;
    uomConversionFactorLbl.layer.mask = maskLayer1;
    
//    uomConversionFactorLbl.layer.borderColor = [UIColor greenColor].CGColor;
//    uomConversionFactorLbl.layer.borderWidth = 3.0;
//    uomConversionFactorLbl.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
   // customerDetailView.layer.masksToBounds = YES;
  //  productDetailView.layer.masksToBounds = YES;
  //  agencyTargetInfoView.layer.masksToBounds = YES;
//   orderItemContainerView.layer.masksToBounds = YES;
//    bottomOrderView.layer.masksToBounds = YES;

    placeHolderView.layer.masksToBounds = YES;
   // productListView.layer.masksToBounds = YES;


    
   // txtProductQty.layer.masksToBounds = YES;
    lblWholesalePrice.layer.masksToBounds = YES;
    lblAvlStock.layer.masksToBounds = YES;
   // lblUOMCode.layer.masksToBounds = YES;
    
    addButton.layer.masksToBounds = YES;
    clearButton.layer.masksToBounds = YES;
    
    
   productTableView.layer.cornerRadius = 8.0;
   productTableView.layer.masksToBounds = YES;
    
    
    bottomOrderView.layer.cornerRadius = 8.0;
    bottomOrderView.layer.masksToBounds = YES;

}
-(void)saveOrder:(id)sender{
    [oldSalesOrderVC saveOrder:nil];
    
}

- (void)closeVisit:(id)sender {
    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        NSLog(@"check the stack %@", [self.navigationController.viewControllers description]);
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[SWVisitOptionsViewController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
    }];
    
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
    }];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Alert", nil) andMessage:NSLocalizedString(@"Would you like to close this order?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.translucent = NO;
    
    
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:headerTitleFont, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    self.title=@"Sales Order";
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];
    
    

    
    NSLog(@"ware house data in sales order %@", [wareHouseDataSalesOrderArray description]);
    
    
    OrderAdditionalInfoViewController * orderVC=[[OrderAdditionalInfoViewController alloc]init];
    
    orderVC.warehouseDataArray=wareHouseDataSalesOrderArray ;
    
    
    NSLog(@"ware house data being pushed to final destination %@", [orderVC.warehouseDataArray description]);
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate = self;
}


-(void)viewDidDisappear:(BOOL)animated
{
    oldSalesOrderVC.delegate = nil;
    txtfldWholesalePrice.delegate=nil;
    productStockViewController.delegate=nil;
}
-(void)borderView:(UIView *)view
{
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    view.layer.shadowOpacity = 0.5f;
    view.layer.shadowPath = shadowPath.CGPath;
    [view.layer setBorderColor:UIColorFromRGB(0xE6E6E6).CGColor];
    [view.layer setBorderWidth:2.0f];
    
}

#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            return 1;
            
        }
        else
        {
            return [finalProductArray count];
        }
    }
    else
    {
        return 1;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return [filteredCandyArray count];
            
        } else {
            NSArray *tempDict =[productDictionary objectForKey:[finalProductArray objectAtIndex:section]];
            
            return [_collapsedSections containsObject:@(section)] ? [tempDict count] :0 ;
            
        }
    }
    else
    {
        return [productArray count];
        
    }
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    return 0.01f;
//}
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    }
    else
    {
        return 0;
    }
    
}



//CRASH FIXED
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//}



- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            static NSString* tableID=@"TableCellHeaderSalesOrder";
            TableCellHeaderSalesOrder * cell=[tv dequeueReusableCellWithIdentifier:tableID];
            
            if (cell==nil) {
                NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:tableID owner:self options:nil];
                cell= [cellArray objectAtIndex:0];
            }
            
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            [cell.btnItemName setTitle:[[row objectAtIndex:0] stringForKey:@"Agency"] forState:UIControlStateNormal];
            
            
            [cell.btnItemName setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
            cell.btnItemName.tag = s;
            [cell.btnItemName addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnItemName.titleLabel.font = kFontWeblySleekSemiBold(16);
            cell.backgroundColor = [UIColor whiteColor];
            
            bool shouldCollapse = ![_collapsedSections containsObject:@(s)];
            if (shouldCollapse) {
                [cell.btnItemName setImage:[UIImage imageNamed:@"plusIcon"] forState:UIControlStateNormal];
                [cell.btnItemName setTitleColor:UIColorFromRGB(0x6A6F7B) forState:UIControlStateNormal];
            }else {
                [cell.btnItemName setImage:[UIImage imageNamed:@"minusIon"] forState:UIControlStateNormal];
                [cell.btnItemName setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
                
            }
            
            CGFloat spacing = 210;
            CGFloat spacingLeft = -14;
            cell.btnItemName.titleEdgeInsets = UIEdgeInsetsMake(0, spacingLeft, 0, 36); // top, left, bottom, right
            cell.btnItemName.imageEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
            
            return cell;
    /*if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0x6A6F7B);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Agency"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=kFontWeblySleekSemiBold(14);
            
            
            [result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            
            return headerView;
        }
    }
    else
    {
        return nil;
    }*/
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"AlseerSalesOrderProductListTableViewCell";
    AlseerSalesOrderProductListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:identifier owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            
            if (filteredCandyArray.count <= indexPath.row){
                return cell;
            }
            
            
            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            cell.productDescriptionLabel.text = [row stringForKey:@"Description"];
            cell.productItemCodeLabel.text = [row stringForKey:@"Item_Code"];

        }
        else
        {
            
            if (finalProductArray.count <= indexPath.section){
                return cell;
            }
            
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            
            NSLog(@"check row for lot qty %@", [row description]);

            cell.productDescriptionLabel.text = [row stringForKey:@"Description"];
            cell.productItemCodeLabel.text = [row stringForKey:@"Item_Code"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
            {
                cell.tag = 1;
                cell.statusIndicatorView.backgroundColor = [UIColor redColor];
            }
            else
            {
                cell.tag = 0;
                cell.statusIndicatorView.backgroundColor = [UIColor clearColor];
            }
        }
    }
    else
    {
//        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
//        cell.textLabel.text = [row stringForKey:@"Description"];
//        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
//        {
//            cell.textLabel.textColor = [UIColor redColor];
//        }
//        else
//        {
//            cell.textLabel.textColor=[UIColor darkTextColor ];
//        }
    }
    cell.productDescriptionLabel.font = kFontWeblySleekSemiBold(16);
    cell.productItemCodeLabel.font = kFontWeblySleekSemiBold(14);
    
    cell.productDescriptionLabel.textColor = UIColorFromRGB(0x2C394A);
    cell.productItemCodeLabel.textColor = UIColorFromRGB(0x6A6F7B);
   // cell.backgroundColor = [UIColor colorWithRed:249.0/255 green:249.0/255  blue:249.0/255  alpha:1.0];
    //cell.contentView.backgroundColor = [UIColor colorWithRed:249.0/255 green:249.0/255  blue:249.0/255  alpha:1.0];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self hidePlaceHolder];

    if ([txtProductQty isFirstResponder]) {
        [txtProductQty resignFirstResponder];
    }
    
    self.stockIndicatorLbl.hidden=YES;

    
    
    
    NSLog(@"selected product item id %@", [[productDictionary valueForKey:@"ItemID"]objectAtIndex:indexPath.row]);
    
    selectedUOMReference=nil;
    noPricesAvailableStatus=NO;
    
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    if (tableView.tag==1)
    {
        
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtRecBonus.text=@"";
        txtDiscount.text=@"";
        txtFOCQty.text=@"";
        txtNotes.text=@"";
        [addButton setTitle:@"Add to Order" forState:UIControlStateNormal];
        
        NSDictionary * row ;
        if (bSearchIsOn)
        {
            row = [filteredCandyArray objectAtIndex:indexPath.row];
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            row= [objectsForCountry objectAtIndex:indexPath.row];
        }
        
        NSLog(@"check row data %@", [row description]);
        
//        
//        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
//        
        
        
        NSArray* productDetail=[[SWDatabaseManager retrieveManager]fetchProductDetails:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"] UOMCode:[row stringForKey:@"Primary_UOM_Code"]];
        
        
        
//         NSArray* produstDetail=[[SWDatabaseManager retrieveManager]fetchProductDetails:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"] UOMCode:[mainProductDict stringForKey:@"UOM"]];
        
        mainProductDict=nil;
        mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[productDetail objectAtIndex:0]];
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        isDidSelectStock=YES;
        [stockBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [stockBtn setSelected:YES];
        
        
        //NSLog(@"OLA!!********* product %@",[produstDetail objectAtIndex:0]);
        
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
            [lblWholesalePrice setHidden:YES];
            [txtfldWholesalePrice setHidden:NO];
        }
        else
        {
            [lblWholesalePrice setHidden:NO];
            [txtfldWholesalePrice setHidden:YES];
        }
        // NSLog(@"Product %@",row);
        
        
        
        
        
        
        NSLog(@"check main product dict here %@", [mainProductDict description]);
        
        
        //check for Quality inspection stock and blocked stock to show indicator
        
        NSInteger selectedIndexpath=indexPath.row;
        
        NSString* productStockQry=[NSString stringWithFormat:@"SELECT  IFNULL(Lot_Qty,0) AS Lot_Qty ,IFNULL(Lot_No,0)AS Lot_No, IFNULL(Expiry_Date,0) AS Expiry_Date ,IFNULL(Custom_Attribute_1,0)AS QC,  IFNULL(Custom_Attribute_1,0)AS Blocked FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY  Expiry_Date ASC",[row stringForKey:@"ItemID"]];
        
        
        NSMutableArray* tempArray=[[NSMutableArray alloc]init];
        
        NSLog(@"product stock qry is %@", productStockQry);
        tempArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:productStockQry];
        if (tempArray.count>0) {
            NSLog(@"product stock array is SO %@", [tempArray description]);
            
            if ([[[tempArray valueForKey:@"QC"] objectAtIndex:0]integerValue]>0) {
                
                self.stockIndicatorLbl.hidden=NO;
                
                
            }
            
            
            
        }
        
     
        
        [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
        
        
        
        
        
        
        //check this in MO while update
        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
        
        
        NSLog(@"main product dict after selecting product %@", [mainProductDict description]);
        
        
        //fetch the converison factor here
        
      
        
        
        
        NSMutableArray* conversionArray=[self fetchConversionFactorforItemCode:[mainProductDict stringForKey:@"Item_Code"] withItemUOM:[mainProductDict stringForKey:@"UOM"]];
        NSLog(@"fetched conversion factor %@", [[conversionArray valueForKey:@"Conversion"]objectAtIndex:0]);
        
        NSInteger conversionVal=[[[conversionArray valueForKey:@"Conversion"]objectAtIndex:0] integerValue];
        mainProductDict[@"conversionfactor"] = [NSString stringWithFormat:@"%ld",(long)conversionVal];
        
        if (conversionVal>0) {
            
            uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ", (long)conversionVal]stringByAppendingString:@"EA"];
            
            
        }
        
        

        
        
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        if (![row objectForKey:@"Guid"])
        {
            [row setValue:[NSString createGuid] forKey:@"Guid"];
        }
        if([fBonusProduct objectForKey:@"Qty"])
        {
            [row setValue:[fBonusProduct objectForKey:@"Qty"] forKey:@"Qty"];
        }
        fBonusProduct=[row mutableCopy];
        [focProductBtn setTitle:[row stringForKey:@"Description"] forState:UIControlStateNormal];
        [self buttonAction:focProductBtn];
        //[tableView reloadData];
    }
    // bonusView = productBonusViewController.view;
    [self selectedStringValueDuplicate:[lblUOMCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    
}

#pragma mark conversion factor method

-(NSMutableArray*)fetchConversionFactorforItemCode:(NSString*)itemCode withItemUOM:(NSString*)itemUOM

{
 
      NSMutableArray* conversionFactorArry=[SWDatabaseManager fetchConversionFactorforUOMitemCode:itemCode ItemUOM:itemUOM];
    
    return conversionFactorArry;
    
    
}

#pragma mark -

- (void)selectedProduct:(NSMutableDictionary *)product
{
    
    
    //update the conversion val
    
    
    
    NSString* itemCode;//=[mainProductDict stringForKey:@"Item_Code"];
    
    
    itemCode=[product valueForKey:@"Item_Code"];

    if (itemCode==nil|| [itemCode isEqual:[NSNull null]]) {
        
    }
    
    
    
    NSMutableArray* fetchedConversionArray=[self fetchConversionFactorforItemCode:itemCode withItemUOM:[product stringForKey:@"Order_Quantity_UOM"]];
    NSLog(@"fetched values after product selected %@", [fetchedConversionArray description]);
    
    if (fetchedConversionArray.count>0) {
        
        NSInteger conversionfactor=[[[fetchedConversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        mainProductDict[@"conversionfactor"] = [NSString stringWithFormat:@"%ld",(long)conversionfactor];
        
        uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ",(long)conversionfactor] stringByAppendingString:@"EA"];
        
    }

    
    
    
    
    
    noPricesAvailableStatus=NO;
    selectedUOMReference=nil;
    productReference=[NSMutableDictionary dictionaryWithDictionary:product];
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        //display prev selected uom
        lblUOMCode.text=[NSString stringWithFormat:@" %@",[product stringForKey:@"Order_Quantity_UOM"]];
        
        // [uomButton setTitle:[product stringForKey:@"Order_Quantity_UOM"] forState:UIControlStateNormal];
        
        
        
        
        
        float selectedWholesalePrice=[[product valueForKey:@"Price"] floatValue];
        
        
        lblWholesalePrice.text=[NSString stringWithFormat:@" %0.2f", selectedWholesalePrice];
        
    }
    else
    {
        //if uom is not selected order captured with default uom and saved, caem to manage order then display the default display uom
        if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"notconfirmed"])
            
        {
            
            lblUOMCode.text=[NSString stringWithFormat:@" %@",[product stringForKey:@"Primary_UOM_Code"]];
            
        }
        
        else
        {
            
            if ([[product stringForKey:@"Item_UOM"] isEqualToString:@""]||[[product stringForKey:@"Item_UOM"] isEqual:[NSNull null]]) {
                
                
                
                NSString* selectedUOMStr=[product stringForKey:@"SelectedUOM"];
                if ([selectedUOMStr isEqualToString:@""]|| [selectedUOMStr isEqual:[NSNull null]]) {
                    lblUOMCode.text=[NSString stringWithFormat:@" %@",[product stringForKey:@"UOM"]];
                    
                    
                    
                }
                
                
            }
            
            
            else
            {
                lblUOMCode.text=[NSString stringWithFormat:@" %@",[product stringForKey:@"Item_UOM"]];
                
            }
        }
        
    }
    
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    mainProductDict=nil;
    mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
    
    txtfldWholesalePrice.text = @"";
    
    
    
    
    if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"]) {
        [lblWholesalePrice setHidden:YES];
        [txtfldWholesalePrice setHidden:NO];
        
        
    }
    else
    {
        [lblWholesalePrice setHidden:NO];
        [txtfldWholesalePrice setHidden:YES];
    }
    
    
    if ([mainProductDict objectForKey:@"Bonus_Product"]) {
        bonusProduct=[mainProductDict objectForKey:@"Bonus_Product"];
    }
    
    if ([mainProductDict objectForKey:@"FOC_Product"]) {
        fBonusProduct=[mainProductDict objectForKey:@"FOC_Product"];
        
    }
    NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
    [mainProductDict setValue:itemID forKey:@"ItemID"];
    
    
    NSString* updatedWholesalePrice;
    
    //Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
        
    {
        
        
        NSLog(@"check is order confirmed %@", self.isOrderConfirmed);
        
        NSLog(@"check net price in confrimed order %@", [mainProductDict valueForKey:@"Unit_Selling_Price"]);
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Unit_Selling_Price"] ;
        
    }
    
    else
    {
        
        updatedWholesalePrice= [mainProductDict valueForKey:@"Net_Price"] ;
    }
    
    
    
    
    [self getProductServiceDiddbGetBonusInfo:[[SWDatabaseManager retrieveManager] dbGetBonusInfo:[mainProductDict stringForKey:@"Item_Code"]]];
    //    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"]]];
    
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    
    
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    NSLog(@"quantity text is %@",txtProductQty.text);
    txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
    txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
    txtDiscount.text=[mainProductDict stringForKey:@"Discount"];
    txtFOCQty.text=[fBonusProduct stringForKey:@"Qty"];
    txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        
        NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
        
        
        
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@" %@%0.2f",userCurrencyCode,[updatedWholesalePrice doubleValue]]];
        
    }
    
    
    [addButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    [self selectedStringValueDuplicate:lblUOMCode.text];
    
    
    
    
    
    NSLog(@"check main product dict after adding product %@", [mainProductDict description]);
    
    
    //do thing
}

- (void) dbGetSelectedProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    
    
    NSLog(@"price detail initially %@", [priceDetail description]);
    
    NSLog(@"check selected uom ref initially %@", selectedUOMReference);
    
    
    NSString* selectedUomVal=selectedUOMReference;
    
    
    //at time uom is comint with a spce ex. @"CS" as @" CS" which is efecting query result, so remove empty space if they exists
    
    NSString *trimmedStringVal = [selectedUomVal stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    

    
    
    NSMutableArray* filteredPriceDetail=[[NSMutableArray alloc]init];
    
    
    for (NSInteger i=0; i<priceDetail.count; i++) {
        
        
        if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:trimmedStringVal]) {
            
            [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
            
        }
        
//        //adding case support
//                else if  ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"CS"]) {
//        
//                    [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
//        
//                }
        
        
    
    }
    priceDetail=filteredPriceDetail;
    
    
    NSLog(@"check price detail after change %@", priceDetail);
    
    
    //stock info based on selected UOM
    
    
    
    
    
    
    
    //OLA!! fetching lot info for selected item
//    NSArray *stockInfo = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"]];
    
    //
    
    
    NSLog(@"check uom for stock %@", [mainProductDict description]);
    
    
    //fetch stock info based on UOM
    
    NSArray* stockInfo;
    
    if (selectedUomVal.length>0) {
         stockInfo=[[SWDatabaseManager retrieveManager]fetchProductDetails:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"] UOMCode:selectedUomVal];
    }
    
    else
    {
        stockInfo=[[SWDatabaseManager retrieveManager]fetchProductDetails:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"] UOMCode:[mainProductDict stringForKey:@"UOM"]];
    
    }
    
    
    
    
//    NSArray* stockInfo=[[SWDatabaseManager retrieveManager]dbGetProductDetail:[mainProductDict stringForKey:@"Inventory_Item_ID"] organizationId:[mainProductDict stringForKey:@"Organization_ID"] with:[mainProductDict stringForKey:@"UOM"]];
    
    
    
    
    
    NSLog(@"stock info array getting saved in main product dict %@", stockInfo);
    
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Lot_Qty"] forKey:@"Lot_Qty"];
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"Expiry_Date"] forKey:@"Expiry_Date"];
    
    
    //default uom being displayed here as EA
    
    [mainProductDict setObject:[[stockInfo objectAtIndex:0] stringForKey:@"UOM"] forKey:@"UOM"];
    
    
    //[mainProductDict setObject:selectedUOMReference forKey:@"UOM"];
    
    
    
    [[NSUserDefaults standardUserDefaults]setValue:selectedUOMReference forKey:@"SelectedUOMRef"];
    
    
    
    NSString* itemCodeforUOM=[mainProductDict stringForKey:@"Item_Code"];
    NSString* selectedUom=selectedUOMReference;
    
    
    //at time uom is comint with a spce ex. @"CS" as @" CS" which is efecting query result, so remove empty space if they exists
    
    NSString *trimmedString = [selectedUom stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@'",itemCodeforUOM,trimmedString];
    
    NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
    
    NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
    
    
    
    if (consersionArr.count>0) {
        
        
        [lblAvlStock setText:[NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Lot_Qty"]]];

        
        NSInteger lotQty=[[mainProductDict stringForKey:@"Lot_Qty"] integerValue];
        
        NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        
        
        NSInteger updatedLotQty=  lotQty/conversion;
        
       // [mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];
        
        
        
    }
    
    
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@" N/A"];
    }
    else
    {
        [lblAvlStock setText:[NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Lot_Qty"]]];
    }

   // lblAvlStock.text=[mainProductDict valueForKey:@"Lot_Qty"];
    
    
    
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            
            //checking the net price junk value
            
            
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            
            
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
            noPricesAvailableStatus=YES;
            return;
        }
    }
    else
    {
        noPricesAvailableStatus=YES;
        return;
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    [lblWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
    [txtfldWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
    
    
    if([mainProductDict stringForKey:@"SelectedUOM"].length==0)
    {
        [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
    }
    else
    {
        [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];
    }

    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@" N/A"];
    }
    else
    {
        [lblAvlStock setText:[NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Lot_Qty"]]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }
}

- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    
    NSMutableArray* filteredPriceDetailUpdate=[[NSMutableArray alloc]init];
    
    
    NSLog(@"check uom %@", [mainProductDict valueForKey:@"Order_Quantity_UOM"]);
    
    
    
    NSString* checkselectedUOM=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
    
    
    
    for (NSInteger i=0; i<priceDetail.count; i++) {
        
        NSString* uomforPriceDetail=[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i];
        
        if ([uomforPriceDetail isEqualToString:checkselectedUOM]) {
            
            
            [filteredPriceDetailUpdate addObject:[priceDetail objectAtIndex:i]];
            
            
        }
        
        
        
        
    }
    if (filteredPriceDetailUpdate.count>0) {
        
        priceDetail=filteredPriceDetailUpdate;
    }
    
    
    
    
    
    else
    {
        
        NSMutableArray* filteredPriceDetail=[[NSMutableArray alloc]init];
        
        NSMutableArray* filteredPriceDetailMO=[[NSMutableArray alloc]init];
        
        
        
        for (NSInteger i=0; i<priceDetail.count; i++) {
            
            
            if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"EA"]) {
                
                [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
                
            }
            
           
            //added case support
            
                    else if  ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"CS"]) {
            
                        [filteredPriceDetail addObject:[priceDetail objectAtIndex:i]];
            
                    }
            
            
            
        }
        
        
        NSLog(@"filtered price detail after adding cs %@", [filteredPriceDetail description]);
        
        
        if (filteredPriceDetail.count==0) {
            
            
        }
        else
            
        {
            
            if ([self.isOrderConfirmed isEqualToString:@"notconfirmed"]) {
                
                
                
                NSString* selectedUOM=[customerDict stringForKey:@"Order_Quantity_UOM"];
                for (NSInteger i=0; i<priceDetail.count; i++) {
                    
                    NSLog(@"%@",[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]);
                    NSLog(@"%@",selectedUOM);
                    
                    if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:selectedUOM])
                    {
                        [filteredPriceDetailMO addObject:[priceDetail objectAtIndex:i]];
                    }
                    
                    
                }
                priceDetail=filteredPriceDetailMO;
                
                
            }
            else
            {
                
                priceDetail=filteredPriceDetail;
            }
        }
        
        
        
    }
    
    //check this object at index 0
    
    
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            
            //risk condition
            
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Unit_Selling_Price"];
            
            
            if ([selectedUOMReference isEqualToString:@"EA"] || selectedUOMReference==nil) {
                for (NSInteger i=0; i<priceDetail.count; i++) {
                    
                    if ([[[priceDetail valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:@"EA"]) {
                        [mainProductDict setValue:[[priceDetail objectAtIndex:i]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
                    }
                }
            }
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else
    {
        [self showPlaceHolderwithMessage:@"No prices available for selected product."];
    }
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblProductBrand setText:[mainProductDict stringForKey:@"Brand_Code"]];
    [lblRetailPrice setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];

    
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"confirmed"])
    {
        
    }
    else
    {
        if (!selectedUOMReference) {
            if([single.visitParentView isEqualToString:@"MO"] && [self.isOrderConfirmed isEqualToString:@"notconfirmed"])
            {
                
                //check if this has order qty uom
                
                NSString* orderqtyUom=[mainProductDict stringForKey:@"Order_Quantity_UOM"];
                
                if (orderqtyUom==nil|| [orderqtyUom isEqualToString:@""]||[orderqtyUom isEqual:[NSNull null]]  ) {
                    
                    NSString* tempUom=[mainProductDict stringForKey:@"UOM"];
                    NSLog(@"tempUom----->%@",tempUom);
                    if(tempUom==nil|| [tempUom isEqualToString:@""]||[tempUom isEqual:[NSNull null]] )
                    {
                        NSLog(@"tempUom2----->%@",tempUom);
                        [lblUOMCode setText:[mainProductDict stringForKey:@"Display_UOM"]];
                    }
                    else
                    {
                        NSLog(@"tempUom3----->%@",tempUom);
                        [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
                    }
                    
                    
                }
                
                else
                {
                    
                    lblUOMCode.text=[NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Order_Quantity_UOM"]];
                    
                    
                }
            }
            
            
            
            else if ([mainProductDict valueForKey:@"UOM"]==nil) {
                
                lblUOMCode.text= [NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Item_UOM"]];
            }
            
            else
            {
                
                if ([mainProductDict valueForKey:@"SelectedUOM"]==nil) {
                    [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
                    
                }
                
                
                else
                {
                    [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];
                    
                }
                
                
            }
            [lblWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
            [txtfldWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
            
        }
        
        else
        {
            
            
            NSString* selectedUOm=[mainProductDict stringForKey:@"SelectedUOM"];
            
            if ([selectedUOm isEqual:[NSNull null]]|| [selectedUOm isEqualToString:@""]) {
                
                [lblUOMCode setText:[mainProductDict stringForKey:@"UOM"]];
                
            }
            
            else
            {
                
                [lblUOMCode setText:[mainProductDict stringForKey:@"SelectedUOM"]];
            }
            
            [lblWholesalePrice setText:[NSString stringWithFormat:@" %@",[[productReference stringForKey:@"Net_Price"] currencyString]]];
            
            
            
            [txtfldWholesalePrice setText:[NSString stringWithFormat:@" %@",[[productReference stringForKey:@"Net_Price"] currencyString]]];
            
        }
    }
    
    
    
    
    //avbl stock based on selected Price UOM
    
    
    
    
    NSString* itemCodeforUOM=[mainProductDict stringForKey:@"Item_Code"];
    NSString* selectedUom=selectedUOMReference;
    
    
    if (selectedUom==nil) {
        
        selectedUom=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
    }
    
    //at time uom is comint with a space ex. @"CS" as @" CS" which is efecting query result, so remove empty space if they exists
    
    NSString *trimmedString = [selectedUom stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    
    NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@'",itemCodeforUOM,trimmedString];
    
    NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
    
    NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
    
    
    
    if (consersionArr.count>0) {
        
        
        NSLog(@"check lot qty %@", [mainProductDict stringForKey:@"Lot_Qty"]);
        
        
        
        
        NSInteger lotQty=[[mainProductDict stringForKey:@"Lot_Qty"] integerValue];
        
        NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        
        
        NSInteger updatedLotQty= lotQty/conversion;
        
        [mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];
        
        
        
    }
    
    

    
    
    
    
    if ([[mainProductDict stringForKey:@"Lot_Qty"] floatValue]==0.0)
    {
        [lblAvlStock setText:@" N/A"];
    }
    else
    {
        [lblAvlStock setText:[NSString stringWithFormat:@" %@",[mainProductDict stringForKey:@"Lot_Qty"]]];

        //[lblAvlStock setText:[mainProductDict stringForKey:@"Lot_Qty"]];
    }
    
    if ([[mainProductDict stringForKey:@"Expiry_Date"] floatValue]==0.0)
    {
        lblExpDate.text = @"N/A";
    }
    else
    {
        lblExpDate.text = [NSString stringWithFormat:@"%@", [mainProductDict dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle]];
    }
    
    //[lblExpDate setText:[mainProductDict stringForKey:@"Expiry_Date"]];
}
- (void)stockAllocated:(NSMutableDictionary *)p {
    mainProductDict = nil;
    NSLog(@"allocated stock is %@", p);
    
    
    mainProductDict=[NSMutableDictionary dictionaryWithDictionary:p];
}
-(void)sectionButtonTouchUpInside:(TableCellHeaderSalesOrder*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        //..sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        // sender.btnItemName.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    else {
        //..r sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        //sender.btnItemName.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        
        
        NSInteger collapedSectionsCount=_collapsedSections.count;
        
        
        for (NSInteger i=0; i<collapedSectionsCount; i++) {
            
            NSArray *myArray = [_collapsedSections allObjects];
            
            NSInteger sectionVal=[[myArray objectAtIndex:i] integerValue];
            int numOfRows = [productTableView numberOfRowsInSection:sectionVal];
            NSArray* indexPaths = [self indexPathsForSection:sectionVal withNumberOfRows:numOfRows];
            [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [_collapsedSections removeObject:@(sectionVal)];
            
        }
         [self fetchLabelDatafromDB:[finalProductArray objectAtIndex:section]];
        
        NSInteger numOfRows = [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
    
    [productTableView reloadData];
    
}

#pragma mark - UISearchBar


-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [candySearchBar setShowsCancelButton:YES animated:YES];
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }else {
        bSearchIsOn = YES;
        [self  searchContent];
    }
    
    
}

-(void)searchContent
{
    NSString *searchString = candySearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Description", searchString];
    //NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filter, @"Description", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1]];
    filteredCandyArray=[[productArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    NSLog(@"\n\n *** Total Search Results: %lu  ***\n\n",(unsigned long)filteredCandyArray.count);
    
    if (filteredCandyArray.count>0) {
        [productTableView reloadData];
        [productTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                      animated:YES
                                scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [productTableView.delegate tableView:productTableView didSelectRowAtIndexPath:firstIndexPath];
        
    }else if (filteredCandyArray.count==0)
    {
        bSearchIsOn=YES;
        [productTableView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
        //[self updateViewEditingStatus:NO];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar{
    
    NSLog(@"\n\n *** Search Button Clicked **\n\n");
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    NSLog(@"\n\n *** Search Bar Cancel Button Clicked **\n\n");
    [candySearchBar setText:@""];
    [candySearchBar setShowsCancelButton:NO animated:YES];
    filteredCandyArray=[[NSMutableArray alloc]init];
    bSearchIsOn=NO;
    if ([candySearchBar isFirstResponder]) {
        [candySearchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (finalProductArray.count>0) {
        [productTableView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
    }
    
}

#pragma mark - Button Action
- (void)getProductServiceDiddbGetBonusInfo:(NSArray *)info{
    isBonusRowDeleted=NO;
    bonusInfo=nil;
    bonusInfo=[NSArray arrayWithArray:info];
    AppControl *appControl = [AppControl retrieveSingleton];
    isBonusAppControl = appControl.ALLOW_BONUS_CHANGE;
    isFOCAppControl = appControl.ENABLE_MANUAL_FOC;
    isDiscountAppControl = appControl.ALLOW_DISCOUNT;
    isDualBonusAppControl = appControl.ALLOW_DUAL_FOC;
    isFOCItemAppControl=appControl.ALLOW_MANUAL_FOC_ITEM_CHANGE;
    
    if([isFOCAppControl isEqualToString:@"Y"])
    {
        isFOCAllow=YES;
        checkFOC=YES;
    }
    else
    {
        if ([isDualBonusAppControl isEqualToString:@"N"])
        {
            isFOCAllow=NO;
            checkFOC=NO;
        }
    }
    
    if([isDiscountAppControl isEqualToString:@"Y"])
    {
        isDiscountAllow=YES;
        checkDiscount = YES;
    }
    else
    {
        isDiscountAllow=YES;
        checkDiscount = NO;
    }
    
    if([isFOCItemAppControl isEqualToString:@"Y"])
    {
        isFOCItemAllow=YES;
    }
    else
    {
        isFOCItemAllow=NO;
    }
    
    if([isDualBonusAppControl isEqualToString:@"Y"])
    {
        isDualBonusAllow=YES;
    }
    else
    {
        isDualBonusAllow=NO;
    }
    
    if (bonusInfo.count > 0)
    {
        isBonus = YES;
        if([isBonusAppControl isEqualToString:@"Y"] || [isBonusAppControl isEqualToString:@"I"] || [isBonusAppControl isEqualToString:@"D"])
        {
            isBonusAllow = YES;
            checkBonus = YES;
        }
        else
        {
            isBonusAllow = NO;
            checkBonus = NO;
        }
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        isBonus = NO;
        bonus=0;
        isBonusAllow = NO;
        checkBonus = NO;
    }
    bonusEdit = @"N";
    [self updatePrice];
    if(isBonus)
    {
        if (bonus > 0)
        {
            if(checkDiscount)
            {
                isDiscountAllow=NO;
            }
            if(checkFOC)
            {
                if (!isDualBonusAllow)
                {
                    isFOCAllow=NO;
                }
            }
        }
        
        else if (bonus <= 0)
        {
            if(checkFOC)
            {
                isFOCAllow=YES;
            }
            if(checkDiscount)
            {
                isDiscountAllow=YES;
            }
        }
    }
    
    if([mainProductDict objectForKey:@"FOC_Product"])
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"FOC_Product"]];
    }
    else
    {
        fBonusProduct=nil;
        fBonusProduct = [NSMutableDictionary dictionaryWithDictionary:mainProductDict];
        [fBonusProduct removeObjectForKey:@"Qty"];
        [fBonusProduct removeObjectForKey:@"Bonus_Product"];
        [fBonusProduct removeObjectForKey:@"StockAllocation"];
    }
    
    if([mainProductDict objectForKey:@"Bonus_Product"])
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[mainProductDict objectForKey:@"Bonus_Product"]];
        bonus = [[bonusProduct stringForKey:@"Qty"] intValue];
    }
    else
    {
        bonusProduct=nil;
        bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[mainProductDict stringForKey:@"Item_Code"]]];
        [bonusProduct removeObjectForKey:@"Qty"];
        [bonusProduct removeObjectForKey:@"FOC_Product"];
        [bonusProduct removeObjectForKey:@"StockAllocation"];
    }
    [focProductBtn setTitle:[fBonusProduct stringForKey:@"Description"] forState:UIControlStateNormal];
    
    //focProductBtn.titleLabel.text = [fBonusProduct stringForKey:@"Description"];
    //[tableView reloadData];
    info=nil;
    if(isBonusAllow)//APP CONTROL
    {
        if (bonus<=0)
        {
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        else{
            [txtRecBonus setUserInteractionEnabled:YES];
        }
    }
    else
    {
        [txtRecBonus setUserInteractionEnabled:NO];
    }
}

- (void)calculateBonus {
    
    int quantity = 0;
    bonus = 0;
    NSDictionary *tempBonus;
    BOOL isBonusGroup = YES;
    if ([[AppControl retrieveSingleton].ENABLE_BONUS_GROUPS isEqualToString:@"Y"])
    {
        NSString *query = [NSString stringWithFormat:@"Select A.* from TBL_BNS_Promotion AS A INNER JOIN TBL_Customer_Addl_Info As B ON B.Attrib_Value=A.BNS_Plan_ID Where B.Customer_ID = '%@' AND B.Site_Use_ID = '%@'",[customerDict stringForKey:@"Customer_ID"],[customerDict stringForKey:@"Site_Use_ID"]];
        if ([[SWDatabaseManager retrieveManager] fetchDataForQuery:query].count==0)
        {
            isBonusGroup = NO;
        }
        else
        {
            isBonusGroup = YES;
        }
    }
    else
    {
        isBonusGroup = YES;
    }
    
    if (isBonusGroup) {
        if ([mainProductDict objectForKey:@"Qty"])
        {
            quantity = [[mainProductDict objectForKey:@"Qty"] intValue];
            for(int i=0 ; i<[bonusInfo count] ; i++ )
            {
                NSDictionary *row = [bonusInfo objectAtIndex:i];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    
                    if(checkBonus)
                    {
                        isBonusAllow = YES;
                    }
                    if ([[AppControl retrieveSingleton].BONUS_MODE isEqualToString:@"DEFAULT"])
                    {
                        if(i == [bonusInfo count]-1)
                        {
                            if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                            {
                                int dividedValue = quantity / rangeStart ;
                                bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                                tempBonus=row;
                            }
                            
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                            {
                                int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                                bonus = floor(dividedValue) ;
                                tempBonus=row;
                                
                            }
                            else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"POINT"])
                            {
                                bonus = [[row objectForKey:@"Get_Qty"] intValue];
                                tempBonus=row;
                                
                            }
                        }
                        else
                        {
                            bonus = [[row objectForKey:@"Get_Qty"] intValue];
                            tempBonus=row;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue] + floorf(([[mainProductDict objectForKey:@"Qty"] intValue] - rangeStart) *[[row objectForKey:@"Get_Add_Per"] floatValue]);
                        tempBonus=row;
                    }
                    break;
                }
                else
                {
                    if(checkBonus)
                    {
                        isBonusAllow = NO;
                    }
                }
            }
        }
        
        if (bonus > 0 && bonusInfo)
        {
            if(![[tempBonus stringForKey:@"Get_Item"] isEqualToString:[bonusProduct stringForKey:@"Item_Code"]])
            {
                NSArray *temBonusArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:[NSString stringWithFormat:@"select * from TBL_Product where Item_Code ='%@'",[tempBonus stringForKey:@"Get_Item"]]];
                bonusProduct=nil;
                bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[temBonusArray objectAtIndex:0]];
                [bonusProduct removeObjectForKey:@"Qty"];
                [bonusProduct removeObjectForKey:@"FOC_Product"];
                [bonusProduct removeObjectForKey:@"StockAllocation"];
            }
            
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Def_Qty"];
            [bonusProduct setValue:[NSString stringWithFormat:@"%d", bonus] forKey:@"Qty"];
            
            if(!isDualBonusAllow)
            {
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct removeObjectForKey:@"Def_Qty"];
                
            }
            if(isDiscountAllow)
            {
                [mainProductDict setValue:@"0" forKey:@"Discount"];
            }
        }
        else
        {
            [bonusProduct removeObjectForKey:@"Qty"];
            [bonusProduct removeObjectForKey:@"Def_Qty"];
            
        }
        
    }
    
}
- (void)updatePrice{
    
    //NSLog(@"OLA!!******* mainProduct at begin updatePrice %@",mainProductDict);
    
    if ([mainProductDict stringForKey:@"Qty"].length > 0)
    {
        int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
        if (qty > 0)
        {
            
            NSMutableString *wholesalePrice;
            
            
            if (txtfldWholesalePrice.text) {
                if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""]){
                    
                    
                    //unit price issue 1,000 to 1 fixed  by syed (Comma issue)
                    NSString * stringValue = txtfldWholesalePrice.text;
                    NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
                    
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
                }
                else
                    //                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
                    wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
                
                
                
                //update netprice
                
                
                
                
                
                
                
                
            }
            else
                wholesalePrice = [NSMutableString stringWithFormat: @"%@",[mainProductDict stringForKey:@"Net_Price"]];
            
            float totalPrice = (double)qty * ([wholesalePrice floatValue]);
            
            NSLog(@"total val %f",[wholesalePrice doubleValue]);
            //            NSLog(@"Price %f",[[mainProductDict stringForKey:@"Net_Price"] doubleValue]);
            //            NSLog(@"Price %f",(double)qty);
            //            NSLog(@"Price %f",totalPrice);
            if(bonus<=0 || !checkDiscount)
            {
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                
                if ([self.parentViewString isEqualToString:@"MO"]) {
                    
                    
                    //                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),[[mainProductDict valueForKey:@"Discounted_Price"] floatValue] ,NSLocalizedString(@"Discounted Amount", nil), [[mainProductDict valueForKey:@"Discounted_Amount"] floatValue]]];
                    
                    
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),[[mainProductDict valueForKey:@"Net_Price"] floatValue] ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                }
                
                
                else
                {
                    
                    
                    
                    
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                    [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                    
                    
                    NSLog(@"check dict at price here %@", [mainProductDict description]);
                    
                    
                    
                    [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                    
                    // lblPriceLabel.text=[NSString stringWithFormat:@"%0.2f",]
                }
            }
            
            else
            {
                //                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                //                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Discounted_Price"];
                //
                //                //why si discount hardcoded as 0.0?
                //
                //                [mainProductDict setValue:@"0.00" forKey:@"Discounted_Amount"];
                //
                //                [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
                
                
                
                
                
                float discountAmount = [[mainProductDict stringForKey:@"Discount"] floatValue];
                float percentDiscount =  discountAmount/100.00 ;
                float TotalDiscount = totalPrice * percentDiscount ;
                float discountedPrice = totalPrice - TotalDiscount ;
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", discountedPrice] forKey:@"Discounted_Price"];
                [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", TotalDiscount] forKey:@"Discounted_Amount"];
                
                
                NSLog(@"check discount amount in main product dict %@", [mainProductDict valueForKey:@"Discounted_Amount"]);
                
                
                
                [lblPriceLabel setText:[NSString stringWithFormat:@" %@  = %.2f , %@ = %.2f , %@ = %.2f ",NSLocalizedString(@"Actual Price", nil),totalPrice ,NSLocalizedString(@"Discounted Price", nil),discountedPrice ,NSLocalizedString(@"Discounted Amount", nil), TotalDiscount]];
                
                
            }
        }
        else{
            [lblPriceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Total Item Price", nil), [mainProductDict currencyStringForKey:@"Price"]]];
            
        }
        //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    }
    else
    {
        [lblPriceLabel setText:@""];
        //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    
    //NSLog(@"OLA!!******* mainProduct at end updatePrice %@",mainProductDict);
}

-(void)saveAsTemplateAction:(id)sender
{
    NSLog(@"Save As Template");
}
-(IBAction)resetAction:(id)sender
{
    //lblProductName.text = @"Product Name";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    lblProductName.text=@"";
    
    uomConversionFactorLbl.text=@"";
    
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
    [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
    mainProductDict = nil;
    bonusProduct = nil;
    fBonusProduct = nil;
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addButton setTitle:@"Add to Order" forState:UIControlStateNormal];
    
}



-(void)ResetData
{
}


-(IBAction)saveOrderAction:(id)sender
{
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    
    if ([self.isOrderConfirmed isEqualToString:@"confirmed"]) {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Confirmed orders cannot be edited." withController:self];
        return;
    }
    
    [txtProductQty resignFirstResponder];
    [txtDefBonus resignFirstResponder];
    [txtRecBonus resignFirstResponder];
    [txtDiscount resignFirstResponder];
    [txtFOCQty resignFirstResponder];
    [txtNotes resignFirstResponder];
    
    if (![txtProductQty.text isEqualToString:@""]) {
        
        [txtfldWholesalePrice resignFirstResponder];
        
        [self saveProductOrder];
        
        [oldSalesOrderVC productAdded:mainProductDict];
        mainProductDict=nil;
        fBonusProduct= nil;
        bonusProduct = nil;
        [focProductBtn setTitle:@"Item" forState:UIControlStateNormal];
        [productTableView deselectRowAtIndexPath:[productTableView indexPathForSelectedRow] animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please enter quantity." withController:self];
    }
}

- (void)saveProductOrder {
    
    
    
    
    NSLog(@"price list id in save order %@", [customerDict valueForKey:@"Price_List_ID"]);
    NSLog(@"item id in save order is %@", [mainProductDict valueForKey:@"ItemID"]);
    NSLog(@"inventory item id while saving order is %@", [mainProductDict valueForKey:@"Inventory_Item_ID"]);
    
    
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict valueForKey:@"Inventory_Item_ID"] :[customerDict valueForKey:@"Price_List_ID"]]];
    
    
    
    NSArray* tempProd=[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict valueForKey:@"Inventory_Item_ID"] :[customerDict valueForKey:@"Price_List_ID"]];
    
    NSLog(@"temp prod al seer in save order %@", [tempProd objectAtIndex:0]);
    
    NSMutableArray* testProdArray=[[NSMutableArray alloc] init];
    
    
    for (NSInteger i=0; i<tempProd.count; i++) {
        
        
        if ([[[tempProd valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:selectedUOMReference]) {
            
            
            [testProdArray addObject:[tempProd objectAtIndex:i]];
        }
        
    }
    
    
    //check price update here
    NSMutableArray* filteredPriceDetailUpdate=[[NSMutableArray alloc]init];
    
    
    NSLog(@"check uom %@", [mainProductDict valueForKey:@"Order_Quantity_UOM"]);
    
    
    
    NSString* checkselectedUOM=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
    
    
    
    for (NSInteger i=0; i<tempProd.count; i++) {
        
        NSString* uomforPriceDetail=[mainProductDict valueForKey:@"Order_Quantity_UOM"];
        
        if ([uomforPriceDetail isEqualToString:checkselectedUOM]) {
            
            
            [filteredPriceDetailUpdate addObject:[tempProd objectAtIndex:i]];
            
            
        }
        
        
        
        
    }
    if (filteredPriceDetailUpdate.count>0) {
        
        tempProd=filteredPriceDetailUpdate;
    }
    
    
    
    
    
    NSLog(@"test prod array in save order %@", [testProdArray description]);
    
    
    //NSString * uspVal=[[testProdArray valueForKey:@"Unit_Selling_Price" ] objectAtIndex:0];
    
    
    if (testProdArray.count>0) {
        [ mainProductDict setValue:[[testProdArray valueForKey:@"Unit_Selling_Price" ] objectAtIndex:0]   forKey:@"Net_Price"];
        
        
        if (selectedUOMReference) {
            
            
            //chnage this str here
            [ mainProductDict setValue:selectedUOMReference   forKey:@"Order_Quantity_UOM"];
            
            
        }
    }
    
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addButton.titleLabel setFont:bodySemiBold];
    
    
    
    [addButton setTitle:@"Add to Order" forState:UIControlStateNormal];
    
    isSaveOrder = YES;
    int totalBonus = 0 ;
    [mainProductDict removeObjectForKey:@"FOC_Product"];
    [mainProductDict removeObjectForKey:@"Bonus_Product"];
    
    [mainProductDict setValue:bonusProduct forKey:@"Bonus_Product"];
    [mainProductDict setValue:fBonusProduct forKey:@"FOC_Product"];
    
    BOOL isBothBonus = NO;
    if([bonusProduct objectForKey:@"Qty"])
    {
        totalBonus = [[bonusProduct stringForKey:@"Qty"] intValue];
        if(!isDualBonusAllow)
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
        else if([fBonusProduct objectForKey:@"Qty"])
        {
            isBothBonus = YES;
            totalBonus = totalBonus + [[fBonusProduct stringForKey:@"Qty"] intValue];
        }
        else
        {
            [mainProductDict removeObjectForKey:@"FOC_Product"];
        }
    }
    else
    {
        [mainProductDict removeObjectForKey:@"Bonus_Product"];
    }
    
    if([fBonusProduct objectForKey:@"Qty"] && !isBothBonus)
    {
        totalBonus = [[fBonusProduct stringForKey:@"Qty"] intValue];
    }
    else if(!isBothBonus)
    {
        [mainProductDict removeObjectForKey:@"FOC_Product"];
    }
    
    
    [mainProductDict setValue:[NSString stringWithFormat:@"%d",totalBonus] forKey:@"Def_Bonus"];
    [mainProductDict setValue:[mainProductDict stringForKey:@"Discount"] forKey:@"DiscountPercent"];
    
    if(![mainProductDict objectForKey:@"StockAllocation"])
    {
        // [mainProductDict setValue:stockItems forKey:@"StockAllocation"];
    }
    else
    {
    }
    //NSLog(@"Product %@",mainProductDict);
    //[self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    //[self.navigationController  popViewControllerAnimated:YES];
    
    
    if(txtfldWholesalePrice.text)
    {
        
        NSMutableString *wholesalePrice;
        if ([[mainProductDict stringForKey:@"Control_1"] isEqualToString:@"Y"] && ![txtfldWholesalePrice.text isEqualToString:@""])
        {
            
            // NSLog(@"OLA!!***** WP 2 %@",txtfldWholesalePrice.text );
            
            //unit price fix 1,000 to 1 fixed by syed (comma issue)
            NSString * stringValue = txtfldWholesalePrice.text;
            NSString * stringValue1 =[stringValue stringByReplacingOccurrencesOfString:@"," withString:@""];
            
            wholesalePrice = [NSMutableString stringWithFormat: @"%@",[stringValue1 substringFromIndex:3]];
            
            
            
            
            
            //NSString *wholesalePrice = [txtfldWholesalePrice.text substringFromIndex:3];
            [mainProductDict setObject:wholesalePrice forKey:@"Net_Price"];
        }
        
    }
    
    
    
    //fix for price getting updated
    
    
    NSLog(@"UOM code is %@", lblUOMCode.text);
    NSLog(@"wholesale price is %@", lblTitleWholesalePrice.text);
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    NSLog(@"selected uom is is %@", selectedUOMReference);
    NSLog(@"quantity is  %@", txtProductQty.text);
    
    
    
    
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    
    
    lblProductName.text = @"";
    lblProductCode.text =@"";
    lblProductBrand.text=@"";
    lblWholesalePrice.text=@"";
    txtfldWholesalePrice.text = @"";
    lblRetailPrice.text=@"";
    lblUOMCode.text=@"";
    lblAvlStock.text=@"";
    lblExpDate.text=@"";
    lblPriceLabel.text=@"";
    
    uomConversionFactorLbl.text=@"";
    
    
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtRecBonus.text=@"";
    txtDiscount.text=@"";
    txtFOCQty.text=@"";
    txtNotes.text=@"";
}

-(IBAction)buttonAction:(id)sender
{
    [self.view endEditing:YES];
    
    //    if (mainProductDict==nil) {
    //
    //
    //    ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
    //    [stockViewController setDelegate:self];
    //    [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
    //    [stockViewController setTarget:self];
    //    [stockViewController setAction:@selector(stockAllocated:)];
    //
    //    UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
    //    modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
    //    [self presentViewController:modalViewNavController animated:YES completion:nil];
    //
    //    }
    
    
    
    int tag = ((UIButton *)sender).tag;
    switch (tag)
    {
        case 1:
            if (isPproductDropDownToggled)
            {
                isPproductDropDownToggled=NO;
                focProductTableView.hidden=YES;
                
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,0) withDuration:0.2];
            }
            else
            {
                isPproductDropDownToggled=YES;
                focProductTableView.hidden=NO;
                productDropDown.frame=CGRectMake(599, 133, 241, 340);
                
                [AnimationUtility resizeFrame:productDropDown withSize:CGSizeMake(productDropDown.frame.size.width,383) withDuration:0.2];
            }
            break;
        case 11:
        {
            if (mainProductDict.count!=0)
            {
                //NSLog(@"Stock");
                //productStockViewController=nil;
                ProductStockViewController *stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDict] ;
                [stockViewController setDelegate:self];
                [stockViewController setOrderQuantity:[[mainProductDict stringForKey:@"Qty"] intValue]];
                [stockViewController setTarget:self];
                [stockViewController setAction:@selector(stockAllocated:)];
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:stockViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 12:
        {
            if (mainProductDict.count!=0)
            {
                ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:bonusViewController];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 13:
        {
            if (mainProductDict.count!=0)
            {
                CustomerSalesViewController *salesHistory = [[CustomerSalesViewController alloc] initWithCustomer:customerDict andProduct:mainProductDict] ;
                
                UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:salesHistory];
                modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
                [self presentViewController:modalViewNavController animated:YES completion:nil];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
            }
        }
            break;
        case 5:
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)textViewDidEndEditing:(UITextView *)textView {
    if(textView==txtNotes)
    {
        [mainProductDict setValue:textView.text forKey:@"lineItemNotes"];
        txtNotes.text=[mainProductDict stringForKey:@"lineItemNotes"];
        
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(noPricesAvailableStatus==YES)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product. Please select another product." withController:self];
        return NO;
    }
    if (mainProductDict.count==0)
    {
        [self showPlaceHolderwithMessage:@"Please select product."];

        isErrorSelectProduct = YES;
        [self performSelector:@selector(resignedTextField:) withObject:textField afterDelay:0.0];
    }
    else
    {
        [self hidePlaceHolder];
    }
    if (textField==txtDiscount) {
        textField.text=@"";
    }
    
    return YES;
}
-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (mainProductDict.count>0)
    {
        [textField resignFirstResponder];
        NSCharacterSet *unacceptedInput = nil;
        NSString *zeroString=@"0";
        
        // textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
        if (textField==txtFOCQty)
        {
            if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
            {
                
                if ([textField.text length]!=0)
                {
                    [fBonusProduct setValue:textField.text forKey:@"Qty"];
                    [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
                }
                else
                {
                    [fBonusProduct removeObjectForKey:@"Qty"];
                    [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
                [fBonusProduct removeObjectForKey:@"Qty"];
                [fBonusProduct setValue:zeroString forKey:@"Def_Qty"];
            }
        }
        if (textField==txtRecBonus)
        {
            if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
            {
                [bonusProduct setValue:textField.text forKey:@"Qty"];
                if([isBonusAppControl isEqualToString:@"Y"])
                {
                    bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                    bonusEdit = @"Y";
                }
                else if([isBonusAppControl isEqualToString:@"I"])
                {
                    if([[bonusProduct stringForKey:@"Qty"] intValue] >= bonus)
                    {
                        bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                        bonusEdit = @"Y";
                    }
                    else
                    {
                        [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can increase only." withController:self];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                    }
                }
                else if([isBonusAppControl isEqualToString:@"D"])
                {
                    if([[bonusProduct stringForKey:@"Qty"] intValue] <= bonus)
                    {
                        bonus =[[bonusProduct stringForKey:@"Qty"] intValue];
                        bonusEdit = @"Y";
                    }
                    else
                    {
                        [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Bonus can decrease only." withController:self];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus] forKey:@"Qty"];
                    }
                }
                
                if (bonus==0)
                {
                    [bonusProduct removeObjectForKey:@"Qty"];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
                [bonusProduct removeObjectForKey:@"Qty"];
            }
        }
        if (textField==txtDiscount)
        {
            if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
            {
                if ([textField.text intValue]>100) {
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Sorry" andMessage:@"Discount (%) should not be more than 100%" withController:self];
                    [mainProductDict removeObjectForKey:@"Discount"];
                }
                else
                {
                    [mainProductDict setValue:textField.text forKey:@"Discount"];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Discount (%) value should have only numeric" withController:self];
                [mainProductDict removeObjectForKey:@"Discount"];
                
            }
        }
        
        //check txt field data
        if (textField==txtProductQty)
        {
            if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
            {
                
                [mainProductDict setValue:textField.text forKey:@"Qty"];
                if (bonusInfo.count > 0 || ![[mainProductDict stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
                {
                    if([bonusEdit isEqualToString:@"N"])
                    {
                        [self calculateBonus];
                    }
                    else
                    {
                        [self calculateBonus];
                        
                        bonusEdit = @"N";
                    }
                }
            }
            else if ([[SWDefaults getValidStringValue:[mainProductDict valueForKey:@"Unit_Selling_Price"]]isEqualToString:@"0"])
            {
                
            }
            else
            {
                if (!isErrorSelectProduct) {
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Quantity value should have only numeric" withController:self];
                }
                isErrorSelectProduct=NO;
                
                [mainProductDict setValue:@"" forKey:@"Qty"];
            }
        }
        
        
        NSLog(@"main product dict before updating price %@", [mainProductDict description]);

        
        [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:selectedUOMReference :[customerDict stringForKey:@"Price_List_ID"]]];
        [self updatePrice];
        
        if(isBonus)
        {
            if (bonus > 0)
            {
                if(checkDiscount)
                {
                    isDiscountAllow=NO;
                }
                if(checkFOC)
                {
                    if (!isDualBonusAllow)
                    {
                        isFOCAllow=NO;
                    }
                }
            }
            else if (bonus <= 0)
            {
                if(checkFOC)
                {
                    isFOCAllow=YES;
                }
                if(checkDiscount)
                {
                    isDiscountAllow=YES;
                }
            }
        }
        
        txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
        txtDefBonus.text=[bonusProduct stringForKey:@"Def_Qty"];
        
        //Bonus
        if(![fBonusProduct objectForKey:@"Qty"]  || isDualBonusAllow)
        {
            txtRecBonus.text=[bonusProduct stringForKey:@"Qty"];
        }
        
        
        if(isBonusAllow)//APP CONTROL
        {
            if (bonus<=0){
                [txtRecBonus setUserInteractionEnabled:NO];
            }
            else{
                [txtRecBonus setUserInteractionEnabled:YES];
            }
        }
        else
        {
            [txtRecBonus setUserInteractionEnabled:NO];
        }
        
        
        //MFOC
        if(![bonusProduct objectForKey:@"Qty"] || isDualBonusAllow)
        {
            [txtFOCQty setText:[fBonusProduct stringForKey:@"Qty"]];
        }
        if(isFOCAllow)//APP CONTROL
        {
            [txtFOCQty setUserInteractionEnabled:YES];
            focProductBtn.userInteractionEnabled=YES;
        }
        else
        {
            [txtFOCQty setUserInteractionEnabled:NO];
            focProductBtn.userInteractionEnabled=NO;
        }
        
        if(isFOCItemAllow)
        {
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            focProductBtn.userInteractionEnabled=YES;
        }
        else
        {
            focProductBtn.userInteractionEnabled=NO;
        }
        //Discount
        NSString *discountString = [mainProductDict stringForKey:@"Discount"];
        
        [txtDiscount setText: discountString];
        if([isDiscountAppControl isEqualToString:@"Y"])//APP CONTROL
        {
            [txtDiscount setUserInteractionEnabled:YES];
        }
        else
        {
            [txtDiscount setUserInteractionEnabled:NO];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSLog(@"OLA!!***** WP %@",txtfldWholesalePrice.text );
    
    [textField resignFirstResponder];
    return YES;
}


//restrictiong text view notes to 50 characters

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0)
    {
        if([textView.text length] != 0)
        {
            return YES;
        }
        else {
            return NO;
        }
    }
    else if([[textView text] length] > 49 )
    {
        return NO;
    }
    return YES;
}


//restrict characters here
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"You have entered wrong input" withController:self];
    }
    
    
    if([textField.text hasPrefix:@"0"])
    {
        textField.text=@"";
    }
    
    if ([textField isEqual:txtRecBonus]) {
        if ([txtProductQty.text isEqualToString:@""] || [txtProductQty.text isEqual:[NSNull null]]) {
            return NO;
        }
    }
    
    if ([textField isEqual:txtProductQty]) {
        
        NSRange textFieldRange = NSMakeRange(0, [textField.text length]);
        if (NSEqualRanges(range, textFieldRange) && [string length] == 0) {
            // Game on: when you return YES from this, your field will be empty
            [txtRecBonus setText:@""];
            [txtDefBonus setText:@""];
            [txtRecBonus setUserInteractionEnabled:NO];
        }
    }
    
    if ([textField isEqual:txtfldWholesalePrice]) {
        
        NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSLog(@"resulting string would be: %@", resultString);
        NSString *prefixString = [[SWDefaults userProfile]valueForKey:@"Currency_Code"];
        NSRange prefixStringRange = [resultString rangeOfString:prefixString];
        if (prefixStringRange.location == 0) {
            // prefix found at the beginning of result string
            
            NSString *allowedCharacters;
            if ([textField.text componentsSeparatedByString:@"."].count >= 2)//no decimal point yet, hence allow point
                allowedCharacters = @"01234567890";
            else
                allowedCharacters = @"0123456789.";
            
            if ([[string stringByReplacingOccurrencesOfString:prefixString withString:@""] rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
            {
                // BasicAlert(@"", @"This field accepts only numeric entries.");
                
                return NO;
            }
        }
        else
            return NO;
    }
    else if ([textField isEqual:txtDiscount])
    {
        NSString *allowedCharacters = @"0123456789";
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            
            return NO;
        }
        
    }
    return YES;
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        
        
        if (!isPinchDone)
        {
            isPinchDone=YES;
            //        recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
            //        recognizer.scale = 1;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(251, 8, 773, 690) withDuration:0.5];
            
            //recognizer.view.frame = CGRectMake(258, 8, 756, 690);
        }
        else
        {
            isPinchDone=NO;
            [AnimationUtility moveFrame:bottomOrderView withFrame:CGRectMake(258, 324, 756, 374) withDuration:0.5];
            
            recognizer.view.frame = CGRectMake(258, 324, 756, 374);
            // recognizer.scale = 1;
            
        }
    }
}

#pragma mark - ProductStockViewControllerDelegate methods

//OLA!
-(void)didTapSaveButton
{
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];//enable product table selection when draggable view is hidden. OLA!
        
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)UOMPickerButton:(id)sender {
    
    UIButton *btn  = (UIButton *)sender;
    outletPopOver = nil;
    valuePopOverController=nil;
    
    outletPopOver = [[OutletPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    
    
    //display values for pop over
    
    NSMutableArray* UOMList=[[SWDatabaseManager retrieveManager] fetchUOMforItem:[NSString stringWithFormat:@"%@",[mainProductDict stringForKey:@"Item_Code"]]];
    
    if (UOMList.count>0) {
        
        
        NSLog(@"get UOM %@", [UOMList valueForKey:@"Item_UOM"]);
        
        outletPopOver.colorNames = [UOMList valueForKey:@"Item_UOM"];// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
        outletPopOver.delegate = self;
        
        if (valuePopOverController == nil) {
            //The color picker popover is not showing. Show it.
            valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:outletPopOver];
            [valuePopOverController presentPopoverFromRect:btn.frame inView:self.viewUOMCode permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            valuePopOverController.delegate=self;
        }
        else {
            //The color picker popover is showing. Hide it.
            [valuePopOverController dismissPopoverAnimated:YES];
            valuePopOverController = nil;
            valuePopOverController.delegate=nil;
        }
    }
}

-(void)selectedStringValue:(id)value
{
    
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SelectedUOMRef"];
    
    
    
    
    //update the conversion val
    
    
    NSMutableArray* fetchedConversionArray=[self fetchConversionFactorforItemCode:[mainProductDict stringForKey:@"Item_Code"] withItemUOM:value];
    NSLog(@"fetched values after uom changed %@", [fetchedConversionArray description]);

    if (fetchedConversionArray.count>0) {
        
        NSInteger conversionfactor=[[[fetchedConversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        mainProductDict[@"conversionfactor"] = [NSString stringWithFormat:@"%ld",(long)conversionfactor];
        uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ",(long)conversionfactor] stringByAppendingString:@"EA"];
        
    }
    

    selectedUOMReference=value;
    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:value :[customerDict stringForKey:@"Price_List_ID"]]];
    
    
    
    //         NSArray* produstDetail=[[SWDatabaseManager retrieveManager]fetchProductDetails:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"] UOMCode:[mainProductDict stringForKey:@"UOM"]];

    
    
    [self updatePrice];
    lblUOMCode.text=[NSString stringWithFormat:@" %@",value];
    [lblWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
    
    
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
        
    }
    [mainProductDict setObject:selectedUOMReference forKey:@"SelectedUOM"];
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
}
/** duplicate*/
-(void)selectedStringValueDuplicate:(id)value

{
    

    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SelectedUOMRef"];
    
    
    selectedUOMReference=value;
    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:value :[customerDict stringForKey:@"Price_List_ID"]]];
    [self updatePrice];
    lblUOMCode.text=[NSString stringWithFormat:@" %@",value];
    [lblWholesalePrice setText:[NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
    
    
    if (txtfldWholesalePrice.isHidden==NO) {
        
        [txtfldWholesalePrice setText: [NSString stringWithFormat:@" %@",[[mainProductDict stringForKey:@"Net_Price"] currencyString]]];
        
    }
    [mainProductDict setObject:selectedUOMReference forKey:@"SelectedUOM"];
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
}

#pragma mark sales statistics data


-(void)fetchLabelDatafromDB:(NSString*)agencyName

{
    
    achievementPercentLbl.text=@"0%";
    
    
//    NSString* labelDataQry =[NSString stringWithFormat:@"select SUM(Sales_Value) AS Sales_Value , SUM(Target_Value) AS Target_Value, sum(Custom_Attribute_7) AS Balance_To_Go ,Custom_Attribute_8 AS Daily_Req FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Classification_1='%@'",[customerDict valueForKey:@"Customer_No"],agencyName];
    
    
     NSString* labelDataQry =[NSString stringWithFormat:@"select IFNULL(Custom_Attribute_1 ,0) AS Actual_Achievement,IFNULL( SUM(Sales_Value),0) AS Sales_Value , IFNULL(SUM(Target_Value),0) AS Target_Value, IFNULL(sum(Custom_Attribute_7),0) AS Balance_To_Go ,IFNULL(SUM(Custom_Attribute_8),0) AS Daily_Req FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Classification_1='%@'",[customerDict valueForKey:@"Customer_No"],agencyName];
    
    
    
    NSLog(@"sales order agency data qry %@", labelDataQry);
    //topLevelDataArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:labelDataQry];
    
    
    NSMutableArray* testArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:labelDataQry];
    
    
    NSLog(@"test actual ach %@", [testArray description]);
    
    
    salesStatsArray=[self nullFreeArray:testArray];
    
    
    if (salesStatsArray.count>0) {
        
        
        NSLog(@"top level data is %@", [salesStatsArray description]);
        
        
        NSString * balanceToGo=[NSString stringWithFormat:@"%@",[[salesStatsArray objectAtIndex:0] valueForKey:@"Balance_To_Go"]];
        NSInteger tempBtg=[balanceToGo integerValue];
        
        balanceToGoLbl.text=[self thousandSaperatorforDouble:tempBtg];
        
//        balanceToGoLbl.text=[NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithDouble:tempBtg] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
        
        NSString * salesValue=[NSString stringWithFormat:@"%@",[[salesStatsArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
        NSInteger tempSales=[salesValue integerValue];
        
//        salesLbl.text=[NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithDouble:tempSales] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
//        
        
        salesLbl.text=[self thousandSaperatorforDouble:tempSales];
        
        
        
        NSString * targetValue=[NSString stringWithFormat:@"%@",[[salesStatsArray objectAtIndex:0] valueForKey:@"Target_Value"]];
        
        
        NSInteger targetTemp=[targetValue integerValue];
        
//          targetLbl.text=[NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithDouble:targetTemp] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
        
        
        targetLbl.text=[self thousandSaperatorforDouble:targetTemp];
        
        
//        targetLbl.text=[NSString stringWithFormat:@"%@",[SWDefaults formatStringWithThousandSeparator:[targetValue integerValue]]];
        
        
        
        NSString* dailyReq=[NSString stringWithFormat:@"%@",[[salesStatsArray objectAtIndex:0] valueForKey:@"Daily_Req"]];
        
        NSInteger tempDailyReq=[dailyReq integerValue];
        
        
        
        self.dailyRequirementLbl.text=[self thousandSaperatorforDouble:tempDailyReq];
        
        
//        self.dailyRequirementLbl.text=[NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithDouble:tempDailyReq] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
//        
        
        
        
        float totalTarget=[targetValue floatValue];
        
        float achievedTarget=[salesValue floatValue];
        //
        //float balanceTogo= totalTarget-achievedTarget;
        
        float achievementPercent=(achievedTarget/totalTarget)*100;
        
        
        NSLog(@"achievement percent for current customer is %0.2f",achievementPercent);
        
        /*
        if (achievedTarget>totalTarget) {
            
            achievementPercentLbl.textColor=[UIColor greenColor];
        }
        
        else if (achievementPercent<totalTarget)
        {
            achievementPercentLbl.textColor=[UIColor redColor];
        }
        
        else
        {
            
        }
        */
        
        
        
        NSString* actualAchievementStr=
        
        [NSString stringWithFormat:@"%@",[salesStatsArray valueForKey:@"Actual_Achievement"]] ;
        
        
       
        
        
        
 double tempDouble=[[[salesStatsArray valueForKey:@"Actual_Achievement"] objectAtIndex:0] doubleValue];
        NSLog(@"actual achievement percent in salesorder is %0.2f", tempDouble);
        
        
        
        
        if (isnan(tempDouble) || isinf(tempDouble)) {
            
        }
        
        else
        {
        
        if (tempDouble>=100) {
            
            achievementPercentLbl.textColor=[UIColor greenColor];
        }
        
        else if (tempDouble>=80)
        {
            achievementPercentLbl.textColor=[UIColor colorWithRed:(242.0/255.0) green:(197.0/255.0) blue:(117.0/255.0) alpha:1];
            
           
        }
        
        else
        {
            achievementPercentLbl.textColor=[UIColor redColor];

        }
        
        }
        
        if (achievementPercent) {
            
            
            if(isinf(achievementPercent))
            {
                achievementPercentLbl.text=@"0%";

            }
            
            else if (isnan(achievementPercent))
                
            {
                achievementPercentLbl.text=@"0%";

            }
            
            else
            {
            NSString* achStr=[NSString stringWithFormat:@"%0.0f",achievementPercent];
            
            
            achievementPercentLbl.text=[achStr stringByAppendingString:@"%"];
            }
            

        }

        
        
        
        
        
        
    }
    
    
}


-(NSMutableArray*)nullFreeArray:(NSMutableArray*)contentArray

{
    
    
    for(NSInteger i=0;i<contentArray.count;i++)
    {
        if([[contentArray objectAtIndex:i] isKindOfClass:[NSNull class]])
            [contentArray replaceObjectAtIndex:i withObject:@""];
    }
    
    return contentArray;
    
}


-(NSString*)thousandSaperatorforDouble:(NSInteger)number

{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
//    [numberFormatter setMinimumFractionDigits:2];
//    
//    [numberFormatter setMaximumFractionDigits:2];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    
    
    return result;
    
    
}


- (IBAction)infoButtonTapped:(id)sender {
    
    NSLog(@"wholesale price is %@", txtfldWholesalePrice.text);
    
    if ([self.isOrderConfirmed isEqualToString:@"confirmed"]) {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Confirmed orders cannot be edited." withController:self];
        return;
    }
    
    [txtProductQty resignFirstResponder];
    
    
    CustomerSalesViewController *salesHistory ;
    if (mainProductDict.count!=0)
    {
        salesHistory = [[CustomerSalesViewController alloc] initWithCustomer:customerDict andProduct:mainProductDict] ;
        if([popoverController isPopoverVisible])
        {
            
            [popoverController dismissPopoverAnimated:YES];
            return;
        }
        
        StockInfoSalesHistoryViewController* stockInfoVC=[[StockInfoSalesHistoryViewController alloc]init];
        
        UINavigationController *favNav = [[UINavigationController alloc]
                                          initWithRootViewController:stockInfoVC];
        
        favNav.title=@"TEST";
        favNav.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"xyz" style:UIBarButtonItemStyleDone target:self action:@selector(closeSalesHistory)];
        stockInfoVC.customDelegate=self;
        stockInfoVC.customerDictionary=customerDict;
        stockInfoVC.mainProductDictionary=mainProductDict;
        
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:favNav];
        popoverController.popoverContentSize=CGSizeMake(800, 650);
        stockInfoVC.popOver=popoverController;
        [popoverController presentPopoverFromRect:infoButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
}

-(void)closeSalesHistory
{
    [popoverController dismissPopoverAnimated:YES];
}


#pragma mark custom delegate for updating stock allocation

-(void) updateMainProductDict:(NSMutableDictionary *)mainProduct

{
    
    mainProductDict = nil;
    mainProductDict=[NSMutableDictionary dictionaryWithDictionary:mainProduct];
}
-(void)showPlaceHolderwithMessage:(NSString*)messageText
{
    placeHolderView.hidden=NO;
    statusLbl.text=messageText;
}
-(void)hidePlaceHolder
{
    placeHolderView.hidden=YES;
    
}

@end
