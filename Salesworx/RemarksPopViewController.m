//
//  RemarksPopViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "RemarksPopViewController.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "FMDBHelper.h"
#import "SWDatabaseManager.h"
#import "SWDefaults.h"

@interface RemarksPopViewController ()

@end

@implementation RemarksPopViewController

@synthesize selectedRow,popItemsArray,popArray,remarksTxtView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"remarks popover VC called");
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue 
 destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)closePopOverTapped:(id)sender {
    [self.popOver dismissPopoverAnimated:YES];
}

- (IBAction)saveRemarksTapped:(id)sender {
    NSString* rowID= [NSString createGuid];
    NSString* popID=[NSString stringWithFormat:@"%@",[[popArray valueForKey:@"POP_ID"] objectAtIndex:0]];
    NSString*popItemID=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"POP_ITEM_ID"] objectAtIndex:selectedRow] ];
    
    NSString* capturedRemarks=remarksTxtView.text;
    
    
    NSDate* currentDate=[NSDate date];
    NSLog(@"current date is %@", currentDate);
    
    NSDateFormatter* dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-dd-MM"];
    NSLog(@"formatted date %@", [dateFormatter stringFromDate:currentDate]);
    NSString* loggedAt=[dateFormatter stringFromDate:currentDate];
    NSString* loggedBy=[NSString stringWithFormat:@"%@", [SWDefaults username]];
    
    NSLog(@"contents in save action %@", [popItemsArray description]);
    NSLog(@"customer in save action %@", [SWDefaults customer]);
    
    
    BOOL success;
    FMDatabase *db = [FMDBHelper getDatabase];
    [db open];
    
    @try {
        success =  [db executeUpdate:@"INSERT into TBL_POP_Remarks(Row_ID,POP_ID,POP_ITEM_ID,Remarks,logged_At,Logged_By) VALUES (?,?,?,?,?,?)", rowID,popID,popItemID,capturedRemarks,loggedAt,loggedBy, nil];
        
        if (success==YES) {
            NSLog(@"insert successfull");
            
            [SWDefaults showAlertAfterHidingKeyBoard:@"Success" andMessage:@"Remarks Added Successfully" withController:self];
            [self.popOver dismissPopoverAnimated:YES];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while updating remarks in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}

#pragma mark UITextField Delegate methods


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if (text.length==0) {
        
        return YES;
    }
    
    
    if ([textView.text length]<=200) {
        
        return YES;
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Limit Reached" andMessage:@"please make sure that your remarks are less than 200 characters" withController:self];
        return NO;
    }
}



@end
