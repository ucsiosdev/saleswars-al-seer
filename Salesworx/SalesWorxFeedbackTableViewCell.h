//
//  SalesWorxFeedbackTableViewCell.h
//  SalesWars
//
//  Created by Neha Gupta on 1/31/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxTableViewHeaderLabel.h"
#import "MedRepTextField.h"

@interface SalesWorxFeedbackTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet SalesWorxTableViewHeaderLabel *lblCode;
@property (strong, nonatomic) IBOutlet SalesWorxTableViewHeaderLabel *lblDescription;
@property (strong, nonatomic) IBOutlet SalesWorxTableViewHeaderLabel *lblInventoryItemID;
@property (strong, nonatomic) IBOutlet MedRepTextField *statusTextField;
@property (strong, nonatomic) IBOutlet UIButton *btnSave;

@end
