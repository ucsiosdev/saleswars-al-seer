//
//  SWCustomerListCell.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWCustomerListCell.h"
#import "SWFoundation.h"
#import "Singleton.h"

@implementation SWCustomerListCell

@synthesize statusImageView;
@synthesize codeLabel;


- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setStatusImageView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"red.png"cache:NO]]];
        [self.statusImageView setBackgroundColor:[UIColor clearColor]];
        //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

        
        [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

        [self.textLabel setFont:kFontWeblySleekSemiBold(14)];
        [self.textLabel setBackgroundColor:[UIColor clearColor]];
        [self.contentView addSubview:self.statusImageView];
        
        [self setCodeLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.codeLabel setBackgroundColor:[UIColor clearColor]];
        [self.codeLabel setFont:kFontWeblySleekSemiBold(14)];
        [self.contentView addSubview:self.codeLabel];


    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //[self.statusImageView setFrame:CGRectMake(self.contentView.bounds.size.width - 17,5, 15,40)];
    [self.statusImageView setFrame:CGRectMake(0, 0, 2, self.contentView.bounds.size.height)];
    CGRect labelFrame = self.textLabel.frame;
    labelFrame.size.width = labelFrame.size.width - 17;
    labelFrame.origin.y = labelFrame.origin.y - 7;
    [self.textLabel setFrame:labelFrame];
    [self bringSubviewToFront:self.statusImageView];
    
    [self.codeLabel setFrame:CGRectMake(10, 26, 200, 25)];
    
    //[self.lblStatus setFrame:CGRectMake(0, 0, 2, self.contentView.bounds.size.height)];

}



- (NSMutableDictionary *)recursive:(NSMutableDictionary *)dictionary {
    for (NSString *key in [dictionary allKeys]) {
        id nullString = [dictionary objectForKey:key];
        if ([nullString isKindOfClass:[NSDictionary class]]) {
            [self recursive:(NSMutableDictionary*)nullString];
        } else {
            if ((NSString*)nullString == (id)[NSNull null])
                [dictionary setValue:@"" forKey:key];
        }
    }
    return dictionary;
}
- (void)applyAttributes:(NSDictionary *)row
{
    
  NSMutableDictionary*  nullFree=[self recursive:[row mutableCopy]];
    
    row=nullFree;
    
    
    
    [self.textLabel setText:[row objectForKey:@"Customer_Name"]];
    //[self.codeLabel setText:[row objectForKey:@"Customer_No"]];
    BOOL customerStatus = [[row objectForKey:@"Cust_Status"] isEqualToString:@"Y"];
    BOOL cashCustomer = [[row objectForKey:@"Cash_Cust"] isEqualToString:@"Y"];
    BOOL creditHold = [[row objectForKey:@"Credit_Hold"] isEqualToString:@"Y"];
   
    NSString *imageName = @"transparent.png";
    
    if (cashCustomer) {
        imageName = @"yellow.png";
    }
    
    if (!customerStatus || creditHold) {
        imageName = @"red.png";
    }
    
    [self.statusImageView setImage:[UIImage imageNamed:imageName cache:NO]];
}


@end
