//
//  FOCView.h
//  SWCustomer
//
//  Created by msaad on 12/19/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWPlatform.h"

@interface FOCView : UIView 
{
    
    UIView *productView;
    UIView *bgTransparentImageView;
    UIView *duesView;
}



@property (nonatomic, strong)  UIView *productView;



@end
