//
//  SWFoundation.h
//  SWFoundation
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "EditableCell.h"
#import "SWTextFieldCell.h"
#import "SWDatePicker.h"
#import "SWDateFieldCell.h"
#import "SWTextViewCell.h"
#import "SWSignatureCell.h"
#import "NSString+Additions.h"
#import "NSDictionary+Additions.h"
#import "UITableViewCell+Responder.h"
#import "GridCellView.h"
#import "GridView.h"
#import "SWSignatureView.h"
#import "NSData+Base64.h"
#import "JBSignatureView.h"
#import "JBSignatureController.h"
#import "NSData+Compression.h"


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define GroupHeaderTextColor    0x4C566C
#define ColorForBackground      0xEDEDED
#define ColorForAlternateRow    0xE5E5E5
#define ColorForHeaderRow       0xAEAEAE
#define ColorForRowBorder       0xD7D7D7
#define ColorForHeaderBorder    0xA8A8A8
#define colorDarkGrey     0x6A6F7B
#define colorBlack     0x2C394A

#define UIColorForHeader() [UIColor \
    colorWithPatternImage:[UIImage imageNamed:@"headerBK.png" cache:NO]]

#define UIColorForPlainTable() [UIColor \
    colorWithPatternImage:[UIImage imageNamed:@"grouptablebackground.png" cache:NO]]

#define RegularFontOfSize(value) [UIFont \
    fontWithName:@"OpenSans-Regular" size:value]

#define BoldFontOfSize(value) [UIFont \
    fontWithName:@"OpenSans-Bold" size:value]

#define LightFontOfSize(value) [UIFont \
    fontWithName:@"OpenSans-Light" size:value]

#define BoldSemiFontOfSize(value) [UIFont \
    fontWithName:@"OpenSans-Semibold" size:value]

#define EditableTextFieldSemiFontOfSize(value) [UIFont \
fontWithName:@"WeblySleekUISemibold" size:value]

#define EditableTextFieldLightFontOfSize(value) [UIFont \
fontWithName:@"WeblySleekUILight" size:value]

#define LabelFieldSemiFontOfSiz(value) [UIFont \
fontWithName:@"WeblySleekUISemibold" size:value]

#define kFontWeblySleekSemiBold(value) [UIFont \
fontWithName:@"WeblySleekUISemibold" size:value]

#define kFontWeblySleekLight(value) [UIFont \
fontWithName:@"WeblySleekUILight" size:value]

#define kFontWeblySleekSemiLight(value) [UIFont \
fontWithName:@"WeblySleekUISemilight" size:value]


#define DARK_MODERATE_BLUE [UIColor colorWithRed:36.0f/255.0f green:89.0f/255.0f blue:114.0f/255.0f alpha:1.0f]
#define OPENSANSREGULAR_16 [UIFont fontWithName:@"opensansregular" size:16.0f]
