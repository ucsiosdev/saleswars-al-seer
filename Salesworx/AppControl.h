//
//  AppControl.h
//  SWPlatform
//
//  Created by msaad on 5/5/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+Additions.h"
@interface AppControl : NSObject
{
    NSMutableDictionary *appControlDict;
    NSString *ENABLE_ORDER_HISTORY;
    NSString *ENABLE_TEMPLATES;
    NSString *ENABLE_COLLECTION;
    NSString *ENABLE_DISTRIB_CHECK;
    NSString *ALLOW_LOT_SELECTION;
    NSString *ENABLE_WHOLESALE_ORDER;
    NSString *ENABLE_ORDER_CONSOLIDATION;
    NSString *ALLOW_SKIP_CONSOLIDATION;
    NSString *ALLOW_SHIP_DATE_CHANGE;
    NSString *ALLOW_BONUS_CHANGE;
    NSString *ENABLE_MANUAL_FOC;
    NSString *ALLOW_MANUAL_FOC_ITEM_CHANGE;
    NSString *ALLOW_DISCOUNT;
    NSString *ALLOW_DUAL_FOC;
    NSString *IS_SIGNATURE_OPTIONAL;
    NSString *ENABLE_LINE_ITEM_NOTES;
    NSString *SHOW_ON_ORDER;
    NSString *SHOW_PRODUCT_TARGET;
    NSString *ROUTE_SYNC_DAYS;
    NSString *SD_BUCKET_SIZE;
    NSString *SD_DURATION_MON;
    NSString *SHOW_SALES_HISTORY;
    NSString *ODOMETER_READING;
    NSString *IS_START_DAY;
    NSString *ALLOW_MULTI_CURRENCY;
    NSString *IS_LOT_OPTIONAL;
    NSString *ENABLE_BONUS_GROUPS;
    NSString *BONUS_MODE;
    NSString *ENABLE_PRODUCT_IMAGE;
    NSString *IS_SURVEY_OPTIONAL;
    NSString *CUST_DTL_SCREEN;
    NSString *DASHBOARD_TYPE;
    NSString * ENABLE_DIST_CHECK_MULTI_LOCATION;
    NSString*  VISIT_TRANSACTION_MANDATORY;
    NSString * DEFAULT_AVAILABILITY_IN_DC;




}
@property (nonatomic, strong)NSString *ENABLE_ORDER_HISTORY;
@property (nonatomic, strong)NSString *ENABLE_TEMPLATES;
@property (nonatomic, strong)NSString *ENABLE_COLLECTION;
@property (nonatomic, strong)NSString *ENABLE_DISTRIB_CHECK;
@property (nonatomic, strong)NSString *ALLOW_LOT_SELECTION;
@property (nonatomic, strong)NSString *ENABLE_WHOLESALE_ORDER;
@property (nonatomic, strong)NSString *ALLOW_SKIP_CONSOLIDATION;
@property (nonatomic, strong)NSString *ALLOW_SHIP_DATE_CHANGE;
@property (nonatomic, strong)NSString *ALLOW_BONUS_CHANGE;
@property (nonatomic, strong)NSString *ENABLE_MANUAL_FOC;
@property (nonatomic, strong)NSString *ALLOW_MANUAL_FOC_ITEM_CHANGE;
@property (nonatomic, strong)NSString *ALLOW_DISCOUNT;
@property (nonatomic, strong)NSString *ALLOW_DUAL_FOC;
@property (nonatomic, strong)NSString *IS_SIGNATURE_OPTIONAL;
@property (nonatomic, strong)NSString *ENABLE_LINE_ITEM_NOTES;
@property (nonatomic, strong)NSString *SHOW_ON_ORDER;
@property (nonatomic, strong)NSString *SHOW_PRODUCT_TARGET;
@property (nonatomic, strong)NSString *ROUTE_SYNC_DAYS;
@property (nonatomic, strong)NSString *ENABLE_ORDER_CONSOLIDATION;
@property (nonatomic, strong)NSString *SD_BUCKET_SIZE;
@property (nonatomic, strong)NSString *SD_DURATION_MON;
@property (nonatomic, strong)NSString *SHOW_SALES_HISTORY;
@property (nonatomic, strong)NSString *ODOMETER_READING;
@property (nonatomic, strong)NSString *IS_START_DAY;
@property (nonatomic, strong)NSString *ALLOW_MULTI_CURRENCY;
@property (nonatomic, strong)NSString *IS_LOT_OPTIONAL;
@property (nonatomic, strong)NSString *ENABLE_BONUS_GROUPS;
@property (nonatomic, strong)NSString *BONUS_MODE;
@property (nonatomic, strong)NSString *ENABLE_PRODUCT_IMAGE;
@property (nonatomic, strong)NSString *IS_SURVEY_OPTIONAL;
@property (nonatomic, strong)NSString *CUST_DTL_SCREEN;
@property (nonatomic, strong)NSString *DASHBOARD_TYPE;
@property(strong,nonatomic)NSString * ENABLE_DIST_CHECK_MULTI_LOCATION;
@property(strong,nonatomic)NSString * DEFAULT_AVAILABILITY_IN_DC;
@property(strong,nonatomic)NSString* ENABLE_DIST_CHECK_MULTI_LOTS ;
@property(strong,nonatomic)NSString* FS_SHOW_DC_FILTER;
@property(strong,nonatomic)NSString* FS_SHOW_DC_MIN_STOCK;
@property(strong,nonatomic)NSString* VISIT_TRANSACTION_MANDATORY ;
@property(strong,nonatomic)NSString* FS_SHOW_DC_AVBL_SEG;
@property(strong,nonatomic) NSString* MANDATORY_QTY_IN_DC;
@property(strong,nonatomic) NSString* MANDATORY_EXP_DT_IN_DC;
@property(strong,nonatomic)NSString* FS_SHOW_DC_RO;

@property(strong,nonatomic) NSString *FSR_LOC_TRK_FREQ;
@property(strong,nonatomic) NSString *LOC_TRK_START_TIME;
@property(strong,nonatomic) NSString *LOC_TRK_END_TIME;

@property(strong,nonatomic) NSString* V_TRX_SYNC_INTERVAL;
@property(strong,nonatomic) NSString* ENABLE_BACKGROUND_SYNC;

- (void)initializeAppControl;
+ (AppControl*) retrieveSingleton;
+ (void) destroyMySingleton;

@end
