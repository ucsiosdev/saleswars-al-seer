//
//  SWGridViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 6/25/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWGridViewController.h"

@interface SWGridViewController ()

@end

@implementation SWGridViewController

@synthesize items;
@synthesize gridView;

- (id)initWithDocumentReference:(NSString *)ref {
    self = [super init];
    
    if (self) {
        [self setTitle:@"GridViewController"];
        [self setItems:[NSMutableArray array]];
    }
    
    return self;
}


//- (void)dealloc
//{
//    NSLog(@"Dealloc %@",self.title);
//    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setGridView:[[GridView alloc] initWithFrame:CGRectZero]];
    [self.gridView setFrame:self.view.bounds];
    [self.gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [self.gridView setDataSource:self];
    [self.gridView setDelegate:self];
    [self.view addSubview:self.gridView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setPreferredContentSize:self.gridView.tableView.contentSize];
}

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];

}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"GridViewItemsCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
    
}

#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return self.items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    
    return text;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
