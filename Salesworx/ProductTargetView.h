//
//  ProductTargetView.h
//  SWPlatform
//
//  Created by Irfan on 11/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "SWPlatform.h"
#import <QuartzCore/QuartzCore.h>

#import "PCPieChart.h"

@class PieChartView;

@interface ProductTargetView : UIView <UITableViewDelegate, UITableViewDataSource,UIPopoverControllerDelegate>
{
    ProductHeaderView *customerHeaderView;

    
    UITableView *targetTableView;
    NSDictionary *customer;
    NSMutableArray *targets;
    NSMutableDictionary *customerPie;
    UITableView *categoryTableView;
    float achievmentValue ;
    UIViewController *popOverContent;
    UIView *backGroundView;
    PCPieChart *pieChartNew;
    NSMutableArray *components;
     NSMutableArray *slices;
    NSArray        *sliceColors;
    
    
     double sampleSalesVal,sampleTargetVal;
    
}
//@property (nonatomic, strong) ProductService *serProduct;





- (id)initWithProduct:(NSDictionary *)row;
- (void)loadTarget;
- (void)loadPieChart;
@end
