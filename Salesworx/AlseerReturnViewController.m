//
//  AlseerReturnViewController.m
//  Salesworx
//
//  Created by Pavan Kumar Singamsetti on 2/25/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlseerReturnViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ReturnTypeViewController.h"
#import "ProductBonusViewController.h"
#import "SWVisitOptionsViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepDefaults.h"
#import "TableCellHeaderReturn.h"
#import "TblCellReturnProduct.h"
#import "SWFoundation.h"


@interface AlseerReturnViewController ()
{
     NSString *dateString;
    IBOutlet UILabel *statusLbl;
    IBOutlet SalesWorxDropShadowView *placeHolderView;
    IBOutlet UIImageView *placeHolderImageView;
}
@end
#define NUMERIC                 @"1234567890"


@implementation AlseerReturnViewController
@synthesize customerDict,uomConversionFactorLbl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
   [self setNavigBar];
    
   [self setupView];
    
    [self setAllViewBorder];
    
    [self setFontAndColor];
    
    [self addShadowOnUI];
    
    //s .tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    productTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    [productTableView registerNib:[UINib nibWithNibName:@"TblCellReturnProduct" bundle:nil] forCellReuseIdentifier:@"TblCellReturnProduct"];
    
    
    [bonusBtn setHidden:YES];
    [lblProductBonus setHidden:YES];
    [txtDefBonus setHidden:YES];
    
    
     self.title = NSLocalizedString(@"Return", nil);
    
    oldSalesOrderVC = [[AlseerReturnCaluclationViewController alloc] initWithCustomer:customerDict andCategory:[SWDefaults productCategory] andViewController:self];
    
    int temp = 60;
    oldSalesOrderVC.view.frame = CGRectMake(0, 0, bottomOrderView.frame.size.width , bottomOrderView.frame.size.height+temp);
    oldSalesOrderVC.view.clipsToBounds = YES;
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];
    
    productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];
    
    
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray )
    {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Agency"]];
        
        if ( theMutableArray == nil )
        {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Agency"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    
    lblCustomerName.text = [customerDict stringForKey:@"Customer_Name"];
    [SWDefaults setPaymentType:nil];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    dateString =  [formatter stringFromDate:[NSDate date]];
    
    if (@available(iOS 15.0, *)) {
        productTableView.sectionHeaderTopPadding = 0;
    }
}

-(void)testShadow:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate=self;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    oldSalesOrderVC.delegate = nil;
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
     self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    //search bar top round corner
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: candySearchBar.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    candySearchBar.layer.mask = maskLayer;
    
    CAShapeLayer * maskLayer_4 = [CAShapeLayer layer];
    maskLayer_4.path = [UIBezierPath bezierPathWithRoundedRect: viewParentSearchBar.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    viewParentSearchBar.layer.mask = maskLayer_4;
    
    
    /*
    CAShapeLayer * maskLayer_2 = [CAShapeLayer layer];
    maskLayer_2.path = [UIBezierPath bezierPathWithRoundedRect: bottomOrderView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    bottomOrderView.layer.mask = maskLayer_2;
    
    
    CAShapeLayer * maskLayer_3 = [CAShapeLayer layer];
    maskLayer_3.path = [UIBezierPath bezierPathWithRoundedRect: bottomOrderView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    bottomOrderView.layer.mask = maskLayer_3;
    
    */
   
    
    
}

/*
- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
 
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {corner = corner | UIRectCornerTopLeft;}
        if (tr) {corner = corner | UIRectCornerTopRight;}
        if (bl) {corner = corner | UIRectCornerBottomLeft;}
        if (br) {corner = corner | UIRectCornerBottomRight;}
 
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}
*/

-(void)setNavigBar{
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    
    //left bar button
    UIBarButtonItem *close = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)];
    
  [close setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = close;
    
    /*
    //right bar button
    UIBarButtonItem *saveOrder = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save Order", nil) style:UIBarButtonItemStyleDone target:self action:@selector(saveReturns:)];
    
    [saveOrder setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    
    self.navigationItem.rightBarButtonItem = saveOrder;
    */
}
- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [candySearchBar layoutSubviews];
    
    //handle case: Sometimes search bar's cancel button overcomes on search bar's text filed
    if (candySearchBar.showsCancelButton){
        return;
    }
    
    for(UIView *view in candySearchBar.subviews)
    {
        for(UITextField *textfield in view.subviews)
        {
            if ([textfield isKindOfClass:[UITextField class]]) {
                textfield.frame = CGRectMake(13.0f, 13.0f, viewParentSearchBar.frame.size.width-26, 30.0f);
            }
        }
    }
}


-(void)setupView{
    
    
    //adding padding to uitextfield
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];
    txtProductQty.leftView = paddingView;
    txtProductQty.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingViewLot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];
    txtLot.leftView = paddingViewLot;
    txtLot.leftViewMode = UITextFieldViewModeAlways;
    
    
    
    // UIColor(red: 235.0/255.0, green: 251.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    //candySearchBar.barTintColor = [UIColor colorWithRed:235.0/255 green:251.0/255 blue:249.0/255 alpha:1.0];

    //set border of search bar text filed as per UI/Design
    for (id object in [[[candySearchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            
            //textFieldObject.frame = CGRectMake(13, 5, 50, 30);
            textFieldObject.layer.borderColor =  UIColorFromRGB(0xE3E4E6).CGColor;
            textFieldObject.layer.borderWidth = 1.0;
            textFieldObject.layer.cornerRadius = 4.5;
            break;
        }
    }
    
    candySearchBar.layer.borderWidth = 1.0;
    candySearchBar.layer.borderColor = [UIColor colorWithRed:235.0/255 green:251.0/255 blue:249.0/255 alpha:1.0].CGColor;
    
    lblCustomerName.text = [SWDefaults getValidStringValue:[customerDict valueForKey:@"Customer_Name"]];
    lblCustomerNo.text = [SWDefaults getValidStringValue:[customerDict valueForKey:@"Customer_No"]];
    
    BOOL customerStatus = [[SWDefaults getValidStringValue: [customerDict objectForKey:@"Cust_Status"]] isEqualToString:@"Y"];
    BOOL creditHold = [[SWDefaults getValidStringValue:[customerDict objectForKey:@"Credit_Hold"]] isEqualToString:@"Y"];
   
    
   if (!customerStatus || creditHold) {
       lblCustomerStatus.text = @"Blocked";
       lblCustomerStatus.backgroundColor =  UIColorFromRGB(0xFF736E);
       lblCustomerStatus.hidden = NO;
   }else {
         lblCustomerStatus.hidden = YES;
        //lblCustomerStatus.text = @"Active";
        //lblCustomerStatus.backgroundColor = UIColorFromRGB(0x47B681);
   }
    lblCustomerStatus.textColor =  [UIColor whiteColor];
    lblCustomerStatus.layer.cornerRadius = 10.0;
    lblCustomerStatus.layer.masksToBounds = YES;

    
    if ([customerDict objectForKey:@"Avail_Bal"]) {
       double availBal = [[customerDict stringForKey:@"Avail_Bal"] doubleValue];
       lblAvailableBalance.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
    }
    
    
   
}

-(void) setAllViewBorder{
    
    bottomOrderView.layer.cornerRadius = 8.0;
    bottomOrderView.layer.masksToBounds = YES;
    
    placeHolderView.layer.cornerRadius = 8.0;
    placeHolderView.layer.masksToBounds = YES;
    
    viewPrice.layer.cornerRadius = 8.0;
    productTableView.layer.cornerRadius = 8.0;
    productTableView.layer.masksToBounds = YES;
    viewPrice.layer.masksToBounds = YES;
    lblWholePrice.layer.cornerRadius = 8.0;
    lblWholePrice.layer.masksToBounds = YES;
    addBtn.layer.cornerRadius = 8.0;
    btnClear.layer.cornerRadius = 8.0;
    addBtn.layer.masksToBounds = YES;
    btnClear.layer.masksToBounds = YES;
}


-(void)setFontAndColor{
    
    lblCustomerName.font = kFontWeblySleekSemiBold(16);
    lblCustomerName.textColor = UIColorFromRGB(0x2C394A);
    
    lblCustomerNo.font = kFontWeblySleekSemiBold(14);
    lblCustomerNo.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblAvailableBalance.font = kFontWeblySleekSemiBold(16);
    lblAvailableBalance.textColor = UIColorFromRGB(0x2C394A);
    
    
    lblAviableBalStatic.font = kFontWeblySleekSemiBold(14);
    lblAviableBalStatic.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblProductName.font = kFontWeblySleekSemiBold(16);
    lblProductName.textColor = UIColorFromRGB(0x2C394A);
    
    lblProductCode.font = kFontWeblySleekSemiBold(14);
    lblProductCode.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblUOMCodeStatic.font = kFontWeblySleekSemiBold(14);
    lblUOMCodeStatic.textColor = UIColorFromRGB(0x6A6F7B);
    
    
    lblProductQty.font = kFontWeblySleekSemiBold(14);
    lblProductQty.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblWholePrice.font = kFontWeblySleekSemiBold(14);
    lblWholePrice.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblLotNumber.font = kFontWeblySleekSemiBold(14);
    lblLotNumber.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblReturnType.font = kFontWeblySleekSemiBold(14);
    lblReturnType.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblExpiryDate.font = kFontWeblySleekSemiBold(14);
    lblExpiryDate.textColor = UIColorFromRGB(0x6A6F7B);
    
    btnClear.titleLabel.font = kFontWeblySleekSemiBold(16);
    addBtn.titleLabel.font = kFontWeblySleekSemiBold(16);
    
    lblUOMCode.font = kFontWeblySleekSemiBold(14);
    lblUOMCode.textColor = UIColorFromRGB(0x2C394A);
    
    txtProductQty.font = kFontWeblySleekSemiBold(14);
    txtProductQty.textColor = UIColorFromRGB(0x2C394A);
    
    lblWPAmount.font = kFontWeblySleekSemiBold(16);
    lblWPAmount.textColor = UIColorFromRGB(0x2C394A);
    
    uomConversionFactorLbl.font = kFontWeblySleekSemiBold(14);
    uomConversionFactorLbl.textColor = UIColorFromRGB(0x6A6F7B);
    
    txtLot.font = kFontWeblySleekSemiBold(14);
    txtLot.textColor = UIColorFromRGB(0x2C394A);
    
    btnReturnType.titleLabel.font = kFontWeblySleekSemiBold(14);
    btnReturnType.titleLabel.textColor = UIColorFromRGB(0x2C394A);
    
    
    btnExpiryDate.titleLabel.font = kFontWeblySleekSemiBold(15);
    btnExpiryDate.titleLabel.textColor = UIColorFromRGB(0x2C394A);
    
    statusLbl.font = kFontWeblySleekSemiBold(16);
    statusLbl.textColor = UIColorFromRGB(0x2C394A);
    
    
    lblUOMCode.backgroundColor = [UIColor whiteColor];
    txtProductQty.backgroundColor = [UIColor whiteColor];
    uomConversionFactorLbl.backgroundColor = [UIColor whiteColor];
    txtLot.backgroundColor = [UIColor whiteColor];
    btnReturnType.backgroundColor = [UIColor whiteColor];
    btnExpiryDate.backgroundColor = [UIColor whiteColor];
    viewUOMCode.backgroundColor = [UIColor whiteColor];
    viewPrice.backgroundColor = [UIColor colorWithRed:212./255 green:219.0/255 blue:226.0/255 alpha:1.0];
    lblWPAmount.backgroundColor = [UIColor clearColor];
}

-(void)addShadowOnUI{
    [self setShadowOnUILayer:viewUOMCode.layer];
    [self setShadowOnUILayer:txtProductQty.layer];
    //[self setShadowOnUILayer:viewPrice.layer];
    [self setShadowOnUILayer:txtLot.layer];
    [self setShadowOnUILayer:btnReturnType.layer];
    [self setShadowOnUILayer:btnExpiryDate.layer];
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}

#pragma mark -

- (void)selectedProduct:(NSMutableDictionary *)product
{
    //NSLog(@"SPRE %@",product);

    
    NSString* itemCode;//=[mainProductDict stringForKey:@"Item_Code"];
    
    
    itemCode=[product valueForKey:@"Item_Code"];
    
    if (itemCode==nil|| [itemCode isEqual:[NSNull null]]) {
        
    }
    
    

    
    NSMutableArray* fetchedConversionArray=[self fetchConversionFactorforItemCode:itemCode withItemUOM:[product stringForKey:@"Order_Quantity_UOM"]];
    NSLog(@"fetched values after product selected %@", [fetchedConversionArray description]);
    
    if (fetchedConversionArray.count>0) {
        
        NSInteger conversionfactor=[[[fetchedConversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ",(long)conversionfactor] stringByAppendingString:@"EA"];
        
    }
    
    
    

    
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    lblUOMCode.text=[NSString stringWithFormat:@" %@",[product valueForKey:@"SelectedUOM"]];
    selectedUOMReference=[product valueForKey:@"SelectedUOM"];
    if (product!=nil) {
        mainProductDict=nil;
        mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
        
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        lblProductName.text = [mainProductDict stringForKey:@"Description"];
        txtProductQty.text = [mainProductDict stringForKey:@"Qty"];
        lblWPAmount.text = [[mainProductDict stringForKey:@"Net_Price"] currencyString];
        
        lblRPAmount.text = [[mainProductDict stringForKey:@"List_Price"] currencyString];
        txtDefBonus.text = [mainProductDict stringForKey:@"Bonus"];
        txtLot.text = [mainProductDict stringForKey:@"lot"];
        [btnExpiryDate setTitle:[mainProductDict stringForKey:@"EXPDate"] forState:UIControlStateNormal];
        [btnReturnType setTitle:[mainProductDict stringForKey:@"reason"] forState:UIControlStateNormal];
        [addBtn setTitle:@"Update" forState:UIControlStateNormal];
    }
    else
    {
        mainProductDict=nil;
        mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
        
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        lblProductName.text = @"Product Name";
        txtProductQty.text = @"";
        lblWPAmount.text = @"";
        lblRPAmount.text = @"";
        txtDefBonus.text =@"";
        txtLot.text = @"";
        lblUOMCode.text=@"";
        uomConversionFactorLbl.text=@"";
        [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
        [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
    }
}

- (void)closeVisit:(id)sender {
    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[SWVisitOptionsViewController class]]) {
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }];
    
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
    }];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Would you like to close this returns?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
}

- (void)saveReturns:(id)sender {
    NSLog(@"\n saveReturns clicked \n");
}

#pragma mark - UITableView
-(void)sectionButtonTouchUpInside:(TableCellHeaderReturn*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        //..sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
       // sender.btnItemName.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    else {
       //..r sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        //sender.btnItemName.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        
        
        NSInteger collapedSectionsCount=_collapsedSections.count;
        
        
        for (NSInteger i=0; i<collapedSectionsCount; i++) {
            
            NSArray *myArray = [_collapsedSections allObjects];
            
            NSInteger sectionVal=[[myArray objectAtIndex:i] integerValue];
            int numOfRows = [productTableView numberOfRowsInSection:sectionVal];
            NSArray* indexPaths = [self indexPathsForSection:sectionVal withNumberOfRows:numOfRows];
            [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            [_collapsedSections removeObject:@(sectionVal)];
            
        }

        
        NSInteger numOfRows = [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
    
    [productTableView reloadData];
   
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == productTableView){
        return 81;
    }
    
    return 55;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            NSLog(@"\n\n *** number Of Sections In TableView: 1 ***\n\n");
            return 1;
            
        }
        else
        {
            NSLog(@"\n\n *** number Of Sections In TableView: %lu ***\n\n",(unsigned long)finalProductArray.count);
            return [finalProductArray count];
        }
    }
    else
    {
        NSLog(@"\n\n *** number Of Sections In TableView: 1 ***\n\n");
        return 1;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            NSLog(@"\n\n *** Section: %ld, rows: %lu ***\n\n",(long)section,(unsigned long)filteredCandyArray.count);
            return [filteredCandyArray count];
            
        } else {
           
            NSLog(@"\n\n *** Section: %ld, rows: %lu ***\n\n",section,[_collapsedSections containsObject:@(section)] ? [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count] :0);
            
            return [_collapsedSections containsObject:@(section)] ? [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count] :0 ;
            
        }
    }
    else
    {
        NSLog(@"\n\n *** Section: %ld, rows: %lu ***\n\n",section,(unsigned long)productArray.count);
        return [productArray count];
        
    }
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    }
    else
    {
        return 0;
    }
    
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            static NSString* tableID=@"TableCellHeaderReturn";
            TableCellHeaderReturn * cell=[tv dequeueReusableCellWithIdentifier:tableID];
            
            if (cell==nil) {
                NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:tableID owner:self options:nil];
                cell= [cellArray objectAtIndex:0];
            }
            
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            [cell.btnItemName setTitle:[[row objectAtIndex:0] stringForKey:@"Agency"] forState:UIControlStateNormal];
            
           
            [cell.btnItemName setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
            cell.btnItemName.tag = s;
            [cell.btnItemName addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnItemName.titleLabel.font = kFontWeblySleekSemiBold(16);
            cell.backgroundColor = [UIColor whiteColor];
            
            bool shouldCollapse = ![_collapsedSections containsObject:@(s)];
            if (shouldCollapse) {
                [cell.btnItemName setImage:[UIImage imageNamed:@"plusIcon"] forState:UIControlStateNormal];
                [cell.btnItemName setTitleColor:UIColorFromRGB(0x6A6F7B) forState:UIControlStateNormal];
            }else {
                 [cell.btnItemName setImage:[UIImage imageNamed:@"minusIon"] forState:UIControlStateNormal];
                 [cell.btnItemName setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
                
            }
            
            
            CGFloat spacing = 210;
            CGFloat spacingLeft = -14;
            cell.btnItemName.titleEdgeInsets = UIEdgeInsetsMake(0, spacingLeft, 0, 36); // top, left, bottom, right
            cell.btnItemName.imageEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
            
            return cell;
            
            /*
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width-33, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            //result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            //result.backgroundColor=[UIColor whiteColor];
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Agency"] forState:UIControlStateNormal];
            result.tag = s;
            //[result.layer setBorderColor:[UIColor whiteColor].CGColor];
           // [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=kFontWeblySleekSemiBold(14);;
            //[result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            //result.titleLabel.textColor = UIColorFromRGB(0x169C5C);
            result.titleLabel.textAlignment = UIControlContentHorizontalAlignmentLeft;
            headerView.backgroundColor=[UIColor whiteColor];
            result.backgroundColor = [UIColor clearColor];
            [result setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];

            [headerView addSubview:result];
            
            UIImageView *arrow =[[UIImageView alloc] initWithFrame:CGRectMake(headerView.bounds.size.width-36,0,25,25)];
            bool shouldCollapse = ![_collapsedSections containsObject:@(s)];
            if (shouldCollapse) {
                arrow.image=[UIImage imageNamed:@"minusIon"];
            }else {
                arrow.image=[UIImage imageNamed:@"plusIcon"];
                
            }
            [headerView addSubview:arrow];
            return headerView;
             */
            
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* tableID=@"TblCellReturnProduct";
     TblCellReturnProduct * cell=[tableView dequeueReusableCellWithIdentifier:tableID];
    
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:tableID owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    if (bSearchIsOn)
    {
        if (filteredCandyArray.count <= indexPath.row){
            return cell;
        }
        
        NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
        cell.lblProductName.text = [row stringForKey:@"Description"];
        cell.lblProdcutCode.text = [row stringForKey:@"Item_Code"];
    } else {
        if (finalProductArray.count <= indexPath.section){
            return cell;
        }
        
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
        
        NSLog(@"check row for lot qty %@", [row description]);

        cell.lblProductName.text = [row stringForKey:@"Description"];
        cell.lblProdcutCode.text = [row stringForKey:@"Item_Code"];
        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            cell.tag = 1;
            cell.viewProdcutStatus.backgroundColor = [UIColor redColor];
        }
        else
        {
            cell.tag = 0;
            cell.viewProdcutStatus.backgroundColor = [UIColor clearColor];
        }
    }
    
    cell.viewProdcutStatus.backgroundColor = [UIColor blueColor];
    
    cell.lblProductName.font = kFontWeblySleekSemiBold(16);
    cell.lblProductName.textColor = UIColorFromRGB(0x2C394A);

    cell.lblProdcutCode.font = kFontWeblySleekSemiBold(14);
    cell.lblProdcutCode.textColor = UIColorFromRGB(0x6A6F7B);
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hidePlaceHolder];
    
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtLot.text=@"";
    lblRPAmount.text=@"";
    lblWPAmount.text=@"";
    [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
    [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
    [addBtn setTitle:@"Add to Return" forState:UIControlStateNormal];
    
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    lblUOMCode.text=@"";
    
    NSDictionary * row ;
    if (bSearchIsOn)
    {
        row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }

    
    NSArray* productDetail=[[SWDatabaseManager retrieveManager]fetchProductDetails:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"] UOMCode:[row stringForKey:@"Primary_UOM_Code"]];
    
    
    mainProductDict=nil;
    mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[productDetail objectAtIndex:0]];
    
     [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"Inventory_Item_ID"] :[customerDict stringForKey:@"Price_List_ID"]] withUOM:@""];
    
    [bonusBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    [bonusBtn setSelected:YES];
    
    
    
    
    
    NSMutableArray* conversionArray=[self fetchConversionFactorforItemCode:[mainProductDict stringForKey:@"Item_Code"] withItemUOM:[mainProductDict stringForKey:@"UOM"]];
    NSLog(@"fetched conversion factor %@", [[conversionArray valueForKey:@"Conversion"]objectAtIndex:0]);
    
    NSInteger conversionVal=[[[conversionArray valueForKey:@"Conversion"]objectAtIndex:0] integerValue];
    
    if (conversionVal>0) {
        uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ", (long)conversionVal]stringByAppendingString:@"EA"];
    }
}

#pragma mark conversion factor method

-(NSMutableArray*)fetchConversionFactorforItemCode:(NSString*)itemCode withItemUOM:(NSString*)itemUOM

{
    
    NSMutableArray* conversionFactorArry=[SWDatabaseManager fetchConversionFactorforUOMitemCode:itemCode ItemUOM:itemUOM];
    
    return conversionFactorArry;
    
    
}


//test method


- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail {
    if([priceDetail count]!=0)
    {
        

        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else
        {
            [self showPlaceHolderwithMessage:@"No prices available for selected product."];
        }
    }
    else
    {
        [self showPlaceHolderwithMessage:@"No prices available for selected product."];
    }
    
    NSLog(@"check unit price here %@%@", [mainProductDict valueForKey:@"Net_Price"], [mainProductDict valueForKey:@"List_Price"]);
    
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblWPAmount setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRPAmount setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
}


/***duplicate*/
- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail withUOM:(NSString *)uomString{
    NSMutableArray *tempPriceDetailArray=[[NSMutableArray alloc]init];
    
    NSString *defaultUom=[[NSString alloc]initWithFormat:@"%@",[mainProductDict valueForKey:@"UOM"]];
     if([priceDetail count]!=0)
    {
        if([uomString isEqualToString:@""])
        {
            uomString=defaultUom;
            lblUOMCode.text=[ NSString stringWithFormat:@" %@",uomString];
            selectedUOMReference=uomString;
        }
            for (NSInteger i=0; i<priceDetail.count; i++) {
            
                if([[[priceDetail objectAtIndex:i]stringForKey:@"Item_UOM"]isEqualToString:uomString])
                {
                    [tempPriceDetailArray addObject:[priceDetail objectAtIndex:i]];
                }
                
            }
            priceDetail=tempPriceDetailArray;

        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else
        {
            [self showPlaceHolderwithMessage:@"No prices available for selected product."];
        }
    }
    else {
        [self showPlaceHolderwithMessage:@"No prices available for selected product."];
    }
    
    
    NSLog(@"check unit price here %@%@", [mainProductDict valueForKey:@"Net_Price"], [mainProductDict valueForKey:@"List_Price"]);
    
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblProductCode setText:[mainProductDict stringForKey:@"Item_Code"]];
    [lblWPAmount setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRPAmount setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
}


#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [candySearchBar setShowsCancelButton:YES animated:YES];
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }else {
        bSearchIsOn = YES;
        [self  searchContent];
    }
}

-(void)searchContent
{
    NSString *searchString = candySearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:filter, @"Description", searchString];
    //NSPredicate *predicate2 = [NSPredicate predicateWithFormat:filter, @"Description", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate1]];
    filteredCandyArray=[[productArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    NSLog(@"\n\n *** Total Search Results: %lu  ***\n\n",(unsigned long)filteredCandyArray.count);
    
    if (filteredCandyArray.count>0) {
        [productTableView reloadData];
        [productTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                 animated:YES
                           scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [productTableView.delegate tableView:productTableView didSelectRowAtIndexPath:firstIndexPath];
        
    }else if (filteredCandyArray.count==0)
    {
        bSearchIsOn=YES;
        [productTableView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
        //[self updateViewEditingStatus:NO];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar{
    
    NSLog(@"\n\n *** Search Button Clicked **\n\n");
    [self.view endEditing:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    NSLog(@"\n\n *** Search Bar Cancel Button Clicked **\n\n");
    [candySearchBar setText:@""];
    [candySearchBar setShowsCancelButton:NO animated:YES];
    filteredCandyArray=[[NSMutableArray alloc]init];
    bSearchIsOn=NO;
    if ([candySearchBar isFirstResponder]) {
        [candySearchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (finalProductArray.count>0) {
        [productTableView reloadData];
        //[self deselectPreviousCellandSelectFirstCell];
    }
    
}

- (void)updatePrice
{
    int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
    if (qty > 0)
    {
        double totalPrice = (double)qty * ([[mainProductDict objectForKey:@"Unit_Selling_Price"] doubleValue]);
        [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
    }
}
-(IBAction)resetAction:(id)sender
{
    lblProductName.text = @"Product Name";
    txtProductQty.text = @"";
    lblWPAmount.text = @"";
    lblRPAmount.text = @"";
    txtDefBonus.text =@"";
    txtLot.text = @"";
    uomConversionFactorLbl.text=@"";
    
    [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
    [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
    
    mainProductDict = nil;
    
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [lblUOMCode setText:@""];
    [uomConversionFactorLbl setText:@""];
    
    [addBtn setTitle:@"Add to Return" forState:UIControlStateNormal];
    
}
-(IBAction)returnTypebuttonAction:(id)sender
{
    
    
    UIButton* returnButton =(UIButton*)sender;
    
    
    
    
    [self.view endEditing:YES];
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
    else
    {
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithEXP];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(returnTypeChanged:)];
        
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        [collectionTypePopOver presentPopoverFromRect:returnButton.bounds inView:returnButton.imageView  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)expiryDatebuttonAction:(id)sender
{
    
    UIButton* expiryButton =(UIButton*)sender;
    
    [self.view endEditing:YES];
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
    else
    {
        SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(ExpdateChanged:)]  ;
        datePickerViewController.isRoute=NO;

        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        collectionTypePopOver.delegate=self;
        [collectionTypePopOver presentPopoverFromRect:expiryButton.bounds inView:expiryButton.imageView  permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)addbuttonAction:(id)sender
{
    if([self validateInput])
    {
        [self.view endEditing:YES];
        for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
            [productTableView deselectRowAtIndexPath:indexPath animated:NO];
        }
        isSaveOrder = YES;
        [mainProductDict setObject:selectedUOMReference forKey:@"SelectedUOM"];
        [oldSalesOrderVC productAdded:mainProductDict];
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtLot.text=@"";
        lblRPAmount.text=@"";
        lblWPAmount.text=@"";
        [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
        [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
        [addBtn setTitle:@"Add to Return" forState:UIControlStateNormal];
        [lblProductName setText:@"Product Name"];
         mainProductDict=nil;
        if (valuePopOverController)
        {
            [valuePopOverController dismissPopoverAnimated:YES];
            valuePopOverController = nil;
            valuePopOverController.delegate=nil;
        }
        lblUOMCode.text=@"";
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please fill all required fields" withController:self];
    }
}
-(IBAction)pullUpbuttonAction:(id)sender
{
    [self.view endEditing:YES];
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
    else
    {
        [bonusBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [bonusBtn setSelected:YES];
        isStockToggled=YES;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(264, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(239, 0, 30, 605) withDuration:0.5];
    }
}

-(IBAction)bonusButtonAction:(id)sender
{
    if (mainProductDict.count!=0)
    {
        UIButton* senderBtn = (UIButton*)sender;
        
        if([popoverController isPopoverVisible])
        {
            [popoverController dismissPopoverAnimated:YES];
            return;
        }
        
        ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
        
        UINavigationController *favNav = [[UINavigationController alloc]
                                          initWithRootViewController:bonusViewController];
        favNav.title=@"TEST";
        favNav.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"xyz" style:UIBarButtonItemStyleDone target:self action:@selector(closeSalesHistory)];
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:favNav];
        popoverController.popoverContentSize=CGSizeMake(800, 650);
        bonusViewController.popover=popoverController;
        [popoverController presentPopoverFromRect:senderBtn.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
}
- (void)returnTypeChanged:(NSDictionary *)newType
{
    [collectionTypePopOver dismissPopoverAnimated:YES];
    [mainProductDict setValue:[newType stringForKey:@"Description"] forKey:@"reason"];
    [mainProductDict setValue:[newType stringForKey:@"RMA_Lot_Type"] forKey:@"RMA_Lot_Type"];
    [btnReturnType setTitle:[mainProductDict stringForKey:@"reason"] forState:UIControlStateNormal];
    collectionTypePopOver.delegate=nil;
    collectionTypePopOver=nil;
}
- (void)ExpdateChanged:(SWDatePickerViewController *)sender {
    
    if ( sender.selectedDate == nil){
        NSLog(@"\n ** cancel button clicked **\n");
    }else {
        NSLog(@"\n ** done button clicked **\n");
        NSDateFormatter *formatter = [NSDateFormatter new]  ;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formatter setLocale:usLocale];
        [mainProductDict setValue:[formatter stringFromDate:sender.selectedDate] forKey:@"EXPDate"];
        [btnExpiryDate setTitle:[formatter stringFromDate:sender.selectedDate] forState:UIControlStateNormal];
    }
    
    [collectionTypePopOver dismissPopoverAnimated:YES];
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (mainProductDict.count==0)
    {
        [self showPlaceHolderwithMessage:@"Please select product."];
        return NO;
    }
    else{
        [self hidePlaceHolder];
    }
    return YES;
}

-(void)showPlaceHolderwithMessage:(NSString*)messageText
{
    placeHolderView.hidden=NO;
    statusLbl.text=messageText;
    placeHolderImageView.image=[UIImage imageNamed:@"no_product_select.png"];
    NSLog(@"place holder image is %f", placeHolderImageView.alpha);
}
-(void)hidePlaceHolder
{
    placeHolderView.hidden=YES;

}

-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    NSCharacterSet *unacceptedInput = nil;
    textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    
    if (textField==txtProductQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [mainProductDict setValue:textField.text forKey:@"Qty"];
        }
        else
        {
            if (!isErrorSelectProduct) {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Quantity value should have only numeric" withController:self];
            }
            isErrorSelectProduct=NO;
            [mainProductDict setValue:@"" forKey:@"Qty"];
        }
    }
    else if (textField==txtDefBonus)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [mainProductDict setValue:textField.text forKey:@"Bonus"] ;
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
            [mainProductDict setValue:@"" forKey:@"Bonus"];
        }
    }
    else if (textField==txtLot)
    {
        [mainProductDict setValue:textField.text forKey:@"lot"];
    }
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    txtDefBonus.text=[mainProductDict stringForKey:@"Bonus"];
    txtLot.text=[mainProductDict stringForKey:@"lot"];
    [self updatePrice];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    collectionTypePopOver.delegate=nil;
    
    collectionTypePopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}

- (BOOL)validateInput
{
    AppControl *appControl = [AppControl retrieveSingleton];
    if ([appControl.IS_LOT_OPTIONAL isEqualToString:@"Y"])
    {
        if (![mainProductDict objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
    }
    else
    {
        if (![mainProductDict objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"lot"] length] < 1)
        {
            return NO;
        }
        
    }
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/* uom selection*/
- (IBAction)UOMPickerButton:(id)sender {
    
    UIButton *btn  = (UIButton *)sender;
    outletPopOver = nil;
    valuePopOverController=nil;
    
    outletPopOver = [[OutletPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    //display values for pop over
    
    NSMutableArray* UOMList=[[SWDatabaseManager retrieveManager] fetchUOMforItem:[NSString stringWithFormat:@"%@",[mainProductDict stringForKey:@"Item_Code"]]];
    
    if (UOMList.count>0) {
   
    NSLog(@"get UOM %@", [UOMList valueForKey:@"Item_UOM"]);
    
    outletPopOver.colorNames = [UOMList valueForKey:@"Item_UOM"];// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
    outletPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:outletPopOver];
        [valuePopOverController presentPopoverFromRect:btn.bounds inView:btn.imageView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    
    }
}

-(void)selectedStringValue:(id)value

{
    selectedUOMReference=value;
    
    
    //update the conversion val
    
    
    NSMutableArray* fetchedConversionArray=[self fetchConversionFactorforItemCode:[mainProductDict stringForKey:@"Item_Code"] withItemUOM:value];
    NSLog(@"fetched values after uom changed %@", [fetchedConversionArray description]);
    
    if (fetchedConversionArray.count>0) {
        
        NSInteger conversionfactor=[[[fetchedConversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        
        uomConversionFactorLbl.text=[[NSString stringWithFormat:@"%ld ",(long)conversionfactor] stringByAppendingString:@"EA"];
        
    }
    
    
    
    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"Inventory_Item_ID"] :[customerDict stringForKey:@"Price_List_ID"]] withUOM:value];
//    [self dbGetSelectedProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProductwithUOM:[mainProductDict stringForKey:@"ItemID"] UOM:value :[customerDict stringForKey:@"Price_List_ID"]]];
//    [self updatePrice];
    lblUOMCode.text=[NSString stringWithFormat:@" %@",value];
//    [lblWholesalePrice setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
//    
//    if (txtfldWholesalePrice.isHidden==NO) {
//        
//        [txtfldWholesalePrice setText: [[mainProductDict stringForKey:@"Net_Price"] currencyString]];
//        
//    }
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
}

@end
