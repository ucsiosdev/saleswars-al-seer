//
//  TblCellReturnProduct.m
//  SalesWars
//
//  Created by Ravinder Kumar on 07/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import "TblCellReturnProduct.h"

@implementation TblCellReturnProduct

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
   
    [super setSelected:selected animated:animated];
    if (self.tag==1){
        self.viewProdcutStatus.backgroundColor = [UIColor redColor];
    }else {
        self.viewProdcutStatus.backgroundColor = [UIColor clearColor];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [super setHighlighted:highlighted animated:animated];
}


@end
