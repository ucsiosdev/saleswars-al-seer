//
//  AuditResponseDetail.m
//  DataSyncApp
//
//  Created by sapna on 1/22/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "AuditResponseDetail.h"

@implementation AuditResponseDetail
@synthesize Survey_ID =_Survey_ID;
@synthesize Question_ID =_Question_ID;
@synthesize Customer_ID =_Customer_ID;
@synthesize Audit_Survey_ID =_Audit_Survey_ID;
@synthesize SalesRep_ID =_SalesRep_ID;
@synthesize Response =_Response;
@synthesize Emp_Code =_Emp_Code;
@synthesize Survey_Timestamp =_Survey_Timestamp;
@synthesize Surveyed_By =_Surveyed_By;
-(id)initWithAudit_Survey_ID:(NSString*)Audit_Survey_ID Survey_ID:(int)Survey_ID Question_ID:(int)Question_ID Response:(NSString*)Response Customer_ID:(int)Customer_ID _Surveyed_By:(int)Surveyed_By SalesRep_ID:(int)SalesRep_ID Emp_Code:(NSString*)Emp_Code Survey_Timestamp:(NSString*)Survey_Timestamp;{
    
    if ((self = [super init])) {
        self.Survey_ID=Survey_ID;
        self.Customer_ID=Customer_ID;
        self.Audit_Survey_ID=Audit_Survey_ID;
        self.SalesRep_ID=SalesRep_ID;
        self.Question_ID=Question_ID;
        self.Response=Response;
        self.Emp_Code=Emp_Code;
        self.Survey_Timestamp=Survey_Timestamp;
        self.Surveyed_By =Surveyed_By;
        
      

    }
    return self;
    
}


@end
