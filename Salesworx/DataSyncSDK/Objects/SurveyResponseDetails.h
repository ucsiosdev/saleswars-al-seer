//
//  SurveyResponseDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SurveyResponseDetails : NSObject{

    int  _Response_ID ;
    int  _Response_Type_ID;
    int _Question_ID;
    NSString *_Response_Text;
}
@property(nonatomic,assign)int Response_ID;
@property(nonatomic,assign)int Response_Type_ID;
@property(nonatomic,assign)int Question_ID;
@property(nonatomic,copy)NSString * Response_Text;

-(id)initWithUniqueQuestionId :(int)Question_ID Response_ID:(int)Response_ID Response_Type_ID:(int)Response_Type_ID Response_Text:(NSString*) Response_Text;

@end
