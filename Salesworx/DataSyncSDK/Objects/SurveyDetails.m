//
//  SurveyDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyDetails.h"

@implementation SurveyDetails
@synthesize Survey_ID =_Survey_ID;
@synthesize Customer_ID=_Customer_ID;
@synthesize  Site_Use_ID=_Site_Use_ID;
@synthesize SalesRep_ID=_SalesRep_ID;
@synthesize Survey_Title=_Survey_Title;
@synthesize Start_Time=_Start_Time;
@synthesize End_Time =_End_Time;
@synthesize Survey_Type_Code=_Survey_Type_Code;

-(id)initWithSurveyId :(int)Survey_ID Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID SalesRep_ID:(int)SalesRep_ID Survey_Title:(NSString*)Survey_Title Start_Time:(NSString*)Start_Time End_Time:(NSString*)End_Time Survey_Type_Code:(NSString*)Survey_Type_Code
{
    if ((self = [super init])) {
        self.Survey_ID=Survey_ID;
        self.Customer_ID=Customer_ID;
        self.Site_Use_ID=Site_Use_ID;
        self.SalesRep_ID=SalesRep_ID;
        self.Survey_Title=Survey_Title;
        self.Start_Time=Start_Time;
        self.End_Time=End_Time;
        self.Survey_Type_Code=Survey_Type_Code;
    }
    return self;
}



@end
