//
//  Status.m
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/4/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "Status.h"

@implementation Status
@synthesize Status1;
@synthesize ProcessResponse;
@synthesize CurrentTimestamp;
@synthesize SyncReferenceNo;
@synthesize SyncStatus;
@synthesize BytesReceived;
@synthesize BytesSent;
@synthesize CurrentProgress;
- (id)init {
	
	if(self = [super init]) {
		self.Status1 = @"";
        self.ProcessResponse=@"";
        self.CurrentTimestamp=@"";
        self.SyncReferenceNo=@"";
        self.SyncStatus=@"";
        self.BytesReceived=@"";
        self.BytesSent=@"";
        self.CurrentProgress=@"";
 
	}
	
	return self;
}

@end
