//
//  CustomerResponseDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/21/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerResponseDetails : NSObject
{
    int _Survey_ID;
    int _Question_ID;
    int _Customer_ID;
    int _Site_Use_ID;
    int _SalesRep_ID;
    NSString *_Response;
    NSString *_Emp_Code;
    NSString *_Survey_Timestamp;
    NSString *_Customer_Survey_ID;
}
@property(nonatomic,assign)int Survey_ID;
@property(nonatomic,assign)int Question_ID;
@property(nonatomic,assign)int Customer_ID;
@property(nonatomic,assign)int Site_Use_ID;
@property(nonatomic,assign)int SalesRep_ID;
@property(nonatomic,copy)NSString * Response;
@property(nonatomic,copy)NSString * Emp_Code;
@property(nonatomic,copy)NSString * Survey_Timestamp;
@property(nonatomic,copy)NSString * Customer_Survey_ID;

-(id)initWithCustomer_Survey_ID:(NSString*)Customer_Survey_ID Survey_ID:(int)Survey_ID Question_ID:(int)Question_ID Response:(NSString*)Response Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID SalesRep_ID:(int)SalesRep_ID Emp_Code:(NSString*)Emp_Code Survey_Timestamp:(NSString*)Survey_Timestamp;
@end
