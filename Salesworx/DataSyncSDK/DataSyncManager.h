//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "AppConstantsList.h"
#import "SurveyDetails.h"
@interface DataSyncManager : NSObject<MBProgressHUDDelegate>
{
     MBProgressHUD *progressHUD;
     NSString *strDeviceId;
    
    // Error Code
    SyncErrorCode errorCode;
    NSString  *errMessage;
    NSMutableDictionary *statusDict;
    NSMutableArray *execArray;
    
    SurveyDetails *surveyDetails;
    NSMutableArray *customerResponseArray;
    NSString *SurveyType;
    int currentPage;
    int MaxPages;
    int isDefaultValueUsed;
    int key;
    int alertMessageShown;
    int isMultiResponseTable;
    BOOL getResponseForAllQuestions;

}
@property(assign)SyncErrorCode errorCode;
@property(nonatomic,strong)NSMutableArray *execArray;
@property(nonatomic,strong) NSMutableDictionary *statusDict;
@property(nonatomic,strong)NSString  *errMessage;
@property(nonatomic,strong)NSString *strDeviceId;

@property(nonatomic,strong)NSMutableArray *customerResponseArray;
@property(nonatomic,strong)NSString *SurveyType;
@property(nonatomic,readwrite)int currentPage;
@property(nonatomic,readwrite)int MaxPages;
@property(nonatomic,readwrite)int isDefaultValueUsed;
@property(nonatomic,strong)SurveyDetails *surveyDetails;
@property(nonatomic,readwrite)int key;
@property(nonatomic,readwrite)int alertMessageShown;
@property(nonatomic,readwrite)int isMultiResponseTable;
@property(assign) BOOL getResponseForAllQuestions;
@property(nonatomic, strong)MBProgressHUD *progressHUD;


+(DataSyncManager *)sharedManager;

-(NSString*)getDeviceID;
-(NSString*) sha1:(NSString*)input;
- (NSString*) dbGetDatabasePath;
- (NSString*) dbGetZipDatabasePath;
- (NSArray*) dbGetSignatureImageFilePath;

-(void)reachabilityNotificationMethod;
- (void)showCustomLoadingIndicator:(UIView*)loadingView;
- (void)hideCustomIndicator;
-(void)UserAlert:(NSString *)Message;
-(void)deleteDatabaseFromApp;
- (NSArray*) dbGetDistributionImageFilePath ;
-(NSArray*)getVisitImagesFilePath;


@end
