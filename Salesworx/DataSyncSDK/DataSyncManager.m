//
//  DataSyncManager.m
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "DataSyncManager.h"
#import "ReachabilitySync.h"
#include <CommonCrypto/CommonDigest.h>
#import <Foundation/NSString.h>
#import "NSString+MD5Addition.h"
#import "UIDevice+IdentifierAddition.h"
#import "SWPlatform.h"
#import "SSKeychain.h"


@implementation DataSyncManager

@synthesize strDeviceId;
@synthesize errMessage,progressHUD;
@synthesize errorCode;
@synthesize statusDict;
@synthesize execArray;
@synthesize customerResponseArray;
@synthesize currentPage;
@synthesize MaxPages;
@synthesize isDefaultValueUsed;
@synthesize surveyDetails;
@synthesize key;
@synthesize alertMessageShown;
@synthesize SurveyType;
@synthesize getResponseForAllQuestions;
@synthesize isMultiResponseTable;
#pragma mark
#pragma mark - Singleton Method
static DataSyncManager *sharedSingleton=nil;

+(DataSyncManager *)sharedManager{
    
    @synchronized(self){
        
        if (!sharedSingleton){
            sharedSingleton = [[DataSyncManager alloc] init];
            [sharedSingleton initialize];
        }
        
        return sharedSingleton;
    }
}
- (void)initialize {
    
	if(!statusDict)
    {
        [statusDict removeAllObjects];
        statusDict=nil;
        statusDict = [NSMutableDictionary dictionary];
    }
    if(!self.customerResponseArray)
    {
        [self.customerResponseArray removeAllObjects];
        self.customerResponseArray=nil;
        self.customerResponseArray = [NSMutableArray array];

    }
	
}

- (NSArray*) dbGetDistributionImageFilePath {
    
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]objectAtIndex:0];
        
        path= [NSString stringWithFormat:@"%@/%@", [filePath path],@"Distribution check images"];
        
    }
    
    else
    {
        path = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Distribution check images"];
    }

    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    NSEnumerator *enumerator = [dirContents objectEnumerator];
    id fileName;
    NSMutableArray *fileArray = [NSMutableArray array];
    while (fileName = [enumerator nextObject])
    {
        NSString *fullFilePath = [path stringByAppendingPathComponent:fileName];
        NSRange textRangeJpg = [[fileName lowercaseString] rangeOfString:[@".jpg" lowercaseString]];
        
        if (textRangeJpg.location != NSNotFound)
        {
            
            [fileArray addObject:fullFilePath];
        }
    }
    return fileArray;
}

-(NSArray*)getVisitImagesFilePath
{
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]objectAtIndex:0];
        
        path= [NSString stringWithFormat:@"%@/%@", [filePath path],@"Visit Images"];
        
    }
    
    else
    {
        path = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Visit Images"];
    }
    
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    NSEnumerator *enumerator = [dirContents objectEnumerator];
    id fileName;
    NSMutableArray *fileArray = [NSMutableArray array];
    while (fileName = [enumerator nextObject])
    {
        NSString *fullFilePath = [path stringByAppendingPathComponent:fileName];
        NSRange textRangeJpg = [[fileName lowercaseString] rangeOfString:[@".jpg" lowercaseString]];
        
        if (textRangeJpg.location != NSNotFound)
        {
            
            [fileArray addObject:fullFilePath];
        }
    }
    return fileArray;
}

-(NSString*)getDeviceID
{
    //Use the bundle name as the App identifier. No need to get the localized version.

    NSString *Appname = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];

    //Check if we have UUID already

    NSString *retrieveuuid = [SSKeychain passwordForService:Appname account:@"user"];
    
    if (retrieveuuid == NULL)
    {
        //Create new key for this app/device

        CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);

        retrieveuuid = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);

        CFRelease(newUniqueId);
        
        //Save key to Keychain
        [SSKeychain setPassword:retrieveuuid forService:Appname account:@"user"];
        
        NSLog(@"####### NEW GENERATED ID: %@",retrieveuuid);
        return retrieveuuid;
    }
    
    NSLog(@"####### EXISTING ID: %@",retrieveuuid);
    return retrieveuuid;
}

#pragma mark
#pragma mark SH1 hashed method
-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

#pragma mark -
#pragma mark - Reachability Methods
-(void)reachabilityNotificationMethod{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    ReachabilitySync * reach = [ReachabilitySync reachabilityWithHostname:@"www.google.com"];
    
    reach.reachableBlock = ^(ReachabilitySync * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           
        });
    };
    
    reach.unreachableBlock = ^(ReachabilitySync * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           
        });
    };
    [reach startNotifier];
}

-(void)reachabilityChanged:(NSNotification*)note
{
    ReachabilitySync * reach = [note object];
    
    if([reach isReachable])
    {
        errorCode=jSuccess;
    }
    else
    {
        errorCode=jNetworkError;
        [[NSNotificationCenter defaultCenter] removeObserver:kReachabilityChangedNotification];

        [reach stopNotifier];
       
        return;
        
    }
}
#pragma mark
#pragma mark - GetDatbasePath Method
- (NSString*) dbGetDatabasePath
{    
    NSString *fileName = databaseName;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    

    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath])
    {
        return @"Database does not exist";
               
    }
    else
    {
        return [documentsDirectory stringByAppendingPathComponent:databaseName] ;

    }
}

- (NSString*) dbGetZipDatabasePath
{
    NSString *fileName = @"swx.sqlite.gz";
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    

    
    
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath])
    {
        return @"Database does not exist";
        
    }
    else
    {
        return [documentsDirectory stringByAppendingPathComponent:@"swx.sqlite.gz"] ;
        
    }
}


#pragma mark
#pragma mark - GetSignatureImagePath Method
- (NSArray*) dbGetSignatureImageFilePath {
    
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]objectAtIndex:0];
        
        path= [NSString stringWithFormat:@"%@/%@", [filePath path],@"Signature"];
    
    }
    
    else
    {
         path = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Signature"];
    }
    
    

    
   
    
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    NSEnumerator *enumerator = [dirContents objectEnumerator];
    id fileName;
    NSMutableArray *fileArray = [NSMutableArray array];
    while (fileName = [enumerator nextObject])
    {
        NSString *fullFilePath = [path stringByAppendingPathComponent:fileName];
        NSRange textRangeJpg = [[fileName lowercaseString] rangeOfString:[@".jpg" lowercaseString]];
        
        if (textRangeJpg.location != NSNotFound)
        {
            
            [fileArray addObject:fullFilePath];
        }
    }
    return fileArray;
}
-(void)deleteDatabaseFromApp
{
    NSString *fileName = databaseName;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //iOS 8 support
    NSString *documentsDirectory;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentsDirectory=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentsDirectory = [paths objectAtIndex:0];
    }
    
    

    
    
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentDBFolderPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    if (![fileManager fileExistsAtPath:documentDBFolderPath])
    {
       
        
    }
    else
        [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:fileName] error:NULL];
}

#pragma mark
#pragma mark - Indicator Method
- (void)showCustomLoadingIndicator:(UIView*)loadingView{
	
    if(self.progressHUD){
        [self.progressHUD removeFromSuperview];
    }
    self.progressHUD=nil;
    self.progressHUD = [[MBProgressHUD alloc] initWithView:loadingView];
	[loadingView addSubview:self.progressHUD];
	self.progressHUD.delegate = self;
    self.progressHUD.labelText=@"Loading";
    
	[self.progressHUD show:YES];
}
- (void)hideCustomIndicator{
	[progressHUD hide:YES];
}

#pragma
#pragma mark - Create Alert method
-(void)UserAlert:(NSString *)Message
{
    if(Message.length!=0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Error", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
        [ErrorAlert show];
    }
}


@end
