 
//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import "RequestResponseManager.h"
#import "DataSyncManager.h"
#import "ParseManager.h"
#import "JSON.h"

@implementation RequestResponseManager

@synthesize requestType;
@synthesize receivedString;
static RequestResponseManager *sharedSingleton=nil;

+ (RequestResponseManager*)sharedInstance {
	
    @synchronized(self){
        
        if (!sharedSingleton){
            sharedSingleton = [[RequestResponseManager alloc] init];
            [sharedSingleton initialize];
        }
        
        return sharedSingleton;
    }
}

- (void)initialize {
	delegateArray = [NSMutableArray array];
	extraInfoObjectArray = [NSMutableArray array];
}

- (void)setRequestPropery:(id<RequestManagerDelegate>)delegate ExtraInfo:(NSObject *)infoObject {
	if(delegate) {
		[delegateArray addObject:delegate];
	}
	if(infoObject) {
		[extraInfoObjectArray addObject:infoObject];
	}
}

- (void)removeRequestProperty:(id<RequestManagerDelegate>)delegate ExtraInfo:(NSObject *)infoObject {
	if(delegate) {
		[delegateArray removeObject:delegate];
	}
	if(infoObject) {
		[extraInfoObjectArray removeObject:infoObject];
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////  response parsing methods /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
-(NSMutableArray *)requestForEXEC:(NSString*)receivedstring
{
   // if(![receivedstring length]==0)
       return  [ParseManager parseExec:receivedstring];
}

@end
