
//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface ParseManager : NSObject {
    NSMutableDictionary *jsonResponse;

}

+(bool)parseActivate:(NSString *)jsonContent;
+(bool)parseUploadData:(NSString *)jsonContent;
+(bool)parseDownload:(NSString *)jsonContent;
+(bool)parseUploadFile:(NSString *)jsonContent;
+(NSMutableArray *)parseExec:(NSString *)jsonContent;
+(bool)parseActivateComplete:(NSString *)jsonContent;
@end
