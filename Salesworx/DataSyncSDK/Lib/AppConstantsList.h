//
//  DataSyncManager.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import <Foundation/Foundation.h>

//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////  Constants /////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////



#define databaseName @"swx.sqlite"
#define SignatureImageFile @"sync.jpg"

#define ResponseFormat @"JSON"
//#define Username  @"ahmed"
//#define Password  @"password"
//#define ClientVersion @"1" //application version
#define SyncType @"Activation"
#define SyncLocation @"location"
#define ProcName @"sync_CUSTOM_TestProc"
#define ProcParams @"location"
#define procValues @"location"
#define FileType @"SIGNATURE"

// Survey

#define CustomerID 2999
#define SiteUseID 1001
#define SalesRepID 14
#define EmpCode @"121"
#define SurveyedBy 1
//#define SurveyType @"N"

typedef enum {
	jErroronServer = 0,
	jSuccess,
    jNetworkError,
    kAlertMsgTimeoutError,
    kAlertMsgConnectionFailError,
    jApplicationResponseError
} SyncErrorCode;


typedef enum {
	jActivateQuery=0,
    jUploadDataQuery,
    jStatusQuery,
    jInitiateQuery,
    jDownloadQuery,
    jUploadFileQuery,
    jCompleteQuery,
    jExecQuery
    
} HTTPRequest;


typedef enum{
    databaseExistAlert=0,
    requestFailureAlert,
}
AlertType;

