//
//  AddFeedbackCommentViewController.h
//  SalesWars
//
//  Created by Neha Gupta on 2/5/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddFeedbackCommentDelegate <NSObject>
-(void)selectFeedback:(NSString*)comment;
@end

@interface AddFeedbackCommentViewController : UIViewController
{
    IBOutlet UITextView *commentTextView;
    id delegate;
}
@property(nonatomic) id AddFeedbackCommentDelegate;

@end

