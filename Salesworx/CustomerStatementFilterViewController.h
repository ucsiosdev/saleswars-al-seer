//
//  CustomerStatementFilterViewController.h
//  SalesWars
//
//  Created by Prasann on 15/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "CustomersListViewController.h"
#import "NSString+Additions.h"

#define kCustomerNameTextField @"Customer_Name"
#define kFromDateTextField @"From_Date"
#define kToDateTextField @"To_Date"
#define kCustomerNameTitle @"Customer Name"

@protocol CustomerStatementFilterDelegate <NSObject>
-(void)filteredCustomerStatement:(id)filteredContent;
-(void)filterParametersCustomerStatement:(id)filterParameter;
-(void)customerStatementFilterDidReset;

@end

@interface CustomerStatementFilterViewController : UIViewController{
    NSMutableDictionary *filterParametersDict;
    NSString *titleText;
    NSString* selectedTextField;
    NSString* selectedPredicateString;
    CustomersListViewController *currencyTypeViewController;
    UIPopoverController *currencyTypePopOver;
    UIPopoverController *datePickerPopOverController;
}
@property (strong, nonatomic) IBOutlet MedRepTextField *customerNameTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *toDateTextField;
@property(strong,nonatomic) NSMutableDictionary *customerDictionary;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) NSString *selectedTextFieldType;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong, nonatomic) NSMutableArray *customerStatementArray;
@end

