//
//  ManageOrderViewController.h
//  SWCustomer
//
//  Created by msaad on 12/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
#import "SWPlatform.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface ManageOrderViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>
{
    NSMutableArray *performaOrderArray;
    NSMutableArray *confirmOrderArray;
    NSMutableArray *totalOrderArray;
    
    IBOutlet MedRepElementDescriptionLabel *customerNameLabel;
    IBOutlet MedRepElementTitleLabel *customerNumberLabel;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;
    IBOutlet UITableView *manageOrderTableView;    
}

@property(strong,nonatomic) NSMutableDictionary * customerDictornary;


@end
