//
//  SettingViewController.m
//  SWPlatform
//
//  Created by msaad on 3/13/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

//
//  NewActivationViewController.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SettingViewController.h"
#import "SWFoundation.h"
#import "SeverEditLocation.h"
#import "SWPlatform.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "MedRepDefaults.h"

@interface SettingViewController ()
@end

@implementation SettingViewController

#define kSNameField 3
#define kSLinkField 4


@synthesize  action;
@synthesize  target;


- (id)init {
    self = [super init];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Settings", nil)
];
        
    }
    
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    //[Singleton getFreeMemory];
        
    
    loadingView=[[SWLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    
    serverDict = [NSMutableDictionary dictionary] ;
    editServerDict = [NSMutableDictionary dictionary ] ;
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer] ;
    NSError *error;
    
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]] ;
    
    
    serverDict=[NSMutableDictionary dictionaryWithDictionary:testServerDict];
    
    
   tableView=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped] ;
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    tableView.backgroundView = nil;
    tableView.backgroundColor = [UIColor whiteColor];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [self.view addSubview:tableView];
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
}

- (void)showActivate
{
    if ([changeMadeFrom isEqualToString:@"edit"])
    {
        CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
        CJSONSerializer *jsonSerializer = [CJSONSerializer serializer];
        
        NSError *error;
        NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ] ;
        for(int i=0 ; i<[testServerArray count] ; i++ )
        {
            NSDictionary *row = [testServerArray objectAtIndex:i];
            
            if([[row stringForKey:@"serverName"] isEqualToString:[serverDict stringForKey:@"serverName"]])
            {
                
                if(editServerName1.length > 0)
                {
                    [serverDict setValue:editServerName1 forKey:@"serverName"];
                }
                
                if(editServerLink.length > 0)
                {
                    [serverDict setValue:editServerLink forKey:@"serverLink"];
                }
                
                [testServerArray removeObjectAtIndex:i];
                
                [testServerArray addObject:serverDict];
                NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
                NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding]  ;
                [SWDefaults setSelectedServerDictionary:stringDataDict];
                
                break;
            }
            
        }
        
        NSData *datas = [jsonSerializer serializeObject:testServerArray error:&error];
        NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding] ;
        [SWDefaults setServerArray:stringData];
    }
    else
    {
        CJSONSerializer *jsonSerializer = [CJSONSerializer serializer];
        NSError *error;
        NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
        NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding]  ;
        [SWDefaults setSelectedServerDictionary:stringDataDict];
    }
    
    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(@"Settings Saved Successfully", nil) withController:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Report", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
}

- (void)serverChanged:(NSMutableDictionary *)d {
    serverDict=nil;
    serverDict = [NSMutableDictionary dictionaryWithDictionary:d  ];
    //[self setServerDict:[d mutableCopy]];
    
    changeMadeFrom = @"change";
    [tableView reloadData];
    [popoverController dismissPopoverAnimated:YES];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];

    
}

#pragma mark UIView delegate


-(void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem* infoBtn=[[UIBarButtonItem alloc]initWithTitle:@"Report" style:UIBarButtonItemStylePlain target:self action:@selector(attachLogFile)];
    
    self.navigationItem.rightBarButtonItem=infoBtn ;
    
}

-(void)attachLogFile
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [[mailController navigationBar] setTintColor: [UIColor blackColor]]; //color
        [mailController setMessageBody:@"Hi,\n Please find the attached device logs for SalesWars." isHTML:NO];
        [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
        [mailController setSubject:@"SalesWars Device Logs"];
        
        
        NSError * error;
        NSMutableArray *totalFilesArray = [[[NSFileManager defaultManager]
                                            contentsOfDirectoryAtPath:[SWDefaults fetchDeviceLogsFolderPath] error:&error] mutableCopy];
        
        for (NSInteger i=0; i<totalFilesArray.count; i++) {
            NSData *noteData = [NSData dataWithContentsOfFile:[[SWDefaults fetchDeviceLogsFolderPath] stringByAppendingPathComponent: [totalFilesArray objectAtIndex:i]]];
            
            NSData *compressedData = [noteData gzipDeflate];
            NSString *fileName = [[totalFilesArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@".txt" withString:@".zip"];
            [mailController addAttachmentData:compressedData mimeType:@"application/zip" fileName:fileName];
        }
        [self presentViewController:mailController animated:YES completion:nil];
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Failure" andMessage:@"Your device doesn't support the mail composer sheet. Please configure mail box in your device" withController:self];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    

    
    
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    editServerName1=nil;
    editServerLink = nil;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [self.navigationItem setRightBarButtonItem:nil ];
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
        return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   
        if (section==1) {
            return 1;
            
        }
        else{
            return 2;
            
        }
  
    
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @autoreleasepool {
        NSString *IDENT = @"SessionCellIdent";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:IDENT] ;
        if(cell)
        {
            cell=nil;
        }
        cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:IDENT] ;
        if (indexPath.section == 0)
        {
            if (indexPath.row == 0)
            {
                ((SWTextFieldCell *)cell).textField.placeholder = @"server name";
                ((SWTextFieldCell *)cell).textField.delegate=self;
                ((SWTextFieldCell *)cell).textField.tag=kSNameField;
                ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverName"];
            }
            else
            {
                ((SWTextFieldCell *)cell).textField.placeholder = @"server link";
                ((SWTextFieldCell *)cell).textField.delegate=self;
                ((SWTextFieldCell *)cell).textField.tag=kSLinkField;
                ((SWTextFieldCell *)cell).textField.text=[serverDict stringForKey:@"serverLink"];
            }
        }
        else
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENT] ;
            [cell.textLabel setText: NSLocalizedString(@"Server locations", nil)];
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        }
        return cell;
    }
}

- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if(section == 0)
    {
        return NSLocalizedString(@"Server", nil);
    }
    else
    {
        return nil;
    }
}

#pragma mark UITableViewDataSource
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.view endEditing:YES];
    
    if (indexPath.section == 0  ) {
        return;
    }
    else if (indexPath.section==1)
    {
        SeverEditLocation *serverSelectionViewController = [[SeverEditLocation alloc] init] ;
        [serverSelectionViewController setTarget:self];
        [serverSelectionViewController setSelectionChanged :@selector(serverChanged:)];
        [serverSelectionViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:serverSelectionViewController]  ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
        popoverController.delegate=self;
        UITableViewCell *cell = [tv cellForRowAtIndexPath:indexPath];
        [popoverController setDelegate:self];
        [popoverController presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

#pragma mark UIPopOver Delegates
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC {
    popoverController=nil;
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
    
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self.navigationItem setRightBarButtonItem:nil ];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField.tag == kSNameField) {
        editServerName1=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverName"];
        
    }
    else if (textField.tag == kSLinkField) {
        editServerLink=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverLink"];
        
    }
    changeMadeFrom = @"edit";
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


@end
