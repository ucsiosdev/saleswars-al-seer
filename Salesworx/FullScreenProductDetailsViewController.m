//
//  FullScreenProductDetailsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/14/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "FullScreenProductDetailsViewController.h"
#import "DataType.h"
#import "SWDefaults.h"

@interface FullScreenProductDetailsViewController ()

@end

@implementation FullScreenProductDetailsViewController
@synthesize imagesArray,pageController,productImagesScrollView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)viewWillAppear:(BOOL)animated
{
    NSInteger numberOfPages=imagesArray.count;
    
    productImagesScrollView.delegate=self;
    productImagesScrollView.backgroundColor=[UIColor whiteColor];
    productImagesScrollView.pagingEnabled = YES;
    productImagesScrollView.contentSize = CGSizeMake(numberOfPages * productImagesScrollView.frame.size.width, productImagesScrollView.frame.size.height);
    
    for (NSInteger i = 0; i < numberOfPages; i++) {
        UIImageView *tmpImg = [[UIImageView alloc] initWithFrame:CGRectMake(i * productImagesScrollView.frame.size.width + 20,
                                                                            20,
                                                                            productImagesScrollView.frame.size.width - 40,
                                                                            768)];
        tmpImg.contentMode=UIViewContentModeScaleAspectFit;
        
        if (imagesArray.count>0) {
            MediaFile* mediaFile=[imagesArray objectAtIndex:i];
            // Get dir
            NSString *documentsDirectory = nil;
            
            //iOS 8 support
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            
            NSString* imagePath=[documentsDirectory stringByAppendingPathComponent:mediaFile.Filename];
            NSLog(@"image path is %@", imagePath);
            tmpImg.image = [UIImage imageWithContentsOfFile:imagePath];
            [productImagesScrollView addSubview:tmpImg];
        }
        [productImagesScrollView setUserInteractionEnabled:YES];
        [productImagesScrollView setMultipleTouchEnabled:YES];
        
        pageController.numberOfPages=imagesArray.count;
        pageController.currentPageIndicatorTintColor=[UIColor colorWithRed:(255/255.0) green:(201/255.0) blue:(7/255.0) alpha:1];
        pageController.pageIndicatorTintColor=[UIColor colorWithRed:(0/255.0) green:(111/255.0) blue:(60/255.0) alpha:1];
        pageController.currentPage=0;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView==productImagesScrollView) {
        CGFloat pageWidth = productImagesScrollView.frame.size.width;
        float fractionalPage = productImagesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageController.currentPage = page;
    }
    else
    {
        CGFloat pageWidth = productImagesScrollView.frame.size.width;
        float fractionalPage = productImagesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        pageController.currentPage = page;
    }
}


- (IBAction)btnCloseTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tapGestureTapped:(id)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
