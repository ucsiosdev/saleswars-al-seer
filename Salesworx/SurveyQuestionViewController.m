//
//  SurveyQuestionViewController.m
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SurveyQuestionViewController.h"
#import "MCQTypeQuestionView.h"
#import "TextTypeQuestionView.h"
#import "RatingTypeViewController.h"
@interface SurveyQuestionViewController ()

@end

@implementation SurveyQuestionViewController

@synthesize customer;
@synthesize pageControl;
@synthesize detailScroller;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(Add:)]  animated:YES];

    
    [self setPageControl:[[UIPageControl alloc] initWithFrame:CGRectZero]];
    [self.pageControl setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
    
    [self.pageControl setNumberOfPages:3];
    [self.pageControl setCurrentPage:0];
    
    self.pageControl.tintColor = [UIColor redColor];
    
    [self setDetailScroller:[[UIScrollView alloc] initWithFrame:self.view.bounds]];
    [self.detailScroller setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.detailScroller setPagingEnabled:YES];
    [self.detailScroller setDelegate:self];
    //[self.detailScroller setBackgroundColor:[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f]];
    
    [self.view addSubview:self.detailScroller];
    [self.view addSubview:self.pageControl];
	// Do any additional setup after loading the view.
    
    MCQTypeQuestionView *question1 = [[MCQTypeQuestionView alloc] init];
    [question1 setFrame:CGRectMake(0, 0, self.detailScroller.bounds.size.width, self.detailScroller.bounds.size.height)];
    [question1 setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.detailScroller addSubview:question1];
    
    TextTypeQuestionView *question2 = [[TextTypeQuestionView alloc] init];
    [question2 setFrame:CGRectMake(self.detailScroller.bounds.size.width , 0, self.detailScroller.bounds.size.width, self.detailScroller.bounds.size.height)];
    [question2 setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

    [self.detailScroller addSubview:question2];
    
    RatingTypeViewController *question3 = [[RatingTypeViewController alloc] init];
    [question3 setFrame:CGRectMake(self.detailScroller.bounds.size.width * 2, 0, self.detailScroller.bounds.size.width, self.detailScroller.bounds.size.height)];
    [question3 setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

    [self.detailScroller addSubview:question3];
    
    UIView *question4 = [[UIView alloc] init];
    question4.backgroundColor = [UIColor yellowColor];
    [question4 setFrame:CGRectMake(self.detailScroller.bounds.size.width * 3, 0, self.detailScroller.bounds.size.width, self.detailScroller.bounds.size.height)];
    [question4 setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self rearrangeViews];
    isQuestionDone = NO;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.detailScroller setContentSize:CGSizeMake(self.detailScroller.bounds.size.width * 3, self.detailScroller.bounds.size.height)];
    [self.pageControl setFrame:CGRectMake(self.view.bounds.size.width / 2 - 100, self.view.bounds.size.height - 30, 200, 20)];
}
- (void)setupToolbar {
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    NSString *amountString = @"Total Price : ";
    UIBarButtonItem *totalLabelButton = [UIBarButtonItem labelButtonWithTitle:amountString];
    [self setupToolbar:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton,flexibleSpace,  nil]];
}
-(void)Add:(id)sender
{
    // Push view controller
    isQuestionDone = YES;
}
- (void)rearrangeViews {
    int i = 0;
    CGRect scrollerRect = self.detailScroller.bounds;
    for (UIView *view in self.detailScroller.subviews)
    {
        CGRect frame = view.frame;
        frame.origin.x = i * scrollerRect.size.width;
        [view setFrame:frame];
        i++;
    }
    
    [self scrollToPage:self.pageControl.currentPage];
}
- (void)scrollToPage:(NSInteger)page {
    CGFloat pageWidth = self.detailScroller.frame.size.width;
    CGFloat pageHeight = self.detailScroller.frame.size.height;
    CGRect scrollTarget = CGRectMake(page * pageWidth, 0, pageWidth, pageHeight);
    [self.detailScroller scrollRectToVisible:scrollTarget animated:YES];
    [self.pageControl setCurrentPage:page];
}

#pragma mark UIScrollview Delegate


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if(isQuestionDone)
    {
        CGFloat pageWidth = self.detailScroller.frame.size.width;
        
        int page = ((int)scrollView.contentOffset.x + (int)pageWidth / 2) / (int)pageWidth;
        
        [self.pageControl setCurrentPage:page];
        
        [self setTitle:[NSString stringWithFormat:@"Question %ld " , (long)self.pageControl.currentPage ]];
        isQuestionDone=NO;
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please answer of the question." withController:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


@end
