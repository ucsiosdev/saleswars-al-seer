//
//  OrderAdditionalInfoViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/18/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ReturnAdditionalViewController.h"
#import "ReturnTypeViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "AlSeerSalesOrderPreviewViewController.h"
#define kImagesFolder @"Signature"


@interface ReturnAdditionalViewController ()

@end

@implementation ReturnAdditionalViewController
@synthesize signVc;



- (id)initWithOrder:(NSMutableDictionary *)so andCustomer:(NSDictionary *)customer
{
    self = [super init];
    
    if (self)
    {
        [self.view setBackgroundColor:[UIColor whiteColor]];
        
        [self.view setFrame:CGRectMake(0, 0, 1024, 768)];
        salesOrder=so;
        customerDict=customer;
        
        form=[NSMutableDictionary dictionary];
  
        
        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customerDict];
        [customerHeaderView setShouldHaveMargins:YES];
        [self.view addSubview:customerHeaderView];
        
        tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 60, 1024, 570) style:UITableViewStyleGrouped] ;
        [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];

        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundView = nil;
        tableView.backgroundColor = [UIColor whiteColor];
        if (@available(iOS 15.0, *)) {
            tableView.sectionHeaderTopPadding = 0;
        }
        [self.view addSubview:tableView];
        
        
        
        CGRect signatureSize=[ReturnAdditionalViewController iOS7StyleScreenBounds];
        
        
        signatureBaseView =[[UIView alloc] initWithFrame:CGRectMake(0, 10, signatureSize.size.width, signatureSize.size.height)] ;
        signatureBaseView.alpha=0.0;
        signatureBaseView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:signatureBaseView];
        
        UIImageView *signatureBGImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signature-pad_new.png" cache:NO]];
       signatureBGImage.frame = CGRectMake(50, 270, 400,300 );
       // [signatureBaseView addSubview:signatureBGImage];
        
        signatureController = [[JBSignatureController alloc] init];
        signatureController.delegate = self;
        signatureController.view.frame= signatureBaseView.bounds;//added since sign cancel/close did not work otherwise. OLA!
        [signatureBaseView addSubview:signatureController.view];
        isSignature = NO;
        isGoodCollectedString = @"NO";
        [form setValue:@"NO" forKey:@"goodCollected"];
        
        UIButton *saveButton = [[UIButton alloc] init] ;
        [saveButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [saveButton setFrame:CGRectMake(tableView.frame.size.width-160,tableView.frame.size.height +80, 120, 42)];
        [saveButton addTarget:self action:@selector(confirmOrder) forControlEvents:UIControlEventTouchUpInside];
        [saveButton bringSubviewToFront:self.view];
        [saveButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
        saveButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
        [self.view addSubview:saveButton];

  
    }
    
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
    return self;
}

+ (CGRect)iOS7StyleScreenBounds {
    CGRect bounds = [UIScreen mainScreen].bounds;
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)) {
        bounds.size = CGSizeMake(bounds.size.height, bounds.size.width);
    }
    return bounds;
}



-(void)viewDidAppear:(BOOL)animated
{
     [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
}

#pragma mark preview method

-(void)presentCustomPreviewViewController

{
    
    AlSeerSalesOrderPreviewViewController* previewVC=[[AlSeerSalesOrderPreviewViewController alloc]init];
    previewVC.salesOrderDict=salesOrder;
    previewVC.isReturn=YES;
    previewVC.delegate=self;
    
    
    // [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom];
    
    
    [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem *leftButtonClicked=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(presentCustomPreviewViewController)];
    
    [self.navigationItem setRightBarButtonItem:leftButtonClicked];

}
-(void)viewDidLoad
{

    
    NSLog(@"%f",tableView.frame.size.height);

}

- (void)setupToolbar {
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;

    UIBarButtonItem *confirmButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Confirm Return", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(confirmOrder)]  ;
    
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, flexibleSpace , confirmButton, nil]];
}
- (NSString*) dbGetImagesDocumentPath
{
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}


- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    NSLog(@"image path %@", path);
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

-(void)confirmOrder
{
    NSData* savedImageData=[SWDefaults fetchSignature];
    
    NSLog(@"check the saved image tyoe %@", savedImageData);
    
    UIImage *image ;
    
    if (savedImageData) {
        
        
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        image = [UIImage imageWithData:savedImageData scale:screenScale];
        
        
        // NSData *imageData = UIImagePNGRepresentation(savedImageData);
        [form setObject:savedImageData forKey:@"signature"];
        
    }
    

    
    if([self validateInput])
    {
        info=form;
        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrder objectForKey:@"info"]]  ;
        [infoOrder setValue:info forKey:@"extraInfo"];
        [infoOrder setValue:@"R" forKey:@"order_Status"];
        [salesOrder setValue:infoOrder forKey:@"info"];
        
       // NSString *orderRef = [self.salesOrderService saveReturnOrderWithCustomerInfo:self.customerDict andOrderInfo:self.salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        

        //salesSer.delegate=self;
        //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);

        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveReturnOrderWithCustomerInfo:customerDict andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        
        [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
    
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"RMA Document created successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=10;
        [alert show];
    }
    else {

        NSMutableString *message= [NSMutableString stringWithString:NSLocalizedString(@"Please fill all required fields", nil)];;
        isSignatureOptional = @"Y";
        if ([isSignatureOptional isEqualToString:@"Y"])
        {
            if ([form objectForKey:@"signature"])
            {
                if (![form objectForKey:@"name"])
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                else if ([[form objectForKey:@"name"] length] < 1)
                {
                     message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
            }
            
        }
        else{
            
            if (![form objectForKey:@"name"])
            {
                 message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            else if ([[form objectForKey:@"name"] length] < 1)
            {
                 message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            
            if (![form objectForKey:@"signature"])
            {
                 message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            else if ([[form objectForKey:@"signature"] length] < 1)
            {
                 message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            
        }

        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:message withController:self];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10)
    {
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        }
        else {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];

            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
        }
    }
}

#pragma mark UIView delegates

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableViews viewForHeaderInSection:(NSInteger)section {
  GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tableView.bounds.size.width text:@""]  ;
    
    if (section == 0) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Customer reference", nil)];
    } else if (section == 1) {
        [sectionHeader.titleLabel setText:[NSString stringWithFormat:@"* %@",NSLocalizedString(@"Return Type", nil)]];
    } else if (section == 2) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Invoice No", nil)];
    }else if (section == 3) {
        [sectionHeader.titleLabel setText:[NSString stringWithFormat:@"* %@",NSLocalizedString(@"Goods Collected", nil)]];
    }else if (section == 4) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Comments", nil)];
    } else if (section == 5) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Customer Signature", nil)];
    } else if (section == 6) {
        [sectionHeader.titleLabel setText:NSLocalizedString(@"Name", nil)];
    }
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    NSString *CellIdentifier = @"InfoCell";
    EditableCell *cell = nil;
    
    if (indexPath.section == 0)
    {
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]  ;
        }
        [cell setKey:@"reference"];
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
        [((SWTextFieldCell *)cell).textField setPlaceholder:@"reference"];
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell) setKey:@"reference"];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"reference"]];    }
    else if (indexPath.section == 1)
    {
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            cell.textLabel.text = returnTypeString;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    else if (indexPath.section == 2)
    {
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]  ;
        }
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
        [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Invoice No", nil)];
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell) setKey:@"invoice_no"];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"invoice_no"]];
    }
    else if (indexPath.section == 3)
    {
        cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
        cell.textLabel.text = isGoodCollectedString;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 4)
    {
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]  ;
        }
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
        [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Comments", nil)];
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell) setKey:@"comments"];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"comments"]];
    }
    else if (indexPath.section == 5)
    {
        if (!cell)
        {
            cell = [[EditableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]  ;
            cell.textLabel.text = NSLocalizedString(@"Tap here for Signature", nil);
            cell.textLabel.textColor = [UIColor lightGrayColor];
            if(isSignature)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
        }
        
    }
    else if (indexPath.section == 6)
    {
        if (!cell)
        {
            cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:CellIdentifier]  ;
        }
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeDefault];
        [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Name", nil)];
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell) setKey:@"name"];
        [((SWTextFieldCell *)cell).textField setText:[form stringForKey:@"name"]];
    }
    return cell;
}
}
- (void)tableView:(UITableView *)tableView3 didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==5) {
        [self.view endEditing:YES];

        tableView.scrollEnabled=NO;
        
//        [UIView beginAnimations:@"Fade-in" context:NULL];
//        [UIView setAnimationDuration:0.5];
//        signatureBaseView.alpha=1.0;
//        
//        [signatureBaseView addSubview:signatureController.view];//added - testing. OLA!
//        [self.view bringSubviewToFront:signatureBaseView];
//        
//        [UIView commitAnimations];

        
        signVc=[[SignatureViewController alloc]init];
        //
        signVc.delegate=self;
        
        
        [self presentPopupViewController:signVc animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];

        
        [tableView3 deselectRowAtIndexPath:indexPath animated:YES];
        
    }
   else  if (indexPath.section == 1)
    {
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithReturnType];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(collectionTypeChanged:)];
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        UITableViewCell *cell = [tableView3 cellForRowAtIndexPath:indexPath];
        [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
   else  if (indexPath.section == 3)
   {
       ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithGoods];
       [collectionTypeViewController setTarget:self];
       [collectionTypeViewController setAction:@selector(goodsCollected:)];
       collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
       collectionTypePopOver.delegate=self;
       UITableViewCell *cell = [tableView3 cellForRowAtIndexPath:indexPath];
       [collectionTypePopOver presentPopoverFromRect:cell.frame inView:tableView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
   }
}

- (void)goodsCollected:(NSString *)newType
{
    [collectionTypePopOver dismissPopoverAnimated:YES];
    isGoodCollectedString = newType;
    [form setValue:newType forKey:@"goodCollected"];
    [tableView beginUpdates];
    [self reloadRowAtIndex:0 andSection:3];
    [tableView endUpdates];
}
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (!scrollViewDelegateFreezed) {
        [self hideKeyboard];
    }
}
- (void)hideKeyboard
{
    int sections = [tableView numberOfSections];
    for (int i = 0; i < sections; i++) {
        for (int j = 0; j < [tableView numberOfRowsInSection:i]; j++)
        {
            [[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:j inSection:i]] resignResponder];
        }
    }
}
- (void)collectionTypeChanged:(NSDictionary *)newType
{
    [collectionTypePopOver dismissPopoverAnimated:YES];
    returnTypeString = [newType stringForKey:@"Description"];
    [form setValue:[newType stringForKey:@"Reason_Code"] forKey:@"returnType"];
    [tableView beginUpdates];
    [self reloadRowAtIndex:0 andSection:1];
    [tableView endUpdates];
}
#pragma mark EditCell Delegate
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key
{
    [form setObject:value forKey:key];
}

- (void)scrollTableViewToTextField:(NSIndexPath *)indexPath {
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [self performSelector:@selector(enableScrollViewDelegates:) withObject:nil afterDelay:0.4f];
}

- (void)enableScrollViewDelegates:(id)sender {
    scrollViewDelegateFreezed = NO;
}
- (void)editableCellDidStartUpdate:(EditableCell *)cell
{
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    scrollViewDelegateFreezed = YES;
    [self performSelector:@selector(scrollTableViewToTextField:) withObject:indexPath afterDelay:0.1f];
}

#pragma mark - *** JBSignatureControllerDelegate ***

-(void)signatureConfirmed:(UIImage *)signatureImage signatureController:(JBSignatureController *)sender
{
    if(signatureImage==nil)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Please provide us your signature.", nil) withController:self];
    }
    else
    {
        signatureSaveImage = [[UIImage alloc] init];
        signatureSaveImage = signatureImage;
        NSData *imageData = UIImagePNGRepresentation(signatureImage);
        [form setObject:imageData forKey:@"signature"];
        isSignature = YES;
   
        [UIView beginAnimations:@"Fade-in" context:NULL];
        [UIView setAnimationDuration:0.5];
        signatureBaseView.alpha=0.0;
        [self.view bringSubviewToFront:signatureBaseView];
        [UIView commitAnimations];
        [self reloadRowAtIndex:0 andSection:5];
        tableView.scrollEnabled=YES;
    }
}
-(void)hideTempView
{
    [UIView beginAnimations:@"Fade-in" context:NULL];
    [UIView setAnimationDuration:0.5];
    signatureBaseView.alpha=0.0;
    [self.view bringSubviewToFront:signatureBaseView];
    [UIView commitAnimations];
    tableView.scrollEnabled=YES;
}

-(void)signatureCancelled:(JBSignatureController *)sender
{
    tableView.scrollEnabled=YES;
    [UIView beginAnimations:@"Fade-in" context:NULL];
    [UIView setAnimationDuration:0.5];
    signatureBaseView.alpha=0.0;
    [self.view bringSubviewToFront:signatureBaseView];
    [UIView commitAnimations];
    
    
}

-(void)signatureCleared:(UIImage *)clearedSignatureImage signatureController:(JBSignatureController *)sender
{
    [form  removeObjectForKey:@"signature"];
    isSignature = NO;
    [self reloadRowAtIndex:0 andSection:5];
    [sender clearSignature];
    
}
#pragma mark UIKeyboard Notifications

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        [tableView setFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 550)];
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        [tableView setFrame:f];
    }];
}

- (BOOL)validateInput
{
    [self hideKeyboard];
    AppControl *appControl = [AppControl retrieveSingleton];

    isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
    //if ([[SWDefaults appControl]count]==0)//IS_SIGNATURE_OPTIONAL
    if ([isSignatureOptional isEqualToString:@"Y"])
    {
        if ([form objectForKey:@"signature"])
        {
            if (![form objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[form objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
    
    }
    else{
        
            if (![form objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[form objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
    
            if (![form objectForKey:@"signature"])
            {
                return NO;
            }
            else if ([[form objectForKey:@"signature"] length] < 1)
            {
                return NO;
            }
        
    }
    if (![form objectForKey:@"goodCollected"])
    {
        return NO;
    }
    else if ([[form objectForKey:@"goodCollected"] length] < 1)
    {
        return NO;
    }
    if (![form objectForKey:@"returnType"])
    {
        return NO;
    }
    else if ([[form objectForKey:@"returnType"] length] < 1)
    {
        return NO;
    }
    
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    collectionTypePopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)reloadRowAtIndex:(int)rowIndex andSection:(int)section{
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:rowIndex inSection:section]] withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
