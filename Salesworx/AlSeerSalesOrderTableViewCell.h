//
//  AlSeerSalesOrderTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerSalesOrderTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *productNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *quantityLbl;
@property (strong, nonatomic) IBOutlet UILabel *unitPriceLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalLbl;
@property (strong, nonatomic) IBOutlet UILabel *uomLbl;
@property (nonatomic, strong) UIColor *borderColor;


@end
