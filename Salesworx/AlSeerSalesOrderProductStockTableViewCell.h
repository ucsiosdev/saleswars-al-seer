//
//  AlSeerSalesOrderProductStockTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/8/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerSalesOrderProductStockTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *expiryLbl;
@property (strong, nonatomic) IBOutlet UILabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *qualityInspectionLbl;
@property (strong, nonatomic) IBOutlet UILabel *blockedStockLbl;

@end
