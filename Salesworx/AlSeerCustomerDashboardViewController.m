//
//  AlSeerCustomerDashboardViewController.m
//  Salesworx
//
//  Created by Unique Computer Systems on 3/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerCustomerDashboardViewController.h"
#import "SWDefaults.h"
#import "DataTableViewCell.h"
#import "ChartTableViewCell.h"
#import "NullFreeDictionary.h"
#import "GraphHeaderCustomerDashTableViewCell.h"
#import "SWCollectionViewController.h"
#import "SWAppDelegate.h"
#import "AlSeerOptionsPopOverTableViewController.h"
#import "CustomerOrderHistoryTableViewCell.h"
#import "AlSeerOrderHistoryPopupViewController.h"
#import "AlSeerDashboardChartTableViewCell.h"




#import "AlSeerCustomerwiseTargetAchievementCell.h"
#import "AlSeerTargetAchievementViewController.h"





@interface AlSeerCustomerDashboardViewController ()

@end

@implementation AlSeerCustomerDashboardViewController

@synthesize customerOrderHistoryCustomView,customerIdLbl,customerNameLbl,customerTypeLbl,custMapView,addressLbl,cityLbl,poBoxLbl,locationLbl,pdcDueLbl,overDueLbl,avblBalanceLbl,creditLimitLbl,overallTargetLbl,totalSalesLbl,totalReturnsLbl,balanceToGoLbl,achievementLbl,agencyLbl,targetValueCatLbl,salesValueCatLbl,prevdaySalesCatLbl,achievementCatLbl,customerPieChart,slices,sliceColorsForActivities,userCurrencyCode,customerDetailsDictionary,salesStatisticsTblView,monthBtgLbl,monthSalesLbl,monthTargetLbl,orderHistoryTblView, chartView, targetTableView,statusLabel,mapView,salesTragetMonthLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIBarButtonItem *displayActionBarButton  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Start_Visit"] style:UIBarButtonItemStylePlain target:self action:@selector(startVisitAction)];
    
    
    UIBarButtonItem *startSurveyBarButtonItem  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Survey_Icon"] style:UIBarButtonItemStylePlain target:self action:@selector(startSurveyAction)];
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:startSurveyBarButtonItem,displayActionBarButton, nil];
    
    slices=[[NSMutableArray alloc]initWithObjects:@"50",@"25",@"25", nil];
    [customerPieChart setDelegate:self];
    [customerPieChart setDataSource:self];
    [customerPieChart setPieCenter:CGPointMake(150, 110)];
    [customerPieChart setShowPercentage:YES];
    [customerPieChart setLabelColor:[UIColor blackColor]];
    customerPieChart.showPercentage=YES;
    
    
    sliceColorsForActivities=[NSArray arrayWithObjects:
                              [UIColor redColor],
                              [UIColor blackColor],[UIColor blueColor],nil];
    
    
    
    
    [customerPieChart reloadData];
    
    [custMapView setShowsUserLocation:YES];
    [self loadCustomerDetailsData];
    
    // ** Don't forget to add NSLocationWhenInUseUsageDescription in MyApp-Info.plist and give it a string
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    salesVsTargetDetailView.hidden = YES;
    statusLabel.layer.cornerRadius = 10.0;
    statusLabel.layer.masksToBounds = YES;
    statusLabel.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    targetTableView.tableFooterView = [UIView new];
    if ([[SWDatabaseManager isCustomerBlocked:[customerDetailsDictionary valueForKey:@"Customer_ID"]] isEqualToString:@"N"]) {
        statusLabel.hidden = NO;
        statusLabel.text = @"Blocked";
    }
    userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

    [self fetchLabelDatafromDB];
    [self targetDetailsLabels];
    
    CAShapeLayer *grapLayer = [CAShapeLayer layer];
    grapLayer.path = [UIBezierPath bezierPathWithRoundedRect: targetTableView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    targetTableView.layer.mask = grapLayer;
    
    CAShapeLayer *targetLayer = [CAShapeLayer layer];
    targetLayer.path = [UIBezierPath bezierPathWithRoundedRect: _graphTableViewCell.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    _graphTableViewCell.layer.mask = targetLayer;
    
    orderHistoryTblView.tableFooterView = [UIView new];
    targetDetailButton.alpha = 0.7;
    targetGraphButton.alpha = 1;
    
    custMapView.layer.cornerRadius = 8.0;
    custMapView.layer.masksToBounds = YES;
    custMapView.layer.borderWidth = 1.0;
    custMapView.layer.borderColor = [UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1.0].CGColor;
    [targetGraphButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:16.0]];
    [targetDetailButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:14.0]];
    [targetGraphButton setTitleColor:[UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    [targetDetailButton setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    
    NSDateFormatter *currentMonthFormatter=[[NSDateFormatter alloc]init];
    currentMonthFormatter.dateFormat=@"MMMM";
    NSString* currentMonth=[currentMonthFormatter stringFromDate:[NSDate date]];
    salesTragetMonthLabel.text = [NSString stringWithFormat:@"%@%@",@"Sales Target - ",[MedRepDefaults getDefaultStringForEmptyString:currentMonth]];
    
    if (@available(iOS 15.0, *)) {
        self.orderHistoryTblView.sectionHeaderTopPadding = 0;
        self.orderHistoryTbl.sectionHeaderTopPadding = 0;
        self.priceListTbl.sectionHeaderTopPadding = 0;
        self.targetTableView.sectionHeaderTopPadding = 0;
        self.salesStatisticsTblView.sectionHeaderTopPadding = 0;
        self.graphTableViewCell.sectionHeaderTopPadding = 0;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //NSLog(@" location from app delegate %@", [locations lastObject]);
}


-(void)targetDetailsLabels
{
    //fetch total Target
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *currDate = [NSDate date];
    NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                          fromDate:currDate];
    
    NSInteger monthValue = [dComp month];
    
    NSString *targetQry=[NSString stringWithFormat:@"select SUM(Sales_Value) AS Sales_Value , SUM(Target_Value) AS Target_Value, sum(Custom_Attribute_7) AS Balance_To_Go  FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%ld'",[customerDetailsDictionary valueForKey:@"Customer_No"],(long)monthValue];
    
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    float totalTarget = 0.0;
    float achievedTarget = 0.0;
    
    if (targetdatArry.count > 0) {
        
        double tempTarget=[[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] doubleValue];
        overallTargetLbl.text= [NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithInt:tempTarget] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
        
        
        //calculate balance
        
        if ([[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
        }
        else
        {
            totalTarget = [[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
            
            
            if ([[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] isEqual:[NSNull null]]) {
            }
            else
            {
                achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
            }
        }
    }
    
    PNCircleChart *tempchartView = [[PNCircleChart alloc] initWithFrame:chartView.bounds total:[NSNumber numberWithFloat:totalTarget] current:[NSNumber numberWithFloat:achievedTarget] clockwise:YES shadow:YES shadowColor:[UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1.0] displayCountingLabel:YES overrideLineWidth:[NSNumber numberWithInt:15] check:YES];
    [tempchartView strokeChart];
    tempchartView.backgroundColor = [UIColor clearColor];
    [tempchartView setStrokeColor:[UIColor clearColor]];
    [tempchartView setStrokeColorGradientStart:[UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0]];
    [chartView addSubview:tempchartView];
}
- (IBAction)targetGraphButtonTapped:(id)sender {
    detailButtomLabel.hidden = YES;
    graphButtomLabel.hidden = NO;
    [targetGraphButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:16.0]];
    [targetDetailButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:14.0]];
    [targetGraphButton setTitleColor:[UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    [targetDetailButton setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];

    salesVsTargetDetailView.hidden = YES;
    _agencyGraphView.hidden = NO;
    [_graphTableViewCell reloadData];
}
- (IBAction)targetDetailButtonTapped:(id)sender {
    graphButtomLabel.hidden = YES;
    detailButtomLabel.hidden = NO;
    [targetDetailButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:16.0]];
    [targetGraphButton.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemilight" size:16.0]];
    [targetGraphButton setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    [targetDetailButton setTitleColor:[UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    salesVsTargetDetailView.hidden = NO;
    _agencyGraphView.hidden = YES;
    [targetTableView reloadData];
}

-(void)loadCustomerTargetPieChart

{
    
    NSString *achieve = @"50";
    NSString *remain = @"25";
    NSString* test=@"25";
    
    
    pieChartNew = [[PCPieChart alloc] initWithFrame:CGRectMake(8, 198, 320, 200)] ;
    [pieChartNew setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
    
    [pieChartNew setDiameter:140];
    [pieChartNew setSameColorLabel:YES];
    components = [NSMutableArray array] ;
    
    PCPieComponent *component = [PCPieComponent pieComponentWithTitle:@"Target" value:[remain floatValue]];
    PCPieComponent *component2 = [PCPieComponent pieComponentWithTitle:@"Previous Day Sales" value:[achieve floatValue]];
    PCPieComponent *component3 = [PCPieComponent pieComponentWithTitle:@"Sales to Date" value:[test floatValue]];
    
    [component2 setColour:PCColorBlue];
    [component setColour:PCColorOrange];
    [component3 setColour:PCColorGreen];
    
    [components addObject:component2];
    [components addObject:component];
    [components addObject:component3];
    [pieChartNew setComponents:components];
    
    
    
    NSLog(@"pie chart being added ");
    
    
    // [self.view addSubview: pieChartNew];
    
    
    
}

-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    //  Format a number with thousand seperators, eg: "12,345"
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}



-(void)loadCustomerDetailsData
{
    
    // customerDict = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults customer]  ] ;
    
    
    [self fetchCategories];
    
    //set header labels
    
    if (customerDetailsDictionary) {
        
        
        
        
        customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_Name"]];
        
        
        NSString* customerTypeStr=[customerDetailsDictionary valueForKey:@"Customer_ID"];
        
        // [SWDatabaseManager retrieveManager]fetchDataForQuery:@""
        
        
        customerIdLbl.text= [ NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_No"]];
        customerTypeLbl.text=[NSString stringWithFormat:@"%@",[customerDetailsDictionary valueForKey:@"Customer_Type"]];
        
        
        //initial values for labels
        
        if (categoriesArray.count>0) {
            
            
            targetValueCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
            
            salesValueCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
            prevdaySalesCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:0] valueForKey:@"Custom_Attribute_6"]];

            NSString* timeGoneStr=[NSString stringWithFormat:@"%@",[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:0]];
            
            [slices removeAllObjects];
            
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
            [slices addObject:[[categoriesArray objectAtIndex:0] valueForKey:@"Custom_Attribute_6"]];
 
            [customerPieChart reloadData];
            
            
            NSString* totalTgt=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:0] valueForKey:@"Target_Value"]];
            
            
            NSString* achTarget=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:0] valueForKey:@"Sales_Value"]];

            float totalTarget=[totalTgt floatValue];
            
            
            float achievedTarget=[achTarget floatValue];
            //
            float balanceTogo= totalTarget-achievedTarget;
            
            float achievementPercent=(achievedTarget/totalTarget)*100;
            
            achievementCatLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
            agencyLbl.text=[[categoriesArray objectAtIndex:0] valueForKey:@"Classification_1"];
            
            
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"hideOrderHistoryHeader"];
    
    
    customerOrderHistoryView=[[CustomerOrderHistoryView alloc] initWithCustomer:customerDetailsDictionary] ;
    
    [customerOrderHistoryView setFrame:CGRectMake(643,421, 375, 250)];

    customerPriceListView=[[CustomerPriceList alloc] initWithCustomer:customerDetailsDictionary] ;
    [customerPriceListView setFrame:CGRectMake(507 ,424, 480 ,276)];

    [customerOrderHistoryView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    if(customerOrderHistoryView.isToggled)
    {
        customerOrderHistoryView.orderItemView.alpha=0.0;
        customerOrderHistoryView.isToggled = NO;
    }
    [customerOrderHistoryView loadOrders];
    [customerPriceListView loadprice];
    
    
}


- (void)getGetOrderHistory:(NSArray *)orderList
{
    orderHistoryArray=[NSMutableArray arrayWithArray:orderList];
    
    
    NSLog(@"order history in customer dash %@", [orderHistoryArray description]);
    
    
    orderList=nil;
}




-(void)viewWillAppear:(BOOL)animated
{

    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Customer Dashboard";
    self.navigationItem.titleView = label;
    
    [orderHistoryTblView registerNib:[UINib nibWithNibName:@"CustomerOrderHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:@"historyCell"];
    
    orderHistoryTblView.delegate=self;
    orderHistoryTblView.dataSource=self;
    self.navigationController.toolbarHidden=YES;
    [self getGetOrderHistory:[[SWDatabaseManager retrieveManager] dbGetRecentOrders:[customerDetailsDictionary objectForKey:@"Customer_No"]]];
    [orderHistoryTblView reloadData];
    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    isCollectionEnable = appControl.ENABLE_COLLECTION;

    
    
    NSMutableDictionary *refinedDict = customerDetailsDictionary;
    
    
    mapView = [[MapView alloc] init] ;
    
    place = [[Place alloc] init] ;
    
    
    NSString * latStr=[NSString stringWithFormat:@"%@",[customerDetailsDictionary objectForKey:@"Cust_Lat"]];
    NSString * longStr=[customerDetailsDictionary objectForKey:@"Cust_Long"];
    
    
    if ([latStr isEqualToString:@"0"]) {
        
        latStr=@"25.204490";
        longStr=@"55.2707830";
        
        
        CLLocationCoordinate2D custLocation;
        custLocation.latitude=[latStr floatValue];
        custLocation.longitude=[longStr floatValue];
        
        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
                                                                              custLocation, 50000, 50000);
        [self.custMapView setRegion:regionCustom animated:NO];
        
        
    }
    
    else
    {
        
        
        place.latitude= [[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
        place.longitude =  [[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
        
    }
    
    
    
    
    place.name= [customerDetailsDictionary objectForKey:@"Customer_Name"];
    place.otherDetail = [customerDetailsDictionary objectForKey:@"City"];
    
    [mapView showCurrentLocationAddress:place];
    
    if ([latStr isEqualToString:@"0"]) {
        
        
    }
    
    
    else
    {
        
        
        Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
        [custMapView addAnnotation:Annotation];
        
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapView] ;
    
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    [locationFilterPopOver setDelegate:self];
    
    CGRect rect = self.view.frame;
    rect.size.width = rect.size.width;
    
    popoverOriented=NO;
    
    
    
    NSArray *keysForNullValues = [refinedDict allKeysForObject:[NSNull null]];
    [refinedDict removeObjectsForKeys:keysForNullValues];
    
    addressLbl.text=[refinedDict valueForKey:@"Address"];
    cityLbl.text=[refinedDict valueForKey:@"CITY"];
    
    poBoxLbl.text=[refinedDict valueForKey:@"Postal_Code"];
    locationLbl.text=[refinedDict valueForKey:@"Location"];
    
    if ([[refinedDict valueForKey:@"PDC_Due"] isEqualToString:@"N/A"]) {
        
        pdcDueLbl.text=[refinedDict valueForKey:@"PDC_Due"];
        overDueLbl.text=[refinedDict valueForKey:@"Over_Due"];
    }
    
    else
    {
        
        pdcDueLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"PDC_Due"]];
        overDueLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"Over_Due"]];
    }
    
    
    
    
    
    NSInteger refinedCreditLimit=[[NSString stringWithFormat:@"%@",[refinedDict valueForKey:@"Credit_Limit"]] integerValue];
    
    creditLimitLbl.text=[[NSString stringWithFormat:@"%ld",(long)refinedCreditLimit] currencyString];
    
    
    
    avblBalanceLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[refinedDict valueForKey:@"Avail_Bal"]];
    
    
    
    [customerOrderHistoryView refreshOrderHistory];
    
    
    
    
    [self testMap];
    
    [self loadCustomerTargetPieChart];
    [self fetchSalesStatistics];

    
    /*** Small Chart ****/
    self.myBarChart.delegate=self;
    self.myBarChart.barGraphStyle=BAR_GRAPH_STYLE_GROUPED;
    self.myBarChart.barLabelStyle=BAR_LABEL_STYLE1;
    self.myBarChart.tag=10;
    self.myBarChart.margin=MIMMarginMake(0, -20, -10, 0);
    self.myBarChart.glossStyle=GLOSS_STYLE_1;
    self.myBarChart.xTitleStyle=XTitleStyle2;
    [self.myBarChart drawBarChart];
    self.myBarChart.backgroundColor = [UIColor clearColor];
    
    self.myBarChart.titleLabel.text = @"One";
    self.myBarChart.titleLabel1.text = @"Two";
    self.myBarChart.titleLabel2.text = @"Three";
    //
    self.myBarChart.titleLabel.frame = CGRectMake(65, 190, 100, 20);
    self.myBarChart.titleLabel1.frame = CGRectMake(205, 190, 100, 20);
    self.myBarChart.titleLabel2.frame = CGRectMake(330, 190, 100, 20);
    
    self.myBarChart.plannedViewColor.frame = CGRectMake(380,5, 30, 10);
    self.myBarChart.completedViewColor.frame = CGRectMake(380, 20, 30, 10);
    
    self.myBarChart.plannedViewColorLable.frame = CGRectMake(415,0, 100, 20);
    self.myBarChart.completedViewColorLable.frame = CGRectMake(415, 15, 100, 20);
    
    self.myBarChart.plannedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    self.myBarChart.completedViewColorLable.font = [UIFont fontWithName:@"Helvetica" size:10];
    
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    

    NSMutableDictionary * monthYearDict=[[SWDatabaseManager retrieveManager]fetchSalesTargetMonthandYearforCustomer:[customerDict valueForKey:@"Customer_No"]];
    
    NSString* monthString=[SWDefaults getValidStringValue:[monthYearDict valueForKey:@"Month"]];
    
    if ([NSString isEmpty:monthString]==NO) {
        
        
        NSString *yearString = [NSString stringWithFormat:@"%@",[monthYearDict valueForKey:@"Year"]];
        NSInteger monthNumber=[[NSString stringWithFormat:@"%@",[monthYearDict valueForKey:@"Month"]] integerValue];
        
        
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:(monthNumber-1)];
        monthLbl.text=[NSString stringWithFormat:@"(Month: %@ - %@)", monthName,yearString];
        
        
    }
  
}

-(NSString*)thousandSaperatorforDouble:(NSInteger)number

{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    [numberFormatter setMinimumFractionDigits:2];
    [numberFormatter setMaximumFractionDigits:2];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}



-(void)displayPopOver:(UIBarButtonItem*)barBtn
{
    
    UIView *view= (UIView *)[self.navigationController.navigationBar.subviews  objectAtIndex:2]; // 0 for the first item
    
    
    AlSeerOptionsPopOverTableViewController* options=[[AlSeerOptionsPopOverTableViewController alloc]init];
    
    
    optionsPopover=[[UIPopoverController alloc]initWithContentViewController:options];
    
    if([optionsPopover isPopoverVisible])
    {
        
        [optionsPopover dismissPopoverAnimated:YES];
        return;
    }
    
    
    UINavigationController *favNav = [[UINavigationController alloc]
                                      initWithRootViewController:options];
    
    
    
    favNav.title=@"TEST";
    
    //    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeSalesHistory)];
    //    favNav.navigationItem.rightBarButtonItem=closeButton;
    
    
    
    
    
    
    
    
    
    optionsPopover = [[UIPopoverController alloc]
                      initWithContentViewController:favNav];
    optionsPopover.popoverContentSize=CGSizeMake(800, 650);
    
    //stockInfoVC.popOver=popoverController;
    
    
    [optionsPopover presentPopoverFromRect:view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
    
    
    
    
    
    NSLog(@"check the subviews %@", [self.navigationController.navigationBar.subviews description]);
    
    
}

- (void)displayActions:(id)sender {
    if (actions == nil)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
        }
        else{
            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
        }
        
    }
    [actions showFromBarButtonItem:sender animated:YES];
}

//- (void)displayActions:(id)sender {
//    if (actions == nil)
//    {
//        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
//        {
//            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Collection", nil),NSLocalizedString(@"Survey", nil), nil]  ;
//        }
//        else{
//            //            actions = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Action", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Start Visit", nil), NSLocalizedString(@"Survey", nil), nil]  ;
//            
//            
//            //check this action sheet
//            
//            
//            actions = [[UIActionSheet alloc]
//                       initWithTitle:@"Select Folder"
//                       delegate:self
//                       cancelButtonTitle:nil
//                       destructiveButtonTitle:nil
//                       otherButtonTitles:nil];
//            
//            NSMutableArray* buttons=[[NSMutableArray alloc] initWithObjects:@"Start Visit",@"Survey", nil];
//            
//            //[actions addButtonWithTitle:@"Start Visit"];
//            
//            for (int nb=0; nb<2; nb++)
//            {
//                
//                
//                [actions addButtonWithTitle:@"Start Visit"];
//                
//            }
//            
//            
//            
//            [actions addButtonWithTitle:@"Survey"];
//            
//            
//            
//            //actions.cancelButtonIndex = [actions addButtonWithTitle: @"Cancel"];
//            
//            
//        }
//        
//    }
//    [actions showFromBarButtonItem:sender animated:YES];
//}
//



- (void)displayCustomerMap:(id)sender {
    
    
    
    
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    //[self applyMapViewMemoryHotFix];
    popoverOriented = YES;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    //  [self applyMapViewMemoryHotFix];
    
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)testMap
{
    double selectedCustLat = [[SWDefaults getValidStringValue:[customerDetailsDictionary objectForKey:@"Cust_Lat"]] doubleValue];
    double selectedCustLong = [[SWDefaults getValidStringValue:[customerDetailsDictionary objectForKey:@"Cust_Long"]] doubleValue];
    
    CLLocationCoordinate2D selectedCustomerCoordinate= CLLocationCoordinate2DMake(selectedCustLat, selectedCustLong);
    MKPointAnnotation *Pin = [[MKPointAnnotation alloc]init];
    Pin.coordinate = selectedCustomerCoordinate;
    Pin.title=[SWDefaults getValidStringValue:[customerDetailsDictionary objectForKey:@"Customer_Name"]];
    
    [custMapView removeAnnotations:custMapView.annotations];
    [custMapView addAnnotation:Pin];
    
    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (selectedCustomerCoordinate, 50000, 50000);
    [custMapView setRegion:regionCustom animated:YES];
    
//    if (custMapView == nil)
//    {
//        self.custMapView = [[MKMapView alloc] init];
//    }
//    // self.mapView.frame = CGRectMake(0, 0, single.frameWidth, single.frameHeight) ;
//
//    [self.custMapView setMapType:MKMapTypeStandard];
//    [self.custMapView setZoomEnabled:YES];
//    [self.custMapView setScrollEnabled:YES];
//    [self.custMapView setShowsUserLocation:YES];
//    [self.custMapView setUserInteractionEnabled:YES];
//
//    // Singleton *single = [Singleton retrieveSingleton];
//
//
//    MKUserLocation *userLocation = self.custMapView.userLocation;
//
//
//    CLLocationCoordinate2D custLocation;
//    custLocation.latitude=[[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
//    custLocation.longitude=[[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
//
//    MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (custLocation, 50000, 50000);
//    [self.custMapView setRegion:regionCustom animated:NO];
//
//
//    //    span.latitudeDelta=0.2;
//    //    span.longitudeDelta=0.2;
//    //    region.span=span;
//
//    //[self.mapView setRegion:region animated:YES];
//    // [self.mapView regionThatFits:region];
//
//
//
//    place = [[Place alloc] init] ;
//    place.latitude= [[customerDetailsDictionary objectForKey:@"Cust_Lat"] floatValue];
//    place.longitude =  [[customerDetailsDictionary objectForKey:@"Cust_Long"] floatValue];
//    place.name= [customerDetailsDictionary objectForKey:@"Customer_Name"];
//    place.otherDetail = [customerDetailsDictionary objectForKey:@"City"];
//
//
//
//    if (place.latitude==0) {
//
//
//        NSString* latStr=@"25.204490";
//        NSString*  longStr=@"55.2707830";
//
//        CLLocationCoordinate2D custLocation;
//        custLocation.latitude= [latStr floatValue];
//        custLocation.longitude=[longStr floatValue];
//
//        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (custLocation, 50000, 50000);
//        [self.custMapView setRegion:regionCustom animated:NO];
//
//    }
//
//
//    else
//    {
//
//        Annotation=[[CSMapAnnotation alloc]initWithPlace:place] ;
//        [self.custMapView addAnnotation:Annotation];
//    }
//
//    [self.custMapView setDelegate:self];
//    [self.view addSubview:self.custMapView];
}


#pragma mark mapview delegate methods

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//
//
//    //   NSLog(@"user location called");
//
//    MKCoordinateSpan spanCustom;
//    spanCustom.latitudeDelta = 0.002;
//    spanCustom.longitudeDelta = 0.002;
//
//
//    CLLocationCoordinate2D customerCoordinates;
//    customerCoordinates.latitude =  [[customerDetailsDictionary valueForKey:@"Cust_Lat"]doubleValue];
//
//    customerCoordinates.longitude = [[customerDetailsDictionary valueForKey:@"Cust_Long"]doubleValue];
//
//    if (customerCoordinates.latitude==0) {
//
//        NSString* latStr=@"25.204490";
//        NSString*  longStr=@"55.2707830";
//
//        CLLocationCoordinate2D custLocation;
//        custLocation.latitude= [latStr floatValue];
//        custLocation.longitude=[longStr floatValue];
//
//        MKCoordinateRegion regionCustom = MKCoordinateRegionMakeWithDistance (
//                                                                              custLocation, 50000,50000);
//        [self.custMapView setRegion:regionCustom animated:NO];
//
//
//    }
//
//    else
//    {
//
//
//
//        MKCoordinateRegion regionCustom;
//        regionCustom.span = span;
//        regionCustom.center = customerCoordinates;
//
//
//        [self.custMapView setRegion:regionCustom animated:YES];
//
//    }
//
//    // MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(customerCoordinates, 80, 80);
//    //    [custMapView setRegion:[custMapView regionThatFits:region] animated:YES];
//    //
//    //    // Add an annotation
//    //    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    //    point.coordinate = userLocation.coordinate;
//    //    point.title = @"Where am I?";
//    //    point.subtitle = @"I'm here!!!";
//    //
//    //    [custMapView addAnnotation:point];
//}
//
//
///*
// #pragma mark - Navigation
//
// // In a storyboard-based application, you will often want to do a little preparation before navigation
// - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
// // Get the new view controller using [segue destinationViewController].
// // Pass the selected object to the new view controller.
// }
// */
//
//
//#pragma mark mapView delegate functions
//- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
//{
//}
//
//- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
//{
//
//}
//
//#pragma mark mapView delegate functions
//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
//}
//
//- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
//}
//
//- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id <MKAnnotation>)annotation {
//    if(annotation == map.userLocation) {
//        return nil;
//    }
//
//
//    static NSString *defaultID=@"MYLoction";
//
//    MKPinAnnotationView *pinView = (MKPinAnnotationView *)[self.custMapView dequeueReusableAnnotationViewWithIdentifier:defaultID] ;
//
//    [pinView setSelected:YES animated:YES];
//    [pinView setEnabled:YES];
//
//    if (!pinView) {
//
//        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
//
//
//        pinView.canShowCallout=YES;
//        pinView.animatesDrop=YES;
//
//
//    }
//    return pinView;
////
////
////    pinView.pinColor=MKPinAnnotationColorGreen;
////
////
////    [pinView setSelected:YES animated:YES];
////    [pinView setEnabled:YES];
////
////    if (!pinView) {
////
////        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
////
////
////        pinView.canShowCallout=YES;
////        pinView.animatesDrop=YES;
////
////        //       UIButton* infoButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
////        //        infoButton.backgroundColor=[UIColor redColor];
////        //
////        //        pinView.rightCalloutAccessoryView = infoButton;
////        //        [infoButton addTarget:self action:@selector(launchMap:) forControlEvents:UIControlEventTouchUpInside];
////        //
////
////    }
////    return pinView;
//}

-(void)selectedStringValue:(id)value
{
    
    NSLog(@"selected value is %@", value);
    agencyLbl.text=[NSString stringWithFormat:@"%@", value];
    
    
    
    [slices removeAllObjects];
    
    
    
    
    
    for (NSInteger i=0; i<categoriesArray.count; i++) {
        
        
        if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Classification_1"] isEqualToString:value] ) {
            
            //update tables accordingly
            
            
            
            //[NSString stringWithFormat:@"%@",
            
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]isEqual:[NSNull null]]) {
                
                
                targetValueCatLbl.text=@"";
                [slices addObject:[NSString stringWithFormat:@"%d",0]];
                
                
                
            }
            else
            {
                
                
                targetValueCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
                
            }
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]isEqual:[NSNull null]]) {
                
                [slices addObject:[NSString stringWithFormat:@"%d",0]];
                
                prevdaySalesCatLbl.text=@"";
                
                
            }
            else
            {
                prevdaySalesCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
                
            }
            
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]isEqual:[NSNull null]]) {
                
                [slices addObject:[NSString stringWithFormat:@"%d",0]];
                
                salesValueCatLbl.text=@"";
                
                
            }
            else
            {
                salesValueCatLbl.text=[NSString stringWithFormat:@"%@%@ ",userCurrencyCode,[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
                [slices addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
            }
            
            
            
            //            salesValueCatLbl.text=[NSString stringWithFormat:@"AED%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
            //            prevdaySalesCatLbl.text=[NSString stringWithFormat:@"AED%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Custom_Attribute_6"]];
            
            NSLog(@"slice values are %@", [slices description]);
            
            
            
            
            
            
            [customerPieChart reloadData];
            
            
            
            NSString* totalTgt=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]];
            
            
            
            if ([[[categoriesArray objectAtIndex:i] valueForKey:@"Target_Value"]isEqual:[NSNull null]]) {
                achievementCatLbl.text=@"";
                
                
            }
            
            else
            {
                NSString* achTarget=[NSString stringWithFormat:@"%@",[[categoriesArray objectAtIndex:i] valueForKey:@"Sales_Value"]];
                
                
                
                float totalTarget=[totalTgt floatValue];
                
                
                float achievedTarget=[achTarget floatValue];
                //
                float balanceTogo= totalTarget-achievedTarget;
                
                float achievementPercent=(achievedTarget/totalTarget)*100;
                
                achievementCatLbl.text=[[NSString stringWithFormat:@"%0.2f",achievementPercent] stringByAppendingString:@"%"];
                
                
            }
            
            
        }
        
    }
    
    
    //update lables based on selected category
    
    
    //Dismiss the popover if it's showing.
    if (valuePopOverController)
    {
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
}


- (IBAction)categoryBtnTapped:(id)sender {
    
    
    
    UIButton *btn  = (UIButton *)sender;
    outletPopOver = nil;
    valuePopOverController=nil;
    
    outletPopOver = [[OutletPopoverTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    
    
    //display values for pop over
    
    
    NSMutableArray* categoryNamesArray=[[NSMutableArray alloc]init];
    
    
    
    
    for (NSInteger i=0; i<categoriesArray.count; i++) {
        
        [categoryNamesArray addObject:[[categoriesArray objectAtIndex:i] valueForKey:@"Classification_1"]];
        
        
        
    }
    
    
    
    
    
    
    outletPopOver.colorNames = categoryNamesArray;// [NSMutableArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
    outletPopOver.delegate = self;
    
    if (valuePopOverController == nil) {
        //The color picker popover is not showing. Show it.
        valuePopOverController = [[UIPopoverController alloc] initWithContentViewController:outletPopOver];
        [valuePopOverController presentPopoverFromRect:CGRectMake(350, 120, 175, 200) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        valuePopOverController.delegate=self;
    }
    else {
        //The color picker popover is showing. Hide it.
        [valuePopOverController dismissPopoverAnimated:YES];
        valuePopOverController = nil;
        valuePopOverController.delegate=nil;
    }
    
    
}

-(void)fetchCategories

{
    NSString*categoriesQry=[NSString stringWithFormat:@"select IFNULL(Target_Value,0) AS Target_Value,IFNULL(Sales_Value,0) AS Sales_Value,IFNULL(Classification_1,0) AS Classification_1 ,IFNULL(Custom_Attribute_6,0) AS Custom_Attribute_6 ,IFNULL(Custom_Attribute_4 ,0)AS Time_Gone from TBL_Sales_Target_Items where Classification_2='%@'", [customerDetailsDictionary valueForKey:@"Customer_No"]];
    
    
    
    NSLog(@"categories query is %@", categoriesQry);
    
    
    categoriesArray=[[NSMutableArray alloc]init];
    
    
    
    categoriesArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:categoriesQry];
    
    
    NSLog(@"categories are %@", [categoriesArray description]);
    
}

-(void)updateCategoryLevelLabels:(NSMutableArray*)labelData

{
    
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    NSMutableArray* targetdatArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
    
    NSInteger tempTarget=[[  NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]] integerValue];
    
    if (targetdatArry.count>0) {
        
        targetValueCatLbl.text= [[NSNumber numberWithInt:tempTarget] descriptionWithLocale:[NSLocale currentLocale]];
        
    }
    
    targetValueCatLbl.text=@"";
    salesValueCatLbl.text=@"";
    prevdaySalesCatLbl.text=@"";
    achievementCatLbl.text=@"";
    
    
    
}

-(void)fetchSalesStatistics

{
    NSString* targetQry=@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value] FROM TBL_Sales_Target_Items";
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *currDate = [NSDate date];
    NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                          fromDate:currDate];
    
    month = [dComp month];
    
    
    NSLog(@"month is %ld", (long)month);
    
    
    [self fetchCustomerStatistics];
    
    
    NSString* salesTargetsQry=[NSString stringWithFormat:@"SELECT IFNULL(Custom_Attribute_1 ,0) AS Actual_Achievement, IFNULL(Classification_1,0) AS Classification_1 ,IFNULL(Custom_Attribute_8,0) AS Custom_Attribute_8 ,IFNULL(Custom_Attribute_7,0) AS Custom_Attribute_7 ,SUM(IFNULL(Target_Value,0)) AS Target_Value , SUM(IFNULL(Sales_Value,0)) AS Sales_Value ,IFNULL(Classification_2,0) AS Classification_2 , SUM(IFNULL(Custom_Attribute_5,0) )AS LYTM,MAX(IFNULL(Custom_Attribute_4,0)) AS Time_Gone FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%ld'  group by Classification_1",[customerDetailsDictionary valueForKey:@"Customer_No"],(long)month];
    
    
    salesTargetItemsArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesTargetsQry];
    
    
    if (salesTargetItemsArray.count>0) {
        
        
        NSMutableArray* tempTimeGoneLblArray=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<salesTargetItemsArray.count; i++) {
            [tempTimeGoneLblArray addObject:[[salesTargetItemsArray valueForKey:@"Time_Gone"] objectAtIndex:i]];
            
        }
        
        NSNumber *maxTimeGoneTempVal=[tempTimeGoneLblArray valueForKeyPath:@"@max.integerValue"];
        self.timegoneLbl.text=[[NSString stringWithFormat:@"%ld", (long)[maxTimeGoneTempVal integerValue]] stringByAppendingString:@"%"];
        [self.salesStatisticsTblView reloadData];
    }
}



#pragma mark Table View Delegate Methods





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==orderHistoryTblView) {
        
        AlSeerOrderHistoryPopupViewController* salesHistoryPopUp=[[AlSeerOrderHistoryPopupViewController alloc]init];
        
        salesHistoryPopUp.preferredContentSize = CGSizeMake(894.0, 600.0);
        
        salesHistoryPopUp.modalPresentationStyle = UIModalPresentationFormSheet;
        
        salesHistoryPopUp.salesOrderArray=orderHistoryArray;
        
        salesHistoryPopUp.selectedIndex=indexPath.row;
        
        
        salesHistoryPopUp.salesOrderNumber=[NSString stringWithFormat:@"%@",[[orderHistoryArray valueForKey:@"Orig_Sys_Document_Ref"] objectAtIndex:indexPath.row]];
        
        
        [self presentViewController:salesHistoryPopUp animated:YES completion:nil];
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ChartTableViewCell";
    static NSString *datacellIdentifier = @"dataTableViewCell";
    static NSString * histotyCellIdentifier=@"historyCell";

    
    tableView.allowsSelection = YES;
    
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.showsVerticalScrollIndicator = NO;
    
    self.graphTableViewCell.showsHorizontalScrollIndicator=NO;
    self.graphTableViewCell.showsVerticalScrollIndicator=NO;
    
    if (tableView.tag == 1) {
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            
            actualAchievementPercent=[[[salesTargetItemsArray valueForKey:@"Actual_Achievement"] objectAtIndex:indexPath.row] doubleValue];
            
            if (actualAchievementPercent>=100.0) {
                
                cell.actualAchievementLbl.backgroundColor=[UIColor greenColor];
            }
            else if (actualAchievementPercent>=80)
            {
                cell.actualAchievementLbl.backgroundColor=[UIColor colorWithRed:(242.0/255.0) green:(197.0/255.0) blue:(117.0/255.0) alpha:1];
            }
            else
            {
                cell.actualAchievementLbl.backgroundColor=[UIColor redColor];
                
            }
            
            if ([[salesTargetItemsArray valueForKey:@"Classification_1"]isEqual:[NSNull null]] || [salesTargetItemsArray valueForKey:@"Classification_1"]==nil) {
                
                cell.agencyLbl.text=@"-";
            }
            
            else
            {
                cell.agencyLbl.text=
                [[salesTargetItemsArray valueForKey:@"Classification_1"] objectAtIndex:indexPath.row];
            }
            
            NSString* formattedStr=[NSString stringWithFormat:@"%@",[[salesTargetItemsArray valueForKey:@"LYTM"] objectAtIndex:indexPath.row]];
            
            
            NSInteger formattedDouble=[formattedStr integerValue];
            
            cell.lytmLbl.text=[self thousandSaperatorforDouble:formattedDouble];
            
            NSString* dailyTargetStr=[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row];
            NSInteger dailyTarget=0;
            
            if (!dailyTargetStr) {
                
                
            }
            
            else
            {
                
                dailyTarget=[dailyTargetStr integerValue];
            }
            
            
            if (!dailyTarget) {
                
                cell.targetLbl.text=@"0";
                
            }
            
            else
            {
                cell.targetLbl.text=[self thousandSaperatorforDouble:dailyTarget];
            }
            
            if ([[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] isEqual:[NSNull null]]|| [[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]==nil) {
                cell.MTDsalesLbl.text=@"0";
                cell.achPercentLbl.text=@"0";
                cell.dailySalesLbl.text=@"0";
                cell.btgLbl.text=@"0";
            }
            
            else
            {
                NSInteger dailySale=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row]integerValue];
                
                
                cell.MTDsalesLbl.text=[self thousandSaperatorforDouble:dailySale];
                
                
                float totalTarget=[[[salesTargetItemsArray valueForKey:@"Target_Value"] objectAtIndex:indexPath.row] floatValue];
                
                float achievedTarget=[[[salesTargetItemsArray valueForKey:@"Sales_Value"] objectAtIndex:indexPath.row] floatValue];
                
                float achievementPercent=(achievedTarget/totalTarget)*100;
                
                
                if (totalTarget==0) {
                    
                    cell.achPercentLbl.text=@"0%";
                }
                
                else
                {
                    
                    cell.achPercentLbl.text=[[NSString stringWithFormat:@"%0.0f", achievementPercent] stringByAppendingString:@"%"];
                    
                }
                
                
                NSInteger tempTargetDsg=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_7"] objectAtIndex:indexPath.row]]integerValue];

                cell.btgLbl.text=[self thousandSaperatorforDouble:tempTargetDsg];
                
                NSInteger dailySalestoGo=[[NSString stringWithFormat:@"%@", [[salesTargetItemsArray valueForKey:@"Custom_Attribute_8"] objectAtIndex:indexPath.row]]integerValue];
                
                
                cell.dailySalesLbl.text=[self thousandSaperatorforDouble:dailySalestoGo];
                
            }
            
            
        }
        
        return cell;
    } else if(tableView.tag ==100){
        tableView.allowsSelection = YES;

        CustomerOrderHistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:histotyCellIdentifier];
        if (cell==nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"CustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
            
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        NSString* dateString=[NSString stringWithFormat:@"%@", [[orderHistoryArray valueForKey:@"Creation_Date"] objectAtIndex:indexPath.row]];
        
        NSDateFormatter * dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"GMT"]];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSLog(@"is date format working? %@", [dateFormatter dateFromString:dateString]);
        
        NSDate * refinedDate=[dateFormatter dateFromString:dateString];
        
        
        NSDateFormatter * refinedDateFormatter=[[NSDateFormatter alloc]init];
        [refinedDateFormatter setDateFormat:@"dd-MM-yyyy"];
        
        NSLog(@"is refined date working %@", [refinedDateFormatter stringFromDate:refinedDate]);
        
        NSString* refinedDateStr=[refinedDateFormatter stringFromDate:refinedDate];
        
        [cell.orderNumberLbl sizeToFit];
        cell.orderNumberLbl.numberOfLines = 0;
        cell.orderNumberLbl.text=[NSString stringWithFormat:@"%@", [[orderHistoryArray valueForKey:@"Orig_Sys_Document_Ref"] objectAtIndex:indexPath.row]];
        
        cell.orderDateLbl.text=refinedDateStr;
        
        
        NSString* orderAmtStr=[NSString stringWithFormat:@"%0.2f", [[[orderHistoryArray valueForKey:@"Transaction_Amt"] objectAtIndex:indexPath.row] doubleValue]];
        
        
        
        cell.statusLbl.text=orderAmtStr;
        
        
        return cell;
        
    }
    else
    {
        tableView.alwaysBounceVertical = NO;
        [tableView setShowsHorizontalScrollIndicator:NO];
        [tableView setShowsVerticalScrollIndicator:NO];
        ChartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ChartTableViewCell" owner:nil options:nil] firstObject];
        }
       // cell.isCommingFromCustDash = YES;
        cell.customerDetail=@"Dashboard";
        
        cell.customerDict=customerDetailsDictionary;
        [cell configUI:indexPath];
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    if (tableView.tag==0) {
        
        
        static NSString *datacellIdentifier = @"dataTableViewCell";
        
        GraphHeaderCustomerDashTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"GraphHeaderCustomerDashTableViewCell" owner:nil options:nil] firstObject];
        }
        return cell;
    }
    
    else if (tableView.tag==100)
    {
        
        static NSString * historyCellIdentifer=@"historyCell";
        
        CustomerOrderHistoryTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:historyCellIdentifer];
        cell=nil;
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"CustomerOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
        
        cell.orderNumberLbl.text=@"Order No.";
        cell.orderDateLbl.text=@"Date";
        cell.statusLbl.text=@"Amount (AED)";
        
        cell.orderNumberLbl.textColor = TableViewHeaderSectionColor;
        cell.orderDateLbl.textColor = TableViewHeaderSectionColor;
        cell.statusLbl.textColor = TableViewHeaderSectionColor;
        return cell;
    }
    
    
    else
    {
        static NSString *datacellIdentifier = @"dataTableViewCell";
        DataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:datacellIdentifier];
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DataTableViewCell" owner:nil options:nil] firstObject];
            
            cell.agencyLbl.text=@"Agency";
            cell.targetLbl.text=@"Target";
            cell.MTDsalesLbl.text=@"MTD";
            cell.achPercentLbl.text=@"Achieved (%)";
            cell.btgLbl.text=@"BTG";
            cell.dailySalesLbl.text=@"Daily Req.";
            cell.lytmLbl.text=@"LYTM";
            
            [cell.agencyLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.targetLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.MTDsalesLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.achPercentLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.btgLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.dailySalesLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            [cell.lytmLbl setFont:EditableTextFieldSemiFontOfSize(14)];
            
            cell.agencyLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.targetLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.MTDsalesLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.achPercentLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.btgLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.dailySalesLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            cell.lytmLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
            
            cell.backgroundColor = [UIColor colorWithRed:(235.0/255.0) green:(251.0/255.0) blue:(249.0/255.0) alpha:1];
        }
        return cell;
    }
    
}// custom view for header. will be adjusted to default or specified header height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag==0) {
        
        return 200;
    }
    
    else
    {
        return 45;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==0) {
        
        return 1;
    }
    else if (tableView.tag==100)
    {
        if(orderHistoryArray.count>0)
        {
            return orderHistoryArray.count;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (salesTargetItemsArray.count>0) {
            return salesTargetItemsArray.count;
        }
        else
        {
            return 0;
        }
    }
}





-(void)fetchLabelDatafromDB

{

    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDate *currDate = [NSDate date];
    NSDateComponents *dComp = [calendar components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit )
                                          fromDate:currDate];
    
    NSInteger monthValue = [dComp month];
    
    NSString *labelDataQry =[NSString stringWithFormat:@"select SUM(Sales_Value) AS Sales_Value , SUM(Target_Value) AS Target_Value, sum(Custom_Attribute_7) AS Balance_To_Go  FROM TBL_Sales_Target_Items where  Classification_2 ='%@' and Month='%ld'",[customerDetailsDictionary valueForKey:@"Customer_No"],(long)monthValue];
    
    NSLog(@"query of top level data %@", labelDataQry);
    
    
    
    NSMutableArray* testArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:labelDataQry];
    
    topLevelDataArray=[self nullFreeArray:testArray];
    
    
    if (topLevelDataArray.count>0) {
        
        NSString *balanceToGo = [NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Balance_To_Go"]];
        if ([balanceToGo isEqualToString:@"<null>"]) {
            monthBtgLbl.text  = @"AED 0.00";
        }else{
            monthBtgLbl.text=[[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%@",balanceToGo] integerValue]] currencyString];
        }
        
        NSString *salesValue = [NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Sales_Value"]];
        if ([salesValue isEqualToString:@"<null>"]) {
            monthSalesLbl.text  = @"AED 0.00";
        }else{
            monthSalesLbl.text=[[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%@",salesValue] integerValue]] currencyString];
        }
        
        NSString *targetVal = [NSString stringWithFormat:@"%@",[[topLevelDataArray objectAtIndex:0] valueForKey:@"Target_Value"]];
        if ([targetVal isEqualToString:@"<null>"]) {
            monthTargetLbl.text  = @"AED 0.00";
        }else{
            
            monthTargetLbl.text=[[NSString stringWithFormat:@"%ld",(long)[[NSString stringWithFormat:@"%@",targetVal] integerValue]] currencyString];
            
        }

    }
}


-(void)fetchCustomerStatistics

{
    
    
    if (customerDetailsDictionary.count>0) {
        
        
        
        NSString* targetQry=[NSString stringWithFormat:@"SELECT SUM(Target_Value) As [Target_Value],SUM(Sales_Value) As [Sales_Value],SUM(custom_Attribute_7) AS Balance_To_Go, SUM(Custom_Attribute_6) As [Previous_Day_Sales] FROM TBL_Sales_Target_Items where Classification_2 ='%@' and Month='%ld' ",[customerDetailsDictionary valueForKey:@"Customer_No"],(long)month];
        
        NSLog(@"query for customer statistics %@", targetQry);
        
        NSMutableArray* responseArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:targetQry];
        
        NSMutableArray* targetdatArry= [self nullFreeArray:responseArray];
        
        
        float totalTarget;
        float achievedTarget;
        float prevDaySales;
        
        
        if (targetdatArry.count>0) {
            
            NSString *checkTargetStr=[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0];
            
            if ([[[targetdatArry valueForKey:@"Target_Value"]objectAtIndex:0]isEqual:[NSNull null]]) {
                
                totalTarget=0.0;
            }
            
            else
            {
                totalTarget=[[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0] floatValue];
            }
            
            if ([[[targetdatArry valueForKey:@"Sales_Value"]objectAtIndex:0]isEqual:[NSNull null]]) {
                achievedTarget=0.0;
            }
            
            else
            {
                achievedTarget=[[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0] floatValue];
            }
            
            
            if ([[[targetdatArry valueForKey:@"Previous_Day_Sales"]objectAtIndex:0]isEqual:[NSNull null]]) {
                prevDaySales=0.0;
            }
            
            else
            {
                
                prevDaySales=[[[targetdatArry valueForKey:@"Previous_Day_Sales"] objectAtIndex:0] floatValue];
            }
            float balanceTogo= totalTarget-achievedTarget;
            
            double achievementPercent=(achievedTarget/totalTarget)*100;
            
            
            NSString* targetStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Target_Value"] objectAtIndex:0]];
            NSString* achTargetStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Sales_Value"] objectAtIndex:0]];
            NSString* precDaySaleStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Previous_Day_Sales"] objectAtIndex:0]];
            NSString* btgStr=[NSString stringWithFormat:@"%@",[[targetdatArry valueForKey:@"Balance_To_Go"] objectAtIndex:0]];
            
            
            double formattedTarget=[targetStr integerValue];
            double formattedAch=[achTargetStr integerValue];
            double formattedPrevDaySales=[precDaySaleStr integerValue];
            double formattedBtg=[btgStr integerValue];
            
            self.totalTargetSalesStatsLbl.text=[self thousandSaperatorforDouble:formattedTarget];
            self.totalSalesStatsLbl.text=[self thousandSaperatorforDouble:formattedAch];
            self.prevDaySalesStatsLbl.text=[self thousandSaperatorforDouble:formattedPrevDaySales];
            self.btgSalesStatsLbl.text=[self thousandSaperatorforDouble:formattedBtg];
            
            
            if (categoriesArray.count>0) {
                
                NSInteger timeGoneIntval=[[NSString stringWithFormat:@"%@", [[categoriesArray objectAtIndex:0]valueForKey:@"Time_Gone"]] integerValue];
                
                NSInteger achPercentVal= (NSInteger) achievementPercent;
                
                NSMutableArray * tempArray=[[NSMutableArray alloc]init];
                
                NSMutableArray* temp=[[NSMutableArray alloc]init];
                for (NSInteger i=0; i<categoriesArray.count; i++) {
                    [tempArray addObject:[[categoriesArray valueForKey:@"Time_Gone"] objectAtIndex:i]];
                    
                }
                
                NSNumber *maxTimeGoneNumber=[tempArray valueForKeyPath:@"@max.integerValue"];
                NSInteger maxTimeGoneTemp=[maxTimeGoneNumber integerValue];
                double tempDifference=(achievementPercent/maxTimeGoneTemp)*100;
                

                if (isnan(tempDifference) || isinf(tempDifference)) {
                }
                else
                {
                    if (achPercentVal>=maxTimeGoneTemp) {
                        self.achSalesStatsLbl.textColor=[UIColor greenColor];
                    }
                    
                    else if (tempDifference>=80)
                    {
                        self.achSalesStatsLbl.textColor=[UIColor colorWithRed:(242.0/255.0) green:(197.0/255.0) blue:(117.0/255.0) alpha:1];
                    }
                    else
                    {
                        self.achSalesStatsLbl.textColor=[UIColor redColor];
                    }
                }
                
                if (totalTarget==0) {
                    
                    self.achSalesStatsLbl.text=@"0.00%";
                }
                
                else
                {
                    self.achSalesStatsLbl.text=[[NSString stringWithFormat:@"%0.0f", achievementPercent] stringByAppendingString:@"%"];
                }
            }
        }
    }
}


-(NSMutableArray*)nullFreeArray:(NSMutableArray*)contentArray

{
    
    
    for(NSInteger i=0;i<contentArray.count;i++)
    {
        if([[contentArray objectAtIndex:i] isKindOfClass:[NSNull class]]){
            [contentArray replaceObjectAtIndex:i withObject:@""];
        }
    }
    
    return contentArray;
    
}


-(void)startSurveyAction
{
    
    SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
    [self.navigationController pushViewController:objSurveyVC animated:YES];
    
}

-(void)startVisitAction

{
    
    
    //check if customer is blocked
    
    
    
    
    
    
    
    
    
    Singleton *single = [Singleton retrieveSingleton];
    single.planDetailId = @"0";
    if([[customerDetailsDictionary stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
        single.isCashCustomer = @"Y";
        CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customerDetailsDictionary] ;
        [self.navigationController pushViewController:cashCustomer animated:YES];
        cashCustomer = nil;
    }
    else{
        single.isCashCustomer = @"N";
        [[[SWVisitManager alloc] init] startVisitWithCustomer:customerDetailsDictionary parent:self andChecker:@"D"];
    }
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [SWDefaults setCustomer:customerDetailsDictionary];
    if (buttonIndex == 0)
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.planDetailId = @"0";
        if([[customerDetailsDictionary stringForKey:@"Cash_Cust"]isEqualToString:@"Y"]){
            single.isCashCustomer = @"Y";
            CashCutomerViewController *cashCustomer = [[CashCutomerViewController alloc] initWithCustomer:customerDetailsDictionary] ;
            [self.navigationController pushViewController:cashCustomer animated:YES];
            cashCustomer = nil;
        }
        else{
            single.isCashCustomer = @"N";
            [[[SWVisitManager alloc] init] startVisitWithCustomer:customerDetailsDictionary parent:self andChecker:@"D"];
        }
    }
    else if (buttonIndex == 1)
    {
        if([isCollectionEnable isEqualToString:@"Y"])//Enable_Collections
        {
            SWCollectionViewController *collectionViewController = [[SWCollectionViewController alloc] initWithCustomer:customerDetailsDictionary] ;
            [self.navigationController pushViewController:collectionViewController animated:YES];
        }
        else
        {
            SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
        
    }
    else if (buttonIndex == 2)
    {
        SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
        [self.navigationController pushViewController:objSurveyVC animated:YES];
        
    }
}

- (IBAction)launchMap:(id)sender {
    
    
    NSLog(@"customer dict is %@", [customerDetailsDictionary description]);
    //    NSString * googleString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@+%@",lat,lon];
    
    
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        
        
        if ([[customerDetailsDictionary valueForKey:@"Cust_Lat"] isEqual:[NSNull null]]) {
            
        }
        
        else if([[customerDetailsDictionary valueForKey:@"Cust_Lat"] integerValue]>0)
        {
            
            NSString* custLat=[customerDetailsDictionary valueForKey:@"Cust_Lat"];
            NSString* custLong=[customerDetailsDictionary valueForKey:@"Cust_Long"];
            
            
            
            
            NSURL *googleUrl=[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@+%@",custLat,custLong]];
            
            [[UIApplication sharedApplication] openURL:googleUrl];
            
        }
        
        //[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@+%@",lat,lon]
    } else {
        // Use Apple maps
    }
}


-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    
    SWAppDelegate * appDelegate=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    
    double currentLat=appDelegate.currentLocation.coordinate.latitude;
    
    double currentLong=appDelegate.currentLocation.coordinate.longitude;
    
    
    
    
    float custLat=[[customerDetailsDictionary valueForKey:@"Lat"] floatValue];
    float custLong=[[customerDetailsDictionary valueForKey:@"Long"] floatValue];
    
    
    NSString *browserUrlString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%f,%f",   currentLat, currentLong,custLat,custLong];
    
    
    NSString *googleMapsUrlString = [NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f", currentLat, currentLong,custLat,custLong];
    
    BOOL canHandle = [[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]];
    
    if (canHandle) {
        // Google maps installed
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsUrlString]];
        
        
    } else {
        // Use Apple maps?
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:browserUrlString]];
        
    }
    
    
}
@end
