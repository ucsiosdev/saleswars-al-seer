//
//  AES.m
//  HelloPhone
//
//  Created by Harpal Singh on 4/18/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "AES.h"
#import "Base64.h"
#import <CommonCrypto/CommonCryptor.h>

@implementation AES

- (NSString *) encrypt:(NSString *) inputData withKey:(NSString *) key {
    return [Base64 encode:[self transform:kCCEncrypt data:[inputData dataUsingEncoding:NSUTF8StringEncoding] withKey:key]];
}

- (NSString *) decrypt:(NSString *) inputData withKey:(NSString *) key {
    NSData *encryptedData = [Base64 decode:inputData];

    NSData *decryptedData = [self transform:kCCDecrypt data:encryptedData withKey:key];
    
    if(decryptedData)
        return [NSString stringWithUTF8String:[decryptedData bytes]];
    else
        return @"";
}

- (NSData *) transform:(CCOperation) encryptOrDecrypt data:(NSData *) inputData withKey:(NSString *) key { 
    
    NSData* secretKey = [key dataUsingEncoding:NSUTF8StringEncoding];
    
    CCCryptorRef cryptor = NULL;
    CCCryptorStatus status = kCCSuccess;
    
    uint8_t iv[kCCBlockSizeAES128];
    memset((void *) iv, 0x0, (size_t) sizeof(iv));
    
    status = CCCryptorCreate(encryptOrDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                             [secretKey bytes], kCCKeySizeAES256, iv, &cryptor);
    
    if (status != kCCSuccess) {
        return nil;
    }
    
    size_t bufsize = CCCryptorGetOutputLength(cryptor, (size_t)[inputData length], true);
    
    void * buf = malloc(bufsize * sizeof(uint8_t));
    memset(buf, 0x0, bufsize);
    
    size_t bufused = 0;
    size_t bytesTotal = 0;
    
    status = CCCryptorUpdate(cryptor, [inputData bytes], (size_t)[inputData length],
                             buf, bufsize, &bufused);
    
    if (status != kCCSuccess) {
        free(buf);
        CCCryptorRelease(cryptor);
        return nil;
    }
    
    bytesTotal += bufused;
    
    status = CCCryptorFinal(cryptor, buf + bufused, bufsize - bufused, &bufused);
    
    if (status != kCCSuccess) {
        free(buf);
        CCCryptorRelease(cryptor);
        return nil;
    }
    
    bytesTotal += bufused;
    
    NSData* retData = [NSData dataWithBytes:buf length:bytesTotal];
    
    free(buf);
    CCCryptorRelease(cryptor);
    
    return retData; //[NSData dataWithBytesNoCopy:buf length:bytesTotal];
}

@end
