//
//  SurveyViewController.h
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"


@interface SurveyViewControllerOld  : SWViewController <UITableViewDataSource , UITableViewDelegate ,UITextFieldDelegate , UIPopoverControllerDelegate>
{
    NSMutableDictionary *customer;
    UIPopoverController *popoverController;
    UITableView *tableViewController;
    
    NSMutableDictionary *dataDict;
    GenCustListViewController *listView;
    //
    BOOL isCustomer;
    
}


- (id)initWithCustomer:(NSDictionary *)customer;
- (id)initWithOutCustomer;

@end
