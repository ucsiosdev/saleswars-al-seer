//
//  SWOrderHistoryTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/9/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SWOrderHistoryTableViewCell : UITableViewCell

@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * docRefNumberLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * orderStatusLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * orderDateLbl;
@property(strong,nonatomic) IBOutlet SalesWorxSingleLineLabel * orderValueLbl;

@end
