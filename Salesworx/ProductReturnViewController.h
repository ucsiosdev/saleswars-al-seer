//
//  ProductReturnViewController.h
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"


@interface ProductReturnViewController : SWViewController < EditableCellDelegate , UITableViewDelegate , UITableViewDataSource , UIPopoverControllerDelegate , UITextFieldDelegate>
{
    NSMutableDictionary *product;
    GroupSectionFooterView *priceFooterView;
    UITableView *tableView;
    UIPopoverController *collectionTypePopOver;
    NSString *bonusString;
    BOOL isSaveOrder;
    BOOL isPopover;
    int bonus;
    //       id target;
    SEL action;
    
    UIView *myBackgroundView;
}
//@property (nonatomic, strong) ProductService *serProduct;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


- (id)initWithProduct:(NSDictionary *)product;

@end
