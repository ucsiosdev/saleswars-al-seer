//
//  SalesOrderViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "ProductOrderViewController.h"
#import "OrderAdditionalInfoViewController.h"

@protocol ChildViewControllerDelegate <NSObject>
- (void)selectedProduct:(NSMutableDictionary *)product;
@end

@interface SalesOrderViewController : SWGridViewController <UIPopoverControllerDelegate>
{
    
    //
    float totalOrderAmount;
    float totalDiscountAmount;
    BOOL isManageOrder;
    double price ;
    double discountAmt ;
    
    NSMutableDictionary *customer;
    NSDictionary *category;
    UILabel *totalPriceLabel;
    double totalPriceforLbl;
    
    double testPriceforLbl;
    double test1Price;
    
    
    
    
    NSMutableDictionary *selectedOrder;
    NSString *manageOrder;
    NSArray *categoryArray;
    NSString *savedCategoryID;
    NSMutableDictionary *productDict;
    ProductListViewController *productListViewController;
    UINavigationController *navigationControllerS;
    ProductOrderViewController *orderViewController;
    UINavigationController *navigationControllerR;
    OrderAdditionalInfoViewController *orderAdditionalInfoViewController;
    NSMutableDictionary *dataItemArray;
    NSMutableDictionary *p;
    NSMutableDictionary *b;
    NSMutableDictionary *f;
    NSMutableArray *arrayTemp;
    NSString *title;
    NSString *amountString;
    
    NSMutableDictionary *deleteRowDict;
    NSMutableArray *tempSalesArray;
    
    NSString *ENABLE_TEMPLATES;
    UIPopoverController *popoverController;
    NSString *name_TEMPLATES;
    UIButton *saveButton;
    UIButton *saveAsTemplateButton;
    UIButton *mslOrderItem,*mslOrderItem1;
    
    UILabel *totalProduct;
    
    
}
@property (unsafe_unretained) id <ChildViewControllerDelegate> delegate;

- (void) productAdded:(NSMutableDictionary *)q;
- (id)initWithCustomer:(NSDictionary *)c andCategory:(NSDictionary *)category;
- (id)initWithCustomer:(NSDictionary *)c andOrders:(NSDictionary *)order andManage:(NSString *)manage;
- (id)initWithCustomer:(NSDictionary *)c andTemplate:(NSDictionary *)order andTemplateItem:(NSArray *)orderItems ;
- (void)updateTotal ;
- (void) dbGetSalesOrderServiceDiddbGetConfirmOrderItems:(NSArray *)performaOrder;
@property(strong,nonatomic)NSMutableArray * wareHouseDataSalesOrder;

@property(strong,nonatomic)NSMutableArray * wareHouseDataOldSalesArray;

@end
