//
//  SWPlatformDatabaseConstants.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#ifndef SWPlatform_SWPlatformDatabaseConstants_h
#define SWPlatform_SWPlatformDatabaseConstants_h

#define kSQLLogin   @"SELECT * FROM TBL_User WHERE Username = '%@' and Password = '%@'"
#define kSQLCustomerListAll   @"SELECT * FROM TBL_Customer ORDER BY Customer_Name Where "
#define kSQLCashCustomerListAll @"SELECT * FROM TBL_Customer Where Cash_Cust = 'Y' ORDER BY Customer_Name "

#define kSQLCustomerListNonAlpha @"SELECT A.*,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone, B.Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, B.Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM %@ AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID  %@ ORDER BY A.Customer_Name"

#define kSQLRoute @"SELECT C.*,A.FSR_Plan_Detail_ID,A.Planned_Visit_ID , A.Visit_Date, A.Start_Time AS Visit_Start_Time, A.End_Time AS Visit_End_Time,A.Visit_Status, B.Customer_Name,B.Customer_No, B.Cust_Lat, B.Cust_Long,B.Customer_ID AS [Ship_Customer_ID] , B.Site_Use_ID AS [Ship_Site_Use_ID] FROM  TBL_FSR_Planned_Visits AS A INNER JOIN TBL_Customer_Ship_Address  AS B ON A.Customer_ID = B.Customer_ID  AND A.Site_Use_ID=B.Site_Use_ID INNER JOIN TBL_Customer AS C ON C.Customer_ID = B.Customer_ID WHERE A.Visit_Date LIKE  '%%%@%%' ORDER BY A.Start_Time ASC , C.Customer_Name ASC"


/** Visits*/
#define K_BS_SQLdbGetVisits @"select * from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'"

/** Orders*/
#define K_BS_SQLdbGetOrders @"select * from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetOrderLineItems @"select * from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
#define K_BS_SQLdbGetOrderLots @"select * from TBL_Order_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"


/** Order History*/
#define K_BS_SQLdbGetOrderHistory @"select * from TBL_Order_History where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetOrderHistoryLineItems @"select * from TBL_Order_History_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from TBL_Order_History where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"


/** Distribution check**/
#define K_BS_SQLdbGetDistributionCheck @"select * from TBL_Distribution_Check where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetDistributionCheckItems @"select * from TBL_Distribution_Check_Items where DistributionCheck_ID in( select DistributionCheck_ID from TBL_Distribution_Check where Visit_ID in(select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"


/* order delete methods*/
#define K_BS_SQLdbDeleteOrders @"Delete from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')"
#define K_BS_SQLdbGetDeleteOrderLineItems @"Delete from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@'))"
#define K_BS_SQLdbGetDeleteOrderLots @"Delete from TBL_Order_Lots where Orig_Sys_Document_Ref in (select Orig_Sys_Document_Ref from TBL_Order_Line_Items where Orig_Sys_Document_Ref in ( select Orig_Sys_Document_Ref from tbl_order where Visit_ID in( select Actual_Visit_ID from TBL_FSR_Actual_Visits where Visit_End_Date > '%@'  AND Visit_End_Date < '%@')))"

#define kSQLCUstomerLocationFilter @"SELECT City FROM TBL_Customer GROUP BY City"

#define kSQLCustomerTargetGeneral @"SELECT DISTINCT B.Item_No As [Category], A.Target_Value,  A.Custom_Value_1 As [FSR_Target],   A.Custom_Value_2 As [Sales_Value],    0.00 As [Achievement]    FROM TBL_Sales_Target_Items As A INNER JOIN TBL_Product As B ON      A.Classification_1=B.Agency WHERE A.Classification_2='%@' AND A.Month=%d"


#define kSQLCustomerTargetAlseer @"SELECT DISTINCT  Classification_1 As [Category], Target_Value,  Custom_Value_1 As [FSR_Target],   Sales_Value As [Sales_Value],    0.00 As [Achievement]    FROM TBL_Sales_Target_Items WHERE Classification_2='%@' AND Month=%d"


#define kSQLCustomerOrderHistory @"SELECT A.Orig_Sys_Document_Ref, A.Creation_Date, A.Transaction_Amt, CASE WHEN A.ERP_Status='C' THEN 'Invoiced' WHEN A.ERP_Status='S' THEN 'Posted' ELSE 'Pending' END As [ERP_Status], B.Customer_Name ,(SELECT C.Username FROM TBL_User AS C WHERE C.SalesRep_ID=A.Created_By) As [FSR] FROM TBL_Order_History As A INNER JOIN TBL_Customer_Ship_Address As B ON B.Customer_ID=A.Ship_To_Customer_ID AND B.Site_Use_ID=A.Ship_To_Site_ID WHERE A.Doc_Type='I' AND B.Customer_No = '%@' ORDER BY A.Orig_Sys_Document_Ref DESC"
/////////////////

#define kSQLCustomerPriceList @"SELECT C.Description , B.Item_UOM , B.Unit_Selling_Price,B.Unit_List_Price,B.Is_Generic, C.Item_Code FROM TBL_Customer AS A INNER JOIN TBL_Price_List AS B ON A.Price_List_ID = B.Price_List_ID AND A.Site_Use_ID = B.Organization_ID INNER JOIN TBL_Product AS C ON B.Inventory_Item_ID = C.Inventory_Item_ID WHERE  A.Customer_No = '%@' %@"


#define  kSQLCustomerPriceListGeneric @"SELECT C.Description , B.Item_UOM , B.Unit_Selling_Price,B.Unit_List_Price,B.Is_Generic, C.Item_Code FROM TBL_Customer AS A  , TBL_Price_List AS B INNER JOIN TBL_Product AS C ON B.Inventory_Item_ID = C.Inventory_Item_ID WHERE  A.Customer_No = '%@' AND B.Is_Generic = 'Y' %@"

#define  kSQLProductDetail @" SELECT IFNULL(A.EANNO,'N/A') AS BarCode,A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM], A.Discount, A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, SUM(CAST((C.Lot_Qty/B.Conversion) AS INTEGER)) As Lot_Qty, MIN(C.Expiry_Date) As Expiry_Date , A.Control_1 FROM TBL_Product As A INNER JOIN TBL_Item_UOM As B ON A.Item_Code=B.Item_Code AND A.Primary_UOM_Code=B.Item_UOM LEFT JOIN TBL_Product_Stock As C ON A.Inventory_Item_ID=C.Item_ID WHERE A.Inventory_Item_ID=%@ AND A.Organization_ID=%@ GROUP BY A.Inventory_Item_ID, A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code, A.Discount, A.Net_Price, A.List_Price, A.Control_1"


#define kSQLProductDetailWithUOM @"SELECT A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.EANNO, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM], A.Discount, A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, SUM(CAST((C.Lot_Qty/B.Conversion) AS INTEGER)) As Lot_Qty, MIN(C.Expiry_Date) As Expiry_Date , A.Control_1 FROM TBL_Product As A INNER JOIN TBL_Item_UOM As B ON A.Item_Code=B.Item_Code  LEFT JOIN TBL_Product_Stock As C ON A.Inventory_Item_ID=C.Item_ID WHERE A.Inventory_Item_ID=%@ AND A.Organization_ID=%@ and B.Item_UOM= '%@' GROUP BY A.Inventory_Item_ID, A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code, A.Discount, A.Net_Price, A.List_Price, A.Control_1"



#define kSQLProductDetailwithUOM @"SELECT A.Inventory_Item_ID,A.Category, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code As [UOM], A.Discount, A.Net_Price As Unit_Selling_Price, A.List_Price As Unit_List_Price, SUM(C.Lot_Qty) As Lot_Qty, MIN(C.Expiry_Date) As Expiry_Date , A.Control_1 FROM TBL_Product As A LEFT JOIN TBL_Product_Stock As C ON A.Inventory_Item_ID=C.Item_ID WHERE A.Inventory_Item_ID=%@ AND A.Organization_ID=%@ GROUP BY A.Inventory_Item_ID, A.Organization_ID, A.Item_Code, A.Description, A.Brand_Code, A.Item_Size, A.Primary_UOM_Code, A.Discount, A.Net_Price, A.List_Price"


#define kSQLOrderInvoiceItems @"SELECT A.Inventory_Item_ID, B.Description, A.Order_Quantity_UOM,  A.Ordered_Quantity, A.Calc_Price_Flag, A.Unit_Selling_Price, B.Item_Code FROM TBL_Order_History_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLOrderInvoice @"SELECT A.Inventory_Item_ID, B.Description,A.Expiry_Date, A.Order_Quantity_UOM,  A.Ordered_Quantity, A.Calc_Price_Flag, A.Unit_Selling_Price,A.Item_Value, B.Item_Code FROM TBL_Sales_History_Line_Items As A INNER JOIN TBL_Product As B ON A.Inventory_Item_ID=B.Inventory_Item_ID WHERE A.Orig_Sys_Document_Ref='%@' ORDER BY A.Line_Number ASC"

#define kSQLOrderAllInvoices @"SELECT CAST('0' AS Bit) AS Selected, C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt, '0' AS Amount, C.Invoice_Date AS InvDate, C.Due_Date as DueDate,C.Customer_ID  FROM TBL_Open_Invoices AS C  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID   LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No   WHERE (C.Customer_ID={0})  UNION ALL  SELECT CAST('0' AS Bit) AS Selected, B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt, '0' AS Amount, B.Creation_Date AS InvDate,  B.Due_Date as DueDate,B.Inv_To_Customer_ID AS Customer_ID  FROM TBL_Sales_History AS B  LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID  LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No   WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID={0}) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices WHERE Customer_ID={0}) ORDER BY InvDate ASC"

#define kSQLCollectionInsertInfo @"INSERT INTO TBL_Collection(Collection_Id,Collection_Ref_No, Collected_On,Collected_By,Collection_Type,Customer_Id,Site_Use_Id,Amount,Cheque_No,Cheque_Date,Bank_Name,Bank_Branch,Emp_Code,Status,Print_Status,Sync_TimeStamp)VALUES ('{0}', '{1}', '{2}', {3}, '{4}', {5}, {6}, {7}, '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}')"

#define kSQLCollectionInsertInvoice @"INSERT INTO TBL_Collection_Invoices(Collection_ID, Invoice_No, Invoice_Date, Amount, ERP_Status, Sync_TimeStamp, Collection_Inv_ID) VALUES ('{0}', '{1}', '{2}', {3}, '{4}','{5}', '{6}')"

#define kSQLProductListNonAlphabet @"SELECT  A.Item_Code,A.Category,A.Brand_Code, A.Description,CASE WHEN C.Inventory_Item_ID IS NULL THEN 'N'	ELSE 'Y'END As IsMSL,CASE WHEN C.Inventory_Item_ID IS NULL THEN '0'	ELSE '1'END As Sts,A.Inventory_Item_ID AS ItemID,A.Organization_ID AS OrgID FROM TBL_Product AS A LEFT OUTER JOIN TBL_Product_MSL As C ON A.Inventory_Item_ID=C.Inventory_Item_ID %@ ORDER BY A.Description"


#define kSQLProductStock @"SELECT Org_ID, CAST(Lot_Qty AS INTEGER) As [Lot_Qty], Lot_No, Expiry_Date, Stock_ID FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY Org_ID ASC, Expiry_Date ASC"

#define kSQLProductBonus @"SELECT A.Prom_Qty_From, A.Prom_Qty_To, A.Price_Break_Type_Code, A.Get_Item, A.Get_Qty ,A.Get_Add_Per ,B.Description  FROM TBL_BNS_Promotion AS A INNER JOIN TBL_Product AS B ON A.Get_Item = B.Item_Code   WHERE   A.Item_Code='%@' ORDER BY A.Prom_Qty_From ASC"


#define KSWLProductTarget @"select Item_No As [Category], Target_Value, Custom_value_1 As [FSR_Target],Custom_value_2 [Sales_Value],0.00 As [Achievement],Target_Type from TBL_Product,TBL_Sales_target_items,TBL_Sales_Target where TBL_Sales_Target_Items.Sales_Target_ID= TBL_Sales_Target.Sales_Target_ID and TBL_Sales_Target_Items.Classification_1=TBL_Product.Category and TBL_Sales_Target_Items.Classification_1='%@' and TBL_Sales_Target_Items.Month='%d' and TBL_Sales_Target.Target_Type='C' "

#define kSQLProductFilterBrand @"SELECT {0}, Count({0}) as Count FROM TBL_Product GROUP BY {0}"

#define kSQLProductCategories @"SELECT DISTINCT A.Category, C.Description As [Description], B.Org_ID, A.Item_No FROM TBL_Product As A INNER JOIN TBL_Customer_Stock_Map As B ON A.Category=B.Product_Category AND B.Customer_ID=%@ AND B.Site_Use_ID=%@ INNER JOIN TBL_Org_Info As C ON B.Org_ID=C.Org_ID ORDER BY B.Org_ID, A.Item_No ASC"


#define kSQLProductListFromCategoriesNA @"SELECT A.Item_Code,A.Brand_Code, A.Description, A.Inventory_Item_ID AS ItemID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,  SUM(B.Lot_Qty) As Lot_Qty, MIN(B.Expiry_Date) As Expiry_Date ,A.Organization_ID AS OrgID ,A.Agency FROM TBL_Product As A LEFT  JOIN TBL_Product_Stock As B ON A.Inventory_Item_ID=B.Item_ID AND B.Org_ID='%@' WHERE A.Category='%@'   GROUP BY A.Item_Code, A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code, A.Control_1, A.Discount, A.List_Price, A.Net_Price,A.Agency,A.Organization_ID ORDER BY A.Description"

#define kSQLFetchAllCategoriesWithProducts @"SELECT A.Item_Code, A.Description,IFNULL(A.Agency,'N/A') AS Agency,SUM(CAST(IFNULL(B.Custom_Attribute_1,'0') As DECIMAL(18,4))) AS Blocked_Stock,SUM(CAST(IFNULL(B.Custom_Attribute_2,'0') As DECIMAL(18,4))) AS QC_Stock,SUM(IFNULL(B.Lot_Qty,'0')) As Lot_Qty FROM TBL_Product As A INNER  JOIN TBL_Product_Stock As B ON A.Inventory_Item_ID=B.Item_ID GROUP BY A.Item_Code , A.Agency, A.Description ORDER BY A.Description"


#define kSQLDistributionCollection @"SELECT A.Description, A.Inventory_Item_ID, A.Primary_UOM_Code ,Item_Code,A.Organization_ID  FROM TBL_Product_MSL AS M  INNER JOIN TBL_Product A ON M.Inventory_Item_ID=A.Inventory_Item_ID AND M.Organization_ID=A.Organization_ID ORDER BY Item_Code ASC"


#endif
