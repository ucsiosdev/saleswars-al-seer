//
//  StockInfoSalesHistoryViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/29/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductStockViewController.h"



@protocol stockInfoUpdatedDelegate <NSObject>

-(void) updateMainProductDict:(NSMutableDictionary *)mainProduct;


@end


@interface StockInfoSalesHistoryViewController : UIViewController<ProductStockViewControllerDelegate,ProductStockViewDelegate,GridViewDelegate,UITableViewDataSource,UITableViewDelegate>




{
    UIScrollView *stockInfoScrollView;
    
    BOOL isStockToggled;
    ProductStockViewController *stockViewController;
    
    NSMutableArray* productStockArray;
    NSMutableDictionary *productDict,*priceDict;
    
    NSString* totalStock;
    
    NSInteger conversionRate;

    
}

@property(strong,nonatomic)UIPopoverController* popOver;

@property (nonatomic, assign) id<stockInfoUpdatedDelegate> customDelegate;

@property(strong,nonatomic)NSMutableDictionary* mainProductDictionary,*customerDictionary;

-(void)updateStockScrollContent:(CGSize)contentSize;

@property(nonatomic)NSInteger stockInfoSize;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

//@property (strong, nonatomic) IBOutlet UIView *salesHistoryView;
//@property (strong, nonatomic) IBOutlet UIView *stockInfoView;
@property (strong, nonatomic) IBOutlet UITableView *stockTblView;



@end
