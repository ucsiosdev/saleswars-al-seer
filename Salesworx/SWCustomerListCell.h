//
//  SWCustomerListCell.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWCustomerListCell : UITableViewCell {
    UIImageView *statusImageView;
    UILabel *codeLabel;

}

@property (nonatomic, strong) UIImageView *statusImageView;
@property (nonatomic, strong) UILabel *codeLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)applyAttributes:(NSDictionary *)row;
@end
