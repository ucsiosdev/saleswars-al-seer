//
//  MenuViewController.h
//  Salesworx
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "AboutUsViewController.h"
#import "DashboardAlSeerViewController.h"
#import "SWAppDelegate.h"

@interface MenuViewController : UIViewController <UITableViewDelegate, UITableViewDataSource , SynViewControllerDelegate> {
    UITableView *tableView;
    NSArray *modules;
    SynViewController *synch;
    
    SWLoadingView *loadingView;
    NSString *dateString;
    NSString *isCollectionEnable;
    
    SettingViewController *settingVController;
    AboutUsViewController *aboutVController;
    DashboardAlSeerViewController *dashBViewController;
    DashboardAlSeerViewController* alseerDashBoard;
    SWAppDelegate *app;
    
    UIViewController *CommunicationViewControllerNew1,*SurveyFormViewController1,*SWDashboardViewController1,*SWRouteViewController1,*SWProductListViewController1,*SWCustomerListViewController1,*ReportViewController1,*SWCollectionCustListViewController1,*newSalesOrder1,*dashboardAlSeerViewController1;
    IBOutlet UILabel *appVersionLabel;
    IBOutlet UITableView *listTableView;
}
@property (nonatomic, assign) NSInteger openSectionIndex;

@end
