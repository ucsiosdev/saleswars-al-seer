//
//  UITableViewCell+Responder.h
//  SWFoundation
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Responder)

- (void)resignResponder;

@end