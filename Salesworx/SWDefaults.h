//
//  SWDefaults.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWDefaults : NSObject

#define KDataUploadProcess_FullSync @"Full Sync"
#define KDataUploadProcess_SendOrders @"Send Orders"
#define KDataUploadProcess_VTRXBS @"V_Trx_BS"
#define KDataUploadProcess_None @"None"

#define KENABLE_V_TRX_BS_NOTIFICATIONNameStr @"ENABLE_V_TRX_BS_NOTIFICATION"
#define KDISABLE_V_TRX_BS_NOTIFICATIONNameStr @"DISABLE_V_TRX_BS_NOTIFICATION"

#define kUsernameKey    @"login"
#define kPasswordKey    @"password"
#define kLocationFilterCustomerList @"location_filter_customerlist"
#define kFilterCustomerList @"filter_customerlist"
#define kLastCollectionRef  @"LastCollectionReference"
#define kPaymentFilterForCustomerList @"Payment_filter_customerlist"
#define kStatusFilterForCustomerList @"Status_filter_customerlist"
#define kNameFilterForCustomerList @"Name_filter_customerlist"
#define kCodeFilterForCustomerList @"Code_filter_customerlist"
#define kBarCodeFilterForCustomerList @"BarCode_filter_customerlist"
#define kProductBrandFilter     @"ProductBrandFilter"
#define kProductNameFilter     @"ProductNameFilter"
#define kProductProductIDFilter     @"ProductProductIDFilter"
#define kUserProfile    @"UserProfile"
#define kUD_IS_CUST_LIST_FILTER_ON    @"UD_IS_CUST_LIST_FILTER_ON"
#define kUD_IS_PRODUCT_LIST_FILTER_ON    @"UD_IS_PRODUCT_LIST_FILTER_ON"
#define kUD_IS_MENU_SURVEY_LIST_FILTER_ON    @"UD_IS_MENU_SURVEY_LIST_FILTER_ON"

#define kDistributionCheckTitle @"DISTRIBUTION CHECK"
#define kSalesOrderTitle @"SALES ORDER"
#define kManageOrdersTitle @"MANAGE ORDERS"
#define kOrderHistoryTitle @"ORDER HISTORY"
#define kReturnsTitle @"RETURNS"
#define kCollectionTitle @"COLLECTION"
#define kSurveyTitle @"SURVEY"
#define kManageOrdersScreenTitle @"Manage Orders"
#define kReturnsInitializationScreenTitle @"Returns Initialization"
#define kTodoTitle @"To-Do"
#define KVisitOtionCompletionNotificationNameStr @"VisitOtionCompletionNotification"
#define KVisitOtionNotification_DictionaryKeyStr @"VisitOptionName"

#define  KAlertOkButtonTitle @"Ok"
#define  KAlertYESButtonTitle @"Yes"
#define  KAlertNoButtonTitle @"No"
#define  KAlertCancelButtonTitle @"Cancel"
#define KMissingData @"Missing Data"
#define KTitleStrAlert @"Alert"
#define KWarningAlertTitleStr @"Warning"
#define KNoMatchesStr @"No Matches"


#define kTextFieldCharacterLimit @"50"

#define KEnglishLocaleStr @"en_US"
#define KNotApplicable @"N/A"
#define kPushNotificationTitle @"Push Notification"


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define NavigationBarButtonItemFont [UIFont fontWithName:@"WeblySleekUISemibold" size:14.0]


#define  UIViewControllerBackGroundColor [UIColor colorWithRed:(240.0/255.0) green:(240.0/255.0) blue:(240.0/255.0) alpha:1]

#define UITableviewUnSelectedCellBackgroundColor [UIColor whiteColor]
#define MedRepUITableviewSelectedCellBackgroundColor [UIColor colorWithRed:(255.0/255.0) green:(238.0/255.0) blue:(203.0/255.0) alpha:1]
#define KTableViewSectionExpandColor [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]
#define kCompletedColor [UIColor colorWithRed:(39.0/255.0) green:(195.0/255.0) blue:(165.0/255.0) alpha:1]

#define SalesWarsMenuTitleFontColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]


#define UITableviewSelectedCellBackgroundColor [UIColor colorWithRed:(81.0/255.0) green:(102.0/255.0) blue:(136.0/255.0) alpha:1]


#define SalesWarsMenuTitleDescFontColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]

//#define headerTitleFont [UIFont fontWithName:@"OpenSans-Semibold" size:20.0]


#define headerFont [UIFont fontWithName:@"WeblySleekUISemibold" size:14.0]

#define bodyFont [UIFont fontWithName:@"OpenSans" size:12.0]

#define bodySemiBold [UIFont fontWithName:@"OpenSans-Semibold" size:14.0]

#define headerTitleFont [UIFont fontWithName:@"WeblySleekUISemibold" size:16.0]
#define UIViewBackGroundColor [UIColor colorWithRed:(225.0/255.0) green:(232.0/255.0) blue:(237.0/255.0) alpha:1]
#define TableViewHeaderSectionColor [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];


#define KDCImagesFolderName @"Distribution check images"
#define KJPEGMIMEFormat @"image/jpeg"



#define KNewSyncNotificationNameStr @"NewSyncStatusNotification"



#define textFont [UIFont fontWithName:@"OpenSans" size:14.0]

#define MedRepElementTitleLabelFontColor [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1]

#define MedRepElementTitleLabelFont [UIFont fontWithName:@"WeblySleekUISemibold" size:14]

#define kSalesReportPerAgencyHeaderTitleFont [UIFont fontWithName:@"WeblySleekUISemibold" size:16]
#define kSalesReportPerAgencyHeaderTitleColor [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]

#define MedRepDescriptionLabelFontColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]


#define kUITableViewSaperatorColor [UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1]


#define kUITableViewBorderColor [UIColor colorWithRed:(182.0/255.0) green:(200.0/255.0) blue:(209.0/255.0) alpha:1]

#define kUIElementDividerBackgroundColor  [UIColor colorWithRed:(227.0/255.0) green:(228.0/255.0) blue:(230.0/255.0) alpha:1]




#define fontColor [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1]

#define kUITableViewBorderColor [UIColor colorWithRed:(182.0/255.0) green:(200.0/255.0) blue:(209.0/255.0) alpha:1]

#define MedRepElementDescriptionLabelFont [UIFont fontWithName:@"WeblySleekUISemibold" size:16]

#define  SalesWorxFilterTitleFont [UIFont fontWithName:@"OpenSans" size:19]

#define SalesWorxFilterTableViewCellTitleColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

#define HAS_DECIMALS(x) (x != (int)x)

#define KAppControlsYESCode @"Y"
#define KAppControlsNOCode @"N"
#define kUITableViewHeaderBackgroundColor [UIColor colorWithRed:(248.0/255.0) green:(250.0/255.0) blue:(252.0/255.0) alpha:1]

#define kHeaderbackgroundColor [UIColor colorWithRed:(240.0/255.0) green:(242.0/255.0) blue:(246.0/255.0) alpha:1]


#define kDistributionCheckedAlertTitle @"Distribution Check Completed"

/* Distribution Check */
#define KDistributionCheckAvailabilityColor [UIColor colorWithRed:(17.0/255.0) green:(176.0/255.0) blue:(101.0/255.0) alpha:1]
#define KDistributionCheckUnAvailabilityColor [UIColor redColor]


#define kVisitOptionsBorderColor [UIColor colorWithRed:(60.0/255.0) green:(146.0/255.0) blue:(202.0/255.0) alpha:1]

#define FilterButtonFont [UIFont fontWithName:@"WeblySleekUISemibold" size:15]
#define kFilterButtonBorderColor [UIColor colorWithRed:(209.0/255.0) green:(209.0/255.0) blue:(209.0/255.0) alpha:1]


#define SalesWorxReadOnlyTextFieldColor [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(250.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextFieldTextColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextViewColor [UIColor colorWithRed:(248.0/255.0) green:(249.0/255.0) blue:(250.0/255.0) alpha:1.0]
#define SalesWorxReadOnlyTextViewTextColor [UIColor colorWithRed:(105.0/255.0) green:(114.0/255.0) blue:(128.0/255.0) alpha:1.0]


#define KBackButtonArrowImageName @"back_btn.png"

#define kReviewDocumentsTitle @"Review Documents"
#define kReviewDocumentsDescription @"View all transactions created for all customers or filter by a single customer for selected date range."

#define kBlockedCustomersTitle @"Blocked Customers"
#define kBlockedCustomersDescription @"Check details of blocked customers."

#define kCustomerStatementTitle @"Customer Statement"
#define kCustomerStatementDescription @"Review invoices and payment details for customers."

#define kPaymentSummaryTitle @"Payment Summary"
#define kPaymentSummaryDescription @"Review payment collected by type and date range."

#define kSalesSummaryTitle @"Sales Summary"
#define kSalesSummaryDescription @"View sales data by date, customer type and sales document type."

#define kStockReportTitle @"Stock Report by Agency"
#define kStockReportDescription @"View product stock by agency"

#define kSalesReportTitle @"Sales Per Agency"
#define kSalesReportDescription @"View comparison of year to date and month to date sales date with last year"

+(NSString*)applicationDocumentsDirectory;

+(UILabel*)fetchTitleView:(NSString*)title;

+ (void)initializeDefaults;

+ (NSString *)username;
+ (void)setUsername:(NSString *)username;

+ (NSString *)password;
+ (void)setPassword:(NSString *)password;

+ (NSString *)lastPerformaOrderReference;
+ (void)setLastPerformaOrderReference:(NSString *)ref;
+(void)ClearLoginCredentials;

+ (NSString *)lastOrderReference;
+ (void)setLastOrderReference:(NSString *)ref;

+ (NSString *)lastCollectionReference;
+ (void)setLastCollectionReference:(NSString *)ref;

+ (NSString *)lastReturnOrderReference ;
+ (void)setLastReturnOrderReference:(NSString *)ref;

+ (NSDictionary *)userProfile;
+ (void)setUserProfile:(NSDictionary *)array;

+ (NSString *)activationStatus ;
+ (void)setActivationStatus:(NSString *)ref;

+ (NSString *)lastSyncDate ;
+ (void)setLastSyncDate:(NSString *)ref;

+ (NSString *)lastSyncStatus ;
+ (void)setLastSyncStatus:(NSString *)ref;

+ (NSString *)lastSyncType ;
+ (void)setLastSyncType:(NSString *)ref;

+ (NSString *)lastSyncOrderSent ;
+ (void)setLastSyncOrderSent:(NSString *)ref;

+ (NSDictionary *)licenseKey ;
+ (void)setLicenseKey:(NSDictionary *)ref;

+ (NSString *)usernameForActivate ;
+ (void)setUsernameForActivate:(NSString *)ref;

+ (NSString *)passwordForActivate;
+ (void)setPasswordForActivate:(NSString *)ref;

+ (NSString *)isActivationDone;
+ (void)setIsActivationDone:(NSString *)ref;

+ (NSString *)serverArray ;
+ (void)setServerArray:(NSString *)ref;

+ (NSString *)selectedServerDictionary ;
+ (void)setSelectedServerDictionary:(NSString *)ref;

+ (NSString *)licenseCustomerID;
+ (void)setLicenseCustomerID:(NSString *)ref;

+ (NSArray *)appControl;
+ (void)setAppControl:(NSArray *)array;

+ (NSString *)versionNumber;
+ (void)setVersionNumber:(NSString *)ref;

+ (NSString *)licenseIDForInfo;
+ (void)setLicenseIDForInfo:(NSString *)ref;

+ (NSString *)lastFullSyncDate;
+ (void)setLastFullSyncDate:(NSString *)ref;

+ (NSString *)lastVTRXBackGroundSyncDate;
+ (void)setLastVTRXBackGroundSyncDate:(NSString *)ref;

+(BOOL)isVTRXBackgroundSyncRequired;

-(NSString *)getAppVersionWithBuild;
+(NSDate*)convertNSStringtoNSDate:(NSString*)dateString;
//////////////////////////////////

+ (NSMutableDictionary *)customer;
+ (void)setCustomer:(NSMutableDictionary *)customerDict;
+ (void)clearCustomer;

+ (NSString *)locationFilterForCustomerList;
+ (void)setLocationFilterForCustomerList:(NSString *)filter;

+ (NSString *)filterForCustomerList;
+ (void)setFilterForCustomerList:(NSString *)cus_filter;

+ (NSString *)statusFilterForCustomerList;
+ (void)setStatusFilterForCustomerList:(NSString *)status_filter;

+ (NSString *)paymentFilterForCustomerList;
+ (void)setPaymentFilterForCustomerList:(NSString *)payment_filter;

+ (NSString *)barCodeFilterForCustomerList;
+ (void)setbarCodeFilterForCustomerList:(NSString *)barcode_filter;

+ (NSString *)codeFilterForCustomerList;
+ (void)setCodeFilterForCustomerList:(NSString *)code_filter;

+ (NSString *)nameFilterForCustomerList;
+ (void)setNameFilterForCustomerList:(NSString *)name_filter;

+ (NSString *)filterForProductList;
+ (void)setFilterForProductList:(NSString *)pro_filter;

+ (NSString *)productFilterBrand;
+ (void)setProductFilterBrand:(NSString *)value;

+ (NSString *)productFilterName;
+ (void)setProductFilterName:(NSString *)value;

+ (NSString *)productFilterProductID;
+ (void)setProductFilterProductID:(NSString *)value;

+ (NSString *)paymentType;
+ (void)setPaymentType:(NSString *)value;

+ (NSString *)currentVisitID;
+ (void)setCurrentVisitID:(NSString *)visit_ID;

+ (void)clearCurrentVisitID;

+(void)clearSignature;


+ (NSString *)orderAmount;

+ (NSString *)pdc_due;

+ (NSString *)Credit_Period;

+ (NSString *)Over_Due;

+ (void)setOrderAmount:(NSString *)orderAmount CreditLimit:(NSString *)credit_Limit PDCDUE:(NSString *)pdc_due Credit_Limit:(NSString *)credit_period OverDues:(NSString *)over_due;

+ (NSString *)availableBalance;
+ (void)setAvailableBalance:(NSString *)avl_bal;

+ (NSDictionary *)productCategory;
+ (void)setProductCategory:(NSDictionary *)category;
+ (void)clearProductCategory;

+ (int)collectionCounter;
+ (void)setCollectionCounter:(int)ref;

+ (NSString *)startDayStatus;
+ (void)setStartDayStatus:(NSString *)ref;

+ (NSMutableDictionary *)validateAvailableBalance:(NSArray *)orderAmmount;

+ (void)clearEverything;
+(NSString*)checkTgtField;
+(NSString*)formatWithThousandSeparator:(NSInteger)number;

+(NSString*)checkTargetSalesDashBoard;
+(NSString*)checkMultiUOM;
+(NSString*)formatStringWithThousandSeparatorString:(NSString*)str;


+(NSString*)fetchDashboardType;

+(NSString*)formatStringWithThousandSeparator:(NSInteger)number;

+(void)setSignature:(id)signatureData;
+(id)fetchSignature;


+(void)hideDistributionCheckDate:(BOOL)option;
+(BOOL)fetchDistributionDateStatus;


+(void)setOnsiteVisitStatus:(BOOL)option;
+(BOOL)fetchOnsiteVisitStatus;


+(NSString*)fetchDistributionSaveStatus :(NSString*)customerID;
+(void)setDistributionSaveStatus:(NSDictionary*)parametersDict;

+(UIView*)createNavigationBarTitleView:(NSString*)title;

+(NSDictionary*)fetchBarAttributes;


+(void)isDCOptionalforOnsiteVisit:(BOOL)status;
+(BOOL)fetchDCOptionalStatusforOnsiteVisit;

+ (CGSize)screenSize;

+(NSString *)getValidStringValue:(id)inputstr;


+ (BOOL)isEmpty:(NSString *)string;
+(void) showAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage withController:(UIViewController *)viewController;
+(void) ShowConfirmationAlertAfterHidingKeyBoard :(NSString *)alertTitle andMessage :(NSString *) alertMessage andActions:(NSMutableArray *)actionsArray withController:(UIViewController *)viewController;

+(NSString*)fetchCurrentDateTimeinDatabaseFormat;
+(NSString*)fetchDatabaseDateFormat;
+(NSString*)convertNSDatetoDataBaseDateTimeFormat:(NSDate *)inputDate;
+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate;

-(void)AddBackButtonToViewcontroller:(UIViewController *)viewController;

+(NSPredicate*)fetchMultipartSearchPredicate:(NSString*)searchString withKey:(NSString*)keyString;
+(void)UpdateGoogleAnalyticsforScreenName:(NSString*)screenName;


+ (void)redirectLogToDocuments;
+(NSString*)fetchDeviceLogsFolderPath;
+(void)deleteOldDeviceLogFiles;


@end
