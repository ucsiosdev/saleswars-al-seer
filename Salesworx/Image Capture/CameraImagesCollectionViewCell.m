//
//  CameraImagesCollectionViewCell.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "CameraImagesCollectionViewCell.h"

@implementation CameraImagesCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CameraImagesCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    
    return self;
    
}


@end
