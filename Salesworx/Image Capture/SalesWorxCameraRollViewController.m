//
//  SalesWorxCameraRollViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxCameraRollViewController.h"
#import "CameraImagesCollectionViewCell.h"
#import "SalesWorxCustomClass.h"
#import "CategoryTableViewCell.h"
#import "ProductTableViewCell.h"
#import "SalesWorxPopOverViewController.h"

@interface SalesWorxCameraRollViewController ()

@end

@implementation SalesWorxCameraRollViewController


@synthesize customerDictornary;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Visit Images"];
    [imagesCollectionView registerClass:[CameraImagesCollectionViewCell class] forCellWithReuseIdentifier:@"cameraImageCell"];
    customerNameLbl.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_Name"]];
    customerNumberLbl.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_No"]];
    double availBal = [[customerDictornary stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [imagesCollectionView registerClass:[CameraImagesCollectionViewCell class] forCellWithReuseIdentifier:@"cameraImageCell"];

    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Visit Images"];
    
    imagesArray = [[NSMutableArray alloc]init];
    NSLog(@"customer dict is %@", customerDictornary);
    productCategoryArray = [[NSMutableArray alloc]init];
    filteredCategoryArray = [[VisitImagesCategories alloc]init];
    filteredProductArray = [[NSMutableArray alloc]init];
    categoryArray = [[VisitImagesCategories alloc]init];
    categoryArray.categoryArray = [[NSMutableArray alloc]init];
    productArray.prodducstArray = [[NSMutableArray alloc]init];
    selectedImageID = [[NSString alloc]init];
    imagesArray=[[NSMutableArray alloc]init];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showFullImage:)] ;
    doubleTap.numberOfTapsRequired = 2;
    [imagesCollectionView addGestureRecognizer:doubleTap];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                   action:@selector(handleLongPress:)];
    longPressGesture.numberOfTouchesRequired = 1;
    [imagesCollectionView addGestureRecognizer:longPressGesture];
    
    selectCellIndexPath = -1;
    
    categoryPlusButton.enabled = NO;
    productPlusButton.enabled = NO;
    
    noCategoriesView.hidden = NO;
    if(productCategoryArray.count == 0)
    {
        noProductsView.hidden = NO;
    }else
    {
        noProductsView.hidden = YES;
    }
    categoryTbl.tableFooterView = [UIView new];
    productsTbl.tableFooterView = [UIView new];
    
    categoryTbl.layer.cornerRadius = 8.0;
    categoryTbl.layer.masksToBounds = YES;

    productsTbl.layer.cornerRadius = 8.0;
    productsTbl.layer.masksToBounds = YES;
    
    imagesCollectionView.layer.cornerRadius = 8.0;
    imagesCollectionView.layer.masksToBounds = YES;
    
    noCategoriesView.layer.cornerRadius = 8.0;
    noCategoriesView.layer.masksToBounds= YES;
    
    noProductsView.layer.cornerRadius = 8.0;
    noProductsView.layer.masksToBounds= YES;
    
    categoryTopView.layer.cornerRadius = 8.0f;
    categoryTopView.layer.masksToBounds= YES;
    
    productTopView.layer.cornerRadius = 8.0f;
    productTopView.layer.masksToBounds= YES;
    

    [self fetchImages];
}

-(void)editButtonTapped
{
    if (isEditing==YES) {
        
        
        NSPredicate * testPredicate=[NSPredicate predicateWithFormat:@"isSelected == YES"];
        NSArray * testArray=[imagesArray filteredArrayUsingPredicate:testPredicate];
        NSLog(@"test array count %ld",(long)testArray.count);
        [imagesArray removeObjectsInArray:testArray];
        
        NSFileManager *removeManager=[NSFileManager defaultManager];
        for (NSInteger i=0; i<testArray.count; i++) {
            VisitImages * imageToDelete=[testArray objectAtIndex:i];
            NSError * error;
            NSMutableString *documentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:imageToDelete.imageName] mutableCopy];
            
            BOOL success = [removeManager removeItemAtPath:[NSString stringWithFormat:@"%@", documentsDirectory]  error:&error];
            
            if (!success || error) {
                // it failed.
                NSLog(@"failed to remove image with error %@", error);
            }
            
            
            
            NSMutableString *tempDocumentsDirectory=[[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] stringByAppendingPathComponent:imageToDelete.imageName] mutableCopy];
            
            
            
            BOOL successTemp = [removeManager removeItemAtPath:[NSString stringWithFormat:@"%@", tempDocumentsDirectory]  error:&error];
            
            if (!successTemp || error) {
                // it failed.
                NSLog(@"failed to remove image with error %@", error);
            }
        }
        
        NSLog(@"images array count is %lu", (unsigned long)imagesArray.count);
        
        [imagesCollectionView reloadData];
        
        
        [editButton setImage:[UIImage imageNamed:@"EditImage"]];
        [cameraButton setImage:[UIImage imageNamed:@"CaptureImage"]];
        
        isEditing=NO;
    }
    else{
        isEditing=YES;
        [editButton setImage:[UIImage imageNamed:@"DeleteImage"]];
        [cameraButton setImage:[UIImage imageNamed:@"CancelImage"]];
        
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    cameraButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"CaptureImage"] style:UIBarButtonItemStylePlain target:self action:@selector(launchCamera)];
    editButton=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"EditImage"] style:UIBarButtonItemStylePlain target:self action:@selector(editButtonTapped)];
    saveButton=[[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    [imagesCollectionView reloadData];
    self.navigationItem.rightBarButtonItem = saveButton;
}

-(void)deleteVisitImages
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *directory;
    //iOS 8 support
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        directory=[NSString stringWithFormat:@"%@/%@",[SWDefaults applicationDocumentsDirectory],@"Visit Images/"];
    }
    else
    {
        directory = [NSString stringWithFormat:@"%@/%@",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)objectAtIndex:0],@"Visit Images/"];
    }
    
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:directory error:&error])
    {@autoreleasepool {
        BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@%@", directory, file] error:&error];
        if (!success || error)
        {
            //NSLog(NSLocalizedString(@"Error", nil));
        }
    }
    }
}

- (void) handleBack:(id)sender
{
    for(VisitImages *visit in imagesArray) {
        if([visit.status isEqualToString:@"N"])
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please associate a category or product against all images.", nil) withController:self];
            return;
        }
    }
    
    if(anyUpdated && imagesArray.count>0)
    {
        [self saveButtonTapped];
    }
    else {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}

-(void)saveButtonTapped {
    for(VisitImages *visit in imagesArray){
        if([visit.status isEqualToString:@"N"])
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Please specify a category or product against all images.", nil) withController:self];
            return;
        }
    }
    categoryArray.categoryArray = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchCategoryArrayForAll]];
    categoryArray.saveCategoryArray = [[NSMutableArray alloc]init];
    for(VisitImages *visit in imagesArray){
        NSMutableArray *missedCategoryArray = [[NSMutableArray alloc]init];
        for(VisitImagesProducts *product in visit.selectedProductArray){
            [categoryArray.saveCategoryArray addObject:product];
            NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.inventory_Item_ID ==[cd] %@",product.inventory_Item_ID];
            NSArray *categoryFiltered = [categoryArray.categoryArray filteredArrayUsingPredicate:bPredicate];
            if(categoryFiltered.count>0){
                NSMutableArray *filteredCategory = [categoryFiltered objectAtIndex:0] ;
                [missedCategoryArray addObject:filteredCategory];
            }
        }
        for(VisitImagesCategories *category in visit.selectedCategoryArray){
            if ([[missedCategoryArray valueForKey:@"categoryDesc"] containsObject:category.categoryDesc]){
                
            }
            else{
                [categoryArray.saveCategoryArray addObject:category];
            }
        }
    }
    BOOL status = NO;
    if (imagesArray.count > 0){
        
        if (categoryArray.filteredCategoryArray.count > 0){
            
            BOOL metaDataStatus = [[SWDatabaseManager retrieveManager]saveCapturedImagesToVisitImages:imagesArray];
            if (metaDataStatus == YES) {
                
                BOOL catStatus = [[SWDatabaseManager retrieveManager]saveCapturedImagesWithMetaData:categoryArray.saveCategoryArray product:categoryArray.categoryArray];
                status = catStatus;
            }
            status = metaDataStatus;
        } else {
            status = [[SWDatabaseManager retrieveManager]saveCapturedImagesToVisitImages:imagesArray];
        }
        
        if (status == YES) {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(@"Visit Images saved successfully", nil) withController:self];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
        }
    }
}

-(IBAction)launchCamera
{
  
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {


        [self showErrorAlertView:@"Error" withMessage:@"Device has no camera"];


        } else {

            if (imagesArray.count >= 5) {
                [self showErrorAlertView:@"Error" withMessage:@"You can not add more than 5 Photos."];
                return;
            }
            
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:picker animated:YES completion:NULL];

    }
}

-(void)showErrorAlertView:(NSString *)title withMessage: (NSString *)message {
    UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                    delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];

    [myAlertView show];
}


-(void)launchCameraRoll
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    //picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:true completion:^{
        PictureViewController * pic=[[PictureViewController alloc]init];
        pic.imagesDelegate = self;
        pic.backImage = chosenImage;
        [self.navigationController pushViewController:pic animated:NO];
    }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark UICollectionView Methods

#pragma mark UICollectionView Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (imagesArray.count>0) {
        return  imagesArray.count;
    }
    return 0;

}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(450, 335);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"cameraImageCell";
    CameraImagesCollectionViewCell *cell = (CameraImagesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
   // cell.photoCell.image =[imagesArray objectAtIndex:indexPath.row];
   
    
   

    VisitImages * currentImage=[[VisitImages alloc]init];
    currentImage=[imagesArray objectAtIndex:indexPath.row];
   
    
    cell.photoCell.image=[UIImage imageNamed:[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",currentImage.imageName]]];
    cell.contentView.layer.cornerRadius = 8.0;
    cell.contentView.layer.masksToBounds = YES;
    cell.deleteButton.hidden = YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deleteButtonTapped:)] ;
    singleTap.numberOfTapsRequired = 1;
    cell.deleteButton.userInteractionEnabled = YES;
    [cell.deleteButton addGestureRecognizer:singleTap];
    
    if (indexPath.row == selectCellIndexPath) {
        cell.layer.borderWidth = 4.0;
        cell.layer.borderColor = [UIColor colorWithRed:(0.0/255.0) green:(192.0/255.0) blue:(115.0/255.0) alpha:1].CGColor;
        cell.layer.cornerRadius = 8.0;
        cell.layer.masksToBounds = YES;
        cell.alphaView.backgroundColor = [UIColor clearColor];
        cell.alphaView.alpha = 1;
        
    }else{
        
        cell.layer.borderWidth = 0;
        cell.alphaView.backgroundColor = [UIColor blackColor];
        cell.alphaView.alpha = 0.6;
        
    }
    
   
    return cell;
}


-(void)remove:(int)i {

    [imagesCollectionView performBatchUpdates:^{
        [self.myImagesArray removeObjectAtIndex:i];
        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:i inSection:0];
        [imagesCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        [imagesCollectionView reloadData];

    } completion:^(BOOL finished) {

    }];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
  
    
    categoryPlusButton.enabled = YES;
    productPlusButton.enabled = YES;
    
    VisitImages * currentImage=[[VisitImages alloc]init];
    currentImage=[imagesArray objectAtIndex:indexPath.row];
    selectedImageID = currentImage.imageID;
    for(VisitImages *visitImage in imagesArray){
        if([visitImage.imageID isEqualToString:selectedImageID]){
            categoryArray.filteredCategoryArray = visitImage.selectedCategoryArray;
            productArray.filteredProductsArray = visitImage.selectedProductArray;
        }
    }
    
    selectCellIndexPath = indexPath.row;
    if (isEditing==YES) {
        
        VisitImages * currentSelectedImage=[imagesArray objectAtIndex:indexPath.row];
        if (currentSelectedImage.isSelected==YES) {
            currentSelectedImage.isSelected=NO;
        }
        else{
            currentSelectedImage.isSelected=YES;
        }
        [imagesArray replaceObjectAtIndex:indexPath.row withObject:currentSelectedImage];
        
        
    }
    else{
        
    }
    [categoryTbl reloadData];
    [productsTbl reloadData];
    [imagesCollectionView reloadData];
    
    
    
    
}
-(void)deleteButtonTapped:(UIGestureRecognizer *)gestureRecognzer
{
    
    NSPredicate * testPredicate=[NSPredicate predicateWithFormat:@"isSelected == YES"];
    NSArray * testArray=[imagesArray filteredArrayUsingPredicate:testPredicate];
    NSLog(@"test array count %ld",(long)testArray.count);
    for (VisitImages *tempImage in imagesArray) {
        if([tempImage.imageID isEqualToString:selectedImageID]){
            if (tempImage.selectedCategoryArray.count > 0){
                [categoryArray.filteredCategoryArray removeObjectsInArray:tempImage.selectedCategoryArray];
                [categoryTbl reloadData];
                [productArray.filteredProductsArray removeObjectsInArray:tempImage.selectedProductArray];
                [productsTbl reloadData];
                
            }
        }
    }
    
    categoryPlusButton.enabled = NO;
    productPlusButton.enabled = NO;
    selectCellIndexPath = -1;
    
    [imagesArray removeObjectsInArray:testArray];
    
    
    NSFileManager *removeManager=[NSFileManager defaultManager];
    for (NSInteger i=0; i<testArray.count; i++) {
        VisitImages * imageToDelete=[testArray objectAtIndex:i];
        NSError * error;
        NSMutableString *documentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:imageToDelete.imageName] mutableCopy];
        
        BOOL success = [removeManager removeItemAtPath:[NSString stringWithFormat:@"%@", documentsDirectory]  error:&error];
        
        if (!success || error) {
            // it failed.
            NSLog(@"failed to remove image with error %@", error);
        }
        
        
        
        NSMutableString *tempDocumentsDirectory=[[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] stringByAppendingPathComponent:imageToDelete.imageName] mutableCopy];
        
        
        
        BOOL successTemp = [removeManager removeItemAtPath:[NSString stringWithFormat:@"%@", tempDocumentsDirectory]  error:&error];
        
        if (!successTemp || error) {
            // it failed.
            NSLog(@"failed to remove image with error %@", error);
        }
    }
    
    
    NSLog(@"images array count is %lu", (unsigned long)imagesArray.count);
    
    [imagesCollectionView reloadData];
    
}
-(void)showFullImage:(UIGestureRecognizer *)gestureRecognzer{
    for(int i=0;i<= [imagesArray count]; i++)
    {
        SYPhotoBrowser *photoBrowser = [[SYPhotoBrowser alloc] initWithImageSourceArray:[imagesArray valueForKey:@"capturedImage"] caption:nil delegate:self];
        photoBrowser.initialPageIndex = i;
        photoBrowser.pageControlStyle = SYPhotoBrowserPageControlStyleSystem;
        [self presentViewController:photoBrowser animated:YES completion:nil];
    }
}

-(void)handleLongPress:(UIGestureRecognizer *)gestureRecognzer{
    
    CGPoint pt = [gestureRecognzer locationInView:imagesCollectionView];
    
    NSIndexPath *indexPath = [imagesCollectionView indexPathForItemAtPoint:pt];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        // get the cell at indexPath (the one you long pressed)
        CameraImagesCollectionViewCell* cell = [imagesCollectionView cellForItemAtIndexPath:indexPath];
        selectedIndexpath = indexPath;
        
        BOOL temp = YES;
        [[imagesArray objectAtIndex:selectedIndexpath.row] setValue:[NSNumber numberWithBool:temp] forKey:@"isSelected"];
        
        VisitImages * currentSelectedImage=[imagesArray objectAtIndex:selectedIndexpath.row];
        if (currentSelectedImage.isSelected==YES) {
            cell.deleteButton.hidden = NO;
        }
        else{
            cell.deleteButton.hidden = YES;
        }
        
    }
    
    
}
-(void)fetchImages
{
    NSMutableString *documentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] mutableCopy];
    
    //NSMutableArray * dirImageArray=[[NSMutableArray alloc]init];
    NSMutableArray *savedArray = [[NSMutableArray alloc]init];
    savedArray = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT Image_ID from TBL_Visit_Images"]];
    dirImageArray= [[[NSFileManager defaultManager]contentsOfDirectoryAtPath:documentsDirectory error:nil] mutableCopy];
    NSURL * tempUrl=[NSURL fileURLWithPath: documentsDirectory];
    
    for (NSInteger i=0; i<dirImageArray.count; i++) {
        VisitImages * currentImage=[[VisitImages alloc]init];
        currentImage.imagePath=[[NSString stringWithFormat:@"%@",tempUrl]stringByAppendingPathComponent:[dirImageArray objectAtIndex:i]];
        currentImage.isSelected=NO;
        currentImage.capturedImage=[UIImage imageNamed:[[[[SWDefaults applicationDocumentsDirectory] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dirImageArray objectAtIndex:i]]]];
        currentImage.imageName=[NSString stringWithFormat:@"%@",[dirImageArray objectAtIndex:i]];
        currentImage.imageID = [[NSString stringWithFormat:@"%@",[dirImageArray objectAtIndex:i]] stringByDeletingPathExtension];//[NSString createGuid];
        currentImage.status = @"N";
        
        if(imagesArray.count == 0 && ![[savedArray valueForKey:@"Image_ID"]containsObject:currentImage.imageID]){
            [imagesArray addObject:currentImage];
        }
        else{
            //  for (int j=0;j<imagesArray.count;j++){
            NSString *imageID = [[dirImageArray objectAtIndex:i]stringByDeletingPathExtension];
            if([[imagesArray valueForKey:@"imageID"] containsObject:currentImage.imageID] || [[savedArray valueForKey:@"Image_ID"]containsObject:currentImage.imageID] ){
                NSLog(@"Id is same");
                
            }else{
                NSLog(@"Add");
                [imagesArray addObject:currentImage];
                
            }
            // }
        }
    }
    if (imagesArray.count>0) {
        noCaptureImageView.hidden=YES;
        [imagesCollectionView reloadData];
    }
    else{
        noCaptureImageView.hidden=NO;
        // noImagesPlaceHolderImageView.image=[UIImage imageNamed:@"ImagePlaceHolder.png"];
    }
    
}
-(void)imagesCaptured:(BOOL)flag
{
    if (flag==YES) {
        [self fetchImages];
    }
}

-(void)retakeImageTaped:(BOOL)flag
{
    if (flag==YES) {
        [self launchCamera];
    }
}

-(IBAction)categoryButtonTapped:(id)sender{
    categoryArray.categoryArray = [[NSMutableArray alloc]init];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchCategoryArrayForAll]];
    for (VisitImagesCategories *obj in array) {
        if (![[categoryArray.categoryArray valueForKey:@"categoryDesc"] containsObject:obj.categoryDesc]) {
            [categoryArray.categoryArray addObject:obj];
        }
    }
    
    for(VisitImages *currentImage in imagesArray){
        if(currentImage.imageID == selectedImageID){
            if(currentImage.selectedCategoryArray.count > 0){
                for (int i= 0; i< categoryArray.categoryArray.count; i++) {
                    id categoryString = [[categoryArray.categoryArray valueForKey:@"categoryDesc"]objectAtIndex:i];
                    for (int j=0; j < currentImage.selectedCategoryArray.count; j++) {
                        id popOverString = [[currentImage.selectedCategoryArray valueForKey:@"categoryDesc"]objectAtIndex:j];
                        if ([popOverString isEqual:categoryString]){
                            VisitImagesCategories *newDict = [currentImage.selectedCategoryArray objectAtIndex:j];
                            [categoryArray.categoryArray replaceObjectAtIndex:i withObject:newDict] ;
                        }
                    }
                }
            }
        }
    }
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = categoryArray.categoryArray;
    popOverVC.titleKey = @"Select Category";
    popOverVC.popoverType = @"Category Type";
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    popOverVC.imageID = selectedImageID;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.popOverController=paymentPopOverController;
    CGRect relativeFrame = [categoryPlusButton convertRect:categoryPlusButton.bounds toView:self.view];
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(500, 500) animated:YES];
    [paymentPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft
                                            animated:YES];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        for (VisitImages *currentImage in imagesArray){
            if([currentImage.imageID isEqualToString:selectedImageID]){
                if(tableView.tag == 1){
                    [currentImage.selectedCategoryArray removeObjectAtIndex:indexPath.row];
                    [categoryTbl reloadData];
                }
                else{
                    [currentImage.selectedProductArray removeObjectAtIndex:indexPath.row];
                    [productsTbl reloadData];
                }
            }
        }
        
    }
}

-(IBAction)productButtonTapped:(id)sender;
{
    productArray = [[VisitImagesProducts alloc]init];
    productArray.prodducstArray = [[NSMutableArray alloc]init];
    
    if (categoryArray.filteredCategoryArray.count == 0) {
        productArray.prodducstArray = [[SWDatabaseManager retrieveManager] fetchProductForCategory:@""];
        for(VisitImages *currentImage in imagesArray){
            if(currentImage.imageID == selectedImageID){
                if (currentImage.selectedProductArray.count > 0){
                    for (int i= 0; i< productArray.prodducstArray.count; i++) {
                        id categoryString = [[productArray.prodducstArray valueForKey:@"inventory_Item_ID"]objectAtIndex:i];
                        for (int j=0; j < currentImage.selectedProductArray.count; j++) {
                            id popOverString = [[currentImage.selectedProductArray valueForKey:@"inventory_Item_ID"]objectAtIndex:j];
                            if ([popOverString isEqual:categoryString]){
                                VisitImagesProducts *newDict = [currentImage.selectedProductArray objectAtIndex:j];
                                [productArray.prodducstArray replaceObjectAtIndex:i withObject:newDict];
                            }
                        }
                        
                    }
                }
            }
        }
        
    }else{
        
        for (int i=0; i< categoryArray.filteredCategoryArray.count; i++) {
            NSMutableArray *prodArray = [[SWDatabaseManager retrieveManager] fetchProductForCategory:[[categoryArray.filteredCategoryArray objectAtIndex:i] valueForKey:@"category_Code"]];
            productArray.prodducstArray = [[productArray.prodducstArray arrayByAddingObjectsFromArray:prodArray] mutableCopy];
            for(VisitImages *currentImage in imagesArray){
                if(currentImage.imageID == selectedImageID){
                    if (currentImage.selectedProductArray.count > 0){
                        for (int i= 0; i< productArray.prodducstArray.count; i++) {
                            id categoryString = [[productArray.prodducstArray valueForKey:@"inventory_Item_ID"]objectAtIndex:i];
                            for (int j=0; j < currentImage.selectedProductArray.count; j++) {
                                id popOverString = [[currentImage.selectedProductArray valueForKey:@"inventory_Item_ID"]objectAtIndex:j];
                                if ([popOverString isEqual:categoryString]){
                                    VisitImagesProducts *newDict = [currentImage.selectedProductArray objectAtIndex:j];
                                    [productArray.prodducstArray replaceObjectAtIndex:i withObject:newDict];
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.titleKey = @"Select Product";
    popOverVC.popOverContentArray = productArray.prodducstArray;
    popOverVC.popoverType = @"Product Type";
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=NO;
    popOverVC.imageID = selectedImageID;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.popOverController=paymentPopOverController;
    CGRect relativeFrame = [productPlusButton convertRect:productPlusButton.bounds toView:self.view];
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(400, 400) animated:YES];
    [paymentPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight
                                            animated:YES];
    
    
}
-(void)didSelectPopOverControllerForCategoryMultiSelection:(NSMutableArray *)selectedIndexPathArray :(NSString *)popoverTitle
{
    
    if ([popoverTitle isEqualToString:@"Category Type"]) {
        categoryArray.filteredCategoryArray = [[NSMutableArray alloc]init];
        categoryArray.categoryArray = selectedIndexPathArray;
        noCategoriesView.hidden = NO;
        for (VisitImagesCategories *dictObj in categoryArray.categoryArray) {
            if ([dictObj.isSelected isEqualToString:KAppControlsYESCode] && [dictObj.selectedImageID isEqualToString:selectedImageID])
            {
                anyUpdated = YES;
                [categoryArray.filteredCategoryArray addObject:dictObj];
                for(VisitImages *visitImage in imagesArray){
                    if ([visitImage.imageID isEqualToString:dictObj.selectedImageID]){
                        visitImage.status = @"Y";
                        visitImage.selectedCategoryArray = categoryArray.filteredCategoryArray;
                        categoryArray.filteredCategoryArray = visitImage.selectedCategoryArray;
                    }
                }
            }
            noCategoriesView.hidden = YES;
        }
    }else{
        if(categoryArray.filteredCategoryArray.count==0){
            productArray.filteredProductsArray = [[NSMutableArray alloc]init];
            categoryArray.filteredCategoryArray = [[NSMutableArray alloc]init];
            noCategoriesView.hidden = NO;
            noProductsView.hidden = NO;
            productArray.prodducstArray = selectedIndexPathArray;
            NSMutableArray *array = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchCategoryArrayForAll]];
            for (VisitImagesCategories *obj in array) {
                if (![[categoryArray.categoryArray valueForKey:@"categoryDesc"] containsObject:obj.categoryDesc]) {
                    [categoryArray.categoryArray addObject:obj];
                }
            }
            for(VisitImagesProducts *dictObj in productArray.prodducstArray){
                if ([dictObj.isSelected isEqualToString:KAppControlsYESCode] && [dictObj.selectedImageID isEqualToString:selectedImageID])
                {
                    anyUpdated = YES;
                    [productArray.filteredProductsArray addObject:dictObj];
                    for(VisitImagesProducts *product in productArray.filteredProductsArray){
                        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.inventory_Item_ID ==[cd] %@",product.inventory_Item_ID];
                        NSArray *categoryFilter = [array filteredArrayUsingPredicate:bPredicate];
                        if(categoryFilter.count>0){
                        NSMutableArray *selectedcategoryFromProduct = [categoryFilter objectAtIndex:0];
                        [categoryArray.filteredCategoryArray addObject:selectedcategoryFromProduct];
                        }
                        
                    }
                    for(VisitImages *visitImage in imagesArray){
                        if ([visitImage.imageID isEqualToString:dictObj.selectedImageID]){
                            visitImage.status = @"Y";
                            visitImage.selectedProductArray = productArray.filteredProductsArray;
                            for(VisitImagesCategories *category in categoryArray.filteredCategoryArray){
                                category.isSelected = @"Y";
                                category.selectedImageID = selectedImageID;
                                category.selectedInventoryID = dictObj.inventory_Item_ID;
                                visitImage.selectedCategoryArray = categoryArray.filteredCategoryArray;
                                categoryArray.filteredCategoryArray = visitImage.selectedCategoryArray;
                            }
                            productArray.filteredProductsArray = visitImage.selectedProductArray;
                        }
                    }
                }
                noCategoriesView.hidden = YES;
                noProductsView.hidden = YES;
            }
            
            
        }
        else{
            productArray.filteredProductsArray = [[NSMutableArray alloc]init];
            productArray.prodducstArray = selectedIndexPathArray;
            noCategoriesView.hidden = NO;
            noProductsView.hidden = NO;
            for(VisitImagesProducts *dictObj in productArray.prodducstArray){
                if ([dictObj.isSelected isEqualToString:KAppControlsYESCode] && [dictObj.selectedImageID isEqualToString:selectedImageID])
                {
                    anyUpdated = YES;
                    [productArray.filteredProductsArray addObject:dictObj];
                    for(VisitImages *visitImage in imagesArray){
                        if ([visitImage.imageID isEqualToString:dictObj.selectedImageID]){
                            visitImage.selectedProductArray = productArray.filteredProductsArray;
                            productArray.filteredProductsArray = visitImage.selectedProductArray;
                        }
                        NSMutableArray *array = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager]fetchCategoryArrayForAll]];
                        for (VisitImagesCategories *obj in array) {
                            if (![[categoryArray.categoryArray valueForKey:@"categoryDesc"] containsObject:obj.categoryDesc]) {
                                [categoryArray.categoryArray addObject:obj];
                            }
                        }
                        //                         for(VisitImagesCategories *category in categoryArray.filteredCategoryArray){
                        //                            if ([category.selectedImageID isEqualToString:selectedImageID]) {
                        //                                for(VisitImagesProducts *product in productArray.filteredProductsArray){
                        //                                    category.selectedInventoryID = product.inventory_Item_ID;
                        //                                }
                        //                            }
                        //                        }
                    }
                }
                noCategoriesView.hidden = YES;
                noProductsView.hidden = YES;
            }
        }
        
    }
    [productsTbl reloadData];
    [categoryTbl reloadData];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1){
        
        return categoryArray.filteredCategoryArray.count;
        
    }
    else{
        return productArray.filteredProductsArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1){
        static NSString* identifier=@"categoryList";
        CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"CategoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        VisitImagesCategories *fileteredCategory = [categoryArray.filteredCategoryArray objectAtIndex:indexPath.row];
        
        cell.categoryLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.categoryDesc]] isEqualToString:@""]?@"N/A" : [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.categoryDesc]];
        cell.agencyLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.agencyDesc]] isEqualToString:@""]?@"N/A" : [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.agencyDesc]];
        cell.brandCodeLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.brandDesc]] isEqualToString:@""]?@"N/A" : [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:fileteredCategory.brandDesc]];
        
        return cell;
    }
    else{
        static NSString* identifier=@"productList";
        ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"ProductTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        VisitImagesProducts *fileteredProduct = [productArray.filteredProductsArray objectAtIndex:indexPath.row];
        cell.productLabel.text = fileteredProduct.productDescription;//[[filteredProductArray valueForKey:@"Description"]objectAtIndex:indexPath.row];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
