//
//  CameraImagesCollectionViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraImagesCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *photoCell;
@property (strong, nonatomic) IBOutlet UIImageView *deleteButton;
@property (strong, nonatomic) IBOutlet UIView *alphaView;

@end
