//
//  SalesWorxCameraRollViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/17/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureViewController.h"
#import <SYPhotoBrowser/SYPhotoViewController.h>
#import "SYPhotoBrowser.h"
#import "MedRepProductCodeLabel.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxCustomClass.h"
#import "SWDatabaseManager.h"

@interface SalesWorxCameraRollViewController : UIViewController<UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,SalesWorxPopoverControllerDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate>

{
    
    NSMutableArray * imagesArray;
    NSMutableArray * imageUrlsArray;
    IBOutlet UICollectionView *imagesCollectionView;
    BOOL isEditing;
    UIBarButtonItem * cameraButton;
    UIBarButtonItem * editButton;
    UIBarButtonItem * saveButton;
    IBOutlet MedRepElementDescriptionLabel *customerNameLbl;
    IBOutlet MedRepElementTitleLabel *customerNumberLbl;
    IBOutlet UIImageView *noImagesPlaceHolderImageView;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;IBOutlet UIView *noCategoriesView;
    IBOutlet UIView *noProductsView;
    IBOutlet UIView *noCaptureImageView;
    UIPopoverController * paymentPopOverController;
    CAGradientLayer *layer;
    
    
     IBOutlet UIView *categoryTopView;
    IBOutlet UIView *productTopView;
    
    
    IBOutlet UITableView *categoryTbl;
    IBOutlet UITableView *productsTbl;
    IBOutlet UIButton *categoryPlusButton;
    IBOutlet UIButton *productPlusButton;
    
    
    VisitImagesCategories *categoryArray;
    VisitImagesCategories *filteredCategoryArray;
    NSMutableArray *filteredProductArray;
    
    VisitImagesProducts *productArray;
    NSArray *prediacteFilteredCategoryArray;
    //VisitImagesCategories *selectedCategoryArray;
    
    
    NSMutableArray *productCategoryArray;
    
    NSString *selectedImageID;
    
    NSIndexPath *selectedIndexpath;
    NSMutableArray *temp;
    NSInteger selectCellIndexPath;
    BOOL anyUpdated;
    NSArray *dirImageArray;
    NSMutableArray * myImagesArray;
    
}

@property(strong,nonatomic) NSMutableDictionary * customerDictornary;
@property (nonatomic, strong) UIImage *finalImage;
@property (nonatomic, strong) NSString *getScreen;

@property(strong,nonatomic) NSMutableArray * myImagesArray;

-(IBAction)categoryButtonTapped:(id)sender;
-(IBAction)productButtonTapped:(id)sender;

@end
