//
//  PictureViewController.h
//  PictureProcess
//
//  Created by ucs on 12/18/14.
//  Copyright (c) 2014 ucs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SWDefaults.h"
#import "CameraImagesCollectionViewCell.h"
#import "NSString+Additions.h"
#import "DataSyncManager.h"
#import "SWDatabaseManager.h"
#import "SalesWorxCameraRollViewController.h"
#define kMaxIdleTimeSeconds 10


@protocol CurrentVisitCapturedImagesDelegate <NSObject>

-(void)imagesCaptured:(BOOL)flag;
-(void)retakeImageTaped:(BOOL)flag;

@end

@interface PictureViewController : UIViewController<AVCaptureVideoDataOutputSampleBufferDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    __weak IBOutlet UIView *footerView;
     NSTimer *idleTimer;
    NSMutableString *InavoiceName;
    IBOutlet UIButton *reTakeButton;
    
    id imagesDelegate;
    
    NSArray * phto_arr_;
    
    __weak IBOutlet UIButton *buttonAddPhoto;
}
@property (strong, nonatomic) IBOutlet UIView *cameraControlsView;
@property (strong, nonatomic) IBOutlet UICollectionView *imagesCollectionView;
@property (strong,nonatomic)IBOutlet UIView *cameraView;
- (IBAction)retakeButtonTapped:(id)sender;
@property(nonatomic) id imagesDelegate;

@property (strong,nonatomic) AVCaptureSession *session;
@property (strong,nonatomic) AVCaptureStillImageOutput *captureOutput;
@property (strong,nonatomic) UIImage *image;
@property (nonatomic)IBOutlet UIButton *takephotoButton;
@property (nonatomic)IBOutlet UIButton *usephotoButton;
@property (nonatomic)IBOutlet UIButton *cancelphotoButton;
@property (nonatomic)IBOutlet UIButton *cameraCancelButton;
@property (nonatomic)IBOutlet UIImageView *imagePreview;
@property(strong,nonatomic)     NSMutableArray * cameraImagesArray;

@property(nonatomic) NSInteger imagesCount;
@property(strong,nonatomic) NSData *currentImageData;
@property(strong,nonatomic) AVCaptureVideoPreviewLayer* CapturePreviewLayer;
-(IBAction)cancelButtonPressed:(id)sender;
@property (strong,nonatomic) NSMutableString* isFromcustomerdetailsPage;
@property (strong,nonatomic) IBOutlet UIImageView *cameraBlackStripImage;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

// by pramod 27/08/2022
@property (nonatomic, strong) UIImage *backImage;
-(IBAction)cameraCancelButtonPressed:(id)sender;
@property (nonatomic, strong) UIImage *getImage;
@property(retain,nonatomic)     NSMutableArray * tImagesArray;

@end
