//
//  ProductTableViewCell.h
//  SalesWars
//
//  Created by Nethra on 12/8/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProductTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *productLabel;

@end

NS_ASSUME_NONNULL_END
