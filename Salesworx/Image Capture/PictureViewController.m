
//
//  PictureViewController.m
//  PictureProcess
//
//  Created by ucs on 12/18/14.
//  Copyright (c) 2014 ucs. All rights reserved.
//
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
#import "PictureViewController.h"
#import  <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import "FMDBHelper.h"
#import <QuartzCore/QuartzCore.h>
@interface PictureViewController ()

@end
@implementation PictureViewController
@synthesize captureOutput,cameraView,currentImageData,session,CapturePreviewLayer,cameraBlackStripImage,isFromcustomerdetailsPage,cameraCancelButton,imagesCollectionView,cameraImagesArray,imagesCount,takephotoButton,cameraControlsView, imagesDelegate, cancelphotoButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [super viewDidLoad];
    [self initializeSession];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];

    [imagesCollectionView registerClass:[CameraImagesCollectionViewCell class] forCellWithReuseIdentifier:@"cameraImageCell"];
    footerView.hidden=YES;
    self.tImagesArray=[[NSMutableArray alloc]init];
   // footerView.hidden=NO;
    cameraControlsView.hidden=YES;
    [self.navigationController setNavigationBarHidden:YES];
    cameraImagesArray=[[NSMutableArray alloc]init];


    NSMutableString *documentsDirectory=[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] mutableCopy];

    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];


    NSString * tempDocumentsDirectory=[documentsDirectory stringByAppendingPathComponent:@"Temp"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:tempDocumentsDirectory])
        [[NSFileManager defaultManager] createDirectoryAtPath:tempDocumentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];

    [self resetIdleTimer];
    self.lblAddress.text = @"";

    [self loadCamera];
    
  /*
    UIImage *chosenImage = [UIImage imageNamed:@"per"];
    
    
    CGSize newSize = CGSizeMake(self.imagePreview.frame.size.width, self.imagePreview.frame.size.height);
    UIGraphicsBeginImageContextWithOptions( newSize, false, 0.0 );
    
    [self.backImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    [chosenImage drawInRect:CGRectMake(self.imagePreview.frame.size.width / 1.4,self.imagePreview.frame.size.height - 242,self.imagePreview.frame.size.width / 4  ,200)];
    
    

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    self.imagePreview.image = newImage;
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy HH:mm"];
    NSString *timeString = [formatter stringFromDate:date];
    NSLog(@"%@",timeString);
    
    self.lblAddress.text = [timeString stringByAppendingString:@" Jaipur Rajasthan"];
    
    */
    
    // Do any additional setup after loading the view from its nib.
}

-(void)loadCamera {
    // By pramod 27/08/2022
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
            UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Device has no camera"
                                                            delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
            
            [myAlertView show];
            
        } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
      //  picker.allowsEditing = YES;
            
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            
            
        [self presentViewController:picker animated:NO completion:NULL];

    }
    // below comment by pramod.
}

-(UIImage *)captureScreenInRect:(CGRect)captureFrame
{
    CALayer *layer;
    layer = self.view.layer;
    UIGraphicsBeginImageContext(self.view.bounds.size);
    CGContextClipToRect (UIGraphicsGetCurrentContext(),captureFrame);
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}


// By pramod 27/08/2022




- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];

    
    
    
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
        {
            NSLog(@"myloi");
        }
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
        {
            
        }
    
    
    
    CGSize newSize = CGSizeMake(self.imagePreview.frame.size.width, self.imagePreview.frame.size.height);
    UIGraphicsBeginImageContextWithOptions( newSize, false, 0.0 );
    
    if (self.backImage.size.width < self.backImage.size.height) {
        [self.backImage drawInRect:CGRectMake(180,0,700,newSize.height)];
        [chosenImage drawInRect:CGRectMake(self.imagePreview.frame.size.width / 1.3,self.imagePreview.frame.size.height - 300,self.imagePreview.frame.size.width / 5  ,240)];
        
    }else {
        [self.backImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//        [chosenImage drawInRect:CGRectMake(self.imagePreview.frame.size.width / 1.3,self.imagePreview.frame.size.height - 270,self.imagePreview.frame.size.width / 5  ,180)];
      //  [chosenImage drawInRect:CGRectMake(self.imagePreview.frame.size.width / 1.3-120,450,240  ,200)];
        
        [chosenImage drawInRect:CGRectMake(self.imagePreview.frame.size.width / 1.3,self.imagePreview.frame.size.height - 270,205  ,170)];
        
    }
    
    
    
    

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
   
 
    
    self.imagePreview.image = newImage;
    
    footerView.hidden=NO;
//    // Continue as appropriate.
//    NSData *imageData = UIImageJPEGRepresentation(self.imagePreview.image,1.0);
//    UIImage *t_image = [UIImage imageWithData:imageData];
//    NSData *newdata=UIImageJPEGRepresentation(t_image, 1.0);
//    self.currentImageData=newdata;
//
//    imagesCount=imagesCount+1;
//    [cameraImagesArray addObject:newdata];
//    imagesCollectionView.hidden=NO;
//    NSLog(@"camera images array count ld%lu", (unsigned long)cameraImagesArray.count);
//
//    [self.imagePreview setHidden:NO];
//
//
//    self.imagePreview.contentMode=UIViewContentModeScaleAspectFit;
//    self.imagePreview.image=t_image;
//    //self.imagePreview.image = img;
    
    
    
   
   // NSData *imageData = UIImageJPEGRepresentation(newImage, 1.0);
    
    
    
    imagesCount=imagesCount+1;
   // [cameraImagesArray addObject:newImage];
    [imagesCollectionView reloadData];
    if (cameraImagesArray.count>0) {
        [imagesCollectionView reloadData];
    }
    
    imagesCollectionView.hidden=NO;
    NSLog(@"camera images array count ld%lu", (unsigned long)cameraImagesArray.count);
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy HH:mm"];
    NSString *timeString = [formatter stringFromDate:date];
    NSLog(@"%@",timeString);
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
        stringForKey:@"preferenceName"];
    if (savedValue == nil) {
        self.lblAddress.text = timeString;
    } else {
        self.lblAddress.text = [[timeString stringByAppendingString:@" "] stringByAppendingString:savedValue];
    }
    
    
    // *********************
    [buttonAddPhoto setUserInteractionEnabled:NO];
    [reTakeButton setUserInteractionEnabled:NO];
    [cancelphotoButton setUserInteractionEnabled:NO];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{

        UIImage *imgFrame = [self captureScreenInRect:CGRectMake(0, 0, self.imagePreview.frame.size.width, self.imagePreview.frame.size.height - 62)];
        self.imagePreview.image = imgFrame;
       // [self.tImagesArray addObject:self.imagePreview.image];
        
        NSData *newdata=UIImageJPEGRepresentation(imgFrame, 1.0);
        self.currentImageData=newdata;
        
        [cameraImagesArray addObject:newdata];
        [buttonAddPhoto setUserInteractionEnabled:YES];
        [reTakeButton setUserInteractionEnabled:YES];
        [cancelphotoButton setUserInteractionEnabled:YES];
    });
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if( [keyPath isEqualToString:@"adjustingFocus"]) {
        BOOL adjustingFocus = [ [change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1] ];
        NSLog(@"Is adjusting focus? %@", adjustingFocus ? @"YES" : @"NO" );
        NSLog(@"Change dictionary: %@", change);
    }
}

-(void)viewWillAppear:(BOOL)animated
{
   
    // obove commented code updated by pramod on 27/08/2022
    
    [self embedPreviewInView:cameraView];
    [self.imagePreview setHidden:NO];
    [self.usephotoButton setHidden:YES];
    [self.cancelphotoButton setHidden:NO];
    reTakeButton.hidden=NO;
    
    imagesCollectionView.hidden=NO;
    self.cancelphotoButton.userInteractionEnabled = YES;
    reTakeButton.userInteractionEnabled = YES;
}

-(void)initializeSession
{
    session = [[AVCaptureSession alloc] init];
    [session setSessionPreset:AVCaptureSessionPresetPhoto];
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
#if 1
    int flags = NSKeyValueObservingOptionNew;
    [device addObserver:self forKeyPath:@"adjustingFocus" options:flags context:nil];
#endif
    
    NSError *error;
    AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!captureInput)
    {
        NSLog(@"Error: %@", error);
        return;
    }
    [session addInput:captureInput];
  
    
    captureOutput = [[AVCaptureStillImageOutput alloc] init] ;
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil];
    [captureOutput setOutputSettings:outputSettings];
    [session addOutput:captureOutput];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) embedPreviewInView: (UIView *) aView {
    if (!session)
        return;
    
    if(CapturePreviewLayer==nil)
    {
        CapturePreviewLayer = (AVCaptureVideoPreviewLayer*)[[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
        CapturePreviewLayer.frame = aView.bounds;
        CapturePreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
        if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown)
        {
            [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else if (deviceOrientation == UIInterfaceOrientationPortrait)
        {
            [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else if (deviceOrientation == UIInterfaceOrientationLandscapeLeft)
        {
            [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else if (deviceOrientation == UIInterfaceOrientationLandscapeRight)
        {
            [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
        }
        
        [aView.layer addSublayer: CapturePreviewLayer];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [session startRunning];
            
            // update UI on the main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                CapturePreviewLayer.frame = aView.bounds;
                CapturePreviewLayer.frame = CGRectMake(CapturePreviewLayer.frame.origin.x, CapturePreviewLayer.frame.origin.y, CapturePreviewLayer.frame.size.width-82, CapturePreviewLayer.frame.size.height);
            });
        });

    }
}

- (IBAction)buttonPressed:(id)sender {
    
    takephotoButton.userInteractionEnabled=YES;
    [self Captureimage];
}

-(void)Captureimage
{
    //get connection
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in captureOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
                if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown)
                {
                    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                }
                else if (deviceOrientation == UIInterfaceOrientationPortrait)
                {
                    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                }
                else if (deviceOrientation == UIInterfaceOrientationLandscapeLeft)
                {
                    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
                }
                else if (deviceOrientation == UIInterfaceOrientationLandscapeRight)
                {
                    [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
                }
                break;
            }
        }
        if (videoConnection) { break; }
    }
    
    //get UIImage
    [captureOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:
     ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        CFDictionaryRef exifAttachments =
        CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
        if (exifAttachments) {
            // Do something with the attachments.
        }
        
        // Continue as appropriate.
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
        UIImage *t_image = [UIImage imageWithData:imageData];
        
        
        UIImageOrientation  currentOrientation;
        if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
        {
            currentOrientation=UIImageOrientationUp;
        }
        else if (([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft))
        {
            currentOrientation=UIImageOrientationUp;
        }
        else if (([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight))
        {
            currentOrientation=UIImageOrientationRight;
        }
        else {
            currentOrientation=UIImageOrientationDown;
        }
        
        
        UIImage *tempImage=[[UIImage alloc]initWithCGImage:t_image.CGImage scale:0.25 orientation:t_image.imageOrientation];
        UIImage *compressedImage=[PictureViewController imageWithImage:tempImage scaledToSize:CGSizeMake(512, 384)];
        NSData *newdata=UIImagePNGRepresentation(compressedImage);
        self.currentImageData=newdata;
        
        imagesCount=imagesCount+1;
        [cameraImagesArray addObject:newdata];
        
        [cameraView setHidden:YES];
        cameraControlsView.hidden=YES;
        [self.takephotoButton setHidden:YES];
        [cameraCancelButton setHidden:YES];
        [cameraBlackStripImage setHidden:YES];
        [self.usephotoButton setHidden:NO];
        [self.cancelphotoButton setHidden:NO];
        reTakeButton.hidden=NO;
        
        if (cameraImagesArray.count>0) {
            [imagesCollectionView reloadData];
        }
        
        imagesCollectionView.hidden=NO;
        NSLog(@"camera images array count ld%lu", (unsigned long)cameraImagesArray.count);
        
        [self.imagePreview setHidden:NO];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView commitAnimations];
        self.imagePreview.contentMode=UIViewContentModeScaleAspectFit;
        self.imagePreview.image=t_image;
        
        footerView.hidden=NO;
    }];
}
- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"this is the view did disappear");
    
}

-(IBAction)addPhotoButtonPressed:(id)sender
{
    takephotoButton.userInteractionEnabled=YES;
    [self addPhotoIntodocumentsDirectory];
   /* SalesWorxCameraRollViewController * pic=[[SalesWorxCameraRollViewController alloc]init];
   // pic.finalImage = self.imagePreview.image;
    pic.getScreen = @"setImage";
    if(pic.myImagesArray==nil)
    {
        pic.myImagesArray=[[NSMutableArray alloc] init];
        
    }
    [pic.myImagesArray addObject:[self.tImagesArray objectAtIndex:0]];
    //pic.myImagesArray = self.tImagesArray;
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController pushViewController:pic animated:YES];*/

    
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)addPhotoIntodocumentsDirectory
{
    NSLog(@"images count is %lu", (unsigned long)cameraImagesArray.count);
    
    for (NSInteger i=0; i<cameraImagesArray.count; i++) {
        
        NSData * currentImageDataContent=[cameraImagesArray objectAtIndex:i];
        UIImage * uncompressedImage=[UIImage imageWithData:currentImageDataContent];
        
        NSMutableString *documentsDirectory=[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] mutableCopy];
        
        
        NSString *documentPathComponent=[NSString stringWithFormat:@"%@.jpg",[NSString createGuid]];
        NSString *fileSavePath=[documentsDirectory stringByAppendingPathComponent:documentPathComponent];
        NSError * saveError;
        float compressionQuality=0.3;
        
        
        BOOL saveSuccess=[UIImageJPEGRepresentation(uncompressedImage, compressionQuality) writeToFile:fileSavePath options:0 error:&saveError];
        if (!saveSuccess) {
            NSLog(@"writeToFile failed with error %@", saveError);
        }
        
        NSMutableString *tempDocumentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] mutableCopy];
        
        NSString *tempFileSavePath=[tempDocumentsDirectory stringByAppendingPathComponent:documentPathComponent];
        BOOL saveSuccessTemp=[UIImageJPEGRepresentation(uncompressedImage, compressionQuality) writeToFile:tempFileSavePath options:0 error:&saveError];
        if (!saveSuccessTemp) {
            NSLog(@"writeToFile failed with error in temp %@", saveError);
        }
    }
    
    if ([self.imagesDelegate respondsToSelector:@selector(imagesCaptured:)]) {
        [self.imagesDelegate imagesCaptured:YES];
    }
    
   // [self dismissViewControllerAnimated:YES completion:^{
  //  }];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)orientationChanged:(NSNotification *)notification {
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    else if (deviceOrientation == UIInterfaceOrientationPortrait)
    {
        [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    else if (deviceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
    }
    else if (deviceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        [CapturePreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
    }
}

-(IBAction)cancelButtonPressed:(id)sender
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController popViewControllerAnimated:NO];

    /*takephotoButton.userInteractionEnabled=YES;
    
    if (imagesCount>=5) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Cannot Take Photos" andMessage:@"Maximum limit reached." withController:self];
    }
    else {
        [self.imagePreview setHidden:YES];
        cameraControlsView.hidden=NO;
        
        [cameraView setHidden:NO];
        [cameraBlackStripImage setHidden:NO];
        [self.takephotoButton setHidden:NO];
        [cameraCancelButton setHidden:NO];
        
        [self.usephotoButton setHidden:YES];
        [self.cancelphotoButton setHidden:YES];
        reTakeButton.hidden=YES;
        
        imagesCollectionView.hidden=YES;
        footerView.hidden=YES;
        
    }*/
}

- (void) dealloc
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [device removeObserver:self forKeyPath:@"adjustingFocus"];
    
    self.session = nil;
    self.image = nil;
    
}
- (NSString *) timeInMiliSeconds
{
    NSDate *date = [NSDate date];
    NSString * timeInMS = [NSString stringWithFormat:@"%lld", [@(floor([date timeIntervalSince1970] * 1000)) longLongValue]];
    return timeInMS;
}
- (void)viewDidUnload
{
    cameraView=nil;
    self.imagePreview=nil;
    
    idleTimer=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

#pragma mark Handling idle timeout

- (void)resetIdleTimer {
    if (!idleTimer) {
        idleTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxIdleTimeSeconds
                                                     target:self
                                                   selector:@selector(idleTimerExceeded)
                                                   userInfo:nil
                                                    repeats:NO];
    }
    else {
        if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < kMaxIdleTimeSeconds-1.0) {
            [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kMaxIdleTimeSeconds]];
        }
    }
}

- (void)idleTimerExceeded {
    idleTimer = nil;
    [idleTimer invalidate];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIResponder *)nextResponder {
    
    [idleTimer invalidate];
    [self resetIdleTimer];
    return [super nextResponder];
}
-(IBAction)cameraCancelButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)retakeButtonTapped:(id)sender {
    
    NSLog(@"retake tapped removing image %lu", (unsigned long)cameraImagesArray.count);
    
    takephotoButton.userInteractionEnabled=YES;
    [cameraImagesArray removeObject:self.currentImageData];
    
    
    NSLog(@"image count after removing %lu",(unsigned long)cameraImagesArray.count);
    imagesCount=imagesCount-1;
    
    [self.imagesDelegate retakeImageTaped:YES];
    
    [self cancelButtonPressed:sender];
}

@end
