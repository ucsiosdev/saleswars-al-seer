//
//  SWAppDelegate.h
//  Salesworx
//
//  Created by msaad on 5/9/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//
//http://install.diawi.com/n81FoA

#import <UIKit/UIKit.h>
#import "SWSession.h"
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>



@interface SWAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate, UNUserNotificationCenterDelegate>
{
    BOOL databaseIsOpen;
    NSTimer *sendLocationTimer;
    NSTimer *checkLocationStartTime;
    NSTimer *V_TRX_BS_Timer;
    NSTimer *backgroundTimer;
    UIBackgroundTaskIdentifier backgroundUpdateTask;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) SWSessionManager *sessionManager;
@property (nonatomic, strong) LicensingViewController *licenseVC;
@property (nonatomic, strong) SWNewActivationViewController *activationManager;
@property (strong, nonatomic) UINavigationController *mainNavigationController;
@property (strong, nonatomic) UINavigationController *menuNavigationController;
@property(strong,nonatomic)NSMutableString * itemCodeString;
@property (strong, nonatomic) NSIndexPath *selectedMenuIndexpath;
@property (assign, nonatomic) BOOL isBackgroundMode;

@property(strong,nonatomic)CLLocationManager *locationManager;

@property(nonatomic)CLLocation * currentLocation;

@property (nonatomic,strong) NSString * currentExceutingDataUploadProcess;


-(void)saveLocationData:(BOOL)isVisitStarted;
-(void)registerforPushNotificationwithDeviceToken:(NSString*)deviceToken;
//708DF295-24DC-4A42-8DB7-09EC3EBE7688
-(void)sendLocationDataToServer;

@end


//http://stackoverflow.com/questions/19360012/app-works-fine-in-ios6-slow-and-jerky-in-ios7
// Product Toket iPad4 : 3ad63f01849e0250ea710a17e471597fa8467efbf82aff26f4f3b345e20405b6
