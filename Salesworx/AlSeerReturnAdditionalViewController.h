//
//  AlSeerReturnAdditionalViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlSeerSignatureViewController.h"
#import "TblCellRetrunConfirmation.h"
#import "SalesWorxDropShadowView.h"


@interface AlSeerReturnAdditionalViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate, UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate>



{
    NSMutableDictionary *salesOrder;
    NSMutableDictionary *info;
    NSMutableDictionary *returnAdditionalInfoDict;
    NSString *performaOrderRef;
    UIImage *signatureSaveImage;
    NSString *isSignatureOptional;
    
    
    CGRect oldFrame;
    
    
    IBOutlet SalesWorxDropShadowView *customerDetailView;
    IBOutlet SalesWorxDropShadowView *commentDetailView;
    IBOutlet UIView *signatureDetailView;
}

@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerIDLbl;

//@property (strong, nonatomic) IBOutlet UITextField *invoiceTxtFld;

@property (strong, nonatomic) IBOutlet UITextView *commentsTxtView;

@property (strong, nonatomic) IBOutlet UITextField *nameTxtFld;

@property (strong, nonatomic) IBOutlet UIView *customSignatureView;

@property (strong, nonatomic) IBOutlet UITextField *invoiceNumberTxtFld;

@property(strong,nonatomic)AlSeerSignatureViewController * signVc;


@property(strong,nonatomic)NSMutableDictionary* salesOrderDict,*customerDict;


// new connections
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *orderItemView;
@property (strong, nonatomic) IBOutlet UILabel *orderItemCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UILabel *totalOrderValueLabel;
@property (strong, nonatomic) IBOutlet UITableView *orderItemTableView;
@property (strong, nonatomic) IBOutlet UILabel *customerAvailableBalanceLbl;
@property(strong,nonatomic)NSArray * arrReturnItem;
@property (strong, nonatomic) IBOutlet UILabel *lblAvilableBalStatic;
@property (strong, nonatomic) IBOutlet UILabel *lblInvoiceNumberStatic;
@property (strong, nonatomic) IBOutlet UILabel *lblSigneeNameStatic;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentStatic;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblReturningItemsStatic;
@property (strong, nonatomic) IBOutlet UILabel *lblDetails;

@end
