//
//  TextTypeQuestionView.h
//  SWPlatform
//
//  Created by msaad on 1/10/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"

@interface TextTypeQuestionView : UIView <UITableViewDelegate, UITableViewDataSource ,UITextFieldDelegate>
{
    UITableView *tableViewController;
    NSString *answerString;
    
}
@property (nonatomic, strong) UITableView *tableViewController;

@end