//
//  CustomerOrderHistoryCell.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerOrderHistoryCell.h"
#import "SWFoundation.h"

@implementation CustomerOrderHistoryCell


@synthesize delegate= _delegate;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        if (refLabel) {
            refLabel=nil;
        }
        if (dateLabel) {
            dateLabel=nil;
        }
        if (amountLabel) {
            amountLabel=nil;
        }
        if (statusLabel) {
            statusLabel=nil;
        }
        refLabel=[[UILabel alloc] initWithFrame:CGRectZero] ;
        dateLabel=[[UILabel alloc] initWithFrame:CGRectZero] ;
        amountLabel=[[UILabel alloc] initWithFrame:CGRectZero] ;
        statusLabel=[[UILabel alloc] initWithFrame:CGRectZero] ;
        
        [refLabel setBackgroundColor:[UIColor clearColor]];
        [dateLabel setBackgroundColor:[UIColor clearColor]];
        [amountLabel setBackgroundColor:[UIColor clearColor]];
        [statusLabel setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:refLabel];
        [self.contentView addSubview:dateLabel];
        [self.contentView addSubview:amountLabel];
        [self.contentView addSubview:statusLabel];
        
        [refLabel setFont:LabelFieldSemiFontOfSiz(16.0f)];
        [dateLabel setFont:LabelFieldSemiFontOfSiz(16.0f)];
        [amountLabel setFont:LabelFieldSemiFontOfSiz(16.0f)];
        [statusLabel setFont:LabelFieldSemiFontOfSiz(16.0f)];

    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect bounds = [self.contentView bounds];
    
    [refLabel setFrame:CGRectMake(10, 10, 200, 25)];
    [dateLabel setFrame:CGRectMake(bounds.size.width - 150, 10, 200, 25)];
    [amountLabel setFrame:CGRectMake(220, 10, 200, 25)];
    [statusLabel setFrame:CGRectMake(bounds.size.width - 250, 10, 200, 25)];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)bindInvoice:(NSDictionary *)i {
    invoice=i;
    
    
    [refLabel setText:[NSString stringWithFormat:@"%@", [invoice objectForKey:@"Orig_Sys_Document_Ref"]]];
    [dateLabel setText:[NSString stringWithFormat:@"%@", [invoice dateStringForKey:@"Creation_Date" withDateFormat:NSDateFormatterMediumStyle]]];
    
    [statusLabel setText:[NSString stringWithFormat:@"%@", [invoice objectForKey:@"ERP_Status"]]];
    
    [amountLabel setText:[invoice currencyStringForKey:@"Transaction_Amt"]];
    
}



- (void)itemsAction{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:invoice];
    [dic setObject:NSStringFromCGRect(self.frame) forKey:@"SenderFrame"];
    [self.delegate customerOrderHistoryCellDidSelectItems:self withInvoice:dic];
}

- (void)invoiceAction{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:invoice];
    [dic setObject:NSStringFromCGRect(self.frame) forKey:@"SenderFrame"];

    [self.delegate customerOrderHistoryCellDidSelectInvoice:self withInvoice:dic];
}

@end
