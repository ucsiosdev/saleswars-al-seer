//
//  GenCustListViewController.h
//  SWPlatform
//
//  Created by msaad on 1/9/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWLoadingView.h"
#import "SWPlatform.h"
#import "ZBarSDK.h"
#import "Singleton.h"


@interface GenCustListViewController : SWViewController < UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UIPopoverControllerDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate> {
    NSArray *customerList;
    
    UITableView *tableView;
    UISearchBar *searchBar;
    NSMutableArray *searchData;
    
    UIPopoverController *locationFilterPopOver;
    int totalRecords;

    
    SEL action;
    
    SWLoadingView *loadingView;
    UILabel *infoLabel;
    ZBarReaderViewController *reader;
    //
    
    UIBarButtonItem *flexibleSpace ;
    
    UIBarButtonItem *totalLabelButton ;
    
    
    
    UIBarButtonItem *displayActionBarButton  ;
    
    UIBarButtonItem *displayMapBarButton ;
    

    UIView *myBackgroundView;
    NSMutableDictionary *sectionList;
    NSMutableDictionary *sectionSearch;
    
    
    //sample survey button
    
    

}
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


@end
