//
//  SWGridViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 6/25/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWFoundation.h"



@interface SWGridViewController : SWViewController < GridViewDataSource, GridViewDelegate > {
    NSArray *items;
    GridView *gridView;
}

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) GridView *gridView;

@end
