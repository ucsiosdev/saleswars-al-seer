//
//  FOCView.m
//  SWCustomer
//
//  Created by msaad on 12/19/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "FOCView.h"

@implementation FOCView

@synthesize productView;

- (id)initWithCustomer:(NSDictionary *)product andFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self loadView];
    }
    return self;
}

-(void)loadView
{
    self.productView=nil;
    self.productView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, self.frame.size.width-20, self.frame.size.height-10)] ;
   
    self.productView.backgroundColor = [UIColor clearColor];
    [self.productView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.productView.layer setBorderWidth:1.0f];
    
    ProductListViewController *productListViewController = [[ProductListViewController alloc] initWithCategory:[SWDefaults productCategory]] ;
    [productListViewController setTarget:self];
    [productListViewController setAction:@selector(productSelected:)];
    
    self.productView = productListViewController.view;
    
    [self addSubview:self.productView];

    
}
- (void)presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"]) {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
