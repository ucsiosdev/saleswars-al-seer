
//
//  SalesSummaryViewController.h
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "CustomersListViewController.h"
#import "CustomerPopOverViewController.h"

@interface SalesSummaryViewController : FTPBaseViewController <GridViewDataSource,GridViewDelegate,UIPopoverControllerDelegate>
{
    GridView *gridView;
    IBOutlet UITableView *filterTableView;
    IBOutlet UIView *backGroundView;
    
    IBOutlet UITableView *salesSummaryTableView;
    UIPopoverController *currencyTypePopOver;
    CustomersListViewController *currencyTypeViewController;
    NSMutableDictionary *customerDict;
    
    NSDate *selectedDate;
    UIPopoverController *datePickerPopOver;
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    
    CustomerPopOverViewController *customVC;
    UIPopoverController *customPopOver;
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    NSMutableArray *salesSummaryArray;
    NSMutableArray *filteredSalesSummaryArray;

    IBOutlet UIButton *filterButton;
    NSMutableDictionary * previousFilteredParameters;
    
    UIPopoverController *filterPopOverController;

}
@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@end

