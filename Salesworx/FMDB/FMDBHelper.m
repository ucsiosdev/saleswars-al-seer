//
//  FMDBHelper.m
//  SmartTracker
//
//  Created by Priyanka Bhatia on 7/25/13.
//  Copyright (c) 2013 UCS. All rights reserved.
//
/*
#import "FMDatabase.h"
#import "FMResultSet.h"
-
@implementation FMDBHelper


- (FMDatabase *)getDatabase
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"SNL.sqlite"]];
    
    return db;
}



#pragma mark - NEWS ITEM

- (BOOL)insertNewsItem:(NewsItem *)newsItem withLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        
        success =  [db executeUpdate:@"INSERT   INTO TBL_News (Row_ID, NEWS_ID, NEWS_URL, NEWS_TITLE, NEWS_DESCRIPTION, NEWS_MAIN_DESCRIPTION, NEWS_DETAIL_DESCRIPTION, NEWS_IMAGE_URL, NEWS_IMAGE_DESCRIPTION, NEWS_VIDEO_URL, NEWS_VIDEO_DESCRIPTION, NEWS_OPEN_MODE_TYPE, NEWS_PUBLISHED_SINCE, Lang_Code, Last_Updated_At) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",newsItem.ID, newsItem.ID, newsItem.URL, newsItem.title, newsItem.description, newsItem.mainDescription, newsItem.mainDescription, newsItem.imageURL, newsItem.imageDescription, newsItem.videoURL, newsItem.videoDescription, newsItem.openModeType, newsItem.publishedSince, langCode, lastModificationDate, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}

- (NSMutableArray *)getAllNewsItemsWithLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSMutableArray *newsItems = [[NSMutableArray alloc] init];
    @try
    {
        NSMutableString *query = [NSMutableString stringWithString:[NSString stringWithFormat:@"SELECT * FROM TBL_News WHERE Lang_Code = '%@'",langCode]];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            NewsItem *newsItem = [[NewsItem alloc] init];
            newsItem.ID = [results stringForColumn:@"NEWS_ID"];
            newsItem.title = [results stringForColumn:@"NEWS_TITLE"];
            newsItem.URL = [results stringForColumn:@"NEWS_URL"];
            newsItem.description = [results stringForColumn:@"NEWS_DESCRIPTION"];
            newsItem.mainDescription = [results stringForColumn:@"NEWS_MAIN_DESCRIPTION"];
            newsItem.imageURL = [results stringForColumn:@"NEWS_IMAGE_URL"];
            newsItem.imageDescription = [results stringForColumn:@"NEWS_IMAGE_DESCRIPTION"];
            newsItem.videoURL = [results stringForColumn:@"NEWS_VIDEO_URL"];
            newsItem.videoDescription = [results stringForColumn:@"NEWS_VIDEO_DESCRIPTION"];
            newsItem.openModeType = [results stringForColumn:@"NEWS_OPEN_MODE_TYPE"];
            newsItem.publishedSince = [results stringForColumn:@"NEWS_PUBLISHED_SINCE"];
            newsItem.lastUpdatedAt = [dateFormatter dateFromString:[results stringForColumn:@"LAST_UPDATED_AT"]];
            
            [newsItems addObject:newsItem];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return newsItems;
}

- (NewsItem *)getNewsItem: (NewsItem *)newsItem
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        NSMutableString *query = [NSMutableString stringWithString:[NSString stringWithFormat:@"SELECT * FROM TBL_News WHERE NEWS_ID = '%@'", newsItem.ID]];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            newsItem.ID = [results stringForColumn:@"NEWS_ID"];
            newsItem.title = [results stringForColumn:@"NEWS_TITLE"];
            newsItem.URL = [results stringForColumn:@"NEWS_URL"];
            newsItem.description = [results stringForColumn:@"NEWS_DESCRIPTION"];
            newsItem.mainDescription = [results stringForColumn:@"NEWS_MAIN_DESCRIPTION"];
            newsItem.imageURL = [results stringForColumn:@"NEWS_IMAGE_URL"];
            newsItem.imageDescription = [results stringForColumn:@"NEWS_IMAGE_DESCRIPTION"];
            newsItem.videoURL = [results stringForColumn:@"NEWS_VIDEO_URL"];
            newsItem.videoDescription = [results stringForColumn:@"NEWS_VIDEO_DESCRIPTION"];
            newsItem.openModeType = [results stringForColumn:@"NEWS_OPEN_MODE_TYPE"];
            newsItem.publishedSince = [results stringForColumn:@"NEWS_PUBLISHED_SINCE"];
            
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining news from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return newsItem;
}

-(BOOL)updateNewsItem:(NewsItem *)newsItem withLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"UPDATE TBL_News SET NEWS_URL = ?, NEWS_TITLE = ?, NEWS_DESCRIPTION = ?, NEWS_MAIN_DESCRIPTION = ?, NEWS_DETAIL_DESCRIPTION = ?, NEWS_IMAGE_URL = ?, NEWS_IMAGE_DESCRIPTION = ?, NEWS_VIDEO_URL = ?, NEWS_VIDEO_DESCRIPTION = ?, NEWS_OPEN_MODE_TYPE = ?, NEWS_PUBLISHED_SINCE = ?, Lang_Code = ?, Last_Updated_At = ? WHERE NEWS_ID = ?", newsItem.URL, newsItem.title, newsItem.description, newsItem.mainDescription, newsItem.mainDescription, newsItem.imageURL, newsItem.imageDescription, newsItem.videoURL, newsItem.videoDescription, newsItem.openModeType, newsItem.publishedSince, langCode, lastModificationDate, newsItem.ID, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while updating news record in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}

- (BOOL)deleteAllNewsItemsWithLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"DELETE FROM TBL_News WHERE Lang_Code = ?", langCode];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}

#pragma mark - EVENT ITEM

- (BOOL)insertEventItem:(EventItem *)eventItem withLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        
        success =  [db executeUpdate:@"INSERT   INTO TBL_Events (Row_ID, EVENTS_ID, EVENTS_TITLE, EVENTS_DESCRIPTION, EVENTS_START_DATE, EVENTS_END_DATE, EVENTS_TYPE, EVENTS_LOCATION, EVENTS_PLACE, Lang_Code, Last_Updated_At) VALUES (?,?,?,?,?,?,?,?,?,?,?)",eventItem.ID, eventItem.ID, eventItem.title, eventItem.description, eventItem.startDayAndTime, eventItem.endDayAndTime, eventItem.eventType, eventItem.location, eventItem.place,  langCode, lastModificationDate, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while adding event to database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}

- (NSMutableArray *)getAllEventItemsWithLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSMutableArray *eventItems = [[NSMutableArray alloc] init];
    @try
    {
        NSDateFormatter* dateFormatter2 = [[NSDateFormatter alloc] init];
        dateFormatter2.dateFormat = @"MM/dd/yyyy";
        
        NSMutableString *query = [NSMutableString stringWithString:[NSString stringWithFormat:@"SELECT * FROM TBL_Events WHERE Lang_Code = '%@'",langCode]];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            EventItem *eventItem = [[EventItem alloc] init];
            eventItem.ID = [results stringForColumn:@"EVENTS_ID"];
            eventItem.title = [results stringForColumn:@"EVENTS_TITLE"];
            eventItem.description = [results stringForColumn:@"EVENTS_DESCRIPTION"];
            eventItem.startDayAndTime = [results stringForColumn:@"EVENTS_START_DATE"];
            eventItem.endDayAndTime = [results stringForColumn:@"EVENTS_END_DATE"];
            eventItem.eventType = [results stringForColumn:@"EVENTS_TYPE"];
            eventItem.location = [results stringForColumn:@"EVENTS_LOCATION"];
            eventItem.place = [results stringForColumn:@"EVENTS_PLACE"];
            eventItem.lastUpdatedAt = [dateFormatter dateFromString:[results stringForColumn:@"LAST_UPDATED_AT"]];
            
             eventItem.startDateForSorting = [dateFormatter2 dateFromString:[[[eventItem.startDayAndTime componentsSeparatedByString:@" "] objectAtIndex:0]  stringByReplacingOccurrencesOfString:@"\\" withString:@""]];
            
            [eventItems addObject:eventItem];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining events from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return eventItems;
}

- (EventItem *)getEventItem:(EventItem *)eventItem
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    @try
    {
        NSMutableString *query = [NSMutableString stringWithString:[NSString stringWithFormat:@"SELECT * FROM TBL_Events WHERE EVENT_ID = '%@'", eventItem.ID]];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            eventItem.ID = [results stringForColumn:@"EVENTS_ID"];
            eventItem.title = [results stringForColumn:@"EVENTS_TITLE"];
            eventItem.description = [results stringForColumn:@"EVENTS_DESCRIPTION"];
            eventItem.startDayAndTime = [results stringForColumn:@"EVENTS_START_DATE"];
            eventItem.endDayAndTime = [results stringForColumn:@"EVENTS_END_DATE"];
            eventItem.eventType = [results stringForColumn:@"EVENTS_TYPE"];
            eventItem.location = [results stringForColumn:@"EVENTS_LOCATION"];
            eventItem.place = [results stringForColumn:@"EVENTS_PLACE"];
            
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining event from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return eventItem;
}

-(BOOL)updateEventItem:(EventItem *)eventItem withLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"UPDATE TBL_Events SET EVENTS_TITLE = ?, EVENTS_DESCRIPTION = ?, EVENTS_START_DATE = ?, EVENTS_END_DATE = ?, EVENTS_TYPE = ?, EVENTS_LOCATION = ?, EVENTS_PLACE = ?, Lang_Code = ?, Last_Updated_At = ? WHERE EVENTS_ID = ?", eventItem.title, eventItem.description, eventItem.startDayAndTime, eventItem.endDayAndTime, eventItem.eventType, eventItem.location, eventItem.place,  langCode, lastModificationDate, eventItem.ID, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while updating event record in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}

- (BOOL)deleteAllEventItemsWithLanguageCode:(NSString *)langCode
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"DELETE FROM TBL_Events WHERE Lang_Code = ?", langCode];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while deleting events from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}

#pragma mark - HEALTH STATS

- (BOOL)insertHealthStats:(HealthStats *)stats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        
        success =  [db executeUpdate:@"INSERT   INTO TBL_HealthStats (USER_ID, WEIGHT, CALORIES, HEIGHT, WAIST, HIP, LAST_UPDATE) VALUES (?,?,?,?,?,?,?)", stats.userID, stats.weight, stats.calories, stats.height, stats.waist, stats.hip, lastModificationDate,  nil];
    
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while adding HEALTH STATS to database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}

- (NSMutableArray *)getAllHealthStats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    NSMutableArray *availableStats = [[NSMutableArray alloc] init];
    @try
    {
        NSMutableString *query = [NSMutableString stringWithString:@"SELECT * FROM TBL_HealthStats"];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            HealthStats *stats = [[HealthStats alloc] init];
            stats.userID = [results stringForColumn:@"USER_ID"];
            stats.weight = [results stringForColumn:@"WEIGHT"];
            stats.calories = [results stringForColumn:@"CALORIES"];
            stats.height = [results stringForColumn:@"HEIGHT"];
            stats.waist = [results stringForColumn:@"WAIST"];
            stats.hip = [results stringForColumn:@"HIP"];
            stats.lastUpdate = [dateFormatter dateFromString:[results stringForColumn:@"LAST_UPDATE"]];
            
            [availableStats addObject:stats];
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining HEALTH STATS from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return availableStats;
}

- (HealthStats *)getHealthStats: (HealthStats *)stats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    @try
    {
        NSMutableString *query = [NSMutableString stringWithString:[NSString stringWithFormat:@"SELECT * FROM TBL_HealthStats WHERE USER_ID = '%@'", stats.userID]];
        FMResultSet *results = [db executeQuery:query];
        while ([results next])
        {
            stats.weight = [results stringForColumn:@"WEIGHT"];
            stats.calories = [results stringForColumn:@"CALORIES"];
            stats.height = [results stringForColumn:@"HEIGHT"];
            stats.waist = [results stringForColumn:@"WAIST"];
            stats.hip = [results stringForColumn:@"HIP"];
            stats.lastUpdate = [dateFormatter dateFromString:[results stringForColumn:@"LAST_UPDATE"]];
            
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining HEALTH STATS from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    
    return stats;
}

-(BOOL)updateHealthStats:(HealthStats *)stats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSDate *now = [NSDate date];
    NSString *lastModificationDate = [dateFormatter stringFromDate:now];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"UPDATE TBL_HealthStats SET WEIGHT = ?, CALORIES = ?, HEIGHT = ?, WAIST = ?, HIP = ?, LAST_UPDATE =? WHERE USER_ID = ?", stats.weight, stats.calories, stats.height, stats.waist, stats.hip, lastModificationDate, stats.userID, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while updating HEALTH STATS in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}

- (BOOL)deleteHealthStats:(HealthStats *)stats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"DELETE FROM TBL_HealthStats WHERE USER_ID = ?", stats.userID, nil];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while deleting HEALTH STATS from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;

}

- (BOOL)deleteAllHealthStats
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try {
        success =  [db executeUpdate:@"DELETE FROM TBL_HealthStats"];
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while deleting HEALTH STATS from database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
    
    return success;
}
 */
#import "FMDBHelper.h"

#import "SWDefaults.h"
@implementation FMDBHelper


+ (FMDatabase *)getDatabase
{
    
    //iOS 8 support
    NSString *documentDir;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        documentDir=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        documentDir = [paths objectAtIndex:0];
    }
    
    
//    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    NSString*thePath=[[NSBundle mainBundle] pathForResource:@"abc" ofType:@"pdf"];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
    return db;
}



#pragma mark - NEWS ITEM

+ (BOOL)executeNonQuery:(NSString *)sqlQuery
{
    FMDatabase *db = [self getDatabase];
    [db open];
    
    BOOL success = NO;
    @try
    {
        success =  [db executeUpdate:sqlQuery];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception while obtaining data from database: %@", exception.reason);
    }
    @finally
    {
        [db close];
    }
    return success;
}

+ (NSMutableArray *)executeQuery:(NSString *)sqlQuery
{
    @autoreleasepool
    {
        FMDatabase *db = [self getDatabase];
        [db open];
        
        NSMutableArray *newsItems = [NSMutableArray array];
        @try
        {
            FMResultSet *results = [db executeQuery:sqlQuery];
            while ([results next])
            {
                [newsItems addObject:[results resultDictionary]];
            }
            [results close];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception while obtaining news from database: %@", exception.reason);
        }
        @finally
        {
            [db close];
        }
        return newsItems;
    }
}


@end
 

