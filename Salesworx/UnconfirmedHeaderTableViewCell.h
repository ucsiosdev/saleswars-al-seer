//
//  UnconfirmedHeaderTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/15/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnconfirmedHeaderTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *docRefNumLbl;
@property (strong, nonatomic) IBOutlet UILabel *orderAmountLbl;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property (strong, nonatomic) IBOutlet UILabel *custNameLbl;
@end
