//
//  AlSeerProductDetailViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxDropShadowView.h"

@interface AlSeerProductDetailViewController_2 : UIViewController<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UIPageViewControllerDelegate>

{
    NSArray* productStockArray;
    UITapGestureRecognizer *doubleTapGesture;
    
    UIImageView *fullscreenProductImageView;
    
    NSMutableArray * imagesArray,*allMediaFilesArray,*pdfArray,*videoArray,*pptArray;
    UIScrollView *fullScreenproductImagesScrollView;
    UIPageControl* fullScreenProductImagesPageControl;
    NSInteger conversionRate;
    
    NSString* totalStock;
    
    NSInteger tempStock;
    
    IBOutlet UILabel *barcodeLbl;
    UIView* bgView;
}

@property(strong,nonatomic)NSDictionary *productDict,*priceDict;
@property (strong, nonatomic) IBOutlet UILabel *itemNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *itemDescLbl;
@property (strong, nonatomic) IBOutlet UIScrollView *productImagesScrollView;

@property (strong, nonatomic) IBOutlet UILabel *brandLbl;
@property (strong, nonatomic) IBOutlet UILabel *agencyLbl;
@property (strong, nonatomic) IBOutlet UILabel *unitPriceLbl;
@property (strong, nonatomic) IBOutlet UIPageControl *productPageControl;

//@property (strong, nonatomic) IBOutlet UIView *customProductStockView;

@property (strong, nonatomic) IBOutlet UITableView *stockTableVIew;
@property (strong, nonatomic) IBOutlet UILabel *totalStockLbl;

@property (strong, nonatomic) IBOutlet UILabel *baseUOMLbl;
//@property (strong, nonatomic) IBOutlet UIImageView *productImageView;


-(void)fetchMediaFilesFromDocuments;
-(void)seperateMediaFilesIntoArrays;

@property (strong, nonatomic) IBOutlet UIView *viewAlignToX;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *viewParentScroolView;
@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *viewParentProductDetails;

@property (strong, nonatomic) IBOutlet SalesWorxDropShadowView *viewParentTbl;



//@property (strong, nonatomic) IBOutlet UIImageView *fullscreenProductImageView;

@end
