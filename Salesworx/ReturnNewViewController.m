//
//  ReturnNewViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 2/11/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "ReturnNewViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ReturnTypeViewController.h"
#import "ProductBonusViewController.h"
@interface ReturnNewViewController ()
{
    NSString *dateString;
}
@end

#define NUMERIC                 @"1234567890"

@implementation ReturnNewViewController
@synthesize customerDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //s .tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    productTableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectZero];
    
    self.title = NSLocalizedString(@"Return", nil);
    
    oldSalesOrderVC = [[ReturnViewController alloc] initWithCustomer:customerDict andCategory:[SWDefaults productCategory]] ;
    oldSalesOrderVC.view.frame = bottomOrderView.bounds;
    oldSalesOrderVC.view.clipsToBounds = YES;
    [bottomOrderView addSubview:oldSalesOrderVC.view];
    [oldSalesOrderVC didMoveToParentViewController:self];
    [self addChildViewController:oldSalesOrderVC];

    productArray =[NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:[SWDefaults productCategory]]];
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray )
    {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        
        if ( theMutableArray == nil )
        {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];
    
    bSearchIsOn = NO;
    
    lblCustomerName.text = [customerDict stringForKey:@"Customer_Name"];
    lblCustomerName.font = RegularFontOfSize(26);
    lblProductName.font = BoldSemiFontOfSize(16);
    lblExpiryDate.font =LightFontOfSize(14);
    lblLotNumber.font=LightFontOfSize(14);
    lblProductBonus.font=LightFontOfSize(14);
    lblProductQty.font=LightFontOfSize(14);
    lblRetailPrice.font=LightFontOfSize(14);
    lblReturnType.font=LightFontOfSize(14);
    lblWholePrice.font=LightFontOfSize(14);
    btnExpiryDate.titleLabel.font = BoldSemiFontOfSize(12);
    btnReturnType.titleLabel.font = BoldSemiFontOfSize(12);
    
    txtDefBonus.textColor=UIColorFromRGB(0x4A5866);
    txtLot.textColor=UIColorFromRGB(0x4A5866);
    txtProductQty.textColor=UIColorFromRGB(0x4A5866);
    lblRPAmount.textColor=UIColorFromRGB(0x4A5866);
    lblWPAmount.textColor=UIColorFromRGB(0x4A5866);
    btnExpiryDate.titleLabel.textColor = UIColorFromRGB(0x4A5866);
    btnReturnType.titleLabel.textColor = UIColorFromRGB(0x4A5866);

    
    lblWPAmount.font=BoldSemiFontOfSize(12);
    lblRPAmount.font=BoldSemiFontOfSize(12);

    lblWPAmount.textColor=UIColorFromRGB(0x4687281);
    lblRPAmount.textColor=UIColorFromRGB(0x4687281);


    candySearchBar.backgroundColor = UIColorFromRGB(0xF6F7FB);
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)] ];
    
    
    [SWDefaults setPaymentType:nil];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    dateString =  [formatter stringFromDate:[NSDate date]];
    
    oldSalesOrderVC.gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    if (@available(iOS 15.0, *)) {
        productTableView.sectionHeaderTopPadding = 0;
        oldSalesOrderVC.gridView.tableView.sectionHeaderTopPadding = 0;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.toolbarHidden = YES;
    oldSalesOrderVC.delegate=self;
    oldSalesOrderVC.gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
}
-(void)viewDidDisappear:(BOOL)animated
{
    oldSalesOrderVC.delegate = nil;
}

#pragma mark -

- (void)selectedProduct:(NSMutableDictionary *)product
{
    //NSLog(@"SPRE %@",product);
    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    if (product!=nil) {
        mainProductDict=nil;
        mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
        
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        lblProductName.text = [mainProductDict stringForKey:@"Description"];
        txtProductQty.text = [mainProductDict stringForKey:@"Qty"];
        lblWPAmount.text = [[mainProductDict stringForKey:@"Net_Price"] currencyString];
        
        lblRPAmount.text = [[mainProductDict stringForKey:@"List_Price"] currencyString];
        txtDefBonus.text = [mainProductDict stringForKey:@"Bonus"];
        txtLot.text = [mainProductDict stringForKey:@"lot"];
        [btnExpiryDate setTitle:[mainProductDict stringForKey:@"EXPDate"] forState:UIControlStateNormal];
        [btnReturnType setTitle:[mainProductDict stringForKey:@"reason"] forState:UIControlStateNormal];
        [addBtn setTitle:@"Update" forState:UIControlStateNormal];
    }
    else
    {
        mainProductDict=nil;
        mainProductDict = [NSMutableDictionary dictionaryWithDictionary:product];
        
        NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
        [mainProductDict setValue:itemID forKey:@"ItemID"];
        
        lblProductName.text = @"Product Name";
        txtProductQty.text = @"";
        lblWPAmount.text = @"";
        lblRPAmount.text = @"";
        txtDefBonus.text =@"";
        txtLot.text = @"";
        [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
        [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
    }
}

- (void)closeVisit:(id)sender {
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        
    }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:@"Would you like to close this returns?" andActions:actionsArray withController:self];
}
#pragma mark - UITableView
-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    [productTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (!shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [productTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        NSInteger numOfRows = [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [productTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    
    [productTableView endUpdates];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            return 1;
            
        }
        else
        {
            return [finalProductArray count];
        }
    }
    else
    {
        return 1;
        
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return [filteredCandyArray count];
            
        } else {
            return [_collapsedSections containsObject:@(section)] ? [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count] :0 ;
            
        }
    }
    else
    {
        return [productArray count];
        
    }
    
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag==1)
    {
        if (bSearchIsOn) {
            return 0;
        }
        else
        {
            return 40.0;
        }
    }
    else
    {
        return 0;
    }
    
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (tv.tag==1)
    {
        if (bSearchIsOn) {
            return nil;
        }
        else
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
            NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
            
            UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
            result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
            [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
            result.backgroundColor=UIColorFromRGB(0xE0E5EC);
            [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
            result.tag = s;
            [result.layer setBorderColor:[UIColor whiteColor].CGColor];
            [result.layer setBorderWidth:1.0f];
            result.titleLabel.font=BoldSemiFontOfSize(16);
            [result setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
            
            [headerView addSubview:result];
            return headerView;
        }
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.textLabel.numberOfLines=2;
        cell.textLabel.font=RegularFontOfSize(5);
        cell.backgroundColor=UIColorFromRGB(0xF6F7FB);
        cell.textLabel.textColor=UIColorFromRGB(0x687281);
        cell.textLabel.textColor=[UIColor darkTextColor ];
        cell.textLabel.font  =  LightFontOfSize(14) ;
    }
    
    
    
    if (tableView.tag==1)
    {
        if (bSearchIsOn)
        {
            NSDictionary * row = [filteredCandyArray objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
        else
        {
            NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
            NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
            NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
            cell.textLabel.text = [row stringForKey:@"Description"];
            if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"]) {
                cell.textLabel.textColor = [UIColor redColor];
            }
            else
            {
                cell.textLabel.textColor=[UIColor darkTextColor ];
            }
        }
    }
    else
    {
        NSDictionary * row = [productArray objectAtIndex:indexPath.row];
        cell.textLabel.text = [row stringForKey:@"Description"];
        if ([[row stringForKey:@"Lot_Qty"] isEqualToString:@"<null>"])
        {
            cell.textLabel.textColor = [UIColor redColor];
        }
        else
        {
            cell.textLabel.textColor=[UIColor darkTextColor ];
        }
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{


}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    txtProductQty.text=@"";
    txtDefBonus.text=@"";
    txtLot.text=@"";
    lblRPAmount.text=@"";
    lblWPAmount.text=@"";
    [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
    [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
    [addBtn setTitle:@"Add" forState:UIControlStateNormal];


    NSDictionary * row ;
    if (bSearchIsOn)
    {
        row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }
    
    
    //retrieving incorrect data here
    
    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
    mainProductDict=nil;
    mainProductDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];

//    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[mainProductDict stringForKey:@"Inventory_Item_ID"]]];

    
    
      [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"Inventory_Item_ID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    
    
//    NSString *itemID = [mainProductDict stringForKey:@"Inventory_Item_ID"];
//    [mainProductDict setValue:itemID forKey:@"ItemID"];
//    
//    lblProductName.text = [mainProductDict stringForKey:@"Description"];
//    lblWPAmount.text = [[mainProductDict stringForKey:@"Unit_Selling_Price"] currencyString];
//    lblRPAmount.text = [[mainProductDict stringForKey:@"Unit_List_Price"] currencyString];
//    [mainProductDict setValue:dateString forKey:@"EXPDate"];
//    [btnExpiryDate setTitle:[mainProductDict stringForKey:@"EXPDate"] forState:UIControlStateNormal];
    
    [bonusBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
    [bonusBtn setSelected:YES];
    
}


//test method


- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customerDict stringForKey:@"Price_List_ID"]])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [mainProductDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
    }
    
    NSLog(@"check unit price here %@%@", [mainProductDict valueForKey:@"Net_Price"], [mainProductDict valueForKey:@"List_Price"]);
    
    [lblProductName setText:[mainProductDict stringForKey:@"Description"]];
    [lblWPAmount setText:[[mainProductDict stringForKey:@"Net_Price"] currencyString]];
    [lblRPAmount setText:[[mainProductDict stringForKey:@"List_Price"] currencyString]];
}


#pragma mark - UISearchBar

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        //[searchBar performSelector: @selector(resignFirstResponder) withObject: nil afterDelay: 0];
        bSearchIsOn = NO;
        [productTableView reloadData];
        [productTableView setScrollsToTop:YES];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [candySearchBar resignFirstResponder];
    NSUInteger len = [ [candySearchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = candySearchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearch = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearch allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each isDualBonusAppControlsection array
            for (NSString *key in [sectionSearch allKeys])
            {
                [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            [productTableView reloadData];
        }
        
    }
    else
    {
        [ candySearchBar resignFirstResponder ];
    }
}

- (void)updatePrice
{
    int qty = [[mainProductDict stringForKey:@"Qty"] intValue];
    if (qty > 0)
    {
        double totalPrice = (double)qty * ([[mainProductDict objectForKey:@"Unit_Selling_Price"] doubleValue]);
        [mainProductDict setValue:[NSString stringWithFormat:@"%.02f", totalPrice] forKey:@"Price"];
    }
}
-(IBAction)resetAction:(id)sender
{
    lblProductName.text = @"Product Name";
    txtProductQty.text = @"";
    lblWPAmount.text = @"";
    lblRPAmount.text = @"";
    txtDefBonus.text =@"";
    txtLot.text = @"";
    [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
    [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];

    mainProductDict = nil;

    for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
        [productTableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    for (NSIndexPath *indexPath in oldSalesOrderVC.gridView.tableView.indexPathsForSelectedRows) {
        [oldSalesOrderVC.gridView.tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    [addBtn setTitle:@"Add" forState:UIControlStateNormal];

}
-(IBAction)returnTypebuttonAction:(id)sender
{
    [self.view endEditing:YES];
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
    else
    {
        ReturnTypeViewController *collectionTypeViewController = [[ReturnTypeViewController alloc] initWithEXP];
        [collectionTypeViewController setTarget:self];
        [collectionTypeViewController setAction:@selector(returnTypeChanged:)];
        
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:collectionTypeViewController];
        collectionTypePopOver.delegate=self;
        [collectionTypePopOver presentPopoverFromRect:btnReturnType.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}

-(IBAction)expiryDatebuttonAction:(id)sender
{
    [self.view endEditing:YES];
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
    else
    {
        SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
        datePickerViewController.isRoute=NO;
        
        collectionTypePopOver=[[UIPopoverController alloc] initWithContentViewController:datePickerViewController];
        collectionTypePopOver.delegate=self;
        [collectionTypePopOver presentPopoverFromRect:btnExpiryDate.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}
-(IBAction)addbuttonAction:(id)sender
{
    if([self validateInput])
    {
        [self.view endEditing:YES];
        for (NSIndexPath *indexPath in productTableView.indexPathsForSelectedRows) {
            [productTableView deselectRowAtIndexPath:indexPath animated:NO];
        }
        isSaveOrder = YES;
        [oldSalesOrderVC productAdded:mainProductDict];
        txtProductQty.text=@"";
        txtDefBonus.text=@"";
        txtLot.text=@"";
        lblRPAmount.text=@"";
        lblWPAmount.text=@"";
        [btnExpiryDate setTitle:@"Expiry Date" forState:UIControlStateNormal];
        [btnReturnType setTitle:@"Select Reason" forState:UIControlStateNormal];
        [addBtn setTitle:@"Add" forState:UIControlStateNormal];
        [lblProductName setText:@"Product Name"];
        mainProductDict=nil;
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please fill all required fields" withController:self];
    }

}
-(IBAction)pullUpbuttonAction:(id)sender
{
    [self.view endEditing:YES];
    if (isStockToggled)
    {
        [productTableView setUserInteractionEnabled:YES];//enable product table selection when draggable view is hidden. OLA!
        isStockToggled=NO;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(1024, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(999, 0, 30, 605) withDuration:0.5];
    }
    else
    {
        [bonusBtn sendActionsForControlEvents:UIControlEventTouchUpInside];
        [bonusBtn setSelected:YES];
        isStockToggled=YES;
        [AnimationUtility moveFrame:dragParentView withFrame:CGRectMake(264, 0, 766, 605) withDuration:0.5];
        [AnimationUtility moveFrame:dragButton withFrame:CGRectMake(239, 0, 30, 605) withDuration:0.5];
    }
}

-(IBAction)bonusButtonAction:(id)sender
{
//    ProductBonusViewController *productBonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
//    productBonusViewController.view.frame = dragChildView.bounds;
//    productBonusViewController.view.clipsToBounds = YES;
//    [dragChildView addSubview:productBonusViewController.view];
//    [productBonusViewController didMoveToParentViewController:self];
//    [self addChildViewController:productBonusViewController];
    
    if (mainProductDict.count!=0)
    {
        ProductBonusViewController *bonusViewController = [[ProductBonusViewController alloc] initWithProduct:mainProductDict] ;
    
        UINavigationController *modalViewNavController = [[UINavigationController alloc] initWithRootViewController:bonusViewController];
        modalViewNavController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        modalViewNavController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:modalViewNavController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
    }
}
- (void)returnTypeChanged:(NSDictionary *)newType
{
    [collectionTypePopOver dismissPopoverAnimated:YES];
    
    //here description is set as reason it shpuld be from TBL_Reason_Codes
    
    [mainProductDict setValue:[newType stringForKey:@"Description"] forKey:@"reason"];
    [mainProductDict setValue:[newType stringForKey:@"RMA_Lot_Type"] forKey:@"RMA_Lot_Type"];
    [btnReturnType setTitle:[mainProductDict stringForKey:@"reason"] forState:UIControlStateNormal];
    collectionTypePopOver.delegate=nil;
    collectionTypePopOver=nil;
}
- (void)dateChanged:(SWDatePickerViewController *)sender {
    
    [collectionTypePopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    [mainProductDict setValue:[formatter stringFromDate:sender.selectedDate] forKey:@"EXPDate"];
    [btnExpiryDate setTitle:[formatter stringFromDate:sender.selectedDate] forState:UIControlStateNormal];
   
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (mainProductDict.count==0)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please select product." withController:self];
        isErrorSelectProduct=YES;
        [self performSelector:@selector(resignedTextField:) withObject:textField afterDelay:0.0];
        
    }
    return YES;
}
-(void)resignedTextField:(UITextField *)textField
{
    [textField resignFirstResponder];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    NSCharacterSet *unacceptedInput = nil;
    textField.text = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    
    if (textField==txtProductQty)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [mainProductDict setValue:textField.text forKey:@"Qty"];
        }
        else
        {
            if (!isErrorSelectProduct) {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Quantity value should have only numeric" withController:self];
            }
            isErrorSelectProduct=NO;
            [mainProductDict setValue:@"" forKey:@"Qty"];
        }
    }
    else if (textField==txtDefBonus)
    {
        if ([[textField.text componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1 && [textField.text intValue]>0)
        {
            [mainProductDict setValue:textField.text forKey:@"Bonus"] ;
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus quantity value should have only numeric" withController:self];
            [mainProductDict setValue:@"" forKey:@"Bonus"];
        }
    }
    else if (textField==txtLot)
    {
        [mainProductDict setValue:textField.text forKey:@"lot"];
    }
    txtProductQty.text=[mainProductDict stringForKey:@"Qty"];
    txtDefBonus.text=[mainProductDict stringForKey:@"Bonus"];
    txtLot.text=[mainProductDict stringForKey:@"lot"];
    [self updatePrice];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    collectionTypePopOver.delegate=nil;

    collectionTypePopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}

- (BOOL)validateInput
{
    AppControl *appControl = [AppControl retrieveSingleton];
    if ([appControl.IS_LOT_OPTIONAL isEqualToString:@"Y"])
    {
        if (![mainProductDict objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
    }
    else
    {
        if (![mainProductDict objectForKey:@"reason"])
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"reason"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"Qty"] length] < 1)
        {
            return NO;
        }
        else if ([[mainProductDict objectForKey:@"lot"] length] < 1)
        {
            return NO;
        }
        
    }
    return YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
