//
//  PointofPurchaseViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "pointofPurchaseTableViewCell.h"
#import "SKSTableView.h"


@interface PointofPurchaseViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SKSTableViewDelegate,UITextViewDelegate>
{
    NSMutableArray* popArray,*popItemsArray,*popRemarksArray;
    
    
    NSInteger endSection;
    NSInteger didSection;
    BOOL ifOpen;
    NSMutableArray* prevRows;
    
    
    CGRect oldFrame;
    
    CGFloat  lastOffset;
    
    NSInteger currentTxtViewTag;
    
    UIPopoverController * remarksPopOverController;
    
    NSIndexPath *indexPathforPopOver;
    
    
    //This is the index of the cell which will be expanded
    NSInteger selectedIndex;


    
    
    NSInteger cellTag;
    NSMutableArray *selectedIndexpatharray,*previousAddedrows,*subCategoriesArray,*arrayforIndexPath,*textArray;
    
    
    NSMutableArray* categoriesArray,*titlesArray,*blockIndexArray,*subCategoryIndex;


}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property(strong,nonatomic) NSString* currentVisitID;

@property (nonatomic, strong) NSArray *contents;

//@property (strong, nonatomic) IBOutlet UITableView *popTableView;

@property (strong, nonatomic) NSMutableArray *expandedCells;

@end
