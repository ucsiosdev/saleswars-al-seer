//
//  StockInfoTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/1/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockInfoTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *quantityLbl;
@property (strong, nonatomic) IBOutlet UILabel *wareHouseLbl;
@property (strong, nonatomic) IBOutlet UITextField *allocatedTextField;

@property (strong, nonatomic) IBOutlet UILabel *expiryLbl;
@end
