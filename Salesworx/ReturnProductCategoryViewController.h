//
//  ReturnProductCategoryViewController.h
//  Salesworx
//
//  Created by msaad on 5/27/13.
//  Copyright (c) 2013 msaad. All rights reserved.


#import "SWPlatform.h"

@interface ReturnProductCategoryViewController : SWTableViewController  {

    SEL action;
    NSMutableDictionary *customer;
    NSArray *items;
    BOOL isPopover;
    UIView *myBackgroundView;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


- (id)initWithCustomer:(NSDictionary *)customer;

@end
