//
//  RemarksPopViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/10/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemarksPopViewController : UIViewController<UITextViewDelegate>

@property(strong,nonatomic)UIPopoverController * popOver;

@property(nonatomic)NSInteger selectedRow;

@property(strong,nonatomic)NSMutableArray* popItemsArray, *popArray;

@property (strong, nonatomic) IBOutlet UITextView *remarksTxtView;


@end
