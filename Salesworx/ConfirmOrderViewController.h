//
//  ConfirmOrderViewController.h
//  Salesworx
//
//  Created by Saad Ansari on 11/17/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "SWTableViewCell.h"

@interface ConfirmOrderViewController : SWTableViewController<SWTableViewCellDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *types;
    SEL action;
    NSString *parentType;
    NSMutableArray *selectedOrders;
    UILabel *companyNameLabel;
    UILabel *companyNameLabel1;
    UILabel *companyNameLabel2;
    UILabel *companyNameLabel3;
    SWLoadingView *loadingView;
    
    NSMutableDictionary *cutomerDict;

}

@property (nonatomic,unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end
