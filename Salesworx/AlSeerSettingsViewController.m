//
//  AlSeerSettingsViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSettingsViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface AlSeerSettingsViewController ()

@end

@implementation AlSeerSettingsViewController

@synthesize serverAddressTxtFld,serverNameTxtFld,serverLocationLabel;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    serverNameTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    serverNameTxtFld.layer.borderWidth = 1.0;
    
    serverAddressTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    serverAddressTxtFld.layer.borderWidth = 1.0;
    
    
    serverLocationLabel.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    serverLocationLabel.layer.borderWidth = 1.0;
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeButtonTapped:(id)sender {
    
    
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];

    
}

- (IBAction)editButtonTapped:(id)sender {
    
    serverAddressTxtFld.userInteractionEnabled=YES;
    serverNameTxtFld.userInteractionEnabled=YES;
    

}
@end
