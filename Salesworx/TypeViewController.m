//
//  TypeViewController.m
//  SWSession
//
//  Created by msaad on 3/17/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "TypeViewController.h"

@interface TypeViewController ()

@end

@implementation TypeViewController

@synthesize target;
@synthesize action;

- (id)init {
    self = [super init];
    
    if (self) {
        types=[NSArray arrayWithObjects:@"PERMANENT",@"Evaluation - 30 days", @"Evaluation - 60 days", @"Evaluation - 90 days", nil];
        [self setPreferredContentSize:CGSizeMake(250, 170)];
    }
    return self;
}

-(void)setNaviTitle{
   
    // White non-transucent navigatio bar, supports dark appearance
    
    if (@available(iOS 15, *)){
        UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
        appearance.backgroundImage = [UIImage imageNamed:@"nav_top"];
        appearance.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:EditableTextFieldSemiFontOfSize(16.0), NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil];
        [UINavigationBar appearance].standardAppearance = appearance;
        [UINavigationBar appearance].scrollEdgeAppearance = appearance;
    }
    else {
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.translucent = NO;
        self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:EditableTextFieldSemiFontOfSize(16.0), NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil];
        self.navigationController.navigationBar.titleTextAttributes = attributes;
    }
    self.title = @"Licence Type";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNaviTitle];
}

#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [types count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    [cell.textLabel setText:[types objectAtIndex:indexPath.row]];
   
    cell.textLabel.font = kFontWeblySleekSemiBold(14.0);
    cell.textLabel.textColor =  UIColorFromRGB(colorBlack);
    return cell;
}
}

#pragma mark UITableView Deleate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *selectedType = [types objectAtIndex:indexPath.row];
    
   // if ([self.target respondsToSelector:self.action]) {
         #pragma clang diagnostic push
                #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
[self.target performSelector:self.action withObject:selectedType];
                #pragma clang diagnostic pop
   // }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
