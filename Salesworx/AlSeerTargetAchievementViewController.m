//
//  AlSeerTargetAchievementViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerTargetAchievementViewController.h"
#import "SWDatabaseManager.h"
#import "AlSeerCustomerwiseTargetAchievementCell.h"


@interface AlSeerTargetAchievementViewController ()

@end

@implementation AlSeerTargetAchievementViewController
@synthesize selectedAgency,achievementArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 15.0, *)) {
        self.achTblView.sectionHeaderTopPadding = 0;
    }
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    
    achievementArray=[[NSMutableArray alloc]init];
    [self fetchAchievementforAgency];
}


-(void)fetchAchievementforAgency

{
    
    NSString* achievementQry=[NSString stringWithFormat:@"select IFNULL(C.Customer_Name,0)AS Customer_Name , IFNULL(B.Target_Value,0) AS Target_Value,IFNULL(B.Sales_Value,0) AS Sales_Value,IFNULL(B.Custom_Attribute_7,0)  AS BTG  from TBL_Sales_Target_Items  AS B inner join TBL_Customer AS C ON B.Classification_2=C.Customer_No  where B.Classification_1='%@'",selectedAgency];
    NSLog(@"query for fetching customerwise target %@", achievementQry);
    
    
    if (selectedAgency) {
        
       achievementArray= [[SWDatabaseManager retrieveManager]fetchDataForQuery:achievementQry];
        if (achievementArray.count>0) {
            
            NSLog(@"check decimals in achievement array  %@", [achievementArray description]);
            
            NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"Customer_Name"
                                                                         ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
            
            NSMutableArray* rawDataArray=[[NSMutableArray alloc]init];
            
            NSMutableArray* tempArray=[[NSMutableArray alloc]init];
            
            
            for (NSInteger i=0; i<achievementArray.count; i++) {
                
                
                NSInteger BTGVal= [[[achievementArray objectAtIndex:i]valueForKey:@"BTG"]integerValue];
                
                NSLog(@"TEST BTG DECIMAL %d", BTGVal);
                
                
                
                if ([[[achievementArray objectAtIndex:i]valueForKey:@"BTG"]integerValue]>0) {
                    
                    [tempArray addObject:[achievementArray objectAtIndex:i]];
                    
                }
                
            }
            
            
            
            NSLog(@"temp array is %@", [tempArray description]);

            sortedArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
            
            NSLog(@"content for selected agency is %@", [sortedArray description]);
            
           // [self.achTblView reloadData];
        }
        
        
    }
    
}

#pragma mark UITableview Methods



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    NSLog(@"count is %d",achievementArray.count);
   return  sortedArray.count;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    AlSeerCustomerwiseTargetAchievementCell *cell = [tableView dequeueReusableCellWithIdentifier:@"achCell"];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerCustomerwiseTargetAchievementCell" owner:nil options:nil] firstObject];
        
        
        [cell.customerNameLbl setFont:EditableTextFieldSemiFontOfSize(14)];
        [cell.btgLbl setFont:EditableTextFieldSemiFontOfSize(14)];
        
        cell.customerNameLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];
        cell.btgLbl.textColor = [UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1];

        cell.customerNameLbl.text=@"Customer Name";
        cell.btgLbl.text=@"BTG";
       
        cell.contentView.backgroundColor=[UIColor colorWithRed:(243.0/255.0) green:(244.0/255.0) blue:(248.0/255.0) alpha:1];
        
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
//    AlSeerCustomerwiseTargetAchievementCell *cell = [tableView dequeueReusableCellWithIdentifier:@"achCell"];
//    if (cell == nil) {
//        // Load the top-level objects from the custom cell XIB.
//        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"AlSeerCustomerwiseTargetAchievementCell" owner:self options:nil];
//        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
//        cell = [topLevelObjects objectAtIndex:0];
//        
//    }
    
    
    
    
    
    static NSString *CellIdentifier = @"achCell";
    
    AlSeerCustomerwiseTargetAchievementCell *cell = (AlSeerCustomerwiseTargetAchievementCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (AlSeerCustomerwiseTargetAchievementCell *) [[[NSBundle mainBundle] loadNibNamed:@"AlSeerCustomerwiseTargetAchievementCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    
    NSInteger i= indexPath.row;

    
        cell.customerNameLbl.text=[NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row] valueForKey:@"Customer_Name"]];
        
        
        
        NSInteger salesValue=[[NSString stringWithFormat:@"%@",[[achievementArray objectAtIndex:indexPath.row] valueForKey:@"Sales_Value"]] integerValue];
        
        
        NSInteger targetValue=[[NSString stringWithFormat:@"%@",[[achievementArray objectAtIndex:indexPath.row] valueForKey:@"Target_Value"]] integerValue];
        
        NSInteger btgValue=[[NSString stringWithFormat:@"%@",[[achievementArray objectAtIndex:indexPath.row] valueForKey:@"BTG"]] integerValue];
        
    NSLog(@"btgValue  %d and cust name %@", btgValue,[[achievementArray objectAtIndex:indexPath.row] valueForKey:@"Customer_Name"]);
    
    
    NSInteger btgVal=[[[sortedArray objectAtIndex:indexPath.row] valueForKey:@"BTG"] integerValue];
    
    
//  cell.btgLbl.text=[NSString stringWithFormat:@"%@",[[sortedArray objectAtIndex:indexPath.row] valueForKey:@"BTG"]];
    
    //avoiding decimals
    
    
    if (btgValue>0) {
        //cell.btgLbl.text=[NSString stringWithFormat:@"%@",[self thousandSaperator:btgValue]];
        
        NSLog(@"BTG IS %d", btgValue);
        
        cell.btgLbl.text=[NSString stringWithFormat:@"%d", btgVal];

        
//        cell.btgLbl.text=[NSString stringWithFormat:@"%@",[[achievementArray objectAtIndex:indexPath.row] valueForKey:@"BTG"]];
        
        
       
        
       // cell.btgLbl.text=[[achievementArray valueForKey:@"BTG"] objectAtIndex:indexPath.row];
        

    }
    else
    {
        cell.btgLbl.text=@"0";

    }
    
        NSLog(@"btg value is %@",[[sortedArray valueForKey:@"BTG"]objectAtIndex:indexPath.row]);
    
    
    return cell;
    
    
    /*
    AlSeerCustomerwiseTargetAchievementCell *cell = [tableView dequeueReusableCellWithIdentifier:@"achCell"];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerCustomerwiseTargetAchievementCell" owner:nil options:nil] firstObject];
        
        
       ;
        
        
    }
    
    return cell;*/
    
}





-(NSString*)thousandSaperator:(NSInteger)number

{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehaviorDefault];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    //    [numberFormatter setMinimumFractionDigits:2];
    //
    //    [numberFormatter setMaximumFractionDigits:2];
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    
    
    return result;
    
    
}



- (IBAction)closeButtonTapped:(id)sender {
    
    [self.popOver dismissPopoverAnimated:YES];
}
@end
