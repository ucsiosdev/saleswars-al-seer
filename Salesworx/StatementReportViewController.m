//  StatementReportViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "StatementReportViewController.h"
#import "SalesWorxCustomerStatementTableViewCell.h"
#import "CustomerStatementFilterViewController.h"
@interface StatementReportViewController ()

@end

@implementation StatementReportViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults fetchTitleView:kCustomerStatementTitle];
    self.view.backgroundColor = [UIColor whiteColor];
    
    datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    self.datePickerViewControllerDate=datePickerViewController;
    
    
    
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1];
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    
    NSLog(@"from date in did load %@", fromDate);
    toDate=[formatter stringFromDate:[NSDate date]];
    
    NSLog(@"from date is %@", fromDate);
    
    formatter=nil;
    usLocale=nil;
    gregorian=nil;
    offsetComponents=nil;
    
    statementTableView.layer.cornerRadius = 8.0;
    statementTableView.layer.masksToBounds = YES;
    
    filterTableView.cellLayoutMarginsFollowReadableWidth = NO;
    gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    [self viewReportAction];
    
    if (@available(iOS 15.0, *)) {
        statementTableView.sectionHeaderTopPadding = 0;
        filterTableView.sectionHeaderTopPadding = 0;
        gridView.tableView.sectionHeaderTopPadding = 0;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    Singleton * singleton=[Singleton retrieveSingleton];
    singleton.showCustomerDashboard=NO;
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        fromDate=[formatter stringFromDate:sender.selectedDate];
    }
    else
    {
        toDate = [formatter stringFromDate:sender.selectedDate];
    }
    [filterTableView reloadData];
    formatter=nil;
    usLocale=nil;
    [self viewReportAction];
}

- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    if ([customer count]!=0) {
        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
        [self viewReportAction];
        [filterTableView reloadData];
    }
    else
    {
        statementArray = nil;
        customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
        NSLog(@"customer dic description %@", [customerDict description]);
        [gridView reloadData];
        [filterTableView reloadData];
    }
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        [fromater setDateFormat:@"yyyy-MM-dd"];
        NSDate * finalDate=[fromater dateFromString:fromDate];
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        isFromDate=NO;
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        [fromater setDateFormat:@"yyyy-MM-dd"];
        NSDate * finalDate=[fromater dateFromString:fromDate];
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return statementArray.count;
    
    NSLog(@"statement report vc count %d", [statementArray count]);
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0)
    {
        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
    } else if (column == 1) {
        title = NSLocalizedString(@"Invoice Date", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Net Amount", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Paid Amount", nil);
    } else {
        title =NSLocalizedString(@"Settled Amount", nil);
    }
    
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSDictionary *data = [statementArray objectAtIndex:row];
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Invoice_Ref_No"]];
    } else if (column == 1) {
        value = [data dateStringForKey:@"InvDate" withDateFormat:NSDateFormatterMediumStyle];
    } else if (column == 2) {
        value = [data currencyStringForKey:@"NetAmount"];
    } else if (column == 3) {
        value = [data currencyStringForKey:@"PaidAmt"];
    } else if (column == 4) {
        if ([data objectForKey:@"Paid"]) {
            value = [data currencyStringForKey:@"Paid"];
        } else {
            value = [NSString stringWithFormat:@"%@0.00",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }
    }
    NSLog(@"Customer dic desc is %@", [customerDict description]);
    return value;
    
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 40;
    } else if (columnIndex == 1) {
        return 20;
    } else if (columnIndex == 2) {
        return 20;
    }
    else if (columnIndex == 3) {
        return 20;
    }
    else
        return 0;
}
- (IBAction)filterButtonTapped:(id)sender {
    if (statementArray.count > 0) {
        CustomerStatementFilterViewController *popOverVC = [[CustomerStatementFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.previousFilterParametersDict = previousFilteredParameters;
        popOverVC.customerDictionary = customerDict;
        popOverVC.customerStatementArray = statementArray;
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController = filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 400) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

-(IBAction)viewReportAction
{
    temp=nil;
    
    NSLog(@"temp is nill %@", [temp description]);
    
    NSString *q=@"select Invoice_Ref_No,max(NetAmount)as NetAmount,sum(PaidAmt) as PaidAmt,max(InvDate) as InvDate, CustomerName from ( SELECT   C.Invoice_Ref_No, C.Pending_Amount AS NetAmount,  CASE WHEN A.ERP_Status='C' THEN 0 ELSE A.Amount END AS PaidAmt,'0' AS Amount, C.Invoice_Date AS InvDate , CS.Customer_Name AS 'CustomerName' FROM TBL_Open_Invoices AS C LEFT JOIN TBL_Collection AS N ON  N.Customer_ID = C.Customer_ID AND C.Site_Use_ID = N.Site_Use_ID LEFT JOIN  TBL_Collection_Invoices AS A ON N.Collection_Id=A.Collection_ID AND C.Invoice_Ref_No = A.Invoice_No inner join Tbl_customer as cs on c.customer_id=cs.customer_id WHERE  (C.Customer_ID= cs.Customer_ID) UNION ALL SELECT  B.Orig_Sys_Document_Ref AS Invoice_Ref_No, B.Transaction_Amt As NetAmount,   CASE WHEN D.ERP_Status='C' THEN 0 ELSE D.Amount END AS PaidAmt,'0' AS Amount, B.Creation_Date AS InvDate ,CS.Customer_Name AS 'CustomerName' FROM TBL_Sales_History AS B LEFT JOIN TBL_Collection AS N ON N.Customer_ID = B.Inv_To_Customer_ID AND  B.Inv_To_Site_ID = N.Site_Use_ID LEFT JOIN  TBL_Collection_Invoices AS D ON N.Collection_ID=D.Collection_ID AND B.Orig_Sys_Document_Ref = D.Invoice_No inner join Tbl_customer as cs on B.Inv_To_Customer_ID = cs.Customer_ID WHERE (B.ERP_Status = 'X') AND (B.Inv_To_Customer_ID= cs.customer_id) AND (B.Inv_To_Site_ID = cs.Site_use_id) AND B.Orig_Sys_Document_Ref NOT IN (SELECT Invoice_Ref_No FROM TBL_Open_Invoices))  as X group by Invoice_Ref_No";
    
    temp = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
    
    
    NSLog(@"db response of docs %@", [temp description]);
    statementArray = nil;
    statementArray =[NSMutableArray arrayWithArray:temp];
    [statementTableView reloadData];
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return statementArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"customerStatement";
    SalesWorxCustomerStatementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.lblDocumentNo.text = @"Document No";
    cell.lblInvoiceDate.text = @"Invoice Date";
    cell.lblNetAmount.text = @"Net Amount (AED)";
    cell.lblPaidAmount.text = @"Paid Amount (AED)";
    
    
    cell.lblDocumentNo.textColor = TableViewHeaderSectionColor;
    cell.lblInvoiceDate.textColor = TableViewHeaderSectionColor;
    cell.lblNetAmount.textColor = TableViewHeaderSectionColor;
    cell.lblPaidAmount.textColor = TableViewHeaderSectionColor;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    static NSString* identifier=@"customerStatement";
    SalesWorxCustomerStatementTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxCustomerStatementTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data = [statementArray objectAtIndex:indexPath.row];
    
    
    cell.lblDocumentNo.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Invoice_Ref_No"]]];
    cell.lblInvoiceDate.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data dateStringForKey:@"InvDate" withDateFormat:NSDateFormatterMediumStyle]]];
    cell.lblNetAmount.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"NetAmount"]]];
    cell.lblPaidAmount.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"PaidAmt"]]];

    return cell;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    currencyTypeViewController=nil;
    customVC=nil;
}

-(void)filteredCustomerStatement:(id)filteredContent
{
    NSLog(@"filtered content in report is %@", filteredContent);
    
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterFilled.png"] forState:UIControlStateNormal];
    statementArray = filteredContent;
    [statementTableView reloadData];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
    
}
-(void)customerStatementFilterDidReset{
    previousFilteredParameters = [[NSMutableDictionary alloc]init];
    [self viewReportAction];
    [statementTableView reloadData ];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];
}

-(void)filterParametersCustomerStatement:(id)filterParameter
{
    previousFilteredParameters=filterParameter;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end

