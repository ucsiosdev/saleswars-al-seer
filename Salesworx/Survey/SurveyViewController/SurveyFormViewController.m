//
//  SurveyFormViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "SurveyFormViewController.h"
#import "SurveyViewController.h"
#import "SurveyDetails.h"
#import "DataSyncManager.h"
#import "SWPlatform.h"
#import "NewSurveyViewController.h"
@interface SurveyFormViewController ()
{
    NSString *globalCustomerID;
    NSString *globalSiteUseID;
}

@end

@implementation SurveyFormViewController
@synthesize SurveyArray,customer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Survey"];
    
    
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleDone target:self action:@selector(btnBackTapped:)];
    
    [btnBack setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                     kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                     [UIColor whiteColor], NSForegroundColorAttributeName,
                                     nil]
                           forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = btnBack;
    
    
    tblViewSurvey.tableHeaderView = nil;

    globalCustomerID = [SWDefaults getValidStringValue:[[SWDefaults customer] stringForKey:@"Customer_ID"]];
    globalSiteUseID = [SWDefaults getValidStringValue:[[SWDefaults customer] stringForKey:@"Site_Use_ID"]];
    
    
    
    customerNameLabel.text = [SWDefaults getValidStringValue:[[SWDefaults customer] stringForKey:@"Customer_Name"]];
    customerNumberLabel.text = [SWDefaults getValidStringValue:[[SWDefaults customer]stringForKey:@"Customer_No"]];
    double availBal = [[[SWDefaults customer]stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
   // tblViewSurvey.tableFooterView = [UIView new];
    
    
     BOOL customerStatus = [[SWDefaults getValidStringValue: [[SWDefaults customer] stringForKey:@"Cust_Status"]] isEqualToString:@"Y"];
     BOOL creditHold = [[SWDefaults getValidStringValue:[[SWDefaults customer] stringForKey:@"Credit_Hold"]] isEqualToString:@"Y"];
    
    if (!customerStatus || creditHold) {
        lblCustomerStatus.text = @"Blocked";
        lblCustomerStatus.backgroundColor =  UIColorFromRGB(0xFF736E);
        lblCustomerStatus.hidden = NO;
    }else {
        lblCustomerStatus.hidden = YES;
        //lblCustomerStatus.text = @"Active";
        //lblCustomerStatus.backgroundColor = UIColorFromRGB(0x47B681);
    }
    lblCustomerStatus.textColor =  [UIColor whiteColor];
    lblCustomerStatus.layer.cornerRadius = 10.0;
    lblCustomerStatus.layer.masksToBounds = YES;
    
    self.navigationItem.hidesBackButton = NO;
   // tblViewSurvey.backgroundView = nil;
    tblViewSurvey.backgroundColor = [UIColor whiteColor];
    //Set Page count
    appDelegate=[DataSyncManager sharedManager];
    appDelegate.alertMessageShown=0;
    appDelegate.currentPage=0;
    appDelegate.MaxPages=0;
    
    self.SurveyArray =[[SWDatabaseManager retrieveManager] selectSurveyWithCustomerID:globalCustomerID andSiteUseID:globalSiteUseID];
    
    if (@available(iOS 15.0, *)) {
        tblViewSurvey.sectionHeaderTopPadding = 0;
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //self.view.backgroundColor = [UIColor whiteColor];
    
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: tblViewSurvey.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    tblViewSurvey.layer.mask = maskLayer;
    
}

- (void)btnBackTapped:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

-(void)ShowSurvey
{
    NSLog(@"button taped");
    
    if(!([self.SurveyArray count]==0))
    {
        for (int i=0; i<[SurveyArray count]; i++) {
             infoForNewSurvey=[self.SurveyArray objectAtIndex:i];
        }
        
        NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:globalCustomerID andSiteUseID:globalSiteUseID ansSurveyID:[NSString stringWithFormat:@"%d",infoForNewSurvey.Survey_ID]];
        
        NSLog(@"survey id before pushing %d", infoForNewSurvey.Survey_ID);
        NSLog(@"temp array before pushing %@", [self.SurveyArray description]);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        
        if(tempArray.count > 0)
        {
            NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
            NSDateFormatter* df = [NSDateFormatter new];
            [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [df setLocale:usLocale];
            NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
            int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
            df=nil;
            usLocale=nil;
            if(noOfDays <= 0)
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"Survey has been already submitted for this customer." withController:self];
            }
        }
    }
    
    NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
    newSurveyVC.survetIDNew=infoForNewSurvey.Survey_ID;
    [self.navigationController pushViewController:newSurveyVC animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.toolbarHidden=YES;
}

-(void)btnBackPressed
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];

}
#pragma mark -
#pragma mark Table view data source


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if([self.SurveyArray count]==0)
    {
        return 1;
    }
    else
    {
        return [self.SurveyArray count];

    }

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    	return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.01;
}
/*
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section {
    
    NSString *title =  NSLocalizedString(@"Survey Name", nil);;
    
    GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:title] ;
    return sectionHeader;
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath


// Customize the appearance of table view cells.
{
    @autoreleasepool
    {
        NSString *CellIdentifier = @"CustomerDetailViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier]  ;
            [cell.textLabel setFont:kFontWeblySleekSemiBold(14)];
            [cell.textLabel setTextColor: UIColorFromRGB(0x2C394A)];

        }
        
        
        

    
    if([self.SurveyArray count] ==0)

        {
            [cell.textLabel setText:NSLocalizedString(@"There is no survey for this customer", nil)];
        }
        else
        {
            SurveyDetails *info =[self.SurveyArray objectAtIndex:indexPath.row];
            [cell.textLabel setText:info.Survey_Title];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
       
        
        return cell;
            
    }
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    if(!([self.SurveyArray count]==0))
    {
        NSLog(@"survey array in first %@", [SurveyArray description]);
        
        SurveyDetails *info =[self.SurveyArray objectAtIndex:indexPath.row];
        NSLog(@"sales rep id is %d", appDelegate.surveyDetails.SalesRep_ID);
        NSLog(@"info %d, %@, %@", info.Survey_ID,info.Survey_Title,info.Survey_Type_Code);
        
        NSArray *tempArray =[[SWDatabaseManager retrieveManager] checkSurveyIsTaken:globalCustomerID andSiteUseID:globalSiteUseID ansSurveyID:[NSString stringWithFormat:@"%d",info.Survey_ID]];
        
        NSLog(@"temp array in did select %@", [tempArray description]);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Start_Time" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        tempArray = [tempArray sortedArrayUsingDescriptors:sortDescriptors];
        
        if([tempArray count]==0)
        {
            appDelegate.surveyDetails=[self.SurveyArray objectAtIndex:indexPath.row];
            
            NSLog(@"survey old  id %d", appDelegate.surveyDetails.Survey_ID);
            NSLog(@"sales rep ID %d", appDelegate.surveyDetails.SalesRep_ID);
            NSLog(@"Emp code %@", [[SWDefaults userProfile]stringForKey:@"Emp_Code"]);
            
            
            NSDate *date =[NSDate date];
            NSDateFormatter *formater =[NSDateFormatter new];
            [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [formater setLocale:usLocale];
            NSString *dateString = [formater stringFromDate:date];
            
            NSLog(@"Survey Time Stamp is %@", dateString);
            if([info.Survey_Type_Code isEqualToString:@"N"]) {
                NSLog(@"app delegate key %d", appDelegate.key);
            }
            
            NSArray *tempSurveyCount = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];
            if (tempSurveyCount == nil || tempSurveyCount.count == 0){
                
                [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"No Survey details available" withController:self];
                return;
            }
            else {
                NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
                if (self.surveyParentLocation) {
                    newSurveyVC.surveyParentLocation=self.surveyParentLocation;
                }
                [self.navigationController pushViewController:newSurveyVC animated:YES];
            }
        }
        else
        {
            NSMutableDictionary *tempDictionary=[tempArray objectAtIndex:0];
            NSDateFormatter* df = [NSDateFormatter new];
            [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [df setLocale:usLocale];
            NSDate *selectedDate = [df dateFromString:[tempDictionary stringForKey:@"Start_Time"]];
            int noOfDays= [self daysBetween:selectedDate and:[NSDate date]];
            df=nil;
            usLocale=nil;
            
            if(noOfDays <= 0) {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"Survey has been already submitted for this customer." withController:self];
            }
            else {
                appDelegate.surveyDetails = [self.SurveyArray objectAtIndex:indexPath.row];
                NSArray *tempSurveyCount = [[SWDatabaseManager retrieveManager] selectQuestionBySurveyId:appDelegate.surveyDetails.Survey_ID];
                
                if (tempSurveyCount == nil || tempSurveyCount.count == 0){
                    [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"No Survey details available" withController:self];
                    return;
                }
                else {
                    NewSurveyViewController* newSurveyVC=[[NewSurveyViewController alloc]init];
                    [self.navigationController pushViewController:newSurveyVC animated:YES];
                }
            }
        }
    }
}
- (int)daysBetween:(NSDate *)dt1 and:(NSDate *)dt2
{
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
