//
//  SurveyPageControlViewControl.h
//  PageControlExample
//
//  Created by Chakra on 26/02/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import "SurveyViewController.h"
#import "SurveyQuestionDetails.h"
#import "CustomerResponseDetails.h"
#import "AuditResponseDetail.h"
#import "DataSyncManager.h"
#import "SurveyDetails.h"
#import "SWPlatform.h"
#import "NewSurveyViewController.h"

@interface SurveyPageControlViewControl : FTPBaseViewController<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,RateViewDelegate,UITextViewDelegate> {

    SurveyViewController *surveyVC;
    DataSyncManager *appDelegate;
    SurveyQuestionDetails *_surveyQuestion;
    CustomerResponseDetails *customerInfo;
    AuditResponseDetail *auditInfo;
    
	IBOutlet UILabel *lblQuestion,*staticQuestion;
	IBOutlet UILabel *lblAnswer;
    IBOutlet UIView  *responseView;
    IBOutlet UILabel *lblSliderValue;
    IBOutlet UITextField *txtSurveyId;
    
    UITextView *txtViewResponse;
    UITableView *tblResponse;
    UISlider *sliderResponse;
       
    NSArray *_surveyResponseArray;
    NSMutableArray *selectedIndexes;
    NSIndexPath *lastIndexPath;
   
    int pageNumber;
    int ResponseId;
    int sliderValueUsed;
    NSString *strResponse;
    NSString * strTableResponse;
    NSString *strRate;
    
    UIView *myBackgroundView;
}
@property (nonatomic, strong) NSArray *surveyResponseArray;
@property (nonatomic, strong) UILabel *pageNumberLabel;

- (id)initWithSurveyQuestion:(SurveyQuestionDetails*)Survey_Question;
-(IBAction)sliderValueChanged:(id)sender;
-(void)setCustomerResponse;
-(void)setViewByResponseType;


@end
