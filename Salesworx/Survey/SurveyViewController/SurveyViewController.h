//
//  SurveyViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/19/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSyncManager.h"
#import "FTPBaseViewController.h"

@interface SurveyViewController : FTPBaseViewController<UIScrollViewDelegate,UIAlertViewDelegate>
{
    
    UISwipeGestureRecognizer * recognizer;
    DataSyncManager *appDelegate;
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *saveSurvey;
    
    NSArray *_surveyQuestionArray;
    NSMutableArray *viewControllers;
    
    BOOL pageControlUsed;
    int kNumberOfPages;
    NSString *alertType;
    NSNotification *tempNoti;
 }

@property (nonatomic, strong) NSArray *surveyQuestionArray;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;


-(IBAction)SaveSurveyAction:(id)sender;
-(void)handleSwipeFrom:(id)sender;
@end
