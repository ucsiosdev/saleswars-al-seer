//
//  SurveyFormViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSyncManager.h"
#import "FTPBaseViewController.h"
#import "CoreLocation/CoreLocation.h"
#import "SWPlatform.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"


@interface SurveyFormViewController : SWViewController<CLLocationManagerDelegate>
{
    
    DataSyncManager *appDelegate;
    NSArray *SurveyArray;
    NSMutableDictionary *customer;
    UIView *myBackgroundView;
    
    //sample button
    
    UIButton * surveyBtn;
     SurveyDetails *infoForNewSurvey;
    
    NSString* visitID;
    
    CLLocationManager * locationManager;
    
    
    // new connections
    IBOutlet MedRepElementDescriptionLabel *customerNameLabel;
    IBOutlet MedRepElementTitleLabel *customerNumberLabel;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;
    IBOutlet UITableView *tblViewSurvey;
    
    IBOutlet UILabel *lblCustomerStatus;
    
}
@property(nonatomic,strong) NSArray *SurveyArray;

@property(nonatomic,strong) NSMutableDictionary *customer;
@property(strong,nonatomic) NSString* surveyParentLocation;
-(void)ShowSurvey;

@end
