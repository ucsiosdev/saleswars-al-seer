//
//  HttpClient.m
//  Cisco
//
//  Created by msaad on 7/8/13.
//  Copyright (c) 2013 Jibran Ahmed. All rights reserved.
//

#import "HttpClient.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation HttpClient

#pragma mark - Initialization

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if(!self)
        return nil;
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    [self setParameterEncoding:AFJSONParameterEncoding];
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    return self;
}

#pragma mark - Singleton Methods

+ (HttpClient *)sharedManager
{
    static dispatch_once_t pred;
    static HttpClient *_sharedManager = nil;
    
    dispatch_once(&pred, ^{ _sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:@""]]; }); // You should probably make this a constant somewhere
    return _sharedManager;
}

@end