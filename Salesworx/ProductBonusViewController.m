//
//  ProductBonusViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductBonusViewController.h"

@interface ProductBonusViewController ()

@end

@implementation ProductBonusViewController



- (id)initWithProduct:(NSMutableDictionary *)p {
    self = [super init];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Bonus", nil)];
        [self updateDateWithProduct:p];
    }
    
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [Flurry logEvent:@"Produst Bonus  View"];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
    [self.view addSubview:bonusView];
}
- (void)updateDateWithProduct:(NSMutableDictionary *)item
{
    product=nil;
    product=[NSMutableDictionary dictionaryWithDictionary:item];
    bonusView = nil;
    [bonusView removeFromSuperview];
    bonusView=[[ProductBonusView alloc] initWithProduct:product];
    [bonusView setFrame:self.view.bounds];
    [bonusView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [bonusView loadBonus];
    [self.view addSubview:bonusView];

}
- (void)closeOrder {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
