//
//  PointofPurchaseViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "PointofPurchaseViewController.h"
#import "FMDBHelper.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "AccordionView.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"
#import "SKSTableViewCellIndicator.h"
#import "pointofPurchaseTableViewCell.h"
#import "RemarksPopViewController.h"
#import "pointofPurchaseTableViewCell.h"
#import <QuartzCore/QuartzCore.h>


#define COMMENT_LABEL_WIDTH 230
#define COMMENT_LABEL_MIN_HEIGHT 65
#define COMMENT_LABEL_PADDING 10

@interface PointofPurchaseViewController (){
    
    float cellHeightSelectd ;
    float cellHeightNoSelectd ;
}

@end

@implementation PointofPurchaseViewController
@synthesize expandedCells;

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self name: UIKeyboardDidShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: UIKeyboardWillHideNotification object: nil];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    cellHeightSelectd =  240.0f;  //257.0f;
    cellHeightNoSelectd = 118.0f;  //140.0f;

   
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"POP"];
    
    //set set our selected Index to -1 to indicate no cell will be expanded
    selectedIndex = -1;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    NSLog(@"current visit id is %@", self.currentVisitID);
    
    oldFrame=self.view.frame;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardDidShow:)
                                                 name: UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillDisappear:)
                                                 name: UIKeyboardWillHideNotification object:nil];
    
    //SetUpTestData with some meaningless strings
    textArray = [[NSMutableArray alloc] init];
    
    NSString *testString;
    
    for(int ii = 0; ii < 6; ii++)
    {
        testString = @"Test comment. Test comment.";
        for (int jj = 0; jj < ii; jj++) {
            testString = [NSString stringWithFormat:@"%@ %@", testString, testString];
        }
        [textArray addObject:testString];
    }
        popArray=[[NSMutableArray alloc]init];
        popItemsArray=[[NSMutableArray alloc]init];
        selectedIndexpatharray=[[NSMutableArray alloc] init];
        previousAddedrows=[[NSMutableArray alloc] init];
        subCategoriesArray=[[NSMutableArray alloc] init];
        arrayforIndexPath=[[NSMutableArray alloc] init];
    [self fetchPOPDatafromDatabase];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: _tableView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    _tableView.layer.mask = maskLayer;
}

//This just a convenience function to get the height of the label based on the comment text
-(CGFloat)getLabelHeightForIndex:(NSInteger)index
{
    CGSize maximumSize = CGSizeMake(COMMENT_LABEL_WIDTH, 10000);
    
    CGSize labelHeighSize = [[textArray objectAtIndex:index] sizeWithFont: [UIFont fontWithName:@"Helvetica" size:14.0f]
                                                        constrainedToSize:maximumSize
                                                            lineBreakMode:UILineBreakModeWordWrap];
    return labelHeighSize.height;
    
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return popItemsArray.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"customCell";
    
    pointofPurchaseTableViewCell *cell = (pointofPurchaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //cell = [[[exerciseListUITableCell alloc] init] autorelease];
        
        NSArray * topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"pointofPurchaseTableViewCell" owner:self options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[UITableViewCell class]])
            {
                
                if (!cell) {
                
                
                cell = (pointofPurchaseTableViewCell *)currentObject;
                }
                    cell.brandNameLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"Brand"] objectAtIndex:indexPath.row]];
                    cell.skuLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"SKU_Count"] objectAtIndex:indexPath.row]];
                     cell.rangeLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"Range"] objectAtIndex:indexPath.row]];
                    cell.shelfLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"Shelf"] objectAtIndex:indexPath.row]];
                     cell.priceLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"Price"] objectAtIndex:indexPath.row]];
                    cell.promotionLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"Promotion"] objectAtIndex:indexPath.row]];
                    cell.mslLbl.text=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"MSL_Count"] objectAtIndex:indexPath.row]];
                
                
                
                
                cell.remarksTxtView.delegate=self;
                cell.remarksTxtView.font=kFontWeblySleekSemiBold(14);
                cell.remarksTxtView.textColor =  UIColorFromRGB(0x2C394A);
                
                //cell.remarksTxtView.text=@"TEST";
                
                
                [cell.remarksTxtView.layer setBorderColor:[[UIColor colorWithRed:(191.0/255.0) green:(212.0/255.0) blue:(222.0/255.0) alpha:1] CGColor]];
                [cell.remarksTxtView.layer setBorderWidth:1.0];
                
                //The rounded corner part, where you specify your view's corner radius:
                cell.remarksTxtView.layer.cornerRadius = 1;
                cell.remarksTxtView.clipsToBounds = YES;
                
                cell.infoBtn.tag=indexPath.row;
                cell.saveBtn.titleLabel.font = kFontWeblySleekSemiBold(16);
                cell.saveBtn.layer.cornerRadius = 8.0;
                cell.saveBtn.layer.masksToBounds = YES;

                [cell.infoBtn addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                
                

                    [cell.saveBtn addTarget:self action:@selector(saveRemarks:) forControlEvents:UIControlEventTouchUpInside];
                
                    cell.saveBtn.tag=indexPath.row;
                
                cell.remarksTxtView.tag=indexPath.row;
                
                
                
                NSLog(@"indec path being added to array is %@", indexPath);
                
                
                    [arrayforIndexPath addObject:indexPath];
                    
                    
                    indexPathforPopOver=indexPath;
                
                NSString* popID=[NSString stringWithFormat:@"%@",[[popArray valueForKey:@"POP_ID"] objectAtIndex:0] ];
                NSString* popItemID=[[popItemsArray valueForKey:@"POP_ITEM_ID"] objectAtIndex:indexPath.row];
                
                
                
                NSMutableArray* prevRemarksArr=[self fetchRemarksifExists:popID with:popItemID];
                
                
                NSLog(@"remarks array in cell for row %@", [prevRemarksArr description]);
                
                
                if (prevRemarksArr.count>0) {
                    
                
                
                if ([[[prevRemarksArr  valueForKey:@"Remarks"] objectAtIndex:0] length]>0) {
                    
                    cell.remarksPlaceholderLbl.text=@"";
                    cell.remarksTxtView.text=[[prevRemarksArr  valueForKey:@"Remarks"] objectAtIndex:0] ;
                    cell.remarksLbl.text=cell.remarksTxtView.text;

                    
                }
                
                else
                {
                    cell.remarksPlaceholderLbl.text=@"Please enter your remarks";
                    
                }
                }
                
                else
                {
                  cell.remarksPlaceholderLbl.text=@"Please enter your remarks";
                }
                
                
                break;
            }
        }
    }
    
    
    
    //If this is the selected index then calculate the height of the cell based on the amount of text we have
    if(selectedIndex == indexPath.row)
    {
        
        cell.saveBtn.hidden=NO;
        cell.remarksTxtView.hidden=NO;
        cell.remarksPlaceholderLbl.hidden=NO;

        cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x,
                                                 cell.contentView.frame.origin.y,
                                                 cell.contentView.frame.size.width,
                                                 257.0f);
        
        

    }
    else {
        
        //Otherwise just return the minimum height for the label.
        cell.contentView.frame = CGRectMake(cell.contentView.frame.origin.x,
                                                 cell.contentView.frame.origin.y,
                                                 cell.contentView.frame.size.width,
                                                 140.0f);
    }
   
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    pointofPurchaseTableViewCell *clickedCell = (pointofPurchaseTableViewCell *)[[textView superview] superview];
    clickedCell.remarksPlaceholderLbl.text=@"";
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    currentTxtViewTag=textView.tag;
    
    pointofPurchaseTableViewCell *cell = (pointofPurchaseTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPathforPopOver];
    cell.remarksPlaceholderLbl.text=@"";
    
    CGPoint txtFieldPosition = [textView convertPoint:CGPointZero toView:self.tableView];
    NSLog(@"Begin txtFieldPosition : %@",NSStringFromCGPoint(txtFieldPosition));
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:txtFieldPosition];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    return YES;
}

-(void)infoButtonTapped:(UIButton*)sender
{
    lastOffset = self.tableView.contentOffset.y;
    pointofPurchaseTableViewCell *cell = (pointofPurchaseTableViewCell*)[[sender superview]superview];
    
    
    NSIndexPath *currentIndexPath = [self.tableView indexPathForCell: cell];
    NSString* popID=[NSString stringWithFormat:@"%@",[[popArray valueForKey:@"POP_ID"] objectAtIndex:0] ];
    NSString* popItemID=[[popItemsArray valueForKey:@"POP_ITEM_ID"] objectAtIndex:currentIndexPath.row];
    
    
    
    NSMutableArray* prevRemarksArr=[self fetchRemarksifExists:popID with:popItemID];
    
    
    NSLog(@"remarks array in cell for row %@", [prevRemarksArr description]);
    
    
    if (prevRemarksArr.count>0) {
        if ([[[prevRemarksArr  valueForKey:@"Remarks"] objectAtIndex:0] length]>0) {
            
            cell.remarksPlaceholderLbl.text=@"";
            cell.remarksTxtView.text=[[prevRemarksArr  valueForKey:@"Remarks"] objectAtIndex:0] ;
            cell.remarksLbl.text=cell.remarksTxtView.text;
            
        }
        
        else
        {
            cell.remarksPlaceholderLbl.text=@"Please enter your remarks";
            
        }
    }
    
    else
    {
       cell.remarksPlaceholderLbl.text=@"Please enter your remarks";
    }
    

    
    
    cell.saveBtn.hidden=NO;
    cell.remarksTxtView.hidden=NO;
    cell.remarksPlaceholderLbl.hidden=NO;
    
    
    //The user is selecting the cell which is currently expanded
    //we want to minimize it back
    if(selectedIndex == currentIndexPath.row)
    {
        cell.saveBtn.hidden=NO;
        cell.remarksTxtView.hidden=NO;
        cell.remarksTxtView.delegate=self;
        cell.remarksPlaceholderLbl.hidden=NO;
        
        
        selectedIndex = -1;
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        return;
    }
    
    //First we check if a cell is already expanded.
    //If it is we want to minimize make sure it is reloaded to minimize it back
    if(selectedIndex >= 0)
    {
        NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        selectedIndex = currentIndexPath.row;
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:previousPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    
    //Finally set the selected index to the new selection and reload it to expand
    selectedIndex = currentIndexPath.row;
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];

    
    if (sender.tag==popItemsArray.count-1) {
        [self.tableView scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}


-(NSMutableArray*)fetchRemarksifExists: (NSString*)popID with:(NSString*)popItemID
{
    NSString* remarksQry=[NSString stringWithFormat:@"Select IFNULL(Remarks,0) AS Remarks, IFNULL(Logged_At,0) AS Logged_At, IFNULL(Logged_By,0) AS Logged_By  from TBL_POP_REMARKS where POP_ID='%@' and POP_Item_ID ='%@' Order by Logged_At Desc", popID,popItemID];
    NSLog(@"query for remarks %@", remarksQry);
    
    
    
    NSMutableArray * remarksResp=[[SWDatabaseManager retrieveManager]fetchDataForQuery:remarksQry];
    NSLog(@"fetched remarks %@", [remarksResp description]);
    
    
    return remarksResp;
}


-(void)saveRemarks:(UIButton*)sender
{
    pointofPurchaseTableViewCell *cell = (pointofPurchaseTableViewCell*)[[sender superview]superview];
    
    NSIndexPath *currentIndexPath = [self.tableView indexPathForCell: cell];
    NSString* rowID= [NSString createGuid];
    NSString* popID=[NSString stringWithFormat:@"%@",[[popArray valueForKey:@"POP_ID"] objectAtIndex:0]];
    NSString*popItemID=[NSString stringWithFormat:@"%@",[[popItemsArray valueForKey:@"POP_ITEM_ID"] objectAtIndex:currentIndexPath.row] ];
    
    NSString* capturedRemarks=cell.remarksTxtView.text;
    NSDate* currentDate=[NSDate date];
    NSLog(@"current date is %@", currentDate);
    
    NSDateFormatter* dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"formatted date %@", [dateFormatter stringFromDate:currentDate]);
    NSString* loggedAt=[dateFormatter stringFromDate:currentDate];
    
    
    NSString* loggedBy=[NSString stringWithFormat:@"%@", [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    
    NSLog(@"logged at and logged by are %@ %@", loggedAt,loggedBy);
    NSLog(@"contents in save action %@", [popItemsArray description]);
    NSLog(@"customer in save action %@", [SWDefaults customer]);
    
    BOOL success;
    FMDatabase *db = [FMDBHelper getDatabase];
    [db open];
    @try {
        success =  [db executeUpdate:@"INSERT into TBL_POP_Remarks(Row_ID,POP_ID,POP_ITEM_ID,Remarks,Logged_At,Logged_By,Custom_Attribute_1) VALUES (?,?,?,?,?,?,?)", rowID,popID,popItemID,capturedRemarks,loggedAt,loggedBy,[SWDefaults currentVisitID], nil];
        
        if (success==YES) {
            NSLog(@"insert successfull");
            [SWDefaults showAlertAfterHidingKeyBoard:@"Success" andMessage:@"Remarks Added Successfully" withController:self];
            
            NSIndexPath* currentIndexPath=[arrayforIndexPath objectAtIndex:sender.tag];
            if(selectedIndex == currentIndexPath.row)
            {
                cell.saveBtn.hidden=NO;
                cell.remarksTxtView.hidden=NO;
                cell.remarksPlaceholderLbl.hidden=NO;
                
                
                selectedIndex = -1;
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPath] withRowAnimation:UITableViewRowAnimationFade];
                
                return;
            }
            [self.tableView reloadData];
        }
        
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while updating remarks in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //If this is the selected index we need to return the height of the cell
    //in relation to the label height otherwise we just return the minimum label height with padding
    if(selectedIndex == indexPath.row)
    {
        return cellHeightSelectd;
    }
    else {
        return cellHeightNoSelectd;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath * index = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:index animated:NO];
}

#pragma mark Database fetch operations

-(void)fetchPOPDatafromDatabase
{
    NSMutableDictionary* customerDict=[SWDefaults customer];
    NSLog(@"customer dict is %@", [customerDict description]);

    NSString* customerID,*siteUseID;
    if ([customerDict objectForKey:@"Customer_ID"] && [customerDict objectForKey:@"Site_Use_ID"]) {

        customerID=[customerDict valueForKey:@"Customer_ID"];
        siteUseID=[customerDict valueForKey:@"Site_Use_ID"];
        popArray=[self fetchPOPDetailsforCustomer:customerID with:siteUseID];
        
        if (popArray.count>0) {
            
                popItemsArray=[self fetchPOPItemsforPOPID:[[popArray valueForKey:@"POP_ID"] objectAtIndex:0]];

            if (popItemsArray.count>0) {
                [self.tableView reloadData];
            }
        }
        else
        {
            UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"POP data unavailable" andMessage:@"please try later" andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
        }
    }
}

#pragma mark TBL POP Operations method

-(NSMutableArray*)fetchPOPDetailsforCustomer:(NSString*)customerID with:(NSString*)site_Use_ID

{
    NSString* popContentQry=[NSString stringWithFormat:@"select IFNULL(POP_ID,0) AS POP_ID, IFNULL(POP_MONTH,0) AS POP_MONTH ,IFNULL(POP_YEAR,0) AS POP_YEAR from TBL_POP where Customer_ID='%@' and Site_Use_ID='%@'", customerID,site_Use_ID];
    NSLog(@"queryfor pop content %@", popContentQry);
    NSArray* popContentArray=[FMDBHelper executeQuery:popContentQry];
    if (popContentArray.count>0) {
        NSLog(@"pop table content %@", [popContentArray description]);
    }
    return [popContentArray mutableCopy];

}


#pragma mark TBL POP Items Method

-(NSMutableArray*)fetchPOPItemsforPOPID:(NSString*)POPID
{
    NSString* popItemsQry=[NSString stringWithFormat:@"select IFNULL(POP_ITEM_ID,0) AS POP_ITEM_ID, IFNULL(Brand,0) AS Brand, IFNULL(SKU_Count,0) AS SKU_Count, IFNULL(MSL_Count,0) AS MSL_Count, IFNULL(Range,0) AS Range, IFNULL(Shelf,0) AS Shelf ,IFNULL(Price,0) AS Price, IFNULL(Promotion,0) AS Promotion from TBL_POP_ITEMS where POP_ID='%@'",POPID];
    NSLog(@"query for pop items %@", popItemsQry);
    NSArray* popItemsContentArray=[FMDBHelper executeQuery:popItemsQry];
    if (popItemsContentArray.count>0) {
        NSLog(@"POP items content %@", [popItemsContentArray description]);
    }
    return [popItemsContentArray mutableCopy];

}

#pragma mark hiding keyboard methods

- (void) keyboardDidShow: (NSNotification*) aNotification
{
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+50, 0.0);
    
    [self.tableView setContentInset:contentInsets];
    
    [self.tableView setScrollIndicatorInsets:contentInsets];
    
    NSIndexPath *currentRowIndex = [NSIndexPath indexPathForRow:currentTxtViewTag inSection:0];
    
    [self.tableView scrollToRowAtIndexPath:currentRowIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void) keyboardWillDisappear: (NSNotification*) aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    [self.tableView setContentInset:contentInsets];
    
    [self.tableView setScrollIndicatorInsets:contentInsets];
}

- (NSTimeInterval) keyboardAnimationDurationForNotification:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    NSValue* value = [info objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue: &duration];
    
    return duration;
}


@end


