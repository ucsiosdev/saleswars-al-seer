//
//  SWVisitOptionsViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "OnsiteOptionsViewController.h"
#import "SalesWorxCameraRollViewController.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepElementTitleLabel.h"

@interface SWVisitOptionsViewController : SWViewController<UIAlertViewDelegate, UICollectionViewDelegate,UICollectionViewDataSource> {
    CustomerHeaderView *customerHeaderView;
    UIScrollView *scrollView;
    UIButton *distributionCheckButton;
    UIButton *salesOrderButton;
    UIButton *returnsButton;
    UIButton *collectionsButton;
    UIButton *feedbackButton;
    UIButton *surveyButton;
    UIButton *manageOrdersButton;
    UIButton *orderHistoryButton;
    UIButton* pointofPurchaseButton;
    UIButton* sendOrdersButton;
    
    UIAlertView* orderSentSuccessAlert;
    
    id delegate;
    CGFloat kIconWidth;
    CGFloat kIconHeight;
    
    UILabel *discountLable;
    NSString *discountRate;
    NSString *isOrderHistoryEnable;
    NSString *isCollectionEnable;
    NSString *isDistributionEnable;
    
    BOOL isDistribution;
    BOOL isCollection;
    
    BOOL dcNotOptionalVisitTelephonic;
    IBOutlet MedRepElementDescriptionLabel *customerNameLabel;
    IBOutlet MedRepElementTitleLabel *customerNumberLabel;
    IBOutlet MedRepElementDescriptionLabel *availableBalanceLabel;
    IBOutlet UICollectionView *visitOptionCollectionView;
    
    NSMutableArray *visitOptionCollectionViewArray;
}

@property (nonatomic, strong) NSMutableDictionary *customerDictornary;
@property (nonatomic, strong) NSString *visitID;
-(void)startVisit;
- (void)closeVisitWithEndDate:(id)sender;

@property(strong,nonatomic)id delegate;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property(strong,nonatomic) SynViewController *synVC;


@end
