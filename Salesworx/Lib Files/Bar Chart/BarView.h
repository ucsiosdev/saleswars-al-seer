 
//  BarView.h
//  MIMChartLib
//
//  Created by  Harshad Ashraf  on 15/08/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@protocol BarViewDelegate <NSObject>

-(void)displayFloatingView:(id)view;

@end

@interface BarView : UIView {
    
    MIMColorClass *color;
    NSDictionary *lColor;
    NSDictionary *dColor;    
    UIColor *borderColor;
    GRADIENT_STYLE gradientStyle;
    GLOSS_STYLE glossStyle;
    BOOL negativeBar;
    

    
}
@property(nonatomic,retain) MIMColorClass *color;
@property(nonatomic,retain) NSDictionary *lColor;
@property(nonatomic,retain) NSDictionary *dColor;    
@property(nonatomic,retain) UIColor *borderColor;
@property(nonatomic,assign) GRADIENT_STYLE gradientStyle;
@property(nonatomic,assign) GLOSS_STYLE glossStyle;
@property(nonatomic,assign) BOOL negativeBar;
@property(nonatomic,assign) id delegate;
@end
