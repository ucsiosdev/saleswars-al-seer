 
//  AnchorInfo.h
//  MIM2D Library
//
//  Created by  Harshad Ashraf  on 27/07/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface AnchorInfo : UIView {
    
    NSString *infoString;
    NSInteger tagID;
    BOOL displaying;
}
@property(nonatomic,retain) NSString *infoString;
@property(nonatomic,assign) NSInteger tagID;
@property(nonatomic,assign) BOOL displaying;
@end
