 
//  YAxisBand.h
//  MIM2D Library
//
//  Created by  Harshad Ashraf  on 08/07/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import "MIMColorClass.h"
#import "Constant.h"

@interface YAxisBand : UIView {


    NSDictionary *properties;
    NSArray *yTitles;

}
@property(nonatomic,retain)NSDictionary *properties;
@property(nonatomic,retain)NSArray *yTitles;

@end
