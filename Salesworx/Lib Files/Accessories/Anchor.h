
//  Anchor.h
//  MIM2D Library
//
//  Created by Harshad Ashraf on 07/07/11.
//  Copyright (c) 2012 __MIM 2D__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnchorDelegate.h"
#import "Constant.h"


@interface Anchor : UIView 
{
    
    id<AnchorDelegate> delegate;
    long int anchorTag;
    NSDictionary *properties;
    
    @private
    UITapGestureRecognizer *tapGesture;

}
@property(nonatomic,strong)id <AnchorDelegate>delegate;
@property(nonatomic,assign)long int anchorTag;
@property(nonatomic,retain)NSDictionary *properties;


-(void)drawAnchor;
-(void)createPopOutAnimation;
-(void)createBounceAnimation;
@end
