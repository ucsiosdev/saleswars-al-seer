//
//  OrderItemsViewController.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWFoundation.h"


@interface OrderItemsViewController : SWViewController < GridViewDataSource> {
    NSString *documentReference;
    NSArray *orderItems;
    GridView *gridView; 
}

@property (nonatomic, strong) NSString *documentReference;
@property (nonatomic, strong) NSArray *orderItems;
@property (nonatomic, strong) GridView *gridView;

- (id)initWithDocumentReference:(NSString *)ref;

@end
