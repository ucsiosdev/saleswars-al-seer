//
//  TamplateNameViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface TamplateNameViewController : SWTableViewController <EditableCellDelegate> {
    NSString *templateName;
    //       id target;
    SEL action;
}
@property (nonatomic, strong) NSString *templateName;

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

- (id)initWithTemplateName;

@end