//
//  ProductBonusView.h
//  SWProducts
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"

@interface ProductBonusView : UIView  < GridViewDataSource, GridViewDelegate > {
    NSArray *items;
    GridView *gridView;
    NSDictionary *product;
    NSString *discountRate;
    UILabel *discountLable;
}

@property (nonatomic, strong) GridView *gridView;

-(void)loadBonus;
- (id)initWithProduct:(NSDictionary *)product;

@end
