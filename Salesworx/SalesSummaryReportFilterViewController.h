//
//  SalesSummaryReportFilterViewController.h
//  SalesWars
//
//  Created by Prasann on 30/11/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "SWDatePickerViewController.h"
#import "CustomersListViewController.h"

#define kCustomerNameTextField @"Customer_Name"
#define kFromDateTextField @"From_Date"
#define kToDateTextField @"To_Date"
#define kCustomerTypeTextField @"Customer_Type"
#define kDocumentTypeTextField @"Document"

#define kCustomerNameTitle @"Customer Name"
#define kCustomerTypeTitle @"Customer Type"
#define kDocumentTypeTitle @"Document Type"

@protocol SalesSummaryFilterDelegate <NSObject>

-(void)filteredSalesSummary:(id)filteredContent;
-(void)filterParametersSalesSummary:(id)filterParameter;
-(void)salesSummaryFilterDidReset;

@end

@interface SalesSummaryReportFilterViewController : UIViewController<UIPopoverControllerDelegate, UITextFieldDelegate>
{
    NSString* selectedTextField;
    NSString* selectedPredicateString;
    UIPopoverController *currencyTypePopOver;
    NSMutableDictionary *filterParametersDict;
    NSString *fromDate;
    NSString *toDate;
    BOOL isFromDate;
    NSString *custType;
    NSString *DocType;
    BOOL isCustType;
    UIPopoverController *datePickerPopOverController;
    NSString *titleText;
    NSArray *customerTypeArray;
    NSArray *documentTypeArray;
    CustomersListViewController *currencyTypeViewController;

}
@property (strong, nonatomic) IBOutlet MedRepTextField *customerNameTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *toDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *customerTypeTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *documentTypeTextFiled;
@property (strong, nonatomic) IBOutlet MedRepButton *resetButton;
@property (strong, nonatomic) IBOutlet MedRepButton *searchButton;
@property(nonatomic) id delegate;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong,nonatomic) NSMutableDictionary *customerDictionary;
@property(strong,nonatomic) NSString* filterTitle;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(strong,nonatomic) NSMutableArray * salesSummaryArray;
@property(strong,nonatomic)SWDatePickerViewController *datePickerViewControllerDate;
@property(strong,nonatomic) NSString *selectedTextFieldType;


@end

