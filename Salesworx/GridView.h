//
//  GridView.h
//  SWFoundation
//
//  Created by Irfan Bashir on 5/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridCellView.h"
#import "GridCell.h"

@class GridView;

@protocol GridViewDataSource <NSObject>

@required
- (int)numberOfRowsInGrid:(GridView *)gridView;
- (int)numberOfColumnsInGrid:(GridView *)gridView;

@optional
- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column;
- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column;
- (int)indexOfColumnForInput:(GridView *)gridView;
- (UITextField *)gridView:(GridView *)gridView inputFieldForColumn:(int)column;
- (UIView *)gridView:(GridView *)gridView viewForColumn:(int)column;
- (NSString *)gridView:(GridView *)gridView keyForRow:(int)rowIndex;

/*
 @discussion
 The returned float value must be under 100 and it is represented as percentage.
 And all columns should be divided into 100%.
 */
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex;
@end

@protocol GridViewDelegate <NSObject>

@optional
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex;
- (void)gridView:(GridView *)gridView didFinishInputForKey:(NSString *)key andValue:(id)value;
- (void)gridView:(GridView *)gridView willStartInputForRow:(GridCell *)row andKey:(NSString *)key;
- (void)gridView:(GridView *)gridView commitDeleteRowAtIndex:(int)rowIndex;
@end

@interface GridView : UIView < UITableViewDelegate, UITableViewDataSource, EditableCellDelegate > {
    int columns;
    //id <GridViewDataSource> dataSource;
    id <GridViewDelegate> delegate;
    UITableView *tableView;
    NSArray *columnWidths;
    UIImageView *background;
    int inputColumnIndex;
    BOOL shouldAllowDeleting;
}

@property (nonatomic, assign) int columns;
@property (nonatomic, unsafe_unretained) id <GridViewDataSource> dataSource;
@property (unsafe_unretained) id <GridViewDelegate> delegate;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *columnWidths;
@property (nonatomic, assign) BOOL shouldAllowDeleting;

- (void)reloadData;
- (void)reloadRow:(int)rowIndex;
- (void)deleteRowAtIndex:(int)rowIndex;
- (void)reloadRowAtIndex:(int)rowIndex;
@end