//
//  ReviewDocumentFilterViewController.h
//  SalesWars
//
//  Created by Prasann on 15/01/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "CustomersListViewController.h"
#define kCustomerNameTextField @"Customer_Name"
#define kFromDateTextField @"From_Date"
#define kToDateTextField @"To_Date"
#define kDocumentTypeTextField @"Document_Type"

#define kCustomerNameTitle @"Customer Name"
#define kDocumentTypeTitle @"Document Type"
@protocol ReviewDocumentFilterDelegate <NSObject>

-(void)filteredReviewDocument:(id)filteredContent;
-(void)filterParametersReviewDocument:(id)filterParameter;
-(void)reviewDocumentFilterDidReset;

@end
@interface ReviewDocumentFilterViewController : UIViewController{
    NSMutableDictionary *filterParametersDict;
    NSArray *documentTypeArray;
    NSString *selectedTextField;
    NSString *titleText;
    NSString* selectedPredicateString;
    UIPopoverController *currencyTypePopOver;
    UIPopoverController *datePickerPopOverController;
    CustomersListViewController *currencyTypeViewController;

}
@property(strong,nonatomic) NSMutableDictionary *customerDictionary;
@property(strong,nonatomic) NSMutableDictionary * previousFilterParametersDict;
@property (strong, nonatomic) IBOutlet MedRepTextField *customerNameTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *toDateTextField;
@property (strong, nonatomic) IBOutlet MedRepTextField *documentTypeTextField;
@property(strong,nonatomic) NSString *selectedTextFieldType;
@property(strong,nonatomic) UIPopoverController * filterPopOverController;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UINavigationController * filterNavController;
@property(strong,nonatomic) NSMutableArray * reviewDocumentArray;
@property(strong,nonatomic) NSString* filterTitle;

@end

