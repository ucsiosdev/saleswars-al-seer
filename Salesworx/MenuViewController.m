//
//  MenuViewController.m
//  Salesworx
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
//UINavigationController *navigationControllerFront
#import "MenuViewController.h"
#import "SWFoundation.h"
#import "MenuTableViewCell.h"
#import "MenuItem.h"
#import <objc/runtime.h>
#import "SWPlatform.h"
#import "SWRouteViewController.h"
#import "AboutUsViewController.h"
#import "SWProductListViewController.h"
#import "CustomersListViewController.h"
#import "SWCollectionCustListViewController.h"
#import <mach/mach.h>
#import <mach/mach_host.h>
#import "ReturnNewViewController.h"
#import "DashboardAlSeerViewController.h"
#import "DashboardAlSeerViewController.h"
#import "AlSeerReportsViewController.h"
#import "SWDefaults.h"
@interface MenuViewController ()
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = true;

    app = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict stringForKey:@"CFBundleShortVersionString"];
    appVersionLabel.text = [NSString stringWithFormat:@"V %@",avid];
    
    listTableView.sectionHeaderHeight = 45;
    listTableView.sectionFooterHeight = 0;
    [self.view setBackgroundColor:UIColor.whiteColor];
    listTableView.tableFooterView = [UIView new];
    //self.navigationController.navigationBar
    
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateMenu];
}

- (void)updateMenu
{
    AppControl *appControl = [AppControl retrieveSingleton];
    isCollectionEnable = appControl.ENABLE_COLLECTION;
    
    if([isCollectionEnable isEqualToString:@"Y"])
    {
        
        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
            
            modules=[NSArray arrayWithObjects:
                     [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"DashboardAlSeerViewController" imageName:@"Menu_Dashboard"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SWRouteViewController" imageName:@"Menu_Route"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Product"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Collection", nil) className:@"SWCollectionCustListViewController" imageName:@"Menu_Collection"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"AlSeerReportsViewController" imageName:@"Menu_Reports"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
        }
        else
        {
            modules=[NSArray arrayWithObjects:
                     [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"DashboardAlSeerViewController" imageName:@"Menu_Dashboard"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SWRouteViewController" imageName:@"Menu_Route"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Product"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Collection", nil) className:@"SWCollectionCustListViewController" imageName:@"Menu_Collection"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"AlSeerReportsViewController" imageName:@"Menu_Reports"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
        }
    }
    else
    {
        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
            
            
            modules=[NSArray arrayWithObjects:
                     [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"DashboardAlSeerViewController" imageName:@"Menu_Dashboard"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SWRouteViewController" imageName:@"Menu_Route"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Product"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"AlSeerReportsViewController" imageName:@"Menu_Reports"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                    [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
   
        }
        
        else
        {
            modules=[NSArray arrayWithObjects:
                     [MenuItem itemWithTitle:NSLocalizedString(@"Dashboard", nil) className:@"DashboardAlSeerViewController" imageName:@"Menu_Dashboard"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Route", nil) className:@"SWRouteViewController" imageName:@"Menu_Route"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Customers", nil) className:@"CustomersListViewController" imageName:@"Menu_Customer"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Products", nil) className:@"SWProductListViewController" imageName:@"Menu_Product"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Survey", nil) className:@"GenCustListViewController" imageName:@"Menu_Survey"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Reports", nil) className:@"AlSeerReportsViewController" imageName:@"Menu_Reports"],
                     [MenuItem itemWithTitle:NSLocalizedString(@"Communication", nil) className:@"CommunicationViewControllerNew" imageName:@"Menu_Communication"],
                     
                     [MenuItem itemWithTitle:NSLocalizedString(@"Logout", nil) className:@"Logout" imageName:@"Menu_Logout"],nil];
        }
        
    }
    SWDashboardViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc] init];
    dashboardAlSeerViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc] init];
    
    
    ReportViewController1 =  (UIViewController *)[[AlSeerReportsViewController alloc]init];
    
    aboutVController = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil] ;
    synch = [[SynViewController alloc] init] ;
    settingVController = [[SettingViewController alloc] init] ;
    SWRouteViewController1 = (UIViewController *)[[SWRouteViewController alloc] init];
    CommunicationViewControllerNew1 = (UIViewController *)[[CommunicationViewControllerNew alloc]initWithNibName:@"CommunicationViewControllerNew" bundle:nil] ;
    SWCollectionCustListViewController1 =  (UIViewController *)[[SWCollectionCustListViewController alloc]init];
    SWCustomerListViewController1 =  (UIViewController *)[[CustomersListViewController alloc]init];
    SWProductListViewController1  =  (UIViewController *)[[SWProductListViewController alloc]init];
    SurveyFormViewController1 =  (UIViewController *)[[GenCustListViewController alloc] init] ;
    newSalesOrder1 = (UIViewController *)[[ReturnNewViewController alloc]initWithNibName:@"ReturnNewViewController" bundle:nil] ;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}


- (IBAction)infoButtonTapped:(id)sender {
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (modules != nil)
    {
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[aboutVController class]])
        {
            UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:aboutVController] ;
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}
- (IBAction)syncButtonTapped:(id)sender {
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (modules != nil)
    {
        if(synch)
        {
            synch=nil;
        }
        synch = [[SynViewController alloc] init] ;
        [synch setTarget:nil];
        [synch setAction:nil];
        
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[synch class]])
        {
            UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:synch] ;
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}
- (IBAction)settinfButtonTapped:(id)sender {
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (modules != nil)
    {
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[settingVController class]])
        {
            UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:settingVController] ;
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else
        {
            [revealController revealToggle:self];
        }
    }
}


#pragma mark UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return modules.count;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"CellIdentifier";
    MenuTableViewCell *cell = [tv dequeueReusableCellWithIdentifier:identifier];
    
    if (nil == cell)
    {
        cell = [[MenuTableViewCell alloc] initWithReuseIdentifier:identifier] ;
        
    }
    
    MenuItem *menuItem = [modules objectAtIndex:indexPath.row];
    [cell setImageName:menuItem.imageName];
    
    cell.textLabel.font = [UIFont fontWithName:@"WeblySleekUILight" size:16];
    
    [cell.textLabel setText:menuItem.title];
    
    if([app.selectedMenuIndexpath isEqual:indexPath])
    {
        [cell.textLabel setText:menuItem.title];
        cell.backgroundColor = MedRepUITableviewSelectedCellBackgroundColor;
        cell.statusLabel.backgroundColor = KTableViewSectionExpandColor;
    }
    else
    {
        cell.backgroundColor = UITableviewUnSelectedCellBackgroundColor;
        [cell.textLabel setText:menuItem.title];
        cell.statusLabel.backgroundColor = UIColor.clearColor;
    }
    
    [tableView setSeparatorColor:[UIColor clearColor]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    [tableView setSeparatorColor:[UIColor clearColor]];
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    synch=nil;
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    Singleton *single = [Singleton retrieveSingleton];
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;

    MenuItem *menuItem = [modules objectAtIndex:indexPath.row];
    
    [tableView1 deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NSString *className = menuItem.className;
    app.selectedMenuIndexpath = indexPath;

    
    if([className isEqualToString:@"Logout"])
    {
        
        UINavigationController *navigationControllerFront;
        
        
        
        
        
        if ([[SWDefaults checkTargetSalesDashBoard]isEqualToString:@"Y"]) {
            
            alseerDashBoard=[[DashboardAlSeerViewController alloc] init];
            navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:alseerDashBoard] ;

            
        }
        else
        {
            dashBViewController = [[DashboardAlSeerViewController alloc] init] ;
          navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:dashBViewController] ;

        }
        
        
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[dashBViewController class]])
        {
            [revealController setFrontViewController:navigationControllerFront animated:YES];
            
        } else {
            [revealController revealToggle:self];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionLogout" object:nil];
    }
    
    else if (modules != nil)
    {
        UIViewController *viewController = nil;
        
        
        if([className isEqualToString:@"SWDashboardViewController"])
        {
            if (single.isFullSyncDashboard) {
                SWDashboardViewController1 = nil;
                SWDashboardViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc] init];
                single.isFullSyncDashboard=NO;
            }
            
            
            
            
            viewController = SWDashboardViewController1;
        }
        
        else if([className isEqualToString:@"DashboardAlSeerViewController"])
        {
            if (single.isFullSyncMessage) {
                dashboardAlSeerViewController1=nil;
                dashboardAlSeerViewController1 = (UIViewController *)[[DashboardAlSeerViewController alloc]initWithNibName:@"DashboardAlSeerViewController" bundle:nil] ;
                single.isFullSyncMessage=NO;
            }
            viewController = dashboardAlSeerViewController1;
        }
        
        
        else if([className isEqualToString:@"CommunicationViewControllerNew"])
        {
            if (single.isFullSyncMessage) {
                CommunicationViewControllerNew1=nil;
                CommunicationViewControllerNew1 = (UIViewController *)[[CommunicationViewControllerNew alloc]initWithNibName:@"CommunicationViewControllerNew" bundle:nil] ;
                single.isFullSyncMessage=NO;
            }
            viewController = CommunicationViewControllerNew1;
        }
        else if([className isEqualToString:@"GenCustListViewController"])
        {
            if (single.isFullSyncSurvey)
            {
                SurveyFormViewController1=nil;
                SurveyFormViewController1 =  (UIViewController *)[[GenCustListViewController alloc] init] ;
                single.isFullSyncSurvey=NO;
            }
            viewController = SurveyFormViewController1;
        }
        else if([className isEqualToString:@"SWRouteViewController"])
        {
            
            viewController = SWRouteViewController1;
        }
        
        else if([className isEqualToString:@"SWProductListViewController"])
        {
            if (single.isFullSyncProduct) {
                SWProductListViewController1=nil;
                SWProductListViewController1  =  (UIViewController *)[[SWProductListViewController alloc]init];
                single.isFullSyncProduct=NO;
            }
            viewController = SWProductListViewController1;
        }
        
        else if([className isEqualToString:@"CustomersListViewController"])
        {
            if (single.isFullSyncCustomer) {
                SWCustomerListViewController1 =nil;
                SWCustomerListViewController1 =  (UIViewController *)[[CustomersListViewController alloc]init];
                single.isFullSyncCustomer=NO;
            }
            
            viewController = SWCustomerListViewController1;
            
            single.showCustomerDashboard=YES;
            
        }

        
        else if([className isEqualToString:@"AlSeerReportsViewController"])
        {
            viewController = ReportViewController1;
        }
        else if([className isEqualToString:@"SWCollectionCustListViewController"])
        {
            if (single.isFullSyncCollection)
            {
                SWCollectionCustListViewController1=nil;
                SWCollectionCustListViewController1 =  (UIViewController *)[[SWCollectionCustListViewController alloc]init];
                single.isFullSyncCollection=NO;
            }
            viewController = SWCollectionCustListViewController1;
        }
        
        NSLog(@"%@",((UINavigationController *)revealController.frontViewController).topViewController);
        if ([revealController.frontViewController isKindOfClass:[UINavigationController class]] && ![((UINavigationController *)revealController.frontViewController).topViewController isKindOfClass:[viewController class]])
        {
            UINavigationController *navigationControllerFront = [[UINavigationController alloc] initWithRootViewController:viewController];
            [revealController setFrontViewController:navigationControllerFront animated:YES];
        }
        else { // Seems the user attempts to 'switch' to exactly the same controller he came from!
            [revealController revealToggle:self];
            
        }
        
        [tableView1 reloadData];
    }
}
- (void) report_memory {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        //NSLog(@"Memory in use (in MB): %f", info.resident_size/(1024*1024.f));
    } else {
        //NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
}
//- (void)customerServiceDidGetCasCutomerList:(NSArray *)customerList
- (void)getServiceDidGetCasCutomerList:(NSArray *)customerList
{
   Singleton *single = [Singleton retrieveSingleton];
    single.cashCustomerList = customerList;
    customerList=nil;
}

- (void)lastSychDelegate:(NSString *)synchDate
{
    dateString = synchDate;
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
