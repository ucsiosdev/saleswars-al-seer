//
//  AlSeerSalesOrderPreviewViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/20/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSalesOrderPreviewViewController.h"
#import "FMDBHelper.h"
#import "SWDatabaseManager.h"
#import "AlSeerSalesOrderPreviewTableViewCell.h"
#import "UIViewController+MJPopupViewController.h"
#import "AlSeerSalesOrderPreviewHeaderTableViewCell.h"
#import "OrderAdditionalInfoViewController.h"



@interface AlSeerSalesOrderPreviewViewController ()

@end

@implementation AlSeerSalesOrderPreviewViewController
@synthesize salesOrderDict,isReturn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSString* proformaOrderQry=[NSString stringWithFormat:@"select  * from TBL_Proforma_Order "];
    NSArray* testArray=[FMDBHelper executeQuery:proformaOrderQry];
    NSLog(@"test array is %@", [testArray description]);
  
    NSLog(@"sales order data %@", [salesOrderDict description]);
    
    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillAppear:(BOOL)animated
{
    if (isReturn==YES) {
        
        self.titleLabel.text=@"Return Preview";
    }
}


#pragma mark table view data source methods

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    
    AlSeerSalesOrderPreviewHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"previewHeaderCell"];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderPreviewHeaderTableViewCell" owner:nil options:nil] firstObject];
        
        //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
    }
    
    return cell;
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  [(NSArray*)[salesOrderDict valueForKey:@"OrderItems"] count];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* identifier=@"previewCell";
    
   // AlSeerSalesOrderPreviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    
    AlSeerSalesOrderPreviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderPreviewTableViewCell" owner:nil options:nil] firstObject];
        
        //cell.textLabel.text=[agencyArray valueForKey:@"Agency"];
    }
    

    
    
    
    
    

    
    cell.productNameLbl.text=[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Description"]];
    
    cell.quantityLbl.text=[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Qty"]];
   
    
    
    NSString* netPriceString=[NSString stringWithFormat:@"%0.2f",[[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Net_Price"] doubleValue]];
    
    
    
    double netPrice=[netPriceString doubleValue];
    
    
    
    cell.unitPriceLbl.text=[NSString stringWithFormat:@"%@",[[NSNumber numberWithDouble:netPrice] descriptionWithLocale:[NSLocale currentLocale]]];
    
    
    
    
//    NSString* formattedNetPrice =[SWDefaults formatStringWithThousandSeparator:[[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Net_Price"]] integerValue]];
//    
//    
//    cell.unitPriceLbl.text=[NSString stringWithFormat:@"%@",formattedNetPrice];
    
    
    
    double discount=[[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Discounted_Amount"] doubleValue];
    cell.discountLbl.text=[NSString stringWithFormat:@"%0.2f",[[[NSNumber numberWithDouble:discount] descriptionWithLocale:[NSLocale currentLocale]] doubleValue]];
    

    
    
//    cell.discountLbl.text=[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Discounted_Amount"]];
//    
    
    NSLog(@"dict in preview %@", [salesOrderDict description]);
    
    
    if (isReturn==YES) {
         cell.uomLbl.text=[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"SelectedUOM"]];
    }
    
    else
    {
    
    cell.uomLbl.text=[NSString stringWithFormat:@"%@",[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Order_Quantity_UOM"]];
    }
    
    
    float total=[[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Qty"] floatValue]*[[[[salesOrderDict valueForKey:@"OrderItems"] objectAtIndex:indexPath.row] valueForKey:@"Net_Price"] floatValue];
    
    
   // NSString* formattedTotal=[SWDefaults formatStringWithThousandSeparator:total ];
    
    
   
    cell.totalLbl.text=[NSString stringWithFormat:@"%0.2f", total];
    cell.netAmountLbl.text=[NSString stringWithFormat:@"%0.2f", total];
    
    
    
    

    
    
    
    return cell;
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)okButtonTapped:(id)sender {
    
      // [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

    
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    
//    OrderAdditionalInfoViewController* orderAddlInfo=[[OrderAdditionalInfoViewController alloc]init];
//    [orderAddlInfo dismissPreview];
    

    
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

@end
