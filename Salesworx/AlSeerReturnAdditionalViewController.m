//
//  AlSeerReturnAdditionalViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/19/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerReturnAdditionalViewController.h"
#import "AlSeerSalesOrderViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "AlSeerSalesOrderPreviewViewController.h"
#import "SWFoundation.h"

@interface AlSeerReturnAdditionalViewController ()

@end


@implementation AlSeerReturnAdditionalViewController

@synthesize customerDict,salesOrderDict,customerIDLbl,customerNameLbl,commentsTxtView,nameTxtFld,customSignatureView,invoiceNumberTxtFld,signVc,customerAvailableBalanceLbl,arrReturnItem,orderItemView,orderItemCountLabel,totalAmount,lblAvilableBalStatic,lblInvoiceNumberStatic,lblSigneeNameStatic,lblCommentStatic,lblCustomerStatus,lblReturningItemsStatic,lblDetails;


- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initObj];
    [self setupView];
    [self setAllViewBorder];
    [self setNavigBar];
    [self setFontsAndColor];
    [self addShadowOnUI];
    
    if (@available(iOS 15.0, *)) {
        self.orderItemTableView.sectionHeaderTopPadding = 0;
    }
}



-(void)viewWillAppear:(BOOL)animated
{
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = kFontWeblySleekSemiBold(19);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Return Confirmation";
    // emboss in the same way as the native title
    //    [label setShadowColor:[UIColor darkGrayColor]];
    //    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(keyboardWillDisappear:)
                                                 name: UIKeyboardWillHideNotification object:nil];

    
     if ([customerDict objectForKey:@"Customer_Name"]) {
         customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_Name"]];
     }
    
    if ([customerDict objectForKey:@"Customer_No"]) {
         customerIDLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_No"]];
    }
    
    if ([customerDict objectForKey:@"Avail_Bal"]) {
        customerAvailableBalanceLbl.text = [NSString stringWithFormat:@"%@",[[customerDict stringForKey:@"Avail_Bal"] currencyString]];
    }
    
    //preview button
    
    
//    UIBarButtonItem *leftButtonClicked=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(presentCustomPreviewViewController)];
//    
//    [self.navigationItem setRightBarButtonItem:leftButtonClicked];
    
    /*
    UIBarButtonItem *previewBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preview1.png"] style:UIBarButtonItemStylePlain target:self action:@selector(presentCustomPreviewViewController)];
    
    [self.navigationItem setRightBarButtonItem:previewBtn];
      */
    
    
    returnAdditionalInfoDict=[[NSMutableDictionary alloc]init];
    //    [orderAdditionalInfoDict setObject:[NSDate date] forKey:@"ship_date"];
    
    oldFrame=self.view.frame;
    
   // customSignatureView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    //customSignatureView.layer.borderWidth = 1.0;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    nameTxtFld.leftView = paddingView;
    nameTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingViewCust = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5,30)];
    invoiceNumberTxtFld.leftView = paddingViewCust;
    invoiceNumberTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    /*
    for (UIView *currentView in self.view.subviews){
        if([currentView isKindOfClass:[UITextField class]]){
            
            
            
            if (currentView.tag==1||currentView.tag==2||currentView.tag==3) {
                currentView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
                currentView.layer.borderWidth = 1.0;
                
            }
            
            
        }
    }
    */
    //signVc=[[AlSeerSignatureViewController alloc]init];
    //
    
    
   // [customSignatureView addSubview:signVc.view];
    
    
    
    [returnAdditionalInfoDict setValue:@"NO" forKey:@"goodCollected"];
    
    
    
    [returnAdditionalInfoDict setValue:@"C" forKey:@"returnType"];

    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    signVc=[[AlSeerSignatureViewController alloc]init];
    [customSignatureView addSubview:signVc.view];
    
    
    //table bottom round corner
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_orderItemTableView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _orderItemTableView.bounds;
    maskLayer.path  = maskPath.CGPath;
    _orderItemTableView.layer.mask = maskLayer;
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [signVc removeFromParentViewController];
    signVc = nil;
}

-(void)initObj{
    
    self.orderItemTableView.delegate=self;
    self.orderItemTableView.dataSource=self;
    
    if ([[salesOrderDict objectForKey:@"info"] valueForKey:@"orderAmt"]){
        totalAmount.text = [[[salesOrderDict valueForKey:@"info"]valueForKey:@"orderAmt"] currencyString];
    }
    
    
    
}

-(void)setupView{
    
    
    
    BOOL customerStatus = [[SWDefaults getValidStringValue: [customerDict objectForKey:@"Cust_Status"]] isEqualToString:@"Y"];
    BOOL creditHold = [[SWDefaults getValidStringValue:[customerDict objectForKey:@"Credit_Hold"]] isEqualToString:@"Y"];
    
    
    if (!customerStatus || creditHold) {
        lblCustomerStatus.text = @"Blocked";
        lblCustomerStatus.backgroundColor =  UIColorFromRGB(0xFF736E);
        lblCustomerStatus.hidden = NO;
    }else {
        lblCustomerStatus.hidden = YES;
        //lblCustomerStatus.text = @"Active";
        //lblCustomerStatus.backgroundColor = UIColorFromRGB(0x47B681);
    }
    lblCustomerStatus.textColor =  [UIColor whiteColor];
    lblCustomerStatus.layer.cornerRadius = 10.0;
    lblCustomerStatus.layer.masksToBounds = YES;
    
     self.orderItemTableView.tableFooterView = [UIView new];
     orderItemCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)arrReturnItem.count];
    
    
    invoiceNumberTxtFld.backgroundColor = [UIColor whiteColor];
    nameTxtFld.backgroundColor = [UIColor whiteColor];
    commentsTxtView.backgroundColor = [UIColor whiteColor];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    
   // invoiceNumberTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
   // invoiceNumberTxtFld.layer.borderWidth = 1.0;
    
    //invoiceNumberTxtFld.layer.sublayerTransform = CATransform3DMakeTranslation(3.0f, 0.0f, 0.0f);
    
    //nameTxtFld.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
   // nameTxtFld.layer.borderWidth = 1.0;
    
    //nameTxtFld.layer.sublayerTransform = CATransform3DMakeTranslation(3.0f, 0.0f, 0.0f);
    
    //adding border to textview
    //commentsTxtView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    //commentsTxtView.layer.borderWidth = 1.0;
    
    //customSignatureView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    //customSignatureView.layer.borderWidth = 1.0;
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    nameTxtFld.leftView = paddingView;
    nameTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingViewCust = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5,30)];
    invoiceNumberTxtFld.leftView = paddingViewCust;
    invoiceNumberTxtFld.leftViewMode = UITextFieldViewModeAlways;
   
    
}

-(void)setNavigBar{
    
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(saveReturnTapped:)];
    
    [save setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    
    
   // UIBarButtonItem *previewBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preview1.png"] style:UIBarButtonItemStylePlain target:self action:@selector(presentCustomPreviewViewController)];
    
    self.navigationItem.rightBarButtonItem = save;
    
   
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [back setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    
    self.navigationItem.leftBarButtonItem = back;
    
}



- (void)handleBack:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) setFontsAndColor{
    
    
    
    customerNameLbl.font = kFontWeblySleekSemiBold(16);
    customerNameLbl.textColor = UIColorFromRGB(0x2C394A);
    
    customerIDLbl.font = kFontWeblySleekSemiBold(14);
    customerIDLbl.textColor = UIColorFromRGB(0x6A6F7B);
    
    customerAvailableBalanceLbl.font = kFontWeblySleekSemiBold(16);
    customerAvailableBalanceLbl.textColor = UIColorFromRGB(0x2C394A);
    
    lblAvilableBalStatic.font = kFontWeblySleekSemiBold(14);
    lblAvilableBalStatic.textColor = UIColorFromRGB(0x6A6F7B);
    

    lblInvoiceNumberStatic.font = kFontWeblySleekSemiBold(14);
    lblInvoiceNumberStatic.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblSigneeNameStatic.font = kFontWeblySleekSemiBold(14);
    lblSigneeNameStatic.textColor = UIColorFromRGB(0x6A6F7B);
    
    lblCommentStatic.font = kFontWeblySleekSemiBold(14);
    lblCommentStatic.textColor = UIColorFromRGB(0x6A6F7B);
    
    nameTxtFld.font=kFontWeblySleekSemiBold(14);
    invoiceNumberTxtFld.font=kFontWeblySleekSemiBold(14);
    nameTxtFld.textColor =  UIColorFromRGB(0x2C394A);
    invoiceNumberTxtFld.textColor =  UIColorFromRGB(0x2C394A);
    commentsTxtView.font=kFontWeblySleekSemiBold(14);
    commentsTxtView.textColor =  UIColorFromRGB(0x2C394A);
    
    
    commentsTxtView.font=kFontWeblySleekSemiBold(14);
    commentsTxtView.textColor =  UIColorFromRGB(0x2C394A);
    
    lblReturningItemsStatic.font=kFontWeblySleekSemiLight(16);
    lblReturningItemsStatic.textColor =  UIColorFromRGB(0x2C394A);
    
    lblDetails.font=kFontWeblySleekSemiLight(16);
    lblDetails.textColor =  UIColorFromRGB(0x2C394A);
    
}


-(void) setAllViewBorder{
    
    //customerDetailView.layer.cornerRadius = 8.0;
   // commentDetailView.layer.cornerRadius = 8.0;
    //signatureDetailView.layer.cornerRadius = 8.0;
    //invoiceNumberTxtFld.layer.cornerRadius = 8.0;
    //nameTxtFld.layer.cornerRadius = 8.0;
   // commentsTxtView.layer.cornerRadius = 8.0;
    //commentsTxtView.layer.masksToBounds = YES;
    //nameTxtFld.layer.masksToBounds = YES;
    //invoiceNumberTxtFld.layer.masksToBounds = YES;
    //signatureDetailView.layer.masksToBounds = YES;
    //commentDetailView.layer.masksToBounds = YES;
    //customerDetailView.layer.masksToBounds = YES;
   // orderItemView.layer.cornerRadius = 8.0;
    //orderItemView.layer.masksToBounds = YES;
    
    orderItemCountLabel.layer.cornerRadius = 9.0;
    orderItemCountLabel.layer.masksToBounds = YES;
    
    //_orderItemTableView.layer.cornerRadius = 8.0;
    //_orderItemTableView.layer.masksToBounds = YES;
    
}


-(void)addShadowOnUI{
    [self setShadowOnUILayer:invoiceNumberTxtFld.layer];
    [self setShadowOnUILayer:nameTxtFld.layer];
    [self setShadowOnUILayer:commentsTxtView.layer];
    
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}



-(void)presentCustomPreviewViewController

{
    
    AlSeerSalesOrderPreviewViewController* previewVC=[[AlSeerSalesOrderPreviewViewController alloc]init];
    previewVC.salesOrderDict=salesOrderDict;
    previewVC.delegate=self;
    previewVC.isReturn=YES;

    
    // [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom];
    
    
    [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
    
    
    
}


#pragma mark UITextView Delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (textField==nameTxtFld) {
        return newLength <= 12;
        
    }
    
    else if (textField==invoiceNumberTxtFld) {
        
        return newLength <= 15;
        
    }
    return 100;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField

{
    
    if (textField.tag==2) {
        
        
    [UIView animateWithDuration: 0.5 animations:^{
        //restore your tableview
        
        NSLog(@"view frame in dis appear %@", NSStringFromCGRect(self.view.frame));
        
        
        self.view.frame = CGRectMake(0, -100, 1024, 704);
        
        
    } completion:^(BOOL finished) {
    }];
        
    }

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==nameTxtFld) {
        
        [returnAdditionalInfoDict setObject:textField.text forKey:@"name"];
        
    }
    
    else if (textField==invoiceNumberTxtFld)
    {
        [returnAdditionalInfoDict setObject:textField.text forKey:@"reference"];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==commentsTxtView) {
        
        [returnAdditionalInfoDict setObject:textView.text forKey:@"comments"];
        
        NSLog(@"comments set %@", [returnAdditionalInfoDict description]);
        
        
    }
}



- (void) keyboardWillDisappear: (NSNotification*) aNotification
{
    [UIView animateWithDuration: [self keyboardAnimationDurationForNotification: aNotification] animations:^{
        //restore your tableview
        
        NSLog(@"view frmae in dis appear %@", NSStringFromCGRect(self.view.frame));
        
        
        self.view.frame = oldFrame;
        
        
    } completion:^(BOOL finished) {
    }];
    
    
    
    //    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
}


- (NSTimeInterval) keyboardAnimationDurationForNotification:(NSNotification*)notification
{
    NSDictionary* infoDict = [notification userInfo];
    NSValue* value = [infoDict objectForKey: UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval duration = 0;
    [value getValue: &duration];
    
    return duration;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

- (IBAction)saveReturnTapped:(id)sender {
    
    
    NSData* savedImageData=[SWDefaults fetchSignature];
    
    NSLog(@"check the saved image tyoe %@", savedImageData);
    
    UIImage *image ;
    
    if (savedImageData) {
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        image = [UIImage imageWithData:savedImageData scale:screenScale];
        [returnAdditionalInfoDict setObject:savedImageData forKey:@"signature"];
        
    }
    
    
    if([self validateInput])
    {
        info=returnAdditionalInfoDict;
        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrderDict objectForKey:@"info"]]  ;
        [infoOrder setValue:returnAdditionalInfoDict forKey:@"extraInfo"];
        [infoOrder setValue:@"R" forKey:@"order_Status"];
        [salesOrderDict setValue:infoOrder forKey:@"info"];
        
        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveReturnOrderWithCustomerInfo:customerDict andOrderInfo:salesOrderDict andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
        
        
        UIAlertView* alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"RMA Document created successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        alert.tag=10;
        [alert show];
    }
    else {
        
        NSMutableString *message= [NSMutableString stringWithString:NSLocalizedString(@"Please fill all required fields", nil)];;
        
        AppControl *appControl = [AppControl retrieveSingleton];
        
        isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
        if ([isSignatureOptional isEqualToString:@"Y"])
        {
            if ([returnAdditionalInfoDict objectForKey:@"signature"])
            {
                if (![returnAdditionalInfoDict objectForKey:@"name"])
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
                else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
                {
                    message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
                }
            }
        }
        else{
            
            if (![returnAdditionalInfoDict objectForKey:@"name"])
            {
                message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
            {
                message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            
            if (![returnAdditionalInfoDict objectForKey:@"signature"])
            {
                message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            else if ([[returnAdditionalInfoDict objectForKey:@"signature"] length] < 1)
            {
                message = [NSMutableString stringWithString:@"Please provide 'Signature' with 'Name'."];
            }
            
        }
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:message withController:self];
    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 10)
    {
        if (buttonIndex==[alertView cancelButtonIndex])
        {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
        }
        else {
            NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
        }
    }
}

- (BOOL)validateInput
{
    AppControl *appControl = [AppControl retrieveSingleton];
    
    
    NSLog(@"is sign optional %@", appControl.IS_SIGNATURE_OPTIONAL );
    
    
    isSignatureOptional = appControl.IS_SIGNATURE_OPTIONAL;
    //if ([[SWDefaults appControl]count]==0)//IS_SIGNATURE_OPTIONAL
    if ([isSignatureOptional isEqualToString:@"Y"])
    {
        if ([returnAdditionalInfoDict objectForKey:@"signature"])
        {
            if (![returnAdditionalInfoDict objectForKey:@"name"])
            {
                return NO;
            }
            else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
            {
                return NO;
            }
        }
        
    }
    else{
        
        if (![returnAdditionalInfoDict objectForKey:@"name"])
        {
            return NO;
        }
        else if ([[returnAdditionalInfoDict objectForKey:@"name"] length] < 1)
        {
            return NO;
        }
        
        if (![returnAdditionalInfoDict objectForKey:@"signature"])
        {
            return NO;
        }
        else if ([[returnAdditionalInfoDict objectForKey:@"signature"] length] < 1)
        {
            return NO;
        }
        
    }
    if (![returnAdditionalInfoDict objectForKey:@"goodCollected"])
    {
        return NO;
    }
    else if ([[returnAdditionalInfoDict objectForKey:@"goodCollected"] length] < 1)
    {
        return NO;
    }
    if (![returnAdditionalInfoDict objectForKey:@"returnType"])
    {
        return NO;
    }
    else if ([[returnAdditionalInfoDict objectForKey:@"returnType"] length] < 1)
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField==invoiceNumberTxtFld) {
        
        [textField resignFirstResponder];
        [commentsTxtView becomeFirstResponder];
        return NO;
    }
    else if(textField==nameTxtFld)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}



#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrReturnItem.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"TblCellRetrunConfirmation";
    TblCellRetrunConfirmation *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:233.0/255.0 green:251.0/255.0 blue:250.0/255.0 alpha:1.0]];
    
    
    
    cell.productNameLbl.text=@"Product";
    cell.quantityLbl.text=@"Quantity";
    cell.uomLbl.text=@"UOM";
    cell.unitPriceLbl.text=@"Unit Price (AED)";
    cell.totalLbl.text=@"Total (AED)";
    
    
    cell.productNameLbl.textColor  = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.quantityLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.uomLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.unitPriceLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.totalLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    [cell setBorderColor:[UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0]];
    
    //    cell.lblCellSeparator.backgroundColor = [UIColor blackColor];
    //    orderItemTableView.separatorColor = [UIColor clearColor];
    cell.lblHeaderLineTop.hidden = NO;
    cell.lblHeaderLineBottom.hidden = NO;
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"TblCellRetrunConfirmation";
    TblCellRetrunConfirmation *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:identifier owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableArray *itemArray = [self.arrReturnItem objectAtIndex:indexPath.row];
    
    cell.productNameLbl.text = [NSString stringWithFormat:@"%@",[itemArray valueForKey:@"Description"]];
    cell.quantityLbl.text = [NSString stringWithFormat:@"%@",[itemArray valueForKey:@"Qty"]];
    
    NSString* netPriceString=[NSString stringWithFormat:@"%0.2f",[[itemArray valueForKey:@"Net_Price"] doubleValue]];
    
    double netPrice=[netPriceString doubleValue];
   
    cell.unitPriceLbl.text=[NSString stringWithFormat:@"%@",[[NSNumber numberWithDouble:netPrice] descriptionWithLocale:[NSLocale currentLocale]]];
    //cell.unitPriceLbl.text = [NSString stringWithFormat:@"%@",[itemArray valueForKey:@"Unit_Selling_Price"]];
    cell.uomLbl.text = [NSString stringWithFormat:@"%@",[itemArray valueForKey:@"SelectedUOM"]];
   
    float total=[[itemArray valueForKey:@"Qty"] floatValue]*[[itemArray valueForKey:@"Net_Price"] floatValue];
    
    cell.totalLbl.text=[NSString stringWithFormat:@"%0.2f", total];
    cell.lblHeaderLineTop.hidden = YES;
    cell.lblHeaderLineBottom.hidden = YES;
    
    return cell;
}

@end
