//
//  CustomerShipAddressDetails.h
//  DataSyncApp
//
//  Created by sapna on 1/30/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerShipAddressDetails : NSObject
{
    int _Customer_ID;
    int _Site_Use_ID;
    NSString *_Customer_Name;
}
@property(nonatomic,assign) int Customer_ID;
@property(nonatomic,assign) int Site_Use_ID;
@property(nonatomic,copy) NSString *Customer_Name;

-(id)initWithCustomerID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID Customer_Name:(NSString*)Customer_Name;
@end
