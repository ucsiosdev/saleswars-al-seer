//
//  AllUserList.h
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllUserList : NSObject
{
    int _User_ID;
    NSString *_Username;
}
@property(nonatomic,assign) int User_ID;
@property(nonatomic,copy) NSString *Username;
-(id)initWithUserId:(int)User_ID User:(NSString*)Username;
@end
