//
//  PlannedVisitDetails.m
//  DataSyncApp
//
//  Created by sapna on 1/30/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "PlannedVisitDetails.h"

@implementation PlannedVisitDetails
@synthesize Planned_Visit_ID =_Planned_Visit_ID;
@synthesize FSR_Plan_Detail_ID =_FSR_Plan_Detail_ID;
@synthesize Visit_Date =_Visit_Date;
@synthesize Customer_ID =_Customer_ID;
@synthesize Site_Use_ID =_Site_Use_ID;
@synthesize Start_Time =_Start_Time;
@synthesize End_Time =_End_Time;
@synthesize SalesRep_ID =_SalesRep_ID;
@synthesize Visit_Status=_Visit_Status;

-(id)initWithPlannedVisitID:(NSString*)Planned_Visit_ID FSR_Plan_Detail_ID:(int)FSR_Plan_Detail_ID Visit_Date:(NSString*)Visit_Date Customer_ID:(int)Customer_ID Site_Use_ID:(int)Site_Use_ID Start_Time:(NSString*)Start_Time End_Time:(NSString*)End_Time SalesRep_ID:(int)SalesRep_ID Visit_Status:(NSString*)Visit_Status
{
      if ((self = [super init])) {
          
          self.Planned_Visit_ID=_Planned_Visit_ID;
          self.FSR_Plan_Detail_ID =FSR_Plan_Detail_ID;
          self.Visit_Date=Visit_Date;
          self.Customer_ID=Customer_ID;
          self.Site_Use_ID=Site_Use_ID;
          self.Start_Time=Start_Time;
          self.End_Time=End_Time;
          self.SalesRep_ID=SalesRep_ID;
          self.Visit_Status=Visit_Status;
          
      }
    return self;
}

@end
