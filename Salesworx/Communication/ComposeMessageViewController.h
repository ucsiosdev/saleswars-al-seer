//
//  ComposeMessageViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RecipientPopoverViewController.h"
#import "FTPBaseViewController.h"

@interface ComposeMessageViewController : FTPBaseViewController <RecipientPickerDelegate,UITextViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate>
{
    
    IBOutlet UIButton *btnDropdown;
    IBOutlet UITextField *txtTitle,*txtToField;
    IBOutlet UITextView *txtViewMessage;
    IBOutlet UILabel *lblDate;
    IBOutlet UIImageView *mainImage;

    IBOutlet UIView *composeView;
    IBOutlet UIButton *sendBtn;

    RecipientPopoverViewController *_recipientPicker;
    UIPopoverController *_colorPickerPopover;
    
    int MessageID;
    
    NSString *alertType;
    
    NSArray *toFieldArray;
    NSArray *recipientIDArray;
    
    CGRect popoverFrame;
    
    
}
@property (nonatomic, strong) RecipientPopoverViewController *recipientPicker;
@property (nonatomic, strong) UIPopoverController *colorPickerPopover;
-(IBAction)btnSubmitPressed:(id)sender;
-(IBAction)btnToFieldPressed:(id)sender;
@end
