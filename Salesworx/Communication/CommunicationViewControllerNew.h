//
//  CommunicationViewControllerNew.h
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatePickerViewController.h"
#import "CustomerShipAddressDetails.h"
#import "SWPlatform.h"
#import "SalesWorxDropShadowView.h"
#import "FSRMessagedetails.h"

@interface CommunicationViewControllerNew : FTPBaseViewController<DatePickerDelegate,UISearchBarDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    UIPopoverController *datePickerPopover;
    DatePickerViewController *_colorPicker;
    CustomerShipAddressDetails *customerInfo;
    
    IBOutlet UIView *noContentView;
    IBOutlet SalesWorxDropShadowView *messageDetailView;
    IBOutlet SalesWorxDropShadowView *replyView;
    NSMutableArray *arrSentMsg;
    NSMutableArray *arrInboxMsg;
    //NSArray *selectedMessage;
    FSRMessagedetails *selectedMessage;
    NSArray *planeedVisitArray;
    NSMutableArray *CustomerShipDetailsArray;
    NSMutableArray *arrFilterdInboxMessage;
    NSMutableArray *arrFilterdSentMessage;
    
    BOOL isInboxViewSelected;
    IBOutlet UITableView *messageTableView;
    
    UIPopoverController *popOverForNewMessage;

    BOOL isSearchingON;
    UIBarButtonItem *composeEmail;

    NSMutableDictionary *customer;
    BOOL isFurtureDate;
    UIView *myBackgroundView;
    IBOutlet UILabel *surveyTitleLabel;
    IBOutlet UILabel *fromTitleLabel;
    IBOutlet UILabel *fromLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UITextView *mailTextView;
    IBOutlet UITextView *replyTextView;
    IBOutlet UIButton *replyButton;
    NSIndexPath *selectedIndexPath;
    IBOutlet UILabel *inboxCountLabel;
    IBOutlet UILabel *inboxBottomBarLabel;
    IBOutlet UILabel *sentBottomBarLabel;
    IBOutlet NSLayoutConstraint *xreplyViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xReplyViewTopConstraint;
    
    IBOutlet SalesWorxDropShadowView *messageView;
    IBOutlet UIView *inboxView;
    IBOutlet UIView *sentView;
    
    IBOutlet UIButton *btnInbox;
    IBOutlet UIButton *btnSent;
    IBOutlet UIView *viewSearchBar;
    IBOutlet UISearchBar *searchBarMsg;
    IBOutlet UIView *sendView;
    IBOutlet UILabel *lblReplySatic;
    IBOutlet UIView *viewInbox;
    IBOutlet UIView *viewSent;
    
    
    
}

@property (nonatomic, strong) NSMutableDictionary *customer;
//-(IBAction)btnAddMessagePressed:(id)sender;
-(IBAction)btnSerachMessagePressed:(id)sender;
-(IBAction)btnDatePressed:(id)sender;

@end
