//
//  ReadMessageViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "ReadMessageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SWPlatform.h"
@interface ReadMessageViewController ()

@end

@implementation ReadMessageViewController
@synthesize messageDeatils;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     //Set back button
    self.title =NSLocalizedString(@"Details", nil);

    self.navigationItem.hidesBackButton = NO;

    [self setMessageDeatils];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        mainImage.image=[UIImage imageNamed:@"Message-details-AR.png" cache:NO];
    }
    else
    {
        mainImage.image=[UIImage imageNamed:@"Message-details.png" cache:NO];
    }
//    [btnclose setTitle:NSLocalizedString(@"Close", nil) forState:UIControlStateNormal];
//    [btnSubmit setTitle:NSLocalizedString(@"Reply", nil) forState:UIControlStateNormal];

}
-(void)setMessageDeatils
{
    //Set content
    
    if(messageDeatils.Message_Reply.length==0)
    {
        
    }
    else
    {
        txtMessageResponseView.text = messageDeatils.Message_Reply;
    }
    lblTitle.text=messageDeatils.Message_Title;
    txtMessageView.text=messageDeatils.Message_Content;
    lblDate.text=messageDeatils.Message_date;
    
    //Update Read status and datetime 
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];

    NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Read = 'Y' , Message_Date ='%@' where Message_ID =%d and salesRep_ID=%d",dateString,messageDeatils.Message_ID,messageDeatils.SalesRep_ID ];
    [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
    formater=nil;
    usLocale=nil;
}
#pragma mark
#pragma mark Button action method
-(void)btnBackPressed
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
-(IBAction)btnPressed:(id)sender
{
    UIButton *btn =(UIButton*)sender;
    if(btn.tag==113)
    {
        //Respond Event
        txtMessageResponseView.editable=YES;
        txtMessageResponseView.hidden=false;
        btnSubmit.hidden=FALSE;
        lblResponse.hidden=FALSE;
        btnResponse.hidden=TRUE;
        btnclose.hidden=TRUE;
        
        
    }
    else if(btn.tag==114)
    {
        //Close Event
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
    }
    else{
       
        //Submit Event
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveNotificationForAddResponse:)
                                                     name:@"AddResponseNotification"
                                                   object:nil];
        

        if([txtMessageResponseView.text length]==0)
           {
               alertType =@"Failure";
               [self UserAlert:@"Error:Please add reply"];
               return;
               
           }
        
        //Update MessageReply and datetime
        NSDate *date =[NSDate date];
        NSDateFormatter *formater =[NSDateFormatter new];
        [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formater setLocale:usLocale];
        NSString *dateString = [formater stringFromDate:date];
        NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Reply ='%@' ,Reply_Date=\"%@\" ,Emp_Code='%@' where Message_ID =%d and salesRep_ID=%d",txtMessageResponseView.text,dateString,[[SWDefaults userProfile] stringForKey:@"Emp_Code"],messageDeatils.Message_ID,messageDeatils.SalesRep_ID ];
        [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
        formater=nil;
        usLocale=nil;
    }
}
#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"AddResponseNotification"])
    {
        alertType =@"Success";
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AddResponseNotification" object:nil];
        [self UserAlert:@"Message sent successfully"];
        txtMessageResponseView.editable=NO;
    
    }
}

#pragma mark
#pragma mark TextView delegate method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
       
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}
#pragma mark
#pragma mark Alertview method
           
-(void)UserAlert:(NSString *)Message
{
    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
    [ErrorAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertType isEqualToString:@"Success"])
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
#pragma mark
#pragma mark Orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
#pragma mark

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
