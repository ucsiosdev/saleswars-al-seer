//
//  CommunicationViewControllerNew.m
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "CommunicationViewControllerNew.h"
#import "MessageTableViewCell.h"
#import  "ComposeMessageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "FSRMessagedetails.h"
#import "ReadMessageViewController.h"
#import "CustomerShipAddressDetails.h"
#import "PlannedVisitDetails.h"
#import "SWPlatform.h"
#import "ReadSentMessagesViewController.h"
#import "CommunicationPopoverViewController.h"
#import "MedRepDefaults.h"

@interface CommunicationViewControllerNew (){
 
    NSDate *dateForReadMsg;
    NSLocale *usLocaleForMsg;
    NSDateFormatter *formaterForReadMsg;
    UILabel *lblPlaceHolderForReply;
}

@end

@implementation CommunicationViewControllerNew

@synthesize customer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(pushNotificationArrived)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushNotificationArrived) name:@"PUSHNOTIFICATIONARRIVED" object:nil];
    
    
    [self initObj];
    
    [self setupColor];
    
    [self setupFont];
    
    [self setupRoundCorner];
    
    [self setupNavigBar];
    
    //[self roundCornersOnView:inboxView onTopLeft:YES topRight:NO bottomLeft:NO bottomRight:NO radius:8.0];
    //[self roundCornersOnView:sentView onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:NO radius:8.0];
    //[self roundCornersOnView:messageTableView onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:8.0];

}

-(void)pushNotificationArrived
{
    NSLog(@"push notification arrived in foreground and delegate called");
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    arrInboxMsg = [[[SWDatabaseManager retrieveManager] dbGetFSRMessages] mutableCopy];
    
    NSMutableArray *pushNotificationsContentArray =  [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray = [self sortMessages:pushNotificationsContentArray];
        
        [arrInboxMsg addObjectsFromArray:pushNotificationsContentArray];
    }
    [messageTableView reloadData];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(NSMutableArray*)sortMessages:(NSMutableArray*)contentArray
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Message_date" ascending:NO];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray = [contentArray sortedArrayUsingDescriptors:descriptors];
    return [sortedArray mutableCopy];
}

-(void)initObj{
    
    dateForReadMsg =[NSDate date];
    formaterForReadMsg =[NSDateFormatter new];
    [formaterForReadMsg setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    usLocaleForMsg = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    arrFilterdInboxMessage = [NSMutableArray array];
    arrFilterdSentMessage = [NSMutableArray array];
    
    CustomerShipDetailsArray =[NSMutableArray array];
    
    self.customer = [NSMutableDictionary dictionary];
    
    isInboxViewSelected = YES;
    sentBottomBarLabel.hidden = YES;
    messageTableView.tableFooterView = [UIView new];
    inboxCountLabel.layer.cornerRadius = 10.0f;
    inboxCountLabel.layer.masksToBounds = YES;
    
    // set placeholder on text view
    
    float tempX = 7.0;
    lblPlaceHolderForReply = [[UILabel alloc] initWithFrame:CGRectMake(tempX, 0.0,replyTextView.frame.size.width - 10.0, 34.0)];
    [lblPlaceHolderForReply setText:@"Type your message"];
    lblPlaceHolderForReply.font = kFontWeblySleekLight(16);
    lblPlaceHolderForReply.textColor = UIColorFromRGB(0x6A6F7B);
    [lblPlaceHolderForReply setBackgroundColor:[UIColor clearColor]];
    [lblPlaceHolderForReply setTextColor:[UIColor lightGrayColor]];
    replyTextView.delegate = self;
    [replyTextView addSubview:lblPlaceHolderForReply];
    
}



-(void)setupNavigBar{
    
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Communication"];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
            
        } else {
        }
    }
    
    composeEmail = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Communication_add.png"] style:UIBarButtonItemStylePlain target:self action:@selector(composeEmail)];
    self.navigationItem.rightBarButtonItem = composeEmail;
}


-(void)setupRoundCorner{
    
    
//    messageTableView.layer.cornerRadius = 8.0;
//    messageTableView.layer.masksToBounds = YES;
//
    noContentView.layer.cornerRadius = 8.0;
    noContentView.layer.masksToBounds = YES;
    
    replyButton.layer.cornerRadius = 8.0;
    replyButton.layer.masksToBounds = YES;
    
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    if(replyTextView.text.length==0) {
        lblPlaceHolderForReply.hidden = NO;
    }
    else {
        lblPlaceHolderForReply.hidden = YES;
    }
    
    /*
    UIBezierPath *maskPath_3 = [UIBezierPath bezierPathWithRoundedRect:messageTableView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer_3 = [[CAShapeLayer alloc] init];
    maskLayer_3.frame = messageTableView.bounds;
    maskLayer_3.path = maskPath_3.CGPath;
    messageTableView.layer.mask = maskLayer_3;
    */
 
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: sendView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    sendView.layer.mask = maskLayer;
    
    
  
    CAShapeLayer * maskLayer_2 = [CAShapeLayer layer];
    maskLayer_2.path = [UIBezierPath bezierPathWithRoundedRect: viewInbox.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    viewInbox.layer.mask = maskLayer_2;
    
    
    CAShapeLayer * maskLayer_3 = [CAShapeLayer layer];
    maskLayer_3.path = [UIBezierPath bezierPathWithRoundedRect: viewSent.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    viewSent.layer.mask = maskLayer_3;
    
    
   /*
    CAShapeLayer * maskLayer_3 = [CAShapeLayer layer];
    maskLayer_3.path = [UIBezierPath bezierPathWithRoundedRect: messageTableView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    messageTableView.layer.mask = maskLayer_3;
    */
}



-(void)setupColor{
    
    self.view.backgroundColor = [UIColor whiteColor];
    mailTextView.backgroundColor = [UIColor whiteColor];
    
    //set border of search bar text filed as per UI/Design
    for (id object in [[[searchBarMsg subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            
            //textFieldObject.frame = CGRectMake(13, 5, 50, 30);
            textFieldObject.layer.borderColor =  UIColorFromRGB(0xE3E4E6).CGColor;
            textFieldObject.layer.borderWidth = 1.0;
            textFieldObject.layer.cornerRadius = 4.5;
            break;
        }
    }
    
    searchBarMsg.layer.borderWidth = 1.0;
    searchBarMsg.layer.borderColor = [UIColor colorWithRed:235.0/255 green:251.0/255 blue:249.0/255 alpha:1.0].CGColor;
    
}

-(void)setupFont{
    
    [btnInbox.titleLabel setFont:kFontWeblySleekSemiLight(16)];
    [btnInbox setTitleColor: UIColorFromRGB(0x2C394A) forState:UIControlStateNormal];
    
    [btnSent.titleLabel setFont:kFontWeblySleekSemiLight(16)];
    [btnSent setTitleColor: UIColorFromRGB(0x6A6F7B) forState:UIControlStateNormal];
    
    lblReplySatic.font = kFontWeblySleekSemiLight(16);
    lblReplySatic.textColor = UIColorFromRGB(0x2C394A);
    
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}



- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [searchBarMsg layoutSubviews];
    
    //handle case: Sometimes search bar's cancel button overcomes on search bar's text filed
    if (searchBarMsg.showsCancelButton){
        return;
    }
    
    
    for(UIView *view in searchBarMsg.subviews)
    {
        for(UITextField *textfield in view.subviews)
        {
            if ([textfield isKindOfClass:[UITextField class]]) {
                
                /*
                float searchBarWidth = searchBarMsg.frame.size.width-26;
                if (searchBarMsg.showsCancelButton){
                    searchBarWidth = searchBarMsg.frame.size.width-65;
                }
                */
                
                textfield.frame = CGRectMake(13.0f, 13.0f, viewSearchBar.frame.size.width-26, 30.0f);
            }
        }
    }
}




- (void)composeEmail
{
    CommunicationPopoverViewController * popOverVC=[[CommunicationPopoverViewController alloc]init];
    popOverVC.popOverController=popOverForNewMessage;
    popOverVC.Delegate = self;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    nav.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popover = nav.popoverPresentationController;
    popover.backgroundColor=[UIColor whiteColor];
    popOverVC.preferredContentSize = CGSizeMake(350, 380);
    popover.delegate = self;
    popover.barButtonItem = self.navigationItem.rightBarButtonItem;
    popover.permittedArrowDirections = UIPopoverArrowDirectionAny;
    [self presentViewController:nav animated:YES completion:^{

    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    //Get FSR Messages
    [Flurry logEvent:@"Cummunication  View"];
    arrInboxMsg = [[SWDatabaseManager retrieveManager] dbGetFSRMessages];
    NSMutableArray *pushNotificationsContentArray =  [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray = [self sortMessages:pushNotificationsContentArray];
        
        [arrInboxMsg addObjectsFromArray:pushNotificationsContentArray];
    }
    
    
    arrSentMsg = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetFSRSentMessages]];
    [messageTableView reloadData];

    
    int totalUnreadMessage = 0;
    for (int i=0;i<[arrInboxMsg count]; i++){
        
        FSRMessagedetails *model = [arrInboxMsg objectAtIndex:i];
        
        
        if ([model.Message_Read isEqualToString:@"N"]) {
            totalUnreadMessage += 1;
        }
    }
    
    if (totalUnreadMessage == 0) {
        inboxCountLabel.hidden = YES;
    }else{
        inboxCountLabel.hidden = NO;
        inboxCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)totalUnreadMessage];
        
    }
}

#pragma mark
#pragma mark Button Action Method

- (IBAction)btnMsgReplyTapped:(id)sender {
   
    
    replyTextView.text = [replyTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([replyTextView.text length]==0){
        [self UserAlert:@"Error: Please add reply"];
        return;
    }
    
    
    if  (selectedMessage.Message_ID <= 0 || selectedMessage.SalesRep_ID  <= 0 ) {
         [self UserAlert:@"Unable to process now, please try again later"];
         return;
     }
    
   
   
     [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotificationForAddResponse:)
                                                 name:@"AddResponseNotification"
                                               object:nil];
    
    //Update MessageReply and datetime
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Reply ='%@' ,Reply_Date=\"%@\" ,Emp_Code='%@' where Message_ID =%d and salesRep_ID=%d",replyTextView.text,dateString,[[SWDefaults userProfile] stringForKey:@"Emp_Code"],selectedMessage.Message_ID,selectedMessage.SalesRep_ID ];
    [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
    formater=nil;
    usLocale=nil;
}



-(void)btnBackPressed
{
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        [revealController revealToggle:self];
}

- (IBAction)inboxButtonTapped:(id)sender {
    
    // handle case when user switch from sent box to inbox
    if (isInboxViewSelected==NO){
        [self handleSearhBarAndDataFlow];
    }
    
    [self highLightInboxMsg];
    arrInboxMsg = [[SWDatabaseManager retrieveManager] dbGetFSRMessages];
    NSMutableArray *pushNotificationsContentArray =  [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
    if (pushNotificationsContentArray.count>0) {
        
        pushNotificationsContentArray = [self sortMessages:pushNotificationsContentArray];
        
        [arrInboxMsg addObjectsFromArray:pushNotificationsContentArray];
    }
    
    
    isInboxViewSelected = YES;
    if (arrInboxMsg.count > 0) {
        noContentView.hidden = YES;
        [messageTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:messageTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }else{
        noContentView.hidden = NO;

    }
    sentBottomBarLabel.hidden = YES;
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations: ^{
        sentBottomBarLabel.frame = CGRectMake(0, 48, sentBottomBarLabel.frame.size.width, sentBottomBarLabel.frame.size.height);
    } completion:nil];
    inboxBottomBarLabel.hidden = NO;
    [messageTableView reloadData];

}
- (IBAction)sendButtonTapped:(id)sender {
    
    // handle case when user switch from inbox to send box
    if (isInboxViewSelected){
       [self handleSearhBarAndDataFlow];
    }
    
    [self highLightSentMsg];
    
    arrSentMsg  = [[NSMutableArray alloc]init];
    
    arrSentMsg = [NSMutableArray arrayWithArray:[[SWDatabaseManager retrieveManager] dbGetFSRSentMessages]];

    isInboxViewSelected = NO;
    inboxBottomBarLabel.hidden = YES;
    sentBottomBarLabel.hidden = NO;
    if (arrSentMsg.count > 0) {
        noContentView.hidden = YES;
        [messageTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:messageTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }else{
        noContentView.hidden = NO;
        
    }
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations: ^{
        inboxBottomBarLabel.frame = CGRectMake(0, 48, inboxBottomBarLabel.frame.size.width, sentBottomBarLabel.frame.size.height);
    } completion:nil];
    
    
    [messageTableView reloadData];
}

-(void)highLightInboxMsg{
 
    [btnInbox setTitleColor: UIColorFromRGB(0x2C394A) forState:UIControlStateNormal];
    [btnSent setTitleColor: UIColorFromRGB(0x6A6F7B) forState:UIControlStateNormal];
    
    /*
    [btnInbox setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(58.0/255.0) blue:(93.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    [btnSent setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    */
     
    /*
    [btnInbox.titleLabel setFont:kFontWeblySleekSemiLight(16)];
    [btnInbox setTitleColor:[UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    
    [btnSent.titleLabel setFont:kFontWeblySleekSemiLight(14)];
    [btnSent setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
     */
    
}
-(void)highLightSentMsg{
    
    [btnSent setTitleColor: UIColorFromRGB(0x2C394A) forState:UIControlStateNormal];
    [btnInbox setTitleColor: UIColorFromRGB(0x6A6F7B) forState:UIControlStateNormal];
    
    /*
    [btnSent setTitleColor:[UIColor colorWithRed:(2.0/255.0) green:(58.0/255.0) blue:(93.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    [btnInbox setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    */
    /*
    [btnSent.titleLabel setFont:kFontWeblySleekSemiLight(16)];
    [btnSent setTitleColor:[UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1.0]forState:UIControlStateNormal];
    
    [btnInbox.titleLabel setFont:kFontWeblySleekSemiLight(14)];
    [btnInbox setTitleColor:[UIColor colorWithRed:(106.0/255.0) green:(111.0/255.0) blue:(123.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    */
}

-(void)handleSearhBarAndDataFlow{
    
    [self.view endEditing:YES];
    isSearchingON = NO;
    searchBarMsg.text = @"";
    [searchBarMsg setShowsCancelButton:NO animated:YES];
 }

#pragma mark
#pragma mark TEXT VIEW DELEGATE METHODS

- (void)textViewDidEndEditing:(UITextView *) textView {
    if (![textView hasText]) {
        lblPlaceHolderForReply.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    if(![textView hasText]) {
        lblPlaceHolderForReply.hidden = NO;
    }
    else {
        lblPlaceHolderForReply.hidden = YES;
    }
}

#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"AddResponseNotification"]){
       
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AddResponseNotification" object:nil];
        [self UserAlert:@"Message sent successfully"];
       
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    _colorPicker=nil;
    datePickerPopover=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
#pragma mark -
#pragma mark Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isInboxViewSelected == YES)
    {
        if(isSearchingON)
            return [arrFilterdInboxMessage count];
        else
            return [arrInboxMsg count];
    }
    else
    {
        if(isSearchingON)
            return [arrFilterdSentMessage count];
        else
            return [arrSentMsg count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if(isInboxViewSelected == YES)
    {
        static NSString* identifier=@"messageTableViewCell";
        MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MessageTableViewCell" owner:nil options:nil] firstObject];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        FSRMessagedetails *info;
        if(isSearchingON){
            
            if (arrFilterdInboxMessage.count <= indexPath.row){
                return cell;
            }
            
            info = [arrFilterdInboxMessage objectAtIndex:indexPath.row];
        }
        else {
           
            if (arrInboxMsg.count <= indexPath.row){
                return cell;
            }
            
            info =[arrInboxMsg objectAtIndex:indexPath.row];
        }
        
        cell.titleLabel.font=kFontWeblySleekSemiBold(16.0);
        cell.contentLabel.font=kFontWeblySleekSemiBold(16.0);
        
        cell.contentLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[info valueForKey:@"Message_Content"]]];
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[info valueForKey:@"Message_Title"]]];
        
        NSString *msgReadStr = [info valueForKey:@"Message_Read"];
        
        if ([msgReadStr isEqualToString:KAppControlsYESCode]){
           
           cell.indicatorView.backgroundColor = [UIColor clearColor];
            
        }else {
           cell.indicatorView.backgroundColor = [UIColor colorWithRed:71.0/255 green:144.0/255  blue:210.0/255  alpha:1.0];
        }
       
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        
        if (selectedIndexPath == indexPath) {
           // cell.indicatorView.backgroundColor = [UIColor colorWithRed:71.0/255 green:144.0/255  blue:210.0/255  alpha:1.0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
        } else {
            //cell.indicatorView.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
       
        
        return cell;
    }
    else
    {
       
        
        static NSString* identifier=@"messageTableViewCell";
        MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"MessageTableViewCell" owner:nil options:nil] firstObject];
        }
        
        
        
        FSRMessagedetails *info;
        if(isSearchingON)
        {
            
            if (arrFilterdSentMessage.count <= indexPath.row){
                return cell;
            }
            info = [arrFilterdSentMessage objectAtIndex:indexPath.row];
        }
        else
        {
            
            if (arrSentMsg.count <= indexPath.row){
                return cell;
            }
            info = [arrSentMsg objectAtIndex:indexPath.row];
        }
        cell.titleLabel.font=kFontWeblySleekSemiBold(16.0);
        cell.contentLabel.font=kFontWeblySleekSemiBold(16.0);
        
        cell.contentLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[info valueForKey:@"Message_Content"]]];
        cell.titleLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[info valueForKey:@"Message_Title"]]];
        
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        if (selectedIndexPath == indexPath) {
           // cell.indicatorView.backgroundColor = [UIColor colorWithRed:71.0/255 green:144.0/255  blue:210.0/255  alpha:1.0];
            cell.contentView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
        } else {
           // cell.indicatorView.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
        }
        
        cell.indicatorView.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //MessageTableViewCell *cell=(MessageTableViewCell*)[messageTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    
   
    
    selectedIndexPath = indexPath;
    
    if(isInboxViewSelected == YES){
       
        noContentView.hidden = YES;
        
        if (isSearchingON){
            
            if (arrFilterdInboxMessage.count <= indexPath.row){
                return ;
            }
            
            selectedMessage = arrFilterdInboxMessage[indexPath.row];
        }else {
            
            if (arrInboxMsg.count <= indexPath.row){
                return ;
            }
            
            selectedMessage = arrInboxMsg[indexPath.row];
        }
        
        
        
        NSMutableArray *messArray = [[[SWDatabaseManager retrieveManager] fetchDataForQuery: [NSString stringWithFormat:@"Select A.*,B.Username  FROM TBL_Incoming_Messages AS A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID where A.Message_Date = '%@'",selectedMessage.Message_date]]mutableCopy];  //[selectedMessage valueForKey:@"Message_date"]]] mutableCopy];
        
        
        NSString *recpString = @"";
        for (int i=0;i<[messArray count]; i++)
        {
            /*
            recpString = [recpString stringByAppendingString:[[messArray objectAtIndex:i] stringForKey:@"Username"]];
            recpString = [recpString stringByAppendingString:@" , "];
            */
            
            if (recpString.length==0){
                recpString = [[messArray objectAtIndex:i] stringForKey:@"Username"];
            }else {
                recpString = [recpString stringByAppendingString:[NSString stringWithFormat:@", %@",[[messArray objectAtIndex:i] stringForKey:@"Username"]]];
            }
            
        }
        
        surveyTitleLabel.text = selectedMessage.Message_Title; //[selectedMessage valueForKey:@"Message_Title"];
        fromTitleLabel.text = @"From: ";
        
        if (recpString != nil && recpString.length>0){
            fromLabel.text = recpString;
        }else {
         
            fromLabel.text = @"N/A";
        }
        
        if (selectedMessage.Message_date != nil && selectedMessage.Message_date.length >0){
            dateLabel.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd MMM, yyyy hh:mm a" scrString:selectedMessage.Message_date];
        }
        
        // dateLabel.text = selectedMessage.Message_date; //[selectedMessage valueForKey:@"Message_date"];
       
        mailTextView.text = selectedMessage.Message_Content; //[selectedMessage valueForKey:@"Message_Content"];
        
        if(selectedMessage.Message_Reply != nil && selectedMessage.Message_Reply.length >0){
             replyTextView.text = selectedMessage.Message_Reply;
        }else {
             replyTextView.text = @"";
        }
        
        
        replyTextView.userInteractionEnabled = YES;
        xReplyViewTopConstraint.constant = 8.0f;
        xreplyViewHeightConstraint.constant = 308.0f;
        replyButton.hidden = NO;
        replyView.hidden = NO;
        
        
        //Update Read status and datetime
        
        [formaterForReadMsg setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [formaterForReadMsg setLocale:usLocaleForMsg];
        NSString *dateString = [formaterForReadMsg stringFromDate:dateForReadMsg];
        
        NSString *strQuery = [NSString stringWithFormat:@"update TBL_FSR_Messages Set Message_Read = 'Y' , Message_Date ='%@' where Message_ID =%d and salesRep_ID=%d",dateString,selectedMessage.Message_ID,selectedMessage.SalesRep_ID ];
        [[SWDatabaseManager retrieveManager] updateReadMessagestatus:strQuery];
        
        arrInboxMsg  = [[NSMutableArray alloc]init];
        arrInboxMsg = [[SWDatabaseManager retrieveManager] dbGetFSRMessages];
        
        //add pushnotification content
        NSMutableArray *pushNotificationsContentArray = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"PUSHED_MESSAGES"]];
        if (pushNotificationsContentArray.count>0) {
            
            //indexpath might change if we add contents to message array sp keep an instance of current indexpath and update that object after adding push messages
            pushNotificationsContentArray = [self sortMessages:pushNotificationsContentArray];

            
            [arrInboxMsg addObjectsFromArray:pushNotificationsContentArray];
            
            selectedMessage = [arrInboxMsg objectAtIndex:indexPath.row];
            selectedMessage.Message_Read = @"Y";
            [arrInboxMsg replaceObjectAtIndex:indexPath.row withObject:selectedMessage];
            
            //update the flag in push messages
        
            NSPredicate * pushNotificationMessagesPredicate=[NSPredicate predicateWithFormat:@"SELF.isFromPushNotifications == 1"];
            NSMutableArray* pushMessageContentArray=[[arrInboxMsg filteredArrayUsingPredicate:pushNotificationMessagesPredicate] mutableCopy];
            
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pushMessageContentArray];
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"PUSHED_MESSAGES"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        // Refresh count again
        int totalUnreadMessage = 0;
        for (int i=0;i<[arrInboxMsg count]; i++){
            
            FSRMessagedetails *model  = [arrInboxMsg objectAtIndex:i];
            if ([model.Message_Read isEqualToString:@"N"]) {
                totalUnreadMessage += 1;
            }
        }
        
        if (totalUnreadMessage == 0) {
            inboxCountLabel.hidden = YES;
        }else{
            inboxCountLabel.hidden = NO;
            inboxCountLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)totalUnreadMessage];
            
        }
        
        if (replyTextView.text.length==0){
            lblPlaceHolderForReply.hidden = NO;
        }else {
            lblPlaceHolderForReply.hidden = YES;
        }
        
        
    }

    else{
        
        if (isSearchingON){
            
            if (arrFilterdSentMessage.count <= indexPath.row){
                return ;
            }
            
            selectedMessage = arrFilterdSentMessage[indexPath.row];
        }else {
            
            if (arrSentMsg.count <= indexPath.row){
                return ;
            }
            
            selectedMessage = arrSentMsg[indexPath.row];
        }
        
        
        NSMutableArray *messArray = [[[SWDatabaseManager retrieveManager] fetchDataForQuery: [NSString stringWithFormat:@"Select A.*,B.Username  FROM TBL_Incoming_Messages AS A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID where A.Message_Date = '%@'",selectedMessage.Message_date]]mutableCopy] ; //[selectedMessage valueForKey:@"Message_date"]]] mutableCopy];
        
        
        NSString *recpString = @"";
        for (int i=0;i<[messArray count]; i++)
        {
            /*
            recpString = [recpString stringByAppendingString:[[messArray objectAtIndex:i] stringForKey:@"Username"]];
            recpString = [recpString stringByAppendingString:@" , "];
            */
            
            if (recpString.length==0){
                 recpString = [[messArray objectAtIndex:i] stringForKey:@"Username"];
            }else {
                 recpString = [recpString stringByAppendingString:[NSString stringWithFormat:@", %@",[[messArray objectAtIndex:i] stringForKey:@"Username"]]];
            }
            
        }
        
        surveyTitleLabel.text = selectedMessage.Message_Title; //[selectedMessage valueForKey:@"Message_Title"];
        fromTitleLabel.text = @"From: ";
        
        if (recpString != nil && recpString.length>0){
            fromLabel.text = recpString;
        }else {
            
            fromLabel.text = @"N/A";
        }
        
        if (selectedMessage.Message_date != nil && selectedMessage.Message_date.length >0){
            dateLabel.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd MMM, yyyy hh:mm a" scrString:selectedMessage.Message_date];
        }
       
       // dateLabel.text = selectedMessage.Message_date; //[selectedMessage valueForKey:@"Message_date"];
        
        mailTextView.text = selectedMessage.Message_Content; //[selectedMessage valueForKey:@"Message_Content"];
        
        NSString *replyMessage = selectedMessage.Message_Content; //[selectedMessage valueForKey:@"Message_Content"];
        if (replyMessage.length > 0) {
            replyTextView.text = replyMessage;
            xreplyViewHeightConstraint.constant = 308.0f;
            xReplyViewTopConstraint.constant = 8.0f;
            replyTextView.userInteractionEnabled = NO;
            replyView.hidden = NO;
        }else{
            replyTextView.text = @"";
            xreplyViewHeightConstraint.constant = 0.0;
            xReplyViewTopConstraint.constant = 0.0f;
            replyView.hidden = YES;
        }
        
        if (replyTextView.text.length==0){
            lblPlaceHolderForReply.hidden = NO;
        }else {
            lblPlaceHolderForReply.hidden = YES;
        }
        
        replyButton.hidden = YES;
    }
    [messageTableView reloadData];
}

//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{

    [SWDefaults setCustomer:self.customer];
    
    self.customer = [SWDefaults validateAvailableBalance:orderAmmount];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isCashCustomer = @"N";
    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
    [visitManager startVisitWithCustomer:self.customer parent:self andChecker:@"R"];
    orderAmmount=nil;
}


#pragma mark -SearchBar Delegate Methods

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    isSearchingON = YES;
    //searchBar.showsScopeBar = YES;
    //[searchBar sizeToFit];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchBarMsg setShowsCancelButton:YES animated:YES]; //
    if([searchText length] == 0){
        isSearchingON = NO;
        [messageTableView reloadData];
        [messageTableView setScrollsToTop:YES];
    }else {
        isSearchingON = YES;
        [self  searchContent];
    }
    
    
}


- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar{
    
    NSLog(@"\n\n *** Search Button Clicked **\n\n");
    [self.view endEditing:YES];
    isSearchingON = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    NSLog(@"\n\n *** Search Bar Cancel Button Clicked **\n\n");
    
    [searchBarMsg setShowsCancelButton:NO animated:YES];//
    
    [searchBarMsg setText:@""];
    arrFilterdInboxMessage=[[NSMutableArray alloc]init];
    isSearchingON=NO;
    if ([searchBarMsg isFirstResponder]) {
        [searchBarMsg resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (arrInboxMsg.count>0) {
        [messageTableView reloadData];
    }
    
}


-(void)searchContent{
    
    NSString *searchString = searchBarMsg.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicateFirst = [NSPredicate predicateWithFormat:filter, @"Message_Title", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateFirst]];
    
    
    NSArray *tempArrayResult =[[NSArray alloc]init];
    if (isInboxViewSelected){
        tempArrayResult = [[arrInboxMsg filteredArrayUsingPredicate:predicate] mutableCopy];
        arrFilterdInboxMessage= [tempArrayResult mutableCopy];
       
    }else {
        tempArrayResult = [[arrSentMsg filteredArrayUsingPredicate:predicate] mutableCopy];
        arrFilterdSentMessage= [tempArrayResult mutableCopy];
    }
    
    NSLog(@"\n\n *** Total Search Results: %lu  ***\n\n",(unsigned long)tempArrayResult.count);
    if (tempArrayResult.count>0) {
        [messageTableView reloadData];
        [messageTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                      animated:YES
                                scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [messageTableView.delegate tableView:messageTableView didSelectRowAtIndexPath:firstIndexPath];
        
    }else if (tempArrayResult.count==0){
        isSearchingON=YES;
        [messageTableView reloadData];
    }
}


/*
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar1 {
    searchBar1.showsScopeBar = NO;
    [searchBar1 sizeToFit];
    [searchBar1 setShowsCancelButton:NO animated:YES];
    
    return YES;
}*/

#pragma mark
#pragma mark TableviewCell Button mehtod

-(void)btnDetailVisitPressed:(id)sender
{
    // add the data to go  Route page using upcoming visits table
}
-(void)btnSelectionPressed:(id)sender
{
   // add the data if require check box pressed event
}
#pragma mark
#pragma mark Alertview method
-(void)UserAlert:(NSString *)Message
{
    if (Message.length!=0) {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:Message withController:self];
    }
}

#pragma mark
-(void)updateValues{
    
    if (isInboxViewSelected==NO){
        [self sendButtonTapped:self];
    }
}

-(NSMutableArray*)fetchReplyMessages
{
    NSString * outStandingQry=[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"SELECT * FROM TBL_FSR_Messages where Message_Reply is not Null"]];
    
    NSLog(@"total outstanding qry %@", outStandingQry);
    
    NSMutableArray *temp=  [[[SWDatabaseManager retrieveManager] fetchDataForQuery:outStandingQry] mutableCopy];
    
    if (temp.count>0) {
        
        return temp;
    }
    else
    {
        return nil;
    }
}

#pragma mark
#pragma mark orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
#pragma mark
- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
