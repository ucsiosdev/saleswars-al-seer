//
//  ComposeMessageViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import "AllUserList.h"
#import "ComposeMessageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppConstantsList.h"
#import "SWPlatform.h"
@interface ComposeMessageViewController ()

@end

@implementation ComposeMessageViewController
@synthesize recipientPicker;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title =NSLocalizedString(@"New Message", nil);
    //Set back button
    self.navigationItem.hidesBackButton = NO;
        self.view.backgroundColor = [UIColor whiteColor];

    popoverFrame=CGRectMake(236, 110, 700, 34);
    
    //
    alertType=nil;
    alertType =[[NSString alloc] init];

    //Get Incoming Mesage Count
    MessageID =[[SWDatabaseManager retrieveManager] dbGetIncomingMessageCount];
    MessageID ++;
    txtViewMessage.delegate=self;
    txtTitle.delegate=self;
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        mainImage.image=[UIImage imageNamed:@"msg-box-AR.png" cache:NO];
    }
    else
    {
        mainImage.image=[UIImage imageNamed:@"msg-box.png" cache:NO];
    }
       // Do any additional setup after loading the view from its nib.
}
#pragma mark
#pragma mark Button action method
-(void)btnBackPressed
{
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
-(IBAction)btnSubmitPressed:(id)sender
{
    
    if([txtToField.text length]==0 ||[txtTitle.text length]==0 || [txtViewMessage.text length]==0)
    {
        alertType = @"InfoFailure";
        [self UserAlert:@"Error:Please fill up all the details"];
        return;
    }
    
    for (int i=0;i<[recipientIDArray count]; i++)
    {
        int RcptUserID =[[recipientIDArray objectAtIndex:i] integerValue];
        NSString *strQuery = [NSString stringWithFormat:@"insert into TBL_Incoming_Messages(Message_ID,SalesRep_ID,Emp_Code,Rcpt_User_ID,Message_title,Message_Content,Message_date) values('%@',%d,'%@',%d,'%@','%@','%@')" ,[NSString createGuid],[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue],[[SWDefaults userProfile] stringForKey:@"Emp_Code"],RcptUserID,txtTitle.text,txtViewMessage.text,lblDate.text];

        [[SWDatabaseManager retrieveManager] InsertdataCustomerResponse:strQuery];
    }
    
    alertType=@"Success";

    [self UserAlert:@"Message sent successfully"];
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];

}
-(IBAction)btnToFieldPressed:(id)sender
{
    txtToField.text=@"";
    if (self.recipientPicker == nil) {
        self.recipientPicker = [[RecipientPopoverViewController alloc] init];
        
        self.recipientPicker.delegate = self;
        self.colorPickerPopover=nil;
        self.colorPickerPopover = [[UIPopoverController alloc] initWithContentViewController:self.recipientPicker];
        self.colorPickerPopover.delegate=self;
    }
  
    [self.colorPickerPopover presentPopoverFromRect:[sender frame] inView:composeView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
-(void)PopOverDonePressed
{
    [self.colorPickerPopover dismissPopoverAnimated:YES];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.recipientPicker = nil;
    self.colorPickerPopover = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
#pragma mark -
#pragma mark Recipient PopOverViewDelegate
- (void)RecipientSelected:(NSString *)Recipient RecipientId:(NSArray*)RecipientId
{
    if ( [Recipient length] > 0)
        Recipient = [Recipient substringToIndex:[Recipient length] - 1];
   
    txtToField.text=Recipient;
    recipientIDArray =[RecipientId copy];

    //Set the curent date time for compose message
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    lblDate.text=dateString;
    formater=nil;
    usLocale=nil;
}

#pragma mark
#pragma mark TextView delegate method

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=255){
        return  NO;
    }
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 100) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"InsertSurveyNotification"])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
        alertType=@"Success";
        [self UserAlert:@"Message sent successfully"];        
    }
}
#pragma mark
#pragma mark Alertview method
-(void)UserAlert:(NSString *)Message
{
    UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:Message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil,nil];
    [ErrorAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertType isEqualToString:@"Success"])
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                   [self.navigationController  popViewControllerAnimated:YES];
}
#pragma mark
#pragma mark Orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

 
@end
