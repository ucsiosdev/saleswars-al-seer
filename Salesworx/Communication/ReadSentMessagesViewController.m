//
//  ReadSentMessagesViewController.m
//  SWPlatform
//
//  Created by msaad on 3/3/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "ReadSentMessagesViewController.h"
#import "SWPlatform.h"
@interface ReadSentMessagesViewController ()

@end

@implementation ReadSentMessagesViewController

@synthesize messageDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.messageDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSMutableArray *messArray = [[[SWDatabaseManager retrieveManager] fetchDataForQuery: [NSString stringWithFormat:@"Select A.*,B.Username  FROM TBL_Incoming_Messages AS A INNER JOIN TBL_Users_All  AS B ON A.Rcpt_User_ID = B.User_ID where A.Message_Date = '%@'",[self.messageDict valueForKey:@"Message_date"]]] mutableCopy];

    
    NSString *recpString = @"";
    for (int i=0;i<[messArray count]; i++)
    {
        recpString = [recpString stringByAppendingString:[[messArray objectAtIndex:i] stringForKey:@"Username"]];
        recpString = [recpString stringByAppendingString:@" , "];
    }
    

    lblTo.text = recpString;
    lblTitle.text = [self.messageDict valueForKey:@"Message_Title"];
    txtMessages.text = [self.messageDict valueForKey:@"Message_Content"];
    lblDate.text = [self.messageDict valueForKey:@"Message_date"];
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        mainImage.image=[UIImage imageNamed:@"sentMsg_details-AR.png" cache:NO];
    }
    else
    {
        mainImage.image=[UIImage imageNamed:@"sentMsg_details.png" cache:NO];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end
