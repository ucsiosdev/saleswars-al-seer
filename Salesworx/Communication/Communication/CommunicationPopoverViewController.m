//
//  CommunicationPopoverViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/21/16.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import "CommunicationPopoverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "SWPlatform.h"

@interface CommunicationPopoverViewController ()

@end

@implementation CommunicationPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedIndexes = [[NSArray alloc]init];
    
    [self setupColorAndFont];
    recipientDetailsArray=[[NSMutableArray alloc]init];
    self.view.backgroundColor = [UIColor whiteColor];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"New Message"];
    self.navigationController.navigationBar.topItem.title = @"";
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=closeButton;
    
    // to show navigation bar title in the middle
    UIBarButtonItem* btnEmpty=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnEmptyTapped)];
    [btnEmpty setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    btnEmpty.tintColor = [UIColor clearColor];
    btnEmpty.enabled = NO;
    self.navigationItem.rightBarButtonItem=btnEmpty;

    _lblDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDateFormatWithTime scrString:[self getCurrentDate]];
    alertType=nil;
    alertType =[[NSString alloc] init];
    
    //Get Incoming Mesage Count
    MessageID =[[SWDatabaseManager retrieveManager] dbGetIncomingMessageCount];
    MessageID ++;
    
    [self.txtMessage setTextContainerInset:UIEdgeInsetsMake(0, 3, 0, 0)];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)setupColorAndFont{
    
    lblToStatic.font = kSWX_FONT_SEMI_BOLD(14);
    lblToStatic.textColor = UIColorFromRGB(0x2C394A);
    
    
    lblTitleStatic.font = kSWX_FONT_SEMI_BOLD(14);
    lblTitleStatic.textColor = UIColorFromRGB(0x2C394A);
    
    lblMessageStatic.font = kSWX_FONT_SEMI_BOLD(14);
    lblMessageStatic.textColor = UIColorFromRGB(0x2C394A);
    
    
    _txtTo.font = kSWX_FONT_SEMI_BOLD(15);
    _txtTo.textColor = UIColorFromRGB(0x2C394A);
    
    _txtTitle.font = kSWX_FONT_SEMI_BOLD(15);
    _txtTitle.textColor = UIColorFromRGB(0x2C394A);
    
    
    _txtMessage.font = kSWX_FONT_SEMI_BOLD(15);
    _txtMessage.textColor = UIColorFromRGB(0x2C394A);
    
    
    _txtTo.backgroundColor = [UIColor whiteColor];
    _txtTitle.backgroundColor = [UIColor whiteColor];
    _txtMessage.backgroundColor = [UIColor whiteColor];
    
    
    [self setShadowOnUILayer:_txtTo.layer];
    [self setShadowOnUILayer:_txtTitle.layer];
    [self setShadowOnUILayer:_txtMessage.layer];
    
   
}


-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}


-(void)btnEmptyTapped{
    NSLog(@"\n\n *** btnEmptyTapped **\n\n");
}

-(void)closeTapped
{
    if ([_txtTo.text length]==0 && [_txtTitle.text length]==0 && [_txtMessage.text length]==0)
    {
        //[popOverForNewMessage dismissPopoverAnimated:YES];
        
        [self dismissViewControllerAnimated:YES completion:^{
            if ([self.Delegate respondsToSelector:@selector(updateValues)]){
                [self.Delegate updateValues];
            }
        }];
        
       
    } else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Message", nil) message:NSLocalizedString(@"Would you like to cancel sending this message?", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Yes", nil),NSLocalizedString(@"No", nil), nil];
        alert.tag = 0;
        [alert show];
    }
}

- (IBAction)btnTo:(id)sender
{
    [_txtTitle resignFirstResponder];
    [_txtMessage resignFirstResponder];
    
    _txtTo.text=@"";
    
    _recipientPicker = [[RecipientPopoverViewController alloc]init];
    _recipientPicker.delegate = self;
    NSArray *arr = [[SWDatabaseManager retrieveManager] dbGetUsersList];
    if (arr.count>0)
    {
        _recipientPicker.selectedIndexes = [selectedIndexes mutableCopy];
        [self.navigationController pushViewController:_recipientPicker animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}
- (IBAction)btnSend:(id)sender
{
    
     _txtTo.text = [_txtTo.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     _txtTitle.text = [_txtTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     _txtMessage.text = [_txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([_txtTo.text length]==0 ||[_txtTitle.text length]==0 || [_txtMessage.text length]==0)
    {
        alertType = @"InfoFailure";
        [self UserAlert:NSLocalizedString(@"Please enter all details", nil)];
        return;
    }
    
    for (int i=0;i<[recipientIDArray count]; i++)
    {
        int RcptUserID =[[recipientIDArray objectAtIndex:i] integerValue];
        
        NSString *DBDate = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:kDatabseDefaultDateFormat scrString:[self getCurrentDate]];
        
        NSString *strQuery = [NSString stringWithFormat:@"insert into TBL_Incoming_Messages(Message_ID,SalesRep_ID,Emp_Code,Rcpt_User_ID,Message_title,Message_Content,Message_date) values('%@',%d,'%@',%d,'%@','%@','%@')" ,[NSString createGuid],[[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"] intValue],[[SWDefaults userProfile] stringForKey:@"Emp_Code"],RcptUserID,_txtTitle.text,_txtMessage.text,DBDate];
        
        [[SWDatabaseManager retrieveManager] InsertdataCustomerResponse:strQuery];
        
    }
    
    alertType=@"Success";
    
    [self UserAlert:@"Message sent successfully"];
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
    //[popOverForNewMessage dismissPopoverAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
        if ([self.Delegate respondsToSelector:@selector(updateValues)]){
            [self.Delegate updateValues];
        }
    }];
    
    if ([self.Delegate respondsToSelector:@selector(updateValues)])
    {
        [self.Delegate updateValues];
    }
}
-(void)PopOverDonePressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)PopOverBackPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    _recipientPicker = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}
#pragma mark -
#pragma mark Recipient PopOverViewDelegate
- (void)RecipientSelected:(NSString *)Recipient RecipientId:(NSArray*)RecipientId RecipientDetails:(NSMutableArray *)RecipientDetails selectedArrayIndex:(NSMutableArray *)selectedArrayIndex
{
    if ( [Recipient length] > 0)
        Recipient = [Recipient substringToIndex:[Recipient length] - 1];
    
    _txtTo.text=Recipient;
    recipientIDArray =[RecipientId copy];
    recipientDetailsArray=[RecipientDetails mutableCopy];
    selectedIndexes = [selectedArrayIndex mutableCopy];
    
}

#pragma mark
#pragma mark TextView delegate method

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=255){
        return  NO;
    }
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == _txtMessage)
    {
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}
/*
- (void)keyboardWillShow:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    
    if( [self.txtMessage isFirstResponder]){
        self.view.frame = CGRectMake(self.view.frame.origin.x, -120, self.view.frame.size.width, self.view.frame.size.height+250);
    }
    
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.view cache:YES];
    self.view.frame = CGRectMake(0,0,400,500);
    [UIView commitAnimations];
}
*/
#pragma mark TextField delegate method
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 100) ? NO : YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark
#pragma mark Notification method
- (void) receiveNotificationForAddResponse:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"InsertSurveyNotification"])
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"InsertSurveyNotification" object:nil];
        alertType=@"Success";
        [self UserAlert:@"Message sent successfully"];
    }
}
#pragma mark
#pragma mark Alertview method
-(void)UserAlert:(NSString *)Message
{
    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(Message, nil) withController:self];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertType isEqualToString:@"Success"])
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
    
    if (alertView.tag == 0) {
        if (buttonIndex == 0) {
            //[popOverForNewMessage dismissPopoverAnimated:YES];
            
            [self dismissViewControllerAnimated:YES completion:^{
                if ([self.Delegate respondsToSelector:@selector(updateValues)]){
                    [self.Delegate updateValues];
                }
            }];
        }
    }
}
#pragma mark
#pragma mark Orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

#pragma mark
-(NSString *)getCurrentDate
{
    //Update MessageReply and datetime
    NSDate *date =[NSDate date];
    NSDateFormatter *formater =[NSDateFormatter new];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formater setLocale:usLocale];
    NSString *dateString = [formater stringFromDate:date];
    
    formater=nil;
    usLocale=nil;
    
    return dateString;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
