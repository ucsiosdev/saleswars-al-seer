//
//  DatePickerViewController.m
//  DataSyncApp
//
//  Created by sapna on 1/29/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "DatePickerViewController.h"
#import "SWPlatform.h"
@interface DatePickerViewController ()

@end

@implementation DatePickerViewController
@synthesize dateArray;
@synthesize delegate ;
@synthesize strdate;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Add require views
    [self setPreferredContentSize:CGSizeMake(300.0, 250.0)];
    [self setTitle:NSLocalizedString(@"Select Date", nil)];
    
    datePicker=nil;
    datePicker =[[UIDatePicker alloc] initWithFrame:CGRectMake(0,0 ,300 ,200 )];
    if (@available(iOS 13.4, *)) {
        datePicker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
    [datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    datePicker.backgroundColor=[UIColor clearColor];
    datePicker.datePickerMode=UIDatePickerModeDate;
    [datePicker setMinimumDate:[NSDate date]];
    [self.view addSubview:datePicker];
    
    
    UIButton *btnSelect =[UIButton buttonWithType:UIButtonTypeCustom];
    [btnSelect addTarget:self action:@selector(btnSelectPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btnSelect setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
    [btnSelect setTitle:NSLocalizedString(@"Select", nil) forState:UIControlStateNormal];
    btnSelect.titleLabel.textColor=[UIColor blackColor];
    btnSelect.frame=CGRectMake(5, 175,290 ,41 );
    [self.view addSubview:btnSelect];
    
    dateArray = [NSMutableArray array];
}
#pragma mark
#pragma mark Picker Value changed method
- (void)pickerChanged:(id)sender
{
    NSDate *dateNow  = [sender date];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setLocale:usLocale];
    self.strdate = [dateFormat stringFromDate:dateNow];
    selecteddate=[dateFormat dateFromString:self.strdate];
    NSTimeInterval dateTime;
    
    dateTime = ([selecteddate timeIntervalSinceDate:dateNow] / 86400);
    if(dateTime < 0){
        datePicker.minimumDate = [NSDate date];
    }
    dateFormat=nil;
    usLocale=nil;
}
#pragma mark
#pragma mark Button action method
-(void)btnSelectPressed:(id)sender
{
    if (self.delegate != nil) {
        
        NSDate *dateNow =[NSDate date];
        
        NSDateFormatter *formater =[NSDateFormatter new];
        [formater setDateFormat:@"yyyy-MM-dd"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [formater setLocale:usLocale];
        NSString *dateString = [formater stringFromDate:dateNow];
        NSDate *currentdate =[formater dateFromString:dateString];
        if(selecteddate ==Nil){
            self.strdate = [formater stringFromDate:currentdate];
            selecteddate =currentdate;
        }
        
        NSTimeInterval dateTime;
        
        dateTime = ([selecteddate timeIntervalSinceDate:currentdate] / 86400);
        if(dateTime < 0) //Check if visit date is a past date, dateTime returns - val
        {
            //[self UserAlert:@"Please select upcoming date"];
        }
        
        else if(dateTime == 0) //There's a chance that this could actually happen
        {
            [self.delegate dateSelected:self.strdate andFuture:@"NO"];
            ////NSLog (@"Same Date & Time");
        }
        
        else
        {   [self.delegate dateSelected:self.strdate andFuture:@"YES"];
            ////NSLog (@"Future Date");
        }
        
        formater=nil;
        usLocale=nil;
    }
    
}
#pragma mark
#pragma mark Orientation method
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}
#pragma mark
#pragma mark AlertView method
-(void)UserAlert:(NSString *)Message
{
    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:Message withController:self];
}

#pragma mark
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
