//
//  RecipientPopoverViewController.h
//  DataSyncApp
//
//  Created by sapna on 2/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import "FTPBaseViewController.h"
#import "AllUserList.h"

@protocol RecipientPickerDelegate
@optional
- (void)RecipientSelected:(NSString *)Recipient RecipientId:(NSArray*)RecipientId RecipientDetails:(NSMutableArray *)RecipientDetails selectedArrayIndex:(NSMutableArray *)selectedArrayIndex;
- (void)PopOverDonePressed;
- (void)PopOverBackPressed;
@end


@interface RecipientPopoverViewController : FTPBaseViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate> {
    NSString *strRecipient;
    NSMutableArray *RecipientIDArray;
    UITableView * tblResponse;
    id<RecipientPickerDelegate> _delegate;
    UISearchBar *userSearchBar;
    //AllUserList *selectedUserDetailsArray;
    
}

@property (nonatomic, strong) NSMutableArray *RecipientArray;
@property (nonatomic, strong) NSMutableArray *filteredRecipientArray;
@property (nonatomic, strong) NSMutableArray *selectedIndexes;
@property (nonatomic,unsafe_unretained) id<RecipientPickerDelegate> delegate;
@end
