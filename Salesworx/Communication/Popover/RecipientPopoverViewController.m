//
//  RecipientPopoverViewController.m
//  DataSyncApp
//
//  Created by sapna on 2/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import "RecipientPopoverViewController.h"
#import "AllUserList.h"
#import "SWFoundation.h"
#import "SWDatabaseManager.h"
@implementation RecipientPopoverViewController{
    BOOL isSearchingON;
}
@synthesize RecipientArray,filteredRecipientArray,selectedIndexes;
@synthesize delegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    isSearchingON = NO;
    
    self.navigationController.navigationBarHidden=false;
    //Add require views
    
   
    RecipientArray=[[NSMutableArray alloc]initWithArray:[[SWDatabaseManager retrieveManager] dbGetUsersList]];
    filteredRecipientArray = RecipientArray;
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:@"Select Recipient"];

    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnDonePressed)];
    [saveButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    
    UIBarButtonItem *btnEmpty = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(btnBackTapped)];
    [btnEmpty setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    btnEmpty.enabled =NO;
    btnEmpty.tintColor = [UIColor clearColor];
    self.navigationItem.leftBarButtonItem = btnEmpty;
    
    tblResponse=[[UITableView alloc] initWithFrame:CGRectMake(0,0,350,480) style:UITableViewStyleGrouped];
    tblResponse.bounces=NO;
    tblResponse.tag=111;
    tblResponse.delegate=self;
    tblResponse.dataSource=self;
    tblResponse.backgroundView = nil;
    tblResponse.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tblResponse];
}

-(void)backButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //[[[SWDefaults alloc]init]AddBackButtonToViewcontroller:self];
    
    UIView *searchbarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 350, 44)];
    userSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 350, 44)];
    userSearchBar.delegate = self;
    [searchbarView addSubview:userSearchBar];
    tblResponse.tableHeaderView = searchbarView;
    
    /*
    if(selectedIndexes)
    {
        [selectedIndexes removeAllObjects];
        selectedIndexes =nil;
    }
    selectedIndexes=[NSMutableArray array];
    */
    
    if(strRecipient){
        strRecipient =nil;
    }
    strRecipient =[[NSString alloc] init];
    if(RecipientIDArray){
        [RecipientIDArray removeAllObjects];
    }
    RecipientIDArray =[NSMutableArray array];
    [tblResponse reloadData];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Override to allow orientations other than the default portrait orientation.
    return YES;
}

#pragma mark -SearchBar Delegate Methods

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    isSearchingON = YES;
    //searchBar.showsScopeBar = YES;
    //[searchBar sizeToFit];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [userSearchBar setShowsCancelButton:YES animated:YES];
    if([searchText length] == 0){
        isSearchingON = NO;
        [tblResponse reloadData];
        [tblResponse setScrollsToTop:YES];
    }else {
        isSearchingON = YES;
        [self  searchContent];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar{
    
    NSLog(@"\n\n *** Search Button Clicked **\n\n");
    [self.view endEditing:YES];
    isSearchingON = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    NSLog(@"\n\n *** Search Bar Cancel Button Clicked **\n\n");
    
    [userSearchBar setShowsCancelButton:NO animated:YES];
    
    [userSearchBar setText:@""];
    filteredRecipientArray=[[NSMutableArray alloc]init];
    isSearchingON=NO;
    if ([userSearchBar isFirstResponder]) {
        [userSearchBar resignFirstResponder];
    }
    
    [self.view endEditing:YES];
    if (RecipientArray.count>0) {
        [tblResponse reloadData];
    }
    
}


-(void)searchContent{
    
    NSString *searchString = userSearchBar.text;
    NSString* filter = @"%K CONTAINS[cd] %@";
    NSPredicate *predicateFirst = [NSPredicate predicateWithFormat:filter, @"Username", searchString];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicateFirst]];
    
    filteredRecipientArray = [[RecipientArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    NSLog(@"\n\n *** Total Search Results: %lu  ***\n\n",(unsigned long)filteredRecipientArray.count);
    
    if (filteredRecipientArray.count>0) {
        [tblResponse reloadData];
        /*
        [tblResponse selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                      animated:YES
                                scrollPosition:UITableViewScrollPositionNone];
        NSIndexPath * firstIndexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [tblResponse.delegate tableView:tblResponse didSelectRowAtIndexPath:firstIndexPath];
         */
        
    }else if (filteredRecipientArray.count==0){
        isSearchingON=YES;
        [tblResponse reloadData];
    }
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(isSearchingON)
        return [filteredRecipientArray count];
    else
        return [RecipientArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
        
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        AllUserList *info;
        
        if(isSearchingON){
            if (filteredRecipientArray.count <= indexPath.row){
                return cell;
            }
            info = [filteredRecipientArray objectAtIndex:indexPath.row];
        }
        else {
            
            if (RecipientArray.count <= indexPath.row){
                return cell;
            }
            
            info =[RecipientArray objectAtIndex:indexPath.row];
        }
        
        
        
        cell.selectionStyle=UITableViewCellEditingStyleNone;
        [cell  setAccessoryType:UITableViewCellAccessoryNone];
        //AllUserList *info = [RecipientArray objectAtIndex:indexPath.row];
        cell.textLabel.text = info.Username;
        cell.textLabel.font = kFontWeblySleekSemiBold(14);
        cell.textLabel.textColor = UIColorFromRGB(0x2C394A);
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User_ID == %d" ,info.User_ID];
        NSArray *filteredArray = [[selectedIndexes filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filteredArray.count>0){
             [cell  setAccessoryType:UITableViewCellAccessoryCheckmark];
        }else {
            [cell  setAccessoryType:UITableViewCellAccessoryNone];
        }
        
        // Remove seperator inset
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        // Explictly set your cell's layout margins
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        return cell;
    }
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate != nil) {
        
        AllUserList *info;
        
        if(isSearchingON){
            info = [filteredRecipientArray objectAtIndex:indexPath.row];
        }
        else {
            info =[RecipientArray objectAtIndex:indexPath.row];
        }
        
       
        /*
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User_ID == %d" ,info.User_ID];
        NSMutableArray *filteredArray = [[selectedIndexes filteredArrayUsingPredicate:predicate] mutableCopy];
        if (filteredArray.count>0){
            
            
            NSInteger anIndex=[selectedIndexes indexOfObject:info];
            if(NSNotFound == anIndex) {
                NSLog(@"\n\n *** WARNING: Index not found **\n\n");
            }else {
                NSLog(@"\n\n *** Index found: %ld **\n\n",(long)anIndex);
                [selectedIndexes removeObjectAtIndex:anIndex];
            }
        }else {
            
            NSLog(@"\n\n *** Model added of index:%ld **\n\n",indexPath.row);
            [selectedIndexes addObject:info];
        }
        */
        
        NSInteger objIndex = -1;
        
        for (int row=0; row<selectedIndexes.count; row++) {
            AllUserList *model = [selectedIndexes objectAtIndex:row];
            if (model.User_ID == info.User_ID){
                objIndex = row;
                break;
            }
        }
        
        // object not added, so add now
        if (objIndex<0){
            NSLog(@"\n\n *** Model added of index:%ld **\n\n",indexPath.row);
            [selectedIndexes addObject:info];
        }
        
        // object already added so remove this
        else {
              NSLog(@"\n\n *** model removed from index : %ld **\n\n",objIndex);
              [selectedIndexes removeObjectAtIndex:objIndex];
        }
        
        
        [tblResponse reloadData];
        
        
    }
}
-(void)btnDonePressed
{
    NSMutableArray *RecipientDetailsArray=[[NSMutableArray alloc]init];

    [self.view endEditing:YES];
    for (int i=0; i<[selectedIndexes count]; i++) {
        @autoreleasepool{
        AllUserList *info = [selectedIndexes objectAtIndex:i];
        strRecipient =[strRecipient stringByAppendingFormat:@"%@,",info.Username];
        [RecipientIDArray addObject:[NSString stringWithFormat:@"%d",info.User_ID]];
            
            NSDictionary *tempDic=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",info.User_ID],@"RecipientID",info.Username,@"RecipientName", nil];
            
            [RecipientDetailsArray addObject:tempDic];
            
        }
    }
    [self.delegate RecipientSelected:strRecipient RecipientId:RecipientIDArray RecipientDetails:RecipientDetailsArray selectedArrayIndex:selectedIndexes];
    [self.delegate PopOverDonePressed];
}


-(void)btnBackTapped{
    NSLog(@"btnBackTapped");
}

/*
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([[searchBar.text trimString] isEqualToString:@""])
    {
        filteredRecipientArray = RecipientArray;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Username CONTAINS[cd] %@",
                                  searchBar.text];
        filteredRecipientArray = [[RecipientArray filteredArrayUsingPredicate:predicate] mutableCopy];
        
    }
    [tblResponse reloadData];
}
*/

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




@end
