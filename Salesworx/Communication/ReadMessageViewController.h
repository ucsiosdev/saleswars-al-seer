//
//  ReadMessageViewController.h
//  DataSyncApp
//
//  Created by sapna on 1/28/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSRMessagedetails.h"
#import "FTPBaseViewController.h"
@interface ReadMessageViewController : FTPBaseViewController

{
    FSRMessagedetails *messageDeatils;
    
    IBOutlet UIButton *btnResponse,*btnclose,*btnSubmit;
    IBOutlet UITextView *txtMessageView,*txtMessageResponseView;
    IBOutlet UILabel *lblTitle,*llbFrom,*lblDate,*lblResponse;
    IBOutlet UIView *readView;
    IBOutlet UIImageView *mainImage;

    NSString *alertType;
    
}
@property(nonatomic,strong)FSRMessagedetails *messageDeatils;
-(void)UserAlert:(NSString *)Message;
-(void)setMessageDeatils;
-(IBAction)btnPressed:(id)sender;

@end
