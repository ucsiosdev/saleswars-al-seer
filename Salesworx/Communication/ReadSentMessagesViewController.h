//
//  ReadSentMessagesViewController.h
//  SWPlatform
//
//  Created by msaad on 3/3/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FTPBaseViewController.h"
@interface ReadSentMessagesViewController : FTPBaseViewController
{
    IBOutlet UILabel *lblTo , *lblTitle  , *lblDate;
    IBOutlet UITextView *txtMessages;
    NSMutableDictionary *messageDict;
    IBOutlet UIImageView *mainImage;

}

@property (nonatomic, strong) NSMutableDictionary *messageDict;

@end
