
//  PaymentReportViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "PaymentReportViewController.h"
#import "SalesWorxPaymentSummaryTableViewCell.h"
#import "PaymentSummaryFilterViewController.h"
@interface PaymentReportViewController ()

@end

@implementation PaymentReportViewController
@synthesize datePickerViewControllerDate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.titleView=[SWDefaults fetchTitleView:kPaymentSummaryTitle];

    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    datePickerViewController.forReports=YES;
    
     self.datePickerViewControllerDate=datePickerViewController;
    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1]; // note that I'm setting it to -1
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    toDate=[formatter stringFromDate:[NSDate date]];
    custType = @"All";
    [self viewReportAction];
    formatter=nil;
    usLocale=nil;
    
    // Do any additional setup after loading the view.
    
    filterTableView.cellLayoutMarginsFollowReadableWidth = NO;
    gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    paymentTableView.layer.cornerRadius = 8.0;
    paymentTableView.layer.masksToBounds = YES;
    
    if (@available(iOS 15.0, *)) {
        paymentTableView.sectionHeaderTopPadding = 0;
        filterTableView.sectionHeaderTopPadding = 0;
        gridView.tableView.sectionHeaderTopPadding = 0;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    Singleton * singleton=[Singleton retrieveSingleton];
    singleton.showCustomerDashboard=NO;
    [self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        fromDate=[formatter stringFromDate:sender.selectedDate];
    }
    else
    {
        toDate = [formatter stringFromDate:sender.selectedDate];
    }
    [filterTableView reloadData];
    formatter=nil;
    usLocale=nil;
    [self viewReportAction];
    
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (IBAction)filterButtonTapped:(id)sender {
    if (paymentSummaryArray.count > 0) {
        PaymentSummaryFilterViewController *popOverVC = [[PaymentSummaryFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.previousFilterParametersDict = previousFilteredParameters;
        popOverVC.customerDictionary = customerDict;
        popOverVC.paymentSummaryArray = paymentSummaryArray;
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController = filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 550) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==0)
    {
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:CGRectMake(280, 95, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
    }
    else if(indexPath.row==1)
    {
        isFromDate=YES;
        
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        [fromater setDateFormat:@"yyyy-MM-dd"];
        NSDate * finalDate=[fromater dateFromString:fromDate];
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        datePickerViewControllerDate.picker.date=finalDate;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,135, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==2)
    {
        isFromDate=NO;
        NSDateFormatter * fromater=[[NSDateFormatter alloc]init];
        [fromater setDateFormat:@"yyyy-MM-dd"];
        NSDate * finalDate=[fromater dateFromString:toDate];
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GST-07:00"];
        [fromater setTimeZone:sourceTimeZone];
        
        NSLog(@"From Date after formatting  %@", finalDate);
        
        datePickerViewControllerDate.picker.date=finalDate;
        [datePickerPopOver presentPopoverFromRect:CGRectMake(280,165, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else if(indexPath.row==3)
    {
        customVC=nil;
        customVC = [[CustomerPopOverViewController alloc] init] ;
        [customVC setTarget:self];
        [customVC setAction:@selector(typeChanged:)];
        [customVC setPreferredContentSize:CGSizeMake(200,250)];
        customPopOver=[[UIPopoverController alloc] initWithContentViewController:customVC];
        customPopOver.delegate=self;
        isCustType=YES;
        [customVC setTypes:[NSArray arrayWithObjects:@"All", @"Cash",@"Current Cheque",@"PDC" , nil]];
        [customPopOver presentPopoverFromRect:CGRectMake(280,195, 10,10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}



- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSDictionary *data = [paymentSummaryArray objectAtIndex:row];
    
    NSLog(@"customer name is %@", [data description]);
    
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data stringForKey:@"Collection_Ref_No"]];
    } else if (column == 1) {
        value = [data dateStringForKey:@"Collected_On" withDateFormat:NSDateFormatterMediumStyle];
    } else if (column == 2) {
        
        
        //fetch customer name
        
        NSString* custNameQry=[NSString stringWithFormat:@"select Customer_Name from tbl_Customer where Customer_ID='%@' ",[data valueForKey:@"Customer_ID"] ];
        NSMutableArray* custNameArry=[[SWDatabaseManager retrieveManager]fetchDataForQuery:custNameQry];
        
        if (custNameArry.count>0) {
            
            value=[[custNameArry valueForKey:@"Customer_Name"] objectAtIndex:0];
        }
        
        else
        {
             value = [data valueForKey:@"Customer_Name"];
        }
        //value = [data stringForKey:@"Customer_Name"];
    } else if (column == 3) {
        value = [data stringForKey:@"Collection_Type"];
    } else if (column == 4) {
        value = [data currencyStringForKey:@"Amount"];
    }else if (column == 5) {
    }
    return value;
}
- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 22;
    } else if (columnIndex == 2) {
        return 30;
    }
    else if (columnIndex == 1) {
        return 15;
    }
    else if (columnIndex == 3) {
        return 15;
    }else
        return 20;
}
-(IBAction)viewReportAction
{
    
    //Payment report redundancy fixed
    
//    NSString *q = @"SELECT   A.Collection_Ref_No,A.Collected_On, C.Customer_No, C.Customer_Name, A.Collection_Type,A.Collection_Type As PayType, A.Cheque_No, A.Cheque_Date, A.Bank_Name, A.Amount, A.Bank_Branch FROM TBL_Collection AS A INNER JOIN TBL_Customer AS C ON A.Customer_ID = C.Customer_ID AND A.Site_Use_ID = C.Site_Use_ID  WHERE A.Collected_On >='{0}'  AND  A.Collected_On <='{1}' ";
    
//    select TBL_Collection.Collection_Ref_NO ,TBL_Collection.Collection_Type,TBL_Collection.Amount,TBL_Collection.Collected_On,TBL_Collection.Customer_ID,
//    TBL_Customer.customer_name ,
//    (TBL_Doc_Addl_Info.Custom_Attribute_5 *TBL_Collection.Amount ) AS Amount from TBL_Collection
//    inner join TBL_Doc_Addl_Info on TBL_Collection.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO
//    inner join TBL_Customer on TBL_Collection.customer_id = TBL_Customer.customer_id
//    where TBL_Collection.Collected_On>='2020-01-22 00:00:00' AND TBL_Collection.Collected_On<='2020-01-23 23:59:59' and TBL_Collection.customer_id = '4682'
//    ORDER BY  TBL_Collection.Collection_Type ASC, TBL_Collection.Collection_Ref_No
    
    NSString *q = @"select TBL_Collection.Collection_Ref_NO ,TBL_Collection.Collection_Type,TBL_Collection.Amount,TBL_Collection.Collected_On,TBL_Collection.Customer_ID,TBL_Customer.customer_name, (TBL_Doc_Addl_Info.Custom_Attribute_5 *TBL_Collection.Amount ) AS Amount from TBL_Collection inner join TBL_Doc_Addl_Info on TBL_Collection.Collection_Ref_No= TBL_Doc_Addl_Info.Doc_NO inner join TBL_Customer on TBL_Collection.customer_id = TBL_Customer.customer_id where TBL_Collection.Collected_On>='{0}' AND TBL_Collection.Collected_On<='{1}' AND TBL_Collection.customer_id = TBL_Customer.customer_id ";
    
    if(customerDict.count!=0)
    {
        q = [q stringByAppendingString:@" AND TBL_Collection.Customer_Id='{2}' AND TBL_Collection.Site_Use_Id='{3}'"];
    }
    if(![custType isEqualToString:@"All"])
    {
        q = [q stringByAppendingString:@" AND TBL_Collection.Collection_Type='{4}'"];
    }
    
    q = [q stringByAppendingString:@" ORDER BY  TBL_Collection.Collection_Type ASC, TBL_Collection.Collection_Ref_No"];
    q = [q stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",fromDate]];
    q = [q stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    if(customerDict.count!=0)
    {
        q = [q stringByReplacingOccurrencesOfString:@"{2}" withString:[customerDict stringForKey:@"Customer_ID"]];
        q = [q stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Site_Use_ID"]];
    }
    
    NSLog(@"query for payment is %@", q);
    
    if(![custType isEqualToString:@"All"])
    {
        if([custType isEqualToString:@"Cash"])
        {
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CASH"];
        }
        else if([custType isEqualToString:@"Current Cheque"])
        {
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"CURR-CHQ"];
        }
        else if([custType isEqualToString:@"PDC"])
        {
            q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:@"POST-CHQ"];
        }
    }
    else
    {
        q = [q stringByReplacingOccurrencesOfString:@"{4}" withString:custType];
    }
    
    
    NSString * finalQueryStr=q;
    
    NSLog(@"payment report  final query is %@", finalQueryStr);
    
    paymentSummaryArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:q];
    [paymentTableView reloadData];
    
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    //datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
    
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return paymentSummaryArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"salesSummary";
    SalesWorxPaymentSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.lblDocumentNo.text = @"Document No";
    cell.lblDate.text = @"Date";
    cell.lblName.text = @"Name";
    cell.lblAmount.text = @"Amount (AED)";
    cell.lblType.text = @"Type";
    
    cell.lblDocumentNo.textColor = TableViewHeaderSectionColor;
    cell.lblType.textColor = TableViewHeaderSectionColor;
    cell.lblDate.textColor = TableViewHeaderSectionColor;
    cell.lblName.textColor = TableViewHeaderSectionColor;
    cell.lblAmount.textColor = TableViewHeaderSectionColor;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *value = @"";
    
    static NSString* identifier=@"salesSummary";
    SalesWorxPaymentSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxPaymentSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data = [paymentSummaryArray objectAtIndex:indexPath.row];
    
    cell.lblDocumentNo.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Collection_Ref_No"]]];
    cell.lblDate.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data dateStringForKey:@"Collected_On" withDateFormat:NSDateFormatterMediumStyle]]];
    cell.lblName.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_Name"]]];
    cell.lblAmount.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data currencyStringForKey:@"Amount"]]];
    cell.lblType.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Collection_Type"]]];
    return cell;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}

-(void)paymentSummaryFilterDidReset
{
    previousFilteredParameters = [[NSMutableDictionary alloc]init];
    paymentSummaryArray = paymentSummaryArray;
    [paymentTableView reloadData ];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];
}

-(void)filteredPaymentSummary:(id)filteredContent
{
    NSLog(@"filtered content in report is %@", filteredContent);
    
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterFilled.png"] forState:UIControlStateNormal];
    paymentSummaryArray = paymentSummaryArray;
    [paymentTableView reloadData];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
    
}

-(void)filterParametersPaymentSummary:(id)filterParameter
{
    previousFilteredParameters = filterParameter;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}


@end

