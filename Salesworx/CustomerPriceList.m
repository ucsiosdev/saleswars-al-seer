//
//  CustomerPriceList.m
//  SWCustomer
//
//  Created by Irfan on 10/8/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.

#import "CustomerPriceList.h"
#import "SWSection.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "ProductBrandFilterViewController.h"
#import "SWFoundation.h"




@implementation CustomerPriceList

//@synthesize tableViewP;
//@synthesize customerP;
//@synthesize price;
//@synthesize popOverController;
//@synthesize customerHeaderView;
//@synthesize refLabel,amountLabel,dateLabel,statusLabel,priceLabel;
//@synthesize gridView,filterPopOver,filterButton;


- (id)initWithCustomer:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        [SWDefaults setProductFilterProductID:nil];
        
        customerP = [NSDictionary dictionaryWithDictionary:row];
        //[self setCustomerP:row];
        [SWDefaults setFilterForProductList:nil];
        self.backgroundColor = [UIColor whiteColor];

        gridView=nil;
        gridView=[[GridView alloc] initWithFrame:CGRectMake(0, 60, self.bounds.size.width, self.bounds.size.height)] ;
        [gridView setDataSource:self];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addSubview:gridView];
        
        customerHeaderView=nil;
        
        
        //apply flag for customer dashboard
        
//        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(-50, 0, 430, 60) andCustomer:customerP];
        
        customerHeaderView=[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:customerP];
        
        [customerHeaderView setShouldHaveMargins:YES];
        
        filterButton=nil;
        filterButton = [[UIButton alloc] init] ;
        //filterButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
//        if([language isEqualToString:@"ar"])
//        {
//            [filterButton setImage:[UIImage imageNamed:@"searchMSG-AR.png" cache:NO] forState:UIControlStateNormal];
//        }
//        else
//        {
//            [filterButton setImage:[UIImage imageNamed:@"searchMSG.png" cache:NO] forState:UIControlStateNormal];
//        }
        
        [filterButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [filterButton setTitle:NSLocalizedString(@"Search", nil) forState:UIControlStateNormal];
        filterButton.titleLabel.font = BoldSemiFontOfSize(14);
        
        //apply flag for customer dashboard
  //[filterButton setFrame:CGRectMake(370,10, 100, 32)];
        
        
        
        [filterButton setFrame:CGRectMake(900,12, 100, 32)];
        [filterButton addTarget:self action:@selector(showFilters:) forControlEvents:UIControlEventTouchUpInside];
        [filterButton bringSubviewToFront:self];
        [self addSubview:filterButton];
        
       // [self.gridView.tableView setTableHeaderView:self.customerHeaderView];
        [self addSubview:customerHeaderView];
        
        ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
        [filterViewController setTarget:self];
        [filterViewController setAction:@selector(filterChanged)];
        [filterViewController setPreferredContentSize:CGSizeMake(300, 270)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filterViewController] ;
        
        filterPopOver=nil;
        filterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
        [filterPopOver setDelegate:self];
        [self getServiceDiddbGetPriceList:[[SWDatabaseManager retrieveManager] dbGetPriceList:[customerP objectForKey:@"Customer_No"]]];

       // [self loadprice];
    }
    return self;
}



- (void)filterChanged
{
    [filterPopOver dismissPopoverAnimated:YES];
    //customerSer.delegate = self;

    
    [self getServiceDiddbGetPriceList:[[SWDatabaseManager retrieveManager] dbGetPriceList:[customerP objectForKey:@"Customer_No"]]];

}

- (void)showFilters:(id)sender {    
    
    [filterPopOver presentPopoverFromRect:CGRectMake(0, 0, 10 ,32) inView:filterButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];}


- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    filterPopOver=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)layoutSubviews {
    [super layoutSubviews];
}


- (void)loadprice {
    
    
    //customerSer.delegate = self;
    

}

#pragma UITableView Datasource

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tv deselectRowAtIndexPath:indexPath animated:YES];
}
#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return price.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column
{
    NSString *title = @"";
    
    if (column == 0) {
        title = NSLocalizedString(@"Item Code", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Description", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Selling Price", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"List Price", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"UOM", nil);
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column
{
    NSString *text = @"";
    NSDictionary *data = [price objectAtIndex:row];
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Item_Code"]];
    } else if (column == 1) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Description"]];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data currencyStringForKey:@"Unit_Selling_Price"]];
    } else if (column == 3) {
        text = [NSString stringWithFormat:@"%@", [data currencyStringForKey:@"Unit_List_Price"]];
    }else if (column == 4) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Item_UOM"]];
    } 
    return text;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 20;
    } else if (columnIndex == 1) {
        return 40;
    } else if (columnIndex == 2) {
        return 15;
    }
    else if (columnIndex == 3) {
        return 15;
    }
    else if (columnIndex == 4) {
        return 15;
    }
    else
        return 0;
}

#pragma mark SWCustomerService Delegate
//- (void)customerServiceDiddbGetPriceList:(NSArray *)priceList {
- (void)getServiceDiddbGetPriceList:(NSArray *)priceList {
    
    price=[NSArray arrayWithArray:priceList];
    
    if((!price || !price.count))
    {
        [self getServiceDiddbGetPriceListGeneric:[[SWDatabaseManager retrieveManager] dbGetPriceListGeneric:[customerP objectForKey:@"Customer_No"]]];
    }
    else
    {
        [gridView reloadData];

    }
    priceList=nil;
}
//- (void)customerServiceDiddbGetPriceListGeneric:(NSArray *)priceList
- (void)getServiceDiddbGetPriceListGeneric:(NSArray *)priceList
{
   price=[NSArray arrayWithArray:priceList];
    [gridView reloadData];
    priceList=nil;
}
@end
