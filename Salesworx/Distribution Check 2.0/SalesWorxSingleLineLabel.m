//
//  SalesWorxSingleLineLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/14/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxSingleLineLabel.h"

@implementation SalesWorxSingleLineLabel

@synthesize isSemiBold,isHeader,RTLSupport;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)setIsHeader:(BOOL)isHeader
{
    self.font=MedRepSingleLineLabelSemiBoldFont;
    self.textColor=MedRepSingleLineLabelSemiBoldFontColor;

}
-(void)awakeFromNib
{
    
    if (isHeader==YES) {
        NSLog(@"is single line label header? %hhd", self.isHeader);

    }
    
   // NSLog(@"single line label class called");
    if (isSemiBold==YES) {
        self.font=MedRepSingleLineLabelSemiBoldFont;
    }
    else
    {
    self.font=MedRepSingleLineLabelSemiBoldFont;
    }
    self.textColor=MedRepDescriptionLabelFontColor;

    //    self.textColor=[UIColor yellowColor];
    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
    
}


@end
