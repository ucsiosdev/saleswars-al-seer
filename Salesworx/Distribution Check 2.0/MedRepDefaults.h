//
//  MedRepDefaults.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImage+ImageEffects.h"
#import "SWDatabaseManager.h"


#define kSWX_FONT_REGULAR(s) [UIFont fontWithName:@"WeblySleekUISemilight" size:s]
#define kSWX_FONT_SEMI_BOLD(s) [UIFont fontWithName:@"WeblySleekUISemibold" size:s]

//check Distribution check Item is available or not
#define KItemAvailable [UIColor colorWithRed:(0.0/255.0) green:(192.0/255.0) blue:(115.0/255.0) alpha:1]
#define KItemNotAvailable [UIColor redColor]
#define KNoChange [UIColor whiteColor]

#define UITextViewBorderColor [UIColor colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1]
#define UITableViewCellTextColor [UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1]

#define MedRepProductCodeLabelFont [UIFont fontWithName:@"OpenSans" size:16]
#define MedRepProductCodeLabelFontColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

#define MedRepTableViewHeaderTitleLabelFont [UIFont fontWithName:@"OpenSans" size:16]
#define MedRepTableViewHeaderTitleLabelFontColor [UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1]

#define MedRepSingleLineLabelFont [UIFont fontWithName:@"OpenSans" size:16]
#define MedRepSingleLineLabelSemiBoldFont [UIFont fontWithName:@"WeblySleekUISemibold" size:14]
#define MedRepSingleLineLabelSemiBoldFontColor [UIColor colorWithRed:(44.0/255.0) green:(57.0/255.0) blue:(74.0/255.0) alpha:1]
#define MedRepElementDescriptionLabelColor [UIColor colorWithRed:(45.0/255.0) green:(57.0/255.0) blue:(76.0/255.0) alpha:1]

#define FeedbackOtherReasonTextViewLimit 250

#define kDateFormatWithoutTime @"dd MMM, yyyy"
#define kDateFormatWithTime @"dd MMM, yyyy hh:mm a"
#define kDatabseDefaultDateFormatWithoutTime @"yyyy-MM-dd"
#define kDatabseDefaultDateFormat @"yyyy-MM-dd HH:mm:ss"
#define kDatabaseFormatDC @"MMM dd, yyyy"


#ifdef DEBUG
#define DebugLog(s, ...) NSLog(s, ##__VA_ARGS__)
#else
#define DebugLog(s, ...)
#endif



@interface MedRepDefaults : NSObject

+(NSString*) refineDateFormat:(NSString *) srcFormat destFormat:(NSString *)destFormat scrString:(NSString *) srcString;
+(NSString *)getDefaultStringForEmptyString:(NSString *)str;

@end


