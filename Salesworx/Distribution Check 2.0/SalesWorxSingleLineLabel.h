//
//  SalesWorxSingleLineLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/14/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepDefaults.h"
@interface SalesWorxSingleLineLabel : UILabel

@property (nonatomic) IBInspectable BOOL isSemiBold;

@property(nonatomic) BOOL isHeader;


@property (nonatomic) IBInspectable BOOL RTLSupport;
-(NSString*)text;
-(void)setText:(NSString*)newText;
@end
