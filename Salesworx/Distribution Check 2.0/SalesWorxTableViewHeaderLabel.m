//
//  SalesWorxTableViewHeaderLabel.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/15/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxTableViewHeaderLabel.h"
#import "MedRepDefaults.h"

@implementation SalesWorxTableViewHeaderLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.font=MedRepTableViewHeaderTitleLabelFont;
    self.textColor=MedRepTableViewHeaderTitleLabelFontColor;
}
@end
