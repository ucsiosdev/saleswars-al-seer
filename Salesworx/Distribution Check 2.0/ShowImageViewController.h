//
//  ShowImageViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/4/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(strong,nonatomic) NSString *imageName;
@property(strong,nonatomic) UIImage *image;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
