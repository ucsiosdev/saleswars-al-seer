//
//  SalesWorxDistributionItemTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 4/8/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxDistributionItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCode;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDescription;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblQuantity;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblExpiry;
@property (weak, nonatomic) IBOutlet UIView *statusView;


@end
