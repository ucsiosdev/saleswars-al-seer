//
//  SalesWorxCustomClass.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/7/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWorxCustomClass : NSObject

@end



@interface PriceList : NSObject
@property(strong,nonatomic) NSString *Price_List_ID,*Inventory_Item_ID,*Organization_ID,*Item_UOM,*Unit_Selling_Price,*Unit_List_Price,*Is_Generic;
@end

@interface Stock : NSObject
@property(strong,nonatomic) NSString *Org_ID,*Lot_Qty,*Lot_No,*Expiry_Date,*Stock_ID,*On_Order_Qty,*warehouse,*lotNumberWithExpiryDate;
@end


@interface BonusItem : NSObject
@property(strong,nonatomic) NSString *Description,*Get_Add_Per,*Get_Item,*Get_Qty,*Price_Break_Type_Code,*Prom_Qty_From,*Prom_Qty_To;

@end

@interface ProductMediaFile : NSObject

@property(strong,nonatomic) NSString *Media_File_ID,*Entity_ID_1,*Entity_ID_2,*Entity_Type,*Media_Type,*Filename,*Caption,*Thumbnail,*Is_Deleted,*Download_Flag,*Custom_Attribute_1,*Custom_Attribute_2,*Custom_Attribute_3,*Custom_Attribute_4,*File_Path;


@end
@interface Products  : NSObject
@property(strong,nonatomic) NSString * Agency;
@property(strong,nonatomic) NSString*Inventory_Item_ID;
@property(strong,nonatomic) NSString * Brand_Code;
@property(strong,nonatomic) NSString * Category;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * IsMSL;
@property(strong,nonatomic) NSString * ItemID;
@property(strong,nonatomic) NSString * Lot_Qty;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * OrgID;
@property(strong,nonatomic) NSString * Sts;
@property(strong,nonatomic) NSString * Discount;
@property(strong,nonatomic) NSString * selectedUOM;
@property(strong,nonatomic) NSString * primaryUOM;
@property(nonatomic) NSInteger  conversionValue;
@property(strong,nonatomic) NSMutableArray* productStockArray;
@property(strong,nonatomic) NSMutableArray* productBonusArray;
@property(strong,nonatomic) NSMutableArray* productPriceListArray;
@property(nonatomic,strong) NSString* stock;
@property(nonatomic,strong) NSString* bonus;
@property(strong,nonatomic) NSMutableArray* productImagesArray;
@property(strong,nonatomic) ProductMediaFile* productMediaFile;
@property(strong,nonatomic) Stock *productStock;
@property(strong,nonatomic)PriceList *productPriceList;
@property(strong,nonatomic)NSString *nearestExpiryDate;
@property(nonatomic)BOOL isSellingPriceFieldEditable;
@property(nonatomic,strong) NSString* specialDicount;
@property(nonatomic,strong) NSString* Blocked_Stock;
@property(nonatomic,strong) NSString* QC_Stock;
@property(nonatomic,strong) NSString* formattedStock;


@end


@interface ProductCategories : NSObject

@property(strong,nonatomic)NSString* Category;

@property(strong,nonatomic)NSString* Description;

@property(strong,nonatomic)NSString* Org_ID;

@property(strong,nonatomic)NSString* Item_No;

@property(strong,nonatomic) NSString * isSelected;
@end

@interface CustomerCategory : NSObject
@property(strong,nonatomic) NSString * Category;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Item_No;
@property(strong,nonatomic) NSString * Org_ID;
@end


@interface SelectedBrand : NSObject

@property(strong,nonatomic) NSString * brandCode;
@property(strong,nonatomic) NSString * Sales_Value;
@property(strong,nonatomic) NSString * Balance_To_Go;
@property(strong,nonatomic) NSString * Target_Value;


@end


@interface CustomerOrderHistory : NSObject

@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) NSString * End_Time;
@property(strong,nonatomic) NSString * ERP_Status;
@property(strong,nonatomic) NSString * FSR;
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * Transaction_Amt;
@property(strong,nonatomic) NSString * Shipping_Instructions;

@end



@interface CustomerOrderHistoryLineItems : NSObject

@property(strong,nonatomic) NSString * Calc_Price_Flag;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Inventory_Item_ID;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * Order_Quantity_UOM;
@property(strong,nonatomic) NSString * Ordered_Quantity;
@property(strong,nonatomic) NSString * Unit_Selling_Price;
@property(strong,nonatomic) NSString * Item_Value;

@end

@interface ToDo : NSObject

@property(strong,nonatomic) NSString *Visit_ID;
@property(strong,nonatomic) NSString *Row_ID;
@property(strong,nonatomic)NSString * Title;
@property(strong,nonatomic)NSString * Description;
@property(strong,nonatomic)NSString * Todo_Date;
@property(strong,nonatomic)NSString * Inv_Customer_ID;
@property(strong,nonatomic)NSString * Inv_Site_Use_ID;
@property(strong,nonatomic)NSString * Created_At;
@property(strong,nonatomic)NSString * Created_By;
@property(strong,nonatomic)NSString * Last_Updated_At;
@property(strong,nonatomic)NSString * Last_Updated_By;
@property(strong,nonatomic)NSString * Status;
@property(strong,nonatomic)NSString * Ship_Customer_ID;
@property(strong,nonatomic)NSString * Ship_Site_Use_ID;

@property(strong,nonatomic)NSString * isUpdated;
@property(strong,nonatomic)NSString * isNew;
@property(strong,nonatomic)NSString * customerName;


@end


@interface CustomerOrderHistoryInvoiceLineItems : NSObject

@property(strong,nonatomic) NSString * Inventory_Item_ID;
@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Expiry_Date;
@property(strong,nonatomic) NSString * Order_Quantity_UOM;
@property(strong,nonatomic) NSString * Ordered_Quantity;
@property(strong,nonatomic) NSString * Calc_Price_Flag;
@property(strong,nonatomic) NSString * Unit_Selling_Price;
@property(strong,nonatomic) NSString * Item_Value;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * ERP_Ref_No;
@property(strong,nonatomic) NSString * Order_Date;

@end

@interface SWCustomerPriceList : NSObject

@property(strong,nonatomic) NSString * Description;
@property(strong,nonatomic) NSString * Brand_Code;
@property(strong,nonatomic) NSString * Is_Generic;
@property(strong,nonatomic) NSString * Item_Code;
@property(strong,nonatomic) NSString * Item_UOM;
@property(strong,nonatomic) NSString * Unit_List_Price;
@property(strong,nonatomic) NSString * Unit_Selling_Price;

@end


@interface CustomerDues : NSObject

@property(strong,nonatomic)NSMutableDictionary * duesDictionary;


@end

@interface Customer : NSObject

@property(strong,nonatomic) NSMutableDictionary * cashCustomerDictionary;
@property(strong,nonatomic) NSString* cash_Name;
@property(strong,nonatomic) NSString* cash_Phone;
@property(strong,nonatomic) NSString* cash_Contact;
@property(strong,nonatomic) NSString* cash_Address;
@property(strong,nonatomic) NSString* cash_Fax;


@property(strong,nonatomic) NSString * Planned_visit_ID;
@property(strong,nonatomic) NSString * Visit_Type;
@property(strong,nonatomic)NSMutableArray* customerDuesArray;
@property(strong,nonatomic) NSString * Address;
@property(strong,nonatomic) NSString * Allow_FOC;
@property(strong,nonatomic) NSString * Avail_Bal;
@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Bill_Credit_Period;
@property(strong,nonatomic) NSString * Total_Due;
@property(strong,nonatomic) NSString * Prior_Due;
@property(strong,nonatomic) NSString * PDC_Due;
@property(strong,nonatomic) NSString * Cash_Cust;
@property(strong,nonatomic) NSString * Chain_Customer_Code;
@property(strong,nonatomic) NSString * City;
@property(strong,nonatomic) NSString * Contact;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) NSString * Credit_Hold;
@property(strong,nonatomic) NSString * Credit_Limit;
@property(strong,nonatomic) NSString * Cust_Lat;
@property(strong,nonatomic) NSString * Cust_Long;
@property(strong,nonatomic) NSString * Cust_Status;
@property(strong,nonatomic) NSString * Customer_Barcode;
@property(strong,nonatomic) NSString * Customer_Class;
@property(strong,nonatomic) NSString * Customer_ID;
@property(strong,nonatomic) NSString * Customer_No;
@property(strong,nonatomic) NSString * Customer_OD_Status;
@property(strong,nonatomic) NSString * Customer_Segment_ID;
@property(strong,nonatomic) NSString * Customer_Type;
@property(strong,nonatomic) NSString * Dept;
@property(strong,nonatomic) NSString * Location;
@property(strong,nonatomic) NSString * Phone;
@property(strong,nonatomic) NSString * Postal_Code;
@property(strong,nonatomic) NSString * Price_List_ID;
@property(strong,nonatomic) NSString * SalesRep_ID;
@property(strong,nonatomic) NSString * Sales_District_ID;
@property(strong,nonatomic) NSString * Ship_Customer_ID;
@property(strong,nonatomic) NSString * Ship_Site_Use_ID;
@property(strong,nonatomic) NSString * Site_Use_ID;
@property(strong,nonatomic) NSString * Sync_Timestamp;
@property(strong,nonatomic) NSString * Trade_Classification;
@property(strong,nonatomic) CustomerCategory * customerCategory;
@property(strong,nonatomic) NSMutableArray * categoriesArray;
@property(strong,nonatomic) NSMutableArray * brandsArray;

@property(strong,nonatomic)CustomerOrderHistory * customerOrderHistory;
@property(strong,nonatomic)CustomerOrderHistoryLineItems * customerOrderHistoryLineItems;
@property(strong,nonatomic)CustomerOrderHistoryInvoiceLineItems * customerOrderHistoryInvoiceLineItems;

@property(strong,nonatomic) NSMutableArray * customerOrderHistoryArray;
@property(strong,nonatomic) NSMutableArray * customerOrderHistoryLineItemsArray;
@property(strong,nonatomic) NSMutableArray * customerOrderHistoryInvoiceLineItemsArray;

@property(strong,nonatomic) NSMutableArray * customerPriceListArray;
@property(strong,nonatomic) CustomerDues* customerDues;

@property(strong,nonatomic)SelectedBrand* selectedBrand;
@property(strong,nonatomic) NSString* targetValue;
@property(strong,nonatomic) NSString* salesValue;
@property(strong,nonatomic) NSString* balanceToGoValue;
@property(strong,nonatomic) NSString* totalOutStanding;
@property(strong,nonatomic) NSString* Over_Due;
@property(nonatomic) double totalOrderAmount;
@property BOOL isBonusAvailable;

@property(strong,nonatomic) NSString* IS_LOC_RNG;
@property(strong,nonatomic) NSString * CUST_LOC_RNG;

@end



//Visit options



@interface DistributionCheck : NSObject
@property(strong,nonatomic) NSString* Item_Code;
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Avl;
@property(strong,nonatomic) NSString* Qty;
@property(strong,nonatomic) NSString* LotNo;
@property(strong,nonatomic) NSString* ExpDate;
@property(strong,nonatomic) Stock *ItemStock;

@end

@interface DistriButionCheckItem : NSObject
@property(strong,nonatomic) Products *DcProduct;
@property(strong,nonatomic) NSString *itemAvailability;
@property(strong,nonatomic) NSString *Quntity;
@property(strong,nonatomic) NSString *LotNumber;
@property(strong,nonatomic) NSString *expiryDate;
@property(strong,nonatomic) NSString *imageName;
@property(strong,nonatomic) NSString *reOrder;
@property(nonatomic) NSMutableArray  *dcItemLots;
@end

@interface DistriButionCheckMinStock : NSObject
@property(strong,nonatomic) NSString *InventoryItemID;
@property(strong,nonatomic) NSString *organizationID;
@property(strong,nonatomic) NSString *attributeValue;
@end

@interface DistriButionCheckItemLot : NSObject
@property(strong,nonatomic) NSString *Quntity;
@property(strong,nonatomic) NSString *LotNumber;
@property(strong,nonatomic) NSString *expiryDate;
@property(strong,nonatomic) NSString *isImage;
@property(strong,nonatomic) NSString * DistriButionCheckItemLotId;
@property(strong,nonatomic) NSString *reOrder;
@property(strong,nonatomic) NSString *itemAvailability;

@end

@interface DistriButionCheckLocation : NSObject
@property(strong,nonatomic) NSString* LocationName;
@property(nonatomic) NSString * LocationID;
@property(strong,nonatomic) NSMutableArray *dcItemsArray;
@property(strong,nonatomic) NSMutableArray *dcItemsUnfilteredArray;
@property(nonatomic) BOOL isLocationDCCompleted;

@end

@interface SalesOrderTemplateLineItems : NSObject
@property(strong,nonatomic) NSString* Order_Template_ID;
@property(strong,nonatomic) NSString* Inventory_Item_ID;
@property(strong,nonatomic) NSString* Organization_ID;
@property(strong,nonatomic) NSString* Quantity;
@property(strong,nonatomic) NSString* Custom_Attribute_1;
@property(strong,nonatomic) NSString* Custom_Attribute_2;
@property(strong,nonatomic) NSString* Custom_Attribute_3;
@property(strong,nonatomic) NSString* Unit_Selling_Price;
@property(strong,nonatomic) NSString* WholeSale_Price;
@property(strong,nonatomic) Products* selectedProduct;

@end

@interface SalesOrderTemplete : NSObject

@property(strong,nonatomic) NSString* Order_Template_ID;
@property(strong,nonatomic) NSString* Template_Name;
@property(strong,nonatomic) NSString* SalesRep_ID;
@property(strong,nonatomic) NSString* Custom_Attribute_1;
@property(strong,nonatomic) NSString* Custom_Attribute_2;
@property(strong,nonatomic) NSString* Custom_Attribute_3;

@end

@interface SalesWorxParentOrder : NSObject
@property(strong,nonatomic) NSString*Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString*Transaction_Amt;

@end

@interface SalesOrderInitialization : NSObject


@end
@interface SalesOrderConfirmationDetails : NSObject
{
    
}
@property(nonatomic,strong) NSString* DocReferenceNumber;
@property(nonatomic,strong) NSString* shipDate;
@property(nonatomic,strong) NSString* signeeName;
@property(nonatomic,strong) NSString* comments;
@property(nonatomic,strong) NSString *isWholeSaleOrder;
@property(nonatomic,strong) NSString *SkipConsolidation;
@property(nonatomic) BOOL isCustomerSigned;
@property(nonatomic) BOOL isLPOImagesCaptured;

@end

@interface ReturnsConfirmationDetails : NSObject
{
    
}
@property(nonatomic,strong) NSString* DocReferenceNumber;
@property(nonatomic,strong) NSString* signeeName;
@property(nonatomic,strong) NSString* comments;
@property(nonatomic,strong) NSString *returnType;
@property(nonatomic,strong) NSString *isGoodsCollected;
@property(nonatomic,strong) NSString* InvoicesNumbers;
@property(nonatomic) BOOL isCustomerSigned;


@end

@interface SalesWorxReturn : NSObject
@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSMutableArray* returnLineItemsArray;
@property (strong,nonatomic)ReturnsConfirmationDetails *confrimationDetails;
@property(strong,nonatomic) NSString* ReturnStartTime;
@end

@interface ReturnsLineItem : NSObject
{
    
}
@property(strong,nonatomic) Products *returnProduct;
@property(nonatomic) Products *bonusProduct;
@property(nonatomic) double defaultBonusQty;
@property(nonatomic) double recievedBonusQty;
@property(nonatomic) double returnProductQty;
@property(nonatomic) double returnItemNetamount;
@property(strong,nonatomic) NSMutableArray* bonusDetailsArray;
@property(strong,nonatomic) NSString* lotNumber;
@property(strong,nonatomic) NSString* expiryDate;
@property(strong,nonatomic) NSString* reasonForReturn;
@property(strong,nonatomic) NSString* reasonForReturnDescription;

@end

@interface ManageOrderDetails : NSObject
@property(strong,nonatomic) NSString * manageOrderStatus;
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * Row_ID;
@property(strong,nonatomic) NSString * Visit_ID;
@property(strong,nonatomic) NSString * FOCOrderNumber;
@property(strong,nonatomic) NSString * FOCParentOrderOrderNumber;
@property(strong,nonatomic) NSString * OrderAmount;
@property(strong,nonatomic) NSString * Creation_Date;
@property(strong,nonatomic) ProductCategories * Category;
@property(strong,nonatomic) Customer * customerDetails;

@end
@interface SalesWorxReOrder : NSObject
@property(strong,nonatomic) NSString * Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString * OrderAmount;
@property(strong,nonatomic) NSString * Creation_Date;
@end
@interface SalesOrder : NSObject

@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) NSMutableArray* salesOrderTempleteArray;
@property(strong,nonatomic) SalesOrderTemplete * selectedTemplete;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSString* selectedOrderType;
@property(strong,nonatomic) NSString* focParentOrderNumber;
@property(nonatomic) BOOL isTemplateOrder;
@property(nonatomic) BOOL isManagedOrder;
@property(strong,nonatomic) NSMutableArray* SalesOrderLineItemsArray;
@property (strong,nonatomic)SalesOrderConfirmationDetails *confrimationDetails;
@property(strong,nonatomic) NSString* OrderStartTime;
@property(strong,nonatomic) ManageOrderDetails* SelectedManageOrderDetails;
@property(strong,nonatomic) SalesWorxReOrder *reOrderDetails;
@end

@interface SalesOrderLineItems : NSObject
@property(strong,nonatomic) NSMutableArray* productCategoriesArray;
@property(strong,nonatomic) NSMutableArray* salesOrderTempleteArray;
@property(strong,nonatomic) SalesOrderTemplete * selectedTemplete;
@property(strong,nonatomic) NSMutableArray* salesOrderTemplateLineItemsArray;
@property(strong,nonatomic) ProductCategories* selectedCategory;
@property(strong,nonatomic) NSString* selectedOrderType;
@property(strong,nonatomic) NSString* focOrderNumber;
@end

@interface OrderHistory : NSObject

@end

@interface Collection : NSObject

@end

@interface Survey : NSObject

@end



@interface VisitOptions : NSObject

@property(strong,nonatomic)Customer* customer;
@property(strong,nonatomic)DistributionCheck* distributionCheck;
@property(strong,nonatomic)SalesOrder* salesOrder;
@property(strong,nonatomic)NSMutableArray* manageOrders;
@property(strong,nonatomic)OrderHistory* orderHistory;
@property(strong,nonatomic)Collection* collection;
@property(strong,nonatomic)Survey* survey;
@property(strong,nonatomic)SalesWorxReturn *returnOrder;
@property(strong,nonatomic)ToDo * toDo;
@property(strong,nonatomic) NSMutableArray* toDoArray;

@end



@interface SalesWorxVisit  : NSObject

@property(strong,nonatomic) VisitOptions * visitOptions;
@property(strong,nonatomic) NSString* Visit_ID;
@property(strong,nonatomic) NSString* Visit_Type;
@property(nonatomic) BOOL isPlannedVisit;
@property(nonatomic) NSString* FSR_Plan_Detail_ID;
@property(strong,nonatomic) NSString* onSiteExceptionReason;
@property(nonatomic) BOOL isSalesOrderCompleted;
@property(nonatomic) BOOL isDistributionCheckCompleted;
@property(nonatomic) BOOL isManageOrderCompleted;
@property(nonatomic) BOOL isReturnsCompleted;
@property(nonatomic) BOOL isSurveyCompleted;
@property(nonatomic) BOOL isCollectionCompleted;
@property(nonatomic) BOOL isOrderHistoryAccessed;
@property(nonatomic) BOOL isTODOCompleted;
@property(nonatomic) BOOL isFSRinCustomerLocation;
@end



@interface SalesOrderLineItem : NSObject
{
    
}
@property(strong,nonatomic) Products *OrderProduct;
@property(strong,nonatomic) Products *manualFocProduct;
@property(nonatomic) double manualFOCQty;
@property(nonatomic) Products *bonusProduct;
@property(nonatomic) double defaultBonusQty;
@property(nonatomic) double requestedBonusQty;
@property(nonatomic) double productDiscount;
@property(nonatomic) double productSelleingPrice;
@property(nonatomic) double OrderProductQty;
@property(nonatomic) double OrderItemTotalamount;
@property(nonatomic) double OrderItemDiscountamount;
@property(nonatomic) double OrderItemNetamount;
@property(nonatomic) double appliedDiscount;
@property(strong,nonatomic) NSMutableArray* bonusDetailsArray;
@property(nonatomic) NSString* NotesStr;
@property(strong,nonatomic) NSMutableArray* AssignedLotsArray;
- (id)copy;
@end



@interface ProductBonusItem : NSObject
{
    
}
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Get_Add_Per;
@property(strong,nonatomic) NSString* Get_Item;
@property(strong,nonatomic) NSString* Get_Qty;
@property(strong,nonatomic) NSString* Price_Break_Type_Code;
@property(strong,nonatomic) NSString* Prom_Qty_From;
@property(strong,nonatomic) NSString* Prom_Qty_To;
@property(strong,nonatomic) Products *bonusProduct;

@end

@interface SalesOrderAssignedLot : NSObject
{
    
}
@property(strong,nonatomic) Stock* lot;
@property(nonatomic) double assignedQuantity;
- (id)copy;


@end

@interface RMALotType : NSObject
{
    
}
@property(strong,nonatomic) NSString* lotType;
@property(strong,nonatomic) NSString* Description;
@end


@interface SalesWorxReasonCode : NSObject
{
    
}
@property(strong,nonatomic) NSString* Reasoncode;
@property(strong,nonatomic) NSString* Description;
@property(strong,nonatomic) NSString* Purpose;

@end


@interface DailyVisitSummaryReport : NSObject
@property(strong,nonatomic) NSString * Customer_ID;
@property(strong,nonatomic) NSString * Customer_Name;
@property(strong,nonatomic) NSString * Payment;
@property(strong,nonatomic) NSString * PaymentValue;
@property(strong,nonatomic) NSString * DC;
@property(strong,nonatomic) NSString * Survey;
@property(strong,nonatomic) NSString * Order;
@property(strong,nonatomic) NSString * OrderValue;
@property(strong,nonatomic) NSString * Return;
@property(strong,nonatomic) NSString * ReturnValue;

@end
@interface SyncImage : NSObject
@property (strong,nonatomic)   NSString * imagePath;
@property (strong,nonatomic)   NSString * imageSyncType;
@property (nonatomic)   BOOL isUploaded ;
@property (nonatomic)   NSString * imageMIMEType ;
@property (nonatomic)   NSInteger uploadFailureCount ;

@end

@interface SalesWorxBrandAmbassadorLocation : NSObject
@property (strong,nonatomic)   NSString * Location_ID;
@property (strong,nonatomic)   NSString * Location_Name;
@property (strong,nonatomic)   NSString * Latitude;
@property (strong,nonatomic)   NSString * Longitude;
@property (strong,nonatomic)   NSString * City;
@property (strong,nonatomic)   NSString * Address;
@property (strong,nonatomic)   NSString * Phone;


@end

@interface SalesWorxBrandAmbassadorTask : NSObject
@property (strong,nonatomic) NSString* Task_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Priority;
@property (strong,nonatomic) NSString* Start_Time;
@property (strong,nonatomic) NSString* Status;
@property (strong,nonatomic) NSString* Assigned_To;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Show_Reminder;
@property (strong,nonatomic) NSString* IS_Active;
@property (strong,nonatomic) NSString* Is_deleted;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Doctor_Name;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Contact_Name;
@property (strong,nonatomic) NSString* Task_Location_Type;
@property (strong,nonatomic) NSString* End_Time;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* isUpdated;
@end


@interface salesWorxBrandAmbassadorNotes : NSObject
@property (strong,nonatomic) NSString* Note_ID;
@property (strong,nonatomic) NSString* Title;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Category;
@property (strong,nonatomic) NSString* Emp_ID;
@property (strong,nonatomic) NSString* Visit_ID;
@property (strong,nonatomic) NSString* Location_ID;
@property (strong,nonatomic) NSString* Doctor_ID;
@property (strong,nonatomic) NSString* Contact_ID;
@property (strong,nonatomic) NSString* Created_At;
@property (strong,nonatomic) NSString* Created_By;
@property (strong,nonatomic) NSString* Last_Updated_By;
@property (strong,nonatomic) NSString* Last_Updated_At;
@property (strong,nonatomic) NSString* isUpdated;
@end


@interface SalesWorxBrandAmbassadorWalkinCustomerVisit : NSObject
@property (strong,nonatomic)   NSString * Walkin_Session_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Start_Time;
@property (strong,nonatomic)   NSString * End_Time;
@property (strong,nonatomic)   NSString * Latitude;
@property (strong,nonatomic)   NSString * Longitude;
@property (strong,nonatomic)   NSString * SalesRep_ID;
@property (strong,nonatomic)   NSString * Customer_Name;
@property (strong,nonatomic)   NSString * Customer_Email;
@property (strong,nonatomic)   NSString * Customer_Phone;
@property (strong,nonatomic)   NSMutableArray * surveyArray;

//this is not db property this is for reference
@property (strong,nonatomic)   NSString * Planned_Visit_ID;
@property (strong,nonatomic)   NSString * sellOutGiven;
@property (strong,nonatomic)   NSString * samplesGiven;





@end

@interface SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses : NSObject

@property (strong,nonatomic)   NSString * Walkin_Response_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Survey_ID;
@property (strong,nonatomic)   NSString * Question_ID;
@property (strong,nonatomic)   NSString * Response;
@property (strong,nonatomic)   NSString * SalesRep_ID;
@property (strong,nonatomic)   NSString * Survey_Timestamp;
@property (strong,nonatomic)   NSString * Walkin_Session_ID;
@property (strong,nonatomic)   NSString * Customer_Phone;

@end

@interface DemoPlan : NSObject
@property (strong,nonatomic) NSString* Demo_Plan_ID;
@property (strong,nonatomic) NSString* Description;
@property (strong,nonatomic) NSString* Valid_From;
@property (strong,nonatomic) NSString* Valid_To;
@property (strong,nonatomic) NSString* Is_Active;
@property (strong,nonatomic) NSString* Is_Deleted;
@end

@interface SalesWorxBrandAmbassadorVisit : NSObject
@property (strong,nonatomic)   SalesWorxBrandAmbassadorLocation * selectedLocation;
@property (strong,nonatomic)   DemoPlan * selectedDemoPlan;
@property (strong,nonatomic)   NSString * Date;
@property (strong,nonatomic)   NSString * Objective;
@property (strong,nonatomic)   NSString * Planned_Visit_ID;
@property (strong,nonatomic)   NSString * Actual_Visit_ID;
@property (strong,nonatomic)   NSString * Previous_Planned_Visit_ID;
@property (strong,nonatomic)   NSString * Emp_ID;
@property (strong,nonatomic)   NSString * Visit_Status;
@property(strong,nonatomic)     NSString * Visit_Conclusion_Notes;
@property(strong,nonatomic)     NSString * Visit_Rating;
@property (strong,nonatomic)   NSMutableArray * tasksArray;
@property (strong,nonatomic)   NSMutableArray * notesArray;
@property (strong,nonatomic)    SalesWorxBrandAmbassadorWalkinCustomerVisit * walkinCustomer;
@property (strong,nonatomic)   NSMutableArray * demoedArray;
@property (strong,nonatomic)   NSMutableArray * samplesGivenArray;
@property(strong,nonatomic)     SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses * surveyResponses;
@property(strong,nonatomic)     NSString* currentCallStartTime;
@property(strong,nonatomic)     NSString* waitTime;
@property(strong,nonatomic)     NSString* Call_Start_Date;
@property(strong,nonatomic)     NSString* Call_End_Date;
@property(strong,nonatomic)     NSString* EDetailing_Start_Time;
@property(strong,nonatomic)     NSString* EDetailing_End_Time;
@property(strong,nonatomic)     NSMutableArray* walkCustomerVisitsArray;
@property(strong,nonatomic)     NSMutableArray* walkinSurveyResponsesArray;
@property(strong,nonatomic)     NSString* Old_Visit_ID;





@end


@interface SalesWorxGoogleAnalytics : NSObject

@property(strong,nonatomic)     NSString* categoryName;
@property(strong,nonatomic)     NSString* screenName;
@property(strong,nonatomic)     NSString* Action;
@property(strong,nonatomic)     NSString* label;
@property(strong,nonatomic)     NSString* userName;
@property(strong,nonatomic)     NSString* userID;
@property(nonatomic)     NSInteger value;


@end

@interface SalesWorxSalesTrend : NSObject
@property(strong,nonatomic)     NSString* Amount;
@property(strong,nonatomic)     NSString* creationDate;
@property(strong,nonatomic)     NSString* NumberofRecords;
@end

@interface SalesWorxProductStockSync : NSObject
@property(strong,nonatomic)     NSString* Custom_Attribute_1;
@property(strong,nonatomic)     NSString* Custom_Attribute_2;
@property(strong,nonatomic)     NSString* Custom_Attribute_3;
@property(strong,nonatomic)     NSString* Custom_Attribute_4;
@property(strong,nonatomic)     NSString* Custom_Attribute_5;
@property(strong,nonatomic)     NSString* Custom_Attribute_6;
@property(strong,nonatomic)     NSString* Expiry_Date;
@property(strong,nonatomic)     NSString* Item_ID;
@property(strong,nonatomic)     NSString* Last_Updated_At;
@property(strong,nonatomic)     NSString* Lot_No;
@property(strong,nonatomic)     NSString* Lot_Qty;
@property(strong,nonatomic)     NSString* Org_ID;
@property(strong,nonatomic)     NSString* Stock_ID;

@end

@interface Customer_SalesSummary : NSObject

@property(strong,nonatomic) NSString *Orig_Sys_Document_Ref;
@property(strong,nonatomic) NSString *Creation_Date;
@property(strong,nonatomic) NSString *Doc_Type;
@property(strong,nonatomic) NSString *Transaction_Amt;
@property(strong,nonatomic) NSString *Customer_No;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *Cash_Cust;
@property(strong,nonatomic) NSString *Phone;
@property(strong,nonatomic) NSString *Visit_ID;
@property(strong,nonatomic) NSString *End_Time;
@property(strong,nonatomic) NSString *ERP_Ref_No;
@property(strong,nonatomic) NSString *Ship_To_Customer_Id;
@property(strong,nonatomic) NSString *Ship_To_Site_Id;

@end

@interface Customer_Statement : NSObject

@property(strong,nonatomic) NSString *Invoice_Ref_No;
@property(strong,nonatomic) NSString *NetAmount;
@property(strong,nonatomic) NSString *PaidAmt;
@property(strong,nonatomic) NSString *InvDate;
@property(strong,nonatomic) NSString *Customer_ID;

@end

@interface Blocked_Statement : NSObject

@property(strong,nonatomic) NSString *Contact;
@property(strong,nonatomic) NSString *Customer_ID;
@property(strong,nonatomic) NSString *Customer_Name;
@property(strong,nonatomic) NSString *Phone;

@end


@interface SalesPerAgency : NSObject
@property(strong,nonatomic)NSString* Customer_No;
@property(strong,nonatomic)NSString* Customer_Name;
@property(strong,nonatomic)NSString* Chain_Customer;
@property(strong,nonatomic)NSString* Agency;
@property(strong,nonatomic)NSString* Item_Code;
@property(strong,nonatomic)NSString* LY_YTD_Qty;
@property(strong,nonatomic)NSString* LY_YTD_Val;
@property(strong,nonatomic)NSString* CY_YTD_Qty;
@property(strong,nonatomic)NSString* CY_YTD_Val;
@property(strong,nonatomic)NSString* LY_MTD_Qty;
@property(strong,nonatomic)NSString* LY_MTD_Val;
@property(strong,nonatomic)NSString* CY_MTD_Qty;
@property(strong,nonatomic)NSString* CY_MTD_Val;
@property(strong,nonatomic)NSString* CY_M1_Qty;
@property(strong,nonatomic)NSString* CY_M1_Val;
@property(strong,nonatomic)NSString* CY_M2_Qty;
@property(strong,nonatomic)NSString* CY_M2_Val;
@property(strong,nonatomic)NSString* CY_M3_Qty;
@property(strong,nonatomic)NSString* CY_M3_Val;
@property(strong,nonatomic)NSString* ytdVariancePercentage;
@property(strong,nonatomic)NSString* mtdVariancePercentage;

@property(strong,nonatomic)NSString* ytdQtyVariancePercentage;
@property(strong,nonatomic)NSString* mtdQtyVariancePercentage;

@property(strong,nonatomic) NSString* Chain_Customer_Code;


@end

@interface VisitImages : NSObject
@property(strong,nonatomic) NSString * imagePath;
@property(strong,nonatomic) NSString * imageName;
@property(nonatomic) BOOL isSelected;
@property(strong,nonatomic) UIImage * capturedImage;
@property(strong,nonatomic) NSString *imageID;
@property (strong, nonatomic) NSString *status;
@property(nonatomic) NSMutableArray  *selectedCategoryArray;
@property(nonatomic) NSMutableArray  *selectedProductArray;
@end

@interface VisitImagesCategories : NSObject
@property(strong,nonatomic) NSString * selectedImageID;
@property(strong,nonatomic) NSString * agency;
@property(strong,nonatomic) NSString * agencyDesc;
@property(strong,nonatomic) NSString * brand_Code;
@property(strong,nonatomic) NSString * brandDesc;
@property(strong,nonatomic) NSString * category_Code;
@property(strong,nonatomic) NSString * categoryDesc;
@property(strong,nonatomic) NSString * selectedInventoryID;
@property(strong,nonatomic) NSString * inventory_Item_ID;
@property(strong,nonatomic) NSString *isSelected;
@property(nonatomic) NSMutableArray  *categoryArray;
@property(nonatomic) NSMutableArray  *filteredCategoryArray;
@property(nonatomic) NSMutableArray  *saveCategoryArray;


@end

@interface VisitImagesProducts : NSObject
@property(strong,nonatomic) NSString * inventory_Item_ID;
@property(strong,nonatomic) NSString * productDescription;
@property(strong,nonatomic) NSString * isSelected;
@property(strong,nonatomic) NSString * selectedImageID;
@property(nonatomic) NSMutableArray  *prodducstArray;
@property(nonatomic) NSMutableArray  *filteredProductsArray;

@end

@interface VisitOption_Feedback : NSObject
@property(strong,nonatomic) NSString *feedbackID;
@property(strong,nonatomic) NSString *feedbackLineID;
@property(strong,nonatomic) NSString *itemCode;
@property(strong,nonatomic) NSString *itemDescription;
@property(strong,nonatomic) NSString *inventoryItemID;
@property(strong,nonatomic) NSString *feedbackStatus;
@property(strong,nonatomic) NSString *comments;
@property(strong,nonatomic) NSString *itemType;
@property(nonatomic) BOOL isSave;
@end

@interface SyncLoctions : NSObject
@property(strong,nonatomic)NSString * name;
@property(strong,nonatomic)NSString * url;
@property(strong,nonatomic)NSString * seq;
@end
