//
//  MedRepPanelTitle.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedRepPanelTitle : UILabel
@property (nonatomic) IBInspectable BOOL RTLSupport;
@property (nonatomic) IBInspectable BOOL isTextEndingWithExtraSpecialCharacter;
-(NSString*)text;
-(void)setText:(NSString*)newText;
@end
