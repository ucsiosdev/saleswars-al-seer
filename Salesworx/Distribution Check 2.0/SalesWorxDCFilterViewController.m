//
//  SalesWorxDCFilterViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 9/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import "SalesWorxDCFilterViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
#import "NSPredicate+Distinct.h"

@interface SalesWorxDCFilterViewController ()

@end

@implementation SalesWorxDCFilterViewController
@synthesize productsArray,previousFilterParametersDict,filterPopOverController, isStockCheck;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    filteredProductsArray=[[NSMutableArray alloc]init];
    
    
    NSLog(@"previous filter parameters are, %@", previousFilterParametersDict);
    
    if (previousFilterParametersDict.count>0) {
        filterParametersDict=previousFilterParametersDict;
    }
    if([[previousFilterParametersDict valueForKey:@"Brand_Code"] length]>0) {
        brandCodeTxtFld.text = [previousFilterParametersDict valueForKey:@"Brand_Code"];
    }
    
    if([[filterParametersDict valueForKey:@"Category"] length]>0) {
        categoryNameTxtFld.text = [previousFilterParametersDict valueForKey:@"Category"];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        SKUTxtFld.text = [previousFilterParametersDict valueForKey:@"Item_Code"];
    }
    
    if([[filterParametersDict valueForKey:@"Agency"] length]>0) {
        categoryNameTxtFld.text = [previousFilterParametersDict valueForKey:@"Agency"];
    }
    
    if([[filterParametersDict valueForKey:@"Product_Code"] length]>0) {
        SKUTxtFld.text = [previousFilterParametersDict valueForKey:@"Product_Code"];
    }
    
    if (isStockCheck) {
        categoryTitleLabel.text = @"Agency";
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.isMovingToParentViewController) {
       // [SWDefaults UpdateGoogleAnalyticsforScreenName:kFieldSalesProductsFilter];
    }
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UIBarButtonItem *clearFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearFilter)];
    clearFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItems = @[clearFilterBtn];
    
    UIBarButtonItem *closeFilterBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeFilter)];
    closeFilterBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeFilterBtn;
    
    [clearFilterBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
    [closeFilterBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                                  forState:UIControlStateNormal];
}


-(void)closeFilter
{
    if ([self.delegate respondsToSelector:@selector(productsFilterDidClose)]) {
        [self.delegate productsFilterDidClose];
    }
    [filterPopOverController dismissPopoverAnimated:YES];
}

-(void)clearFilter
{
    brandCodeTxtFld.text=@"";
    categoryNameTxtFld.text=@"";
    SKUTxtFld.text=@"";
    filterParametersDict=[[NSMutableDictionary alloc]init];
}

#pragma UItextField Methods


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == brandCodeTxtFld)
    {
        selectedTextField=@"Brand Code";
        selectedPredicateString=@"Brand_Code";
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    else if (textField == categoryNameTxtFld)
    {
        if (isStockCheck) {
            selectedTextField=@"Agency";
            selectedPredicateString=@"Agency";
        } else {
            selectedTextField=@"Category";
            selectedPredicateString=@"Category";
        }
        
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        
        return NO;
    }
    else if (textField == SKUTxtFld)
    {
        selectedTextField=@"Item Code";
        
        if (isStockCheck) {
            selectedPredicateString=@"Product_Code";
        } else {
            selectedPredicateString=@"Item_Code";
        }
        
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    return YES;
}

-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    MedRepDoctorFilterDescriptionViewController *filterDescVC = [[MedRepDoctorFilterDescriptionViewController alloc]init];
    filterDescVC.selectedFilterDelegate=self;
    filterDescVC.descTitle=selectedTextField;
    filterDescVC.filterNavController=self.filterNavController;
    filterDescVC.filterPopOverController=self.filterPopOverController;
    
    NSMutableArray *filterDescArray = [[NSMutableArray alloc]init];
    NSMutableArray *unfilteredArray = [[NSMutableArray alloc]init];
    
    //get distinct keys
    NSPredicate *refinedPred = [NSPredicate predicateForDistinctWithProperty:predicateString];
    unfilteredArray = [[productsArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
    
    //now sort by asc
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    filterDescArray=[sortedArray valueForKey:predicateString];
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

#pragma mark Selected Filter Data

-(void)selectedFilterValue:(NSString*)selectedString
{
    if ([selectedTextField isEqualToString:@"Brand Code"]) {
        brandCodeTxtFld.text = selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Category"] || [selectedTextField isEqualToString:@"Agency"])
    {
        categoryNameTxtFld.text = selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
    else if ([selectedTextField isEqualToString:@"Item Code"])
    {
        SKUTxtFld.text = selectedString;
        [filterParametersDict setValue:selectedString forKey:selectedPredicateString];
    }
}

- (IBAction)searchButtonTapped:(id)sender {

    filteredProductsArray=[[NSMutableArray alloc]init];
    NSMutableArray *predicateArray = [NSMutableArray array];
    
    if([[filterParametersDict valueForKey:@"Brand_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Brand_Code ==[cd] %@",[filterParametersDict valueForKey:@"Brand_Code"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Category"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Category ==[cd] %@",[filterParametersDict valueForKey:@"Category"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Agency"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Agency ==[cd] %@",[filterParametersDict valueForKey:@"Agency"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Item_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Item_Code ==[cd] %@",[filterParametersDict valueForKey:@"Item_Code"]]];
    }
    
    if([[filterParametersDict valueForKey:@"Product_Code"] length]>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Product_Code ==[cd] %@",[filterParametersDict valueForKey:@"Product_Code"]]];
    }


    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    filteredProductsArray=[[productsArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    if (predicateArray.count==0) {
        [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Please select your filter criteria and try again" withController:self];
    }
    
    else  if (filteredProductsArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredProducts:)]) {
            [self.delegate filteredProducts:filteredProductsArray];
            [self.delegate filteredProductParameters:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:KNoMatchesStr andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

- (IBAction)resetButtonTapped:(id)sender {
    
    previousFilterParametersDict=[[NSMutableDictionary alloc]init];
    
    if ([self.delegate respondsToSelector:@selector(productsFilterdidReset)]) {
        [self.delegate productsFilterdidReset];
        [self.filterPopOverController dismissPopoverAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
