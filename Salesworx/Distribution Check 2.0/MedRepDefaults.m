//
//  MedRepDefaults.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/23/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "MedRepDefaults.h"

@implementation MedRepDefaults

+(NSString*)refineDateFormat:(NSString *)srcFormat destFormat:(NSString *)destFormat scrString:(NSString *)srcString
{
    
    /**
     cheat sheet
     
     Tuesday, Aug 9, 2016
     EEEE, MMM d, yyyy
     
     08/09/2016
     MM/dd/yyyy
     
     08-09-2016 06:09
     MM-dd-yyyy HH:mm
     
     Aug 9, 6:09 AM
     MMM d, H:mm a
     
     August 2016
     MMMM yyyy
     
     Aug 9, 2016
     MMM d, yyyy
     
     Tue, 9 Aug 2016 06:09:29 +0000
     E, d MMM yyyy HH:mm:ss Z
     
     2016-08-09T06:09:29+0000
     yyyy-MM-dd'T'HH:mm:ssZ
     
     09.08.16	
     dd.MM.yy
     
     **/
 
    srcString=[SWDefaults getValidStringValue:srcString];
    
    if(srcString.length==0)
    {
        return nil;
    }
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];

    [dateFormatter setDateFormat:srcFormat];
    NSDate* myDate = [dateFormatter dateFromString:srcString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone systemTimeZone];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr]];
    [formatter setDateFormat:destFormat];
    NSString *stringFromDate = [formatter stringFromDate:myDate];
    
    if (stringFromDate.length>0) {
        
        return stringFromDate;
    }
    
    else
    {
        return nil;
    }
}

+(NSString *)getDefaultStringForEmptyString:(NSString *)str
{
    if([[[SWDefaults getValidStringValue:str] trimString] length]==0)
        return KNotApplicable;
    else
        return str;
}

@end


