//
//  SWDistributionCheckViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/31/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SWDistributionCheckViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepDefaults.h"
#import "ShowImageViewController.h"
#import "SalesWorxDistributionItemHeaderTableViewCell.h"
#import "SalesWorxDistributionItemTableViewCell.h"
#import "SalesWorxPopOverViewController.h"
#import "SalesWorxDistributionItemNewTableViewCell.h"
#import "SalesWorxDistributionStockViewController.h"
#import "SalesWorxCustomClass.h"



@interface SWDistributionCheckViewController (){
    PNCircleChart *pieChart;
}

@end

@implementation SWDistributionCheckViewController

@synthesize customerDictornary;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Distribution Check"];
    self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStyleDone target:self action:@selector(handleBack:)];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    
    appControl =[AppControl retrieveSingleton];
    itemDataWithLocation = [[NSMutableDictionary alloc]init];

    NSArray *arri = [segAvailable subviews];
    
    // Change the tintColor of each subview within the array:
    
    [[arri objectAtIndex:0] setTintColor:[UIColor colorWithRed:16.0/255.0 green:184/255.0 blue:118/255.0 alpha:1.0]];
    
    [[arri objectAtIndex:1] setTintColor:KItemNotAvailable];
    
    NSArray *arriSegReorder = [segReorder subviews];
    
    // Change the tintColor of each subview within the array:
    
    [[arriSegReorder objectAtIndex:0] setTintColor:[UIColor colorWithRed:16.0/255.0 green:184/255.0 blue:118/255.0 alpha:1.0]];
    
    [[arriSegReorder objectAtIndex:1] setTintColor:KItemNotAvailable];

//    [_gridMainView addSubview:_itemTableView];

    indexPathArray=[[NSMutableArray alloc]init];
    _lblName.text = [customerDictornary valueForKey:@"Customer_Name"];
    _lblNumber.text = [customerDictornary valueForKey:@"Customer_No"];
    
    
    MSLItemsArray = [[SWDatabaseManager retrieveManager]fetchDistributionCheckItemsForcustomer];

    if ([appControl.FS_SHOW_DC_MIN_STOCK isEqualToString:KAppControlsYESCode]) {
        MSLMinStockOfItemsArray = [[SWDatabaseManager retrieveManager]fetchDCMinStockOfMSLItems];
    } else {
        minReqStockTitleLabel.hidden = YES;
        minReqStockLabel.hidden = YES;
    }
    
    fetchDistriButionCheckLocations=[[SWDatabaseManager retrieveManager]fetchDistriButionCheckLocations];

    selectedDistributionCheckLocation=[fetchDistriButionCheckLocations objectAtIndex:0];
    selectedDistributionCheckLocation.dcItemsArray = [NSMutableArray arrayWithArray: [[SWDatabaseManager retrieveManager] dbGetDistributionCollection]];
    selectedDistributionCheckLocation.dcItemsUnfilteredArray = [NSMutableArray arrayWithArray: [[SWDatabaseManager retrieveManager] dbGetDistributionCollection]];
    
    _txtDropdown.text = selectedDistributionCheckLocation.LocationName;
    
    NSLog(@"%@",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID);
    
    
    @try {
        NSMutableArray *lastDistributionCheckID = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Distribution_Check where Customer_ID = '%@' ORDER BY Checked_On DESC LIMIT 1",[customerDictornary valueForKey:@"Customer_ID"]]];
        
        NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where Inventory_Item_ID = '%@'",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID]];
        
        _lblLastVisitStock.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]]];
        _lblLastVisitOn.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd HH:mm:ss" destFormat:@"dd MMM, yyyy" scrString:[[lastDistributionCheckID objectAtIndex:0] valueForKey:@"Checked_On"]];
    } @catch (NSException *exception) {
        _lblLastVisitStock.text = @"N/A";
        _lblLastVisitOn.text = @"N/A";
    }
    
    
    //selectedDistributionCheckLocation.dcItemsArray = [NSMutableArray arrayWithArray:items];
    if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
        segAvailable.hidden = YES;
        
    } else {
        segAvailabelWithThreeOption.hidden = YES;
    }
   
    NSLog(@"items count is %lu", (unsigned long)items.count);
    
    
    //[self loadPieChart];
    
    _imgView.userInteractionEnabled = YES;
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)] ;
    
    if ([selectedDistributionCheckLocation.dcItemsArray count] >0)
    {
        [_itemTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
        [self tableView:_itemTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    }
    else
    {
        _txtDropdown.hidden = YES;
        _btnDropDown.hidden = YES;
        _itemTableView.hidden = YES;
        _detailView.hidden = YES;
        self.navigationItem.rightBarButtonItem.enabled = NO;
        [btnAdd setHidden:YES];

    }
    _itemTableView.tableFooterView = [UIView new];
    [_itemTableView reloadData];
    /****** check ENABLE_DIST_CHECK_MULTI_LOCATION is Y or N ******/
    
    appControl =[AppControl retrieveSingleton];
    
    _arrayOfLocation= [[NSMutableArray alloc]init];
    
    //appControl.ENABLE_DIST_CHECK_MULTI_LOCATION=@"N";
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOCATION isEqualToString:@"Y"])
    {
        NSLog(@"Yes");
        NSArray *arrOfLocations = [self fetchListValueOfLocations];
        for (int i = 0; i<[arrOfLocations count]; i++)
        {
            NSString *str = [self fetchListOfLocations:[arrOfLocations objectAtIndex:i]];
            [_arrayOfLocation addObject:str];
        }
    }
    else
    {
        _txtDropdown.hidden = YES;
        _btnDropDown.hidden = YES;
    }
    
    locationKey = @"S";
    if (![appControl.FS_SHOW_DC_FILTER isEqualToString:KAppControlsYESCode]) {
        filterButton.hidden = YES;
    }
    
    /************************************************************/
    /******* check ENABLE_DIST_CHECK_MULTI_LOTS is Y or N *******/
    /************************************************************/
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        btnAdd.backgroundColor = KDistributionCheckAvailabilityColor;
        [btnAdd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        NSArray *arri = [segAvailable subviews];
        [[arri objectAtIndex:0] setTintColor:KDistributionCheckUnAvailabilityColor];
        [[arri objectAtIndex:1] setTintColor:KDistributionCheckAvailabilityColor];
        _xProductDetailHeightConstraint.constant = 388;
        _xDetailViewBottomConstraint.constant = 8;
        _xAddButtonHeightConstraint.constant = 30;
    }
    else{
        [btnAdd setHidden:YES];
        _xProductDetailHeightConstraint.constant = 342;
        _xDetailViewBottomConstraint.constant = 0;
        _xAddButtonHeightConstraint.constant = 0;
    }
    if ([appControl.DEFAULT_AVAILABILITY_IN_DC isEqualToString:KAppControlsYESCode]) {
        for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
            DCItem.itemAvailability = @"Y";
            selectedDistributionCheckLocation.isLocationDCCompleted = YES;
            
            NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
            DistriButionCheckItemLot *distributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
            distributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
            distributionCheckItemLot.itemAvailability = @"Y";
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
            distributionCheckItemLot.reOrder = @"N";
            DCItem.reOrder = @"N";
            
            [DCItemLotsArray addObject:distributionCheckItemLot];
            DCItem.dcItemLots = DCItemLotsArray;
           
        }
        [_itemTableView reloadData];
        [self updatePieChart];
    }
    
    if ([appControl.FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
        xTopConstraintOfQunatityField.constant = 44;
        lblReorder.hidden = NO;
        segReorder.hidden = NO;
        segReorder.selectedSegmentIndex = 1;
    } else {
        
        lblReorder.hidden = YES;
        segReorder.hidden = YES;
        xTopConstraintOfQunatityField.constant = 8;
    }
    

    //round corner
    _itemTableView.tableFooterView = [UIView new];
    if (@available(iOS 15.0, *)) {
        self.itemTableView.sectionHeaderTopPadding = 0;
    }
    [_itemTableView reloadData];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    _imgView.layer.cornerRadius = 8.0f;
    _imgView.clipsToBounds = YES;
   
    segAvailable.selectedSegmentIndex = 0;
    if (self.isMovingToParentViewController) {
        [_itemTableView reloadData];
        [self loadPieChart];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Distribution Check  View"];
    
    if (flagRetake)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[distributionItem valueForKey:@"DistributionCheckLine_ID"]]];
            _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
        });
    }
    flagRetake = false;
}

- (void) handleBack:(id)sender
{
    if(anyUpdated)
    {
        UIAlertAction *yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController popViewControllerAnimated:YES];
            
            @try {
                for (int i=0; i<[items count]; i++)
                {
                    NSFileManager *fileManager = [NSFileManager defaultManager];
                    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                    
                    NSString *str = [[items objectAtIndex:i]valueForKey:@"DistributionCheckLine_ID"];
                    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",str]];
                    NSError *error;
                    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                    if (!success)
                    {
                        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                    }
                }
            }
            @catch (NSException *exception) {
                
            }
        }];
        
        UIAlertAction *noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
        }];
        
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Would you like to close the form without updating the distribution information?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
    }
    else {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
#pragma mark
-(void)disableFields
{
    _txtQuantity.text = @"";
    _txtLotNo.text = @"";
    _txtExpiryDate.text = @"";
    
    _txtQuantity.enabled = NO;
    _txtQuantity.alpha = 0.7;
    
    _txtLotNo.enabled = NO;
    _txtLotNo.alpha = 0.7;
    
    _txtExpiryDate.enabled = NO;
    _txtExpiryDate.alpha = 0.7;
    
    segReorder.enabled = NO;
    segReorder.alpha = 0.7;
    
    [_imgView removeGestureRecognizer:tap];
}
-(void)enableFields
{
    _txtQuantity.enabled = YES;
    _txtQuantity.alpha = 1.0;
    
    _txtLotNo.enabled = YES;
    _txtLotNo.alpha = 1.0;
    
    _txtExpiryDate.enabled = YES;
    _txtExpiryDate.alpha = 1.0;
    
    segReorder.enabled = YES;
    segReorder.alpha = 1.0;
    
    [_imgView addGestureRecognizer:tap];
}

- (IBAction)segmentReorderValueChanged:(id)sender {
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        selectedDistributionCheckItemLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
        
        if (segReorder.selectedSegmentIndex == 0) {
            selectedDistributionCheckItem.reOrder = @"Y";
            selectedDistributionCheckItemLot.reOrder = @"Y";
        } else {
            selectedDistributionCheckItem.reOrder = @"N";
            selectedDistributionCheckItemLot.reOrder = @"N";
        }
    }
}

#pragma mark Load PieChart
-(void)loadPieChart
{
    /* shouldAllowSave = YES;
     
     int updatedDistribution = 0;
     int nonUpdatedDistribution = 0;
     
     if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
     {
     @try {
     for (int i = 0; i<[items count]; i++)
     {
     if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]] != nil)
     {
     updatedDistribution++;
     anyUpdated = YES;
     } else {
     nonUpdatedDistribution++;
     shouldAllowSave = NO;
     }
     }
     }
     @catch (NSException *exception) {
     NSLog(@"%@",exception);
     }
     }
     else
     {
     @try {
     for (int i = 0; i<[items count]; i++) {
     if ([[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]]objectForKey:@"S"] valueForKey:@"Avl"] != nil || [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:i]]objectForKey:@"B"] valueForKey:@"Avl"] != nil)
     {
     updatedDistribution++;
     anyUpdated = YES;
     } else {
     nonUpdatedDistribution++;
     shouldAllowSave = NO;
     }
     }
     }
     @catch (NSException *exception) {
     NSLog(@"%@",exception);
     }
     }
     
     [pieChartNew removeFromSuperview];
     pieChartNew = [[PNCircleChart alloc] initWithFrame:CGRectMake(0, 0, 90, 90) total:[NSNumber numberWithInt:(updatedDistribution+nonUpdatedDistribution)] current:[NSNumber numberWithInt:updatedDistribution] clockwise:YES shadow:YES shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:YES lineWidth:@6.0f];
     
     
     // pieChartNew=[PNCircleChart alloc]initwit
     
     
     pieChartNew.backgroundColor = [UIColor clearColor];
     [pieChartNew setStrokeColor:[UIColor clearColor]];
     [pieChartNew setStrokeColorGradientStart:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
     [pieChartNew strokeChart];
     [checkCompletionPieChartContainerView addSubview:pieChartNew];
     
     if (shouldAllowSave)
     {
     allUpdated=YES;
     }
     if (nonUpdatedDistribution == 0) {
     selectedDistributionCheckLocation.isLocationDCCompleted = YES;
     }
     else{
     selectedDistributionCheckLocation.isLocationDCCompleted = NO;
     }*/
    
    int updatedDistribution = 0;
    int nonUpdatedDistribution = 0;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        if ([DCItem.dcItemLots count] > 0) {
            updatedDistribution++;
            anyUpdated = YES;
        }
        else
        {
            nonUpdatedDistribution++;
        }
    }
    [pieChart removeFromSuperview];
    pieChart = [[PNCircleChart alloc] initWithFrame:CGRectMake(0, 0, 66, 66) total:[NSNumber numberWithInt:(updatedDistribution+nonUpdatedDistribution)] current:[NSNumber numberWithInt:updatedDistribution] clockwise:YES shadow:YES shadowColor:[UIColor colorWithRed:(232.0f/255.0f) green:(243.0f/255.0f) blue:(246.0f/255.0f) alpha:1] check:YES lineWidth:@8.0f];
    
    pieChart.backgroundColor = [UIColor clearColor];
    [pieChart setStrokeColor:[UIColor clearColor]];
    [pieChart setStrokeColorGradientStart:[UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]];
    [pieChart strokeChart];
    [pieChartNew addSubview:pieChart];
    
    [pieChart updateChartByCurrent:[NSNumber numberWithInt:updatedDistribution]];
    
    if (nonUpdatedDistribution == 0) {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
    }
    else{
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
    }
}
-(void)updatePieChart
{
    int updatedDistribution = 0;
    int nonUpdatedDistribution = 0;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        if ([DCItem.dcItemLots count] > 0) {
            updatedDistribution++;
            anyUpdated = YES;
        }
        else
        {
            nonUpdatedDistribution++;
        }
    }
    [pieChart updateChartByCurrent:[NSNumber numberWithInt:updatedDistribution]];
    
    if (nonUpdatedDistribution == 0) {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
    }
    else{
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
    }
    
    
}
#pragma mark Filter

- (IBAction)filterButtonTapped:(id)sender {
    
    NSMutableArray *arrDCItems = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    productsArray = [[NSMutableArray alloc]init];
    
    for (DistriButionCheckItem *dcItem in arrDCItems) {
        [productsArray addObject:dcItem.DcProduct];
    }
    
    if (productsArray.count>0) {
        
        [self.view endEditing:YES];
        
        /*:::::::::::::::::::::::: Create Blurred View ::::::::::::::::::::::::::*/
        
        // Blurred with UIImage+ImageEffects
        blurredBgImage = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
        blurredBgImage.image=[UIImage imageNamed:@"BlurView"];
        [self.view addSubview:blurredBgImage];
        
        
        SalesWorxDCFilterViewController * popOverVC=[[SalesWorxDCFilterViewController alloc]init];
        popOverVC.delegate=self;
        
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }
        
        popOverVC.productsArray=productsArray;
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=NSLocalizedString(@"Filter", nil);
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        
        UIPopoverController *filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController=filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 380) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        
        
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        
        
        
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later"  withController:nil];
    }
}
#pragma mark Button Actions
- (IBAction)tabAvailable:(id)sender
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        tempDistributionItem = [[NSMutableDictionary alloc]init];
        tempDistributionItem = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:selectedRowIndex]];
        
        if (_tabAvailable.selectedSegmentIndex == 0)
        {
            //[tempDistributionItem setValue:@"Y" forKey:@"Avl"];
            [self enableFields];
        } else
        {
            //[tempDistributionItem setValue:@"N" forKey:@"Avl"];
            [self disableFields];
        }

        [tempDistributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        [tempDistributionItem setValue:nil forKey:@"Qty"];
        [tempDistributionItem setValue:nil forKey:@"LotNo"];
        [tempDistributionItem setValue:nil forKey:@"ExpDate"];
        [tempDistributionItem setValue:nil forKey:@"Image"];
    }
    else
    {
        if (_tabAvailable.selectedSegmentIndex == 0)
        {
            if ([distributionItem valueForKey:@"Image"] == nil)
            {
                [distributionItem setValue:@"N" forKey:@"Image"];
            }
            [distributionItem setValue:@"Y" forKey:@"Avl"];
            [self enableFields];
        } else
        {
            [distributionItem setValue:@"N" forKey:@"Avl"];
            [distributionItem setValue:@"" forKey:@"Qty"];
            [distributionItem setValue:@"" forKey:@"LotNo"];
            [distributionItem setValue:@"N" forKey:@"Image"];
            [distributionItem setValue:@"" forKey:@"ExpDate"];
            
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
            [self disableFields];
        }
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
}

- (IBAction)segmentValueChangedWithThreeOption:(id)sender{
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
        {
            [self disableFields];
            segReorder.selectedSegmentIndex = 0;
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
        }  else {
            [self enableFields];
            _txtQuantity.text = @"";
            _txtLotNo.text = @"";
            _txtExpiryDate.text = @"";
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                segReorder.enabled = NO;
                segReorder.alpha = 0.7;
                segReorder.selectedSegmentIndex = 0;
            }
            else {
                segReorder.selectedSegmentIndex = 1;
            }
        }
    }
    else
    {
        NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
        {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
            selectedDistributionCheckItem.imageName = nil;
            [self disableFields];
            
            segReorder.selectedSegmentIndex = 0;
            selectedDistributionCheckItemLot.reOrder = @"Y";
            selectedDistributionCheckItem.reOrder = @"Y";
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
        } else
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            [self enableFields];
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                segReorder.enabled = NO;
                segReorder.alpha = 0.7;
                segReorder.selectedSegmentIndex = 0;
                selectedDistributionCheckItemLot.reOrder = @"Y";
                selectedDistributionCheckItem.reOrder = @"Y";
            }
            else {
                segReorder.selectedSegmentIndex = 1;
                selectedDistributionCheckItem.reOrder = @"N";
                selectedDistributionCheckItemLot.reOrder = @"N";
            }
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                selectedDistributionCheckItemLot.itemAvailability = @"L";
            } else if (segAvailabelWithThreeOption.selectedSegmentIndex == 2) {
                selectedDistributionCheckItemLot.itemAvailability = @"H";
            }
        }
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        [_itemTableView reloadData];
        [self updatePieChart];
    }
}
- (IBAction)segmentValueChanged:(id)sender{
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        btnAdd.enabled = YES;
        btnAdd.alpha = 1.0;
        
        if (segAvailable.selectedSegmentIndex == 0)
        {
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
        } else
        {
            [self disableFields];
             segReorder.selectedSegmentIndex = 0;
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
        }
    }
    else
    {
        NSMutableArray *DCItemLotsArray=[[NSMutableArray alloc]init];
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        
        if (segAvailable.selectedSegmentIndex == 0)
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            [self enableFields];
            segReorder.selectedSegmentIndex = 1;
            selectedDistributionCheckItemLot.reOrder = @"N";
            selectedDistributionCheckItem.reOrder = @"N";
        } else
        {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
            selectedDistributionCheckItem.imageName = nil;
            
            [self disableFields];
            segReorder.selectedSegmentIndex = 0;
            selectedDistributionCheckItemLot.reOrder = @"Y";
            selectedDistributionCheckItem.reOrder = @"Y";
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
        }
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        [_itemTableView reloadData];
        [self updatePieChart];
    }
}
- (IBAction)btnAdd:(id)sender {
    
    isAddButtonPressed = YES;
    
    if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
        if (segAvailabelWithThreeOption.selectedSegmentIndex == 1 || segAvailabelWithThreeOption.selectedSegmentIndex == 2)
        {
            if (_txtQuantity.text.length == 0 && [appControl.MANDATORY_QTY_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Quantity is mandatory" withController:self];
            }
            else if (_txtExpiryDate.text.length == 0 && [appControl.MANDATORY_EXP_DT_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Expiry date is mandatory" withController:self];
            }
            else
            {
                [self addItems];
            }
        }
        else
        {
            [self addItems];
        }
        
    } else {
        if (segAvailable.selectedSegmentIndex == 0)
        {
            if (_txtQuantity.text.length == 0 && [appControl.MANDATORY_QTY_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Quantity is mandatory" withController:self];
            }
            else if (_txtExpiryDate.text.length == 0 && [appControl.MANDATORY_EXP_DT_IN_DC isEqualToString:KAppControlsYESCode]) {
                isAddButtonPressed = NO;
                [SWDefaults showAlertAfterHidingKeyBoard:KMissingData andMessage:@"Expire date is mandatory" withController:self];
            }
            
            else
            {
                [self addItems];
            }
        }
        else
        {
            [self addItems];
        }
    }
}
-(void)addItems
{
    BOOL duplicacy = NO;
    NSMutableArray * DCItemLotsArray;
    if ([selectedDistributionCheckItem.dcItemLots count] == 0)
    {
        DCItemLotsArray = [[NSMutableArray alloc]init];
    }
    else
    {
        for (DistriButionCheckItemLot *DCItemLot in selectedDistributionCheckItem.dcItemLots) {
            if([[DCItemLot.LotNumber localizedLowercaseString] isEqualToString:[_txtLotNo.text localizedLowercaseString]] && [DCItemLot.expiryDate isEqualToString:_txtExpiryDate.text])
            {
                duplicacy = YES;
                break;
            }
        }
        
        if (duplicacy)
        {
            [SWDefaults showAlertAfterHidingKeyBoard:KTitleStrAlert andMessage:@"Item is already updated for this Lot No" withController:self];
        }
        else
        {
            DCItemLotsArray = selectedDistributionCheckItem.dcItemLots;
        }
    }
    
    if (duplicacy == NO) {
        
        if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
            
            // three option segment
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 0)
            {
                selectedDistributionCheckItem.itemAvailability = @"N";
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                
                NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (!success)
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
                selectedDistributionCheckItem.imageName = nil;
                _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
                
                // remove all existing lots if item availability is N for any location
                DCItemLotsArray = [[NSMutableArray alloc]init];
            } else
            {
                selectedDistributionCheckItem.itemAvailability = @"Y";
                
                // check and delete lot if item availability is N for any location
                
                for (DistriButionCheckItemLot *DCitemLot in selectedDistributionCheckItem.dcItemLots) {
                    if ([DCitemLot.Quntity isEqualToString:@""] || DCitemLot.Quntity == nil) {
                        [selectedDistributionCheckItem.dcItemLots removeObject:DCitemLot];
                    }
                }
            }
        }
        else {
            if (segAvailable.selectedSegmentIndex == 0)
            {
                selectedDistributionCheckItem.itemAvailability = @"Y";
                
                
                // check and delete lot if item availability is N for any location
                
                for (DistriButionCheckItemLot *DCitemLot in selectedDistributionCheckItem.dcItemLots) {
                    if ([DCitemLot.Quntity isEqualToString:@""] || DCitemLot.Quntity == nil) {
                        [selectedDistributionCheckItem.dcItemLots removeObject:DCitemLot];
                    }
                }
                
            } else
            {
                selectedDistributionCheckItem.itemAvailability = @"N";
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
                
                NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
                NSError *error;
                BOOL success = [fileManager removeItemAtPath:filePath error:&error];
                if (!success)
                {
                    NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
                }
                selectedDistributionCheckItem.imageName = nil;
                _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
                
                // remove all existing lots if item availability is N for any location
                DCItemLotsArray = [[NSMutableArray alloc]init];
            }
        }
        
        selectedDistributionCheckItem.Quntity = _txtQuantity.text;
        selectedDistributionCheckItem.LotNumber = _txtLotNo.text;
        selectedDistributionCheckItem.expiryDate = _txtExpiryDate.text;
        
        if ([appControl.FS_SHOW_DC_RO isEqualToString:KAppControlsYESCode]) {
            if (segReorder.selectedSegmentIndex == 0) {
                selectedDistributionCheckItem.reOrder = @"Y";
            } else {
                selectedDistributionCheckItem.reOrder = @"N";
            }
        } else {
            selectedDistributionCheckItem.reOrder = @"";
        }
        
        
        selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
        selectedDistributionCheckItemLot.Quntity = _txtQuantity.text;
        selectedDistributionCheckItemLot.LotNumber = _txtLotNo.text;
        selectedDistributionCheckItemLot.expiryDate = _txtExpiryDate.text;
        selectedDistributionCheckItemLot.reOrder = selectedDistributionCheckItem.reOrder;
        
        if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
            
            if (segAvailabelWithThreeOption.selectedSegmentIndex == 0) {
                selectedDistributionCheckItemLot.itemAvailability = @"N";
            }
            else if (segAvailabelWithThreeOption.selectedSegmentIndex == 1) {
                selectedDistributionCheckItemLot.itemAvailability = @"L";
            } else if (segAvailabelWithThreeOption.selectedSegmentIndex == 2) {
                selectedDistributionCheckItemLot.itemAvailability = @"H";
            }
        }
        
        if (selectedDistributionCheckItem.imageName.length == 0) {
            selectedDistributionCheckItemLot.isImage = @"N";
        }
        else {
            selectedDistributionCheckItemLot.isImage = @"Y";
        }
        selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];
        [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
        
        selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
        
        
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        [_itemTableView reloadData];
        [self disableFields];
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
        
        [self loadPieChart];
    }
}

- (IBAction)btnDropDown:(id)sender
{
    [self.view endEditing:YES];
    SalesWorxPopOverViewController * popOverVC=[[SalesWorxPopOverViewController alloc]init];
    popOverVC.popOverContentArray = _arrayOfLocation;
    popOverVC.salesWorxPopOverControllerDelegate=self;
    popOverVC.disableSearch=YES;
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.popOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:_txtDropdown.dropdownImageView.frame inView:_txtDropdown.rightView permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (IBAction)btnExpiryDate:(id)sender
{
    [self.view endEditing:YES];
    
    popOverTitle=@"Check Date";
    NSLog(@"payment method tapped");
    
    SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
    popOverVC.didSelectDateDelegate=self;
    popOverVC.titleString = @"DistributionCheck";
    
    if (_txtExpiryDate.text == nil || [_txtExpiryDate.text isEqualToString:@""])
    {
        popOverVC.setMinimumDateCurrentDate=YES;
    } else {
        popOverVC.setMinimumDateCurrentDate=NO;
        popOverVC.previousSelectedDate = previousSelectedDate;
    }
    
    UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
    paymentPopOverController=nil;
    paymentPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    popOverVC.datePickerPopOverController=paymentPopOverController;
    
    [paymentPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:_txtExpiryDate.dropdownImageView.frame inView:_txtExpiryDate.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(void)didSelectDate:(NSString *)selectedDate
{
    [_txtExpiryDate setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        selectedDistributionCheckItem.expiryDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        selectedDistributionCheckItemLot.expiryDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [_itemTableView reloadData];
    }
}

#pragma mark UITableView Data source Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return items.count;
    return selectedDistributionCheckLocation.dcItemsArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
- ( UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        
        static NSString* identifier=@"cellItemNew";
        SalesWorxDistributionItemNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell=nil;
        
        if(cell == nil) {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemNewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
        cell.lblCode.text = @"Code";
        cell.lblDescription.text = @"Description";
        cell.lblQuantity.text = @"Qty";
        
        cell.lblCode.textColor = TableViewHeaderSectionColor;
        cell.lblDescription.textColor = TableViewHeaderSectionColor;
        cell.lblQuantity.textColor = TableViewHeaderSectionColor;
        cell.backgroundColor = [UIColor redColor];
        return cell;
        
    }
    else
    {
        static NSString* identifier=@"cellItem";

        SalesWorxDistributionItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
        
        cell.lblCode.text = @"Code";
        cell.lblDescription.text = @"Description";
        cell.lblQuantity.text = @"Qty";
        cell.lblExpiry.text = @"Expiry";

        cell.lblCode.textColor = TableViewHeaderSectionColor;
        cell.lblDescription.textColor = TableViewHeaderSectionColor;
        cell.lblQuantity.textColor = TableViewHeaderSectionColor;
        cell.lblExpiry.textColor = TableViewHeaderSectionColor;

        return cell;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        static NSString* identifier=@"cellItemNew";
        SalesWorxDistributionItemNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemNewTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
            cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
            cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
        }
        else
        {
            cell.lblCode.textColor = UITableViewCellTextColor;
            cell.lblDescription.textColor = UITableViewCellTextColor;
            cell.lblQuantity.textColor = UITableViewCellTextColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        
        
        DistriButionCheckItem *DCItem= [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        cell.lblCode.text = DCItem.DcProduct.Item_Code;
        cell.lblDescription.text = DCItem.DcProduct.Description;
        cell.lblQuantity.text = DCItem.Quntity;
        
        if ([DCItem.itemAvailability isEqualToString:@"Y"]) {
            cell.statusView.backgroundColor = KItemAvailable;
            @try {
                if(DCItem.dcItemLots!=nil && DCItem.dcItemLots.count>0)
                {
                    NSArray *qtyArray=[DCItem.dcItemLots valueForKey:@"Quntity"];
                    NSInteger Qty=0;
                    for (NSString *x in qtyArray)
                    {
                        Qty += [x integerValue];
                    }
                    cell.lblQuantity.text=[NSString stringWithFormat:@"%ld",(long)Qty ];
                }
            }
            @catch (NSException *exception) {
                cell.lblQuantity.text = DCItem.Quntity;
            }
            
        } else if ([DCItem.itemAvailability isEqualToString:@"N"]) {
            cell.statusView.backgroundColor = KItemNotAvailable;
            cell.lblQuantity.text=@"";
        } else {
            cell.statusView.backgroundColor = KNoChange;
            cell.lblQuantity.text=@"";
            
        }
        
        return cell;
        
    }
    else
    {
        static NSString* identifier=@"cellItem";
        SalesWorxDistributionItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionItemTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        if ([indexPathArray containsObject:indexPath])
        {
            cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
            cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
            cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
            cell.lblExpiry.textColor = MedRepDescriptionLabelFontColor;
            [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        }
        else
        {
            cell.lblCode.textColor = UITableViewCellTextColor;
            cell.lblDescription.textColor = UITableViewCellTextColor;
            cell.lblQuantity.textColor = UITableViewCellTextColor;
            cell.lblExpiry.textColor = UITableViewCellTextColor;
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
        }
        
        DistriButionCheckItem *DCItem= [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        cell.lblCode.text = DCItem.DcProduct.Item_Code;
        cell.lblDescription.text = DCItem.DcProduct.Description;
        if ([DCItem.itemAvailability isEqualToString:@"Y"]) {
            cell.statusView.backgroundColor = KItemAvailable;
        } else if ([DCItem.itemAvailability isEqualToString:@"N"]) {
            cell.statusView.backgroundColor = KItemNotAvailable;
        } else {
            cell.statusView.backgroundColor = KNoChange;
        }
        
        cell.lblQuantity.text = DCItem.Quntity;
        cell.lblExpiry.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:DCItem.expiryDate];
        return cell;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"More" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                //insert your editAction here
                                                @try {
                                                    [self editTableRow:indexPath];
                                                }
                                                @catch (NSException *exception) {
                                                    NSLog(@"%@",exception);
                                                }
                                                
                                            }];
        editAction.backgroundColor = [UIColor redColor];
        return @[editAction];
    }
    return nil;
}
-(void)editTableRow:(NSIndexPath *)indexPath
{
    editIndexPath = indexPath;
    [self.view endEditing:YES];
    SalesWorxDistributionStockViewController *salesOrderStockInfoViewController=[[SalesWorxDistributionStockViewController alloc]initWithNibName:@"SalesWorxDistributionStockViewController" bundle:[NSBundle mainBundle]];
    salesOrderStockInfoViewController.view.backgroundColor = [UIColor clearColor];
    salesOrderStockInfoViewController.modalPresentationStyle = UIModalPresentationCustom;
    salesOrderStockInfoViewController.Delegate = self;
    salesOrderStockInfoViewController.strCode = [[items objectAtIndex:indexPath.row] valueForKey:@"Item_Code"];
    salesOrderStockInfoViewController.strDescription = [[items objectAtIndex:indexPath.row] valueForKey:@"Description"];
    salesOrderStockInfoViewController.selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];//[itemDataWithLocation objectForKey:[NSNumber numberWithInteger:indexPath.row]];
    [self.navigationController presentViewController:salesOrderStockInfoViewController animated:NO completion:nil];
}
-(void)updateStockValue:(NSMutableArray *)updatedStockArray
{
    selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:editIndexPath.row];
    
    if ([selectedDistributionCheckItem.dcItemLots count] == 0) {
        
        selectedDistributionCheckLocation.isLocationDCCompleted = NO;
        
        selectedDistributionCheckItem.itemAvailability = nil;
        selectedDistributionCheckItem.Quntity = nil;
        selectedDistributionCheckItem.LotNumber = nil;
        selectedDistributionCheckItem.expiryDate = nil;
    }
    else
    {
        DistriButionCheckItemLot *DcItemLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
        
        if (DcItemLot.Quntity.length == 0) {
            selectedDistributionCheckItem.itemAvailability = @"N";
            selectedDistributionCheckItem.Quntity = nil;
            selectedDistributionCheckItem.LotNumber = nil;
            selectedDistributionCheckItem.expiryDate = nil;
        }
        else
        {
            selectedDistributionCheckItem.itemAvailability = @"Y";
            selectedDistributionCheckItem.Quntity = DcItemLot.Quntity;
            selectedDistributionCheckItem.LotNumber = DcItemLot.LotNumber;
            selectedDistributionCheckItem.expiryDate = DcItemLot.expiryDate;
        }
    }
    
    [_itemTableView reloadData];
}

#pragma mark UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if (indexPathArray.count==0)
    {
        if (indexPath==nil)
        {
            [indexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        else
        {
            [indexPathArray addObject:indexPath];
        }
    }
    else
    {
        [indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
    {
        if ((![selectedDistributionCheckItem.itemAvailability isEqualToString:@"Y"]) && selectedDistributionCheckItem.imageName.length > 0) {
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *dataPath = [documentsPath stringByAppendingPathComponent:@"/Distribution check images"];
            
            NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
            NSError *error;
            BOOL success = [fileManager removeItemAtPath:filePath error:&error];
            if (!success)
            {
                NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
            }
            selectedDistributionCheckItem.imageName = nil;
        }
        
        SalesWorxDistributionItemNewTableViewCell *cell = (SalesWorxDistributionItemNewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
        cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
        cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        [self disableFields];
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
        
        _txtQuantity.text = nil;
        _txtLotNo.text = nil;
        _txtExpiryDate.text = nil;
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = (SalesWorxDistributionItemTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = MedRepDescriptionLabelFontColor;
        cell.lblDescription.textColor = MedRepDescriptionLabelFontColor;
        cell.lblQuantity.textColor = MedRepDescriptionLabelFontColor;
        cell.lblExpiry.textColor = MedRepDescriptionLabelFontColor;
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        selectedDistributionCheckItem = [selectedDistributionCheckLocation.dcItemsArray objectAtIndex:indexPath.row];
        
        if ([selectedDistributionCheckItem.itemAvailability isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
            [self enableFields];
            
            if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                DistriButionCheckItemLot *selectedLot = [selectedDistributionCheckItem.dcItemLots objectAtIndex:0];
                if ([selectedLot.itemAvailability isEqualToString:@"L"]) {
                    segAvailabelWithThreeOption.selectedSegmentIndex = 1;
                    segReorder.enabled = NO;
                    segReorder.alpha = 0.7;
                } else {
                    segAvailabelWithThreeOption.selectedSegmentIndex = 2;
                }
            } else {
                segAvailable.selectedSegmentIndex = 0;
            }
        } else if ([selectedDistributionCheckItem.itemAvailability isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
            [self disableFields];
            
            if ([appControl.FS_SHOW_DC_AVBL_SEG isEqualToString:KAppControlsYESCode]) {
                segAvailabelWithThreeOption.selectedSegmentIndex = 0;
            } else {
                segAvailable.selectedSegmentIndex = 1;
            }
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
            segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
           {
                segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
                [self disableFields];
            }
            
            else{
                NSMutableArray * DCItemLotsArray=[[NSMutableArray alloc]init];
                selectedDistributionCheckItemLot = [[DistriButionCheckItemLot alloc]init];
                selectedDistributionCheckItemLot.DistriButionCheckItemLotId = [NSString createGuid];

                selectedDistributionCheckItem.itemAvailability = @"Y";
                [self enableFields];
                segReorder.selectedSegmentIndex = 1;
                selectedDistributionCheckItemLot.reOrder = @"N";
                selectedDistributionCheckItem.reOrder = @"N";
                segAvailable.selectedSegmentIndex = 0;
                [DCItemLotsArray addObject:selectedDistributionCheckItemLot];
                selectedDistributionCheckItem.dcItemLots = DCItemLotsArray;
                [_itemTableView reloadData];
                [self updatePieChart];
           }
        }
        
        if ([selectedDistributionCheckItem.reOrder isEqualToString:@"Y"]) {
            segReorder.selectedSegmentIndex = 0;
        } else {
            segReorder.selectedSegmentIndex = 1;
        }
        
        _txtDropdown.text = selectedDistributionCheckLocation.LocationName;
        _txtQuantity.text = selectedDistributionCheckItem.Quntity;
        _txtLotNo.text = selectedDistributionCheckItem.LotNumber;
        _txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:kDateFormatWithoutTime scrString:selectedDistributionCheckItem.expiryDate];
    }
    
    if (selectedDistributionCheckItem.imageName.length == 0) {
        _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
        
        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
        UIImage *DcImage = [UIImage imageWithContentsOfFile:savedImagePath];
        if (DcImage) {
            _imgView.image = DcImage;
        }
        else {
            _imgView.image = [UIImage imageNamed:@"DC_Product_Icon"];
        }
    }
    
    if ([appControl.FS_SHOW_DC_MIN_STOCK isEqualToString:KAppControlsYESCode]) {
        [self setMinStockValueOfMSLItems];
    }
    NSLog(@"%@",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID);
    
    NSMutableArray *arrlastStock = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select sum(Qty) as totalStock from TBL_Distribution_Check_Items where Inventory_Item_ID = '%@'",selectedDistributionCheckItem.DcProduct.Inventory_Item_ID]];
    
    _lblLastVisitStock.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[[arrlastStock  objectAtIndex:0] valueForKey:@"totalStock"]]];
    
    NSMutableArray *arrlastAvailabiility;
    NSMutableArray *lastDistributionCheckID = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select * from TBL_Distribution_Check where Customer_ID = '%@' ORDER BY Checked_On DESC LIMIT 1",[customerDictornary valueForKey:@"Customer_ID"]]];
    if(lastDistributionCheckID.count > 0){
    arrlastAvailabiility = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Is_Available as Is_Available from TBL_Distribution_Check_Items where DistributionCheck_ID = '%@' and Inventory_Item_ID = '%@'",[[lastDistributionCheckID objectAtIndex:0]valueForKey:@"DistributionCheck_ID"] ,selectedDistributionCheckItem.DcProduct.Inventory_Item_ID]];
    }
    if(arrlastAvailabiility.count > 0){
    NSString *lblString =  [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[[arrlastAvailabiility  objectAtIndex:0] valueForKey:@"Is_Available"]]];
        if([lblString isEqualToString:@"Y"]){
        lastVisitAvailability.text = @"Yes";
    }else{
        lastVisitAvailability.text = @"No";
    }
    }
    [tableView reloadData];
    /*[self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    
    [UIView setAnimationsEnabled:YES];
    if (indexPathArray.count==0)
    {
        if (indexPath==nil)
        {
            [indexPathArray addObject:[NSIndexPath indexPathForRow:0 inSection:0]];
        }
        else
        {
            [indexPathArray addObject:indexPath];
        }
    }
    else
    {
        [indexPathArray replaceObjectAtIndex:0 withObject:indexPath];
    }
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        SalesWorxDistributionItemNewTableViewCell *cell = (SalesWorxDistributionItemNewTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        tempDistributionItem = [NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:indexPath.row]];
        
        if (!tempDistributionItem[@"DistributionCheckLine_ID"])
        {
            [tempDistributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        }
        selectedIndex = indexPath;
        selectedRowIndex = (int) indexPath.row;

        [self disableFields];
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;

        _txtDropdown.text = @"Shelf";
        
        _txtQuantity.text = nil;
        _txtLotNo.text = nil;
        _txtExpiryDate.text = nil;
        _imgView.image = [UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png"];
    }
    else
    {
        SalesWorxDistributionItemTableViewCell *cell = (SalesWorxDistributionItemTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.lblCode.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        cell.lblQuantity.textColor = [UIColor whiteColor];
        cell.lblExpiry.textColor = [UIColor whiteColor];
        [cell.contentView setBackgroundColor:MedRepUITableviewSelectedCellBackgroundColor];
        
        distributionItem=[NSMutableDictionary dictionaryWithDictionary:[items objectAtIndex:indexPath.row]];
        
        if (!distributionItem[@"DistributionCheckLine_ID"])
        {
            [distributionItem setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
        }
        selectedIndex = indexPath;
        selectedRowIndex = (int)indexPath.row;
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"Y"])
        {
            cell.statusView.backgroundColor = KItemAvailable;
            [self enableFields];
            _tabAvailable.selectedSegmentIndex = 0;
        } else if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Avl"] isEqualToString:@"N"])
        {
            cell.statusView.backgroundColor = KItemNotAvailable;
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = 1;
        }
        else
        {
            cell.statusView.backgroundColor = KNoChange;
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Loc"] isEqualToString:@"B"]) {
            _txtDropdown.text = @"Back Room";
        } else {
            _txtDropdown.text = @"Shelf";
        }
        
        _txtQuantity.text = [[items objectAtIndex:indexPath.row] valueForKey:@"Qty"];
        _txtLotNo.text = [[items objectAtIndex:indexPath.row] valueForKey:@"LotNo"];
        _txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"MMM dd, yyyy" scrString:[[items objectAtIndex:indexPath.row] valueForKey:@"ExpDate"]];
        
        
        if ([[[items objectAtIndex:indexPath.row] valueForKey:@"Image"] isEqualToString:@"Y"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *key;
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    key = @"S";
                } else {
                    key = @"B";
                }
                @try {
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
                    NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:key] valueForKey:@"DistributionCheckLine_ID"]]];
                    _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
                }
                @catch (NSException *exception)
                {
                    _imgView.image = [UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png"];
                    NSLog(@"%@",exception);
                }
            });
        } else
        {
            _imgView.image = [UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png"];
        }
    }*/
}
-(void)setMinStockValueOfMSLItems
{
    Products *selectedProduct = selectedDistributionCheckItem.DcProduct;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.InventoryItemID == [cd] %@ AND SELF.organizationID == [cd] %@",[SWDefaults getValidStringValue:selectedProduct.Inventory_Item_ID],[SWDefaults getValidStringValue:selectedProduct.OrgID]];
    NSLog(@"template predicate is %@", predicate);
    NSMutableArray *filteredCategoriesArray = [[MSLMinStockOfItemsArray filteredArrayUsingPredicate:predicate] mutableCopy];
    
    if (filteredCategoriesArray.count > 0) {
        
        DistriButionCheckMinStock *minStock = [filteredCategoriesArray objectAtIndex:0];
        minReqStockLabel.text = [SWDefaults getValidStringValue:minStock.attributeValue];
        
    } else {
        minReqStockLabel.text = @"N/A";
    }
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        [self itemUpdate:tempDistributionItem];
    }
    else
    {
        [self itemUpdate:distributionItem];
    }
}

#pragma mark update item details
-(void)itemUpdate:(NSMutableDictionary *)item
{
    if (item)
    {
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [_itemTableView reloadData];
            if ([item valueForKey:@"Avl"] != nil)
            {
                if (![item valueForKey:@"Loc"])
                {
                    if ([_txtDropdown.text isEqualToString:@"Shelf"])
                    {
                        [item setValue:@"S" forKey:@"Loc"];
                    } else {
                        [item setValue:@"B" forKey:@"Loc"];
                    }
                }
                
                NSMutableArray *array = [NSMutableArray arrayWithArray:items];
                [array replaceObjectAtIndex:selectedRowIndex withObject:item];
                
                items = array;
                [_itemTableView reloadData];
                
                NSMutableDictionary *firstOne = [NSMutableDictionary dictionary];
                [firstOne setObject:item forKey:[item valueForKey:@"Loc"]];
                
                
                if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]])
                {
                    if (isAddButtonPressed)
                    {
                        stockArray = [itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]];
                        
                        [stockArray addObject:item];
                        [itemDataWithLocation setObject:stockArray forKey:[NSNumber numberWithInt:selectedRowIndex]];
                    }
                } else
                {
                    if (isAddButtonPressed)
                    {
                        stockArray  = [[NSMutableArray alloc]init];
                        [stockArray addObject:item];
                        [itemDataWithLocation setObject:stockArray forKey:[NSNumber numberWithInt:selectedRowIndex]];
//                        [self loadPieChart];
                    }
                }
                isAddButtonPressed = false;
            }
        }
        else
        {
            if (![item valueForKey:@"Loc"])
            {
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    [item setValue:@"S" forKey:@"Loc"];
                } else {
                    [item setValue:@"B" forKey:@"Loc"];
                }
            }

            if (([item valueForKey:@"Avl"] == nil) && ((_txtQuantity.text.length >0) || (_txtLotNo.text.length >0) || (_txtExpiryDate.text.length >0)))
            {
                [item setValue:@"Y" forKey:@"Avl"];
            }
            
            NSMutableArray *array = [NSMutableArray arrayWithArray:items];
            [array replaceObjectAtIndex:selectedRowIndex withObject:item];
            
            items = array;
            [_itemTableView reloadData];
            
            NSMutableDictionary *firstOne = [NSMutableDictionary dictionary];
            [firstOne setObject:item forKey:[item valueForKey:@"Loc"]];
            
            if ([item valueForKey:@"Avl"] != nil)
            {
                if ([itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]])
                {
                    [[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]] setObject:item forKey:[item valueForKey:@"Loc"]];
                } else
                {
                    [itemDataWithLocation setObject:firstOne forKey:[NSNumber numberWithInt:selectedRowIndex]];
//                    [self loadPieChart];
                }
            }
        }
    }
}


#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    if(textField==_txtDropdown)
    {
        [self btnDropDown:textField];
        return NO;
    }
    if(textField==_txtExpiryDate)
    {
        [self btnExpiryDate:textField];
        return NO;
    }

    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtQuantity)
    {
        [textField resignFirstResponder];
        [_txtLotNo becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"N"])
    {
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }
    return YES;
}

#define MAX_LENGTH 100
#define QTY_MAX_LENTH 6
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //OLA! Allow only positive numerical input for quantity
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == _txtQuantity)
    {
        NSString *allowedCharacters;
        if ([textField.text length]>0)//no decimal point yet, hence allow point
            allowedCharacters = @"0123456789";
        else
            allowedCharacters = @"123456789";//first input character cannot be '0'
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [tempDistributionItem setValue:newText forKey:@"Qty"];
        }
        else
        {
            selectedDistributionCheckItemLot.Quntity = newText;
            //[distributionItem setValue:newText forKey:@"Qty"];
        }
        return (newString.length<=QTY_MAX_LENTH);
    }
    else if (textField == _txtLotNo)
    {
        /*NSString *allowedCharacters;
        if ([textField.text length]>0)//no decimal point yet, hence allow point
            allowedCharacters = @"0123456789.";
        else
            allowedCharacters = @"123456789";//first input character cannot be '0'
        
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet characterSetWithCharactersInString:allowedCharacters] invertedSet]].location != NSNotFound)
        {
            return NO;
        }*/
        NSString *newText = [textField.text stringByReplacingCharactersInRange: range withString:string];

        
        if( [newText length]<= MAX_LENGTH ){
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
            {
                [tempDistributionItem setValue:newText forKey:@"LotNo"];
            }
            else
            {
                [distributionItem setValue:newText forKey:@"LotNo"];
            }
            return YES;
        }
        // case where text length > MAX_LENGTH
        textField.text = [newText substringToIndex: MAX_LENGTH ];
        if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
        {
            [tempDistributionItem setValue:textField.text forKey:@"LotNo"];
        }
        else
        {
            [distributionItem setValue:textField.text forKey:@"LotNo"];
        }
        return NO;
    }
    return YES;
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 220; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
    //[self.view setFrame:CGRectMake(0, -50, 1024, 768)];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
   // [self.view setFrame:CGRectMake(0, 65, 1024, 768)];
    
   /* [self animateTextField:textField up:NO];

    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        if (textField == _txtQuantity)
        {
            [tempDistributionItem setValue:textField.text forKey:@"Qty"];
        } else {
            [tempDistributionItem setValue:textField.text forKey:@"LotNo"];
        }
    }
    else
    {
        if (textField == _txtQuantity)
        {
            [distributionItem setValue:textField.text forKey:@"Qty"];
        } else {
            [distributionItem setValue:textField.text forKey:@"LotNo"];
        }
        [self checkImage];
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }*/
    [self animateTextField:textField up:NO];
    
    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsNOCode])
    {
        if (textField == _txtQuantity)
        {
            selectedDistributionCheckItem.Quntity = textField.text;
            selectedDistributionCheckItemLot.Quntity = textField.text;
        } else {
            selectedDistributionCheckItem.LotNumber = textField.text;
            selectedDistributionCheckItemLot.LotNumber = textField.text;
        }
        [_itemTableView reloadData];
    }
}

#pragma mark
- (void)save:(id)sender
{
    [self.view endEditing:YES];
    allUpdated = NO;
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(@"Distribution information saved successfully", nil) withController:self];
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController  popViewControllerAnimated:YES];
            });
            Singleton *single = [Singleton retrieveSingleton];
            single.distributionRef = [[SWDatabaseManager retrieveManager] saveDistributionCheckWithCustomerInfo:customerDictornary andDistChectItemInfo:fetchDistriButionCheckLocations];
            single.isDistributionChecked = YES;
            single.isDistributionItemGet = NO;
            
            NSDictionary *visitOptionDict=[[NSDictionary alloc]initWithObjectsAndKeys:kDistributionCheckTitle,KVisitOtionNotification_DictionaryKeyStr, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:KVisitOtionCompletionNotificationNameStr
                                                                object:self
                                                              userInfo:visitOptionDict];
        });
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:NSLocalizedString(@"Kindly complete all distribution check", nil) withController:self];
    }
}

#pragma mark UIPopOverDelegate
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC {
    popoverController=nil;
    selectedRowIndex = -1;
}
-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}
-(void)productsFilterDidClose
{
    [blurredBgImage removeFromSuperview];
}
-(void)filteredProducts:(NSMutableArray*)filteredArray
{
    [blurredBgImage removeFromSuperview];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_Active"] forState:UIControlStateNormal];
    [self updateProductTableViewOnFilter:filteredArray];
}
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict
{
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    previousFilteredParameters=parametersDict;
}
-(void)updateProductTableViewOnFilter:(NSMutableArray *)filteredProductsArray
{
    NSMutableArray *arrDCItems = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    NSMutableArray *filteredMSLArray = [[NSMutableArray alloc]init];
    
    for (Products *selectedProduct in filteredProductsArray) {
        [filteredMSLArray addObject:[[[arrDCItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(DcProduct == %@)", selectedProduct]]mutableCopy] objectAtIndex:0]];
    }
    
    selectedDistributionCheckLocation.dcItemsArray = filteredMSLArray;
   // items = filteredMSLArray;
    [_itemTableView reloadData];

    [self updatePieChart];
}
-(void)productsFilterdidReset
{
    [blurredBgImage removeFromSuperview];
    previousFilteredParameters=[[NSMutableDictionary alloc]init];
    [filterButton setBackgroundImage:[UIImage imageNamed:@"FilterIcon_InActive"] forState:UIControlStateNormal];
    
    selectedDistributionCheckLocation.dcItemsArray = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
   // items = selectedDistributionCheckLocation.dcItemsUnfilteredArray;
    [_itemTableView reloadData];
    
    [_itemTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [_itemTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    [self tableView:_itemTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self updatePieChart];
}


-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
   /* if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
    {
        _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
    }
    else
    {
        @try {
            if (![_txtDropdown.text isEqualToString:[_arrayOfLocation objectAtIndex:selectedIndexPath.row]])
            {
                _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
                
                if ([_txtDropdown.text isEqualToString:@"Shelf"])
                {
                    locationKey = @"S";
                } else {
                    locationKey = @"B";
                }
                
                if ([[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey])
                {
                    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                    [mutDict setDictionary:distributionItem];
                    distributionItem = nil;
                    distributionItem = mutDict;
                    
                    if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Image"] isEqualToString:@"Y"])
                    {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
                        NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"DistributionCheckLine_ID"]]];
                        _imgView.image = [UIImage imageWithContentsOfFile:savedImagePath];
                        [distributionItem setValue:@"Y" forKey:@"Image"];
                    } else
                    {
                        _imgView.image = [UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png"];
                        [distributionItem setValue:@"N" forKey:@"Image"];
                    }
                    
                    if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Avl"] isEqualToString:@"Y"])
                    {
                        _tabAvailable.selectedSegmentIndex = 0;
                        [self enableFields];
                        [distributionItem setValue:@"Y" forKey:@"Avl"];
                        
                    } else if ([[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Avl"] isEqualToString:@"N"])
                    {
                        _tabAvailable.selectedSegmentIndex = 1;
                        [self disableFields];
                        [distributionItem setValue:@"N" forKey:@"Avl"];
                    }
                    else
                    {
                        [distributionItem setValue:@"" forKey:@"Image"];
                        _tabAvailable.selectedSegmentIndex = -1;
                        [self disableFields];
                        [distributionItem setValue:nil forKey:@"Avl"];
                    }
                    _txtQuantity.text = [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"Qty"];
                    _txtLotNo.text = [[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"LotNo"];
                    _txtExpiryDate.text = [MedRepDefaults refineDateFormat:@"yyyy-MM-dd" destFormat:@"MMM dd, yyyy" scrString:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"ExpDate"]];
                    
                    [distributionItem setValue:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"DistributionCheckLine_ID"] forKey:@"DistributionCheckLine_ID"];
                    [distributionItem setValue:_txtQuantity.text forKey:@"Qty"];
                    [distributionItem setValue:_txtLotNo.text forKey:@"LotNo"];
                    [distributionItem setValue:[[[itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]]objectForKey:locationKey] valueForKey:@"ExpDate"] forKey:@"ExpDate"];
                    
                }
                else
                {
                    NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                    [mutDict setDictionary:distributionItem];
                    [mutDict setValue:[NSString createGuid] forKey:@"DistributionCheckLine_ID"];
                    distributionItem = nil;
                    distributionItem = mutDict;
                    
                    _tabAvailable.selectedSegmentIndex = -1;
                    [self disableFields];
                    _txtQuantity.text = @"";
                    _txtLotNo.text = @"";
                    _txtExpiryDate.text = @"";
                    [distributionItem setValue:nil forKey:@"Qty"];
                    [distributionItem setValue:nil forKey:@"LotNo"];
                    [distributionItem setValue:nil forKey:@"ExpDate"];
                    [distributionItem setValue:nil forKey:@"Image"];
                    [distributionItem setValue:nil forKey:@"Avl"];
                    _imgView.image = [UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png" ];
                }
                
                NSMutableDictionary *mutDict = [NSMutableDictionary dictionary];
                [mutDict setDictionary:distributionItem];
                [mutDict setValue:locationKey forKey:@"Loc"];
                distributionItem = nil;
                distributionItem = mutDict;
                //[self itemUpdate:distributionItem];
            }
            else
            {
                _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
            }
        }
        @catch (NSException *exception) {
            _txtDropdown.text = [_arrayOfLocation objectAtIndex:selectedIndexPath.row];
        }
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
    }*/
    [self productsFilterdidReset];
    
    BOOL isAllDCItemUpdated = YES;
    BOOL isAnyDCItemUpdated = NO;
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        
        if ([DCItem.dcItemLots count] == 0) {
            isAllDCItemUpdated = NO;
            break;
        }
    }
    
    for (DistriButionCheckItem *DCItem in selectedDistributionCheckLocation.dcItemsArray) {
        
        if ([DCItem.dcItemLots count] > 0) {
            isAnyDCItemUpdated = YES;
            break;
        }
    }
    
    if (isAllDCItemUpdated)
    {
        selectedDistributionCheckLocation.isLocationDCCompleted = YES;
        [self switchLocation:selectedIndexPath];
    } else if(isAnyDCItemUpdated){
        
        UIAlertAction* yesAction = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        selectedDistributionCheckLocation.dcItemsArray = nil;
                                        [self switchLocation:selectedIndexPath];
                                    }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       
                                   }];
        NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
        [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:KWarningAlertTitleStr andMessage:@"Distribution check entries in current location are incomplete. Switching the location will delete the changes made in this location . \n Would you like to discard them?" andActions:actionsArray withController:self];
    }
    else{
        [self switchLocation:selectedIndexPath];
    }
}
-(void)switchLocation:(NSIndexPath*)selectedIndexPath
{
    selectedDistributionCheckLocation = [fetchDistriButionCheckLocations objectAtIndex:selectedIndexPath.row];
    _txtDropdown.text = selectedDistributionCheckLocation.LocationName;
    
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        NSLog(@"Location completed");
    }
    else
    {
        selectedDistributionCheckLocation.dcItemsArray = [NSMutableArray arrayWithArray: [[SWDatabaseManager retrieveManager] dbGetDistributionCollection]];
        selectedDistributionCheckLocation.dcItemsUnfilteredArray = [NSMutableArray arrayWithArray: [[SWDatabaseManager retrieveManager] dbGetDistributionCollection]];
        
        [self disableFields];
        segAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        segAvailabelWithThreeOption.selectedSegmentIndex = UISegmentedControlNoSegment;
        segReorder.selectedSegmentIndex = 1;
    }
    
    [_itemTableView reloadData];
    
    [_itemTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [_itemTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:0];
    [self tableView:_itemTableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    [self updatePieChart];
    
    if (selectedDistributionCheckLocation.isLocationDCCompleted) {
        [_imgView removeGestureRecognizer:tap];
    }
    else
    {
        [_imgView addGestureRecognizer:tap];
    }
}
#pragma mark
-(NSMutableArray*)fetchListValueOfLocations
{
    NSString* specilizationQry=[NSString stringWithFormat:@"select IFNULL(code_Value,'N/A') AS Dist_Check_Locations from TBL_App_Codes  where code_type ='DIST_CHECK_LOCATIONS'"];
    NSMutableArray* contentArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:specilizationQry];
    if (contentArray.count>0)
    {
        NSLog(@"Dist Check Locations  are %@", [contentArray description]);
        NSMutableArray* tempArry=[[NSMutableArray alloc]init];
        for (NSInteger i=0; i<contentArray.count; i++)
        {
            [tempArry addObject:[[contentArray valueForKey:@"Dist_Check_Locations"] objectAtIndex:i]];
        }
        return tempArry;
    }
    {
        return 0;
    }
}
-(NSString*)fetchListOfLocations:(NSString*)productType
{
    NSString* appCodeDesc;
    
    NSString* appCodeQry=[NSString stringWithFormat:@"select  Code_Description from TBL_App_Codes  where Code_Type='DIST_CHECK_LOCATIONS' and Code_Value='%@'",productType];
    //NSLog(@"qry for app code %@", appCodeQry);
    
    NSMutableArray* appCodeArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:appCodeQry];
    if (appCodeArray.count>0)
    {
        appCodeDesc=[[appCodeArray objectAtIndex:0]valueForKey:@"Code_Description"];
        return appCodeDesc;
    }
    else
    {
        return nil;
    }
}

#pragma mark
#pragma mark Add Availability
/*-(void) btnAddAvailability:(UIButton*)sender
{
   [self.view endEditing:YES];
   
    BOOL duplicacy = NO;
    isAddButtonPressed = YES;
    if ([_txtDropdown.text isEqualToString:@"Shelf"])
    {
        [tempDistributionItem setValue:@"S" forKey:@"Loc"];
    } else {
        [tempDistributionItem setValue:@"B" forKey:@"Loc"];
    }
    
    if (_tabAvailable.selectedSegmentIndex == 0)
    {
        [tempDistributionItem setValue:@"Y" forKey:@"Avl"];
    } else if (_tabAvailable.selectedSegmentIndex == 1)
    {
        [tempDistributionItem setValue:@"N" forKey:@"Avl"];
    }
    
    @try {
        NSMutableArray *arr = [itemDataWithLocation objectForKey:[NSNumber numberWithInt:selectedRowIndex]];
        if ([arr count] == 0)
        {
            btnAdd.enabled = NO;
            btnAdd.alpha = 0.7;
            
            [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
            [self disableFields];
            _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
        }
        for (int i = 0; i<[arr count]; i++)
        {
            if ([[[arr objectAtIndex:i]valueForKey:@"LotNo"] isEqualToString:[tempDistributionItem valueForKey:@"LotNo"]] && [[[arr objectAtIndex:i]valueForKey:@"ExpDate"] isEqualToString:[tempDistributionItem valueForKey:@"ExpDate"]])
            {
                duplicacy = YES;
                break;
            }
            else
            {
                duplicacy = NO;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    if (duplicacy)
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Alert" andMessage:@"Item is already updated for this Lot No" withController:self];
    }
    else
    {
        btnAdd.enabled = NO;
        btnAdd.alpha = 0.7;
        
        [self tableView:_itemTableView didDeselectRowAtIndexPath:selectedIndex];
        [self disableFields];
        _tabAvailable.selectedSegmentIndex = UISegmentedControlNoSegment;
    }
}
*/
#pragma mark
#pragma mark image action
- (void)imageTapped:(UIGestureRecognizer *)gestureRecognizer
{
//    if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:@"Y"])
//    {
        if ([_imgView.image isEqual:[UIImage imageNamed:@"DC_Product_Icon"]])
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }
        else
        {
            flagRetake = true;
            ShowImageViewController * popOverVC=[[ShowImageViewController alloc]init];
            popOverVC.imageName = [tempDistributionItem valueForKey:@"DistributionCheckLine_ID"];
            popOverVC.image = _imgView.image;
            [self.navigationController pushViewController:popOverVC animated:YES];
        }
   // }
    /*else
    {
        if ([_imgView.image isEqual:[UIImage imageNamed:@"SalesWorxDistributionCheckDefault.png"]])
        {
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                imagePicker.delegate = self;
                imagePicker.sourceType =UIImagePickerControllerSourceTypeCamera;
                imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage,nil];
                imagePicker.allowsEditing = NO;
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }
        else
        {
            flagRetake = true;
            ShowImageViewController * popOverVC=[[ShowImageViewController alloc]init];
            popOverVC.imageName = [distributionItem valueForKey:@"DistributionCheckLine_ID"];
            popOverVC.image = _imgView.image;
            [self.navigationController pushViewController:popOverVC animated:YES];
        }
    }*/
}
#pragma mark image delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage])
        {
            _imgView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/Distribution check images"];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
            
            if ([appControl.ENABLE_DIST_CHECK_MULTI_LOTS isEqualToString:KAppControlsYESCode])
            {
                if (selectedDistributionCheckItem.imageName.length ==  0) {
                    selectedDistributionCheckItem.imageName = [NSString createGuid];
                }
            }
            else
            {
                selectedDistributionCheckItem.imageName = selectedDistributionCheckItemLot.DistriButionCheckItemLotId;
            }
            
            NSString *savedImagePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",selectedDistributionCheckItem.imageName]];
            
            float compressionQuality=0.3;
            // Now, save the image to a file.
            
            if(NO == [UIImageJPEGRepresentation(_imgView.image,compressionQuality) writeToFile:savedImagePath atomically:YES]) {
                [NSException raise:@"Image Save Failed" format:@"Unable to store image %@", savedImagePath];
            }
            else
            {
                NSLog(@"Image saved");
            }
        });
    }];
}
-(void)checkImage
{
    if ([distributionItem valueForKey:@"Image"] == nil)
    {
        [distributionItem setValue:@"N" forKey:@"Image"];
    }
}

#pragma mark
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
