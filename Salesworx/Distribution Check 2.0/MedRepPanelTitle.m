//
//  MedRepPanelTitle.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "MedRepPanelTitle.h"
#import "MedRepDefaults.h"

@implementation MedRepPanelTitle

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.font=kFontWeblySleekSemiLight(16);
    self.textColor= UIColorFromRGB(0x2C394A); // Drak Blue. Taken as SalesWorx > SalesWorxPanelTitleLabel
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && super.text.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[super.text characterAtIndex:super.text.length-1]];
            NSString *nString=[super.text stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(super.text, nil);
            
        }
    }
    
    
}

-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
    {
        if(_isTextEndingWithExtraSpecialCharacter && newText.length>2)
        {
            NSString *specialCharcaterStr= [NSString stringWithFormat:@"%C",[newText characterAtIndex:newText.length-1]];
            NSString *nString=[newText stringByReplacingOccurrencesOfString:specialCharcaterStr withString:@""];
            super.text=[NSLocalizedString(nString, nil) stringByAppendingString:specialCharcaterStr];
            
        }
        else
        {
            super.text=NSLocalizedString(newText, nil);
            
        }
    }
    else
        super.text = newText;
    
    
}

@end
