//
//  SalesWorxPopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/30/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"
#import "CategoryTableViewCell.h"
@interface SalesWorxPopOverViewController ()

@end

@implementation SalesWorxPopOverViewController
@synthesize enableArabicTranslation;
@synthesize popOverContentArray,titleKey,popOverSearchBar,popOverTableView,popOverController,disableSearch,popoverTableViewTopConstraint,popoverType,enableSwipeToDelete,headerTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (enableSwipeToDelete==YES) {
        popOverTableView.allowsMultipleSelectionDuringEditing=NO;
    }
    

    // Do any additional setup after loading the view from its nib.
    filteredPopOverContentArray=[[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    
    
    CGRect frame = CGRectMake(0, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = titleKey;
    self.navigationItem.titleView = label;
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:titleKey];

   // self.navigationController.navigationBar.topItem.title = @"";
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(21.0/255.0) green:(157.0/255.0) blue:(94.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
        
        
    }
    
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeTapped)];
    
    [closeButton setTitleTextAttributes:[SWDefaults fetchBarAttributes] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=closeButton;
    
    
    if (disableSearch==YES) {
        
        
        popOverSearchBar.hidden=YES;
       // popoverTableViewTopConstraint.constant=-44;
        
        [self.view layoutIfNeeded];
    }
    
    else
    {
        popOverSearchBar.hidden=NO;
        self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        self.searchController.searchResultsUpdater = self;
        self.searchController.delegate=self;
        self.searchController.searchBar.barTintColor = [UIColor colorWithRed:235.0 / 255.0 green:251.0 / 255.0 blue:249.0 / 255.0 alpha:1.0f];
        self.searchController.searchBar.layer.borderColor = [[UIColor grayColor] CGColor];
        self.searchController.dimsBackgroundDuringPresentation = NO;
        self.popOverTableView.tableHeaderView = self.searchController.searchBar;
        [[UIBarButtonItem appearanceWhenContainedIn: [UISearchBar class], nil] setTintColor:[UIColor grayColor]];

        
        [popOverTableView reloadData];
        self.definesPresentationContext = NO;
        self.searchController.hidesNavigationBarDuringPresentation = NO;

    }
    filteredPopOverContentArray=[popOverContentArray mutableCopy];
    if ([popoverType isEqualToString:@"Category Type"] || [popoverType isEqualToString:@"Product Type"] ){
        UIBarButtonItem *accompaniedByDoneButton=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonItemStylePlain target:self action:@selector(multipleCategoryDoneButtonTapped)];
        self.navigationItem.rightBarButtonItem=accompaniedByDoneButton;
        popOverTableView.allowsMultipleSelection = YES;
    }
}


-(void)closeTapped
{
    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UITableView DataSource Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredPopOverContentArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString* identifier =@"filterCell";
    
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    cell.textLabel.font=MedRepSingleLineLabelFont;
    
    cell.textLabel.textColor=MedRepDescriptionLabelFontColor;
    
    
    //open simularor
    
    if ([popoverType isEqualToString:@"Activation"]) {
        
        SyncLoctions * synLocation=[filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text=[NSString stringWithFormat:@"%@",synLocation.name];
        return cell;
    }else if ([popoverType isEqualToString:@"Category Type"]){
        NSMutableDictionary *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        
        static NSString* identifier=@"categoryList";
        CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"CategoryTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        //        if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsYESCode]) {
        //            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        //        } else {
        //            cell.accessoryType = UITableViewCellAccessoryNone;
        //        }
        
        VisitImagesCategories *visitCategory = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        
        if([visitCategory.isSelected isEqualToString:KAppControlsYESCode]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        //  for (NSMutableDictionary *dictObj in filteredPopOverContentArray) {
        cell.categoryLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:visitCategory.categoryDesc]] isEqualToString:@""]?@"N/A" : [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:visitCategory.categoryDesc]];
        cell.agencyLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:visitCategory.agencyDesc]] isEqualToString:@""]?@"N/A" : [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:visitCategory.agencyDesc]];
        cell.brandCodeLabel.text = [[NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:visitCategory.brandDesc]]isEqualToString:@""]?@"N/A":[NSString stringWithFormat:@"%@",visitCategory.brandDesc];
        
        return cell;
    }else if ([popoverType isEqualToString:@"Product Type"]){
        //  NSMutableDictionary *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        VisitImagesProducts *visitProducts = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        cell.textLabel.font = MedRepSingleLineLabelSemiBoldFont;
        cell.textLabel.textColor = MedRepSingleLineLabelSemiBoldFontColor;
        
        if ([visitProducts.isSelected isEqualToString:KAppControlsYESCode]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        cell.textLabel.text=visitProducts.productDescription;//[NSString stringWithFormat:@"%@",[[filteredPopOverContentArray objectAtIndex:indexPath.row]valueForKey:@"Description"]];
        return cell;
    }
    
    //    else if ([popoverType isEqualToString:kOrderTempletePopOverTitle]) {
    //        SalesOrderTemplete * OrderTemplete=[filteredPopOverContentArray objectAtIndex:indexPath.row];
    //        cell.textLabel.text=[NSString stringWithFormat:@"%@",OrderTemplete.Template_Name];
    //    }
    //    else  if ([popoverType isEqualToString:KSalesOrderManualFocPopOverTitle])
    //    {
    //        Products *product=[filteredPopOverContentArray objectAtIndex:indexPath.row];
    //        cell.textLabel.text=[NSString stringWithFormat:@"%@",product.Description];
    //
    //    }
    else
        
    {
        
        if(enableArabicTranslation)
            cell.textLabel.text=[NSString stringWithFormat:@"%@",NSLocalizedString([filteredPopOverContentArray objectAtIndex:indexPath.row],nil)];
        else
            cell.textLabel.text=[NSString stringWithFormat:@"%@",[filteredPopOverContentArray objectAtIndex:indexPath.row]];
        return cell;
    }
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverController:)] && enableSwipeToDelete==YES) {
            
            NSIndexPath *selectedIndexPath;
            selectedIndexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];
            [self.salesWorxPopOverControllerDelegate didDeleteItematIndex:selectedIndexPath];
            [popOverContentArray removeObjectAtIndex:selectedIndexPath.row];
            [popOverTableView reloadData];
        }
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (enableSwipeToDelete==YES) {
        
        return UITableViewCellEditingStyleDelete;
    }
    else
    {
        return UITableViewCellEditingStyleNone;

    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *matchedindexPath = [NSIndexPath indexPathForRow:[popOverContentArray indexOfObject:[filteredPopOverContentArray objectAtIndex:indexPath.row]] inSection:0];
    
    if ([popoverType isEqualToString:@"Visit_Option_Feedback"] && [[[[filteredPopOverContentArray objectAtIndex:matchedindexPath.row]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString] isEqualToString:@"other reason"]) {
        AddFeedbackCommentViewController *objVC = [[AddFeedbackCommentViewController alloc]init];
        objVC.AddFeedbackCommentDelegate = self;
        [self.navigationController pushViewController:objVC animated:YES];
        
    }else if ([popoverType isEqualToString:@"Category Type"] || [popoverType isEqualToString:@"Product Type"]){
        //NSMutableDictionary *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        VisitImagesCategories *selectedDict = [filteredPopOverContentArray objectAtIndex:indexPath.row];
        //        if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsYESCode]) {
        //            [selectedDict setValue:KAppControlsNOCode forKey:@"isSelected"];
        //        } else if ([[selectedDict valueForKey:@"isSelected"] isEqualToString:KAppControlsNOCode]) {
        //            [selectedDict setValue:KAppControlsYESCode forKey:@"isSelected"];
        //        }
        
        if ([selectedDict.isSelected isEqualToString:KAppControlsYESCode]) {
            selectedDict.isSelected = KAppControlsNOCode;
        } else if ([selectedDict.isSelected isEqualToString:KAppControlsNOCode]) {
            selectedDict.isSelected = KAppControlsYESCode;
            selectedDict.selectedImageID = _imageID;
        }
        [popOverTableView reloadData];
    }
    else {
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverController:)]) {
            [self.salesWorxPopOverControllerDelegate didSelectPopOverController:matchedindexPath];
        }
        if(popOverController==nil)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [popOverController dismissPopoverAnimated:YES];
    }
}

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
    // [textField release] // uncomment if not using ARC
}

-(void)multipleCategoryDoneButtonTapped{
    
    if ([popoverType isEqualToString:@"Category Type"]) {
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverControllerForCategoryMultiSelection::)]) {
            
            [self.salesWorxPopOverControllerDelegate didSelectPopOverControllerForCategoryMultiSelection:filteredPopOverContentArray :popoverType];
        }
        
        if(popOverController==nil)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [popOverController dismissPopoverAnimated:YES];
    } else if ([popoverType isEqualToString:@"Product Type"]){
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverControllerForCategoryMultiSelection::)]) {
            
            [self.salesWorxPopOverControllerDelegate didSelectPopOverControllerForCategoryMultiSelection:filteredPopOverContentArray :popoverType];
        }
        
        if(popOverController==nil)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [popOverController dismissPopoverAnimated:YES];
    }
    
    else{
        if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSelectPopOverControllerForCategoryMultiSelection::)]) {
            
            [self.salesWorxPopOverControllerDelegate didSelectPopOverControllerForCategoryMultiSelection:filteredPopOverContentArray :popoverType];
        }
        
        if(popOverController==nil)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [popOverController dismissPopoverAnimated:YES];
    }
    
}
#pragma mark Search Controller methods

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if(searchText.length==0)
    {
        filteredPopOverContentArray=[popOverContentArray mutableCopy];
    }
    else{
        if ([popoverType isEqualToString:@"Activation"]) {
            NSPredicate *resultPredicate = [NSPredicate
                                            predicateWithFormat:@"SELF.name contains[cd] %@",
                                            searchText];
            filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        } else if ([popoverType isEqualToString:@"Category Type"]){
            
            NSString* filter = @"%K CONTAINS[cd] %@";
            NSPredicate *agencyDesc = [NSPredicate predicateWithFormat:filter, @"agencyDesc", searchText];
            NSPredicate *categoryDesc = [NSPredicate predicateWithFormat:filter, @"categoryDesc", searchText];
            NSPredicate *brandDesc = [NSPredicate predicateWithFormat:filter, @"brandDesc", searchText];
            
            NSPredicate *finalPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[agencyDesc, categoryDesc, brandDesc]];
            filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:finalPredicate] mutableCopy];
        }else if ([popoverType isEqualToString:@"Product Type"]){
            NSPredicate *resultPredicate = [NSPredicate
                                            predicateWithFormat:@"SELF.productDescription contains[cd] %@",
                                            searchText];
            
            filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        }
        else
        {
            NSPredicate *resultPredicate = [NSPredicate
                                            predicateWithFormat:@"SELF contains[cd] %@",
                                            searchText];
            
            filteredPopOverContentArray = [[popOverContentArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
        }
    }
    [popOverTableView reloadData];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text
                               scope:@""];

}

- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;

    filteredPopOverContentArray=[popOverContentArray mutableCopy];
    [popOverTableView reloadData];
}
- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}

-(void)selectFeedback:(NSString*)comment {
    if ([self.salesWorxPopOverControllerDelegate respondsToSelector:@selector(didSaveFeedbackForOtherReason:)]) {
        [self.salesWorxPopOverControllerDelegate didSaveFeedbackForOtherReason:comment];
    }
    if(popOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [popOverController dismissPopoverAnimated:YES];
}

@end
