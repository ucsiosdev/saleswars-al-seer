//
//  SalesWorxCustomClass.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 3/7/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxCustomClass.h"

@implementation SalesWorxCustomClass

@end


@implementation PriceList

@synthesize Price_List_ID,Inventory_Item_ID,Organization_ID,Item_UOM,Unit_Selling_Price,Unit_List_Price,Is_Generic;

@end

@implementation Stock

@synthesize Org_ID,Lot_No,Lot_Qty,Expiry_Date,Stock_ID,lotNumberWithExpiryDate;

@end

@implementation SalesOrderInitialization



@end

@implementation ProductCategories
@end

@implementation DistributionCheck
@synthesize Item_Code, description, Avl, Qty, LotNo, ExpDate, ItemStock;
@end

@implementation SalesOrderTemplateLineItems



@end

@implementation SalesWorxParentOrder



@end

@implementation SalesOrderTemplete



@end
@implementation ManageOrderDetails
@synthesize manageOrderStatus,Orig_Sys_Document_Ref,Row_ID,Visit_ID,FOCOrderNumber,FOCParentOrderOrderNumber,OrderAmount,Creation_Date,Category,customerDetails;


@end

@implementation SalesWorxReturn
@synthesize productCategoriesArray,selectedCategory,returnLineItemsArray,confrimationDetails,ReturnStartTime;
@end

@implementation ReturnsLineItem
@synthesize returnProduct,bonusProduct,defaultBonusQty,recievedBonusQty,returnProductQty,bonusDetailsArray,lotNumber,expiryDate,reasonForReturn,returnItemNetamount,reasonForReturnDescription;


- (id)copy
{
    ReturnsLineItem *returnsLineItem = [[ReturnsLineItem alloc] init];
       //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    returnsLineItem.returnProduct = self.returnProduct;
    returnsLineItem.bonusProduct = self.bonusProduct;
    returnsLineItem.defaultBonusQty = self.defaultBonusQty;
    returnsLineItem.recievedBonusQty = self.recievedBonusQty;
    returnsLineItem.returnProductQty = self.returnProductQty;
    returnsLineItem.bonusDetailsArray = self.bonusDetailsArray;
    returnsLineItem.lotNumber = self.lotNumber;
    returnsLineItem.expiryDate = self.expiryDate;
    returnsLineItem.reasonForReturn = self.reasonForReturn;
    returnsLineItem.returnItemNetamount = self.returnItemNetamount;
    returnsLineItem.reasonForReturnDescription = self.reasonForReturnDescription;

    return returnsLineItem;
}

@end
@implementation ReturnsConfirmationDetails
@synthesize DocReferenceNumber,signeeName,comments,returnType,isGoodsCollected,InvoicesNumbers,isCustomerSigned;
@end
@implementation SalesWorxReOrder
//@synthesize productCategoriesArray,salesOrderTempleteArray,selectedTemplete,selectedCategory,selectedOrderType,focParentOrderNumber,SalesOrderLineItemsArray,confrimationDetails,isTemplateOrder,isManagedOrder,OrderStartTime,SelectedManageOrderDetails,reOrderDetails;
@end
@implementation SalesOrder
@synthesize productCategoriesArray,salesOrderTempleteArray,selectedTemplete,selectedCategory,selectedOrderType,focParentOrderNumber,SalesOrderLineItemsArray,confrimationDetails,isTemplateOrder,isManagedOrder,OrderStartTime,SelectedManageOrderDetails,reOrderDetails;
@end

@implementation SalesWorxVisit

@synthesize visitOptions,Visit_ID,Visit_Type,isPlannedVisit,isTODOCompleted;

@end

@implementation VisitOptions



@end


@implementation ToDo



@end
@implementation BonusItem

@end

@implementation ProductMediaFile



@end

@implementation Products

@synthesize Brand_Code,Category,Description,IsMSL,Item_Code,ItemID,OrgID,Sts,productPriceList,nearestExpiryDate,isSellingPriceFieldEditable,specialDicount;

@end

@implementation CustomerCategory

@synthesize Category,Description,Item_No,Org_ID;

@end

@implementation SelectedBrand



@end

@implementation CustomerOrderHistory

@synthesize Customer_Name,Creation_Date,ERP_Status,FSR,Orig_Sys_Document_Ref,Transaction_Amt;

@end

@implementation SWCustomerPriceList



@end

@implementation CustomerOrderHistoryLineItems


@end

@implementation CustomerOrderHistoryInvoiceLineItems



@end

@implementation CustomerDues



@end

@implementation Customer





@synthesize Address,Allow_FOC,Avail_Bal,Customer_Name,Bill_Credit_Period,Cash_Cust,Chain_Customer_Code,City,Contact,Creation_Date,Credit_Hold,Credit_Limit,Cust_Lat,Cust_Long,Cust_Status,Customer_Barcode,Customer_Class,Customer_ID,Customer_No,Customer_OD_Status,Customer_Segment_ID,Customer_Type,Dept,Location,Phone,Postal_Code,Price_List_ID,SalesRep_ID,Sales_District_ID,Ship_Customer_ID,Ship_Site_Use_ID,Site_Use_ID,Trade_Classification,categoriesArray,targetValue,salesValue,balanceToGoValue,isBonusAvailable;


@end





@implementation SalesOrderLineItem
@synthesize OrderProduct,manualFocProduct,manualFOCQty,bonusProduct,defaultBonusQty,requestedBonusQty,productDiscount,productSelleingPrice,OrderProductQty,OrderItemTotalamount,OrderItemDiscountamount,OrderItemNetamount,appliedDiscount,bonusDetailsArray,NotesStr,AssignedLotsArray;
- (id)copy
{
    SalesOrderLineItem *salesOrderLineItem = [[SalesOrderLineItem alloc] init];
    
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    salesOrderLineItem.OrderProduct = self.OrderProduct;
    salesOrderLineItem.manualFocProduct = self.manualFocProduct;
    salesOrderLineItem.manualFOCQty = self.manualFOCQty;
    salesOrderLineItem.bonusProduct = self.bonusProduct;
    salesOrderLineItem.defaultBonusQty = self.defaultBonusQty;
    salesOrderLineItem.requestedBonusQty = self.requestedBonusQty;
    salesOrderLineItem.productDiscount = self.productDiscount;
    salesOrderLineItem.productSelleingPrice = self.productSelleingPrice;
    salesOrderLineItem.OrderProductQty = self.OrderProductQty;
    salesOrderLineItem.OrderItemTotalamount = self.OrderItemTotalamount;
    salesOrderLineItem.OrderItemDiscountamount = self.OrderItemDiscountamount;
    salesOrderLineItem.OrderItemNetamount = self.OrderItemNetamount;
    salesOrderLineItem.appliedDiscount = self.appliedDiscount;
    salesOrderLineItem.bonusDetailsArray = self.bonusDetailsArray;
    salesOrderLineItem.NotesStr = self.NotesStr;
    salesOrderLineItem.AssignedLotsArray = self.AssignedLotsArray;

    return salesOrderLineItem;
}
@end


@implementation ProductBonusItem
@synthesize Description,Get_Add_Per,Get_Item,Get_Qty,Price_Break_Type_Code,Prom_Qty_From,Prom_Qty_To,bonusProduct;
@end

@implementation SalesOrderAssignedLot
@synthesize lot,assignedQuantity;

- (id)copy;
{
    SalesOrderAssignedLot *assignLot = [[SalesOrderAssignedLot alloc] init];
    
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    assignLot.lot = self.lot;
    assignLot.assignedQuantity = self.assignedQuantity;
    
    return assignLot;
}
@end


@implementation SalesOrderConfirmationDetails
@synthesize DocReferenceNumber,shipDate,signeeName,comments,isWholeSaleOrder,SkipConsolidation,isCustomerSigned;
@end

@implementation RMALotType
@synthesize lotType,Description;
@end

@implementation SalesWorxReasonCode
@synthesize Reasoncode,Description,Purpose;
@end

@implementation DailyVisitSummaryReport
@synthesize Customer_ID, Customer_Name, Payment, PaymentValue, DC, Survey, Order, OrderValue, Return, ReturnValue;
@end
@implementation SyncImage
@synthesize imagePath,imageSyncType,isUploaded,imageMIMEType,uploadFailureCount;
- (id)copy
{
    SyncImage *image = [[SyncImage alloc] init];
    //YOu may need to copy these values too, this is a shallow copy
    //If YourValues and someOtherValue are only primitives then this would be ok
    //If they are objects you will need to implement copy to these objects too
    image.imagePath = self.imagePath;
    image.imageSyncType = self.imageSyncType;
    image.isUploaded = self.isUploaded;
    image.imageMIMEType = self.imageMIMEType;
    image.uploadFailureCount = self.uploadFailureCount;
    
    return image;
}

@end

@implementation SalesWorxBrandAmbassadorLocation



@end

@implementation SalesWorxBrandAmbassadorTask



@end

@implementation salesWorxBrandAmbassadorNotes

@end

@implementation SalesWorxBrandAmbassadorWalkinCustomerVisitSurveyResponses



@end


@implementation SalesWorxBrandAmbassadorWalkinCustomerVisit



@end

@implementation DemoPlan
@end

@implementation SalesWorxBrandAmbassadorVisit



@end

@implementation SalesWorxGoogleAnalytics



@end

@implementation SalesWorxSalesTrend

@end

@implementation SalesWorxProductStockSync



@end


@implementation Customer_SalesSummary

@synthesize Orig_Sys_Document_Ref, Creation_Date, Doc_Type, Transaction_Amt, Customer_No, Customer_Name, Cash_Cust, Phone, Visit_ID, End_Time, ERP_Ref_No, Ship_To_Customer_Id, Ship_To_Site_Id;

@end


@implementation Customer_Statement

@synthesize Invoice_Ref_No, NetAmount, PaidAmt, InvDate, Customer_ID;

@end


@implementation Blocked_Statement

@synthesize Contact, Customer_ID, Customer_Name, Phone;

@end

@implementation SalesPerAgency



@end

@implementation VisitImages



@end

@implementation VisitOption_Feedback

@synthesize feedbackID, feedbackLineID, itemCode, itemDescription, inventoryItemID, feedbackStatus, isSave, comments, itemType;

@end
@implementation DistriButionCheckItemLot
@end

@implementation DistriButionCheckLocation
@end

@implementation DistriButionCheckItem
@end

@implementation DistriButionCheckMinStock
@end
@implementation VisitImagesCategories
@synthesize selectedImageID, agency, agencyDesc, brandDesc, brand_Code, category_Code,categoryDesc, inventory_Item_ID, isSelected;
@end

@implementation VisitImagesProducts
@synthesize inventory_Item_ID,productDescription,isSelected,selectedImageID,prodducstArray,filteredProductsArray;
@end

@implementation SyncLoctions
@synthesize name,url,seq;
@end
