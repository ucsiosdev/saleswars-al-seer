//
//  SalesWorxDatePickerPopOverViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/1/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SalesWorxDatePickerDelegate <NSObject>


-(void)didSelectDate:(NSString*)selectedDate;

@end


@interface SalesWorxDatePickerPopOverViewController : UIViewController<UIPickerViewDelegate>

{
    id didSelectDateDelegate;

}


@property (strong, nonatomic) IBOutlet UIDatePicker *salesWorxDatePicker;

@property (strong, nonatomic) IBOutlet UIDatePicker *visitsDatePicker;

@property(strong,nonatomic) id  didSelectDateDelegate;

@property(strong,nonatomic) NSString* titleString;

@property(strong,nonatomic) NSString* datePickerMode;

@property(nonatomic) BOOL setMinimumDateCurrentDate;

@property(nonatomic) BOOL setMaximumDateCurrentDate;

@property(strong,nonatomic) UIPopoverController * datePickerPopOverController;

@property(strong,nonatomic) NSString* previousSelectedDate;

@end
