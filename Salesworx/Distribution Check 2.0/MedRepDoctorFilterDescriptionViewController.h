//
//  MedRepDoctorFilterDescriptionViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SelectedFilterDelegate <NSObject>

-(void)selectedFilterValue:(NSString*)selectedString;
-(void)selectedDemoPlanDetails:(NSMutableArray*)demoplanDetails;
-(void)selectedVisitLocationDetails:(NSMutableArray*)visitDetails;
-(void)selectedValueforCreateVisit:(NSIndexPath*)indexPath;
-(void)selectedDemoPlanDetaislwithIndex:(NSMutableArray*)demoPlanDetails selectedIndex:(NSInteger)index;

@end

@interface MedRepDoctorFilterDescriptionViewController : UIViewController <UISearchControllerDelegate, UISearchResultsUpdating>
{
    id selectedFilterDelegate;
    BOOL isSearching;
}
@property (strong, nonatomic) IBOutlet UISearchBar *filterSearchBar;

@property(strong,nonatomic) id selectedFilterDelegate;
@property(strong,nonatomic) UIPopoverController* filterPopOverController;
@property(strong,nonatomic)UINavigationController * filterNavController;

@property(strong,nonatomic) IBOutlet UITableView* filterDescTblView;
@property(strong,nonatomic) NSMutableArray* filterDescArray;
@property(strong,nonatomic) NSMutableArray* refinedFilterDescArray;
@property(strong,nonatomic) NSString* descTitle;
@property (strong, nonatomic) UISearchController *searchController;

@end
