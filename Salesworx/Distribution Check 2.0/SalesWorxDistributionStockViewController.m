//
//  SalesWorxDistributionStockViewController.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/18/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxDistributionStockViewController.h"
#import "SalesWorxCustomClass.h"

@interface SalesWorxDistributionStockViewController ()

@end

@implementation SalesWorxDistributionStockViewController
@synthesize stockLotsArray,selectedDistributionCheckItem, isStockCheck;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appControl = [AppControl retrieveSingleton];
    if (@available(iOS 15.0, *)) {
        stockTableView.sectionHeaderTopPadding = 0;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    productNameLabel.text = _strDescription;
    productCodeLabel.text = _strCode;
    
    stockPopUpView.backgroundColor=UIViewBackGroundColor;
    [self setHiddenAnimated:NO duration:0.5 transitionType:kCATransitionPush];
    //[self updateStockArray];
}
-(IBAction)cancelButtonTapped:(id)sender
{
    [self setHiddenAnimated:YES duration:0.5 transitionType:kCATransitionPush];
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(CloseStockPopOver) userInfo:nil repeats:NO];
}
-(void)CloseStockPopOver
{
    [self dismissViewControllerAnimated:NO completion:^{
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return stockLotsArray.count;
    return selectedDistributionCheckItem.dcItemLots.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- ( UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, stockTableHeaderViewWithDistribution.frame.size.width, stockTableHeaderViewWithDistribution.frame.size.height)];
    stockTableHeaderViewWithDistribution.backgroundColor=kUITableViewHeaderBackgroundColor;
    [view addSubview:stockTableHeaderViewWithDistribution];
    return view;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"stockLotCell";
    SalesWorxDistributionStockTableViewCell *cell = (SalesWorxDistributionStockTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxDistributionStockTableViewCell" owner:nil options:nil] firstObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    DistriButionCheckItemLot *DCItemLot =[selectedDistributionCheckItem.dcItemLots objectAtIndex:indexPath.row];
    cell.LotNumberLbl.text = [DCItemLot.LotNumber isEqualToString:@""] || DCItemLot.LotNumber == nil ? KNotApplicable:DCItemLot.LotNumber;
    cell.QuantityLbl.text = [DCItemLot.Quntity isEqualToString:@""] || DCItemLot.Quntity == nil ? KNotApplicable:DCItemLot.Quntity;
    cell.ExpiryDateLbl.text = [DCItemLot.expiryDate isEqualToString:@""] || DCItemLot.expiryDate == nil ? KNotApplicable:DCItemLot.expiryDate;
    
    cell.LotNumberLbl.textColor=UITableViewCellTextColor;
    cell.QuantityLbl.textColor=UITableViewCellTextColor;
    cell.ExpiryDateLbl.textColor=UITableViewCellTextColor;
    cell.contentView.backgroundColor=UITableviewUnSelectedCellBackgroundColor;
    
    return cell;
}
- (void)setHiddenAnimated:(BOOL)hide duration:(NSTimeInterval)duration transitionType:(NSString*)transitionType
{
    CATransition* transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.duration = duration;
    transition.type = transitionType;
    
    
    transition.subtype = (hide ? @"fromBottom" : @"fromBottom");
    
    UIView* containerView = stockPopUpView;
    [containerView.layer removeAllAnimations];
    [containerView.layer addAnimation:transition forKey:kCATransition];
    containerView.hidden = hide;
    if (NO == hide)
        [containerView bringSubviewToFront:self.view];
}
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              //insert your deleteAction here
                                              @try {
                                                  [selectedDistributionCheckItem.dcItemLots removeObjectAtIndex:indexPath.row];
                                                  [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                            
                                                  if ([self.Delegate respondsToSelector:@selector(updateStockValue:)])
                                                  {
                                                      [self.Delegate updateStockValue:selectedDistributionCheckItem.dcItemLots];
                                                  }
                                              }
                                              @catch (NSException *exception) {
                                                  NSLog(@"%@",exception);
                                              }
                                          }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction];
}
-(void)updateStockArray
{
    NSMutableArray *tempArray = selectedDistributionCheckItem.dcItemLots;
    
    @try
    {
        selectedDistributionCheckItem.dcItemLots = [[NSMutableArray alloc]init];
        if (tempArray.count>0)
        {
            for (NSMutableDictionary * currentStockDict in tempArray)
            {
                @try {
                    Stock * productStock=[[Stock alloc]init];
                    productStock.Lot_No=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"LotNo"]];
                    productStock.Lot_Qty=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Qty"]];
                    productStock.Org_ID=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Organization_ID"]];
                    productStock.Expiry_Date=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"ExpDate"]];
                    productStock.warehouse=[SWDefaults getValidStringValue:[currentStockDict valueForKey:@"Description"]];
                    
                    [selectedDistributionCheckItem.dcItemLots addObject:productStock];
                }
                @catch (NSException *exception) {
                    [selectedDistributionCheckItem.dcItemLots addObject:currentStockDict];
                }
            }
        }
    }
    @catch (NSException *exception) {
        selectedDistributionCheckItem.dcItemLots = tempArray;
    }
    
    [stockTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
