//
//  SWDistributionCheckViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 3/31/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxCustomClass.h"
#import "MedRepTextField.h"
#import "PNCircleChart.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "MedRepButton.h"
#import "SWPlatform.h"
#import "SalesWorxDCFilterViewController.h"
#import "SalesWorxDescriptionLabel4_SemiBold.h"


@interface SWDistributionCheckViewController : UIViewController < UIPopoverControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    AppControl* appControl;
    
    NSDictionary *customer;
    CustomerHeaderView *customerHeaderView;
    UIPopoverController *popoverController;
    int selectedRowIndex;
    BOOL shouldAllowSave;
    BOOL allUpdated;
    BOOL anyUpdated;
    
    NSString* previousSelectedDate;
    
    //PCPieChart *pieChartNew;
    
    NSMutableArray *fetchDistriButionCheckLocations;
    NSMutableDictionary *distributionItem;
    NSMutableDictionary *tempDistributionItem;
    
    NSString* popOverTitle;
    UIPopoverController * paymentPopOverController;
    NSMutableDictionary *itemDataWithLocation;
    NSMutableArray *MSLItemsArray;
    NSMutableArray *MSLMinStockOfItemsArray;

    NSArray *items;
    UITapGestureRecognizer *tap;
    BOOL flagRetake;    
    NSMutableArray* indexPathArray;
    NSString *locationKey;
    
    DistriButionCheckLocation *selectedDistributionCheckLocation;
    DistriButionCheckItem *selectedDistributionCheckItem;
    DistriButionCheckItemLot *selectedDistributionCheckItemLot;

    NSIndexPath *selectedIndex;
    IBOutlet UIView *pieChartNew;
    
    IBOutlet UISegmentedControl *segAvailable;
    IBOutlet UIView *availDetailView;

    NSMutableArray *stockArray;
    BOOL isAddButtonPressed;
    NSIndexPath *editIndexPath;
    IBOutlet MedRepButton *btnAdd;
    IBOutlet UIView *checkCompletionPieChartContainerView;
    IBOutlet UISegmentedControl *segAvailabelWithThreeOption;
    IBOutlet UISegmentedControl *segReorder;
    IBOutlet UILabel *lblReorder;
    IBOutlet NSLayoutConstraint *xTopConstraintOfQunatityField;

    NSMutableArray*productsArray;
    UIImageView *blurredBgImage;
    NSMutableDictionary *previousFilteredParameters;
    IBOutlet UIButton *filterButton;

    IBOutlet MedRepElementTitleLabel *minReqStockTitleLabel;
    IBOutlet SalesWorxDescriptionLabel4_SemiBold *minReqStockLabel;
    
    NSMutableArray * DCItemLotsArray;
    IBOutlet UIView *lastVisitView;
    IBOutlet UILabel *lastVisitAvailability;
    
}
@property (weak, nonatomic) IBOutlet UITableView *itemTableView;
@property (strong, nonatomic) IBOutlet SalesWorxTableViewHeaderView *headerView;

@property (weak, nonatomic) IBOutlet SalesWorxDropShadowView *pieChartMainView;
@property (weak, nonatomic) IBOutlet UIView *detailView;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnDropDown;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtDropdown;

@property (weak, nonatomic) IBOutlet UILabel *lblLastVisitStock;
@property (weak, nonatomic) IBOutlet UILabel *lblLastVisitOn;

@property (weak, nonatomic) IBOutlet SalesWorxDefaultSegmentedControl *tabAvailable;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtQuantity;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtLotNo;
@property (weak, nonatomic) IBOutlet MedRepTextField *txtExpiryDate;
@property (weak, nonatomic) IBOutlet UIButton *btnExpiryDate;

@property(nonatomic,retain) NSMutableArray *arrayOfLocation;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblImage;
@property (weak, nonatomic) IBOutlet UIImageView *dividerLineForImageView;
@property(strong,nonatomic) NSMutableDictionary * customerDictornary;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xProductDetailHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xDetailViewBottomConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *xAddButtonHeightConstraint;



@end
