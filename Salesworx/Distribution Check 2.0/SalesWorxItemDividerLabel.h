//
//  SalesWorxItemDividerLabel.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/11/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"SWDefaults.h"
#import "MedRepDefaults.h"

@interface SalesWorxItemDividerLabel : UILabel

@end
