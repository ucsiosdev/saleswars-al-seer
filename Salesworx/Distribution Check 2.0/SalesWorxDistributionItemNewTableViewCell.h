//
//  SalesWorxDistributionItemNewTableViewCell.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/15/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface SalesWorxDistributionItemNewTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblCode;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblDescription;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *lblQuantity;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property(nonatomic) BOOL isHeader;
@end
