//
//  SalesWorxDistributionStockViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/18/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppControl.h"
#import "SalesWorxDistributionStockTableViewCell.h"
#import "SWDefaults.h"
#import "SalesWorxTableViewHeaderView.h"
#import "SalesWorxCustomClass.h"


@protocol dismissDelegate <NSObject>

-(void)updateStockValue:(NSMutableArray *)updatedStockArray;

@end

@interface SalesWorxDistributionStockViewController : UIViewController
{
    IBOutlet  UILabel *productNameLabel;
    IBOutlet  UILabel *productCodeLabel;
    IBOutlet UITableView *stockTableView;
    IBOutlet UIView *stockPopUpView;
    
    IBOutlet UIView *onOrderQuantityView;
    IBOutlet SalesWorxSingleLineLabel*OnOrderQtyLabel;
    
    AppControl *appControl;
    IBOutlet SalesWorxTableViewHeaderView *stockTableHeaderViewWithDistribution;
}
@property(nonatomic) id Delegate;

@property (strong,nonatomic)NSMutableArray *stockLotsArray;
@property (strong,nonatomic)NSString *strCode;
@property (strong,nonatomic)NSString *strDescription;

@property (strong,nonatomic)DistriButionCheckItem *selectedDistributionCheckItem;
@property(nonatomic) BOOL isStockCheck;


@end
