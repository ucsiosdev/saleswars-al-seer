//
//  MedRepDoctorFilterDescriptionViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 11/3/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "MedRepDoctorFilterDescriptionViewController.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"
@interface MedRepDoctorFilterDescriptionViewController ()

@end

@implementation MedRepDoctorFilterDescriptionViewController
@synthesize filterDescArray, refinedFilterDescArray, filterDescTblView, selectedFilterDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:  kSWX_FONT_REGULAR(16), NSFontAttributeName, nil];
    CGSize requestedTitleSize = [self.descTitle sizeWithAttributes:textTitleOptions];
    CGFloat titleWidth = MIN(self.view.frame.size.width, requestedTitleSize.width);
    CGRect frame = CGRectMake(0, 0, titleWidth, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = NSLocalizedString(self.descTitle, nil);
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:self.descTitle];
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    UINavigationBar *nbar = self.navigationController.navigationBar;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        //iOS 7
        nbar.barTintColor = [UIColor colorWithRed:(71.0/255.0) green:(144.0/255.0) blue:(210.0/255.0) alpha:1]; // bar color
        nbar.translucent = NO;
        
        nbar.tintColor = [UIColor whiteColor]; //bar button item color
        
    } else {
        //ios 4,5,6
        nbar.tintColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[[SWDefaults alloc]init] AddBackButtonToViewcontroller:self];
    
//    [self.filterDescTblView flashScrollIndicators];
//
//    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
   
    
    refinedFilterDescArray = [filterDescArray mutableCopy];
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.delegate=self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    filterDescTblView.tableHeaderView = self.searchController.searchBar;
    
    [filterDescTblView reloadData];
    self.definesPresentationContext = NO;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    
    
    filterDescTblView.estimatedRowHeight = 44.0;
    filterDescTblView.rowHeight = UITableViewAutomaticDimension;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableView DataSource Methods

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return refinedFilterDescArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier =@"filterCell";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.font=kSWX_FONT_SEMI_BOLD(14);
    cell.textLabel.textColor=MedRepElementDescriptionLabelColor;
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[refinedFilterDescArray objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

#pragma mark UITableView Delegate methods

-(void)dismissKeyboard {
    UITextField *textField;
    textField=[[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:textField];
    [textField becomeFirstResponder];
    [textField resignFirstResponder];
    [textField removeFromSuperview];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    
    NSString *selectedString = [refinedFilterDescArray objectAtIndex:indexPath.row];
    
    tableView.userInteractionEnabled=NO;
    self.searchController.active=NO;
    
    if ([self.selectedFilterDelegate respondsToSelector:@selector(selectedFilterValue:)]) {
        [self.selectedFilterDelegate selectedFilterValue:selectedString];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Search Controller methods

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self filterContentForSearchText:searchController.searchBar.text scope:@""];
}

- (void)didDismissSearchController:(UISearchController *)searchController
{
    self.navigationController.navigationBar.translucent = NO;
    refinedFilterDescArray = [filterDescArray mutableCopy];
    [filterDescTblView reloadData];
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    // do something before the search controller is presented
    self.navigationController.navigationBar.translucent = YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    if(searchText.length == 0)
    {
        refinedFilterDescArray = [filterDescArray mutableCopy];
    }
    else if ([self.descTitle isEqualToString:@"Contact"])
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.Contact_Name contains[cd] %@", searchText];
        NSLog(@"predicate for contact is %@", resultPredicate);
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    else if ([_descTitle isEqualToString:@"Doctors"])
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ANY SELF contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }else{
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchText];
        refinedFilterDescArray = [[filterDescArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    }
    
    [filterDescTblView reloadData];
}


@end
