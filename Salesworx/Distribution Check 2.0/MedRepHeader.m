//
//  MedRepHeader.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 2/16/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "MedRepHeader.h"
#import "MedRepDefaults.h"

@implementation MedRepHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/





-(void)awakeFromNib
{
    self.font=MedRepHeaderTitleFont;
    self.textColor=MedRepHeaderTitleFontColor;
    //    self.textColor=[UIColor yellowColor];
    

    if(self.RTLSupport)
        super.text=NSLocalizedString(super.text, nil);
    
}
-(NSString*)text{
    return super.text;
}

-(void)setText:(NSString*)newText {
    
    if(self.RTLSupport)
        super.text=NSLocalizedString(newText, nil);
    else
        super.text = newText;
    
}



@end
