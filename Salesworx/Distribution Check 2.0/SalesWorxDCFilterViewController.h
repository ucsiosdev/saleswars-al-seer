//
//  SalesWorxDCFilterViewController.h
//  SalesWorx
//
//  Created by USHYAKU-IOS on 9/20/17.
//  Copyright © 2017 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepTextField.h"
#import "MedRepButton.h"
#import "MedRepDoctorFilterDescriptionViewController.h"
#import "AppControl.h"
#import "NSString+Additions.h"
#import "SalesWorxDefaultSegmentedControl.h"
#import "MedRepElementTitleLabel.h"

@protocol SalesWorxDCFilterViewControllerDelegate <NSObject>

-(void)filteredProducts:(NSMutableArray*)filteredArray;
-(void)productsFilterDidClose;
-(void)productsFilterdidReset;
-(void)filteredProductParameters:(NSMutableDictionary*)parametersDict;

@end


@interface SalesWorxDCFilterViewController : UIViewController<SelectedFilterDelegate>
{
    IBOutlet MedRepTextField *brandCodeTxtFld;
    IBOutlet MedRepTextField *categoryNameTxtFld;
    IBOutlet MedRepTextField *SKUTxtFld;
    
    IBOutlet MedRepElementTitleLabel *categoryTitleLabel;
    
    NSMutableDictionary *filterParametersDict;
    NSMutableArray *filteredProductsArray;
    
    
    NSString* selectedTextField;
    NSString* selectedPredicateString;
}

@property(strong,nonatomic)NSMutableArray *productsArray;
@property(strong,nonatomic)NSMutableDictionary *previousFilterParametersDict;
@property(nonatomic) id delegate;
@property(strong,nonatomic) UIPopoverController *filterPopOverController;
@property(strong,nonatomic)UINavigationController *filterNavController;
@property(strong,nonatomic) NSString* filterTitle;

@property(nonatomic) BOOL isStockCheck;

@end
