
//
//  SalesWorxDistributionItemNewTableViewCell.m
//  SalesWorx
//
//  Created by USHYAKU-IOS on 7/15/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxDistributionItemNewTableViewCell.h"

@implementation SalesWorxDistributionItemNewTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setIsHeader:(BOOL)isHeader
{
    if (isHeader==YES) {
        
        CGRect sepFrame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1);
        UIView*  seperatorView = [[UIView alloc] initWithFrame:sepFrame];
        seperatorView.backgroundColor = kUITableViewSaperatorColor;
        [self addSubview:seperatorView];
    }
}

@end
