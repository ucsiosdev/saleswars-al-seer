//
//  SalesWorxDatePickerPopOverViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 12/1/15.
//  Copyright © 2015 msaad. All rights reserved.
//

#import "SalesWorxDatePickerPopOverViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"


@interface SalesWorxDatePickerPopOverViewController ()

@end

@implementation SalesWorxDatePickerPopOverViewController

@synthesize salesWorxDatePicker,datePickerMode,datePickerPopOverController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
    if ([_titleString isEqualToString:@"DistributionCheck"]) {
        _titleString = @"Expiry Date";
    }
    
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:_titleString];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{

    
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    
    
    
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    
    saveBtn.tintColor=[UIColor whiteColor];
    
    
    
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    
    
    
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    
    closeBtn.tintColor=[UIColor whiteColor];
    
    
    
    //    [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:self]setTitleTextAttributes:barattributes forState:forState:UIControlStateNormal
    
   	
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    self.navigationItem.rightBarButtonItem=saveBtn;
    self.navigationItem.leftBarButtonItem=closeBtn;

    salesWorxDatePicker.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
    if ([_titleString isEqualToString:@"Ship Date"]|| [_titleString isEqualToString:@"Status"]) {
        salesWorxDatePicker.datePickerMode=UIDatePickerModeDate;
        
        salesWorxDatePicker.minimumDate=[NSDate date];
    }
    
   else if ([datePickerMode isEqualToString:@"dateandTime"]) {
        
        salesWorxDatePicker.datePickerMode=UIDatePickerModeDateAndTime;
        
        salesWorxDatePicker.minimumDate=[NSDate date];
    }
    else if ([_titleString isEqualToString:@"cheque"])
    {
        @try {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // this is imporant - we set our input date format to match our input string
            // if format doesn't match you'll get nil from your string, so be careful
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [dateFormatter dateFromString:_previousSelectedDate];
            
            NSLog(@"%@",[NSDate date]);
            
            salesWorxDatePicker.maximumDate = [NSDate date];
            salesWorxDatePicker.date = dateFromString;
        }
        @catch (NSException *exception) {
            
        }
    }
    else if ([_titleString isEqualToString:@"PDC"])
    {
        if (self.setMinimumDateCurrentDate==YES)
        {
            //here date
            
            NSTimeInterval MY_EXTRA_TIME = 86400; // 10 Seconds
            NSDate *futureDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
            
            salesWorxDatePicker.minimumDate=futureDate;
        }
        else if (self.setMinimumDateCurrentDate == NO)
        {
            @try
            {
                NSTimeInterval MY_EXTRA_TIME = 86400; // 10 Seconds
                NSDate *futureDate = [[NSDate date] dateByAddingTimeInterval:MY_EXTRA_TIME];
                
                salesWorxDatePicker.minimumDate=futureDate;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [dateFormatter dateFromString:_previousSelectedDate];
                
                salesWorxDatePicker.date = dateFromString;
            }
            @catch (NSException *exception) {
                
            }
        }
    }
    else if ([_titleString isEqualToString:@"DistributionCheck"])
    {
        @try
        {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [dateFormatter dateFromString:_previousSelectedDate];
            
            salesWorxDatePicker.minimumDate = [NSDate date];
            salesWorxDatePicker.date = dateFromString;
        }
        @catch (NSException *exception) {
        }
    }
}
-(void)closeButtonTapped
{
    if(datePickerPopOverController==nil)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [datePickerPopOverController dismissPopoverAnimated:YES];
}
-(void)backButtontapped
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)saveDateTapped
{
    if ([self.didSelectDateDelegate respondsToSelector:@selector(didSelectDate:)]) {
        
        NSString *todayString;
        
        //refine date as per DB format
        
        
        
        if ([datePickerMode isEqualToString:@"dateandTime"]) {
            
            NSDateFormatter  *customDateFormatter = [[NSDateFormatter alloc] init];
            [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            [customDateFormatter setDateFormat:@"dd/MM/yyyy hh:mm a"];
            customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

            todayString = [NSString stringWithFormat:@" %@",[customDateFormatter stringFromDate:salesWorxDatePicker.date]];
        }
        else
        {
            NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
            myDateFormatter.dateFormat = @"yyyy-MM-dd hh:MM:ss";
            [myDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
            myDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];

         
             todayString = [NSString stringWithFormat:@"%@",[myDateFormatter stringFromDate:salesWorxDatePicker.date]];
        }
        
        NSLog(@"refined date is %@", todayString);
        
        [self.didSelectDateDelegate didSelectDate:todayString];
        
        if(datePickerPopOverController==nil)
            [self dismissViewControllerAnimated:YES completion:nil];
        else
            [datePickerPopOverController dismissPopoverAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
