//
//  SalesWorxDefaultSegmentedControl.m
//  SalesWorx
//
//  Created by Pavan Kumar Singamsetti on 8/1/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxDefaultSegmentedControl.h"

@implementation SalesWorxDefaultSegmentedControl
@synthesize EnableTitleRTLSupport;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    
    //if ([UIImage instancesRespondToSelector:@selector(imageFlippedForRightToLeftLayoutDirection)]) {
    //    self.imageView.image = [self.imageView.image imageFlippedForRightToLeftLayoutDirection];
    //}
    
    
    if(EnableTitleRTLSupport)
    {
        for (NSInteger i=0; i<super.numberOfSegments; i++) {
            [super setTitle:NSLocalizedString([super titleForSegmentAtIndex:i], nil) forSegmentAtIndex:i];

        }
       
    }

    
}

-(void)setTitle:(NSString *)title forSegmentAtIndex:(NSUInteger)segment
{
    if(EnableTitleRTLSupport)
    {
        [super setTitle:NSLocalizedString([super titleForSegmentAtIndex:segment], nil) forSegmentAtIndex:segment];
    }
    else
    {
        [super setTitle:[super titleForSegmentAtIndex:segment] forSegmentAtIndex:segment];
    }

}
@end
