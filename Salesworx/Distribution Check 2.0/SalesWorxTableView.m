//
//  SalesWorxTableView.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/10/16.
//  Copyright © 2016 msaad. All rights reserved.
//

#import "SalesWorxTableView.h"
#import "MedRepDefaults.h"
#import "SWDefaults.h"

@implementation SalesWorxTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderColor = kUITableViewBorderColor.CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = 1.0;
    self.layer.masksToBounds=YES;
   
    
}

@end
