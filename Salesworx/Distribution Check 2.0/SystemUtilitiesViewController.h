//
//  SystemUtilitiesViewController.h
//  Total
//
//  Created by Pavan Kumar Singamsetti on 12/11/14.
//  Copyright (c) 2014 Unique Computer Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SystemUtilitiesViewController : UIViewController

+ (NSString *)freeDiskSpace;
+ (CGFloat)freeDiskSpaceInBytes;

@end
