//
//  SWNoResultsView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 6/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWNoResultsView.h"
#import "SWFoundation.h"

@implementation SWNoResultsView

@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self setBackgroundColor:[UIColor whiteColor ]];
        [self setTitleLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [self.titleLabel setText:NSLocalizedString(@"No Results Found", nil)];
        [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:[UIColor darkTextColor]];
        [self.titleLabel setShadowColor:[UIColor whiteColor]];
        [self.titleLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.titleLabel setFont:BoldSemiFontOfSize(20.0f)];
        
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.titleLabel setFrame:self.bounds];
}


@end