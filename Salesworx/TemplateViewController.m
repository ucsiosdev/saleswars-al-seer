//
//  TemplateViewController.m
//  Salesworx
//
//  Created by msaad on 5/26/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "TemplateViewController.h"
#import "SalesOrderNewViewController.h"
@interface TemplateViewController ()

@end

@implementation TemplateViewController

- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:c ];
        [self setTitle:@"Templates"];
        self.view.backgroundColor = [UIColor whiteColor];
        CustomerHeaderView *headerView = [[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60) andCustomer:customer] ;
        [headerView setShouldHaveMargins:YES];
        gridView=[[GridView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width , self.view.frame.size.height-60) ];
        [gridView setDataSource:self];
        [gridView setDelegate:self];
        [gridView setShouldAllowDeleting:YES];
        [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.view addSubview:gridView];
        [self.view addSubview:headerView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Template View"];
    [self getSalesOrderServiceDidGetTemplate:[[SWDatabaseManager retrieveManager] dbGetTemplateOrder]];
}
#pragma mark GridView DataSource
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return 4;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 4;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 30.0f;
            break;
        case 1:
            return 25.0f;
            break;
        case 2:
            return 25.0f;
            break;
        case 3:
            return 25.0f;
            break;
        default:
            break;
    }
    return 20.0f;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"Reference No", nil);
            break;
        case 1:
            title = NSLocalizedString(@"Amount", nil);
            break;
        case 2:
            title = NSLocalizedString(@"Status", nil);
            break;
        case 3:
            title = NSLocalizedString(@"Date", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    switch (column)
    {
        case 0:
            text = @"Test 1";
            break;
        case 1:
            text = @"Test 2";
            break;
        case 2:
            text = @"Test 3";
            break;
        case 3:
            text = @"Test 4";
            break;
        default:
            break;
    }
    
    return text;
}

#pragma mark GridView Delegate
- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{

    
    NSString *savedCategoryID = [[totalOrderArray objectAtIndex:rowIndex] stringForKey:@"Custom_Attribute_1"];    
    [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer] andCategoryName:savedCategoryID andOrderDictionary:[totalOrderArray objectAtIndex:rowIndex]];
    NSMutableArray *navigationStack = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [navigationStack removeObjectAtIndex:[navigationStack count] - 2];
    self.navigationController.viewControllers = navigationStack;
}

- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict
{
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
            [SWDefaults setProductCategory:[row mutableCopy]];
            break;
        }
    }
    
    SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
    newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    newSalesOrder.parentViewString = @"MO";
    newSalesOrder.preOrderedItems=[NSMutableArray new];
    newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
    newSalesOrder.isOrderConfirmed=@"confirmed";
    [self.navigationController pushViewController:newSalesOrder animated:YES];
}

- (void)gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex
{
    NSDictionary *data = [totalOrderArray objectAtIndex:rowIndex];
    [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[data stringForKey:@"Orig_Sys_Document_Ref"]];
    [totalOrderArray removeObjectAtIndex:rowIndex];
    [gv deleteRowAtIndex:rowIndex];
}

- (void)presentProductOrder:(NSDictionary *)product
{
    
    
}
- (void)getSalesOrderServiceDidGetTemplate:(NSArray *)templates
{
    totalOrderArray = [NSMutableArray arrayWithArray:templates ];
    templates=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


@end
