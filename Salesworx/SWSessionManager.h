//
//  SWSessionManager.h
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SWSessionManager : NSObject {
    UIWindow *window;
    UIViewController *applicationViewController;
    UINavigationController *navigationControllerFront;

}

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UIViewController *applicationViewController;

- (id)initWithWindow:(UIWindow *)window andApplicationViewController:(UIViewController *)applicationViewController;

@end