//
//  SWViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWViewController.h"
#import "SWFoundation.h"
#import "SWDefaults.h"


@interface SWViewController ()

@end

@implementation SWViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:headerTitleFont, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)]) {
        
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self) {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
        }
    }
}

- (void)changeUISegmentFont:(UIView *)aView {
    NSString *className = NSStringFromClass(aView.class);
	if ([className isEqualToString:@"UISegmentLabel"]) {
		UILabel* label = (UILabel*)aView;
		[label setTextAlignment:NSTextAlignmentCenter];
		[label setFont:kFontWeblySleekSemiLight(14.0f)];
	}

	NSArray* subs = [aView subviews];
	NSEnumerator* iter = [subs objectEnumerator];
	UIView* subView;
	while (subView = [iter nextObject]) {
		[self changeUISegmentFont:subView];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
    {
        return YES;
    }
    else {
        return NO;
    }
}

- (void)setupToolbar:(NSArray *)buttons {    
    [self setToolbarItems:nil];
    
    if (buttons) {
        [self setToolbarItems:buttons];
        [self.navigationController setToolbarHidden:NO animated:YES];
    } else {
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //[cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    [cell.textLabel setFont:kFontWeblySleekSemiBold(14.0f)];
    [cell.detailTextLabel setFont:kFontWeblySleekSemiBold(14.0f)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
