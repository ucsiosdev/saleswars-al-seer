//
//  SalesWorxBackgroundSyncManager.h
//  SalesWars
//
//  Created by Neha Gupta on 25/01/21.
//  Copyright © 2017 Unique Computer Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SalesWorxBackgroundSyncManager : NSObject
{
    NSString *lastSynctimeStr;
    NSString *currentSyncStarttimeStr;
}
-(void)postVisitDataToserver;

@end
