//
//  SWLoadingView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/24/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWLoadingView : UIView {
    UILabel *loadingLabel;
    UIActivityIndicatorView *activityIndicatorView;
}

@property (nonatomic, strong) UILabel *loadingLabel;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;

@end