//
//  AlSeerSettingsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerSettingsViewController : UIViewController

{
    
}
@property (strong, nonatomic) IBOutlet UITextField *serverNameTxtFld;
@property (strong, nonatomic) IBOutlet UITextField *serverAddressTxtFld;
@property (strong, nonatomic) IBOutlet UILabel *serverLocationLabel;

@property (nonatomic, assign) id delegate;
- (IBAction)closeButtonTapped:(id)sender;
- (IBAction)editButtonTapped:(id)sender;

@end

