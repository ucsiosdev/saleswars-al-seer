//
//  SWNewActivationViewController.h
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWSession.h"
#import "PNCircleChart.h"
#import "SWPlatform.h"



@interface SWNewActivationViewController : FTPBaseViewController < UITextFieldDelegate,UIPopoverControllerDelegate,SynViewControllerDelegate,MBProgressHUDDelegate>
{
    //****** New connections added ******
    
    IBOutlet UITextField *usernameTextField;
    IBOutlet UILabel *syncStatusLbl;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UIView *credentialsView;
    IBOutlet UITextField *servernameTextField;
    IBOutlet UITextField *serverURLTextField;
    IBOutlet UIButton *activateButton;
    IBOutlet UILabel *lblVersion;
    IBOutlet UIView *activatingView;
    IBOutlet UIButton *signInNowButton;
    IBOutlet UIButton *backToSignInButton;
    IBOutlet NSLayoutConstraint *xConstraintCredentialViewVerticalAlignment;
    IBOutlet UIImageView *logoImageView;
    IBOutlet NSLayoutConstraint *xLogoImageHeightConstraint;
    IBOutlet NSLayoutConstraint *xLogoImageWidthConstraint;
    IBOutlet NSLayoutConstraint *xLogoImageCentreConstraint;
    IBOutlet NSLayoutConstraint *xConstraintActivatingViewVerticalAlignment;
    IBOutlet NSLayoutConstraint *xCredentialViewHeightConstraint;
    IBOutlet NSLayoutConstraint *xActivatingViewHeightConstraint;
    IBOutlet UIView *activationCompletedView;
    IBOutlet PNCircleChart *syncProgressBar;
    IBOutlet UILabel *lblUserNameStatic;
    IBOutlet UILabel *lblPasswordStatic;
    IBOutlet UILabel *lblServerLocationsStatic;
    IBOutlet UILabel *lblSeverURLStatic;
    IBOutlet UILabel *lblNewActivationStatic;
    IBOutlet UILabel *lblActvatingUserStatic;
    CGFloat lastProgress;
    
    IBOutlet NSLayoutConstraint *btnActivateConstraintTopSpace;
    
  //****** Old connections ********
   
    UIPopoverController *popoverController;
    UIWindow *window;
    UIViewController *applicationViewController;
    NSString *login;
    NSString *password;
    NSMutableDictionary *serverDict;
    NSString *editServerLink;
    NSString *editServerName1;
    NSMutableDictionary *newServerDict;
    SWLoadingView *loadingView;
    SynViewController *viewSync;
    DataSyncManager *appData;
    UIImageView *bacgroundBar;
    MBProgressHUD *progressHUD;
    NSMutableDictionary *editServerDict;
    SEL action;
    BOOL isActivation;
    BOOL isRootView;
    UIView *myBackgroundView;
}

@property (nonatomic, unsafe_unretained) id target;
@property(nonatomic)    BOOL alertDisplayed;
@property(nonatomic) BOOL invalidCredentials;
@property (nonatomic, assign) SEL action;
//demo logout test
@property(nonatomic,assign)BOOL *isDemo;
- (id)initWithNoDB;
@property(nonatomic)    BOOL isReActivateUser;
@end

