//
//  IdleTimeInfoViewController.m
//  SalesWars
//
//  Created by UshyakuMB2 on 25/08/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import "IdleTimeInfoViewController.h"
#import "SWDefaults.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWFoundation.h"
#import "SalesWorxPopOverViewController.h"
#import "SWDatabaseManager.h"
#import "SalesWorxTimePickerViewController.h"
#import "MedRepDefaults.h"
#import "SWDatabaseManager.h"
#import "FMDBHelper.h"
#import "SharedUtils.h"
#import "ReachabilitySync.h"
#import "SWAppDelegate.h"

@interface IdleTimeInfoViewController ()<UIPopoverControllerDelegate, UITextViewDelegate, UITextFieldDelegate>

@end

static NSString *ClientVersion = @"1";

@implementation IdleTimeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    lblStartTime.font = kFontWeblySleekSemiBold(16);
    lblStartTime.textColor = UIColorFromRGB(0x2C394A);
    
    lblEndTime.font = kFontWeblySleekSemiBold(16);
    lblEndTime.textColor = UIColorFromRGB(0x2C394A);
    
    lblReason.font = kFontWeblySleekSemiBold(16);
    lblReason.textColor = UIColorFromRGB(0x2C394A);
    
    lblComments.font = kFontWeblySleekSemiBold(16);
    lblComments.textColor = UIColorFromRGB(0x2C394A);

    txtViewComments.font = kFontWeblySleekSemiBold(15);
    txtViewComments.textColor = SalesWarsMenuTitleDescFontColor;
    
    txtStartTime.backgroundColor = [UIColor whiteColor];
    txtEndTime.backgroundColor = [UIColor whiteColor];
    txtReason.backgroundColor = [UIColor whiteColor];
    txtViewComments.backgroundColor = [UIColor whiteColor];
    
    [self setShadowOnUILayer:txtStartTime.layer];
    [self setShadowOnUILayer:txtEndTime.layer];
    [self setShadowOnUILayer:txtReason.layer];
    [self setShadowOnUILayer:txtViewComments.layer];
    
    viewComments.hidden = YES;
    txtViewComments.clipsToBounds = YES;
    
    arrReason = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select * from TBL_App_Codes where Code_Type = 'FSR_IDLE_REASON'"];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    ClientVersion = avid;
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3, 3);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}

- (IBAction)closeButtonTapped:(id)sender {
    [self.view endEditing:YES];
    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}

- (IBAction)saveButtonTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (txtStartTime.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select start time" withController:self];
    } else if (txtEndTime.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select end time" withController:self];
    } else if (txtReason.text.length == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select reason" withController:self];
    } else if (txtViewComments.text.length == 0 && [txtReason.text isEqualToString:@"Others"]) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please enter comments" withController:self];
    }
    else {
        
        BOOL success;
        FMDatabase *db = [FMDBHelper getDatabase];
        [db open];
        NSString *salesRepID = [NSString stringWithFormat:@"%@", [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
        NSString *comments = [txtViewComments.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        SWAppDelegate * appDeleage=(SWAppDelegate*)[[UIApplication sharedApplication]delegate];
        NSString *latitudePoint = [NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.latitude];
        NSString *longitudePoint = [NSString stringWithFormat:@"%f",appDeleage.currentLocation.coordinate.longitude];
        
        NSString *rowID = [NSString createGuid];
        
        if (latitudePoint == nil) {
            latitudePoint = @"0.0";
            longitudePoint = @"0.0";
        }
        
        @try {
            success = [db executeUpdate:@"INSERT into TBL_FSR_Idle_Time_Reason(Row_ID, SalesRep_ID, Start_Time, End_Time, Reason_Code, Comments, Latitude, Longitude) VALUES (?,?,?,?,?,?,?,?)", rowID, salesRepID, selectedStartTime, selectedEndTime, selectedReasonCodeValue, comments.length == 0 ? [NSNull null] : comments, latitudePoint, longitudePoint, nil];
            
            if (success == YES) {
                
                [self saveLocationData:latitudePoint andLongitude:longitudePoint andVisitID:rowID];
                
                NSLog(@"TBL_FSR_Idle_Time_Reason insert successfull");
                
                BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
                if (checkInternetConnectivity==YES) {
                    [self sendDataToServer];
                } else {
                    UIAlertAction *okAction = [UIAlertAction
                                                actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                                                {
                                                    [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
                                                }];

                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Success" andMessage:@"Data saved successfully" andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
                }
            } else {
                UIAlertAction *okAction = [UIAlertAction
                                            actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                                                [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
                                            }];
                
                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Error" andMessage:@"Error while inserting data" andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
            }
            
        }@catch (NSException *exception)
        {
            NSLog(@"Exception while inserting idle time reason in database: %@", exception.reason);
        }
        @finally {
            [db close];
        }
    }
}

-(void)saveLocationData:(NSString *)latitude andLongitude:(NSString *)longitude andVisitID:(NSString *)visitID {
    
    NSString *salesRepID = [NSString stringWithFormat:@"%@", [[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
    SWAppDelegate *appDelegate = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSString *gpsStatus = @"";
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
        {
            // authorized
            gpsStatus = @"Y";
        } else {
            // permission issue
            gpsStatus = @"P";
        }
    } else {
        // no GPS
        gpsStatus = @"N";
    }
    
    
    CLLocationAccuracy locationAccuracy = appDelegate.currentLocation.horizontalAccuracy;
    NSString *accuracy = [NSString stringWithFormat:@"%f", locationAccuracy];
    
    BOOL success;
    FMDatabase *db = [FMDBHelper getDatabase];
    [db open];
    
    @try {
        success = [db executeUpdate:@"INSERT into TBL_FSR_Locations(Row_ID, SalesRep_ID, Activity_Type, Latitude, Longitude, Status, Accuracy, Customer_ID, Site_Use_ID, Visit_ID, Logged_At) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [NSString createGuid], salesRepID, @"idle", latitude, longitude, gpsStatus, accuracy, @"", @"", visitID, [SWDefaults fetchCurrentDateTimeinDatabaseFormat], nil];
        
        if (success == YES) {
            
            NSLog(@"TBL_FSR_Locations insert successfull");
            
            BOOL checkInternetConnectivity = [SharedUtils isConnectedToInternet];
            if (checkInternetConnectivity==YES) {
                [appDelegate sendLocationDataToServer];
            }
        }
    }@catch (NSException *exception)
    {
        NSLog(@"Exception while inserting FSR Location data in database: %@", exception.reason);
    }
    @finally {
        [db close];
    }
}

#pragma mark UITextField Delegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self.view endEditing:YES];
    
    if (textField == txtStartTime) {
        selectedTextField = txtStartTime;
        [self openTimePicker];
        
    } else if (textField == txtEndTime) {
        selectedTextField = txtEndTime;
        
        if (txtStartTime.text.length == 0) {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"Please select start time first" withController:self];
        } else {
            [self openTimePicker];
        }
    } else {
        selectedTextField = txtReason;
        
        NSMutableArray *contentArray = [[NSMutableArray alloc]init];
        if (arrReason.count>0) {
            for (NSDictionary *dic in arrReason) {
                [contentArray addObject:[dic valueForKey:@"Code_Description"]];
            }
        }
        
        SalesWorxPopOverViewController *popOverVC = [[SalesWorxPopOverViewController alloc]init];
        popOverVC.popOverContentArray = contentArray;
        popOverVC.salesWorxPopOverControllerDelegate=self;
        popOverVC.disableSearch=YES;
        popOverVC.titleKey = @"Select Reason";
        UINavigationController *popOverNavigationCroller = [[UINavigationController alloc]initWithRootViewController:popOverVC];
        UIPopoverController *paymentPopOverController = [[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        paymentPopOverController.delegate=self;
        popOverVC.popOverController=paymentPopOverController;
        [paymentPopOverController setPopoverContentSize:CGSizeMake(266, 273) animated:YES];
        [paymentPopOverController presentPopoverFromRect:txtStartTime.dropdownImageView.frame inView:txtStartTime.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    return NO;
}

-(void)openTimePicker {
    
    SalesWorxTimePickerViewController *popOverVC = [[SalesWorxTimePickerViewController alloc]init];
    popOverVC.didSelectTimeDelegate = self;
    
    if (selectedTextField == txtStartTime) {
        popOverVC.titleString = @"Select Start Time";
    } else {
        popOverVC.titleString = @"Select End Time";
    }
    
    UINavigationController *popOverNavigationCroller = [[UINavigationController alloc]initWithRootViewController:popOverVC];
    UIPopoverController *paymentPopOverController = [[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
    paymentPopOverController.delegate=self;
    [paymentPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
    [paymentPopOverController presentPopoverFromRect:selectedTextField.dropdownImageView.frame inView:selectedTextField.rightView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark UIDatePickerDelegate
-(void)didSelectTime:(NSString *)selectedTime
{
    if (selectedTextField == txtStartTime) {
        selectedStartTime = selectedTime;
        txtEndTime.text = @"";
        selectedEndTime = @"";
    } else {
        selectedEndTime = selectedTime;
        
        NSDateFormatter* df = [NSDateFormatter new];
        [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [df setLocale:usLocale];
        NSDate *startTime = [df dateFromString:selectedStartTime];
        NSDate *endTime = [df dateFromString:selectedEndTime];
    
        
        NSComparisonResult result = [endTime compare:startTime];
        if(result==NSOrderedDescending) {
            NSLog(@"startTime is in the past");
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Error" andMessage:@"End time should be greater than start time" withController:self];
            return;
        }
    }
    [selectedTextField setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithTime scrString:selectedTime]];
}

#pragma mark UIPopOverDelegate
-(void)didSelectPopOverController:(NSIndexPath*)selectedIndexPath
{
    NSString *selectedReason = [[arrReason objectAtIndex:selectedIndexPath.row] valueForKey:@"Code_Description"];
    selectedReasonCodeValue = [[arrReason objectAtIndex:selectedIndexPath.row] valueForKey:@"Code_Value"];
    if (![selectedReason isEqualToString:selectedTextField.text]) {
        txtViewComments.text = @"";
        selectedTextField.text = selectedReason;
        if ([selectedTextField.text isEqualToString:@"Others"]) {
            viewComments.hidden = NO;
        } else {
            viewComments.hidden = YES;
        }
    }
}

#pragma mark UITextView Delegate methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView.text.length == 0 && ([text isEqualToString:@"\n"] || [text isEqualToString:@" "])) {
        return NO;
    }
    
    if ([textView.text length] <= 500) {
        return YES;
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Limit Reached" andMessage:@"please make sure that your comments are less than 500 characters" withController:self];
        return NO;
    }
}

#pragma mark Send data to Server
-(void)sendDataToServer
{
    SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
    if([syncObject licenseVerification])
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.isFromSync = 2;
        single.isActivated = YES;
        [[DataSyncManager sharedManager]showCustomLoadingIndicator:self.view];
        [[SWDatabaseManager retrieveManager] writeIdleReasonXMLString];
        [[SWDatabaseManager retrieveManager] setTarget:self];
        [[SWDatabaseManager retrieveManager] setAction:@selector(xmlIdleReasonDone:)];
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"License validation error" andMessage:@"You license has been expired or contact your administrator for further assistance" withController:self];
    }
}

-(void)xmlIdleReasonDone:(NSString *)st
{
    if([st isEqualToString:@"done-03"])
    {
        Singleton *single = [Singleton retrieveSingleton];
        NSString *XMLPassingString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)single.xmlParsedString, NULL, CFSTR("!$&'()*+,-./:;=?@_~"), kCFStringEncodingUTF8));
        [self sendRequestForExecWithUserName:[SWDefaults username] andPassword:[SWDefaults password] andProcName:@"sync_MC_ExecSave_FSR_Idle_Time_Reason" andProcParam:@"IdleReasonInfo" andProcValue: XMLPassingString];
    }
    else
    {
        //hide local notification
        [[UIApplication sharedApplication]setApplicationIconBadgeNumber:0];
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        [[DataSyncManager sharedManager]hideCustomIndicator];
        [self stopAnimationofIdleReason];
        [self performSelector:@selector(stopAnimationofIdleReason) withObject:nil afterDelay:0.1];
    }
}

- (void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue
{
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData *dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]  ] ;
    NSString *serverAPI = [NSString stringWithFormat:@"http://%@/SWX/Sync/",[testServerDict stringForKey:@"serverLink"]];
    NSString *strurl = [serverAPI stringByAppendingString:@"Exec"];
    NSURL *url = [NSURL URLWithString:strurl];

    
    NSString *strDeviceID = [[DataSyncManager sharedManager]getDeviceID];
    AFHTTPClient *request = [[HttpClient sharedManager] initWithBaseURL:url];
    
    NSString *strParams =[[NSString alloc] init];
    NSString *strValues=[[NSString alloc] init];
    NSString *strName=procName;
    strParams=[strParams stringByAppendingFormat:@"&ProcParams=%@",procPram]; //
    strValues=[strValues stringByAppendingFormat:@"&ProcValues=%@",procValue];
    NSString *strProcedureParameter =[strParams stringByAppendingFormat:@"%@",strValues];
    
    NSString *myRequestString =[NSString stringWithFormat:@"Username=%@&Password=%@&ResponseFormat=JSON&DeviceID=%@&ClientVersion=%@&ProcName=%@%@",username,[[DataSyncManager sharedManager] sha1:password],strDeviceID,ClientVersion,strName,strProcedureParameter];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    NSData *myRequestData = [ NSData dataWithBytes: [ myRequestString UTF8String ] length: [ myRequestString length ] ];
    [urlRequest setURL:[NSURL URLWithString:[serverAPI stringByAppendingString:@"Exec"]]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:myRequestData];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest] ;
    
    [request registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operationQ,id responseObject)
     {
         NSString *responseText = [operationQ responseString];
         NSMutableArray *resultArray = [NSMutableArray arrayWithArray:[responseText JSONValue]];
         
         if([procName isEqualToString:@"sync_MC_ExecSave_FSR_Idle_Time_Reason"])
         {
             resultArray = [responseText JSONValue];
             NSString *ProcResponse = [[resultArray objectAtIndex:0] stringForKey:@"ProcResponse"];
             if([ProcResponse isEqualToString:@"FSR Idle Reasons imported successfully"])
             {
                 [[SWDatabaseManager retrieveManager] executeNonQuery:@"Delete from TBL_FSR_Idle_Time_Reason"];
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofIdleReason) withObject:nil afterDelay:0.1];
                 
                 UIAlertAction *okAction = [UIAlertAction
                                            actionWithTitle:NSLocalizedString(KAlertOkButtonTitle, nil)
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                     [self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
                 }];
                 
                 [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Success" andMessage:ProcResponse andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
             }
             else {
                 [[DataSyncManager sharedManager] hideCustomIndicator];
                 [self performSelector:@selector(stopAnimationofIdleReason) withObject:nil afterDelay:0.1];
                 [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:ProcResponse withController:self];
             }
         }
     }
                                     failure:^(AFHTTPRequestOperation *operationQ, NSError *error)
     {
         [[DataSyncManager sharedManager] hideCustomIndicator];
         [self performSelector:@selector(stopAnimationofIdleReason) withObject:nil afterDelay:0.1];
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Server unreachable at the moment" withController:self];
     }];
    
    //call start on your request operation
    [operation start];
}

- (void)stopAnimationofIdleReason
{
    [[[HttpClient sharedManager] operationQueue] cancelAllOperations];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
