//
//  LicensingViewController.m
//  SalesWars

#import "LicensingViewController.h"
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "SWSessionConstants.h"
#import "TypeViewController.h"
#import "FMDBHelper.h"
#import "SharedUtils.h"
#import "LoginViewController.h"
#import "Constant.h"

@interface LicensingViewController ()

@end

@implementation LicensingViewController

@synthesize customerID, target,action,TFCustomerID,TFLicenseType,lblPowerdBy,lblCustomerID,lblLicenseType,btnActivate,viewContent;

- (id)init {
    self = [super init];
    
    if (self) {
        
        AppControl *appCtrl=[AppControl retrieveSingleton];
        NSString* appCtrlQry=@"select * from TBL_App_Control";
        NSArray* appCtrlArray=[FMDBHelper executeQuery:appCtrlQry];
        NSLog(@"app control in licensing %@",appCtrlArray);
        if ([appCtrl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
            [self setTitle:@"SalesWars - License Activation"];
        }
        else{
            //no chance of app control database didnt even download (actionation not done yet)
            [self setTitle:@"SalesWars - License Activation"];
        }
        
        //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        verifyLicenceController = nil;
        verifyLicenceController = [[SynchroniseViewController alloc] init] ;
        
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setNaviTitle];
    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    loadingView.loadingLabel.text = @"Please Wait..";
    [Flurry logEvent:@"Licensing View"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.view.backgroundColor =[UIColor whiteColor];
}

-(void)setNaviTitle{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_top"] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:EditableTextFieldSemiFontOfSize(16.0), NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.title = @"License Activation";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight){
        return YES;
    }
    return NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    TFCustomerID.delegate = self;
    TFLicenseType.delegate = self;
    
    viewContent .layer.masksToBounds = NO;
    viewContent.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    viewContent.layer.shadowColor = [UIColor colorWithRed:210.f/255.f green:210.f/255.f blue:210.f/255.f alpha:1.f].CGColor;
    viewContent.layer.shadowRadius = 12.0f;
    viewContent.layer.shadowOpacity = 10.0f;
    viewContent.layer.cornerRadius = 4.0;
    viewContent.backgroundColor = [UIColor whiteColor];
    
    
    TFLicenseType.rightViewMode = UITextFieldViewModeAlways;
    TFLicenseType.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow_DropDown"]];
    
    //UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(96, 40, 130, 20)];
   // [textField setLeftViewMode:UITextFieldViewModeAlways];
    //TFLicenseType.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchIccon.png"]];
    
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Arrow_DropDown"]];
    arrow.frame = CGRectMake(0.0, 0.0, arrow.image.size.width+30.0, arrow.image.size.height);
    arrow.contentMode = UIViewContentModeCenter;
    
    TFLicenseType.rightView = arrow;
    TFLicenseType.rightViewMode = UITextFieldViewModeAlways;
    
    btnActivate.layer.cornerRadius = 4.0f;
    btnActivate.layer.masksToBounds = YES;
    
    //[[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    ClientVersion =avid;
    customerID = @"C16K82563";
    licenceType = @"Evaluation - 30 days";
    [self updateCustomerIDAndLicType];
}

-(void)updateCustomerIDAndLicType{
    TFCustomerID.text = customerID;
    TFLicenseType.text = licenceType;
}


- (IBAction)btnActivateClicked:(id)sender {
    [self selectServerButton];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC{
    popoverController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    
    return YES;
}
-(void)selectServerButton {
    
    BOOL checkInternetConnectivity= [SharedUtils isConnectedToInternet];
    
    if (checkInternetConnectivity==YES) {
        
        BOOL isValid=NO;
        NSString *lincesnValue ;
        NSString *timePeriod = @"";
        if (customerID.length > 0 && licenceType.length > 0)
        {
            // customerID
            if([licenceType isEqualToString:@"Evaluation - 30 days"])
            {
                lincesnValue = @"EVAL_TIME";
                timePeriod = @"30";
            }
            else if([licenceType isEqualToString:@"Evaluation - 60 days"])
            {
                lincesnValue = @"EVAL_TIME";
                timePeriod = @"60";
            }
            else if([licenceType isEqualToString:@"Evaluation - 90 days"])
            {
                lincesnValue = @"EVAL_TIME";
                timePeriod = @"90";
            }
            else
            {
                lincesnValue = @"PERMANENT";
                timePeriod = @"0";
            }
            // [sessionService verifyLogin:customerID andPassword:licenceType];
            
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            isValid=[verifyLicenceController startLicenceingWithCustomerIF:customerID andLicenceType:lincesnValue andLicenceLimit:timePeriod];
            
            if (isValid)
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [[NSUserDefaults standardUserDefaults]setValue:customerID forKey:@"Customer_ID"];
                
                UIAlertAction *okAction = [UIAlertAction
                                            actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                           {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                    [self.target performSelector:self.action withObject:nil];
#pragma clang diagnostic pop
                }];
                
                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:@"License activation completed successfully" andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
            }
            else
            {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            }
        }
        else
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [SWDefaults showAlertAfterHidingKeyBoard:@"Please fill all fields" andMessage:@"" withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Internet unavailable" andMessage:@"Please connect to the internet and try again" withController:self];
    }
}

- (void)typeChanged:(NSString *)type {
    [popoverController dismissPopoverAnimated:YES];
    licenceType=type;
    [self updateCustomerIDAndLicType];
}


#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger maxLength = 50;
    if (textField == TFCustomerID){
        maxLength = MAX_LENGTH_CUSTOMERID;
    }
    
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= maxLength;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField == TFCustomerID) {
        customerID=textField.text;
    } else {
        licenceType=textField.text;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField == TFLicenseType) {
        [self.view endEditing:YES];
        
        TypeViewController *serverSelectionViewController = [[TypeViewController alloc] init]  ;
        [serverSelectionViewController setTarget:self];
        [serverSelectionViewController setAction:@selector(typeChanged:)];
        UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:serverSelectionViewController]  ;
        popoverController=nil;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
        [popoverController presentPopoverFromRect:TFLicenseType.rightView.bounds inView:TFLicenseType.rightView permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        popoverController.delegate=self;
        return  NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    
    if ([self isViewLoaded] && self.view.window == nil){
        self.view = nil;
    }
}


@end
