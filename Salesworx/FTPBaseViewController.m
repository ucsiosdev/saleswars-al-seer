//
//  FTPBaseViewController.m
//  FastTrackParking
//
//  Created by Saqib Khan on 10/30/13.
//  Copyright (c) 2013 Saqib Khan. All rights reserved.
//

#import "FTPBaseViewController.h"
#import "SWFoundation.h"
#import "MedRepDefaults.h"
@interface FTPBaseViewController ()

@end

@implementation FTPBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x169C5C);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x169C5C);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,
                                        nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x169C5C)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x169C5C)];
    }
}

- (BOOL)prefersStatusBarHidden{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
