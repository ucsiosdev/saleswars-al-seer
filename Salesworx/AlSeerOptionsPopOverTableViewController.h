//
//  AlSeerOptionsPopOverTableViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/4/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerOptionsPopOverTableViewController : UITableViewController

@property(strong,nonatomic)UINavigationController * navController;

@end
