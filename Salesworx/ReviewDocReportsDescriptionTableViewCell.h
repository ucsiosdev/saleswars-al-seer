//
//  ReviewDocReportsTableViewCell.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 1/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface ReviewDocReportsDescriptionTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *itemCodeLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *quantityLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *unitPriceLbl;
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *totalLbl;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *itemDescLbl;
@end
