//
//  ReturnNewViewController.h
//  Salesworx
//
//  Created by Saad Ansari on 2/11/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "AnimationUtility.h"
#import "ReturnViewController.h"
@interface ReturnNewViewController : SWViewController<UIPopoverControllerDelegate,OldReturnControllerDelegate>

{
    IBOutlet UITextField *txtProductQty;
    IBOutlet UITextField *txtDefBonus;
    IBOutlet UITextField *txtWholePrice;
    IBOutlet UITextField *txtRetailPrice;
    IBOutlet UITextField *txtLot;
    
    IBOutlet UILabel *lblRetailPrice;
    IBOutlet UILabel *lblWholePrice;
    IBOutlet UILabel *lblProductQty;
    IBOutlet UILabel *lblExpiryDate;
    IBOutlet UILabel *lblProductName;
    IBOutlet UILabel *lblCustomerName;
    IBOutlet UILabel *lblReturnType;
    IBOutlet UILabel *lblProductBonus;
    IBOutlet UILabel *lblLotNumber;
    IBOutlet UILabel *lblRPAmount;
    IBOutlet UILabel *lblWPAmount;
    
    IBOutlet UIButton *btnExpiryDate;
    IBOutlet UIButton *btnReturnType;
    IBOutlet UIButton *dragButton;
    IBOutlet UIButton *bonusBtn;
    IBOutlet UIButton *addBtn;

    
    IBOutlet UITableView *productTableView;
    IBOutlet UISearchBar *candySearchBar;
    BOOL bSearchIsOn;
    
    IBOutlet UIView *bottomOrderView;
    
    ReturnViewController *oldSalesOrderVC;
    UIPopoverController *collectionTypePopOver;
    
    NSMutableDictionary *productDictionary;
    NSMutableDictionary *mainProductDict;
    
    NSMutableArray *productArray;
    NSMutableArray *filteredCandyArray;
    NSMutableArray *finalProductArray;
    NSMutableSet* _collapsedSections;
    
    BOOL isSaveOrder;
    BOOL isStockToggled;
    BOOL isErrorSelectProduct;

    
    IBOutlet UIView *dragParentView;
    IBOutlet UIView *dragChildView;
}


@property (nonatomic,strong)NSMutableDictionary *customerDict;

-(IBAction)expiryDatebuttonAction:(id)sender;
-(IBAction)addbuttonAction:(id)sender;
-(IBAction)pullUpbuttonAction:(id)sender;
@end
