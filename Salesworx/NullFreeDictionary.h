//
//  NullFreeDictionary.h
//  Salesworx
//
//  Created by Unique Computer Systems on 4/8/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NullFreeDictionary : NSMutableDictionary

- (id)safeObjectForKey:(id)aKey;

@end
