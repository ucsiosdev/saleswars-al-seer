    //
//  SWNewActivationViewController.m
//  SWSession
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWNewActivationViewController.h"
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "SWSession.h"
#import "SWDatabaseManager.h"
#import <sqlite3.h>
#import "SSKeychain.h"
#import "Constant.h"
#import "SWAppDelegate.h"

@interface SWNewActivationViewController ()

@end

@implementation SWNewActivationViewController

@synthesize alertDisplayed,invalidCredentials;

#define kLoginField 1
#define kPasswordField 2
#define kSNameField 3
#define kSLinkField 4



@synthesize target,action,isDemo;
- (id)initWithNoDB {
    self = [super init];
    
    if (self) {
        [self setTitle:@"New Activation"];
        isRootView=YES;
        isActivation = YES;
        ////NSLog(@"From Appdelegate");
    }
    
    return self;
}

- (id)init {
    self = [super init];
    
    if (self) {
        isActivation = NO;
        [self setTitle:@"New Activation"];
        isRootView=NO;
        ////NSLog(@"From Lofig");
    }
    
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initObj];
    [self setupView];
    [self loadSyncProgressBar];
    [self handleActivateNewAlseerCase];
    
    
    usernameTextField.delegate = self;
    passwordTextField.delegate = self;
    servernameTextField.delegate = self;
    serverURLTextField.delegate = self;
    
    [Flurry logEvent:@"Activation View"];
    appData=[DataSyncManager sharedManager];
    progressHUD.delegate = self;
    
    serverDict = [NSMutableDictionary dictionary];
    editServerDict = [NSMutableDictionary dictionary ];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    NSError *error;
    NSData* dataDict = [[SWDefaults selectedServerDictionary] dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *testServerDict = [NSMutableDictionary dictionaryWithDictionary:[djsonSerializer deserializeAsDictionary:dataDict error:&error]];
    serverDict =[NSMutableDictionary dictionaryWithDictionary:[testServerDict mutableCopy]];
    [self updateServerDetails];
    
    login = [SWDefaults usernameForActivate];
    password = [SWDefaults passwordForActivate];
    
    editServerName1 = [serverDict stringForKey:@"serverName"];
    editServerLink = [serverDict stringForKey:@"serverLink"];
    
    if (isRootView)
    {
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
    }
    else {
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Upload Database" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
    }
}

-(void)handleActivateNewAlseerCase {
    if (isRootView){
        [activateButton setTitle:@"Activate" forState:UIControlStateNormal];
        backToSignInButton.hidden = YES;
        btnActivateConstraintTopSpace.constant = 33;
    } else {
        [activateButton setTitle:@"Upload Database" forState:UIControlStateNormal];
        backToSignInButton.hidden = NO;
        btnActivateConstraintTopSpace.constant = 19;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
     self.view = nil;
}

-(void)initObj {
    
    logoImageView.hidden = YES;
    lblVersion.hidden = YES;
    
    activateButton.layer.cornerRadius = 4.0f;
    activateButton.layer.masksToBounds = YES;
   
    signInNowButton.layer.cornerRadius = 4.0f;
    signInNowButton.layer.masksToBounds = YES;
    signInNowButton.enabled = false;
    activationCompletedView.hidden = YES;
}

-(void)setupView{
 
    signInNowButton.backgroundColor = UIColorFromRGB(0xE3E4E6);
    
    // set border and other stuff
    [self setBorderShadowAndCornder:credentialsView];
    [self setBorderShadowAndCornder:activatingView];
    
    //set font
    lblUserNameStatic.font = kFontWeblySleekSemiBold(14);
    usernameTextField.font = kFontWeblySleekSemiBold(14);
    lblPasswordStatic.font = kFontWeblySleekSemiBold(14);
    passwordTextField.font = kFontWeblySleekSemiBold(14);
    lblServerLocationsStatic.font = kFontWeblySleekSemiBold(14);
    servernameTextField.font = kFontWeblySleekSemiBold(14);
    lblSeverURLStatic.font = kFontWeblySleekSemiBold(14);
    servernameTextField.font = kFontWeblySleekSemiBold(14);
    serverURLTextField.font = kFontWeblySleekSemiBold(14);
    
    syncStatusLbl.font = kFontWeblySleekSemiBold(16);
    
    lblNewActivationStatic.font = kFontWeblySleekSemiBold(25);
    lblActvatingUserStatic.font = kFontWeblySleekSemiBold(25);

    //set color
    lblUserNameStatic.textColor =  UIColorFromRGB(colorDarkGrey);
    lblPasswordStatic.textColor =  UIColorFromRGB(colorDarkGrey);
    lblServerLocationsStatic.textColor =  UIColorFromRGB(colorDarkGrey);
    lblSeverURLStatic.textColor =  UIColorFromRGB(colorDarkGrey);
    syncStatusLbl.textColor =  UIColorFromRGB(colorBlack);
    
    usernameTextField.textColor =  UIColorFromRGB(colorBlack);
    passwordTextField.textColor =  UIColorFromRGB(colorBlack);
    servernameTextField.textColor =  UIColorFromRGB(colorBlack);
    servernameTextField.textColor =  UIColorFromRGB(colorBlack);
    lblNewActivationStatic.textColor =  UIColorFromRGB(colorBlack);
    lblActvatingUserStatic.textColor =  UIColorFromRGB(colorBlack);
    serverURLTextField.textColor = UIColorFromRGB(colorBlack);
}

-(void)setBorderShadowAndCornder:(UIView*)customeView {
    customeView.layer.masksToBounds = NO;
    customeView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    customeView.layer.shadowColor = [UIColor colorWithRed:210.f/255.f green:210.f/255.f blue:210.f/255.f alpha:1.0].CGColor;
    customeView.layer.shadowRadius = 12.0f;
    customeView.layer.shadowOpacity = 10.0f;
    customeView.layer.cornerRadius = 4.0;
    customeView.backgroundColor = [UIColor whiteColor];
}

-(IBAction)activateNewUserTapped:(id)sender{
    [self showActivate];
}

- (IBAction)backToSignInButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signInNowButtonTapped:(id)sender {
    NSLog(@"\n ** signInNowButtonTapped **\n\n");
    
    if (self.target == nil || self.action == nil){
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        NSLog(@"is activation target called");
        [self.target performSelector:self.action withObject:nil];
        NSLog(@"is activation target completed");
        
#pragma clang diagnostic pop
    }
}

- (void)showActivate {
    
    alertDisplayed=NO;
    [self.view endEditing:YES];
    
    CJSONDeserializer *djsonSerializer = [CJSONDeserializer deserializer];
    CJSONSerializer *jsonSerializer = [CJSONSerializer serializer] ;
    
    NSError *error;
    NSData* data = [[SWDefaults serverArray] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableArray *testServerArray = [NSMutableArray arrayWithArray:[djsonSerializer deserializeAsArray:data error:&error]  ];
    if([self validateInput])
    {
        for(int i=0 ; i<[testServerArray count] ; i++ )
        {
            NSDictionary *row = [testServerArray objectAtIndex:i];
            if([[row stringForKey:@"serverName"] isEqualToString:[serverDict stringForKey:@"serverName"]])
            {
                if(editServerName1.length > 0)
                {
                    [serverDict setValue:editServerName1 forKey:@"serverName"];
                }
                if(editServerLink.length > 0)
                {
                    [serverDict setValue:editServerLink forKey:@"serverLink"];
                }
                
                [testServerArray removeObjectAtIndex:i];
                [testServerArray addObject:serverDict];
                NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
                NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
                [SWDefaults setSelectedServerDictionary:stringDataDict];
                break;
            }
        }
        if([serverDict count]==0)
        {
            serverDict = editServerDict;
            [testServerArray addObject:serverDict];
            NSData *dataDict = [jsonSerializer serializeObject:serverDict error:&error];
            NSString* stringDataDict = [[NSString alloc] initWithData:dataDict encoding:NSUTF8StringEncoding] ;
            [SWDefaults setSelectedServerDictionary:stringDataDict];
        }
        NSData *datas = [jsonSerializer serializeObject:testServerArray error:&error];
        NSString* stringData = [[NSString alloc] initWithData:datas encoding:NSUTF8StringEncoding];
        [SWDefaults setServerArray:stringData];
        
        if (isRootView)
        {
            if ([SWDefaults usernameForActivate].length > 0 && [SWDefaults passwordForActivate].length > 0)
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                
                viewSync=nil;
                viewSync.delegate=nil;
                viewSync = [[SynViewController alloc] init];
                viewSync.delegate=self;
                [viewSync setTarget:self];
                [viewSync setAction:@selector(activationDone:)];
                [viewSync setRequestForSyncActivateWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"PLease fill all fields", nil) withController:self];
            }
        }
        else
        {
            [self.view addSubview:progressHUD];
            Singleton *single = [Singleton retrieveSingleton];
            single.isActivated = NO;
            viewSync=nil;
            viewSync.delegate=nil;
            viewSync = [[SynViewController alloc] init];
            viewSync.delegate=self;
            [viewSync setTarget:self];
            [viewSync setAction:@selector(uploadDone:)];
            [viewSync setRequestForSyncUpload];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"PLease fill all fields", nil) withController:self];
    }
}

- (void)activationDone:(NSString *)msg{
    
    
    NSArray* arr=[[NSArray alloc]init];
    arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select Username from TBL_User"]];
    if ([arr count]>0) {
        NSString * userNameStr = [[arr objectAtIndex:0]valueForKey:@"Username"];
        
        if ([userNameStr isEqualToString:@"demo"]) {
            NSLog(@"it is a demo");
        }
        else
        {
            NSLog(@"its not a demo %@", msg);
            
            isDemo=NO;
            [SSKeychain setPassword:@"registereduser" forService:@"registereduser" account:@"user"];
        }
        
        
        if([msg isEqualToString:@"error"])
        {
            [SWDefaults setIsActivationDone:@"NO"];
            [progressHUD removeFromSuperview];
        }
        else
        {
            [SWDefaults setIsActivationDone:@"YES"];
            NSString *temp =[[SWDatabaseManager retrieveManager] verifyLogin:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate]];
            if ([temp isEqualToString:@"Done"])
            {
                [SWDefaults setLastSyncStatus:KNotApplicable];
                [SWDefaults setLastSyncOrderSent:KNotApplicable];
                [SWDefaults setLastSyncType:KNotApplicable];
                [SWDefaults setLastSyncDate:@"Never"];
                [SWDefaults setLastFullSyncDate:[SWDefaults fetchCurrentDateTimeinDatabaseFormat]];
                
                SWAppDelegate *appDel = (SWAppDelegate*)[[UIApplication sharedApplication]delegate];
                [appDel registerforPushNotificationwithDeviceToken:[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceID"]];
                
                
                viewSync.delegate=nil;
                viewSync=nil;
                viewSync = [[SynViewController alloc] init];
                [viewSync viewDidLoad];
                viewSync.delegate=self;
                [viewSync setTarget:self];
                [viewSync setAction:@selector(activationCompleted)];
                [viewSync sendRequestForExecWithUserName:[SWDefaults usernameForActivate] andPassword:[SWDefaults passwordForActivate] andProcName:@"sync_MC_GetLastDocumentReferenceAll" andProcParam:@"SalesRepID" andProcValue:[[SWDefaults userProfile] stringForKey:@"SalesRep_ID"]];
                
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:temp withController:self];
            }
            [SWDefaults setActivationStatus:@"YES"];
        }

    }
    else
    {
        if (alertDisplayed==NO) {
            UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                alertDisplayed=YES;
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Invalid Credentials" andMessage:@"Login/Server  credentials are invalid" andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
            invalidCredentials=YES;
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

- (void)uploadDone:(NSString *)msg {
    [progressHUD removeFromSuperview];
   
    if(![msg isEqualToString:@"error"])
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Database Uploaded Successfully, please enter credentials to activate new user", nil) withController:self];
        
        isRootView=YES;
        [SWDefaults setIsActivationDone:@"NO"];
        
        //handle activate view again
        credentialsView.hidden = NO;
        activatingView.hidden = YES;
        usernameTextField.text = @"";
        passwordTextField.text = @"";
        servernameTextField.text = @"";
        serverURLTextField.text = @"";
        
        editServerName1 = nil;
        editServerLink = nil;
        login = nil ;
        password = nil ;
        
        [self handleActivateNewAlseerCase];
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];
        self.navigationItem.hidesBackButton = YES;
        [SWDefaults setUsernameForActivate:@""];
        [SWDefaults setPasswordForActivate:@""];
        [SWDefaults setPassword:@""];
        [SWDefaults setUsername:@""];
    }
}
-(void)viewDidDisappear:(BOOL)animated
{
    popoverController=nil;
    window=nil;
    applicationViewController=nil;
    login=nil;
    password=nil;
    serverDict=nil;
    
    editServerLink=nil;
    editServerName1=nil;
    newServerDict=nil;
    loadingView=nil;
    appData=nil;
    bacgroundBar=nil;
    progressHUD=nil;
    progressHUD.delegate=nil;
    [[HttpClient sharedManager].operationQueue cancelAllOperations];
    [viewSync cancelHTTPRequest];
    viewSync.delegate = nil;
    viewSync=nil;
}

- (void)serverChanged:(NSMutableDictionary *)d {
    serverDict=nil;
    serverDict=d;
    
    editServerLink = [d stringForKey:@"serverLink"];
    editServerName1 = [d stringForKey:@"serverName"];
    [self updateServerDetails];
    
    [popoverController dismissPopoverAnimated:YES];
}

-(void)updateServerDetails{
    
    if ([serverDict stringForKey:@"serverName"] != nil ){
        servernameTextField.text = [serverDict stringForKey:@"serverName"];
    }
    
    if ([serverDict stringForKey:@"serverLink"] != nil ){
        serverURLTextField.text = [serverDict stringForKey:@"serverLink"];
    }
    
    usernameTextField.text = [SWDefaults usernameForActivate];
    passwordTextField.text = [SWDefaults passwordForActivate];
}


#pragma mark UIView delegate
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[HttpClient sharedManager].operationQueue cancelAllOperations];
    [viewSync cancelHTTPRequest];
    viewSync.delegate = nil;
    viewSync=nil;
   //.. [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark UIPopOver Delegates
- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController1{
  //..  [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    popoverController1=nil;
}
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger maxLength = 50;
    if (textField == usernameTextField){
        maxLength = MAX_LENGTH_USERNAME;
    }else if (textField == passwordTextField){
        maxLength = MAX_LENGTH_PASSWORD;
    }
    
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= maxLength;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.navigationItem setRightBarButtonItem:nil];

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    if (textField.tag == kLoginField) {
        login=textField.text;
        [SWDefaults setUsername:textField.text];
        [SWDefaults setUsernameForActivate:textField.text];
    }
    else if (textField.tag == kPasswordField) {
        password=textField.text;
        [SWDefaults setPassword:textField.text];
        [SWDefaults setPasswordForActivate:textField.text];
        
    }
    else if (textField.tag == kSNameField) {
        editServerName1=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverName"];

    }
    else if (textField.tag == kSLinkField) {
        editServerLink=textField.text;
        [editServerDict setValue:textField.text forKey:@"serverLink"];

    }
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Activate" style:UIBarButtonItemStylePlain target:self action:@selector(showActivate)] ];

    return YES;
}

- (void)activationCompleted {
    
    if (progressHUD != nil){
        [progressHUD removeFromSuperview];
    }
    
    activationCompletedView.hidden = NO;
    signInNowButton.enabled = YES;
    [signInNowButton setBackgroundColor:UIColorFromRGB(0x47B681)];
    [syncProgressBar.countingLabel setHidden:true];
}

- (BOOL)validateInput {

    if (login.length > 0 && password.length > 0 && editServerName1.length > 0 && editServerLink.length > 0 )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)updateActivationTextDelegate:(NSString *)synchDate{
    
    NSLog(@"\n\n ** Sync Date:%@ **\n\n", synchDate);
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    credentialsView.hidden = YES;
    activatingView.hidden = NO;
}

- (void)updateProgressDelegateWithType:(MBProgressHUDMode)type andTitle:(NSString *)title andDetail:(NSString *)detail andProgress:(float)progress{
   
    NSLog(@"\n\n ** Title:%@ , Detail: %@, Progress: %f **\n\n", title,detail,progress);
    syncStatusLbl.text = [NSString stringWithFormat:@"Sync status: %@",title];
    [self updateSyncProgressBar:progress];
}


#pragma mark Load Sync progress Chart
-(void)loadSyncProgressBar
{
    [syncProgressBar removeFromSuperview];
    syncProgressBar = [[PNCircleChart alloc] initWithFrame:syncProgressBar.frame total:[NSNumber numberWithInt:1] current:0 clockwise:YES shadow:YES shadowColor:UIColorFromRGB(0xE3E4E6) check:NO lineWidth:@30.0f];
    
    [syncProgressBar.countingLabel setTextColor:[UIColor blackColor] ];
    [syncProgressBar.countingLabel setFont:kFontWeblySleekSemiBold(36)];
    
    syncProgressBar.backgroundColor = [UIColor clearColor];
    [syncProgressBar setStrokeColor:[UIColor clearColor]];
    [syncProgressBar setStrokeColorGradientStart:UIColorFromRGB(0x4A92D5)];
    [syncProgressBar strokeChart];
    [activatingView addSubview:syncProgressBar];
}


-(void)updateSyncProgressBar:(CGFloat)progress
{
    NSLog(@"\n\n ***  UpdateSyncProgressBar: %f  **\n\n",progress);
    if (progress == 0 && lastProgress <= 0) {
        [self loadSyncProgressBar];
    }
    else if (lastProgress != progress) {
        lastProgress = progress;
        [syncProgressBar updateChartByCurrent:[NSNumber numberWithFloat:progress]];
    }
}

@end

