//
//  ReturnTypeViewController.h
//  SWCustomer
//
//  Created by msaad on 1/2/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"

@interface ReturnTypeViewController : SWTableViewController 
{
    NSArray *types;
//       id target;
    SEL action;
    NSString *parentType;
    
    
    
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

- (id)initWithEXP;
- (id)initWithReturnType;
- (id)initWithGoods;
@end

//istuart@technicaltalent.co.uk