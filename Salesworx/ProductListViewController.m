//
//  ProductListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "ProductListViewController.h"
#import "SWSection.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "ProductBrandFilterViewController.h"

@interface ProductListViewController ()
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation ProductListViewController
@synthesize target;
@synthesize action,products,searchData,category;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"Products";
        titleLabel.font = kFontWeblySleekSemiBold(19);
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.navigationItem.titleView = titleLabel;
        searchData=[NSMutableArray array];
    }
    return self;
}

- (id)initWithCategory:(NSDictionary *)c {
    self = [self init];
    
    if (self) {
        [self setTitle:@"Select Product"];
        category=[NSDictionary dictionaryWithDictionary:c] ;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"\n\n*** Class name:: ProductListViewController  ***\n\n");
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
    
    sectionSearch = [[NSMutableDictionary alloc] init];
    [Flurry logEvent:@"Product List View"];
    
    // loading view
    [SWDefaults setFilterForProductList:nil];
    
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    loadingView.backgroundColor = [UIColor whiteColor];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    isBarCode=NO;
    
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(8, 8, self.view.frame.size.width-16 , self.view.frame.size.height-138)];
    subView.backgroundColor = [UIColor whiteColor];
    
    [self addShadowOnCustomLayer:subView.layer];
    
    // table view
    mainTableView=[[UITableView alloc] initWithFrame:CGRectMake(0,52, subView.bounds.size.width, subView.bounds.size.height-57) style:UITableViewStylePlain] ;
    [mainTableView setDataSource:self];
    [mainTableView setDelegate:self];
    [mainTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    if (@available(iOS 15.0, *)) {
        mainTableView.sectionHeaderTopPadding = 0;
    }
    
    // search display controller
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, subView.frame.size.width, 52)];
    searchBar.delegate = self;
    searchBar.showsCancelButton = YES;
    
    subView.layer.shadowColor = [UIColor blackColor].CGColor;
    subView.layer.shadowOffset = CGSizeMake(2, 2);
    subView.layer.shadowOpacity = 0.1;
    subView.layer.shadowRadius = 1.0;
    
    [subView addSubview:searchBar];
    [subView addSubview:mainTableView];
    [subView addSubview:loadingView];
    
    [self.view addSubview:subView];
    
    
    // Count label
    countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)];
    [countLabel setText:@""];
    [countLabel setTextAlignment:NSTextAlignmentCenter];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor:[UIColor whiteColor]];
    [countLabel setFont:kFontWeblySleekSemiBold(14.0f)];
    
    
    // popovers
    ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
    [filterViewController setTarget:self];
    [filterViewController setAction:@selector(filterChanged)];
    [filterViewController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filterViewController] ;
    
    filterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
    [filterPopOver setDelegate:self];
    [mainTableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    totalLabelButton = [[UIBarButtonItem alloc] init];
    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:countLabel];
    [totalLabelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                              kFontWeblySleekSemiBold(16), NSFontAttributeName,
                                              [UIColor redColor], NSForegroundColorAttributeName,[UIColor whiteColor],NSBackgroundColorAttributeName,
                                              nil]forState:UIControlStateNormal];
    
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    if (!category)
    {
        displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filterActiveOff"] style:UIBarButtonItemStylePlain target:self action:@selector(showFilters:)] ;
        displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarCode_Scan"] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    }
    if (self.target && [self.target respondsToSelector:self.action])
    {
        Singleton *single = [Singleton retrieveSingleton];
        if ([single.productBarPopOver isEqualToString:@"PopOver"])
        {
            single.productBarPopOver = @"NoPopOver";
        }
        else
        {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] ];
        }
        [self setTitle:@"Select Product"];
    }
    [self performSelector:@selector(navigatetodeals) withObject:nil afterDelay:0.0];
    mainTableView.cellLayoutMarginsFollowReadableWidth = NO;
}

-(void)navigatetodeals
{
    NSLog(@"category is %@", [category description]);
    
    if (category) {
        [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductsOfCategory:category]];
    } else {
        [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];
    }
    [self setupToolbar];
}

- (void)setupToolbar
{
    if (bSearchIsOn) {
        [countLabel setText:[NSString stringWithFormat:@"Total Products: %lu", (unsigned long)searchData.count]];
        totalLabelButton.title = [NSString stringWithFormat:@"Total Products: %lu", (unsigned long)searchData.count];
    } else {
        [countLabel setText:[NSString stringWithFormat:@"Total Products: %lu", (unsigned long)productArray.count]];
        totalLabelButton.title = [NSString stringWithFormat:@"Total Products: %lu", (unsigned long)productArray.count];
    }
    
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    if (@available(iOS 15, *)){
        self.navigationController.toolbar.backgroundColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1];
    }
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    if (!category)
    {
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    }
}

-(void)addShadowOnCustomLayer:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
}

- (void)displayMap:(id)sender {
    
    Singleton *single = [Singleton retrieveSingleton];
    [filterPopOver dismissPopoverAnimated:YES];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = UIColorFromRGB(0x169C5C); //[UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
   
    [logoutButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                   kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                   nil]
                         forState:UIControlStateNormal];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    reader.wantsFullScreenLayout = NO;
    reader.readerView.zoom = 1.0;
    reader.showsZBarControls=NO;
    [self presentViewController: reader animated: YES completion:nil];
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) readers
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    for(symbol in results){
        single.valueBarCode=symbol.data;
        ProductBrandFilterViewController *filterViewController = [[ProductBrandFilterViewController alloc] init] ;
        [filterViewController setTarget:self];
        [filterViewController setAction:@selector(filterChangedBarCode)];
        [filterViewController selectionDone:self];
        [reader dismissViewControllerAnimated: YES completion:nil];
        single.isBarCode = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.navigationController setToolbarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setupToolbar];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: searchBar.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    searchBar.layer.mask = maskLayer;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}

- (void)executeSearch:(NSString *)term
{
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in productArray)
    {
        NSString *customerName = [element objectForKey:@"Description"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
    }
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        if (!found)
        {
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
    }

    
    if (bSearchIsOn){
        [countLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }else {
        [countLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)productArray.count]];
    }
    [mainTableView reloadData];
}

- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showFilters:(id)sender {
    [filterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)filterChangedBarCode
{
    [loadingView setHidden:NO];
    [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];
}

- (void)filterChanged {
    [filterPopOver dismissPopoverAnimated:YES];
    
    [loadingView setHidden:NO];
    [self getProductServiceDidGetList:[[SWDatabaseManager retrieveManager] dbGetProductCollection]];
    
    //Handle filter icon
    if([[NSUserDefaults standardUserDefaults] boolForKey:kUD_IS_PRODUCT_LIST_FILTER_ON]) {
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveON"]];
    } else {
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveOff"]];
    }
    
    if (bSearchIsOn) {
        [countLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }else {
        [countLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)productArray.count]];
    }
    [mainTableView reloadData];
    
}
- (void)clearFilter {
    [SWDefaults setFilterForProductList:nil];
    [SWDefaults setProductFilterProductID:nil];
    [SWDefaults setProductFilterName:nil];
    [SWDefaults setProductFilterBrand:nil];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action];
#pragma clang diagnostic pop;
    
}
#pragma mark Product List service delegate
- (void)getProductServiceDidGetList:(NSArray *)p
{
    [loadingView setHidden:YES];
    productArray =[NSMutableArray arrayWithArray:p];
    filteredCandyArray= [NSMutableArray arrayWithCapacity:productArray.count];
    NSMutableDictionary * theDictionary = [NSMutableDictionary dictionary];
    for ( NSMutableDictionary * object in productArray ) {
        NSMutableArray * theMutableArray = [theDictionary objectForKey:[object stringForKey:@"Brand_Code"]];
        if ( theMutableArray == nil ) {
            theMutableArray = [NSMutableArray array];
            [theDictionary setObject:theMutableArray forKey:[object stringForKey:@"Brand_Code"]];
        }
        
        [theMutableArray addObject:object];
    }
    productDictionary = [NSMutableDictionary dictionaryWithDictionary:theDictionary];
    finalProductArray = [NSMutableArray arrayWithArray:[[theDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] ];
    _collapsedSections = [NSMutableSet new];

    
    totalRecords=products.count;
    [mainTableView reloadData];
    [self setupToolbar];
    
    
    NSLog(@"\n\n *** finalProductArray count: %lu ***\n\n ",(unsigned long)finalProductArray.count);
    
    if (finalProductArray == nil ||  finalProductArray.count == 0) {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No Products found." withController:self];
    }
}

-(void)sectionButtonTouchUpInside:(UIButton*)sender {
    
    [mainTableView beginUpdates];
    int section = sender.tag;
    bool shouldCollapse = ![_collapsedSections containsObject:@(section)];
    
    if (shouldCollapse) {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        //sender.backgroundColor = [UIColor lightGrayColor];
        
        int numOfRows = [mainTableView numberOfRowsInSection:section];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [mainTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections addObject:@(section)];
    }
    else {
        sender.backgroundColor=UIColorFromRGB(0xE0E5EC);
        sender.titleLabel.textColor=UIColorFromRGB(0x4A5866);
        
        NSInteger numOfRows = [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
        NSArray* indexPaths = [self indexPathsForSection:section withNumberOfRows:numOfRows];
        [mainTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
        [_collapsedSections removeObject:@(section)];
    }
    
    sender.titleLabel.textAlignment = NSTextAlignmentLeft;
    [mainTableView endUpdates];
}


#pragma mark -SEARCH BAR DELEGATE METHODS

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length > 0){
        bSearchIsOn = YES;
    }else{
        bSearchIsOn = NO;
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBara textDidChange:(NSString *)searchText
{
    if (searchText.length > 0) {
        bSearchIsOn = YES;
         [self executeSearch:searchText];
    }else {
        bSearchIsOn = NO;
         [self executeSearch:@""];
    }
    
    int len = [ [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = searchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearchq = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearchq allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearchq setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearchq objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each section array
            for (NSString *key in [sectionSearchq allKeys])
            {
                [[sectionSearchq objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
        }
    }
    else
    {
        [ searchBar resignFirstResponder ];
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    bSearchIsOn = NO;
    searchBar.text = @"";
    [self executeSearch:@""];
    [self.view endEditing:YES];
}

- (void) searchBarSearchButtonClicked:(UISearchBar*) theSearchBar
{
    bSearchIsOn = YES;
    [self executeSearch:searchBar.text];
    [self.view endEditing:YES];
    [searchBar resignFirstResponder];
    
    int len = [ [searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length];
    if (len > 0)
    {
        NSString *searchText = searchBar.text;
        
        if ([searchText length] > 0)
        {
            [filteredCandyArray removeAllObjects];
            NSDictionary *element=[NSDictionary dictionary];
            for(element in productArray)
            {
                NSString *customerName = [element objectForKey:@"Description"];
                NSRange r = [customerName rangeOfString:searchText options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
                if (r.length > 0)
                {
                    [filteredCandyArray addObject:element];
                }
            }
            NSMutableDictionary *sectionSearchq = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in filteredCandyArray)
            {
                NSString *c = [[book objectForKey:@"Description"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionSearchq allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionSearchq setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in filteredCandyArray)
            {
                [[sectionSearchq objectForKey:[[book objectForKey:@"Description"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each section array
            for (NSString *key in [sectionSearchq allKeys])
            {
                [[sectionSearchq objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Description" ascending:YES]]];
            }
            [mainTableView reloadData];
        }
    }
    else
    {
        [ searchBar resignFirstResponder ];
    }
}
#pragma mark - UITableView

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (bSearchIsOn) {
        return 1;
        
    } else {
        return [finalProductArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     if (bSearchIsOn) {
        return searchData.count;
    } else {
        return [_collapsedSections containsObject:@(section)] ? 0 : [(NSArray*)[productDictionary objectForKey:[finalProductArray objectAtIndex:section]] count];
    }
}

-(NSArray*) indexPathsForSection:(int)section withNumberOfRows:(int)numberOfRows {
    NSMutableArray* indexPaths = [NSMutableArray new];
    for (int i = 0; i < numberOfRows; i++) {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:section];
        [indexPaths addObject:indexPath];
    }
    return indexPaths;
}
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    return YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if (bSearchIsOn) {
        return 0;
    }
    else
    {
        return 40.0;
    }
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s
{
    if (bSearchIsOn) {
        return nil;
    }
    else
    {
        if (finalProductArray != nil && finalProductArray.count == 0){
             return nil;
        }
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
        NSArray *row =  [productDictionary objectForKey:[finalProductArray objectAtIndex:s]] ;
        
        UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
        result = [[UIButton alloc]initWithFrame:CGRectMake(0,0, headerView.bounds.size.width, 40)];
        [result addTarget:self action:@selector(sectionButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [result setTitle:[[row objectAtIndex:0] stringForKey:@"Brand_Code"] forState:UIControlStateNormal];
        result.tag = s;
        [result.layer setBorderColor:[UIColor whiteColor].CGColor];
        [result.layer setBorderWidth:1.0f];
        result.titleLabel.font=kFontWeblySleekSemiBold(14);
        [result setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
        result.titleLabel.textAlignment = NSTextAlignmentLeft;
        headerView.backgroundColor= UIColorFromRGB(0xE1E8ED);
        [headerView addSubview:result];
        return headerView;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableViewq cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableViewq dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    if (bSearchIsOn ) {
        if (searchData.count>0) {
            if (indexPath.row< searchData.count){
                  cell.textLabel.text=[[searchData valueForKey:@"Description"] objectAtIndex:indexPath.row];
            }
        }
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        NSDictionary *row= [objectsForCountry objectAtIndex:indexPath.row];
        cell.textLabel.text = [row stringForKey:@"Description"];
    }
    
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.numberOfLines=2;
    cell.textLabel.font=kFontWeblySleekSemiBold(14);
    cell.backgroundColor=[UIColor whiteColor];//UIColorFromRGB(0xF6F7FB);
    cell.textLabel.textColor=UIColorFromRGB(0x2C394A);
    
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [mainTableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *row ;
    if (bSearchIsOn)
    {
        row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }
    
    NSLog(@"check row  product list %@", [row description]);
    
    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
    if (produstDetail.count>0) {
        NSLog(@"product detail before pushing %@", [produstDetail description]);
        
        
        NSDictionary * product =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
        NSString *itemID = [product stringForKey:@"Inventory_Item_ID"];
        [product setValue:itemID forKey:@"ItemID"];
        
        
        if (self.target)
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:product];
#pragma clang diagnostic pop
            
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end

