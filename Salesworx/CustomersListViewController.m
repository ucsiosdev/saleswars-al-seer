//
//  CustomersListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "CustomersListViewController.h"
#import "SWSection.h"
#import "LocationFilterViewController.h"
#import "SWCustomerListCell.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "SWDefaults.h"

#import "SWCustomerDetailViewController.h"



#define kNavigationFilterLocationButton 1
#define kNavigationFilterCategoryButton 2

@interface CustomersListViewController (){
    SWCustomerDetailViewController *customerDetailViewController ;
}
- (void)locationFilter:(id)sender;
- (void)locationFilterSelected;
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation CustomersListViewController

@synthesize target;
@synthesize action;

- (id)init {
    self = [super init];
    
    if (self) {
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"Customers";
        titleLabel.font = kFontWeblySleekSemiBold(16);
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.navigationItem.titleView = titleLabel;
        
        
        //[self setTitle:NSLocalizedString(@"Customers", nil)];
        
        searchData=[NSMutableArray array];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds];
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    
    
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(8, 8, self.view.frame.size.width-16 , self.view.frame.size.height-138)];
    //subView.layer.cornerRadius = 8.0;
    //subView.layer.masksToBounds = true;
    subView.backgroundColor = [UIColor whiteColor];
    
    // table view
    tableView=nil;
    tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,52, subView.bounds.size.width, subView.bounds.size.height-57) style:UITableViewStylePlain] ;
    
    [tableView setSectionIndexColor:UIColorFromRGB(0x169C5C)];
    
    searchBar=nil;
    if (_isFromReport == YES) {
        subView = [[UIView alloc] initWithFrame:CGRectMake(8, 8, 384 , self.view.frame.size.height - 230)];
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,384, 52)] ;
        tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,52, 384, subView.bounds.size.height-57) style:UITableViewStylePlain] ;

    }else{
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,subView.frame.size.width, 52)] ;
    }
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    if (@available(iOS 15.0, *)) {
        tableView.sectionHeaderTopPadding = 0;
    }
    
    searchBar.delegate=self;
    searchBar.showsCancelButton = YES;
    
    [self addShadowOnCustomLayer:subView.layer];
    
    
    
    [subView addSubview:searchBar];
    [subView addSubview:tableView];
    [subView addSubview:loadingView];
    
    
    [self.view addSubview:subView];
    
    
    // popovers
    LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
    [locationFilterController setTarget:self];
    [locationFilterController setAction:@selector(locationFilterSelected)];
    [locationFilterController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:locationFilterController] ;
    locationFilterPopOver=nil;
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController] ;
    locationFilterPopOver.delegate=self;
    // Count label
    infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 25)];
    [infoLabel setText:@""];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:kFontWeblySleekSemiBold(14.0f)];
    
    
    flexibleSpace=nil;
    totalLabelButton=nil;
    displayActionBarButton=nil;
    displayMapBarButton=nil;
    
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    totalLabelButton = [[UIBarButtonItem alloc] init];
    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:infoLabel];
    
    
    
    [totalLabelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                              kFontWeblySleekSemiBold(16), NSFontAttributeName,
                                              [UIColor redColor], NSForegroundColorAttributeName,[UIColor whiteColor],NSBackgroundColorAttributeName,
                                              nil]forState:UIControlStateNormal];
    
    
    displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filterActiveOff"] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
    displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarCode_Scan"] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
    
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    if (_isPendingNotificationTapped) {
        NSLog(@"PendingNotificationTapped");
        
        [self getListFunction:[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT A.*,IFNULL(A.Cust_Lat,0)AS Lat,IFNULL(A.Cust_Long,0)AS Long,A.Customer_ID AS [Ship_Customer_ID] , A.Site_Use_ID AS [Ship_Site_Use_ID], B.Customer_ID, B.Site_Use_ID, B.Contact, B.Phone, B.Credit_Limit, B.Credit_Hold, B.Customer_Type, B.Customer_Class, B.Trade_Classification, B.Chain_Customer_Code, B.Cust_Status, B.Customer_OD_Status, B.Cash_Cust, B.Price_List_ID, B.Avail_Bal, B.Bill_Credit_Period, B.Allow_FOC, B.Creation_Date, A.Customer_Name FROM TBL_Customer_Ship_Address AS A INNER JOIN TBL_Customer AS B ON A.Customer_ID = B.Customer_ID inner join TBL_FeedBack_Request AS F ON B.Customer_ID = F.Customer_ID where F.Status = 'N' and F.Expiry_Date > datetime('now', 'localtime') ORDER BY A.Customer_Name"]];
        
        NSString *text = [NSString stringWithFormat:@"%@: %lu",NSLocalizedString(@"Total Customers", nil), (unsigned long)customerList.count];
        infoLabel.text = [NSString stringWithFormat:@"%@ (filtered by: Pending Feedback)", text];
        
        if (!self.navigationController.toolbarHidden) {
            return;
        }
        
        [totalLabelButton setTitle:[infoLabel text]];
        [self setToolbarItems:nil];
        [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
        
        self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    }
}

#pragma mark -SEARCH BAR DELEGATE METHODS
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self executeSearch:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    searchBar.text = @"";
    [self executeSearch:@""];
    [self.view endEditing:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
     [self executeSearch:searchBar.text];
     [self.view endEditing:YES];
}

-(void)addShadowOnCustomLayer:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [SWDefaults clearCustomer];
    [self navigatetodeals];
}
-(void)navigatetodeals
{
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
    [self setupToolbar];
}
- (void)setupToolbar {
    self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x169C5C);

    NSString *text = [NSString stringWithFormat:@"%@: %lu",NSLocalizedString(@"Total Customers", nil), (unsigned long)customerList.count];
    
    if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
        text = [NSString stringWithFormat:@"%@ (filtered by: %@)", text, [SWDefaults locationFilterForCustomerList]];
    }
    
    [infoLabel setText:text];
    
    if (!self.navigationController.toolbarHidden) {
        return;
    }
    
    [totalLabelButton setTitle:[infoLabel text]];
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    if (@available(iOS 15, *)){
        self.navigationController.toolbar.backgroundColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1];
    }
    [self.navigationController setToolbarHidden:NO animated:YES];
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
}

- (void)displayMap:(id)sender {
    
    
    Singleton *single = [Singleton retrieveSingleton];
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = UIColorFromRGB(0x169C5C);
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0,0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    [logoutButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          kFontWeblySleekSemiBold(14), NSFontAttributeName,
                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                          nil]
                                forState:UIControlStateNormal];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    
    reader.wantsFullScreenLayout = NO;
    
    reader.readerView.zoom = 1.0;
    
    reader.showsZBarControls=NO;
    
    [self presentViewController: reader animated: YES completion:nil];
    
    
}
- (void)barButtonBackPressed:(id)sender
{
    [reader dismissViewControllerAnimated: YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController*) readers
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        
        [reader dismissViewControllerAnimated: YES completion:nil];
        
        single.isBarCode = NO;
    }
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    if (self.target && [self.target respondsToSelector:self.action]) {
        UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Clear", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
        
        [self.navigationItem setLeftBarButtonItem:logoutButton ];
        [self setTitle:NSLocalizedString(@"Select Customer", nil)];
    }
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: searchBar.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    
    searchBar.layer.mask = maskLayer;
    
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;
}



- (void)locationFilter:(id)sender
{
    [locationFilterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


- (void)filterChangedBarCode {
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:YES animated:YES];
    // [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;
    
    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
    
}
- (void)locationFilterSelected {
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    [[SWDatabaseManager retrieveManager] cancel];
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:NO animated:YES];
    //[self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;
    
    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
    
    //Handle filter icon
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:kUD_IS_CUST_LIST_FILTER_ON]) {
        
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveON"]];
        
    } else {
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveOff"]];
    }
    
    if (searchBar.text.length==0){
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)customerList.count]];
    }else {
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }
    
}

- (void)executeSearch:(NSString *)term
{
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in customerList)
    {
        NSString *customerName = [element objectForKey:@"Customer_Name"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
    }
    sectionSearch=nil;
    sectionSearch = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
    }
    
    
    NSLog(@"check cust list %@", [sectionSearch description]);
    
    if (searchBar.text.length==0){
         [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)customerList.count]];
    }else {
         [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }
    [tableView reloadData];
}

- (void)cancel:(id)sender {
    if (self.target && [self.target respondsToSelector:self.action])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:[NSDictionary dictionary]];
#pragma clang diagnostic pop
        return;
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)clearFilter {
    [SWDefaults setLocationFilterForCustomerList:nil];
    [SWDefaults setPaymentFilterForCustomerList:nil];
    [SWDefaults setStatusFilterForCustomerList:nil];
    [SWDefaults setNameFilterForCustomerList:nil];
    [SWDefaults setCodeFilterForCustomerList:nil];
    [SWDefaults setbarCodeFilterForCustomerList:nil];
    if ([[SWDefaults filterForCustomerList]isEqualToString:@""] || [SWDefaults filterForCustomerList].length == 0 || [SWDefaults filterForCustomerList] == nil) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action];
#pragma clang diagnostic pop;
        
    }
    else {
        [SWDefaults setFilterForCustomerList:nil];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action];
#pragma clang diagnostic pop;
        
    }
}
#pragma mark Customer List service delegate
-(void)getListFunction:(NSArray *)temp
{
    [loadingView setHidden:YES];
    customerList = [NSMutableArray arrayWithArray:temp ] ;
    
    if([[SWDefaults filterForCustomerList] isEqualToString:@"BarCode"] && [temp count]==0)
    {
        [reader dismissViewControllerAnimated: YES completion:nil];
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No customer found." withController:self];
        [self clearFilter];
        [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
        
        return;
    }
    
    if(customerList.count > 0)
    {
        if([[searchBar text] length] > 0){
            [self executeSearch:[searchBar text]];
            [tableView reloadData];
        }
        else
        {
            sectionList=nil;
            sectionList = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            
            NSLog(@"check customer list initially %@", [customerList description]);
            
            
            // Loop through the books and create our keys
            @autoreleasepool {
                
                for (NSDictionary *book in customerList)
                {
                    NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
                    
                    found = NO;
                    
                    for (NSString *str in [sectionList allKeys])
                    {
                        if ([str isEqualToString:c])
                        {
                            found = YES;
                        }
                    }
                    
                    if (!found)
                    {
                        [sectionList setValue:[[NSMutableArray alloc] init] forKey:c];
                    }
                }
            }
            
            // Loop again and sort the books into their respective keys
            @autoreleasepool {
                
                
                for (NSDictionary *book in customerList)
                {
                    [[sectionList objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
                }
            }
            // Sort each section array
            @autoreleasepool {
                
                
                for (NSString *key in [sectionList allKeys])
                {
                    [[sectionList objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
                }
            }
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No customer found." withController:self];
    }
    
    [self setupToolbar];
    [tableView reloadData];
    temp=nil;
    
}
- (void)customerServiceDidGetList:(NSArray *)cl {
    
    
}

#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv
{
    if([[searchBar text] length] > 0){
        return [[sectionSearch allKeys] count];
    }
    return [[sectionList allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)s
{
    if([[searchBar text] length] > 0){
        return [(NSArray*)[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
    }
    // return [customerList count];
    return [(NSArray*)[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Identifier = @"CUSTOMERLISTCELL";
    SWCustomerListCell *cell = [tv dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[SWCustomerListCell alloc] initWithReuseIdentifier:Identifier] ;
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    if([[searchBar text] length] > 0){
        //    {
        [cell applyAttributes:[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell applyAttributes:[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    
    
    NSLog(@"customer list is %@", [sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]]);
    
    
    // cell background color
    
    
    //    UIView *selectionColor = [[UIView alloc] init];
    ////   selectionColor.backgroundColor = [UIColor colorWithRed:223 green:232 blue:244 alpha:1.0];
    //    selectionColor.backgroundColor = [UIColor darkGrayColor];
    
    
    //cell.selectedBackgroundView = selectionColor;
    
    
    //cell.textLabel.highlightedTextColor=[UIColor blueColor];
    
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section{
    
    NSString *title  = @"";
    if([[searchBar text] length] > 0){
        title = [[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        
    }else{
        title = [[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    }
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
    UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
    result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
    result.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    result.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    [result setTitle:title forState:UIControlStateNormal];
    [result.layer setBorderColor:[UIColor whiteColor].CGColor];
    [result.layer setBorderWidth:1.0f];
    result.titleLabel.font=kFontWeblySleekSemiBold(14);
    [result setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
    headerView.backgroundColor= UIColorFromRGB(0xE1E8ED);
    [headerView addSubview:result];
    return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;//28.0f;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    return 50.0f;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tv
{
    if([[searchBar text] length] > 0){
        return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
    }
    return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    AppControl *appControl = [AppControl retrieveSingleton];
    
    
    Singleton* singleton=[Singleton retrieveSingleton];
    
    
    if ([appControl.CUST_DTL_SCREEN isEqualToString:@"DASHBOARD"]&& singleton.showCustomerDashboard==YES) {
        
        
        AlSeerCustomerDashboardViewController* customerDash=[[AlSeerCustomerDashboardViewController alloc]init];
        if([[searchBar text] length] > 0){
            
            NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            
            customerDict =[NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithDictionary:row]];
        }
        else
        {
            NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            
            customerDict=[NSMutableDictionary dictionaryWithDictionary:[row mutableCopy]];
            
        }
        
        NSLog(@"custom dict before dashboard %@", [customerDict description]);
        NSLog(@"check customer from defaults %@", [SWDefaults customer]);
        
        customerDash.customerDetailsDictionary=customerDict;
        [self.navigationController pushViewController:customerDash animated:YES];
    }
    else {
        Singleton *single = [Singleton retrieveSingleton];
        single.customerIndexPath = indexPath.row;
        
        SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
        
        if (revealController.currentFrontViewPosition !=0)
        {
            [revealController revealToggle:self];
        }
        revealController=nil;
  
        
        if([[searchBar text] length] > 0){
            NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            if (self.target && [self.target respondsToSelector:self.action]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
                
                //[self dismissViewControllerAnimated:YES completion:nil];
                
                return;
            }
            customerDict =[NSMutableDictionary dictionaryWithDictionary:[NSMutableDictionary dictionaryWithDictionary:row   ]];
            
            
            [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
            
            
        } else
        {
            NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
            // NSDictionary *row = [self.customerList objectAtIndex:indexPath.row];
            if (self.target && [self.target respondsToSelector:self.action])
            {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop
                return;
            }
            
            customerDict=[NSMutableDictionary dictionaryWithDictionary:[row mutableCopy]];
            
            
            [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[row stringForKey:@"Customer_ID"]]];
            
        }
    }
    
}
//- (void)customerServiceDidGetOrderAmount:(NSArray *)orderAmmount{
- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
    
    [SWDefaults setCustomer:customerDict];
    
    customerDict = [NSMutableDictionary dictionaryWithDictionary:[SWDefaults validateAvailableBalance:orderAmmount]];
    customerDetailViewController=nil;
    customerDetailViewController = [[SWCustomerDetailViewController alloc] initWithCustomer:customerDict] ;
    [self.navigationController pushViewController:customerDetailViewController animated:YES];
    // [self performSelector:@selector(pushCustomerDetailPage) withObject:nil afterDelay:0.0];
    
    orderAmmount=nil;
}

-(void)pushCustomerDetailPage {
    if (customerDetailViewController) {
        NSLocale* currentLoc = [NSLocale currentLocale];
        NSLog(@"***");
        NSLog(@"Push %@",[[NSDate date] descriptionWithLocale:currentLoc]);
        
        [customerDetailViewController view];
        customerDetailViewController=nil;
    }
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:kFontWeblySleekSemiBold(14.0f)];
    [cell.detailTextLabel setFont:kFontWeblySleekSemiBold(14.0f)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end


