//
//  CustomerHeaderView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 6/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerHeaderView : UIView {
    NSDictionary *customer;
    UILabel *companyNameLabel;
    UILabel *codeLabel;
    UILabel *availableBalanceLabel;
    UILabel *availableBalanceValue;
    BOOL shouldHaveMargins;
    UIButton *statusButton;
    UIImageView *availableBalanceImage;
}

@property (nonatomic, strong) NSDictionary *customer;
@property (nonatomic, strong) UILabel *companyNameLabel;
@property (nonatomic, strong) UILabel *codeLabel;
@property (nonatomic, strong) UILabel *availableBalanceLabel;
@property (nonatomic, strong) UILabel *availableBalanceValue;
@property (nonatomic, strong) UIImageView *availableBalanceImage;
@property (nonatomic, assign) BOOL shouldHaveMargins;
@property (nonatomic, strong) UIButton *statusButton;

- (id)initWithFrame:(CGRect)frame andCustomer:(NSDictionary *)customer;

@end
