//
//  MapViewAnnotation.h
//  Salesworx
//
//  Created by Unique Computer Systems on 3/16/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapView.h"
@interface MapViewAnnotation : NSObject



@property (nonatomic,copy) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
-(id) initWithTitle:(NSString *) title AndCoordinate:(CLLocationCoordinate2D)coordinate;
@end
