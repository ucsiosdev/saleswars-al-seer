

#import "SalesSummaryViewController.h"
#import "SalesWorxSalesSummaryTableViewCell.h"
#import "SalesSummaryReportFilterViewController.h"

@interface SalesSummaryViewController ()

@end

@implementation SalesSummaryViewController
@synthesize datePickerViewControllerDate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults fetchTitleView:kSalesSummaryTitle];
    SWDatePickerViewController *datePickerViewController = [[SWDatePickerViewController alloc] initWithTarget:self action:@selector(dateChanged:)]  ;
    datePickerViewController.isRoute=NO;
    
     self.datePickerViewControllerDate=datePickerViewController;
    datePickerViewController.forReports=YES;
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:datePickerViewController]  ;
    datePickerPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController]  ;
    datePickerPopOver.delegate=self;
    // Do any additional setup after loading the view.
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    toDate=[formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:-1]; // note that I'm setting it to -1
    fromDate=[formatter stringFromDate:[gregorian dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0]];
    DocType = @"All";
    custType = @"All";
    formatter=nil;
    usLocale=nil;
    filteredSalesSummaryArray = [[NSMutableArray alloc]init];
    [self viewReportAction];
    
    salesSummaryTableView.layer.cornerRadius = 8.0;
    salesSummaryTableView.layer.masksToBounds = YES;
    
    if (@available(iOS 15.0, *)) {
        salesSummaryTableView.sectionHeaderTopPadding = 0;
        filterTableView.sectionHeaderTopPadding = 0;
        gridView.tableView.sectionHeaderTopPadding = 0;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    Singleton * singleton=[Singleton retrieveSingleton];
    
    singleton.showCustomerDashboard=NO;
    
    [self viewReportAction];
}
- (void)typeChanged:(NSString *)sender
{
    [customPopOver dismissPopoverAnimated:YES];
    if(isCustType)
    {
        custType=sender;
    }
    else
    {
        DocType = sender;
    }
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (void)dateChanged:(SWDatePickerViewController *)sender
{
    [datePickerPopOver dismissPopoverAnimated:YES];
    NSDateFormatter *formatter = [NSDateFormatter new]  ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:usLocale];
    if(isFromDate)
    {
        fromDate=[formatter stringFromDate:sender.selectedDate];
    }
    else
    {
        toDate = [formatter stringFromDate:sender.selectedDate];
    }
    [filterTableView reloadData];
    formatter=nil;
    usLocale=nil;
    [self viewReportAction];
    
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDict = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterTableView reloadData];
    [self viewReportAction];
    
}
- (IBAction)filterButtonTapped:(id)sender {
    if (filteredSalesSummaryArray.count > 0) {
        SalesSummaryReportFilterViewController *popOverVC = [[SalesSummaryReportFilterViewController alloc]init];
        popOverVC.delegate=self;
        popOverVC.previousFilterParametersDict = previousFilteredParameters;
        popOverVC.customerDictionary = customerDict;
        popOverVC.salesSummaryArray = filteredSalesSummaryArray;
        if (previousFilteredParameters.count>0) {
            popOverVC.previousFilterParametersDict = previousFilteredParameters;
        }
        popOverVC.filterNavController=self.navigationController;
        popOverVC.filterTitle=@"Filter";
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        filterPopOverController=nil;
        filterPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        filterPopOverController.delegate=self;
        popOverVC.filterPopOverController = filterPopOverController;
        [filterPopOverController setPopoverContentSize:CGSizeMake(366, 550) animated:YES];
        popOverVC.previousFilterParametersDict=previousFilteredParameters;
        CGRect relativeFrame = [filterButton convertRect:filterButton.bounds toView:self.view];
        [filterPopOverController presentPopoverFromRect:relativeFrame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }else{
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

-(IBAction)viewReportAction
{
    //try distinct
    
    //OLA!! Added ERP_Ref_No to list
    NSString *sQry = @" SELECT  A.Orig_Sys_Document_Ref, A.Creation_Date,A.Doc_Type,A.Transaction_Amt, B.Customer_No, B.Customer_Name,C.Cash_Cust AS Status,C.Phone, A.Visit_ID, A.End_Time, A.ERP_Ref_No FROM TBL_Sales_History AS A INNER JOIN  TBL_Customer_Ship_Address AS B ON A.Ship_To_Customer_ID = B.Customer_ID AND A.Ship_To_Site_ID = B.Site_Use_ID INNER JOIN  TBL_Customer AS C ON C.Customer_ID = A.Inv_To_Customer_ID AND C.Site_Use_ID = A.Inv_To_Site_ID WHERE A.Creation_Date >='{0}'  AND A.Creation_Date<='{1}'";
    if(customerDict.count!=0)
    {
        sQry=[sQry stringByAppendingString:@" AND A.Ship_To_Customer_Id='{3}' AND A.Ship_To_Site_Id='{4}'"];
    }
    if(![DocType isEqualToString:@"All"])
    {
        sQry=[sQry stringByAppendingString:@" AND A.Doc_Type='{5}'"];
    }
    if(![custType isEqualToString:@"All"])
    {
        sQry=[sQry stringByAppendingString:@" AND C.Cash_Cust='{6}'"];
    }
    sQry=[sQry stringByAppendingString:@" ORDER BY C.Cash_Cust Desc , A.End_Time ASC,A.ERP_Ref_No ASC"];
    
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{0}" withString:[NSString stringWithFormat:@"%@ 00:00:00",@"2020-01-01"]];
    sQry = [sQry stringByReplacingOccurrencesOfString:@"{1}" withString:[NSString stringWithFormat:@"%@ 23:59:59",toDate]];
    if(customerDict.count!=0)
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{3}" withString:[customerDict stringForKey:@"Ship_Customer_ID"]];
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{4}" withString:[customerDict stringForKey:@"Ship_Site_Use_ID"]];
    }
    if(![DocType isEqualToString:@"All"])
    {
        if([DocType isEqualToString:@"Invoice"])
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"I"];
        }
        else
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:@"R"];
        }
    }
    else
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{5}" withString:DocType];
    }
    
    if(![custType isEqualToString:@"All"])
    {
        if([custType isEqualToString:@"Cash"])
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"Y"];
        }
        else
        {
            sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:@"N"];
        }
    }
    else
    {
        sQry = [sQry stringByReplacingOccurrencesOfString:@"{6}" withString:custType];
    }
    salesSummaryArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sQry];
    filteredSalesSummaryArray = salesSummaryArray;
    [gridView reloadData];
    
    
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    currencyTypePopOver=nil;
    // datePickerPopOver=nil;
    customPopOver=nil;
    
    currencyTypeViewController=nil;
    customVC=nil;
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filteredSalesSummaryArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"salesSummary";
    SalesWorxSalesSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.lblDocumentNo.text = @"Document No";
    cell.lblDocumentType.text = @"Document Type";
    cell.lblDate.text = @"Date";
    cell.lblName.text = @"Name";
    cell.lblAmount.text = @"Amount (AED)";
   cell.lblAmount.textAlignment = NSTextAlignmentRight;

    cell.lblDocumentNo.textColor = TableViewHeaderSectionColor;
    cell.lblDocumentType.textColor = TableViewHeaderSectionColor;
    cell.lblDate.textColor = TableViewHeaderSectionColor;
    cell.lblName.textColor = TableViewHeaderSectionColor;
    cell.lblAmount.textColor = TableViewHeaderSectionColor;

    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"salesSummary";
    SalesWorxSalesSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SalesWorxSalesSummaryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *data = [filteredSalesSummaryArray objectAtIndex:indexPath.row];
    
    NSString *value = @"";
    
    
    cell.lblDocumentNo.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Orig_Sys_Document_Ref"]]];
    if([[data stringForKey:@"Doc_Type"] isEqualToString:@"I"])
    {
        value = @"Invoice";
    }
    else if([[data stringForKey:@"Doc_Type"] isEqualToString:@"R"])
    {
        value = @"Credit Note";
    }
    [cell.lblAmount setTextAlignment:NSTextAlignmentRight];

    cell.lblDocumentType.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:value]];
    cell.lblDate.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data dateStringForKey:@"Creation_Date" withDateFormat:NSDateFormatterMediumStyle]]];
    cell.lblName.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_Name"]]];
    cell.lblAmount.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data currencyStringForKey:@"Transaction_Amt"]]];
    return cell;
}
-(void)filteredSalesSummary:(id)filteredContent
{
    NSLog(@"filtered content in report is %@", filteredContent);
    
    [filterButton setBackgroundImage:[UIImage imageNamed:@"filterFilled.png"] forState:UIControlStateNormal];
    filteredSalesSummaryArray = filteredContent;
    [salesSummaryTableView reloadData];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"filterFilled.png"]];
    
}
-(void)salesSummaryFilterDidReset{
    previousFilteredParameters = [[NSMutableDictionary alloc]init];
    [self viewReportAction];
    filteredSalesSummaryArray = salesSummaryArray;
    [salesSummaryTableView reloadData ];
    [self.navigationItem.rightBarButtonItem setImage:[UIImage imageNamed:@"ReportsFilter.png"]];
}

-(void)filterParametersSalesSummary:(id)filterParameter
{
    previousFilteredParameters=filterParameter;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end

