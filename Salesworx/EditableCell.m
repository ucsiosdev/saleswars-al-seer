//
//  EditableCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 7/1/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "EditableCell.h"

@implementation EditableCell

@synthesize delegate= _delegate;
@synthesize key;



- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end