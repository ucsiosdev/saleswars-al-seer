//
//  SWOrderHistoryViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWOrderHistoryViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SWOrderHistoryTableViewCell.h"
#import "AlSeerOrderHistoryPopupViewController.h"

@interface SWOrderHistoryViewController ()

@end

@implementation SWOrderHistoryViewController
@synthesize customerDictornary,orderHistoryArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Order History"];
    customerNameLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_Name"]];
    customerNumberLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_No"]];
    double availBal = [[customerDictornary stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
    orderHistoryTableView.tableFooterView = [UIView new];
    
    orderHistoryArray = [[NSArray alloc]init];
    [self roundCornersOnView:orderHistoryTableView onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:8.0];
    
    if (@available(iOS 15.0, *)) {
        orderHistoryTableView.sectionHeaderTopPadding = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Order History View"];
    
    orderHistoryArray = [[SWDatabaseManager retrieveManager] dbGetRecentOrders:[customerDictornary objectForKey:@"Customer_No"]];


    [orderHistoryTableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated{
    
    UIBezierPath *maskPath_3 = [UIBezierPath bezierPathWithRoundedRect:orderHistoryTableView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer_3 = [[CAShapeLayer alloc] init];
    maskLayer_3.frame = orderHistoryTableView.bounds;
    maskLayer_3.path = maskPath_3.CGPath;
    orderHistoryTableView.layer.mask = maskLayer_3;
   
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderHistoryArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"manageOrderTableCell";
    SWOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.docRefNumberLbl.text = @"Reference No.";
    cell.orderDateLbl.text = @"Date";
    cell.orderStatusLbl.text = @"Status";
    cell.orderValueLbl.text = @"Amount (AED)";
    
    cell.docRefNumberLbl.textColor = TableViewHeaderSectionColor;
    cell.orderDateLbl.textColor = TableViewHeaderSectionColor;
    cell.orderStatusLbl.textColor = TableViewHeaderSectionColor;
    cell.orderValueLbl.textColor = TableViewHeaderSectionColor;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"manageOrderTableCell";
    SWOrderHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"SWOrderHistoryTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data = [orderHistoryArray objectAtIndex:indexPath.row];
    
    NSString* dateStr=[data stringForKey:@"Creation_Date"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateStr = [dateFormat stringFromDate:date];
    
    NSString* formattedDate = dateStr;
        
    cell.docRefNumberLbl.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Orig_Sys_Document_Ref"]]];
    cell.orderDateLbl.text = [NSString stringWithFormat:@"%@",formattedDate];
    cell.orderValueLbl.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Transaction_Amt"]]];
    cell.orderStatusLbl.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"ERP_Status"]]];

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSDictionary *data = [orderHistoryArray objectAtIndex:indexPath.row];
    AlSeerOrderHistoryPopupViewController* salesHistoryPopUp=[[AlSeerOrderHistoryPopupViewController alloc]init];
    
    salesHistoryPopUp.preferredContentSize = CGSizeMake(720.0, 460.0);
    
    salesHistoryPopUp.modalPresentationStyle = UIModalPresentationFormSheet;
    
    salesHistoryPopUp.salesOrderArray=[orderHistoryArray mutableCopy];
    
    salesHistoryPopUp.selectedIndex=indexPath.row;
    
    
    salesHistoryPopUp.salesOrderNumber=[NSString stringWithFormat:@"%@",[data valueForKey:@"Orig_Sys_Document_Ref"]];
    
    
    [self presentViewController:salesHistoryPopUp animated:YES completion:nil];
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {corner = corner | UIRectCornerTopLeft;}
        if (tr) {corner = corner | UIRectCornerTopRight;}
        if (bl) {corner = corner | UIRectCornerBottomLeft;}
        if (br) {corner = corner | UIRectCornerBottomRight;}
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
