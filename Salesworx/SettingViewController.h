//
//  SettingViewController.h
//  SWPlatform
//
//  Created by msaad on 3/13/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "SWPlatform.h"

#import <MessageUI/MFMailComposeViewController.h>


@interface SettingViewController : SWViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIPopoverControllerDelegate,MFMailComposeViewControllerDelegate>
{
    // id target;
    SEL action;
    BOOL isRootView;
    UITableView *tableView;
    UIPopoverController *popoverController;
    UIWindow *window;
    UIViewController *applicationViewController;
    NSString *login;
    NSString *password;
    NSMutableDictionary *serverDict;
    NSString *editServerLink;
    NSString *editServerName1;
    NSMutableDictionary *newServerDict;
    SWLoadingView *loadingView;
    NSMutableDictionary *editServerDict;
    UIView *myBackgroundView;
    NSString *changeMadeFrom;

}
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;


@end
