//
//  NewSurveyViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/22/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SurveyQuestionDetails.h"
#import "SurveyResponseDetails.h"
#import "SurveyPageControlViewControl.h"
#import "DataSyncManager.h"
#import "FMDB/FMDBHelper.h"

#import "FMDBHelper.h"


@interface NewSurveyViewController : UIViewController<UIScrollViewDelegate,UITableViewDelegate,UITextViewDelegate,UIAlertViewDelegate>
{
    DataSyncManager *appDelegate;
    SurveyQuestionDetails * surveyQuestionDetails;
    SurveyResponseDetails *info;
    
    int viewX;
    int viewY;
    int viewWidth;
    int saveSurveyID;
    UIAlertView* saveSuccess;
    UIAlertView* saveError;
    BOOL surveyIncomplete;
    
    UITextView * txtview ;
    
    NSMutableArray * textAreaQuestionAnswerArray;
    NSMutableArray * checkBoxQuestionAnswerArray;
    NSMutableArray * radioQuestionAnswerArray;
    
    
    
    
    NSMutableArray * radioQuestionAnswerValidationArray;
    NSMutableArray * checkBoxQuestionAnswerValidationArray;
    NSMutableArray * textAreaQuestionAnswerValidationArray;
    NSMutableArray * textAreaAnswerDescriptionArray;
    NSInteger questionsCount;
    NSInteger answersCount;
    
    NSString* visitID;
    
    __weak IBOutlet NSLayoutConstraint *constrContainerViewHeight;
    __weak IBOutlet SalesWorxDropShadowView *shadowView;
    
    
}

- (IBAction)dismissButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//@property (strong, nonatomic) IBOutlet UILabel *questionLbl;
@property (nonatomic, strong) NSArray *surveyQuestionArray,*surveyResponseArray;
@property(strong,nonatomic)NSMutableArray *modifiedSurveyQuestionsArray;

@property(nonatomic,assign)int survetIDNew,qID;

//@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;



-(void)Save;
- (void)buttonClickedForRadio:(UIButton*)button;

-(void)CreateQuestionsView;


@property(nonatomic,strong)NSString* surveyParentLocation;
@property(nonatomic,strong) NSMutableDictionary *customer;


-(void)ModifyQuestionResponse;
@end

