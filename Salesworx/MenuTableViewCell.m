//
//  MenuTableViewCell.m
//  Salesworx
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "MenuTableViewCell.h"
#import "SWFoundation.h"
#import "SWDefaults.h"

@implementation MenuTableViewCell

@synthesize imageName,statusLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 4, self.frame.size.height)];
        [self.contentView addSubview:statusLabel];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (self.imageName != nil) {
        NSString *imageFile = [NSString stringWithFormat:@"%@", self.imageName];
        
        if (selected) {
            imageFile = [NSString stringWithFormat:@"%@_selected", self.imageName];
        }
        self.imageView.image=[[UIImage imageNamed:imageFile] imageFlippedForRightToLeftLayoutDirection];
    }
}

@end
