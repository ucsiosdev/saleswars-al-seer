//
//  SWDateFieldCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "EditableCell.h"
#import "SWDatePicker.h"

@interface SWDateFieldCell : EditableCell <SWDatePickerDelegate, UITextFieldDelegate> {
    UITextField *textField;
    NSDate *date;
    SWDatePicker *datePicker;
    UILabel *titleLabel;
    UILabel *detailLabel;
}

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) SWDatePicker *datePicker;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property(nonatomic)BOOL hideDateText;




- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end