//
//  MenuTableViewCell.h
//  Salesworx
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell {
    NSString *imageName;
}

@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) UILabel *statusLabel;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
@end
