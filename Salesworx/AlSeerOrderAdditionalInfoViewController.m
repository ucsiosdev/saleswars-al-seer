//
//  AlSeerOrderAdditionalInfoViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerOrderAdditionalInfoViewController.h"
#import "AlSeerSalesOrderPreviewViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "AlSeerSalesOrderTableViewCell.h"
#import "SalesOrderTemplate.h"

@interface AlSeerOrderAdditionalInfoViewController ()

@end

@implementation AlSeerOrderAdditionalInfoViewController

@synthesize salesOrderDict,customerDict,customerIDLbl,customerNameLbl,customerAvailableBalanceLbl,customSignatureView,commentsTxtView,nameTxtFld,txtfldCustomerDocRef,performaOrderRef,signVc,reqShipDateLbl,orderItemView,orderItemCountLabel,totalOrderValueLabel,totalAmount;


#pragma mark View Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"customer dict in new order additional info %@", [customerDict description]);
    NSLog(@"salesorder dict in new order additional info %@", [salesOrderDict description]);
    
    [orderItemCountLabel setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.itemsSalesOrder.count]];
    totalAmount.text = [[[salesOrderDict valueForKey:@"info"]valueForKey:@"orderAmt"] currencyString];
    
    txtfldCustomerDocRef.backgroundColor = [UIColor whiteColor];
    nameTxtFld.backgroundColor = [UIColor whiteColor];
    commentsTxtView.backgroundColor = [UIColor whiteColor];
    [self setAllViewBorder];
    [self addShadowOnUI];
    self.orderItemTableView.tableFooterView = [UIView new];
    if (@available(iOS 15.0, *)) {
        self.orderItemTableView.sectionHeaderTopPadding = 0;
    }
}

-(void)addShadowOnUI {
    [self setShadowOnUILayer:txtfldCustomerDocRef.layer];
    [self setShadowOnUILayer:nameTxtFld.layer];
    [self setShadowOnUILayer:commentsTxtView.layer];
}

-(void)setShadowOnUILayer:(CALayer*)viewLayer{
    
    viewLayer.borderWidth=1.0;
    UIColor *borderColor = [UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.masksToBounds = NO;
    viewLayer.cornerRadius = 5.0;
    viewLayer.shadowColor = [UIColor blackColor].CGColor;
    viewLayer.shadowOffset = CGSizeMake(3.0, 3.0);
    viewLayer.shadowOpacity = 0.1;
    viewLayer.shadowRadius = 1.0;
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [_orderItemTableView.layer setCornerRadius:8.0f];
    [_orderItemTableView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_orderItemTableView.layer setShadowOpacity:0.1];
    [_orderItemTableView.layer setShadowRadius:1.0];
    [_orderItemTableView.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
}

-(void) setAllViewBorder {
    _lblDetails.font=kFontWeblySleekSemiLight(16);
    _lblDetails.textColor =  UIColorFromRGB(0x2C394A);
    
    _lblOrderItems.font=kFontWeblySleekSemiLight(16);
    _lblOrderItems.textColor =  UIColorFromRGB(0x2C394A);
    
    _lblTotalOrderValue.font=kFontWeblySleekSemiLight(16);
    _lblTotalOrderValue.textColor =  UIColorFromRGB(0x2C394A);
    
    orderItemCountLabel.layer.cornerRadius = 8.0;
    orderItemCountLabel.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    customerNameLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_Name"]];
    customerIDLbl.text=[NSString stringWithFormat:@"%@",[customerDict valueForKey:@"Customer_No"]];
    customerAvailableBalanceLbl.text = [NSString stringWithFormat:@"%@",[[customerDict stringForKey:@"Avail_Bal"] currencyString]];
    
    
    CGRect frame = CGRectMake(260, 0, 400, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = headerTitleFont;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.text = @"Sales Order Confirmation";
    // emboss in the same way as the native title
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1000, 40)];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setFrame:CGRectMake(770, 0, 60, 40)];
    [button2 setTitle:@"Save" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button2.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemibold" size:14.0]];
    [button2 addTarget:self action:@selector(saveButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button3 setFrame:CGRectMake(840, 0, 60, 40)];
    [button3 setTitle:@"Confirm" forState:UIControlStateNormal];
    [button3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button3.titleLabel setFont:[UIFont fontWithName:@"WeblySleekUISemibold" size:14.0]];
    [button3 addTarget:self action:@selector(confirmButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [buttonView addSubview:label];
    [buttonView addSubview:button2];
    [buttonView addSubview:button3];
    self.navigationItem.titleView = buttonView;
    
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preview1.png"] style:UIBarButtonItemStylePlain target:self action:@selector(presentCustomPreviewViewController)];
    [self.navigationItem setRightBarButtonItem:btn];
    
    orderAdditionalInfoDict=[[NSMutableDictionary alloc]init];    
    
    //adding border to textview
    commentsTxtView.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    commentsTxtView.layer.borderWidth = 1.0;
    
    
    reqShipDateLbl.layer.borderColor = [UIColor colorWithRed:(221.0/255.0) green:(221.0/255.0) blue:(221.0/255.0) alpha:1].CGColor;
    reqShipDateLbl.layer.borderWidth = 1.0;
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 30)];
    nameTxtFld.leftView = paddingView;
    nameTxtFld.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingViewCust = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5,30)];
    txtfldCustomerDocRef.leftView = paddingViewCust;
    txtfldCustomerDocRef.leftViewMode = UITextFieldViewModeAlways;
    
    
    //check if this manage order and it has customer doc reference
    Singleton *singleton = [Singleton retrieveSingleton];
    if([singleton.visitParentView isEqualToString:@"MO"])
    {
        NSLog(@"check sales order dict in manage orders %@", [salesOrderDict description]);
        
        NSString* docNumber=[[[salesOrderDict valueForKey:@"OrderItems"] valueForKey:@"Orig_Sys_Document_Ref"] objectAtIndex:0];
        
        
        NSString* customerDocRef=[NSString stringWithFormat:@"select IFNULL(Customer_PO_Number,0) AS Customer_PO_Number from tbl_Order where Orig_Sys_Document_Ref ='%@' ",docNumber];
        NSLog(@"query for fetching doc number is %@", customerDocRef);
    }
}

-(void)presentCustomPreviewViewController
{
    [self.view endEditing:YES];
    
    AlSeerSalesOrderPreviewViewController* previewVC=[[AlSeerSalesOrderPreviewViewController alloc]init];
    NSLog(@"sales order dict before preview is %@", salesOrderDict);
    
    previewVC.salesOrderDict=salesOrderDict;
    previewVC.delegate=self;
    [self presentPopupViewController:previewVC animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    signVc=[[AlSeerSignatureViewController alloc]init];
    [customSignatureView addSubview:signVc.view];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [signVc removeFromParentViewController];
    signVc = nil;
}

#pragma mark Save and close order methods

- (IBAction)saveButtonTapped:(id)sender {
    UIAlertAction *yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self generateReceiptPDFDocument:performaOrderRef];
    }];
    
    UIAlertAction *noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        [self closeViewIfCameFromSalesOrder];
    }];
    
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"" andMessage:@"Send Sales Order for Review?" andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
}

- (IBAction)confirmButtonTapped:(id)sender {
    [self.view endEditing:YES];
    //saving signature to documents directory
    //fetch image from defaults
    
    NSData* savedImageData=[SWDefaults fetchSignature];
    NSLog(@"check the saved image tyoe %@", savedImageData);
    
    UIImage *image;
    if (savedImageData) {
        CGFloat screenScale = [[UIScreen mainScreen] scale];
        image = [UIImage imageWithData:savedImageData scale:screenScale];
        [orderAdditionalInfoDict setObject:savedImageData forKey:@"signature"];
    }
    
    if([self validateInput])
    {
        [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:performaOrderRef];
        NSMutableDictionary *infoOrder = [NSMutableDictionary dictionaryWithDictionary:[salesOrderDict objectForKey:@"info"]] ;
        [infoOrder setValue:orderAdditionalInfoDict forKey:@"extraInfo"];
        [infoOrder setValue:@"I" forKey:@"order_Status"];
        [salesOrderDict setValue:infoOrder forKey:@"info"];
        
        
        NSLog(@"sales order dict with all details %@", [salesOrderDict description]);
        
        NSString *orderRef = [[SWDatabaseManager retrieveManager] saveOrderWithCustomerInfo:customerDict andOrderInfo:salesOrderDict andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
        if (orderRef) {
            [self signatureSaveImage:image withName:[NSString stringWithFormat:@"%@.jpg",orderRef] ];
            
            UIAlertAction *okAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertOkButtonTitle,nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                dispatch_async(dispatch_get_main_queue(),
                       ^{
                    Singleton *single = [Singleton retrieveSingleton];
                    
                    UIAlertAction *yesAction = [UIAlertAction
                                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                                style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action)
                                                {
                        [self generateReceiptPDFDocument:orderRef];
                    }];
                    
                    UIAlertAction *noAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                        if([single.visitParentView isEqualToString:@"MO"])
                        {
                            [self closeViewIfCameFromManageOrder];
                        }
                        else
                        {
                            [self closeViewIfCameFromSalesOrder];
                        }
                    }];
                    
                    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"" andMessage:@"Would you like to send the Confirmed Order via email?" andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
                });
            }];
            
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Order Created Successfully" andMessage:[NSString stringWithFormat:@"%@: \n %@. \n",NSLocalizedString(@"Reference No", nil),orderRef] andActions:[NSMutableArray arrayWithObjects:okAction, nil] withController:self];
        }
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please provide 'Signature' with 'Name'." withController:self];
    }
}

-(void) generateReceiptPDFDocument:(NSString *)orderRef {
    salesOrderDict[@"orderRef"] = orderRef;
    
    long numberOfPage = (self.itemsSalesOrder.count % 10 == 0) ? (self.itemsSalesOrder.count / 10) : (self.itemsSalesOrder.count / 10)+1;
    if (numberOfPage == 0) {
        numberOfPage = 1;
    }
    NSMutableData *pdfData = [[NSMutableData alloc]init];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);
    
    for (int i = 0; i < numberOfPage; i++) {
        SalesOrderTemplate *pdfTemplate = [[SalesOrderTemplate alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
        pdfTemplate.pageNumber = i +1;
        pdfTemplate.customerDict = customerDict;
        pdfTemplate.salesOrderDict = salesOrderDict;
        pdfTemplate.itemsSalesOrder = _itemsSalesOrder;
        pdfTemplate.orderAdditionalInfoDict = orderAdditionalInfoDict;
        [pdfTemplate updatePDF];
        UIGraphicsBeginPDFPage();
        struct CGContext *pdfContext = UIGraphicsGetCurrentContext();
        if(pdfContext) {
            [pdfTemplate.layer renderInContext:pdfContext];
        }
    }
    UIGraphicsEndPDFContext();
    
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[NSString stringWithFormat:@"Sales Order Receipt - %@",self.customerDict[@"Customer_Name"]]];
        [mailViewController setMessageBody:@"Hi,\n Please find the attached receipt." isHTML:NO];
        [mailViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"Sares Order Receipt- %@.pdf",self.customerDict[@"Customer_No"]]];
//        [mailViewController setToRecipients:[NSArray arrayWithObjects:@"neha.gupta@ushyaku.com",nil]];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:true completion:^{
        
        Singleton *single = [Singleton retrieveSingleton];
        if([single.visitParentView isEqualToString:@"MO"])
        {
            [self closeViewIfCameFromManageOrder];
        } else {
            [self closeViewIfCameFromSalesOrder];
        }
    }];
    return;
}

-(void)closeViewIfCameFromSalesOrder {
    Singleton *single = [Singleton retrieveSingleton];
    BOOL duesScreen=single.isDueScreen;
    NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
    
    if (duesScreen==YES) {
        NSLog(@"coming from dues");
        
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-4)] animated:YES];
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
    }
}

-(void)closeViewIfCameFromManageOrder {
    NSInteger noOfViewControllers = [self.navigationController.viewControllers count];
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(noOfViewControllers-3)] animated:YES];
}

- (BOOL)validateInput
{
    BOOL isOnsiteVisit = [SWDefaults fetchOnsiteVisitStatus];
    if (isOnsiteVisit==NO) {
        return YES;
    }
    else
    {
        if (![orderAdditionalInfoDict objectForKey:@"name"])
        {
            return NO;
        }
        else if ([[orderAdditionalInfoDict objectForKey:@"name"] length] < 1)
        {
            return NO;
        }
        
        if (![orderAdditionalInfoDict objectForKey:@"signature"])
        {
            return NO;
        }
        else if ([[orderAdditionalInfoDict objectForKey:@"signature"] length] < 1)
        {
            return NO;
        }
        return YES;
    }
}


#pragma mark saving signature methods

- (NSString*) dbGetImagesDocumentPath
{
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}

#pragma mark UItextField Delegate methods
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (textField==nameTxtFld) {
        return newLength <= 12;
    }
    else if (textField==txtfldCustomerDocRef) {
        return newLength <= 15;
    }
    return 100;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtfldCustomerDocRef) {
        [textField resignFirstResponder];
        [commentsTxtView becomeFirstResponder];
        return NO;
    }
    else if (textField==nameTxtFld)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==nameTxtFld) {
        [orderAdditionalInfoDict setObject:textField.text forKey:@"name"];
    }
    else if (textField==txtfldCustomerDocRef)
    {
        [orderAdditionalInfoDict setObject:textField.text forKey:@"reference"];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==commentsTxtView) {
        [orderAdditionalInfoDict setObject:textView.text forKey:@"comments"];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(textView.text.length == 0 && ([text isEqualToString:@"\n"] || [text isEqualToString:@" "])) {
        return NO;
    }
    if (textView.text.length + (text.length - range.length) <= 150) {
        return YES;
    }
    else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Limit Reached" andMessage:@"please make sure that your comments are less than 150 characters" withController:self];
        return NO;
    }
}

#pragma mark UITableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.itemsSalesOrder.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"salesOrderCell";
    AlSeerSalesOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:233.0/255.0 green:251.0/255.0 blue:250.0/255.0 alpha:1.0]];
    
    
    
    cell.productNameLbl.text=@"Product";
    cell.quantityLbl.text=@"Quantity";
    cell.uomLbl.text=@"UOM";
    cell.unitPriceLbl.text=@"Unit Price (AED)";
    cell.totalLbl.text=@"Total (AED)";
    
    
    cell.productNameLbl.textColor  = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.quantityLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.uomLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.unitPriceLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    cell.totalLbl.textColor = [UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0];
    [cell setBorderColor:[UIColor colorWithRed:218.0/255.0 green:219.0/255.0 blue:222.0/255.0 alpha:1.0]];
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"salesOrderCell";
    AlSeerSalesOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSMutableArray *itemArray = [self.itemsSalesOrder objectAtIndex:indexPath.row];
    cell.productNameLbl.text = [NSString stringWithFormat:@"%@", [itemArray valueForKey:@"Description"]];
    cell.quantityLbl.text = [NSString stringWithFormat:@"%@", [itemArray valueForKey:@"Qty"]];
    cell.unitPriceLbl.text = [NSString stringWithFormat:@"%@", [itemArray valueForKey:@"Unit_Selling_Price"]];
    cell.uomLbl.text = [NSString stringWithFormat:@"%@", [itemArray valueForKey:@"Order_Quantity_UOM"]];
    cell.totalLbl.text = [NSString stringWithFormat:@"%@", [itemArray valueForKey:@"Price"]];
    
    return cell;
}

@end
