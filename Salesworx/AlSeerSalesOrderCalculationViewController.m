//
//  AlSeerSalesOrderCalculationViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/13/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSalesOrderCalculationViewController.h"
#import "ProductOrderViewController.h"
#import "Singleton.h"
#import "TamplateNameViewController.h"
#include <CommonCrypto/CommonHMAC.h>
#import "AlSeerSalesOrderPreviewViewController.h"


#import "MJPopupBackgroundView.h"
#import "UIViewController+MJPopupViewController.h"


#define NUMERIC  @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890 "

@interface AlSeerSalesOrderCalculationViewController (){
 
    float temp1;
    float temp2;
    float temp3;
    float temp4;
    float temp5;
}

@end

@implementation AlSeerSalesOrderCalculationViewController
@synthesize delegate,wareHouseDataSalesOrder,wareHouseDataOldSalesArray;


- (id) initWithCustomer:(NSDictionary *)c andCategory:(NSDictionary *)categorya{
    self = [super init];
    if (self)
    {
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionaryWithDictionary:categorya];
        
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        [self setItems:[NSMutableArray array]];
        Singleton *single = [Singleton retrieveSingleton];
        single.isManageOrder=NO;
        single.manageOrderRefNumber = @"SO";
        isManageOrder = NO;
        
        
        NSLog(@"got new al seer cal");
        
        
        
        
        
        //        mslOrderItem1 = [[UIButton alloc] init] ;
        //        [mslOrderItem1 setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        //        [mslOrderItem1 setFrame:CGRectMake(320,self.gridView.bounds.size.height + 50, 140, 42)];
        //        [mslOrderItem1 addTarget:self action:@selector(mslItems:) forControlEvents:UIControlEventTouchUpInside];
        //        [mslOrderItem1 bringSubviewToFront:self.view];
        //        mslOrderItem1.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        //        [mslOrderItem1 setTitle:NSLocalizedString(@"Load from MSL1", nil) forState:UIControlStateNormal];
        //        mslOrderItem1.titleLabel.font = BoldSemiFontOfSize(15);
        //        [self.view addSubview:mslOrderItem1];
        
        
        
        
        
        
        
        
        //        saveButton = [[UIButton alloc] init] ;
        //        [saveButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        //        [saveButton setFrame:CGRectMake(360,self.gridView.bounds.size.height + 50, 120, 42)];
        //        [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
        //        [saveButton bringSubviewToFront:self.view];
        //        saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        //        [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
        //        saveButton.titleLabel.font = BoldSemiFontOfSize(15);
        //        [self.view addSubview:saveButton];
        //
        //        saveAsTemplateButton = [[UIButton alloc] init] ;
        //        [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        //        [saveAsTemplateButton setFrame:CGRectMake(490,self.gridView.bounds.size.height + 50, 120, 42)];
        //        [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
        //        [saveAsTemplateButton bringSubviewToFront:self.view];
        //        saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        //        [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
        //        saveAsTemplateButton.titleLabel.font =BoldSemiFontOfSize(15);
        //        [self.view addSubview:saveAsTemplateButton];
        
        
        
        
        
        
        
        if (!single.isDistributionChecked)
        {
        }
        else
        {
            [mslOrderItem setEnabled:NO];
        }
        
        
        
    }
    return self;
}


- (id) initWithCustomer:(NSDictionary *)c andOrders:(NSDictionary *)order andManage:(NSString *)manage{
    self = [super init];
    if (self)
    {
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        
        Singleton *single = [Singleton retrieveSingleton];
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionary];
        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
        savedCategoryID = [selectedOrder stringForKey:@"Custom_Attribute_2"];
        manageOrder = manage;
        
        if (![customer objectForKey:@"Ship_Customer_ID"])
        {
            NSString *shipCustomer = [customer stringForKey:@"Customer_ID"];
            NSString *shipSite = [customer stringForKey:@"Site_Use_ID"];
            [customer setValue:shipCustomer forKey:@"Ship_Customer_ID"];
            [customer setValue:shipSite forKey:@"Ship_Site_Use_ID"];
        }
        
        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
        
        if([selectedOrder objectForKey:@"Schedule_Ship_Date"])
        {
            [self dbGetSalesOrderServiceDiddbGetConfirmOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesConfirmOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
        }
        else
        {
            //Saad Line
            [self dbGetSalesOrderServiceDiddbGetPerformaOrderItems:[[SWDatabaseManager retrieveManager] dbGetSalesPerformaOrderItems:[selectedOrder objectForKey:@"Orig_Sys_Document_Ref"]]];
        }
        
        single.isManageOrder=YES;
        isManageOrder = YES;
        single.manageOrderRefNumber = [selectedOrder objectForKey:@"Orig_Sys_Document_Ref"];
    }
    return self;
}

- (id) initWithCustomer:(NSDictionary *)c andTemplate:(NSDictionary *)order andTemplateItem:(NSArray *)orderItems{
    self = [super init];
    if (self)
    {
        
        
        [self setTitle:NSLocalizedString(@"Sales Order", nil)];
        customer = [NSMutableDictionary dictionaryWithDictionary:c];
        category = [NSDictionary dictionary];
        selectedOrder=[NSMutableDictionary dictionaryWithDictionary:order] ;
        savedCategoryID = [order stringForKey:@"Custom_Attribute_1"];
        
        
        name_TEMPLATES = [order stringForKey:@"Template_Name"];
        
        //serProduct.delegate=self;
        
        [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
        [self setItems:orderItems];
        [self.gridView reloadData];
        [self updateTotal];
        
    }
    return self;
}




- (void) viewWillAppear:(BOOL)animated{
    
    [Flurry logEvent:@"Sales Order  View"];
    
    UIBarButtonItem* previewBtn=[[UIBarButtonItem alloc]initWithTitle:@"Preview" style:UIBarButtonItemStylePlain target:self action:@selector(presentCustomPreviewViewController)];
    self.navigationController.navigationItem.rightBarButtonItem=previewBtn;
    self.navigationController.toolbarHidden=YES;
}
- (void) viewDidLoad
{
    [super viewDidLoad];
    
    temp1 = 36.8f;
    temp2 = 15.0;
    temp3 = 15.0;
    temp4 = 17.0f;
    temp5 = 16.0f;
    
    int xTemp = 0;
    int yTemp = 45;
    int widthTemp = self.view.bounds.size.width;
    int heightTemp = self.view.bounds.size.height-100;
    
    gridView.frame=CGRectMake(xTemp, yTemp, widthTemp, heightTemp);
    gridView.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor clearColor];
    
    productDict = [NSMutableDictionary dictionary];
    arrayTemp = [NSMutableArray array];
    tempSalesArray = [NSMutableArray array];
    p = [NSMutableDictionary dictionary];
    b = [NSMutableDictionary dictionary ];
    f = [NSMutableDictionary dictionary ];
    deleteRowDict = [NSMutableDictionary dictionary ];
    
    Singleton *single = [Singleton retrieveSingleton];
    single.isSaveOrder = NO;
   
    
    if (saveButton==nil) {
        saveButton = [[UIButton alloc] init] ;
        [saveButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [saveButton setFrame:CGRectMake(610,self.gridView.bounds.size.height + 50, 120, 42)];
        [saveButton addTarget:self action:@selector(saveOrder:) forControlEvents:UIControlEventTouchUpInside];
        [saveButton bringSubviewToFront:self.view];
        saveButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [saveButton setTitle:NSLocalizedString(@"Save Order", nil) forState:UIControlStateNormal];
        saveButton.titleLabel.font = headerFont;
    }
    
    if (saveAsTemplateButton==nil) {
        saveAsTemplateButton = [[UIButton alloc] init] ;
        [saveAsTemplateButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [saveAsTemplateButton setFrame:CGRectMake(480,self.gridView.bounds.size.height + 50, 120, 42)];
        [saveAsTemplateButton addTarget:self action:@selector(saveTemplate:) forControlEvents:UIControlEventTouchUpInside];
        [saveAsTemplateButton bringSubviewToFront:self.view];
        saveAsTemplateButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [saveAsTemplateButton setTitle:NSLocalizedString(@"Save Template", nil) forState:UIControlStateNormal];
        saveAsTemplateButton.titleLabel.font =headerFont;
    }
    
    if (mslOrderItem==nil) {
        mslOrderItem = [[UIButton alloc] init] ;
        [mslOrderItem setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
        [mslOrderItem setFrame:CGRectMake(460,self.gridView.bounds.size.height + 50, 140, 42)];
        [mslOrderItem addTarget:self action:@selector(mslItems:) forControlEvents:UIControlEventTouchUpInside];
        [mslOrderItem bringSubviewToFront:self.view];
        mslOrderItem.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [mslOrderItem setTitle:NSLocalizedString(@"Load from MSL", nil) forState:UIControlStateNormal];
        mslOrderItem.titleLabel.font = headerFont;
    }
    if (!single.isDistributionChecked)
    {
        
    }
    else
    {
        [mslOrderItem setEnabled:NO];
    }
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeOrder)] ];
    [self.gridView setShouldAllowDeleting:YES];
    
    
    UILabel* bgLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 800, 45)];
    bgLbl.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgLbl];
    
    
    UILabel *orderItemLbl=[[UILabel alloc] initWithFrame:CGRectMake(8, 0, 133, 40)] ;
    [orderItemLbl setText:@"Order Items"];
    orderItemLbl.backgroundColor = [UIColor whiteColor];
    orderItemLbl.font = kFontWeblySleekSemiLight(16);
    orderItemLbl.textColor = UIColorFromRGB(0x2C394A);
    [self.view addSubview:orderItemLbl];
    
    orderItemCountLabel=[[UILabel alloc] initWithFrame:CGRectMake(100, 9.5, 33, 22)] ;
    orderItemCountLabel.backgroundColor = UIColorFromRGB(0x169C5C);  //[UIColor colorWithRed:22.0/255.0 green:156.0/255.0 blue:92.0/255.0 alpha:1.0];
    orderItemCountLabel.layer.cornerRadius = 9.0;
    orderItemCountLabel.textAlignment = NSTextAlignmentCenter;
    orderItemCountLabel.layer.masksToBounds = YES;
    [orderItemCountLabel setTextColor:[UIColor whiteColor]];
    [orderItemCountLabel setFont:kFontWeblySleekSemiBold(14)];
    [self.view addSubview:orderItemCountLabel];
    
    NSLog(@"CHECK  VIEW FRAME %@", NSStringFromCGRect(self.view.bounds));
    NSLog(@"CHECK GRID VIEW FRAME %@", NSStringFromCGRect(self.gridView.bounds));
        
    int tempY = 182;
    totalPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(520,tempY, 200, 30)] ;
    [totalPriceLabel setText:@"Total Order Value:"];
    [totalPriceLabel setTextAlignment:NSTextAlignmentLeft];
    [totalPriceLabel setTextColor:[UIColor darkGrayColor]];
    [totalPriceLabel setFont:kFontWeblySleekSemiLight(16)];
    totalPriceLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:totalPriceLabel];
    
    totalPriceValue=[[UILabel alloc] initWithFrame:CGRectMake(665,tempY, 200, 30)] ;
    [totalPriceValue setText:@""];
    [totalPriceValue setTextAlignment:NSTextAlignmentLeft];
    [totalPriceValue setTextColor:[UIColor blackColor]];
    [totalPriceValue setFont:kFontWeblySleekSemiBold(14)];
    totalPriceValue.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:totalPriceValue];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    ENABLE_TEMPLATES = appControl.ENABLE_TEMPLATES;
    [self updateTotal];
}

- (void) closeOrder
{
    if([self.items count] > 0 )
    {
        Singleton *single = [Singleton retrieveSingleton];
        if(single.isSaveOrder)
        {
            [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
            [self.navigationController  popViewControllerAnimated:YES];
        }
        else {
            if([manageOrder isEqualToString:@"confirmed"])
            {
                [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                [self.navigationController  popViewControllerAnimated:YES];
            }
            else
            {
                UIAlertAction *yesAction = [UIAlertAction
                                            actionWithTitle:NSLocalizedString(KAlertYESButtonTitle,nil)
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                    [self.navigationController  popViewControllerAnimated:YES];
                }];
                
                UIAlertAction *noAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(KAlertNoButtonTitle,nil)
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action)
                                           {
                }];
                
                [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:NSLocalizedString(@"Would you like to cancel the order without saving?", nil) andActions:[NSMutableArray arrayWithObjects:yesAction, noAction, nil] withController:self];
            }
        }
    }
    else
    {
        [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
        [self.navigationController  popViewControllerAnimated:YES];
    }
}
- (void) presentProductList:(id)sender{
    Singleton *single = [Singleton retrieveSingleton];
    
    if ([single.visitParentView isEqualToString:@"SO"])
    {
        [SWDefaults setProductCategory:category];
        [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
    }
    else
    {
        [self categorySelected:category];
    }
    NSLog(@"setProductCategory 2");
    
}
#pragma mark Product Service Delegate
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories {
    //categoryArray=categories;
    
    categoryArray = [NSMutableArray arrayWithArray:categories];
    
    for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
            category=[NSDictionary dictionaryWithDictionary:row];
            [SWDefaults setProductCategory:category];
            break;
        }
    }
    
    //NSLog(@"setProductCategory 3 %@",[SWDefaults productCategory]);
    
    
    //    productListViewController = [[ProductListViewController alloc] initWithCategory:category] ;
    //    [productListViewController setTarget:self];
    //    [productListViewController setAction:@selector(productSelected:)];
    //    navigationControllerS = [[UINavigationController alloc] initWithRootViewController:productListViewController] ;
    //    productListViewController.modalTransitionStyle=UIModalTransitionStyleCoverVertical;
    categories=nil;
}
- (void) categorySelected:(NSDictionary *)product{
    
    [self.navigationController presentViewController:navigationControllerS animated:YES completion:nil];
}
- (void) presentProductOrder:(NSDictionary *)product {
    
    if (![product objectForKey:@"Guid"])
    {
        [product setValue:[NSString createGuid] forKey:@"Guid"];
    }
    if (isManageOrder)
    {
        if(![product objectForKey:@"ItemID"])
        {
            [product setValue:[product stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
        }
    }
    [self.delegate selectedProduct:[product mutableCopy]];
    //    orderViewController = [[ProductOrderViewController alloc] initWithProduct:product] ;
    //    [orderViewController setTarget:self];
    //    [orderViewController setAction:@selector(productAdded:)];
    //    [self.navigationController pushViewController:orderViewController animated:YES];
    //    orderViewController = nil ;
    product=nil;
}
#pragma mark ProductListView Controller action
- (void) productSelected:(NSDictionary *)product{
    productDict = [NSMutableDictionary dictionaryWithDictionary:product];
    //serProduct.delegate=self;
//    [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager] checkGenericPriceOfProduct:[product stringForKey:@"ItemID"]]];
//     [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
    
    
     [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[product stringForKey:@"ItemID"] :[customer stringForKey:@"Price_List_ID"]]];
    
    productDict=nil;
}
- (void) dbGetProductServiceDidGetCheckedPrice:(NSArray *)priceDetail{
    if([priceDetail count]!=0)
    {
        if([[[priceDetail objectAtIndex:0]stringForKey:@"Price_List_ID"] isEqualToString:[customer stringForKey:@"Price_List_ID"]])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
        }
        else if ([[[priceDetail objectAtIndex:0] stringForKey:@"Is_Generic"] isEqualToString:@"Y"])
        {
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
            [productDict setValue:[[priceDetail objectAtIndex:0]stringForKey:@"Unit_List_Price"] forKey:@"List_Price"];
            //[self performSelector:@selector(presentProductOrder:) withObject:productDict afterDelay:0.50f];
        }
        else
        {
            // no price . stop here
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No prices available for selected product." withController:self];
    }
    priceDetail=nil;
}
- (void)mslItems:(id)sender
{
    
    NSMutableArray* testProdArray=[[NSMutableArray alloc] init];

    Singleton *single = [Singleton retrieveSingleton];
    
    NSMutableArray *distItem = [[SWDatabaseManager retrieveManager] fetchDataForQuery:[NSString stringWithFormat:@"SELECT A.Inventory_Item_ID , B.Category,B.Organization_ID  FROM TBL_Distribution_Check_Items AS A , TBL_Product AS B WHERE A.Inventory_Item_ID = B.Inventory_Item_ID AND A.DistributionCheck_ID = '%@' AND A.Is_Available='N' ",single.distributionRef]];
    NSMutableArray *tempArray = [NSMutableArray new];
    BOOL isCategory=NO;
    for (int i=0 ; i<[distItem count]; i++)
    {
        if ([[[distItem objectAtIndex:i] stringForKey:@"Category"] isEqualToString:[[SWDefaults productCategory] stringForKey:@"Category"]])
        {
            [tempArray addObject:[distItem objectAtIndex:i]];
            isCategory=YES;
        }
    }
    
    NSMutableArray *tempArray1 = [NSMutableArray new];
    
    for (int i = 0 ; i<tempArray.count; i++)
    {
        
        
        NSLog(@"temp array in order calculation %@", [tempArray description]);
        
        
        NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] organizationId:[[tempArray objectAtIndex:i] stringForKey:@"Organization_ID"]];
        
        
//         [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[mainProductDict stringForKey:@"ItemID"] :[customerDict stringForKey:@"Price_List_ID"]]];
        
//        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] :[customer stringForKey:@"Price_List_ID"]]];
//
        
        [self dbGetProductServiceDidGetCheckedPrice:[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] :[customer stringForKey:@"Price_List_ID"]]];
        
        
        
        NSArray* tempProd=[[SWDatabaseManager retrieveManager]checkGenericPriceOfProduct:[[tempArray objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] :[customer stringForKey:@"Price_List_ID"]];

        NSLog(@"temp prod al seer is %@", [tempProd objectAtIndex:0]);
        
        
        
        
        //fetch primary uom fo msl
        
        
        NSString* primartUOMQry=[NSString stringWithFormat:@"SELECT Primary_UOM_Code FROM tbl_Product where Inventory_Item_ID='%@' ", [[distItem valueForKey:@"Inventory_Item_ID"] objectAtIndex:i]];
        
        NSMutableArray* primaryUOMArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:primartUOMQry];
        
        NSLog(@"Primary UOM for MSL is %@", [[primaryUOMArray valueForKey:@"Primary_UOM_Code"] objectAtIndex:0]);
        
        NSString* priumaryProductUOMStr=[[primaryUOMArray valueForKey:@"Primary_UOM_Code"] objectAtIndex:0];
        
        
        //now take only the object which has matching primary UOM
        
        
        for (NSInteger i=0; i<tempProd.count; i++) {
            
            
            if ([[[tempProd valueForKey:@"Item_UOM"] objectAtIndex:i]isEqualToString:priumaryProductUOMStr]) {
                
                
                [testProdArray addObject:[tempProd objectAtIndex:i]];
            }
            
        }
        
        
        
       // [testProdArray addObjectsFromArray:tempProd ];
        
        NSLog(@"final prd array is %@", [testProdArray description]);
        
        
//               NSMutableDictionary *tempDict =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
        NSMutableDictionary *tempDict =[NSMutableDictionary dictionaryWithDictionary:[testProdArray objectAtIndex:i]];

        
        NSLog(@"check temp dict for price %@", [tempDict description]);
        
        
        NSString *itemID = [tempDict stringForKey:@"Inventory_Item_ID"];
        [tempDict setValue:itemID forKey:@"ItemID"];
        [tempDict setValue:[[produstDetail valueForKey:@"Description"] objectAtIndex:0] forKey:@"Description"];
        
        [tempDict setValue:@"1" forKey:@"Qty"];
        [tempDict setValue:[NSString createGuid] forKey:@"Guid"];
        [tempDict setValue:@"N" forKey:@"priceFlag"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
        [tempDict setValue:[tempDict stringForKey:@"Unit_Selling_Price"] forKey:@"Discounted_Price"];
        
        [tempArray1 addObject:tempDict];
    }
    
    if (isCategory) {
        //single.isDistributionChecked = NO;
        single.isDistributionItemGet = YES;
        [mslOrderItem setEnabled:NO];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Message", nil) andMessage:@"No MSL items are available in this product category." withController:self];
    }
    
    NSMutableArray *templateItemsArray = [NSMutableArray array];
    BOOL isBonusDone;
    for(int i = 0; i < tempArray1.count; i++)
    {
        NSMutableDictionary *iTemDict= [tempArray1 objectAtIndex:i];
        NSMutableArray *bonusArray = [NSMutableArray arrayWithArray:[[[SWDatabaseManager retrieveManager] dbGetBonusInfoOfProduct:[iTemDict stringForKey:@"Item_Code"]] mutableCopy]];
        int quantity = 0;
        quantity = [[iTemDict objectForKey:@"Qty"] intValue];
        int bonus = 0;
        isBonusDone=NO;
        //  NSLog(@"NAme %@",[iTemDict stringForKey:@"Description"]);
        if([bonusArray count]!=0)
        {
            for(int j=0 ; j<[bonusArray count] ; j++ )
            {
                NSDictionary *row = [bonusArray objectAtIndex:j];
                int rangeStart = [[row objectForKey:@"Prom_Qty_From"] intValue];
                int rangeEnd = [[row objectForKey:@"Prom_Qty_To"] intValue];
                if (quantity >= rangeStart && quantity <=rangeEnd)
                {
                    //NSMutableDictionary *bonusProduct=[NSMutableDictionary new];
                    if(j == [bonusArray count]-1)
                    {
                        if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"RECURRING"])
                        {
                            int dividedValue = quantity / rangeStart ;
                            bonus = [[row objectForKey:@"Get_Qty"] intValue] * floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            // [iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                        else if([[row stringForKey:@"Price_Break_Type_Code"] isEqualToString:@"PERCENT"])
                        {
                            int dividedValue = [[row objectForKey:@"Get_Qty"] intValue] * (quantity / rangeStart) ;
                            bonus = floor(dividedValue) ;
                            
                            NSString *childGUID = [NSString createGuid];
                            
                            NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                            [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                            [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                            [bonusProduct setValue:childGUID forKey:@"Guid"];
                            // [bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                            [bonusProduct setValue:@"0" forKey:@"Price"];
                            [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                            
                            [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                            [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                            [templateItemsArray addObject:iTemDict];
                            [templateItemsArray addObject:bonusProduct];
                            isBonusDone=YES;
                        }
                        
                    }
                    else
                    {
                        bonus = [[row objectForKey:@"Get_Qty"] intValue];
                        NSString *childGUID = [NSString createGuid];
                        
                        NSMutableDictionary *bonusProduct = [NSMutableDictionary dictionaryWithDictionary:[[SWDatabaseManager retrieveManager] dbGetBonusItemOfProduct:[row stringForKey:@"Get_Item"]]];
                        [bonusProduct setValue:[NSString stringWithFormat:@"%d",bonus]  forKey:@"Qty"];
                        [bonusProduct setValue:@"F"  forKey:@"priceFlag"];
                        [bonusProduct setValue:childGUID forKey:@"Guid"];
                        //[bonusProduct setValue:[NSString createGuid] forKey:@"Guid"];
                        [bonusProduct setValue:@"0" forKey:@"Price"];
                        [bonusProduct setValue:@"0" forKey:@"Discounted_Price"];
                        
                        [iTemDict setValue:bonusProduct   forKey:@"Bonus_Product"];
                        [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                        //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                        int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                        double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                        [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                        [iTemDict setValue:childGUID forKey:@"ChildGuid"];
                        [templateItemsArray addObject:iTemDict];
                        [templateItemsArray addObject:bonusProduct];
                        isBonusDone=YES;
                    }
                }
                
            }
            if (!isBonusDone)
            {
                [iTemDict setValue:@"N"   forKey:@"priceFlag"];
                //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
                int qty = [[iTemDict stringForKey:@"Qty"] intValue];
                double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
                [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
                [templateItemsArray addObject:iTemDict];
            }
        }
        else
        {
            [iTemDict setValue:@"N"   forKey:@"priceFlag"];
            //[iTemDict setValue:[NSString createGuid] forKey:@"Guid"];
            int qty = [[iTemDict stringForKey:@"Qty"] intValue];
            double totalPrice = (double)qty * ([[iTemDict objectForKey:@"Net_Price"] doubleValue]);
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [iTemDict setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Discounted_Price"];
            [templateItemsArray addObject:iTemDict];
        }
    }
    
    
    
    self.items = templateItemsArray;
    
    [self.gridView reloadData];
    [self updateTotal];
    
    
    
}
- (void) productAdded:(NSMutableDictionary *)q{
    
    arrayTemp=nil;
    p=nil;
    arrayTemp= [NSMutableArray arrayWithArray:self.items];
    p=q;
    if([[q stringForKey:@"Qty"] isEqualToString:@"0"] || [[q stringForKey:@"Qty"] isEqualToString:@""])
    {
        for (int i = arrayTemp.count - 1; i >= 0; i--)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
            else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
            {
                [arrayTemp removeObjectAtIndex:i];
            }
        }
    }
    else
    {
        @autoreleasepool
        {
            for (int i = arrayTemp.count - 1; i >= 0; i--)
            {
                dataItemArray=nil;
                dataItemArray = [self.items objectAtIndex:i];
                if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"Guid"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
                else if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:[p stringForKey:@"ChildGuid_1"]])
                {
                    [arrayTemp removeObjectAtIndex:i];
                }
            }
        }
        
        if ([p objectForKey:@"Bonus_Product"])
        {
            b=nil;
            b = [p objectForKey:@"Bonus_Product"];
            [b setValue:[NSString createGuid] forKey:@"Guid"];
            [b setValue:@"0" forKey:@"Price"];
            [b setValue:@"0" forKey:@"Bonus"];
            [b setValue:@"0" forKey:@"Discounted_Price"];
            [b setValue:@"0" forKey:@"DiscountPercent"];
            [b setValue:@"F" forKey:@"priceFlag"];
            
            [p setValue:[b stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [b setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [p setValue:@"N" forKey:@"priceFlag"];
            
            if([p objectForKey:@"FOC_Product"])
            {
                f=nil;
                f = [p objectForKey:@"FOC_Product"];
                [f setValue:[NSString createGuid] forKey:@"Guid"];
                [f setValue:@"0" forKey:@"Price"];
                [f setValue:@"0" forKey:@"Bonus"];
                [f setValue:@"0" forKey:@"Discounted_Price"];
                [f setValue:@"0" forKey:@"DiscountPercent"];
                [f setValue:@"M" forKey:@"priceFlag"];
                [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid_1"];
                [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
                [arrayTemp addObject:p];
                [arrayTemp addObject:b];
                [arrayTemp addObject:f];
            }
            else
            {
                [arrayTemp addObject:p];
                [arrayTemp addObject:b];
            }
        }
        else if([p objectForKey:@"FOC_Product"])
        {
            f=nil;
            f = [p objectForKey:@"FOC_Product"];
            [f setValue:[NSString createGuid] forKey:@"Guid"];
            [f setValue:@"0" forKey:@"Price"];
            [f setValue:@"0" forKey:@"Bonus"];
            [f setValue:@"0" forKey:@"Discounted_Price"];
            [f setValue:@"0" forKey:@"DiscountPercent"];
            [f setValue:@"M" forKey:@"priceFlag"];
            [p setValue:[f stringForKey:@"Guid"] forKey:@"ChildGuid"];
            [f setValue:[p stringForKey:@"Guid"] forKey:@"ParentGuid"];
            [p setValue:@"N" forKey:@"priceFlag"];
            
            [arrayTemp addObject:p];
            [arrayTemp addObject:f];
        }
        else
        {
            [p setValue:@"N" forKey:@"priceFlag"];
            [arrayTemp addObject:p];
        }
    }
    
    [self setItems:arrayTemp];
    [self.gridView reloadData];
    
    
    //scrolling to last row
    
        [gridView.tableView reloadData];
        NSInteger lastRowNumber = [gridView.tableView  numberOfRowsInSection:0] - 1;
        NSIndexPath* ip = [NSIndexPath indexPathForRow:lastRowNumber inSection:0];
        [gridView.tableView  scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
    

    
    [self updateTotal];
    arrayTemp=nil;
    p=nil;
    b=nil;
    f=nil;
    q=nil;
    
    [orderItemCountLabel setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.items.count]];
    
}
- (void) setupToolbar {
    
    amountString =[NSString stringWithFormat:@"%@ : ",NSLocalizedString(@"Total Price", nil)];
    amountString = [amountString stringByAppendingString:[[NSString stringWithFormat:@"%.2f",totalOrderAmount] currencyString]];
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        
        saveButton.hidden=YES;
        
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            saveAsTemplateButton.hidden=NO;
        }
        else
        {
            saveAsTemplateButton.hidden=YES;
        }
        mslOrderItem.hidden=YES;
        saveAsTemplateButton.hidden = YES;//OLA!!
    }
    else
    {
        saveButton.hidden=NO;
        if([ENABLE_TEMPLATES isEqualToString:@"Y"])
        {
            saveAsTemplateButton.hidden=NO;
        }
        else
        {
            saveAsTemplateButton.hidden=YES;
        }
    }
}


- (void) updateTotal {
    price = 0.0f;
    discountAmt = 0.0f;
    for (int i = 0;i < self.items.count; i++)
    {
        dataItemArray=nil;
        dataItemArray = [self.items objectAtIndex:i];
        if([dataItemArray objectForKey:@"Discounted_Price"])
        {
            Singleton *single = [Singleton retrieveSingleton];
            if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"notconfirmed"] )
            {
                NSLog(@"this is manage order scrrrn total %@", [dataItemArray description]);
                int qty = [[dataItemArray valueForKey:@"Qty"] intValue];
                
                
                
                //manage order with foc
                double totalPrice;
                
                if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
                    totalPrice   = (double)qty * ([[dataItemArray valueForKey:@"Price"] doubleValue]);
                }
                else {
                    totalPrice = (double)qty * ([[dataItemArray valueForKey:@"Net_Price"] doubleValue]);
                }
                
                double dicounttedPrice = totalPrice * ([[dataItemArray valueForKey:@"DiscountPercent"] doubleValue])/(double)100;
                double dicountedAmt = totalPrice - dicounttedPrice;
                
                price = price + [[NSString stringWithFormat:@"%f",dicountedAmt] doubleValue];
                discountAmt = discountAmt + [[NSString stringWithFormat:@"%f",dicounttedPrice] doubleValue];
            }
            
            else
            {
                price = price + [[dataItemArray objectForKey:@"Discounted_Price"] doubleValue];
                discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
            }
        }
        else
        {
            //case new item added in manage order / unconfirmed
            
            Singleton *single = [Singleton retrieveSingleton];
            if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"notconfirmed"] )
                
            {
                NSLog(@"this is manage order scrrrn total %@", [dataItemArray description]);
                int qty = [[dataItemArray valueForKey:@"Qty"] intValue];
                double totalPrice = (double)qty * ([[dataItemArray valueForKey:@"Net_Price"] doubleValue]);
                
                double dicounttedPrice = totalPrice * ([[dataItemArray valueForKey:@"DiscountPercent"] doubleValue])/(double)100;
                
                double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
                double dicountedAmt = totalPrice - dicounttedPrice;
                price = price + [[NSString stringWithFormat:@"%f",dicountedAmt] doubleValue];
                discountAmt = discountAmt + [[NSString stringWithFormat:@"%f",dicounttedPrice] doubleValue];
            }
            else
            {
                price = price + [[dataItemArray objectForKey:@"Price"] doubleValue];
                discountAmt = discountAmt + [[dataItemArray objectForKey:@"Discounted_Amount"] doubleValue];
                
            }
        }
        
        totalDiscountAmount = discountAmt;
        totalOrderAmount = price;
        
        NSLog(@"total order amount is %f", totalOrderAmount);
    }
    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
    totalPriceValue.text=[NSString stringWithFormat:@"%@ %0.2f",userCurrencyCode, price];
    [orderItemCountLabel setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.items.count]];
    
    [self setupToolbar];
}

- (void) saveTemplate:(id)sender{
    if(items.count!=0)
    {
        TamplateNameViewController *viewController = [[TamplateNameViewController alloc] initWithTemplateName] ;
        [viewController setPreferredContentSize:CGSizeMake(600, self.view.bounds.size.height - 100)];
        [viewController setTarget:self];
        [viewController setAction:@selector(itemUpdated:)];
        viewController.templateName = name_TEMPLATES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController] ;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        [popoverController setDelegate:self];
        [popoverController presentPopoverFromRect:saveAsTemplateButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please add items first" withController:self];
    }
}
- (void) itemUpdated:(NSString *)item {
    NSCharacterSet *unacceptedInput = nil;
    unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:NUMERIC] invertedSet];
    if ([[item componentsSeparatedByCharactersInSet:unacceptedInput] count] <= 1)
    {
        Singleton *single = [Singleton retrieveSingleton];
        if (item)
        {
            [popoverController dismissPopoverAnimated:YES];
            NSMutableArray *tempTemplate = [NSMutableArray array];
            NSMutableDictionary *dataDict;
            for (int i = 0;i < self.items.count; i++)
            {
                
                if([[[self.items objectAtIndex:i] stringForKey:@"priceFlag"] isEqualToString:@"N"] || [[[self.items objectAtIndex:i] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
                {
                    dataDict =[NSMutableDictionary dictionary] ;
                    if(![[self.items objectAtIndex:i] objectForKey:@"ItemID"])
                    {
                        [dataDict setValue:[[self.items objectAtIndex:i] stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
                    }
                    else
                    {
                        [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"ItemID"] forKey:@"ItemID"];
                    }
                    [dataDict setValue:[[self.items objectAtIndex:i] objectForKey:@"Qty"] forKey:@"Qty"];
                    [tempTemplate addObject:dataDict];
                }
            }
            
            //salesSer.delegate=self;
            if([item isEqualToString:name_TEMPLATES])
            {
                [[SWDatabaseManager retrieveManager] deleteTemplateOrderWithRef:[selectedOrder stringForKey:@"Order_Template_ID"]];
                [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
                [SWDefaults showAlertAfterHidingKeyBoard:@"Template saved" andMessage:@"" withController:self];
            }
            else
            {
                BOOL isTheObjectThere = [single.savedOrderTemplateArray containsObject:item];
                if(isTheObjectThere)
                {
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Template name already exist." withController:self];
                }
                else
                {
                    [[SWDatabaseManager retrieveManager] saveTemplateWithName:item andOrderInfo:tempTemplate];
                    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Template saved", nil) andMessage:@"" withController:self];
                }
            }
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Template name can't have special characters. Please correct name and try to save again" withController:self];
    }
}
- (void) saveOrder:(id)sender{
    Singleton *single = [Singleton retrieveSingleton];
    NSMutableDictionary *info = [[NSMutableDictionary alloc] init] ;
    NSMutableDictionary *salesOrder = [[NSMutableDictionary alloc] init] ;
    NSString *orderRef;
    
    [tempSalesArray setArray:nil];
    
    
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        //dataDict = ;
        if(![dataDict objectForKey:@"ItemID"])
        {
            [dataDict setValue:[dataDict stringForKey:@"Inventory_Item_ID"] forKey:@"ItemID"];
        }
        
    }
    for (int i = 0 ; i < [self.items count]; i++){
        NSMutableDictionary *dataDict=[self.items objectAtIndex:i] ;
        /// dataDict = [self.items objectAtIndex:i];
        if([dataDict objectForKey:@"Discounted_Price"])
        {
            [dataDict setValue:[dataDict objectForKey:@"Discounted_Price"] forKey:@"Price"];
        }
        [tempSalesArray addObject:dataDict];
    }
    
    
    
    //copy selected uom into temp sales for unconfirmed manage orders, beacuase for un confirmed manage orders we wont have a Orig_Sys_Docnumber to fetch it from db.
    
    
    
    
    
    self.items=[tempSalesArray copy];
    
    NSLog(@"items in sales order %@", [self.items description]);
    
    [salesOrder setValue:self.items forKey:@"OrderItems"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalOrderAmount] forKey:@"orderAmt"];
    [info setValue:[NSString stringWithFormat:@"%.2f",totalDiscountAmount] forKey:@"discountAmt"];
    [info setValue:[category stringForKey:@"Category"] forKey:@"productCategory"];
    [salesOrder setValue:info forKey:@"info"];
    
    
    
    // add selected UOM in save Order
    
    
    
    
    category =[SWDefaults productCategory];
    
    if([single.visitParentView isEqualToString:@"MO"] || single.isSaveOrder){
        if(![self.items count]==0)
        {
            
            //NSLog(@"Currecnt Visit ID = %@",[SWDefaults currentVisitID]);
            
            //salesSer.delegate=self;
            [[SWDatabaseManager retrieveManager] deleteSalesPerformaOrderItems:single.savedPerformaCurrentOrder];
            orderRef = [[SWDatabaseManager retrieveManager] updatePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID] andOrderRef:single.savedPerformaCurrentOrder];
            
            
//            //NSLog(@"orderRef %@",orderRef);
//            orderAdditionalInfoViewController = nil;
//            orderAdditionalInfoViewController = [[OrderAdditionalInfoViewController alloc] initWithOrder:salesOrder andCustomer:customer] ;
//            orderAdditionalInfoViewController.performaOrderRef = orderRef;
//            orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
//            orderAdditionalInfoViewController.itemsSalesOrder=[self.items mutableCopy];
//            
//            NSLog(@"data being pushed to order addl final %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
//            
//            // orderAdditionalInfoViewController.warehouseDataArray=wareHouseDataSalesOrder;
//            //NSLog(@"ware house data being pushed to order addl %@", [orderAdditionalInfoViewController.warehouseDataArray description]);
//            
//            [self.navigationController pushViewController:orderAdditionalInfoViewController animated:YES];
//            orderAdditionalInfoViewController = nil;
            
            
            
            alseerOrderAdditionalInfoVC = nil;
            alseerOrderAdditionalInfoVC = [[AlSeerOrderAdditionalInfoViewController alloc] init] ;
            alseerOrderAdditionalInfoVC.salesOrderDict=salesOrder;
            alseerOrderAdditionalInfoVC.customerDict=customer;
            alseerOrderAdditionalInfoVC.performaOrderRef = orderRef;
            
            alseerOrderAdditionalInfoVC.warehouseDataArray=wareHouseDataSalesOrder;
            alseerOrderAdditionalInfoVC.itemsSalesOrder=[self.items mutableCopy];
            
            NSLog(@"data being pushed to order addl final %@", [alseerOrderAdditionalInfoVC.warehouseDataArray description]);
            
            [self.navigationController pushViewController:alseerOrderAdditionalInfoVC animated:YES];
            orderAdditionalInfoViewController = nil;
            
            
            
            
            
            
            
            
            
            
            
            
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please add items first" withController:self];
        }
    }
    else{
        if(self.items.count > 0)
        {
            single.isSaveOrder = YES;
            
            orderRef =  [[SWDatabaseManager retrieveManager] savePerformaOrderWithCustomerInfo:customer andOrderInfo:salesOrder andFSRInfo:[SWDefaults userProfile] andVisitID:[SWDefaults currentVisitID]];
            single.savedPerformaCurrentOrder = orderRef;
            
            alseerOrderAdditionalInfoVC = nil;
            alseerOrderAdditionalInfoVC = [[AlSeerOrderAdditionalInfoViewController alloc] init] ;
            alseerOrderAdditionalInfoVC.salesOrderDict=salesOrder;
            alseerOrderAdditionalInfoVC.customerDict=customer;
            alseerOrderAdditionalInfoVC.performaOrderRef = orderRef;
            
            alseerOrderAdditionalInfoVC.warehouseDataArray=wareHouseDataSalesOrder;
            alseerOrderAdditionalInfoVC.itemsSalesOrder=[self.items mutableCopy];
            
            NSLog(@"data being pushed to order addl final %@", [alseerOrderAdditionalInfoVC.warehouseDataArray description]);
            
            [self.navigationController pushViewController:alseerOrderAdditionalInfoVC animated:YES];
            orderAdditionalInfoViewController = nil;
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Please add items first" withController:self];
        }
    }
}

- (int)numberOfRowsInGrid:(GridView *)gridView
{
    return self.items.count;
}

//CRASH FIXED
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark GridView DataSource
- (int) numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return temp1;
            break;
        case 1:
            return  temp2;
            break;
        case 2:
            return  temp3;
            break;
            //        case 3:
            //            return 15.0f;
            //            break;
        case 3:
            return  temp4;
            break;
            
        case 4:
            return  temp5;
            break;
            
        default:
            break;
    }
    return 10.0f;
}


- (NSString *) gridView:(GridView *)gridView titleForColumn:(int)column {
    title=@"";
    switch (column) {
        case 0:
            title = NSLocalizedString(@"Product", nil);
            break;
        case 1:
            title = NSLocalizedString(@"Quantity", nil);
            break;
        case 2:
            title = NSLocalizedString(@"UOM", nil);
            break;
        case 3:
            title = NSLocalizedString(@"Unit Price (AED)", nil);
            break;
        case 4:
            title = NSLocalizedString(@"Total (AED)      ", nil);
            break;
       
        default:
            break;
    }
    
    return title;
}
- (NSString *) gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    
    
    
    title = @"";
    dataItemArray=nil;
    dataItemArray = [self.items objectAtIndex:row];
    NSString* selectedUOM=[dataItemArray valueForKey:@"SelectedUOM"];
    NSString* selectedUOMforMOConfirmed;
    Singleton *single = [Singleton retrieveSingleton];

    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

    NSString* currencyCheckerStr = [NSString stringWithFormat:@"%@0.00",userCurrencyCode];

    
    
    //check unconfirmed order
    
    
    
//    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])


    {
    
     selectedUOMforMOConfirmed=[[customer valueForKey:@"Order_Quantity_UOM"] objectAtIndex:row];
        
        
        NSLog(@"order qty uom in manage orders %@", selectedUOMforMOConfirmed);
        
    }
    //    NSString* titleStr=[dataItemArray valueForKey:@"Discounted_Amount"] ;
    //
    //    float titleVal=[titleStr floatValue]*100;
    NSString* unitPriceMO;
    NSString* checkNetPrice=[dataItemArray stringForKey:@"Net_Price"];
    
    NSLog(@"check net price for currency %@", checkNetPrice);

   
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        unitPriceMO=[dataItemArray stringForKey:@"Unit_Selling_Price"];
        
    }
    else
    {
        
    }
    
    
    
    switch (column) {
        case 0:
            title = [dataItemArray stringForKey:@"Description"];
            NSLog(@"Title is %@",title);
            break;
        case 1:
            title = [dataItemArray stringForKey:@"Qty"];
            break;
            
        case 2:
            
            if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])

            {
                title=selectedUOMforMOConfirmed;
            }
            
           else if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"notconfirmed"])
                
            {
                
                
                NSString* changedUOM=[dataItemArray stringForKey:@"Order_Quantity_UOM"];
                
                
                if ([changedUOM isEqualToString:@""]||[changedUOM isEqual:[NSNull null]]) {
                    
                    NSLog(@"data item array when order qty uom is nill %@", [customer description]);
                    
                    
                    
                    
                    title=[dataItemArray stringForKey:@"Primary_UOM_Code"];

                }
                
                else
                {
                title=[dataItemArray stringForKey:@"Order_Quantity_UOM"];
                }
                
                
            }
             else if (selectedUOM==nil) {
                
                title=[dataItemArray stringForKey:@"UOM"];
                
                
            }
            else
            {
            
            
            title = [dataItemArray stringForKey:@"SelectedUOM"];
            }
            break;
        case 3:
            
            if (unitPriceMO!=nil) {
                
                
                NSLog(@"check net price here %@", [dataItemArray currencyStringForKey:@"Net_Price"]);

                
                
                
                
                
                
                if ([unitPriceMO isEqualToString:currencyCheckerStr]) {
                    
                    title =checkNetPrice;
                }
                else
                {
                    
                    
                    
                    
                    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
                        
                    {
                        
                        
                        title=[NSString stringWithFormat:@"%@",unitPriceMO];
                        
                    }
                    
                    else
                    {
                        
                        
                        title=[dataItemArray valueForKey:@"Net_Price"];
                    }
                }
                
                
            }
            else
            {
                
                //if its a FOC it is displaying correctly in SO because of currency string for key method which lacks in manage orders
                
                
                NSLog(@"check net price here %@", [dataItemArray currencyStringForKey:@"Net_Price"]);
                
                
                
                //                if ([manageOrder isEqualToString:@"notconfirmed"]) {
                //
                //                    //unconfirmed manage order
                //
                //                    //check for FOC now
                //
                //
                //                    NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
                //
                //                    title=[dataItemArray currencyStringForKey:@"Price"];
                //
                //
                //
                //
                //                }
                
                
                
                
                if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                    
                    
                    
                    //check if this is sales order FOC item
                    
                    if ([[dataItemArray valueForKey:@"Price"] isEqualToString:@"0"]) {
                        //its na FOC item
                        
                        title=[dataItemArray valueForKey:@"Price"];
                        
                        
                        
                        
                    }
                    
                    else
                    {
                        
                        
                        title =checkNetPrice;
                        
                    }
                }
                else
                {
                    
                    
                    title = [dataItemArray stringForKey:@"Net_Price"];
                }
            }
            
            
            
            break;
        case 4:
            if([single.visitParentView isEqualToString:@"MO"])
            {
                
                //                   double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
                //
                //                   double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
                //
                //                   double discountPrice=[[dataItemArray valueForKey:@"Discounted_Amount"] doubleValue];
                //
                //
                //
                //                   double tempTotal= qty*usp;
                //
                //                   double totalMO=tempTotal-discountPrice;
                
                double usp;
                
                
                if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
                    
                    
                {
                    usp=[[dataItemArray valueForKey:@"Unit_Selling_Price"] doubleValue];
                    
                }
                
                else
                {
                    usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
                    
                }
                
                double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
                
                
                double discountPrice=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
                NSLog(@"check discount percent %f", discountPrice);
                
                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

                
                double tempTotal= qty*usp;
                
                discountPrice=discountPrice/100;
                
                double totalMO = tempTotal*discountPrice;
                
                
                
                if ([manageOrder isEqualToString:@"notconfirmed"]) {
                    
                    //unconfirmed manage order
                    
                    //check for FOC now
                    
                    
                    
                    
                    if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
                        
                        //this is foc
                        title=[dataItemArray valueForKey:@"Price"];
                        
                    }
                    
                    else
                    {
                        
                        
                        
                        NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
                        title=[NSString stringWithFormat:@"%0.2f",tempTotal];
                        
                        
                    }
                    
                    
                }
                
                
                else  if ([unitPriceMO isEqualToString:currencyCheckerStr]) {
                    
                    title =checkNetPrice;
                }
                else
                {
                    
                    
                    title=[NSString stringWithFormat:@"%0.2f",tempTotal];
                }
                
                
                
            }
            
            else
            {
                
                NSLog(@"check data item array in case 4 total %@", [dataItemArray description]);
                
                
                title = [dataItemArray valueForKey:@"Price"];
            }
            break;
//        case 5:
//            
//            
//            //calculate discount
//            
//            
//            if ([single.visitParentView isEqualToString:@"MO"]&& ![manageOrder isEqualToString:@"confirmed"]) {
//                
//                double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
//                
//                double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
//                
//                double discountPrice=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
//                NSLog(@"check discount percent %f", discountPrice);
//                
//                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
//
//                
//                double tempTotal= qty*usp;
//                
//                discountPrice=discountPrice/100;
//                
//                double totalMO=tempTotal*discountPrice;
//                
//                
//                NSLog(@"check total MO for discount %f", totalMO);
//                
//                NSLog(@"check discounted amount in doct %@", [dataItemArray valueForKey:@"Discounted_Price"]);
//                
//                
//                
//                
//                
//                if ([checkNetPrice isEqualToString:currencyCheckerStr]) {
//                    
//                    title =checkNetPrice;
//                }
//                else
//                {
//                    
//                    
//                    title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
//                }
//                
//                
//            }
//            else
//            {
//                
//                title = [dataItemArray currencyStringForKey:@"Discounted_Amount"];//[NSString stringWithFormat:@"AED%0.2f",titleVal];
//            }
//            break;
//        case 6:
//            
//            if ([single.visitParentView isEqualToString:@"MO"]&& ![manageOrder isEqualToString:@"confirmed"]) {
//                
//                
//                double qty=[[dataItemArray valueForKey:@"Qty"] doubleValue];
//                
//                double usp=[[dataItemArray valueForKey:@"Net_Price"] doubleValue];
//                
//                double discountPrice=[[dataItemArray valueForKey:@"DiscountPercent"] doubleValue];
//                
//                double tempTotal= qty*usp;
//                
//                discountPrice=discountPrice/100;
//                
//                double totalMO=tempTotal*(1-discountPrice);
//                NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
//
//                
//                if ([manageOrder isEqualToString:@"notconfirmed"]) {
//                    
//                    //unconfirmed manage order
//                    
//                    //check for FOC now
//                    
//                    
//                    NSLog(@" check manual FOC item price %@", [dataItemArray valueForKey:@"Price"]);
//                    
//                    
//                    
//                    
//                    if ([[dataItemArray valueForKey:@"Price"]isEqualToString:@"0"]) {
//                        
//                        title=[dataItemArray currencyStringForKey:@"Price"];
//                        
//                        
//                        
//                    }
//                    
//                    else
//                    {
//                        
//                        title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
//                        
//                    }
//                    
//                    
//                    
//                }
//                
//                else  if ([checkNetPrice isEqualToString:currencyCheckerStr]) {
//                    
//                    title =checkNetPrice;
//                }
//                else
//                {
//                    
//
//                    title=[NSString stringWithFormat:@"%@%0.2f",userCurrencyCode,totalMO];
//                }
//                
//                
//                //title=[NSString stringWithFormat:@"AED%0.2f",totalMO];
//                
//                
//                totalPriceforLbl=totalPriceforLbl+totalMO;
//                
//                //totalPriceLabel.text=[NSString stringWithFormat:@"AED%0.2f", totalMO];
//                
//            }
//            
//            else
//            {
//                title = [dataItemArray currencyStringForKey:@"Discounted_Price"];
//                
//                
//            }
            break;
        default:
            break;
    }
    

    return title;
}
#pragma mark GridView Delegate
- (void) gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex{
    
    if(![[[self.items objectAtIndex:rowIndex] valueForKey:@"Price"] isEqualToString:@"0"])
    {
        deleteRowDict=nil;
        deleteRowDict = [self.items objectAtIndex:rowIndex];
        NSString *guid = [deleteRowDict stringForKey:@"Guid"];
        if ([deleteRowDict objectForKey:@"ParentGuid"])
        {
            guid = [deleteRowDict objectForKey:@"ParentGuid"];
        }
        
        for (int i = 0;i < self.items.count; i++)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
            {
                deleteRowDict=nil;
                deleteRowDict = dataItemArray ;
                break;
            }
        }
        if (deleteRowDict)
        {
            [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
            [self presentProductOrder:deleteRowDict];
        }
    }
    else
    {
        deleteRowDict=nil;
        deleteRowDict = [self.items objectAtIndex:rowIndex-1];
        NSString *guid = [deleteRowDict stringForKey:@"Guid"];
        if ([deleteRowDict objectForKey:@"ParentGuid"])
        {
            guid = [deleteRowDict objectForKey:@"ParentGuid"];
        }
        
        for (int i = 0;i < self.items.count; i++)
        {
            dataItemArray=nil;
            dataItemArray = [self.items objectAtIndex:i];
            if ([[dataItemArray objectForKey:@"Guid"] isEqualToString:guid])
            {
                deleteRowDict = nil;
                deleteRowDict = dataItemArray ;
                break;
            }
        }
        if (deleteRowDict)
        {
            [deleteRowDict setValue:[deleteRowDict stringForKey:@"Bonus"] forKey:@"GivenBonus"];
            [self presentProductOrder:deleteRowDict];
        }
        
    }
}
- (void) gridView:(GridView *)gv commitDeleteRowAtIndex:(int)rowIndex {
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.items];
    deleteRowDict=nil;
    deleteRowDict = [array objectAtIndex:rowIndex];
    Singleton *single = [Singleton retrieveSingleton];
    if([single.visitParentView isEqualToString:@"MO"])
    {
        single.savedPerformaCurrentOrder =  [[self.items objectAtIndex:0] stringForKey:@"Orig_Sys_Document_Ref"];
    }
    
    if([single.visitParentView isEqualToString:@"MO"] && [manageOrder isEqualToString:@"confirmed"])
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Confirmed orders cannot be changed" withController:self];
    }
    else
    {
        if(![[deleteRowDict valueForKey:@"Price"] isEqualToString:@"0"])
        {
            if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"M"])
            {
                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"F"])
                {
                    //For F
                    [array removeObjectAtIndex:rowIndex+2];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+2];
                    //For M
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                    
                }
                else
                {
                    //For M
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
            }
            else if(rowIndex+1 < [array count] && [[[array objectAtIndex:rowIndex+1]stringForKey:@"priceFlag"] isEqualToString:@"F"])
            {
                
                if(rowIndex+2 < [array count] &&[[[array objectAtIndex:rowIndex+2]stringForKey:@"priceFlag"] isEqualToString:@"M"])
                {
                    // M
                    [array removeObjectAtIndex:rowIndex+2];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+2];
                    //for F
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
                else
                {
                    //For F
                    [array removeObjectAtIndex:rowIndex+1];
                    [self setItems:array];
                    [self.gridView deleteRowAtIndex:rowIndex+1];
                }
            }
            
            
            [array removeObjectAtIndex:rowIndex];
            [self setItems:array];
            [gv deleteRowAtIndex:rowIndex];
            [self updateTotal];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Bonus product can not be deleted" withController:self];
        }
    }
    [orderItemCountLabel setText:[NSString stringWithFormat:@"%lu",(unsigned long)self.items.count]];
}

#pragma saleorder delegate
- (void) dbGetSalesOrderServiceDiddbGetConfirmOrderItems:(NSArray *)performaOrder{
    for(int i = 0; i < performaOrder.count; i++)
    {
        NSDictionary *row = [performaOrder objectAtIndex:i];
        if([[row stringForKey:@"Unit_Selling_Price"] isEqualToString:@"0"])
        {
            [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
        }
        else
        {
            if (![[row stringForKey:@"Def_Bonus"] isEqualToString:@"0"])
            {
                if(![[row stringForKey:@"Def_Bonus"] isEqualToString:@""])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"GivenBonus"];
                    [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Bonus"];
                    [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Bonus"];
                }
            }
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            
            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
            
            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue]);
            
            double dicountedAmt = totalPrice - dicounttedPrice;
            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
            
            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
        }
        
    }
    [self setItems:performaOrder];
    [self.gridView reloadData];
    [self updateTotal];
    
}
- (void) dbGetSalesOrderServiceDiddbGetPerformaOrderItems:(NSArray *)performaOrder{
    // NSLog(@"Order %@",performaOrder);
    
    for(int i = 0; i < performaOrder.count; i++)
    {
        
        NSDictionary *row = [performaOrder objectAtIndex:i];
        
        if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
        {
            if(i+1 < [performaOrder count] && [[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
            {
                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"FOC_Product"];
                
                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"Bonus_Product"];
                }
            }
            else if(i+1 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+1]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
            {
                [row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid"];
                [row setValue:[performaOrder objectAtIndex:i+1] forKey:@"Bonus_Product"];
                
                if(i+2 < [performaOrder count] &&[[[performaOrder objectAtIndex:i+2]stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"])
                {
                    [row setValue:[[performaOrder objectAtIndex:i+2] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                    [row setValue:[performaOrder objectAtIndex:i+2] forKey:@"FOC_Product"];
                    //[row setValue:[[performaOrder objectAtIndex:i+1] stringForKey:@"Row_ID"] forKey:@"ChildGuid_1"];
                }
            }
            Singleton *single = [Singleton retrieveSingleton];
            single.savedPerformaCurrentOrder = [row stringForKey:@"Orig_Sys_Document_Ref"];
            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
            
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            
            int qty = [[row stringForKey:@"Ordered_Quantity"] intValue];
            double totalPrice = (double)qty * ([[row objectForKey:@"Unit_Selling_Price"] doubleValue]);
            
            [row setValue:[NSString stringWithFormat:@"%f",totalPrice] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Custom_Attribute_2"] forKey:@"lineItemNotes"];
            
            double dicounttedPrice = totalPrice * ([[row objectForKey:@"Custom_Attribute_3"] doubleValue])/(double)100;
            
            double percentDiscount = (dicounttedPrice/totalPrice )*(double)100;
            [row setValue:[NSString stringWithFormat:@"%.0f",percentDiscount] forKey:@"Discount"];
            
            double dicountedAmt = totalPrice - dicounttedPrice;
            
            [row setValue:[NSString stringWithFormat:@"%f",dicountedAmt] forKey:@"Discounted_Price"];
            [row setValue:[NSString stringWithFormat:@"%f",dicounttedPrice] forKey:@"Discounted_Amount"];
            [row setValue:[NSString stringWithFormat:@"%@",[row objectForKey:@"Custom_Attribute_3"]] forKey:@"DiscountPercent"];
        }
        
        else if([[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"M"] || [[row stringForKey:@"Calc_Price_Flag"] isEqualToString:@"F"])
        {
            if([[[performaOrder objectAtIndex:i-1] stringForKey:@"Calc_Price_Flag"] isEqualToString:@"N"])
            {
                [row setValue:[[performaOrder objectAtIndex:i-1] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            }
            else
            {
                [row setValue:[[performaOrder objectAtIndex:i-2] stringForKey:@"Row_ID"] forKey:@"ParentGuid"];
            }
            [row setValue:[row stringForKey:@"Row_ID"] forKey:@"Guid"];
            [row setValue:[row stringForKey:@"Ordered_Quantity"] forKey:@"Qty"];
            [row setValue:[row stringForKey:@"Def_Bonus"] forKey:@"Def_Qty"];
            [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Price"];
            [row setValue:[row stringForKey:@"Calc_Price_Flag"] forKey:@"priceFlag"];
        }
        
        [row setValue:[row stringForKey:@"Unit_Selling_Price"] forKey:@"Net_Price"];
    }
    [self setItems:performaOrder];
    [self.gridView reloadData];
    [self updateTotal];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)pC
{
    popoverController=nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    
    return YES;
}
- (void) viewDidUnload{
    // //NSLog(@" Sales Order NLOAD");
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: gridView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    gridView.layer.mask = maskLayer;
    
    
    
    
    
}

- (void) viewDidDisappear:(BOOL)animated{
    
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void) didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}


@end
