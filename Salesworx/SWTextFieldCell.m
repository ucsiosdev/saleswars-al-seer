//
//  SWTextFieldCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 5/3/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWTextFieldCell.h"
#import "UITableViewCell+Responder.h"
#import "SWFoundation.h"
#import "SWDefaults.h"

@implementation SWTextFieldCell

@synthesize label , secondLabel;
@synthesize textField;

#pragma mark Initialization

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:reuseIdentifier]) {
        // turn off selection rendering
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        // setup the label portion of the cell
        if (self.label!=nil) {
            self.label=nil;
        }
        [self setLabel:[[UILabel alloc] initWithFrame:CGRectZero] ];
		self.label.backgroundColor = [UIColor clearColor];
		self.label.textAlignment = NSTextAlignmentRight;
        self.label.textColor = UIColorFromRGB(0x2C394A);
        self.label.font = kFontWeblySleekSemiBold(14.0f);
        [self.contentView addSubview:self.label];
        
        // setup the textfield portion of the cell
        if (self.textField!=nil) {
            self.textField=nil;
        }
        [self setTextField:[[UITextField alloc] initWithFrame:CGRectZero] ];
        self.textField.font = kFontWeblySleekSemiBold(14.0f);
        [self.textField setDelegate:self];
        self.textField.backgroundColor = [UIColor clearColor];

        [self.textField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [self.contentView addSubview:self.textField];
        
        if (self.secondLabel) {
            self.secondLabel=nil;
        }
        [self setSecondLabel:[[UILabel alloc] initWithFrame:CGRectZero] ];
		self.secondLabel.backgroundColor = [UIColor clearColor];
		self.secondLabel.textAlignment = NSTextAlignmentLeft;
        self.secondLabel.textColor = UIColorFromRGB(0x2C394A);
        self.secondLabel.font = kFontWeblySleekSemiBold(14.0f);
        [self.contentView addSubview:self.secondLabel];
    }
    
    return self;
}



#pragma mark UITableViewCell+Responder override
- (void)resignResponder {
    if ([self.textField isFirstResponder]) {
        [self.textField resignFirstResponder];
    }
}

#pragma mark UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // get the bounding rectangle of the content view
    CGRect bounds = [self.contentView bounds];
    
    // if we have a label to render, then it gets 1/3 the width...
    if ([label.text length] > 0 && [secondLabel.text length] <= 0) {
        // label gets 1/3 the width and automatically centers itself
        label.frame = CGRectMake(0, 0, 
                                 CGRectGetWidth(bounds)/3, CGRectGetHeight(bounds));
        
        // the height of the text field is the same as the height of the font used within it
        UIFont *textFieldFont = kFontWeblySleekSemiBold(14.0f);
        CGFloat textFieldHeight = textFieldFont.ascender - textFieldFont.descender;
        
        // add 2px to support the (x) clear indicator
        textFieldHeight += 20;
        
        // text field gets remaining width
        textField.frame = CGRectMake(CGRectGetMaxX(label.frame) + 10, 
                                     (CGRectGetHeight(bounds) - textFieldHeight) / 2, 
                                     2 * CGRectGetWidth(bounds) / 3 - 20,
                                     textFieldHeight);
    }
    // otherwise, we just give the text field the entire content pane
    
    else if ([secondLabel.text length] > 0)
    {
        label.frame = CGRectMake(0, 0,CGRectGetWidth(bounds)/3, CGRectGetHeight(bounds));
        UIFont *textFieldFont = kFontWeblySleekSemiBold(14.0f);
        CGFloat textFieldHeight = textFieldFont.ascender - textFieldFont.descender;
        textFieldHeight += 20;
        textField.frame = CGRectMake(
                                     CGRectGetMaxX(label.frame) + 10,
                                     
                                     (CGRectGetHeight(bounds) - textFieldHeight) / 2,
                                     
                                     CGRectGetWidth(bounds)/3  - 20,
                                     
                                     textFieldHeight
                                     );
        
        secondLabel.frame = CGRectMake(
                                     CGRectGetMaxX(textField.frame) + 10,
                                     
                                     0,
                                     
                                     CGRectGetWidth(bounds)/3,
                                     
                                     CGRectGetHeight(bounds)
                                       
                                     );
    }
    
    else {
        [label setFrame:CGRectZero];
        [secondLabel setFrame:CGRectZero];

        [textField setFrame:CGRectInset(bounds, 10, 0)];
    }
    
    //textField.backgroundColor= [UIColor redColor];
}

#pragma mark UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([self.delegate respondsToSelector:@selector(editableCell:didUpdateValue:forKey:)]) {
        if([key isEqualToString:@"Amount"])
        {
            double amountValue;
            amountValue = [self.textField.text doubleValue];
            NSString *realNumber = self.textField.text ;
            self.textField.text= [self currencyStringWithDouble:amountValue];
            [self.delegate editableCell:self didUpdateValue:realNumber forKey:self.key];

        }
        else {
            [self.delegate editableCell:self didUpdateValue:self.textField.text forKey:self.key];

        }
    }
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //restricting to 50 characters
    if (textField.text.length >= 50 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    
    else
    {
        return YES;
    
    }
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([self.delegate respondsToSelector:@selector(editableCellDidStartUpdate:)]) {
        [self.delegate editableCellDidStartUpdate:self];
    }
}
- (NSString *)currencyStringWithDouble:(double)value {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init] ;
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setCurrencyCode:[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
    
    return [formatter stringFromNumber:[NSNumber numberWithDouble:value]];
}

@end
