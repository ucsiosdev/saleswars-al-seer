//
//  SalesWorxTimePickerViewController.m
//  SalesWars
//
//  Created by UshyakuMB2 on 26/08/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import "SalesWorxTimePickerViewController.h"
#import "SWDefaults.h"
#import "MedRepDefaults.h"

@interface SalesWorxTimePickerViewController ()

@end

@implementation SalesWorxTimePickerViewController
@synthesize salesWorxDatePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];

    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:_titleString];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1]];
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    UIBarButtonItem * saveBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Save",nil) style:UIBarButtonItemStylePlain target:self action:@selector(saveDateTapped)];
    [saveBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                           forState:UIControlStateNormal];
    saveBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=saveBtn;
    
    
    UIBarButtonItem * closeBtn=[[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Close",nil) style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    [closeBtn setTitleTextAttributes:[SWDefaults fetchBarAttributes]
                            forState:UIControlStateNormal];
    closeBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=closeBtn;
    

    self.navigationController.navigationBar.topItem.title = @"";
    
    salesWorxDatePicker.locale = [NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
    salesWorxDatePicker.datePickerMode = UIDatePickerModeTime;
    salesWorxDatePicker.date = [NSDate date];
    salesWorxDatePicker.minimumDate = [NSDate date];
}

-(void)closeButtonTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)saveDateTapped
{
    if ([self.didSelectTimeDelegate respondsToSelector:@selector(didSelectTime:)]) {
        
        NSDateFormatter *customDateFormatter = [[NSDateFormatter alloc] init];
        [customDateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [customDateFormatter setDateFormat:kDatabseDefaultDateFormat];
        customDateFormatter.locale=[NSLocale localeWithLocaleIdentifier:KEnglishLocaleStr];
        NSString *todayString = [NSString stringWithFormat:@"%@",[customDateFormatter stringFromDate:salesWorxDatePicker.date]];

        [self.didSelectTimeDelegate didSelectTime:todayString];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
