//
//  AlSeerOrderHistoryPopupViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/24/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerOrderHistoryPopupViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    BOOL soAvailablewithNoData;
    
    BOOL istableViewHidden;
}
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *saleswarsOrderNoView;
@property (weak, nonatomic) IBOutlet UILabel *navigationTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *blockView;
@property (weak, nonatomic) IBOutlet UIView *noDataView;

@property (strong, nonatomic) IBOutlet UITableView *salesOrderHistoryTblView;

@property (strong, nonatomic) IBOutlet UILabel *deliveriesLbl;

@property (strong, nonatomic) IBOutlet UILabel *invoicesLbl;

@property (strong, nonatomic) IBOutlet UILabel *tripsLbl;

@property (strong, nonatomic) IBOutlet UILabel *salesworxOrderNumberLbl;

@property (strong, nonatomic) IBOutlet UILabel *purchaseOrderNumberLbl;

@property (strong, nonatomic) IBOutlet UILabel *purchaseOrderDateLbl;

@property (strong, nonatomic) IBOutlet UILabel *salesOrderNumberLbl;

- (IBAction)closeButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *purchaseOrderView;

@property (strong, nonatomic) IBOutlet UIView *orderDateView;

@property (strong, nonatomic) IBOutlet UIView *orderNumberView;

@property (strong, nonatomic) IBOutlet UIButton *previousOrderBtn;

- (IBAction)previousOrderBtnTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *nextOrderBtn;

- (IBAction)nextOrderBtntapped:(id)sender;

@property(strong,nonatomic)NSMutableArray* orderHistoryData;

@property(strong,nonatomic)NSString* salesOrderNumber;

@property (strong, nonatomic) IBOutlet UILabel *noDataAvailableLbl;

@property(strong,nonatomic)NSMutableArray* salesOrderArray;

@property(nonatomic)NSInteger selectedIndex;

@property (strong, nonatomic) IBOutlet UIImageView *noDataAvailableImage;

@property (strong, nonatomic) IBOutlet UIView *footerLabelsView;




























@end
