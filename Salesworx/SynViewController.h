//
//  ViewController.h
//  DataSyncApp
//
//  Created by Sapna.Shah on 1/2/13.
//  Copyright (c) 2013 Sapna.Shah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestResponseManager.h"
#import "AppConstantsList.h"
#import "DataSyncManager.h"
#import "Status.h"
#import "SWPlatform.h"
#import "AFHTTPClient.h" 
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"
#import "PNCircleChart.h"
#import "MedRepElementDescriptionLabel.h"
#import "MedRepPanelTitle.h"
#import "MedRepElementTitleLabel.h"
#import "SalesWorxDropShadowView.h"


#define KSyncDic_SyncStausStr @"SyncStatus"
#define KSyncDic_SyncProgressStr @"SyncProgress"
#define KSyncDic_ShowAlertStr @"ShowAlert"
#define KSync_AlertTitleStr @"AlertTitle"
#define KSync_AlertMessageStr @"AlertMessage"
#define KSync_YesCode @"Y"
#define KSync_NoCode @"N"
#define KSync_UploadingDatabaseStatusStr @"Uploading database..."
#define KSync_DatabaseUploadedStatusStr @"Database uploaded successfully"
#define KSync_DatabaseDownloadInitiationStatusStr @"Initiating sync"
#define KSync_DownloadingDatabaseStatusStr @"Downloading database..."
#define KSync_DatabaseDownloadedStatusStr @"Database downloaded successfully"
#define KSync_UploadingFilesStatusStr @"Uploading signature files"
#define KSync_CompletingStatusStr @"Updating completion status"
#define KSync_CompleteStatusStr @"Sync completed successfully"
#define KSync_SyncStausStr @"Sync status: "
#define KSync_URLRequestFailureAlertMessageStr @"Please check server settings or Internet connection and try again."
#define KSync_NoSyncrefrencenoAlertMessageStr @"Please start the process again require Syncrefrenceno."
#define KSync_UploadingDistributionImagesStatusStr @"Uploading distribution images"

#define KSync_UploadingVisitImagesStatusStr @"Uploading Visit Images"


#define KSync_SyncFailureContactAdminAlertMessageStr @" Sync process failed. Please try again or contact your administrator for assistance "
#define KSync_ErrorAlertTitleStr @"Error"
#define KStatusSynString @"200"

#define KSync_PendingStatus @"Sync status: Pending"
#define KSync_CopyingStatus @"Sync status: Copying"
#define KSync_SynchronizingStatus @"Sync status: Synchronizing"
#define KSync_PreparingoutputStatus @"Sync status: Preparing output"
#define KSync_ReadyfordownloadStatusStr @"Sync status: Ready for download"

#define KSendOrders_StartingUploadStatus @"Starting order upload"
#define KSendOrders_TakeOrdersFirstAlertMessage @"There are no pending orders"
#define KSendOrders_OrdersSentSuccesAlertMessage @"Orders sent successfully"
#define KSendOrders_OrdersUploadSuccesAlertMessage @"Orders upload completed successfully"



@protocol SynViewControllerDelegate <NSObject>
@optional
- (void)updateActivationTextDelegate:(NSString *)synchDate;
- (void)lastSychDelegate:(NSString *)synchDate;
- (void)updateProgressDelegateWithType:(MBProgressHUDMode)type andTitle:(NSString *)title andDetail:(NSString *)detail andProgress:(float)progress ;
@end


@interface SynViewController : SWViewController <UITextFieldDelegate,UIPopoverControllerDelegate,UIAlertViewDelegate>
{
      id < SynViewControllerDelegate > delegate;
    
    
    int isFromSync,isForSyncUpload,uploadFileCount;
    int paramCount;
    //id target;
    SEL action;
    //
    BOOL isByteRecived;
    BOOL isActivated;
    BOOL isSendOrder;
    BOOL isCompleted;
    BOOL isByteSent;
    float progressBarFloatValue;

    DataSyncManager *appData;
    Status *status_;
    AlertType alertType;
    NSString *strDeviceID;
    NSMutableArray *procParamArray,*procValueArray;
    NSDictionary *syncStatusDict;
    UITextField *txtProcParams,*txtProcValues;
    UIImageView *activityImageView;
    UIImageView *activityImageView2;
    NSString *passwordActivate;
    NSString *usernameActivate;
    NSString *serverAPI;
    UIButton *startSynch;
    UIButton *startSynchOrder;
    UIButton *selectServer;
    UIProgressView *progressBar;
    UITextView *logTextView;
   // UILabel *lastSyncLable;
   // UILabel *lastSyncStatusLable;
   // UILabel *lastSyncType;
   // UILabel *lastSyncOrderSent;
   // UILabel *pendingOrder;
    //UILabel *pendingCollection;
    //UILabel *pendingReturn;
    NSString *statusSynString;
    UIPopoverController *popoverController;
    NSMutableDictionary *serverDict;
    NSString *progressDetail;
    NSString *orderCountString;
    NSString *returnCountString;
    NSString *collectionCountString;
    NSTimer *timerObj;
    NSMutableArray *resultArray;
    
    AFHTTPClient *request;    //--add new line by paracha
    AFHTTPRequestOperation *operation;
    BOOL isSuccessfull;
    UIPopoverController *locationFilterPopOver;
    
    NSInteger uploadDistributionImageFileCount;
    NSInteger uploadVisitImagesFileCount;
    
    NSMutableArray* mediaFilesToDeleteArray,*allMediaFilesArray;
    Singleton *single;
    
    // ****  New Connections Created for new design ****

    IBOutlet UIView *viewParentMain;
    IBOutlet SalesWorxDropShadowView *viewParentStatistics;
    IBOutlet MedRepPanelTitle *lblStatisticsStatic;
    IBOutlet UIView *viewOrder;
    IBOutlet MedRepElementDescriptionLabel *lblOrderStatic;
    IBOutlet UILabel *pendingOrder;
    IBOutlet MedRepElementDescriptionLabel *lblReturnsStatic;
    IBOutlet UILabel *pendingReturn;
    IBOutlet MedRepElementDescriptionLabel *lblCollectionStatic;
    IBOutlet UILabel *pendingCollection;
    IBOutlet MedRepElementDescriptionLabel *lblSyncProgressStatus;
    IBOutlet MedRepPanelTitle *lblLastSyncStatic;
    IBOutlet MedRepElementDescriptionLabel *lblAppUserName;
    IBOutlet MedRepElementTitleLabel *lblServerNameStatic;
    IBOutlet MedRepElementDescriptionLabel *lblServerName;
    IBOutlet MedRepElementTitleLabel *lblAppUserStatic;
    IBOutlet MedRepElementDescriptionLabel *lastSyncLable;
    IBOutlet MedRepElementTitleLabel *lblLastSyncOnStatic;
    IBOutlet MedRepElementDescriptionLabel *lastSyncType;
    IBOutlet MedRepElementTitleLabel *lblSyncTypeStatic;
    IBOutlet MedRepElementDescriptionLabel *lastSyncOrderSent;
    IBOutlet MedRepElementTitleLabel *lblOrderSentStatic;
    IBOutlet MedRepElementDescriptionLabel *lastSyncStatusLable;
    IBOutlet MedRepElementTitleLabel *lblSyncStatusStatic;
    IBOutlet UIImageView *fullSyncButtonImageView;
    IBOutlet UIImageView *SendOrdersButtonImageView;
    IBOutlet PNCircleChart *pieChart;
    IBOutlet SalesWorxDropShadowView *viewSyncProgress;
    IBOutlet SalesWorxDropShadowView *viewLastSyncInfo;
    IBOutlet UIView *viewReturns;
    IBOutlet UIView *viewCollections;
    
    
}

@property (unsafe_unretained) id < SynViewControllerDelegate > delegate;
@property (nonatomic, strong) AFHTTPClient *request;
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
-(void)setRequestForSyncActivateWithUserName:(NSString *)username andPassword:(NSString *)password;
-(void)setRequestForSyncUpload;
-(void)sendRequestForExecWithUserName:(NSString *)username andPassword:(NSString *)password andProcName:(NSString *)procName andProcParam:(NSString *)procPram andProcValue:(NSString *)procValue;
-(void)sendRequestForComplete;
-(void)sendRequestForUploadFile;
-(void)sendRequestForDownload;
-(void)sendRequestForInitiate;
-(void)sendRequestForStatus;
-(void)sendRequestForUploadData;
-(void)sendRequestForActivate;
-(void)finalLogPrint:(NSString *)text;
-(void)cancelHTTPRequest;

- (void)RequestFailureAlert:(NSString *)Message;
@end
