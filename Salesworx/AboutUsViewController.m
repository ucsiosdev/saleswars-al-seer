//
//  AboutUsViewController.m
//  Salesworx
//
//  Created by msaad on 6/12/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "AboutUsViewController.h"
#import "SWPlatform.h"
#import "UIDevice+IdentifierAddition.h"
#import "UIDeviceHardware.h"
#import "SSKeychain.h"
@interface AboutUsViewController ()

@end

@implementation AboutUsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
    
    UIDeviceHardware *h=[[UIDeviceHardware alloc] init];
    NSLog(@"Device %@",[h platformString]);
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    self.view.backgroundColor = UIColorFromRGB(0xE1E8ED);
}

-(void)presentCustomPreviewViewController
{
    NSLog(@"subviews are %@", [self.navigationController.navigationBar.subviews description]);
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Information"];
    self.navigationItem.hidesBackButton = YES;

    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        if (self.navigationController.viewControllers > 0 && [self.navigationController.viewControllers objectAtIndex:0] == self)
        {
            [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu_icon"] style:UIBarButtonItemStylePlain target:self.navigationController.parentViewController action:@selector(revealToggle:)]];
        }
    }
    
    NSString *customerID = [SWDefaults licenseIDForInfo];
    for (int i = 3; i < customerID.length; i++) {
        
        if (![[[SWDefaults licenseIDForInfo] substringWithRange:NSMakeRange(i, 1)] isEqual:@" "]) {
            
            NSRange range = NSMakeRange(i, 1);
             customerID = [customerID stringByReplacingCharactersInRange:range withString:@"X"];
            customerLabel.text = customerID;
            
        }
    }
    userLabel.text = [[SWDefaults userProfile] stringForKey:@"Username"];
    deviceLabel.text = [SSKeychain passwordForService:@"UCS_Salesworx" account:@"user"];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict stringForKey:@"CFBundleShortVersionString"];
    versionLabel.text = [NSString stringWithFormat:@"Version: %@",avid];
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)actionEmailComposer
{    
    if ([MFMailComposeViewController canSendMail])
    {
        [self applyComposerInterfaceApperance];
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"SalesWars"];
        
        [mailViewController.navigationBar setTitleTextAttributes:
         @{NSForegroundColorAttributeName:[UIColor whiteColor],
           NSFontAttributeName:kFontWeblySleekSemiBold(19)}];
        [mailViewController setMessageBody:@"Kindly provide additional information on SalesWars \n Regards" isHTML:NO];
        NSArray *toRecipients = [NSArray arrayWithObjects:@"info@ucssolutions.com",nil];
        [mailViewController setToRecipients:toRecipients];
        [[mailViewController navigationBar] setTintColor:[UIColor whiteColor]];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Failure" andMessage:@"Your device doesn't support the mail composer sheet. Please configure mail box in your device"  withController:nil];
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[self becomeFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)applyComposerInterfaceApperance
{
    [UINavigationBar appearance].barTintColor = self.navigationController.navigationBar.barTintColor;
    [UINavigationBar appearance].tintColor =  [UIColor whiteColor];
}

- (void)applyGlobalInterfaceAppearance
{
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

- (IBAction)ucsWebsiteButtonTapped:(id)sender {
    NSURL* ucsUrl=[NSURL URLWithString:@"http://www.ucssolutions.com"];
    if ([[UIApplication sharedApplication]canOpenURL:ucsUrl]==YES) {
        [[UIApplication sharedApplication]openURL:ucsUrl];
    }
}

@end
