//
//  AppControl.m
//  SWPlatform
//
//  Created by msaad on 5/5/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import "AppControl.h"
#import "SWDefaults.h"
@implementation AppControl
@synthesize ENABLE_ORDER_HISTORY;
@synthesize ENABLE_COLLECTION;
@synthesize ENABLE_DISTRIB_CHECK;
@synthesize ENABLE_LINE_ITEM_NOTES;
@synthesize ENABLE_MANUAL_FOC;
@synthesize ENABLE_TEMPLATES;
@synthesize ENABLE_WHOLESALE_ORDER;
@synthesize ALLOW_BONUS_CHANGE;
@synthesize ALLOW_DISCOUNT;
@synthesize ALLOW_DUAL_FOC;
@synthesize ALLOW_LOT_SELECTION;
@synthesize ALLOW_MANUAL_FOC_ITEM_CHANGE;
@synthesize ALLOW_SHIP_DATE_CHANGE;
@synthesize ALLOW_SKIP_CONSOLIDATION;
@synthesize IS_SIGNATURE_OPTIONAL;
@synthesize SHOW_ON_ORDER;
@synthesize SHOW_PRODUCT_TARGET;
@synthesize ROUTE_SYNC_DAYS;
@synthesize ENABLE_ORDER_CONSOLIDATION;
@synthesize SD_BUCKET_SIZE;
@synthesize SD_DURATION_MON;
@synthesize SHOW_SALES_HISTORY;
@synthesize ODOMETER_READING;
@synthesize IS_START_DAY,ALLOW_MULTI_CURRENCY;
@synthesize IS_LOT_OPTIONAL;
@synthesize BONUS_MODE;
@synthesize ENABLE_BONUS_GROUPS,ENABLE_PRODUCT_IMAGE,IS_SURVEY_OPTIONAL,CUST_DTL_SCREEN;
@synthesize ENABLE_DIST_CHECK_MULTI_LOCATION,ENABLE_DIST_CHECK_MULTI_LOTS,VISIT_TRANSACTION_MANDATORY,DEFAULT_AVAILABILITY_IN_DC;

- (void)initializeAppControl
{
    if([[SWDefaults appControl] count] !=0)
    {
        
        NSLog(@"app control flags are %@", [appControlDict description]);
        
        
        appControlDict = [NSMutableDictionary dictionaryWithDictionary:[[SWDefaults appControl] objectAtIndex:0]];
        self.ENABLE_ORDER_HISTORY               = [appControlDict valueForKey:@"ENABLE_ORDER_HISTORY"];
        self.ENABLE_TEMPLATES                   = [appControlDict valueForKey:@"ENABLE_TEMPLATES"];
        self.ENABLE_COLLECTION                  = [appControlDict valueForKey:@"ENABLE_COLLECTIONS"];
        self.ENABLE_DISTRIB_CHECK               = [appControlDict valueForKey:@"ENABLE_DISTRIB_CHECK"];
        self.ALLOW_LOT_SELECTION                = [appControlDict valueForKey:@"ALLOW_LOT_SELECTION"];
        self.ENABLE_WHOLESALE_ORDER             = [appControlDict valueForKey:@"ENABLE_WHOLESALE_ORDER"];
        self.ENABLE_ORDER_CONSOLIDATION         = [appControlDict valueForKey:@"ENABLE_ORDER_CONSOLIDATION"];
        self.ALLOW_SKIP_CONSOLIDATION           = [appControlDict valueForKey:@"ALLOW_SKIP_CONSOLIDATION"];
        self.ALLOW_SHIP_DATE_CHANGE             = [appControlDict valueForKey:@"ALLOW_SHIP_DATE_CHANGE"];
        self.ALLOW_BONUS_CHANGE                 = [appControlDict valueForKey:@"ALLOW_BONUS_CHANGE"];
        self.ENABLE_MANUAL_FOC                  = [appControlDict valueForKey:@"ENABLE_MANUAL_FOC"];
        self.ALLOW_MANUAL_FOC_ITEM_CHANGE       = [appControlDict valueForKey:@"ALLOW_MANUAL_FOC_ITEM_CHANGE"];
        self.ALLOW_DISCOUNT                     = [appControlDict valueForKey:@"ALLOW_DISCOUNT"];
        self.ALLOW_DUAL_FOC                     = [appControlDict valueForKey:@"ALLOW_DUAL_FOC"];
        self.IS_SIGNATURE_OPTIONAL              = [appControlDict valueForKey:@"IS_SIGNATURE_OPTIONAL"];
        self.ENABLE_LINE_ITEM_NOTES             = [appControlDict valueForKey:@"ENABLE_LINE_ITEM_NOTES"];
        self.SHOW_ON_ORDER                      = [appControlDict valueForKey:@"SHOW_ON_ORDER_QTY"];
        self.SHOW_PRODUCT_TARGET                = [appControlDict valueForKey:@"SHOW_PRODUCT_TARGET"];
        self.ROUTE_SYNC_DAYS                    = [appControlDict valueForKey:@"ROUTE_SYNC_DAYS"];
        self.SD_BUCKET_SIZE                     = [appControlDict valueForKey:@"SD_BUCKET_SIZE"];
        self.SD_DURATION_MON                    = [appControlDict valueForKey:@"SD_DURATION_MON"];
        self.SHOW_SALES_HISTORY                 = [appControlDict valueForKey:@"SHOW_SALES_DATA"];
        self.ODOMETER_READING                   = [appControlDict valueForKey:@"ODOMETER_READING"];
        self.IS_START_DAY                       = [appControlDict valueForKey:@"IS_START_DAY"];
        self.ALLOW_MULTI_CURRENCY               = [appControlDict valueForKey:@"ALLOW_MULTI_CURRENCY"];
        self.IS_LOT_OPTIONAL                    = [appControlDict valueForKey:@"IS_LOT_OPTIONAL"];
        self.BONUS_MODE                         = [appControlDict valueForKey:@"BONUS_MODE"];
        self.ENABLE_BONUS_GROUPS                = [appControlDict valueForKey:@"ENABLE_BONUS_GROUPS"];
        self.ENABLE_PRODUCT_IMAGE               = [appControlDict valueForKey:@"ENABLE_PRODUCT_IMAGE"];
        self.CUST_DTL_SCREEN                    = [appControlDict valueForKey:@"CUST_DTL_SCREEN"];
        self.DASHBOARD_TYPE                    =  [appControlDict valueForKey:@"DASHBOARD_TYPE"];

        self.IS_SURVEY_OPTIONAL                 = @"Y";
        self.ENABLE_DIST_CHECK_MULTI_LOCATION   = [appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOCATION"];

        self.ENABLE_DIST_CHECK_MULTI_LOTS=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOTS"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOTS"]];

        //no multi lots for al seer
      // self.ENABLE_DIST_CHECK_MULTI_LOTS=@"N";//?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_DIST_CHECK_MULTI_LOTS"]];
        self.FS_SHOW_DC_AVBL_SEG = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_AVBL_SEG"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_AVBL_SEG"]];
        
        self.MANDATORY_QTY_IN_DC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_QTY_IN_DC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_QTY_IN_DC"]];
        
        self.MANDATORY_EXP_DT_IN_DC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_EXP_DT_IN_DC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"MANDATORY_EXP_DT_IN_DC"]];
        
        self.FS_SHOW_DC_FILTER=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_FILTER"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_FILTER"]];
        
        self.FS_SHOW_DC_MIN_STOCK=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_MIN_STOCK"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_MIN_STOCK"]];
        
        self.FS_SHOW_DC_RO = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_RO"]] isEqualToString:@""]?@"Y":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FS_SHOW_DC_RO"]];
        
        self.VISIT_TRANSACTION_MANDATORY=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"VISIT_TRANSACTION_MANDATORY"]];
        
        self.DEFAULT_AVAILABILITY_IN_DC=[SWDefaults getValidStringValue:[appControlDict valueForKey:@"DEFAULT_AVAILABILITY_IN_DC"]];
        
        self.FSR_LOC_TRK_FREQ = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_LOC_TRK_FREQ"]] isEqualToString:@""]?@"0":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"FSR_LOC_TRK_FREQ"]];
        
        self.LOC_TRK_START_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"LOC_TRK_START_TIME"]] isEqualToString:@""]?@"00:00":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"LOC_TRK_START_TIME"]];
        
        self.LOC_TRK_END_TIME = [[SWDefaults getValidStringValue:[appControlDict valueForKey:@"LOC_TRK_END_TIME"]] isEqualToString:@""]?@"23:59":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"LOC_TRK_END_TIME"]];
        
        self.ENABLE_BACKGROUND_SYNC=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BACKGROUND_SYNC"]] isEqualToString:@""]?KAppControlsNOCode:[SWDefaults getValidStringValue:[appControlDict valueForKey:@"ENABLE_BACKGROUND_SYNC"]];

        self.V_TRX_SYNC_INTERVAL=[[SWDefaults getValidStringValue:[appControlDict valueForKey:@"V_TRX_SYNC_INTERVAL"]] isEqualToString:@""]?@"5":[SWDefaults getValidStringValue:[appControlDict valueForKey:@"V_TRX_SYNC_INTERVAL"]];
        
        
        if ([NSString isEmpty:self.VISIT_TRANSACTION_MANDATORY]) {
            
            self.VISIT_TRANSACTION_MANDATORY=@"N";
        }
        
        if (self.ENABLE_ORDER_HISTORY.length==0)
        {
            self.ENABLE_ORDER_HISTORY                = @"Y";
        }
        if (self.ENABLE_TEMPLATES.length==0)
        {
            self.ENABLE_TEMPLATES                = @"N";
        }
        if (self.ENABLE_COLLECTION.length==0)
        {
            self.ENABLE_COLLECTION                = @"Y";
        }
        if (self.ENABLE_DISTRIB_CHECK.length==0)
        {
            self.ENABLE_DISTRIB_CHECK                = @"Y";
        }
        if (self.ALLOW_LOT_SELECTION.length==0)
        {
            self.ALLOW_LOT_SELECTION                = @"Y";
        }
        if (self.ENABLE_WHOLESALE_ORDER.length==0)
        {
            self.ENABLE_WHOLESALE_ORDER                = @"N";
        }
        if (self.ENABLE_ORDER_CONSOLIDATION.length==0)
        {
            self.ENABLE_ORDER_CONSOLIDATION                = @"N";
        }
        if (self.ALLOW_SKIP_CONSOLIDATION.length==0)
        {
            self.ALLOW_SKIP_CONSOLIDATION                = @"N";
        }
        if (self.ALLOW_SHIP_DATE_CHANGE.length==0)
        {
            self.ALLOW_SHIP_DATE_CHANGE                = @"Y";
        }
        if (self.ALLOW_BONUS_CHANGE.length==0)
        {
            self.ALLOW_BONUS_CHANGE                = @"Y";
        }
        if (self.ENABLE_MANUAL_FOC.length==0)
        {
            self.ENABLE_MANUAL_FOC                = @"Y";
        }
        if (self.ALLOW_MANUAL_FOC_ITEM_CHANGE.length==0)
        {
            self.ALLOW_MANUAL_FOC_ITEM_CHANGE                = @"Y";
        }
        if (self.ALLOW_DISCOUNT.length==0)
        {
            self.ALLOW_DISCOUNT                = @"Y";
        }
        if (self.ALLOW_DUAL_FOC.length==0)
        {
            self.ALLOW_DUAL_FOC                = @"N";
        }
        if (self.IS_SIGNATURE_OPTIONAL.length==0)
        {
            self.IS_SIGNATURE_OPTIONAL                = @"Y";
        }
        if (self.ENABLE_LINE_ITEM_NOTES.length==0)
        {
            self.ENABLE_LINE_ITEM_NOTES                = @"N";
        }
        if (self.SHOW_ON_ORDER.length==0)
        {
            self.SHOW_ON_ORDER                = @"N";
        }
        if (self.SHOW_PRODUCT_TARGET.length==0)
        {
            self.SHOW_PRODUCT_TARGET                = @"Y";
        }
        if (self.ROUTE_SYNC_DAYS.length==0)
        {
            self.ROUTE_SYNC_DAYS                = @"1";
        }
        if (self.SD_BUCKET_SIZE.length==0)
        {
            self.SD_BUCKET_SIZE                = @"1";
        }
        if (self.SD_DURATION_MON.length==0)
        {
            self.SD_DURATION_MON                = @"6";
        }
        if (self.SHOW_SALES_HISTORY.length==0)
        {
            self.SHOW_SALES_HISTORY                = @"N";
        }
        if (self.ODOMETER_READING.length==0)
        {
            self.ODOMETER_READING                = @"N";
        }
        if (self.IS_START_DAY.length==0)
        {
            self.IS_START_DAY                = @"N";
        }
        if (self.ALLOW_MULTI_CURRENCY.length==0)
        {
            self.ALLOW_MULTI_CURRENCY                = @"N";
        }
        if (self.IS_LOT_OPTIONAL.length==0)
        {
            self.IS_LOT_OPTIONAL                = @"Y";
        }
        if (self.BONUS_MODE.length==0)
        {
            self.BONUS_MODE                = @"DEFAULT";
        }
        if (self.ENABLE_BONUS_GROUPS.length==0)
        {
            self.ENABLE_BONUS_GROUPS                = @"N";
        }
        if (self.ENABLE_PRODUCT_IMAGE.length==0)
        {
            self.ENABLE_PRODUCT_IMAGE                = @"N";
        }
        if (self.IS_SURVEY_OPTIONAL.length==0)
        {
            self.IS_SURVEY_OPTIONAL                = @"Y";
        }
        if (self.ENABLE_DIST_CHECK_MULTI_LOCATION.length==0) {
            
            self.ENABLE_DIST_CHECK_MULTI_LOCATION = @"N";
        }
        
        
    }
    
    else
    {
        self.ENABLE_ORDER_HISTORY               = @"Y";
        self.ENABLE_TEMPLATES                   = @"N";
        self.ENABLE_COLLECTION                  = @"Y";
        self.ENABLE_DISTRIB_CHECK               = @"Y";
        self.ALLOW_LOT_SELECTION                = @"Y";
        self.ENABLE_WHOLESALE_ORDER             = @"N";
        self.ENABLE_ORDER_CONSOLIDATION         = @"N";
        self.ALLOW_SKIP_CONSOLIDATION           = @"Y";
        self.ALLOW_SHIP_DATE_CHANGE             = @"Y";
        self.ALLOW_BONUS_CHANGE                 = @"Y";
        self.ENABLE_MANUAL_FOC                  = @"Y";
        self.ALLOW_MANUAL_FOC_ITEM_CHANGE       = @"Y";
        self.ALLOW_DISCOUNT                     = @"Y";
        self.ALLOW_DUAL_FOC                     = @"N";
        self.IS_SIGNATURE_OPTIONAL              = @"Y";
        self.ENABLE_LINE_ITEM_NOTES             = @"N";
        self.SHOW_ON_ORDER                      = @"N";
        self.SHOW_PRODUCT_TARGET                = @"Y";
        self.ROUTE_SYNC_DAYS                    = @"1";
        self.SD_BUCKET_SIZE                     = @"1";
        self.SD_DURATION_MON                    = @"6";
        self.SHOW_SALES_HISTORY                 = @"N";
        self.ODOMETER_READING                   = @"N";
        self.IS_START_DAY                       = @"N";
        self.ALLOW_MULTI_CURRENCY               = @"N";
        self.IS_LOT_OPTIONAL                    = @"Y";
        self.BONUS_MODE                         = @"DEFAULT";
        self.ENABLE_BONUS_GROUPS                = @"N";
        self.ENABLE_PRODUCT_IMAGE               = @"N";
        self.IS_SURVEY_OPTIONAL                 = @"Y";
        self.ENABLE_DIST_CHECK_MULTI_LOCATION   = @"N";

    }
    
}



static AppControl *sharedSingleton = nil;

+ (AppControl*) retrieveSingleton {
	@synchronized(self)
    {
		if (sharedSingleton == nil) {
			sharedSingleton = [[AppControl alloc] init];
            [sharedSingleton initializeAppControl];
		}
	}
	return sharedSingleton;
}

+ (void) destroyMySingleton
{
    sharedSingleton=nil;
}

+ (id) allocWithZone:(NSZone *) zone
{
    //TODO: Replace with newer version of alloc. Memory zones are now not used.
    
	@synchronized(self) {
		if (sharedSingleton == nil) {
			sharedSingleton = [super allocWithZone:zone];
			return sharedSingleton;
		}
	}
	return nil;
}




@end
