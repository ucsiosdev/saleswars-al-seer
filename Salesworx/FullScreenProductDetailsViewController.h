//
//  FullScreenProductDetailsViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/14/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FullScreenProductDetailsViewController : UIViewController<UIPageViewControllerDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIPageControl *pageController;
@property (strong, nonatomic) IBOutlet UIScrollView *productImagesScrollView;

@property(strong,nonatomic)NSMutableArray* imagesArray;
@end
