//
//  CustomerHeaderView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 6/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerHeaderView.h"
#import "SWFoundation.h"
#import "SWDefaults.h"

@implementation CustomerHeaderView

@synthesize customer;
@synthesize companyNameLabel;
@synthesize codeLabel;
@synthesize shouldHaveMargins;
@synthesize statusButton;
@synthesize availableBalanceLabel;
@synthesize availableBalanceValue;
@synthesize availableBalanceImage;

- (id)initWithFrame:(CGRect)frame andCustomer:(NSDictionary *)c {
    self = [super initWithFrame:frame];
    if (self) {
        [self setCustomer:c];
        [self setBackgroundColor:[UIColor whiteColor]];
        self.companyNameLabel=nil;
        [self setCompanyNameLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        //[self.companyNameLabel setBackgroundColor:[UIColor blueColor]];
        [companyNameLabel setText:[SWDefaults getValidStringValue:[self.customer objectForKey:@"Customer_Name"]]];
        [companyNameLabel setBackgroundColor:[UIColor clearColor]];
        [companyNameLabel setTextColor:[UIColor colorWithRed:44.0/255.0 green:57.0/255.0 blue:74.0/255.0 alpha:1.0]];
        [companyNameLabel setShadowColor:[UIColor whiteColor]];
        [companyNameLabel setShadowOffset:CGSizeMake(0, 1)];
        [companyNameLabel setFont:LabelFieldSemiFontOfSiz(16.0f)];
        
        self.codeLabel=nil;
        [self setCodeLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [codeLabel setText:[SWDefaults getValidStringValue:[self.customer objectForKey:@"Customer_No"]]];
        [codeLabel setBackgroundColor:[UIColor clearColor]];
        [codeLabel setTextColor:[UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0]];
        [codeLabel setShadowColor:[UIColor whiteColor]];
        [codeLabel setShadowOffset:CGSizeMake(0, 1)];
        [codeLabel setFont:LabelFieldSemiFontOfSiz(14.0f)];
        
        self.availableBalanceLabel=nil;
        [self setAvailableBalanceLabel:[[UILabel alloc] initWithFrame:CGRectZero]];
        [availableBalanceLabel setText:@"Available Balance"];
        [availableBalanceLabel setBackgroundColor:[UIColor clearColor]];
        [availableBalanceLabel setTextColor:[UIColor colorWithRed:106.0/255.0 green:111.0/255.0 blue:123.0/255.0 alpha:1.0]];
        [availableBalanceLabel setShadowColor:[UIColor whiteColor]];
        [availableBalanceLabel setShadowOffset:CGSizeMake(0, 1)];
        [availableBalanceLabel setFont:LabelFieldSemiFontOfSiz(14.0f)];
        
        self.availableBalanceValue=nil;
        [self setAvailableBalanceValue:[[UILabel alloc] initWithFrame:CGRectZero]];
        //[self.companyNameLabel setBackgroundColor:[UIColor blueColor]];
        double availBal = [[self.customer stringForKey:@"Avail_Bal"] doubleValue];
        [availableBalanceValue setText:[[NSString stringWithFormat:@"%f",availBal] currencyString]];
        [availableBalanceValue setBackgroundColor:[UIColor clearColor]];
        [availableBalanceValue setTextColor:[UIColor colorWithRed:44.0/255.0 green:57.0/255.0 blue:74.0/255.0 alpha:1.0]];
        [availableBalanceValue setShadowColor:[UIColor whiteColor]];
        [availableBalanceValue setShadowOffset:CGSizeMake(0, 1)];
        [availableBalanceValue setFont:LabelFieldSemiFontOfSiz(16.0f)];
        
        self.availableBalanceImage=nil;
        [self setAvailableBalanceImage:[[UIImageView alloc] initWithFrame:CGRectZero]];
        //[self.companyNameLabel setBackgroundColor:[UIColor blueColor]];
        [availableBalanceImage setBackgroundColor:[UIColor clearColor]];
        [availableBalanceImage setImage:[UIImage imageNamed:@"Available_Balance"]];
        
        [self setStatusButton:[UIButton buttonWithType:UIButtonTypeCustom]];
        
        BOOL customerStatus = [[SWDefaults getValidStringValue:[c objectForKey:@"Cust_Status"]] isEqualToString:@"Y"];
        BOOL cashCustomer = [[SWDefaults getValidStringValue:[c objectForKey:@"Cash_Cust"]] isEqualToString:@"Y"];
        BOOL creditHold = [[SWDefaults getValidStringValue:[c objectForKey:@"Credit_Hold"]] isEqualToString:@"Y"];
        
        NSString *imageName = @"transparent.png";
        
        if (cashCustomer) {
            imageName = @"yellow.png";
        }
        
        if (!customerStatus || creditHold) {
            imageName = @"red.png";
        }

        [self.statusButton setImage:[UIImage imageNamed:imageName cache:NO] forState:UIControlStateNormal];
        
        [self addSubview:self.statusButton];
        [self addSubview:self.codeLabel];
        [self addSubview:self.companyNameLabel];
        [self addSubview:self.availableBalanceLabel];
        [self addSubview:self.availableBalanceValue];
        [self addSubview:self.availableBalanceImage];


        
        [self setShouldHaveMargins:NO];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect b = self.bounds;
    int margin = 0;
    
    if (shouldHaveMargins) {
        margin = 21;
    }
    
    [self.companyNameLabel setFrame:CGRectMake(margin, 10, b.size.width - (margin * 2), 30)];
    [self.codeLabel setFrame:CGRectMake(margin, 40, b.size.width - (margin * 2), 16)];
    [self.availableBalanceLabel setFrame:CGRectMake(865,36,135,22)];
    [self.availableBalanceValue setFrame:CGRectMake(865,8,135,24)];
    [self.availableBalanceImage setFrame:CGRectMake(820,14.5,37,37)];
    
    CGSize maximumLabelSize = CGSizeMake(self.companyNameLabel.bounds.size.width, 9999);
    
    CGSize expectedLabelSize = [self.companyNameLabel.text sizeWithFont:self.companyNameLabel.font 
                                       constrainedToSize:maximumLabelSize
                                           lineBreakMode:NSLineBreakByWordWrapping];
    
    [self.statusButton setFrame:CGRectMake(expectedLabelSize.width + 65, 18, 13, 13)];
}


@end
