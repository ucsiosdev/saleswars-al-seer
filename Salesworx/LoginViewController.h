//
//  LoginViewController.h
//  Salesworx
//
//  Created by Saad Ansari on 12/8/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"
#import "FTPBaseViewController.h"
@interface LoginViewController : FTPBaseViewController <UIAlertViewDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    //IBOutlet UITextField *txtUserName;
    //IBOutlet UITextField *txtPassword;
    IBOutlet UILabel *lblLogin;
    IBOutlet UILabel *lblVersion;
    IBOutlet UILabel *lblPowered;
    
    NSString *login;
    NSString *password;
    SWLoadingView *loadingView;
    NSString *ClientVersion;
    IBOutlet UIView *loginView;
     IBOutlet NSLayoutConstraint *appLogoHorizontalConstraint;
    IBOutlet NSLayoutConstraint *appLogoHightConstraint;
    IBOutlet NSLayoutConstraint *appLogoWidthConstraint;
    IBOutlet UIImageView *appImageView;
    IBOutlet NSLayoutConstraint *loginViewHorizontalConstraint;
    AppControl* appControl;
}
-(IBAction)buttonAction:(id)sender;
@property(strong,nonatomic) IBOutlet UITextField*txtUserName;
@property(strong,nonatomic) IBOutlet UITextField*txtPassword;
@property(strong,nonatomic)    IBOutlet UIButton *btnLogin;

@end
