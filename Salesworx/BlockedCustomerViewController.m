//
//  BlockedCustomerViewController.m
//  Salesworx
//
//  Created by msaad on 6/13/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "BlockedCustomerViewController.h"
#import "BlockedCustomerTableViewCell.h"
@interface BlockedCustomerViewController ()

@end

@implementation BlockedCustomerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.titleView=[SWDefaults fetchTitleView:kBlockedCustomersTitle];
    
    NSString *sql = @"Select Customer_ID , Customer_Name , Contact , Phone from TBL_Customer where Cust_Status='N' OR Credit_Hold = 'Y'";
    blockedCustomerArray = [[SWDatabaseManager retrieveManager] dbGetDataForReport:sql];
    
    blockedCustomerTableView.layer.cornerRadius = 8.0;
    blockedCustomerTableView.layer.masksToBounds = YES;

    if (@available(iOS 15.0, *)) {
        blockedCustomerTableView.sectionHeaderTopPadding = 0;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"Block Documents");
}
- (void)currencyTypeChanged:(NSDictionary *)customer {
    
    [currencyTypePopOver dismissPopoverAnimated:YES];
    currencyTypeViewController = nil;
    currencyTypePopOver = nil;
    
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return blockedCustomerArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"blockedCustomerCell";
    BlockedCustomerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"BlockedCustomerTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.customerCodeLabel.text=@"Code";
    cell.customerNameLabel.text=@"Name";
    cell.contactNumerLabel.text=@"Contact";
    cell.phoneNumberLabel.text=@"Phone";
    
    
    cell.customerCodeLabel.textColor = TableViewHeaderSectionColor;
    cell.customerNameLabel.textColor = TableViewHeaderSectionColor;
    cell.contactNumerLabel.textColor = TableViewHeaderSectionColor;
    cell.phoneNumberLabel.textColor = TableViewHeaderSectionColor;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"blockedCustomerCell";
    BlockedCustomerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"BlockedCustomerTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *data = [blockedCustomerArray objectAtIndex:indexPath.row];
    
    cell.customerCodeLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_ID"]]];
    cell.contactNumerLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Contact"]]];
    cell.customerNameLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Customer_Name"]]];
    cell.phoneNumberLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Phone"]]];
    
    return cell;
}
- (void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
