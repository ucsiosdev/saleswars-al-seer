//
//  SalesSummaryReportFilterViewController.m
//  SalesWars
//
//  Created by Prasann on 30/11/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import "SalesSummaryReportFilterViewController.h"
#import "CustomersListViewController.h"
#import "SWDefaults.h"
#import "NSString+Additions.h"
#import "NSPredicate+Distinct.h"
#import "AlSeerPopupViewController.h"
#import "SalesWorxDatePickerPopOverViewController.h"
#import "MedRepDefaults.h"
@interface SalesSummaryReportFilterViewController ()

@end

@implementation SalesSummaryReportFilterViewController
@synthesize customerNameTextField,customerTypeTextField,fromDateTextField,toDateTextField,documentTypeTextFiled,previousFilterParametersDict,datePickerViewControllerDate, selectedTextFieldType,customerDictionary;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.titleView = [SWDefaults createNavigationBarTitleView:NSLocalizedString(@"Filter", nil)];
    
    UINavigationBar *bar = [self.navigationController navigationBar];
    bar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
    bar.tintColor = [UIColor whiteColor];
    bar.translucent = NO;
    
    UIBarButtonItem * clearButton=[[UIBarButtonItem alloc]initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:@selector(clearButtontapped)];
    self.navigationItem.leftBarButtonItem=clearButton;
    
    UIBarButtonItem * closeButton=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonTapped)];
    self.navigationItem.rightBarButtonItem=closeButton;
    
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerDictionary = [[NSMutableDictionary alloc]init];
    if (previousFilterParametersDict.count>0) {
        
        filterParametersDict = previousFilterParametersDict;
    }
    else
    {
        previousFilterParametersDict=[[NSMutableDictionary alloc]init];
        
    }
    
    NSString *selectedCustomerName = [SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kCustomerNameTextField]];
    NSString *selectedCustomerType = [SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kCustomerTypeTextField]];
    NSString *selectedTODate= [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kToDateTextField]]];
    NSString *selectedFromDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormatWithoutTime destFormat:kDateFormatWithoutTime scrString:[SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kFromDateTextField]]];
    NSString *selectedDocumentType = [SWDefaults getValidStringValue:[previousFilterParametersDict valueForKey:kDocumentTypeTextField]];
   
    
    
    if ([NSString isEmpty:selectedCustomerName] == NO) {
        customerNameTextField.text=[NSString stringWithFormat:@"%@",selectedCustomerName];
    }
    
    if ([NSString isEmpty:selectedCustomerType] == NO) {
        customerTypeTextField.text  = [NSString stringWithFormat:@"%@",selectedCustomerType];
    }
    
    if ([NSString isEmpty:selectedFromDate] == NO) {
        fromDateTextField.text = [NSString stringWithFormat:@"%@",selectedFromDate];
        
    }
    
    if ([NSString isEmpty:selectedTODate] == NO) {
        toDateTextField.text = [NSString stringWithFormat:@"%@",selectedTODate];
    }
    
    if ([NSString isEmpty:selectedDocumentType] == NO) {
        documentTypeTextFiled.text = [NSString stringWithFormat:@"%@",selectedDocumentType];
    }
    
    DocType = @"All";
    custType = @"All";

    customerTypeArray =  [NSArray arrayWithObjects:@"All", @"Cash",@"Credit" , nil];
    documentTypeArray =  [NSArray arrayWithObjects:@"All", @"Invoice",@"Credit Note" , nil];
    selectedTextFieldType = [[NSString alloc]init];
}



-(void)clearButtontapped
{
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerNameTextField.text = @"";
    customerTypeTextField.text = @"";
    toDateTextField.text = @"";
    fromDateTextField.text = @"";
    documentTypeTextFiled.text = @"";
    
}
-(void)closeButtonTapped
{
    [self.filterPopOverController dismissPopoverAnimated:YES];
    
}

-(void)selectedContent:(id)selectedObject
{
    if ([selectedTextField isEqualToString:kCustomerTypeTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:@"Customer_Type"];
        customerTypeTextField.text = [SWDefaults getValidStringValue:selectedObject];
    }
    else if ([selectedTextField isEqualToString:kDocumentTypeTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:@"Document_Type"];
        documentTypeTextFiled.text=[SWDefaults getValidStringValue:selectedObject];
    }
    else if ([selectedTextField isEqualToString:kCustomerNameTextField])
    {
        [filterParametersDict setValue:selectedObject forKey:@"Customer_Name"];
        customerNameTextField.text=[SWDefaults getValidStringValue:selectedObject];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    if (textField == customerNameTextField) {
        
//        selectedTextField = kCustomerNameTextField;
//        selectedPredicateString = kCustomerNameTextField;
//        titleText = kCustomerNameTitle;
//        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
//        return NO;
        
        selectedTextField = kCustomerNameTextField;
        selectedPredicateString = kCustomerNameTextField;
        titleText = kCustomerNameTitle;
        currencyTypeViewController=nil;
        currencyTypeViewController = [[CustomersListViewController alloc] init] ;
        currencyTypeViewController.isFromReport = YES;
        [currencyTypeViewController setTarget:self];
        [currencyTypeViewController setAction:@selector(currencyTypeChanged:)];
        [currencyTypeViewController setPreferredContentSize:CGSizeMake(400,600)];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:currencyTypeViewController] ;
        navigationController.navigationBar.barStyle = UIBarStyleBlack;
        navigationController.navigationBar.translucent = NO;
        currencyTypePopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
        currencyTypePopOver.delegate=self;
        [currencyTypePopOver presentPopoverFromRect:customerNameTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];

        return NO;
    }
    else if (textField == fromDateTextField){
        selectedTextFieldType = kFromDateTextField;
        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = NSLocalizedString(@"Date", nil);
        popOverVC.datePickerMode=kTodoTitle;
        popOverVC.setMinimumDateCurrentDate=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController = datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:fromDateTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        return NO;
        
    }else if (textField == toDateTextField){
        selectedTextFieldType = kToDateTextField;

        SalesWorxDatePickerPopOverViewController * popOverVC=[[SalesWorxDatePickerPopOverViewController alloc]init];
        popOverVC.didSelectDateDelegate=self;
        popOverVC.titleString = NSLocalizedString(@"Date", nil);
        popOverVC.datePickerMode=kTodoTitle;
        popOverVC.setMinimumDateCurrentDate=YES;
        
        UINavigationController * popOverNavigationCroller=[[UINavigationController alloc]initWithRootViewController:popOverVC];
        datePickerPopOverController=nil;
        datePickerPopOverController=[[UIPopoverController alloc]initWithContentViewController:popOverNavigationCroller];
        datePickerPopOverController.delegate=self;
        popOverVC.datePickerPopOverController = datePickerPopOverController;
        
        [datePickerPopOverController setPopoverContentSize:CGSizeMake(366, 273) animated:YES];
        [datePickerPopOverController presentPopoverFromRect:toDateTextField.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
        return NO;
        
    }else if(textField == customerTypeTextField){
        selectedTextField = kCustomerTypeTextField;
        selectedPredicateString = kCustomerTypeTextField;
        titleText = kCustomerTypeTitle;
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }else if(textField == documentTypeTextFiled){
        selectedTextField = kDocumentTypeTextField;
        selectedPredicateString = kDocumentTypeTextField;
        titleText = kDocumentTypeTitle;
        [self textfieldDidTap:textField withTitle:selectedTextField withFilterPredicate:selectedPredicateString];
        return NO;
    }
    
    return NO;
}

- (void)currencyTypeChanged:(NSDictionary *)customer {
    [currencyTypePopOver dismissPopoverAnimated:YES];
    customerDictionary = [NSMutableDictionary dictionaryWithDictionary:customer];
    [filterParametersDict setValue:[customerDictionary stringForKey:@"Customer_Name"] forKey:@"Customer_Name"];
    customerNameTextField.text = [SWDefaults getValidStringValue:[customerDictionary stringForKey:@"Customer_Name"]];
}

-(void)didSelectDate:(NSString *)selectedDate
{
    if ([selectedTextFieldType isEqualToString:kFromDateTextField] ) {
        [fromDateTextField setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];
        
        NSString *fromDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [filterParametersDict setValue:fromDate forKey:@"From_Date"];
    }else{
        [toDateTextField setText:[MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDateFormatWithoutTime scrString:selectedDate]];
        
        NSString *toDate = [MedRepDefaults refineDateFormat:kDatabseDefaultDateFormat destFormat:kDatabseDefaultDateFormatWithoutTime scrString:selectedDate];
        [filterParametersDict setValue:toDate forKey:@"To_Date"];
    }
}
-(void)textfieldDidTap:(UITextField*)tappedTextField withTitle:(NSString*)title withFilterPredicate:(NSString*)predicateString
{
    AlSeerPopupViewController *filterDescVC=[[AlSeerPopupViewController alloc]init];
    filterDescVC.selectedFilterDelegate = self;
    filterDescVC.descTitle = titleText;
    filterDescVC.filterNavController = self.filterNavController;
    filterDescVC.filterPopOverController = self.filterPopOverController;
    
    NSMutableArray *filterDescArray = [[NSMutableArray alloc]init];
    NSMutableArray*unfilteredArray=[[NSMutableArray alloc]init];

    if(tappedTextField == documentTypeTextFiled){
        filterDescArray = [documentTypeArray mutableCopy];
    }else if (tappedTextField == customerTypeTextField){
        filterDescArray = [customerTypeArray mutableCopy];
    }
    else if (tappedTextField == customerNameTextField){
        NSPredicate * refinedPred=[NSPredicate predicateForDistinctWithProperty:predicateString];
        unfilteredArray = [[_salesSummaryArray filteredArrayUsingPredicate:refinedPred] mutableCopy];
        
        //now sort by asc
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:predicateString
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        
        
        
        NSArray *sortedArray = [unfilteredArray sortedArrayUsingDescriptors:sortDescriptors];
        filterDescArray=[sortedArray valueForKey:[SWDefaults getValidStringValue:predicateString]];
    }
    
    
    if (filterDescArray.count>0) {
        filterDescVC.filterDescArray=filterDescArray;
        [self.navigationController pushViewController:filterDescVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Data" andMessage:@"Please try again later" withController:self];
    }
}

- (IBAction)resetButtonTapped:(id)sender {
    filterParametersDict=[[NSMutableDictionary alloc]init];
    customerNameTextField.text = @"";
    customerTypeTextField.text = @"";
    fromDateTextField.text = @"";
    toDateTextField.text = @"";
    documentTypeTextFiled.text = @"";
    
    if ([self.delegate respondsToSelector:@selector(salesSummaryFilterDidReset)]) {
        [self.delegate salesSummaryFilterDidReset];
    }
    [self.filterPopOverController dismissPopoverAnimated:YES];
}
- (IBAction)searchButtonTapped:(id)sender {
    
    NSMutableArray * filteredSalesArray = [[NSMutableArray alloc]init];
    NSMutableArray* predicateArray = [[NSMutableArray alloc]init];
    
    
    NSString *selectedCustomerName = [SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Customer_Name"]];
    NSString *selectedCustomerType = [SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Customer_Type"]];
    NSString *selectedDocumentType = [SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"Document_Type"]];
    NSString *selectedFromDate = [NSString stringWithFormat:@"%@ 00:00:00",[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"From_Date"]]];
    NSString *selectedToDate = [NSString stringWithFormat:@"%@ 23:59:59",[SWDefaults getValidStringValue:[filterParametersDict valueForKey:@"To_Date"]]];
    
    
    if (selectedCustomerName.length >0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Customer_Name ==[cd] %@",selectedCustomerName]];
    }
    
    if (selectedCustomerType.length>0) {
        if ([selectedCustomerType isEqualToString:@"All"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",selectedCustomerType]];
        }else if([selectedCustomerType isEqualToString:@"Cash"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",@"Y"]];
        }else{
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Status ==[cd] %@",@"N"]];
        }
    }
    if (selectedDocumentType.length>0) {
        if ([selectedDocumentType isEqualToString:@"All"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] %@",selectedDocumentType]];
        }else if([selectedDocumentType isEqualToString:@"Invoice"]) {
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] %@",@"I"]];
        }else{
            
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"SELF.Doc_Type ==[cd] %@",@"R"]];
        }
    }
    
    if (selectedFromDate.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"(Creation_Date >= %@)",selectedFromDate]];
    }
    if (selectedToDate.length>0) {
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"(Creation_Date <= %@)",selectedToDate]];
    }
    
    NSLog(@"predicate array is %@", predicateArray);
    
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    NSLog(@"predicate is %@", [compoundpred description]);
    
    filteredSalesArray=[[_salesSummaryArray filteredArrayUsingPredicate:compoundpred] mutableCopy];
    
    NSLog(@"filtered products are %@", filteredSalesArray);
    
    if (predicateArray.count==0) {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Missing Filter Criteria" andMessage:@"Please select filter criteria and try again" withController:self];
    }
    else  if (filteredSalesArray.count>0) {
        if ([self.delegate respondsToSelector:@selector(filteredSalesSummary:)]) {
            previousFilterParametersDict=filterParametersDict;
            [self.delegate filteredSalesSummary:filteredSalesArray];
            [self.delegate filterParametersSalesSummary:filterParametersDict];
            [self.filterPopOverController dismissPopoverAnimated:YES];
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"No Matches" andMessage:@"Please change your filter criteria and try again" withController:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
