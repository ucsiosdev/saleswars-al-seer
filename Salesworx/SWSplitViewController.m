//
//  SWSplitViewController.m
//  Salesworx
//
//  Created by Irfan Bashir on 5/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWSplitViewController.h"

#define kMenuFullWidth 320.0f
#define kMenuDisplayedWidth 280.0f
#define kMenuOverlayWidth (self.view.bounds.size.width - kMenuDisplayedWidth)
#define kMenuBounceOffset 10.0f
#define kMenuBounceDuration .3f
#define kMenuSlideDuration .3f

@interface SWSplitViewController ()

@end

@implementation SWSplitViewController

#pragma mark - Initialization

- (id)initWithFrontViewController:(UIViewController *)aFrontViewController rearViewController:(UIViewController *)aBackViewController {
	self = [super initWithFrontViewController:aFrontViewController rearViewController:aBackViewController];
	
	if (nil != self) {
		self.delegate = self;
	}
	
	return self;
}

#pragma - ZUUIRevealControllerDelegate Protocol.

/*
 * All of the methods below are optional. You can use them to control the behavior of the ZUUIRevealController, 
 * or react to certain events.
 */
- (BOOL)revealController:(ZUUIRevealController *)revealController shouldRevealRearViewController:(UIViewController *)rearViewController {
    CGRect frame = revealController.view.bounds;
    frame.size.width = self.rearViewRevealWidth;
    [rearViewController.view setFrame:frame];
	return YES;
}

- (BOOL)revealController:(ZUUIRevealController *)revealController shouldHideRearViewController:(UIViewController *)rearViewController  {
	return YES;
}

- (void)revealController:(ZUUIRevealController *)revealController willRevealRearViewController:(UIViewController *)rearViewController  {

}

- (void)revealController:(ZUUIRevealController *)revealController didRevealRearViewController:(UIViewController *)rearViewController {
    
    UIView *touchIntercepterView = [[UIView alloc] initWithFrame:revealController.frontViewController.view.frame];
    //[touchIntercepterView setBackgroundColor:[UIColor redColor]];
    [revealController.frontViewController.view endEditing:YES];
    touchIntercepterView.tag=555555555;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:revealController action:@selector(revealToggle:)];
    [touchIntercepterView addGestureRecognizer:tapGestureRecognizer];
    [revealController.frontViewController.view insertSubview:touchIntercepterView atIndex:INT_MAX];

}

- (void)revealController:(ZUUIRevealController *)revealController willHideRearViewController:(UIViewController *)rearViewController {

}

- (void)revealController:(ZUUIRevealController *)revealController didHideRearViewController:(UIViewController *)rearViewController  {
//    rearViewController=nil;
    NSArray *subviews = [revealController.frontViewController.view subviews];
    UIView *touchInterceptionView = [subviews lastObject];
    if(touchInterceptionView.tag==555555555)
    {
        [touchInterceptionView removeFromSuperview];
    }
}

- (void)revealController:(ZUUIRevealController *)revealController willResignRearViewControllerPresentationMode:(UIViewController *)rearViewController {

}

- (void)revealController:(ZUUIRevealController *)revealController didResignRearViewControllerPresentationMode:(UIViewController *)rearViewController {

}

- (void)revealController:(ZUUIRevealController *)revealController willEnterRearViewControllerPresentationMode:(UIViewController *)rearViewController {

}

- (void)revealController:(ZUUIRevealController *)revealController didEnterRearViewControllerPresentationMode:(UIViewController *)rearViewController {

}

#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
