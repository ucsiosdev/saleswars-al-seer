//
//  LicensingViewController.h
//  SalesWars
//


#import <UIKit/UIKit.h>
#import "SWPlatform.h"

NS_ASSUME_NONNULL_BEGIN

@interface LicensingViewController :  SWViewController <UIPopoverControllerDelegate, UITextFieldDelegate> {
    
    NSString *licenceType;
    SWLoadingView *loadingView;
    UIPopoverController *popoverController;
    SynchroniseViewController *verifyLicenceController;
    SEL action;
    NSString *ClientVersion;
}

/********  IBOutlet Connections  *******/
@property (strong, nonatomic) IBOutlet UIView *viewContent;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerID;
@property (strong, nonatomic) IBOutlet UITextField *TFCustomerID;
@property (strong, nonatomic) IBOutlet UILabel *lblLicenseType;
@property (strong, nonatomic) IBOutlet UITextField *TFLicenseType;
@property (strong, nonatomic) IBOutlet UIButton *btnActivate;
@property (strong, nonatomic) IBOutlet UILabel *lblPowerdBy;

/********  Customer Properties  *******/
@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;
@property(strong,nonatomic)  NSString *customerID;


@end

NS_ASSUME_NONNULL_END
