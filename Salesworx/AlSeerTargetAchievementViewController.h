//
//  AlSeerTargetAchievementViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/7/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlSeerTargetAchievementViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

{
    NSArray *sortedArray;
}
@property(strong,nonatomic)NSString* selectedAgency;

@property(strong,nonatomic)UIPopoverController * popOver;

@property(strong,nonatomic)NSMutableArray* achievementArray;
@property (strong, nonatomic) IBOutlet UITableView *achTblView;
- (IBAction)closeButtonTapped:(id)sender;

@end
