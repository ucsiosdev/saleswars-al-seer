//
//  TypeViewController.h
//  SWSession
//
//  Created by msaad on 3/17/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWPlatform.h"

@interface TypeViewController : SWTableViewController <EditableCellDelegate , UITextFieldDelegate>{
    NSArray *types;
    SEL action;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL action;

@end
