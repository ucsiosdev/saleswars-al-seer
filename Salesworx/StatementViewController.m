//
//  StatementViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/21/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "StatementViewController.h"
#define kAlertViewAmountCollectExceeds  1

@interface StatementViewController ()
- (void)saveCollection;
@end

@implementation StatementViewController
@synthesize action;
@synthesize target,totalAmtPaid;

- (id)initWithCustomer:(NSDictionary *)c
{
    self = [super init];
    if (self)
    {
        customer=c;
        [self setTitle:NSLocalizedString(@"Statement", nil)];
        _total = 0;
        _balance = 0;
        isStatment = NO;
    }
    
    return self;
}

- (id)initWithCustomer:(NSDictionary *)c andCollectionInfo:(NSDictionary *)ci
{
    self = [self initWithCustomer:c];
    if (self) {
        collectionInfo=ci;
        [self setTitle:@"Settle Collection"];
        _balance = [[collectionInfo objectForKey:@"Amount"] doubleValue];
        _payAmount = _balance;
        
        paidInvoices = [NSMutableArray array] ;
        isStatment = NO;
        
        saveButton=YES;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    balanceButton.enabled=YES;

    selectedIndexArray=[[NSMutableArray alloc]init];
    
    NSLog(@"invoices count in view did load %d", [invoices count]);
    
    
    
    [Flurry logEvent:@"Statement View"];
    
    balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 25)] ;
    [balanceLabel setText:@""];
    [balanceLabel setTextAlignment:NSTextAlignmentCenter];
    [balanceLabel setBackgroundColor:[UIColor redColor]];
    [balanceLabel setTextColor:[UIColor whiteColor]];
    [balanceLabel setFont:BoldSemiFontOfSize(14.0f)];
   
    self.gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"customer data is %@", [customer description]);
    
    NSLog(@"count of customer data at statement %d",[customer count]);
    
    [self getSalesOrderServiceDidGetAllInvoices: [[SWDatabaseManager retrieveManager] dbGetInvoicesOfCustomer:[customer stringForKey:@"Customer_ID"] withSiteId:[customer stringForKey:@"Site_Use_ID"]andShipCustID:[customer stringForKey:@"Ship_Customer_ID"] withShipSiteId:[customer stringForKey:@"Ship_Site_Use_ID"] andIsStatement:isStatment]];
    
    
    
    //fixing the total label
    
    totalAmtPaid=[[NSUserDefaults standardUserDefaults]doubleForKey:@"totalAmountPaid"];
 
    
    _total=_total-totalAmtPaid;
    
    NSLog(@"final total %f", _total);
    
    
    
    
    NSLog(@"total amot paid from db %f", totalAmtPaid);
    
    
  //  [balanceLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Balance", nil),[NSString currencyStringWithDouble:_balance]]];
    
    [self setupToolbar];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)setupToolbar {
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
    }
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
    
    lastSyncButton = [UIBarButtonItem labelButtonWithTitle:[NSString stringWithFormat:@"Total: %@", [NSString currencyStringWithDouble:_total]]];
    
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    if (collectionInfo) {
        //remainingBalance=_balance;
       
       // [balanceLabel setText:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Balance", nil),[NSString currencyStringWithDouble:_balance]]];
        
        balanceLabel.text=[NSString currencyStringWithDouble:_balance];
        
        balanceButton=[[UIBarButtonItem alloc]initWithTitle:balanceLabel.text style:UIBarButtonItemStylePlain target:self action:nil];
      
        
        [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, balanceButton, flexibleSpace, lastSyncButton, flexibleSpace, nil] animated:YES];
    } else {

        

        [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace,flexibleSpace, lastSyncButton, flexibleSpace, nil] animated:YES];
    }

    NSLog(@"balance in toolbar is %f", _balance);

}

- (void)save:(UIBarButtonItem *)sender {
    
    [self.view endEditing:YES];
    NSLog(@"invoice count in save %d", [invoices count]);
    float totalPaid = 0.0f;
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool{
            NSMutableDictionary *row = [NSMutableDictionary dictionaryWithDictionary:[invoices objectAtIndex:i]];
            float amount = [[row objectForKey:@"Paid"] doubleValue];
            totalPaid = totalPaid + amount;
        }
    }
    
    NSLog(@"total paid is %f", totalPaid);
    double remaining = [[collectionInfo objectForKey:@"Amount"] floatValue];
    remaining = remaining - totalPaid;
    
    NSLog(@"remaining is %f", remaining);
    
    
    if ([[collectionInfo objectForKey:@"CollectionType"] isEqualToString:@"CASH"])
    {
        [self saveCollection];
    } else {
        if (remaining > _payAmount) {
            UIAlertAction* yesAction = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                [self saveCollection];
            }];
            UIAlertAction* noAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                
            }];
            NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
            [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:@"Amount collected exceeds the total amount allocated for invoices" andMessage:@"Do you wish to continue?" andActions:actionsArray withController:self];
        } else if (remaining < 0) {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"Settled amount can't be in negative" withController:self];
        } else {
            [self saveCollection];
        }
    }
    
}
- (void)scrollTableViewToTextField:(NSIndexPath *)indexPath {
    [self.gridView.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)saveCollection {
    UIAlertAction* yesAction = [UIAlertAction
                                actionWithTitle:NSLocalizedString(KAlertYESButtonTitle, nil)
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
        [self performSelector:@selector(callSaveCollection) withObject:nil afterDelay:1.0];
    }];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(KAlertNoButtonTitle, nil)
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        
    }];
    NSMutableArray *actionsArray=[[NSMutableArray alloc]initWithObjects:yesAction,noAction,nil];
    [SWDefaults ShowConfirmationAlertAfterHidingKeyBoard:NSLocalizedString(@"Warning", nil) andMessage:@"Would you like to save this collections?" andActions:actionsArray withController:self];
}

- (void)callSaveCollection{
    NSMutableArray *array = [NSMutableArray array] ;
    
    NSLog(@"invoices in save action %d", [invoices count]);
    
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool{
            NSMutableDictionary *row = [NSMutableDictionary dictionaryWithDictionary:[invoices objectAtIndex:i]];
            
            if ([row objectForKey:@"Paid"])
            {
                if ([[row objectForKey:@"Paid"] doubleValue] > 0)
                {
                    [array addObject:row];
                }
            }
            
            NSLog(@"total updated %@",[row description]);
        }
    }
    
    
    //deadly insertion logic written which duplicates table view
    
    NSLog(@"array  before insertion %@", [array description]);
    
    NSLog(@"Array count before insertion %lu", (unsigned long)[array count]);
    
    
    NSString *collectionReference = [[SWDatabaseManager retrieveManager] saveCollection:collectionInfo withCustomerInfo:customer andInvoices:array];
    AppControl *appControl = [AppControl retrieveSingleton];
    if ([appControl.ALLOW_MULTI_CURRENCY isEqualToString:@"Y"])
    {
        [[SWDatabaseManager retrieveManager] saveDocAdditionalInfoWithDoc:collectionReference andDocType:@"C" andAttName:@"CURR" andAttValue:[collectionInfo stringForKey:@"selectedCurrency"] andCust_Att_5:[collectionInfo stringForKey:@"currencyRate"]];
    }
   
    [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Collection Saved Successfully", nil) andMessage:[NSString stringWithFormat:@"%@ %@: \n %@",NSLocalizedString(@"Vide", nil),NSLocalizedString(@"Reference No", nil), collectionReference] withController:self];
    
    
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger count = [userDefaults integerForKey:@"collectionCount"];
    
    if (count>=0) {
        [[NSUserDefaults standardUserDefaults]setInteger:count+1 forKey:@"collectionCount"];
    }

    
    NSLog(@"array count after insertion %lu", (unsigned long)[array count]);
    
    NSLog(@"array  after insertion %@", [array description]);
    
    
    NSLog(@"total count is %@",[array valueForKey:@"PaidAmt"]);
    
    
    
    //incrementing the collection counter
    [SWDefaults setCollectionCounter:[SWDefaults collectionCounter]+1];
    
    if (self.target)
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action];
#pragma clang diagnostic pop;
    }
    
    
    
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
}
#pragma mark GridView DataSource
- (int)indexOfColumnForInput:(GridView *)gridView {
    if (collectionInfo) {
        return 4;
    }
    
    return -1;
}
- (int)numberOfColumnsInGrid:(GridView *)gridView {
    
    if (collectionInfo) {
        return 5;
        
    }
    
    return 4;
}
- (int)numberOfRowsInGrid:(GridView *)gridView {
    
    
    
    
    NSLog(@"statement screen count %d", [invoices count]);
    
    return invoices.count;
    
    
}
- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0)
    {
        title = [NSString stringWithFormat:@"    %@",NSLocalizedString(@"Document No", nil)];
        
    } else if (column == 1) {
        title = NSLocalizedString(@"Invoice Date", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Balance Amount", nil);
    } else if (column == 3) {
        title = NSLocalizedString(@"Paid Amount", nil);
    } else {
        title = NSLocalizedString(@"Settled Amount", nil);
    }
    
    return title;
}



- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    

   
    NSDictionary *data = [invoices objectAtIndex:row];
    
    NSLog(@"invoice count in text for row %d", [invoices count]);
    
    NSString *value = @"";
    if (column == 0) {
        value = [NSString stringWithFormat:@"     %@", [data objectForKey:@"C.Invoice_Ref_No"]];
    } else if (column == 1) {
        value = [data dateStringForKey:@"InvDate" withDateFormat:NSDateFormatterMediumStyle];
    } else if (column == 2) {
        value = [data currencyStringForKey:@"NetAmount"];
    } else if (column == 3) {
        value = [data currencyStringForKey:@"PaidAmt"];
    } else if (column == 4) {
        if ([data objectForKey:@"Paid"]) {
            value = [data currencyStringForKey:@"Paid"];
        } else {
            value = [NSString stringWithFormat:@"%@0.00",[[SWDefaults userProfile]valueForKey:@"Currency_Code"]];
        }
        
        
    }
    
    // [invoices removeAllObjects];
    
    return value;
}
- (NSString *)gridView:(GridView *)gridView keyForRow:(int)rowIndex {
    NSDictionary *data = [invoices objectAtIndex:rowIndex];
    
    
    
    return [data objectForKey:@"C.Invoice_Ref_No"];
}

#pragma mark GridView Delegate




- (void)gridView:(GridView *)gridView didFinishInputForKey:(NSString *)key andValue:(id)value {
    
    NSLog(@"value entered");
    
    
    balanceLabel.text=nil;
    //[invoices removeAllObjects];
    float totalPaid = 0.0f;
    
    if ([value length] == 0) {
        value = @"0.00";
    }
    
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool{
            NSMutableDictionary *row = [NSMutableDictionary dictionaryWithDictionary:[invoices objectAtIndex:i]];
            
            if ([[row objectForKey:@"C.Invoice_Ref_No"] isEqualToString:key]) {
                [row setObject:value forKey:@"Paid"];
                [invoices replaceObjectAtIndex:i withObject:row];
                [self.gridView reloadRow:i];
            }
            
            float amount = [[row objectForKey:@"Paid"] doubleValue];
            totalPaid = totalPaid + amount;
        }
    }
    
    _balance = [[collectionInfo objectForKey:@"Amount"] floatValue];
    _balance = _balance - totalPaid;
    
    //[balanceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Balance", nil), [NSString currencyStringWithDouble:_balance]]];
    //balanceLabel.text=[NSString currencyStringWithDouble:_balance];
    [self setupToolbar];
    
    NSLog(@"balance is %@", balanceLabel.text);
    
}

- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex {
    
    NSLog(@"selected index %d", rowIndex);
   
}
- (void)gridView:(GridView *)gridView willStartInputForRow:(GridCell *)row andKey:(NSString *)key {
    
   // [row.labelView setHighlightedTextColor:[UIColor blueColor]];
    
    NSIndexPath *indexPath = [self.gridView.tableView indexPathForCell:row];
    
    NSLog(@"index path is %d", indexPath.row);
    
    
    
    NSString* selectedIndex=[NSString stringWithFormat:@"%d", indexPath.row];
    
    
    NSLog(@"selected int index %d", [selectedIndex intValue]);
    
    
    
    
    
    if ([selectedIndexArray containsObject:selectedIndex]) {
        
        
        NSLog(@"update the inserted query dont insert again");
        
        
        //UPDATING THE INSERTED QUERY
        
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"Update"];
        
        NSLog(@"bool is %d",[[NSUserDefaults standardUserDefaults]boolForKey:@"Update"]);
        
        NSLog(@"array desc  %d", [selectedIndexArray count]);
        
        
        
    }
    else
    {
        NSLog(@"else hit");
        [selectedIndexArray addObject:selectedIndex];
        
        
        NSLog(@"array desc after adding %@", [selectedIndexArray description]);
    }
    
    
    
    
    
    
    
    
    [self performSelector:@selector(scrollTableViewToTextField:) withObject:indexPath afterDelay:0.1f];
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:[invoices objectAtIndex:indexPath.row]] ;
    
    if (_balance > 0)
    {
        float netAmount =  [[data objectForKey:@"NetAmount"] floatValue];
        float paidAmount;
        if((((NSNull *) [data objectForKey:@"PaidAmt"]  == [NSNull null]) || ([data objectForKey:@"PaidAmt"]  == nil) ))
        {       paidAmount =  0;    }
        else
        {       paidAmount =  [[data objectForKey:@"PaidAmt"] floatValue];   }
        float payingAmount = netAmount - paidAmount;
        if(payingAmount > _balance)
        {       payingAmount= _balance;   }
        //[row setText:[NSString stringWithFormat:@"%.2f", payingAmount] forColumn:4];
        [row setText:[NSString stringWithFormat:@"%.2f", payingAmount] forColumn:4 isTextCenterAlign:NO];
        
        [data setObject:[NSString stringWithFormat:@"%.2f",payingAmount] forKey:@"Paid"];
        [data setObject:@"Y" forKey:@"CheckedInvoice"];
        [paidInvoices addObject:data];
        [invoices replaceObjectAtIndex:indexPath.row withObject:data];
        _balance = _balance - payingAmount;
        [balanceLabel setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Balance", nil), [NSString currencyStringWithDouble:_balance]]];
    }
    
    
}

#pragma Sales Order Service Delegate
- (void)getSalesOrderServiceDidGetAllInvoices:(NSArray *)_invoices {
    invoices=[NSMutableArray arrayWithArray:_invoices];
    
    for (int i = 0; i < invoices.count; i++) {
        @autoreleasepool{
            NSDictionary *row = [invoices objectAtIndex:i];
            double amount = [[row objectForKey:@"NetAmount"] doubleValue];
            
            _total = _total + amount;
        }
    }
    
    
    
    NSLog(@"total here is %f", _total);
    [self.gridView reloadData];
    if (invoices.count > 0) {
        [self setupToolbar];
        if (saveButton) {
            [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)] ];
        }
        
    }
    _invoices=nil;
}

#pragma mark UIKeyboard Notifications
- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        toolbarFrame.origin.y = self.view.bounds.size.height + 64;
        
    }
    else
    {
        toolbarFrame.origin.y = self.view.bounds.size.height + 44;
    }
    
    [UIView animateWithDuration:animationDuration animations:^ {
        [self.gridView setFrame:self.view.bounds];
        [self.navigationController.toolbar setFrame:toolbarFrame];
        
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        // here you go with iOS 7
        toolbarFrame.origin.y = self.view.bounds.size.height + 64 - keyboardHeight;
        
    }
    else
    {
        toolbarFrame.origin.y = self.view.bounds.size.height + 44 - keyboardHeight;
    }
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        [self.gridView setFrame:f];
        [self.navigationController.toolbar setFrame:toolbarFrame];
    }];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
