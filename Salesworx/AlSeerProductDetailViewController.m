//
//  AlSeerProductDetailViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerProductDetailViewController.h"
#import "SWDatabaseManager.h"
#import "AlSeerProductStockViewTableViewCell.h"
#import "AlSeerProductStockViewHeaderTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "FMDBHelper.h"
#import "DataType.h"
#import "AlSeerProductStockHeaderTableViewCell.h"
#import "AlSeerSalesOrderProductStockTableViewCell.h"
#import "FullScreenProductDetailsViewController.h"

@interface AlSeerProductDetailViewController ()

@end

@implementation AlSeerProductDetailViewController

@synthesize productDict,itemDescLbl,itemNumberLbl,brandLbl,customProductStockView,productImageView,productImagesScrollView,priceDict,baseUOMLbl,totalStockLbl,productPageControl;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    allMediaFilesArray=[[NSMutableArray alloc]init];
    imagesArray=[[NSMutableArray alloc]init];
    
    
    [self fetchMediaFilesFromDocuments];
    [self seperateMediaFilesIntoArrays];
    
    NSInteger numberOfPages = imagesArray.count;
    
    productPageControl.numberOfPages=imagesArray.count;
    productPageControl.currentPage=0;
    
    
    productImagesScrollView.pagingEnabled = YES;
    productImagesScrollView.backgroundColor=[UIColor whiteColor];
    productImagesScrollView.contentSize = CGSizeMake(numberOfPages * productImagesScrollView.frame.size.width, productImagesScrollView.frame.size.height);
    productImagesScrollView.delegate=self;
    
    
    productImagesScrollView.layer.cornerRadius=1.0f;
    productImagesScrollView.layer.masksToBounds=YES;
    productImagesScrollView.layer.borderColor=[[UIColor colorWithRed:(91/255.0) green:(91/255.0) blue:(107/255.0) alpha:1]CGColor];
    productImagesScrollView.layer.borderWidth= 1.0f;
    
    for (int i = 0; i < numberOfPages; i++) {
        UIImageView *tmpImg = [[UIImageView alloc] initWithFrame:CGRectMake(i * productImagesScrollView.frame.size.width + 20,
                                                                            20,
                                                                            productImagesScrollView.frame.size.width - 40,
                                                                            638)];
        tmpImg.contentMode=UIViewContentModeScaleAspectFit;
        
        
        if (imagesArray.count>0) {
            MediaFile* mediaFile=[imagesArray objectAtIndex:i];
            // Get dir
            NSString *documentsDirectory = nil;
            
            //iOS 8 support
            if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
                documentsDirectory=[SWDefaults applicationDocumentsDirectory];
            }
            else
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                documentsDirectory = [paths objectAtIndex:0];
            }
            
            NSString* imagePath=[documentsDirectory stringByAppendingPathComponent:mediaFile.Filename];
            
            NSLog(@"image path is %@", imagePath);
            
            tmpImg.image = [UIImage imageWithContentsOfFile:imagePath];
            [productImagesScrollView addSubview:tmpImg];
        }
    }
    
    //fetching product stock data
    NSString* productStockQry=[NSString stringWithFormat:@"SELECT  IFNULL(Lot_Qty,0) AS Lot_Qty ,IFNULL(Lot_No,0)AS Lot_No, IFNULL(Expiry_Date,0) AS Expiry_Date ,IFNULL(Custom_Attribute_1,0)AS QC,  IFNULL(Custom_Attribute_2,0)AS Blocked FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY  Expiry_Date ASC",[productDict stringForKey:@"ItemID"]];
    NSLog(@"product stock qry is %@", productStockQry);
    productStockArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:productStockQry];
    if (productStockArray.count>0) {
        NSLog(@"product stock array is %@", [productStockArray description]);
    }
    
    priceDict=[[SWDatabaseManager retrieveManager] dbdbGetPriceListProduct:[productDict valueForKey:@"ItemID"]];
    NSLog(@"price dict data %@", [priceDict description]);
    
    if (priceDict.count>0) {
        NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];
        self.unitPriceLbl.text=[NSString stringWithFormat:@"%@ %@", userCurrencyCode,[priceDict valueForKey:@"sellingPrice"]];
    }
    
    NSLog(@"check product dict in product details screen %@",[productDict description] );
    
    //fetch agency
    NSString* agencyQry=[NSString stringWithFormat:@"select IFNULL(Agency,0) AS Agency from TBL_Product where Inventory_Item_ID='%@' and Item_Code='%@' ", [productDict valueForKey:@"Inventory_Item_ID"],[productDict valueForKey:@"Item_Code"]];
    NSLog(@"query for fetching agency %@", agencyQry);
    NSMutableArray* agencyArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:agencyQry];
    NSLog(@"agencyArray details %@", [agencyArray description]);
    if (agencyArray.count>0) {
        self.agencyLbl.text=[NSString stringWithFormat:@"%@",[[agencyArray valueForKey:@"Agency"] objectAtIndex:0]];
    }
    
    
    //fetch sum
    NSString* sumofLotsQty=[NSString stringWithFormat:@"SELECT IFNULL(Lot_Qty,0) AS Total from TBL_Product_Stock WHERE Item_ID='%@' ",[productDict valueForKey:@"ItemID"]];
    NSLog(@"product stock qry is %@", sumofLotsQty);
    NSMutableArray*  sumArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:sumofLotsQty];
    if (sumArray.count>0) {
        totalStock=[NSString stringWithFormat:@"%@",[[sumArray valueForKey:@"Total"] objectAtIndex:0]];
    }
    
    for (UIView *i in self.view.subviews){
        if([i isKindOfClass:[UIView class]]){
            UIView *currentView = (UIView *)i;
            if(currentView.tag == 1|| currentView.tag == 2||currentView.tag == 3||currentView.tag == 4||currentView.tag == 5 ||currentView.tag == 6||currentView.tag == 7){
                CALayer *layer = currentView.layer;
                layer.shadowOffset = CGSizeMake(1, 1);
                layer.shadowColor = [[UIColor blackColor] CGColor];
                layer.shadowRadius = 2.0f;
                layer.shadowOpacity = 0.80f;
                layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];
            }
        }
    }
    
    doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapOnce:)];
    doubleTapGesture.numberOfTapsRequired = 1;
    [productImagesScrollView addGestureRecognizer:doubleTapGesture];
    
    if (@available(iOS 15.0, *)) {
        self.stockTableVIew.sectionHeaderTopPadding = 0;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView==fullScreenproductImagesScrollView) {
        CGFloat pageWidth = fullScreenproductImagesScrollView.frame.size.width;
        float fractionalPage = fullScreenproductImagesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        fullScreenProductImagesPageControl.currentPage = page;
    }
    else {
        CGFloat pageWidth = productImagesScrollView.frame.size.width;
        float fractionalPage = productImagesScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        productPageControl.currentPage = page;
    }
}

- (void)tapOnce:(id)tapOnce {
    FullScreenProductDetailsViewController * fullScreenVC= [[FullScreenProductDetailsViewController alloc]init];
    fullScreenVC.imagesArray=imagesArray;
    [self presentViewController:fullScreenVC animated:YES completion:nil];
}

-(void)collapseImages {
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES animated:YES];
    brandLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Brand_Code"]];
    itemNumberLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Item_Code"]];
    itemDescLbl.text=[NSString stringWithFormat:@"%@", [productDict valueForKey:@"Description"]];
    
    NSLog(@"product dict is %@", productDict);
    
    NSString* barcodeStr=[NSString stringWithFormat:@"%@",[productDict valueForKey:@"BarCode"]];
    
    if ([NSString isEmpty:barcodeStr]==NO) {
        barcodeLbl.text=[SWDefaults getValidStringValue:barcodeStr];
    }
    else{
        barcodeLbl.text=@"N/A";
    }
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:headerTitleFont, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.title=@"Product Details";
    
    
    //fetch base UOM
    //fetch agency
    
    NSString* baseUOMQry=[NSString stringWithFormat:@"SELECT IFNULL(Item_UOM,0)AS  Item_UOM  FROM TBL_Item_UOM where Item_Code ='%@' and Conversion ='1' order by Sync_Timestamp desc ", [productDict valueForKey:@"Item_Code"]];
    NSLog(@"query for fetching base uom %@", baseUOMQry);
    NSMutableArray* baseUOMArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:baseUOMQry];
    NSLog(@"base UOM details %@", [baseUOMArray description]);
    if (baseUOMArray.count>0) {
        self.baseUOMLbl.text=[NSString stringWithFormat:@"%@",[[baseUOMArray valueForKey:@"Item_UOM"] objectAtIndex:0]];
    }
    
    //fetch conversion
    NSArray* conversionArray=[SWDatabaseManager fetchConversionFactorforUOMitemCode:itemNumberLbl.text ItemUOM:@"CS"];
    NSLog(@"test stock %@", [conversionArray description]);
    if (conversionArray.count>0) {
        
        conversionRate=[[[conversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        NSInteger tempStock=[totalStock integerValue];
        NSInteger convertedStock= tempStock/conversionRate;
        totalStockLbl.text=[[NSString stringWithFormat:@"%d", convertedStock] stringByAppendingString:@"(CS)"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark UITableView data source methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (productStockArray.count>0) {
        return productStockArray.count;
    }
    else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* stockViewCell=@"customHeader";
    AlSeerProductStockHeaderTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"AlSeerProductStockHeaderTableViewCell" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.contentView.backgroundColor=[UIColor colorWithRed:(246.0/255.0) green:(247.0/255.0) blue:(251.0/255.0) alpha:1];
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* stockViewCell=@"soStockCell";
    AlSeerSalesOrderProductStockTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderProductStockTableViewCell" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
    }
    
    cell.lotNumberLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_No"] objectAtIndex:indexPath.row]];
    cell.lotNumberLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];
    
    
    NSInteger tempQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]] integerValue];
    NSInteger convertedQty= tempQty/conversionRate;
    
    cell.qtyLbl.text=[NSString stringWithFormat:@"%d", convertedQty];
    
    
    tempStock=tempStock+convertedQty;
    totalStockLbl.text=[[NSString stringWithFormat:@"%d", tempStock] stringByAppendingString:@"(CS)"];
    cell.qtyLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];
    
    
    NSInteger blockedQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Blocked"] objectAtIndex:indexPath.row]] integerValue];
    NSInteger convertedBlockedQty= blockedQty/conversionRate;
    
    cell.blockedStockLbl.text=[NSString stringWithFormat:@"%d", convertedBlockedQty];
    cell.blockedStockLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];
    
    NSInteger qc=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"QC"] objectAtIndex:indexPath.row]] integerValue];
    NSInteger convertedQC= qc/conversionRate;
    cell.qualityInspectionLbl.text=[NSString stringWithFormat:@"%d", convertedQC];
    
    cell.qualityInspectionLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];

    
    // Convert string to date object
    NSString*  dateStr=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Expiry_Date"] objectAtIndex:indexPath.row]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateStr = [dateFormat stringFromDate:date];
    
    
    NSLog(@"desired date format %@", dateStr);
    cell.expiryLbl.text=dateStr;
    cell.expiryLbl.textColor=[UIColor colorWithRed:(91.0/255.0) green:(91.0/255.0) blue:(104.0/255.0) alpha:1];
    
    return cell;
}

+(NSString*)getDateStr:(NSString *)dateStr
{
    if([dateStr isEqualToString:@""] || dateStr.length<8)
        return @"";
    else
    {
        // Convert string to date object
        dateStr=[dateStr substringToIndex:8];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd MMM, YYYY"];
        dateStr = [dateFormat stringFromDate:date];
        return dateStr;
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark fetching images from documents directory
-(void)fetchMediaFilesFromDocuments {
    
    [allMediaFilesArray removeAllObjects];
    
    NSString* inventoryItemID=[productDict stringForKey:@"Inventory_Item_ID"];
    
    NSString *query = [NSString stringWithFormat:@"Select * from TBL_Media_Files WHERE Download_Flag='N' AND Entity_ID_1 = '%@'",inventoryItemID];
    
    NSLog(@"query for media in product details %@", query);
    
    
    NSMutableArray * array = [NSMutableArray arrayWithArray:[FMDBHelper executeQuery:query]];
    
    NSLog(@"media content %@", [array description]);
    
    
    for (NSMutableDictionary *customerDic in array) {
        MediaFile *customer = [MediaFile new];
        
        if ([[customerDic valueForKey:@"Media_File_ID"] isEqual: [NSNull null]]) {
            customer.Media_File_ID = @"";
        }else{
            customer.Media_File_ID = [customerDic valueForKey:@"Media_File_ID"];
        }
        
        if ([[customerDic valueForKey:@"Entity_ID_1"] isEqual: [NSNull null]]) {
            customer.Entity_ID_1 = @"";
        }else{
            customer.Entity_ID_1 = [customerDic valueForKey:@"Entity_ID_1"];
        }
        
        if ([[customerDic valueForKey:@"Item_ID_2"] isEqual: [NSNull null]]) {
            customer.Entity_ID_2 = @"";
        }else{
            customer.Entity_ID_2 = [customerDic valueForKey:@"Entity_ID_2"];
        }
        
        if ([[customerDic valueForKey:@"Entity_Type"] isEqual: [NSNull null]]) {
            customer.Entity_Type = @"";
        }else{
            customer.Entity_Type = [customerDic valueForKey:@"Entity_Type"];
        }
        
        if ([[customerDic valueForKey:@"Media_Type"] isEqual: [NSNull null]]) {
            customer.Media_Type = @"";
        }else{
            customer.Media_Type = [customerDic valueForKey:@"Media_Type"];
        }
        
        if ([[customerDic valueForKey:@"Filename"] isEqual: [NSNull null]]) {
            customer.Filename = @"";
        }else{
            customer.Filename = [customerDic valueForKey:@"Filename"];
        }
        
        if ([[customerDic valueForKey:@"Caption"] isEqual: [NSNull null]]) {
            customer.Caption = @"";
        }else{
            customer.Caption = [customerDic valueForKey:@"Caption"];
        }
        
        if ([[customerDic valueForKey:@"Thumbnail"] isEqual: [NSNull null]]) {
            customer.Thumbnail = @"";
        }else{
            customer.Thumbnail = [customerDic valueForKey:@"Thumbnail"];
        }
        if ([[customerDic valueForKey:@"Is_Deleted"] isEqual: [NSNull null]]) {
            customer.Is_Deleted = @"";
        }else{
            customer.Is_Deleted = [customerDic valueForKey:@"Is_Deleted"];
        }
        if ([[customerDic valueForKey:@"Download_Flag"] isEqual: [NSNull null]]) {
            customer.Download_Flag = @"";
        }else{
            customer.Download_Flag = [customerDic valueForKey:@"Download_Flag"];
        }
        [allMediaFilesArray addObject:customer];
    }
}

-(void)seperateMediaFilesIntoArrays {
    
    [imagesArray removeAllObjects];
    [pdfArray removeAllObjects];
    [videoArray removeAllObjects];
    [pptArray removeAllObjects];
    
    
    for (MediaFile * mfile in allMediaFilesArray) {
        
        NSLog(@"check media type %@", mfile.Media_Type);
        
        
        if ([mfile.Media_Type isEqualToString:@"Image"]) {
            [imagesArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Video"]) {
            [videoArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Brochure"]) {
            [pdfArray addObject:mfile];
        }else if ([mfile.Media_Type isEqualToString:@"Powerpoint"]) {
            [pptArray addObject:mfile];
        }
        else{
        }
    }
}

@end
