//
//  TableCellHeaderSalesOrder.h
//  SalesWars
//
//  Created by Nethra on 1/10/20.
//  Copyright © 2020 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableCellHeaderSalesOrder : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnItemName;
@end

NS_ASSUME_NONNULL_END
