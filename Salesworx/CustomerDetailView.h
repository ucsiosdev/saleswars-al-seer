//
//  CustomerDetailView.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "CustomerHeaderView.h"
@interface CustomerDetailView : UIView <UITableViewDelegate, UITableViewDataSource > {
    UITableView *tableView;
    NSMutableDictionary *customer;
    CustomerHeaderView *customerHeaderView;
   // float availableBalance ;
    

}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *customer;
@property (nonatomic, strong) CustomerHeaderView *customerHeaderView;

- (id)initWithCustomer:(NSDictionary *)row;

@end