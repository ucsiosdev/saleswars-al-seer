    //
//  SWRouteTableViewCell.m
//  SalesWars
//
//  Created by Prasann on 31/12/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import "SWRouteTableViewCell.h"

@implementation SWRouteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    if (self.tag==1){
        self.statusView.backgroundColor = [UIColor colorWithRed:243.0 / 255.0 green:150.0 / 255.0 blue:0.0 / 255.0 alpha:1.0f];
    }else {
         self.statusView.backgroundColor = [UIColor clearColor];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    
    [super setHighlighted:highlighted animated:animated];
    
    
}
@end
