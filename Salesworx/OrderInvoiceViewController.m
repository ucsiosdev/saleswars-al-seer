//
//  OrderInvoiceViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 6/25/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "OrderInvoiceViewController.h"
#import "SWPlatform.h"

@interface OrderInvoiceViewController ()

@end

@implementation OrderInvoiceViewController

- (id)initWithDocumentReference:(NSString *)ref {
    self = [super init];
    
    if (self) {
        documentReference=ref;
        [self setTitle:@"Invoice"];
        [self getSalesOrderServiceDidGetInvoice:[[SWDatabaseManager retrieveManager] dbGetSalesOrderInvoice:documentReference]];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool {
        NSString *CellIdentifier = @"OrderItemsCell";
        UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        return cell;
    }
}

#pragma mark GridView Delegate
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return self.items.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 5;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    
    if (column == 0) {
        title = NSLocalizedString(@"Item Code", nil);
    } else if (column == 1) {
        title = NSLocalizedString(@"Description", nil);
    } else if (column == 2) {
        title = NSLocalizedString(@"Quantity", nil);
    } else if(column == 3) {
        title = NSLocalizedString(@"Unit Price", nil);
    } else if(column == 4){
        title = NSLocalizedString(@"Total", nil);
    }
    
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [self.items objectAtIndex:row];
    
    if (column == 0) {
        text = [NSString stringWithFormat:@"     %@", [data objectForKey:@"Item_Code"]];
    } else if (column == 1) {
        text = [data objectForKey:@"Description"];
    } else if (column == 2) {
        text = [NSString stringWithFormat:@"%@", [data objectForKey:@"Ordered_Quantity"]];
    } else if (column == 3) {
        text = [data currencyStringForKey:@"Unit_Selling_Price"];
    }else if (column == 4) {
        text = [data currencyStringForKey:@"Item_Value"];
    } 
    
    return text;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    if (columnIndex == 0) {
        return 20;
    } else if (columnIndex == 1) {
        return 40;
    } else if (columnIndex == 2) {
        return 10;
    }
    else if (columnIndex == 3) {
        return 15;
    }
    else if (columnIndex == 4) {
        return 15;
    }
    else
        return 0;
}

#pragma mark SalesOrderService Delegate
- (void)getSalesOrderServiceDidGetInvoice:(NSArray *)invoice {
    [self setItems:invoice];
    [self.gridView reloadData];
    
    if (self.items.count == 0) {
        noResultsView=[[SWNoResultsView alloc] initWithFrame:self.view.bounds];
        [self.view addSubview:noResultsView];
        [self setPreferredContentSize:CGSizeMake(300, 100)];
    } else {
        [self setPreferredContentSize:CGSizeMake(400, self.gridView.tableView.contentSize.height)];
    }
    
    invoice=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
