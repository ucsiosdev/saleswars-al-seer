//
//  AlSeerSalesOrderTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/21/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TblCellProductDetails : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *expiryLbl;
@property (strong, nonatomic) IBOutlet UILabel *lotNumberLbl;
@property (strong, nonatomic) IBOutlet UILabel *qtyLbl;
@property (strong, nonatomic) IBOutlet UILabel *qualityInspectionLbl;
@property (strong, nonatomic) IBOutlet UILabel *blockedStockLbl;
@property (strong, nonatomic) IBOutlet UILabel *lblTopLine;
@property (strong, nonatomic) IBOutlet UILabel *lblBottomLine;


@end
