//
//  CustomerTargetView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/16/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerTargetView.h"
#import "SWPlatform.h"


@implementation CustomerTargetView

@synthesize targetTableView;
@synthesize customer,customerPie;
@synthesize targets;
@synthesize customerHeaderView;
@synthesize categoryTableView;
@synthesize backGroundView;
@synthesize pieChartNew,saperatorView;
@synthesize slices = _slices,descriptionLblsArray,detailedDesc,detailedDescArray;


- (id)initWithCustomer:(NSDictionary *)row {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setCustomer:row];
        self.backgroundColor = [UIColor whiteColor];
       
       
        self.customerHeaderView=nil;
        [self setCustomerHeaderView:[[CustomerHeaderView alloc] initWithFrame:CGRectMake(0, 0, 700, 60) andCustomer:self.customer] ];
        [self.customerHeaderView setShouldHaveMargins:YES];
        [self addSubview:customerHeaderView];
        
        self.backGroundView=nil;
        self.backGroundView = [[UIView alloc] initWithFrame:CGRectMake(300,70,700,320)] ;
       //self.backGroundView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        detailedDescArray=[[NSMutableArray alloc]init];
        
        saperatorView=[[UIView alloc]initWithFrame:CGRectMake(280, 40, 2, 600)];
        saperatorView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"main-background.png"]];
        
        [self addSubview:saperatorView];
        
        
        self.categoryTableView=nil;
        self.categoryTableView=[[UITableView alloc]initWithFrame:CGRectMake(20, 70, 250, 560) style:UITableViewStyleGrouped] ;
        [self.categoryTableView setDelegate:self];
        [self.categoryTableView setDataSource:self];
        [self.categoryTableView setTag:2];
        self.categoryTableView.backgroundView = nil;
        self.categoryTableView.backgroundColor = [UIColor whiteColor];

        self.targetTableView=nil;
        [self setTargetTableView:[[UITableView alloc] initWithFrame:CGRectMake(300,400, 700,230) style:UITableViewStyleGrouped] ];
        [self.targetTableView setDelegate:self];
        [self.targetTableView setDataSource:self];
        [self.targetTableView setTag:1];
        self.targetTableView.backgroundView = nil;
        self.targetTableView.backgroundColor = [UIColor whiteColor];
        self.targetTableView.hidden=YES;
        
        if (@available(iOS 15.0, *)) {
            categoryTableView.sectionHeaderTopPadding = 0;
            targetTableView.sectionHeaderTopPadding = 0;
        }
        
        [self addSubview:self.targetTableView];

    }
    
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
}


//loading the target for customer

- (void)loadTarget {
    
    
    
    appCtrlFlag=[SWDefaults appControl];
    NSLog(@"check app control flag for custmer target %@",[appCtrlFlag valueForKey:@"ENABLE_TGT_CUST_FLD"]);
    
    
    
    NSString* appFlag=[SWDefaults checkTgtField];
    if (appFlag==nil) {
        
        
    }
    
    
       if ([appFlag  isEqualToString:@"Y"] ) {
        
        NSLog(@"This is Al seer");
           
           
           descriptionLblsArray=nil;
           
           descriptionLblsArray=[[NSMutableArray alloc]initWithObjects:@"Target Value",@"Sales Value",@"Achievement", nil];
           
           
           //get static labels from TBL_Sales_Target_Meta tbl
           
           
           
           staticLblData=[[SWDatabaseManager retrieveManager]fetchDataForQuery:@"select Column_Name , Column_Label from TBL_SALES_TARGET_META where Target_Type ='N'"];
           
           NSLog(@"static lable array %@", [staticLblData description]);
           
           
           
           for (NSInteger i=0; i<staticLblData.count; i++) {
               
           [descriptionLblsArray addObject:[[staticLblData valueForKey:@"Column_Label"] objectAtIndex:i]];
               
           }
           
           
           
           
           NSLog(@"desc lable array %@", [descriptionLblsArray description]);

           
           [self getCustomerServiceTargets:[[SWDatabaseManager retrieveManager] dbGetTarget:[self.customer objectForKey:@"Customer_No"]]];
           
           
           
           
        
    }
    
    
    else
    {
    
    
    //customerSer.delegate = self;
    [self getCustomerServiceTargets:[[SWDatabaseManager retrieveManager] dbGetTarget:[self.customer objectForKey:@"Customer_No"]]];
        
    }
    
    
}
- (void)loadPieChartWithAchievement:(NSDictionary *)pie;{
    @autoreleasepool {
    NSString *achieve = [pie valueForKey:@"Achieved_Target"] ;
    NSString *remain = [pie valueForKey:@"Remainning_Value"] ;
    
//        [pieChartNew removeFromSuperview];
//        
//        pieChartNew=nil;
        
        [self.backGroundView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
    pieChartNew = [[PCPieChart alloc] initWithFrame:CGRectMake(150, 10, 400, 350)] ;
    [pieChartNew setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];

    [pieChartNew setDiameter:240];
    [pieChartNew setSameColorLabel:YES];
    components = [NSMutableArray array] ;
    PCPieComponent *component = [PCPieComponent pieComponentWithTitle:@"Remaining Target" value:[remain floatValue]];
    PCPieComponent *component2 = [PCPieComponent pieComponentWithTitle:@"Sales" value:[achieve floatValue]];
    
    [component2 setColour:PCColorOrange];
    [component setColour:PCColorBlue];

    [components addObject:component2];
    [components addObject:component];
    [pieChartNew setComponents:components];
    
        
        
        NSLog(@"pie chart being added ");
        
        
    [self.backGroundView addSubview: pieChartNew];
    }
}
#pragma UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(tableView.tag==1)
    {
            return 1;
    }
    else 
    {
        return 1;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag==1)
    {
        if ([self.targets count] == 0)
        {
            return 1;
        }
        else
        {
            
            //remove hard coded val and return tha dynamic val
            return descriptionLblsArray.count;
        }

    }
    else 
    {
       return [self.targets count];
        
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    @autoreleasepool {
    NSString *CellIdentifier = @"CustomerDetailViewCell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) 
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    


    if(tv.tag==1)
    {
     
    
        if ([self.targets count] == 0) 
        {
            [cell.textLabel setText:NSLocalizedString(@"No target or achievement found", nil)];
        
        }
        
        else 
        {
            //NSDictionary *row = [self.targets objectAtIndex:indexPath.section];
           
        
            
            
            
            if ([[SWDefaults checkTgtField]isEqualToString:@"Y"])
            {
                
                
                cell.textLabel.text=[descriptionLblsArray objectAtIndex:indexPath.row];
                
                if (indexPath.row==2) {
                    
                    NSString* percentStr=@"%";
                    
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"%@%@",[detailedDescArray objectAtIndex:indexPath.row],percentStr];
                    
                }
                else
                {
                    NSString* userCurrencyCode=[[SWDefaults userProfile] valueForKey:@"Currency_Code"];

                    
                    cell.detailTextLabel.text= [NSString stringWithFormat:@"%@%@",userCurrencyCode,[detailedDescArray objectAtIndex:indexPath.row]];
                    
                }

                
                
            }
            
            
            else
            {
                if (indexPath.row == 0) {
                    [cell.textLabel setText:@"Target Value"];
                    [cell.detailTextLabel setText:[ self.customerPie currencyStringForKey:@"Target_Value"]];
                }
                else if (indexPath.row == 1) {
                    [cell.textLabel setText:@"FSR Target"];
                    [cell.detailTextLabel setText:[ self.customerPie currencyStringForKey:@"FSR_Target"]];
                }
                else if (indexPath.row == 2) {
                    [cell.textLabel setText:@"Sales Value"];
                    [cell.detailTextLabel setText:[ self.customerPie currencyStringForKey:@"Sales_Value"]];
                }
                else if (indexPath.row == 3)
                {
                    [cell.textLabel setText:@"Achievement"];
                    NSString *achievPercent = [[ self.customerPie objectForKey:@"Achieved_Target"]stringValue];
                    achievPercent = [achievPercent stringByAppendingString:@"%"];
                    [cell.detailTextLabel setText:achievPercent];
                }

            }
            
            
            
            
////            else if (indexPath.row == 1) {
////                [cell.textLabel setText:@"FSR Target"];
////                [cell.detailTextLabel setText:[ self.customerPie currencyStringForKey:@"FSR_Target"]];
////            } 
//            else if (indexPath.row == 1) {
//                [cell.textLabel setText:@"Sales Value"];
//                [cell.detailTextLabel setText:[ self.customerPie currencyStringForKey:@"Sales_Value"]];
//            }   
//            else if (indexPath.row == 2)
//            {
//                [cell.textLabel setText:@"Achievement"];
//                  NSString *achievPercent = [[ self.customerPie objectForKey:@"Achieved_Target"]stringValue];
//                achievPercent = [achievPercent stringByAppendingString:@"%"];
//                [cell.detailTextLabel setText:achievPercent];
//            }
        }

    }
    
    else 
    {
        
        
        
        //second cell tapped tables tag changed reload accordingly
        
        
        
       
        NSDictionary *row = [self.targets objectAtIndex:indexPath.row];

        [cell.textLabel setText:[row objectForKey:@"Category"]];
            
      
       // cell.textLabel.textColor = UIColorForPlainTable();
    }
    return cell;

}
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView.tag==1) 
    {
        if ([self.targets count] == 0) 
        {
            return nil;
        }
        else 
        {
            return [customerPie objectForKey:@"Category"];
        }
    }
    else
    {
        
        
        if ([[SWDefaults checkTgtField]isEqualToString:@"Y"]) {
            
            
            return  @"Agency/Category";
        }
        
        else
        {
        
        return @"Brand/Category";
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag==1) 
    {
        if ([self.targets count] == 0)
        {
            return 0.0f;
        }
        else
        {
            return 20.0f;
        }
    }
    else
    {
        return 40.0f;
    }
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)s {
    if (tv.tag==1) 
    {
        
        if ([self.targets count] == 0)
        {
            return nil;
        }
        
       GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithWidth:tv.bounds.size.width text:[customerPie objectForKey:@"Category"]] ;
        return sectionHeader;
    }
    else {
        return nil;
    }
}
#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tv.tag==1) 
    {
        [tv deselectRowAtIndexPath:indexPath animated:YES];
    }
    else 
    {
        //[self setCustomerPie:[targets objectAtIndex:indexPath.row]];
        
       
        
        if ([[SWDefaults checkTgtField]isEqualToString:@"Y"])
        
        {
            customerPie = [targets objectAtIndex:indexPath.row];
            
            NSLog(@"customer pie before reload %@", [customerPie description]);
            
            //[self.targetTableView setTag:1];
            
            
            [detailedDescArray removeAllObjects];
            
            selectedIndexPath=indexPath;
            
            
            [self getCustomerServiceTargets:targets];
            
            
            //[self reloadTargetData:(NSMutableArray*)customerPie];
            NSLog(@"customer pie after reload %@", [customerPie description]);
            
            
            //  [self loadPieChartWithAchievement:customerPie];
            [targetTableView reloadData];
            
            

        }
        else
        {
            [self setCustomerPie:[targets objectAtIndex:indexPath.row]];
            customerPie = [self.targets objectAtIndex:indexPath.row];
            [self.pieChartNew removeFromSuperview];
            [self loadPieChartWithAchievement:customerPie];
            [self.targetTableView reloadData];
        }
        
        
        

        
       
        
        
    }
    [tv deselectRowAtIndexPath:indexPath animated:YES];

}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell.textLabel setFont:LightFontOfSize(14.0f)];
    [cell.detailTextLabel setFont:LightFontOfSize(14.0f)];
}
#pragma mark SWCustomerService Delegate
//- (void)customerServiceDiddbGetTargets:(NSArray *)t {
- (void)getCustomerServiceTargets:(NSArray *)t
{
    [self setTargets:[NSMutableArray arrayWithArray:t   ] ];
    
    
    
    //[self setTargets:[t mutableCopy]];
    if ([self.targets count] == 0)
    {
        self.targetTableView.frame = CGRectMake(20, 70, self.frame.size.width-40,self.frame.size.height-80);
        //self.targetTableView.frame=self.bounds;
    }
    else 
    {
        
 
        
        
        int targetValue;
        int saleValue;
        for (int i=1; i<=[self.targets count];i++)
        {
            customerPie = [NSMutableDictionary dictionaryWithDictionary:[targets objectAtIndex:i-1]];
            targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
            if([customerPie objectForKey:@"Sales_Value"]==(id) [NSNull null] ||  [[customerPie objectForKey:@"Sales_Value"]isEqual:@""])
            {
                saleValue = 0;
            }
            else 
            {
                saleValue = [[customerPie objectForKey:@"Sales_Value"] intValue];
            }
            achievmentValue = saleValue * 100 /targetValue ; 
            float remainingValue = 100 - achievmentValue ;
            [customerPie setObject:[NSNumber numberWithFloat:achievmentValue] forKey:@"Achieved_Target"];
            [customerPie setObject:[NSNumber numberWithFloat:remainingValue] forKey:@"Remainning_Value"];
            [self.targets replaceObjectAtIndex:i-1 withObject:customerPie];
        }
        
        
        NSLog(@"array after target is %@", [self.targets description]);
        
        
        NSString* categoryStr;
        
        
        if (selectedIndexPath) {
            
            customerPie =[self.targets objectAtIndex:selectedIndexPath.row];
             categoryStr=[[self.targets valueForKey:@"Category"] objectAtIndex:selectedIndexPath.row];

        }
        else
        {
            customerPie = [self.targets objectAtIndex:0];
            categoryStr=[[self.targets valueForKey:@"Category"] objectAtIndex:0];

        }
        
        // open ended custom attribues to customer pie
        
        //fetch value for custom attributes
        
        
        NSLog(@"check static lbl data %@", [staticLblData description]);
        
        
        //append the custom attributes
        
        NSString* queryforCustomAttr=@"Select ";
        
        for (NSInteger i=0; i<staticLblData.count; i++) {
            
           queryforCustomAttr= [queryforCustomAttr stringByAppendingString:[NSString stringWithFormat:@"%@",[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:i] ]];
            
            
            if (i==staticLblData.count-1) {
                
               // queryforCustomAttr=[queryforCustomAttr stringByReplacingOccurrencesOfString:@"," withString:@""];
                queryforCustomAttr=[queryforCustomAttr stringByAppendingString:@" "];

            }
            
            else
            {
                queryforCustomAttr=[queryforCustomAttr stringByAppendingString:@","];

            }
            
        }
        
        
        
        
        queryforCustomAttr=[queryforCustomAttr stringByAppendingString:[NSString stringWithFormat: @"From TBL_Sales_Target_Items where Classification_1='%@' and Classification_2='%@'",categoryStr,[self.customer valueForKey:@"Customer_No"]]];
        
        NSLog(@"check appended str %@",queryforCustomAttr);

        
        NSMutableArray* customAttrData=[[NSMutableArray alloc] init];
        
        
        
        
        for (NSInteger i=0; i<staticLblData.count; i++) {
            
            

               [customAttrData addObject:[[staticLblData valueForKey:@"Column_Name"]objectAtIndex:i]];
            
            
            
            
            
        }
        NSLog(@"custom Attr data is %@", [customAttrData description]);
        
        //fetch data for custom attr
        
        detailedDesc=[[NSMutableDictionary alloc] init];
        
        
        
        NSLog(@"check customer pie %@", [self.customerPie description]);
        
        //populate detailed desc arr
        
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Target_Value"]];
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Sales_Value"]];
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Achieved_Target"]];
        
        
        
        NSLog(@"final detailed desc Array %@", [detailedDescArray description]);
        
        
        //adding custom attributes to detaile desc
        
        
        NSMutableArray* tempAttrArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:queryforCustomAttr];
        
        
        NSLog(@"temp attr array is %@", [tempAttrArry description]);
        
        
        

        
            
            NSString * attrtoAdd=[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:0];
            
        
             for (NSInteger i=0; i<staticLblData.count; i++) {
        
           // [detailedDescArray addObject:[NSString stringWithFormat:@"%@" ,[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:i]]];
                 
                 NSString * attrString=[[tempAttrArry valueForKey:[NSString stringWithFormat:@"%@", [[staticLblData objectAtIndex:i] valueForKey:@"Column_Name"] ]] objectAtIndex:0];
                 
                 
                 [detailedDescArray addObject: attrString];
                 
            
            
        }
    
        
        
        //sample removing nulls in detailed desc
        
        for (NSInteger i=0; i<detailedDescArray.count; i++) {
            
           
            
            if ([detailedDescArray [i] isKindOfClass:[NSNull class]]) {
                
                detailedDescArray [i] =@"N/A";
            }
            
            
            
        }
        
        
        
        NSLog(@"detailed desc array is %@", [detailedDescArray description]);
        
        
       // [detailedDescArray addObject:[[[SWDatabaseManager retrieveManager] fetchDataForQuery:queryforCustomAttr] objectAtIndex:0]];
        

//        [detailedDesc setValue:[self.customerPie valueForKey:@"Target_Value"] forKey:@"Target_Value"];
//        [detailedDesc setValue:[self.customerPie valueForKey:@"Sales_Value"] forKey:@"Sales_Value"];
//        [detailedDesc setValue:[self.customerPie valueForKey:@"Achieved_Target"] forKey:@"Achieved_Target"];

    
        
        
        
        
        
        
        
        
        targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
        NSNumber *num = [NSNumber numberWithInt:targetValue];
        if((num == nil) || [num intValue] == 0)
        {
        }
        else
        {
            [self addSubview:self.backGroundView];
            [self addSubview:self.categoryTableView];
            self.slices = [NSMutableArray arrayWithCapacity:2] ;
        }
        
        
      
            [self loadPieChartWithAchievement:customerPie];


    }
    
    [self.targetTableView reloadData];
    [self.categoryTableView reloadData];
    self.targetTableView.hidden=NO;
    
    t=nil;
}





-(void)reloadTargetData:(NSMutableArray*)targetData

{
    
    self.targets=targetData;
    
    
    //[self setTargets:[t mutableCopy]];
    if ([self.targets count] == 0)
    {
        self.targetTableView.frame = CGRectMake(20, 70, self.frame.size.width-40,self.frame.size.height-80);
        //self.targetTableView.frame=self.bounds;
    }
    else
    {
        
        //removing null values
        
        
        //
        //        for (NSInteger i=0; i<self.targets.count; i++) {
        //
        //
        //            if ([[self.targets objectAtIndex:i]isEqual:[NSNull null]]) {
        //
        //                [self.targets removeObjectAtIndex:i];
        //
        //            }
        //
        //        }
        
        
        
        int targetValue;
        int saleValue;
        
            customerPie = (NSMutableDictionary*)self.targets;
            targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
            if([customerPie objectForKey:@"Sales_Value"]==(id) [NSNull null] ||  [[customerPie objectForKey:@"Sales_Value"]isEqual:@""])
            {
                saleValue = 0;
            }
            else
            {
                saleValue = [[customerPie objectForKey:@"Sales_Value"] intValue];
            }
            achievmentValue = saleValue * 100 /targetValue ;
            float remainingValue = 100 - achievmentValue ;
            [customerPie setObject:[NSNumber numberWithFloat:achievmentValue] forKey:@"Achieved_Target"];
            [customerPie setObject:[NSNumber numberWithFloat:remainingValue] forKey:@"Remainning_Value"];
                 
        
        
        NSLog(@"array after target is %@", [self.targets description]);
        
        
        //customerPie = self.targets ;
        
        // open ended custom attribues to customer pie
        
        //fetch value for custom attributes
        
        
        NSLog(@"check static lbl data %@", [staticLblData description]);
        
        
        //append the custom attributes
        
        NSString* queryforCustomAttr=@"Select ";
        
        for (NSInteger i=0; i<staticLblData.count; i++) {
            
            queryforCustomAttr= [queryforCustomAttr stringByAppendingString:[NSString stringWithFormat:@"%@",[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:i] ]];
            
            
            if (i==staticLblData.count-1) {
                
                // queryforCustomAttr=[queryforCustomAttr stringByReplacingOccurrencesOfString:@"," withString:@""];
                queryforCustomAttr=[queryforCustomAttr stringByAppendingString:@" "];
                
            }
            
            else
            {
                queryforCustomAttr=[queryforCustomAttr stringByAppendingString:@","];
                
            }
            
        }
        
        
        NSString* categoryStr=[self.targets valueForKey:@"Category"] ;
        
        
        queryforCustomAttr=[queryforCustomAttr stringByAppendingString:[NSString stringWithFormat: @"From TBL_Sales_Target_Items where Classification_1='%@' and Classification_2='%@'",categoryStr,[self.customer valueForKey:@"Customer_No"]]];
        
        NSLog(@"check appended str %@",queryforCustomAttr);
        
        
        NSMutableArray* customAttrData=[[NSMutableArray alloc] init];
        
        
        
        
        for (NSInteger i=0; i<staticLblData.count; i++) {
            
            
            
            [customAttrData addObject:[[staticLblData valueForKey:@"Column_Name"]objectAtIndex:i]];
            
            
            
            
            
        }
        NSLog(@"custom Attr data is %@", [customAttrData description]);
        
        //fetch data for custom attr
        
        detailedDesc=[[NSMutableDictionary alloc] init];
        
        detailedDescArray=[[NSMutableArray alloc]init];
        
        NSLog(@"check customer pie %@", [self.customerPie description]);
        
        //populate detailed desc arr
        
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Target_Value"]];
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Sales_Value"]];
        [detailedDescArray addObject:[self.customerPie valueForKey:@"Achieved_Target"]];
        
        
        
        //adding custom attributes to detaile desc
        
        
        NSMutableArray* tempAttrArry=[[SWDatabaseManager retrieveManager] fetchDataForQuery:queryforCustomAttr];
        
        
        NSLog(@"temp attr array is %@", [tempAttrArry description]);
        
        
        
        
        
        
        NSString * attrtoAdd=[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:0];
        
        
        for (NSInteger i=0; i<staticLblData.count; i++) {
            
            // [detailedDescArray addObject:[NSString stringWithFormat:@"%@" ,[[staticLblData valueForKey:@"Column_Name"] objectAtIndex:i]]];
            
            NSString * attrString=[[tempAttrArry valueForKey:[NSString stringWithFormat:@"%@", [[staticLblData objectAtIndex:i] valueForKey:@"Column_Name"] ]] objectAtIndex:0];
            
            
            [detailedDescArray addObject: attrString];
            
            
            
        }
        
        
        
        //sample removing nulls in detailed desc
        
        for (NSInteger i=0; i<detailedDescArray.count; i++) {
            
            
            
            if ([detailedDescArray [i] isKindOfClass:[NSNull class]]) {
                
                detailedDescArray [i] =@"N/A";
            }
            
            
            
        }
        
        
        
        NSLog(@"detailed desc array is %@", [detailedDescArray description]);
        
        
        // [detailedDescArray addObject:[[[SWDatabaseManager retrieveManager] fetchDataForQuery:queryforCustomAttr] objectAtIndex:0]];
        
        
        //        [detailedDesc setValue:[self.customerPie valueForKey:@"Target_Value"] forKey:@"Target_Value"];
        //        [detailedDesc setValue:[self.customerPie valueForKey:@"Sales_Value"] forKey:@"Sales_Value"];
        //        [detailedDesc setValue:[self.customerPie valueForKey:@"Achieved_Target"] forKey:@"Achieved_Target"];
        
        
        
        
        
        
        
        
        
        
        targetValue = [[customerPie objectForKey:@"Target_Value"] intValue];
        NSNumber *num = [NSNumber numberWithInt:targetValue];
        if((num == nil) || [num intValue] == 0)
        {
        }
        else
        {
            [self addSubview:self.backGroundView];
            [self addSubview:self.categoryTableView];
            self.slices = [NSMutableArray arrayWithCapacity:2] ;
        }
        
    }
    
    [self.targetTableView reloadData];
    //[self.categoryTableView reloadData];
    self.targetTableView.hidden=NO;
    
    
    
}

@end
