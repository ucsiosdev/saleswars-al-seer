//
//  LoginViewController.m
//  Salesworx
//
//  Created by Saad Ansari on 12/8/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "LoginViewController.h"
#import "SWFoundation.h"
#import "SWPlatform.h"
#import "SWSessionConstants.h"
#import "UIDevice+IdentifierAddition.h"
#import "SSKeychain.h"
#import "SWDefaults.h"
#import "AppControl.h"
#import "SWNewActivationViewController.h"
#import "Constant.h"
#import "SWAppDelegate.h"
#import <IQKeyboardManager.h>

@interface LoginViewController ()

@end


#define kLoginField 1
#define kPasswordField 2

@implementation LoginViewController
@synthesize txtPassword,txtUserName,btnLogin;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        appControl = [AppControl retrieveSingleton];
        if ([appControl.DASHBOARD_TYPE isEqualToString:@"TARGET-SALES"]) {
            
            CGRect frame = CGRectMake(0, 0, [self.title sizeWithFont:headerTitleFont].width, 44);
            UILabel *label = [[UILabel alloc] initWithFrame:frame];
            label.backgroundColor = [UIColor clearColor];
            label.textColor=[UIColor whiteColor];
            label.font = headerTitleFont;
            label.textAlignment = NSTextAlignmentCenter;
            self.navigationItem.titleView = label;
            label.text = @"SalesWars";
            
            //[self setTitle:@"SalesWars"];
            
        }
        else
        {
            [self setTitle:@"SalesWorx"];
            
        }
        
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    txtPassword.keyboardDistanceFromTextField = 28;
    

    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *avid = [infoDict objectForKey:@"CFBundleShortVersionString"];
    ClientVersion =avid;
    
    txtUserName.font = EditableTextFieldSemiFontOfSize(15);
    txtPassword.font = EditableTextFieldSemiFontOfSize(15);
    
    btnLogin.layer.cornerRadius = 4.0f;
    btnLogin.layer.masksToBounds = YES;
    
    loginView.layer.masksToBounds = NO;
    loginView.layer.shadowOffset = CGSizeMake(0.0f,0.0f);
    loginView.layer.shadowColor = [UIColor colorWithRed:210.f/255.f green:210.f/255.f blue:210.f/255.f alpha:1.f].CGColor;
    loginView.layer.shadowRadius = 12.0f;
    loginView.layer.shadowOpacity = 10.0f;
    loginView.layer.cornerRadius = 4.0;
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self doAnimationOnView];
    
    txtUserName.text = @"";
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void)doAnimationOnView{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self logoAnimationFromBottomToTop];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self loginViewAnimationFromBottomToTop];
    });
}

-(void) logoAnimationFromBottomToTop {
    
    
    [UIView animateWithDuration:1 animations:^{
        appLogoHorizontalConstraint.constant = -265;
        appLogoHightConstraint.constant = appImageView.frame.size.height;
        appLogoWidthConstraint.constant = appImageView.frame.size.width;
        appImageView.contentMode = UIViewContentModeScaleAspectFit;
        appImageView.clipsToBounds = YES;
        [self.view layoutIfNeeded];
    }];
}

-(void) loginViewAnimationFromBottomToTop  {
    
    [UIView animateWithDuration:1 animations:^{
        
        loginView.frame = CGRectMake(loginView.frame.origin.x, loginView.frame.origin.y-570, 350, 400); //570 - loginView.frame.origin.y;
        loginView.frame = loginView.frame;
        loginViewHorizontalConstraint.constant = 0;
        
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([SWDefaults username].length > 0)
    {
        login = [SWDefaults username];
        password = [SWDefaults password];
    }
    
   
    
    NSString * test=[SSKeychain passwordForService:@"registereduser" account:@"user"];
    
    NSLog(@"key chain result is %@", test);
    
    if ([test isEqualToString:@"registereduser"])
    {
        txtUserName.text=@"";
        txtPassword.text= @"";
        btnLogin.enabled=NO;
        
        login=@"";
        password=@"";
       
        
    }
    
    else{
        
   
    //txtUserName.text = login;
    //txtPassword.text = password;
    }
    if (login.length > 0 && password.length > 0)
    {
        btnLogin.enabled=YES;
    }
    
   /*
    if (DEBUG) {
        
    login=@"rashid";
    password=@"password";
    }*/
    
    login=@"";
    password=@"";
    btnLogin.enabled=YES;
    
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    /** disabling the background sync on log out*/
    [[NSNotificationCenter defaultCenter] postNotificationName:KDISABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
}
- (IBAction)activateNewUser:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    [alertTextField setTextAlignment:NSTextAlignmentCenter];
    alert.tag=1000;
    [alert show];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger maxLength = 50;
    if (textField == txtUserName){
        maxLength = MAX_LENGTH_USERNAME;
    }else if (textField == txtPassword){
        maxLength = MAX_LENGTH_PASSWORD;
    }
    
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= maxLength;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    if (textField == txtUserName) {
        login=textField.text;
    } else {
        password=textField.text;
    }
    
    if (login.length > 0 && password.length > 0)
    {
        btnLogin.enabled=YES;
    }
    
    return YES;
}
-(IBAction)buttonAction:(id)sender
{
    [txtPassword resignFirstResponder];
    [txtUserName resignFirstResponder];
    
//    login = @"ff002";
//    password = @"password";
    
    if (login.length > 0 && password.length > 0) {
        // login
        NSString *temp  = [[SWDatabaseManager retrieveManager] verifyLogin:login andPassword:password];
        
        if ([temp isEqualToString:@"Done"])
        {
            
            
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"welcomeDisplayed"];
            
            NSArray* arr=[[NSArray alloc]init];
            
            arr=[[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat: @"select * from TBL_User"]];
            
            [SWDefaults setUsername:login];
            [SWDefaults setUsernameForActivate:login];
            
            [SWDefaults setPassword:password];
            [SWDefaults setPasswordForActivate:password];
            
            SynchroniseViewController *syncObject = [[SynchroniseViewController alloc] init];
            if([syncObject licenseVerification])
            {
                NSMutableArray *arrPendingFeedback = [[SWDatabaseManager retrieveManager]fetchDataForQuery:@"SELECT * FROM TBL_FeedBack_Request Where Status = 'N' and Expiry_Date > datetime('now', 'localtime')"];
                if (arrPendingFeedback.count > 0) {
                    
                    UILocalNotification *locNotification = [[UILocalNotification alloc]init];
                    locNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
                    locNotification.alertTitle = @"Pending Feedback";
                    locNotification.alertBody = @"There are some pending feedback, please complete them.";
                    locNotification.timeZone = [NSTimeZone defaultTimeZone];
                    locNotification.soundName = UILocalNotificationDefaultSoundName;
                    locNotification.applicationIconBadgeNumber = 1;
                    [[UIApplication sharedApplication] scheduleLocalNotification:locNotification];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:kSessionLoginDoneNotification object:nil];
                
                if([appControl.ENABLE_BACKGROUND_SYNC isEqualToString:KAppControlsYESCode])
                {
                    // Post a notification to enableBackGroundSync
                    [[NSNotificationCenter defaultCenter] postNotificationName:KENABLE_V_TRX_BS_NOTIFICATIONNameStr object:nil];
                }
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"License validation error", nil) andMessage:@"You license has been expired or contact your administrator for further assistance" withController:self];
            }
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:temp withController:self];
        }
    }

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1000) {
        //1 for OK
        if (buttonIndex==1) {
            NSString *name = [alertView textFieldAtIndex:0].text;
            if ([name isEqualToString:@"Alseer"]) {
                
                SWNewActivationViewController *newActivationViewController = [[SWNewActivationViewController alloc] init] ;
                [self.navigationController pushViewController:newActivationViewController animated:YES];
            }
            else
            {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Invalid password" andMessage:@"Please try again" withController:self];
            }
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Text Field Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField==txtUserName) {
        
        [txtUserName resignFirstResponder];
        //[txtPassword becomeFirstResponder];
    }
}

- (IBAction)sendLogsTapped:(id)sender {
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        [[mailController navigationBar] setTintColor: [UIColor blackColor]];
        [mailController setSubject:@"SalesWars Device Logs"];
        [mailController setMessageBody:@"Hi,\n Please find the attached device logs for SalesWorx." isHTML:NO];
        [mailController setToRecipients:@[@"ucssupport@ucssolutions.com"]];
        
        NSURL* filePath= [[[NSFileManager defaultManager]URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask]lastObject];
        NSString * documentDir = [filePath path] ;
        NSData *sqliteData = [NSData dataWithContentsOfFile:[documentDir stringByAppendingPathComponent:@"swx.sqlite"]];
        [mailController addAttachmentData:sqliteData mimeType:@"text/plain" fileName:@"swx.sqlite"];
        [self presentViewController:mailController animated:YES completion:nil];
    }
}
#pragma mark Mail Composer Delegate


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"Log email sent.");
            
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
