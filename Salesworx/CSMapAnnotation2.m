//
//  CSMapAnnotation.m
//  mapLines
//
//  Created by Craig on 5/15/09.
//  Copyright 2009 Craig Spitzkoff. All rights reserved.
//

#import "CSMapAnnotation2.h"
#import "Place2.h"

@implementation CSMapAnnotation2

@synthesize coordinate     = _coordinate;
@synthesize userData       = _userData;
@synthesize url            = _url;
@synthesize place;
-(id) initWithPlace: (Place2*) p 
{
	self = [super init];
	if (self != nil) {
		_coordinate.latitude = p.latitude;
		_coordinate.longitude = p.longitude;
		self.place = p;
	}
	return self;
}

- (NSString *)subtitle
{
	return self.place.otherDetail;
}
- (NSString *)title
{
	return self.place.name;
}


@end