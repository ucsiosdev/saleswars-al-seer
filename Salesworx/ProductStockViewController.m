//
//  ProductStockViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ProductStockViewController.h"
#import "StockInfoSalesHistoryViewController.h"


@interface ProductStockViewController ()

@end

@implementation ProductStockViewController


@synthesize target,orderQuantity;
@synthesize action;

@synthesize delegate;

- (id)initWithProduct:(NSDictionary *)p {
    self = [super init];
    
    if (self) {
        product=[NSDictionary dictionaryWithDictionary:p];
        [self setTitle:NSLocalizedString(@"Stock Info", nil)];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0x4790D2);
        self.navigationController.navigationBar.translucent = NO;
        
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x4790D2);
        self.navigationController.toolbar.translucent = NO;
        
        self.extendedLayoutIncludesOpaqueBars = TRUE;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.edgesForExtendedLayout = UIRectEdgeBottom;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        
        NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],NSForegroundColorAttributeName,nil];
        
        self.navigationController.navigationBar.titleTextAttributes = textAttributes;
        self.navigationController.toolbar.tintColor = [UIColor whiteColor];
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        
    }
    else
    {
        [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
        [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    }
    [Flurry logEvent:@"Product Stock View"];
    
    stockView=[[ProductStockView alloc] initWithProduct:product ];
    [stockView setFrame:self.view.bounds];
    [stockView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [stockView setEditMode:YES];
    [stockView setDelegate:self];
    [stockView loadStock];
    
    [self.view addSubview:stockView];
    AppControl *appControl = [AppControl retrieveSingleton];
    
    isLotSelection = appControl.ALLOW_LOT_SELECTION;
    // if([[SWDefaults appControl] count]!=0)//ALLO_LOT_SELECTION
    if(![isLotSelection isEqualToString:@"Y"])
    {
        stockView.gridView.userInteractionEnabled = NO;
    }
    //Labels
    countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [countLabel setText:@""];
    [countLabel setTextAlignment:NSTextAlignmentCenter];
    [countLabel setBackgroundColor:[UIColor clearColor]];
    [countLabel setTextColor:[UIColor whiteColor]];
    [countLabel setFont:LightFontOfSize(14.0f)];
    [countLabel setShadowColor:[UIColor blackColor]];
    [countLabel setShadowOffset:CGSizeMake(0, -1)];
    
    orderedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [orderedLabel setText:@""];
    [orderedLabel setTextAlignment:NSTextAlignmentCenter];
    [orderedLabel setBackgroundColor:[UIColor clearColor]];
    [orderedLabel setTextColor:[UIColor whiteColor]];
    [orderedLabel setFont:LightFontOfSize(14.0f)];
    [orderedLabel setShadowColor:[UIColor blackColor]];
    [orderedLabel setShadowOffset:CGSizeMake(0, -1)];
    
    allocatedLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 25)] ;
    [allocatedLabel setText:@""];
    [allocatedLabel setTextAlignment:NSTextAlignmentCenter];
    [allocatedLabel setBackgroundColor:[UIColor clearColor]];
    [allocatedLabel setTextColor:[UIColor whiteColor]];
    [allocatedLabel setFont:LightFontOfSize(14.0f)];
    [allocatedLabel setShadowColor:[UIColor blackColor]];
    [allocatedLabel setShadowOffset:CGSizeMake(0, -1)];
    
    
    
    //priceString = [NSString stringWithFormat:priceString, [product currencyStringForKey:@"Net_Price"], [product currencyStringForKey:@"List_Price"]];
    
    footerView = [[GroupSectionFooterView alloc] initWithFrame:CGRectMake(0, 0, stockView.bounds.size.width, 72)] ;
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    
    
    NSString* priceString=[NSString stringWithFormat:@"Available Stock : %d", totalGoods];

    
    footerView.titleLabel.font=headerFont;
    
    [footerView.titleLabel setText:[NSString stringWithFormat:@"Available Stock : %d",totalGoods]];
    [stockView.gridView.tableView setTableFooterView:footerView];
    
    
    float requiredHeightforProductScrollView=stockView.gridView.tableView.contentSize.height+footerView.frame.size.height;
    
    
    
    
    [[NSUserDefaults standardUserDefaults]setFloat:requiredHeightforProductScrollView forKey:@"StockScrollViewHeight"];
    
    
    
    
    StockInfoSalesHistoryViewController * stockInofVC=[[StockInfoSalesHistoryViewController alloc]init];
    stockInofVC.stockInfoSize=requiredHeightforProductScrollView;
    
    
    
    
    
    filterButton = [[UIButton alloc] init] ;
    //filterButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language isEqualToString:@"ar"])
    {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
    }
    else
    {
        [filterButton setBackgroundImage:[UIImage imageNamed:@"green_button.png" cache:NO] forState:UIControlStateNormal];
    }
//    [filterButton setFrame:CGRectMake(self.view.frame.size.width-120,12, 100, 32)];
    
    
    NSLog(@"product stock view controller frame %@", NSStringFromCGRect(self.view.frame));
    
    
    [filterButton setFrame:CGRectMake(550,0, 200, 52)];
    filterButton.backgroundColor=[UIColor redColor];

    [filterButton addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
    [filterButton bringSubviewToFront:self.view];
    [filterButton setTitle:NSLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    filterButton.titleLabel.font = BoldSemiFontOfSize(14.0f);
    //filterButton.enabled = NO;
    [self.view addSubview:filterButton];
    
    // [self.gridView.tableView setTableHeaderView:self.customerHeaderView];
    //[self addSubview:self.customerHeaderView];
    
    stockView.gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self
                                                                              action:@selector(closeOrder)] ];
    
    
}
- (void)closeOrder {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    
}

- (void)save:(id)sender {
    
    [self.view endEditing:YES];
    NSMutableArray *stockAllocation = [NSMutableArray array];
    
    if (stockView.invalidAllocation==YES) {
        
        [SWDefaults showAlertAfterHidingKeyBoard:@"Invalid Allocation" andMessage:@"Allocated quantity cannot be greater than ordered quantity" withController:self];
    }
    else {
        
        for (NSDictionary *row in stockView.items) {
            
            if ([row stringForKey:@"Allocated"].length > 0) {
                [stockAllocation addObject:row];
            }
        }
        
        NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:product];
        [p setValue:stockAllocation forKey:@"StockAllocation"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.action withObject:p];
#pragma clang diagnostic pop
        
        if (delegate) {
            [delegate didTapSaveButton];
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)updateAllocation {
    orderedQuantity = 0;
    //BOOL shouldDisplaySaveButton = NO;
    for (NSDictionary *row in stockView.items) {
        
        if ([row objectForKey:@"Allocated"]) {
            orderedQuantity = orderedQuantity + [[row objectForKey:@"Allocated"] intValue];
        } else
        {
            //shouldDisplaySaveButton = NO;
        }
    }
    if (orderQuantity == orderedQuantity && !orderQuantity==0) {
        filterButton.enabled = YES;
        
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(save:)]  animated:YES];
    } else {
     //   filterButton.enabled = NO;
        
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
    }
    [allocatedLabel setText:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Allocated Quantity", nil) ,orderedQuantity]];
    
    
    
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    
    
    NSString* priceString=[NSString stringWithFormat:@"Available Stock : %d", totalGoods];
    
    footerView.titleLabel.font=headerFont;
    
    [footerView.titleLabel setText:priceString];
    
    
    
    
}
#pragma mark Product Stock View Delegate
- (void)productStockViewLoaded {
    NSArray *selectedItems = [product objectForKey:@"StockAllocation"];
    
    if (selectedItems.count > 0) {
        for (int i = 0; i < stockView.items.count; i++) {
            @autoreleasepool{
                NSDictionary *row = [stockView.items objectAtIndex:i];
                
                for (NSDictionary *selectedRow in selectedItems) {
                    if ([[row stringForKey:@"Stock_ID"] isEqualToString:[selectedRow stringForKey:@"Stock_ID"]]) {
                        [row setValue:[selectedRow stringForKey:@"Allocated"] forKey:@"Allocated"];
                        [stockView.items replaceObjectAtIndex:i withObject:row];
                        break;
                    }
                }
            }
        }
        
    }
    Singleton *single = [Singleton retrieveSingleton];
    
    

    
    for (NSInteger i=0; i<stockView.items.count; i++) {
        
        
        NSDictionary *data = [stockView.items objectAtIndex:i];

        
        
        NSString* selectedUOMReference=[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedUOMRef"];
        
        
        if ([selectedUOMReference isEqualToString:@"CS"] ||[[product stringForKey:@"Order_Quantity_UOM"] isEqualToString:@"CS"]) {

            
            
            NSString* itemCodeforUOM=[product stringForKey:@"Item_Code"];
            NSString* selectedUom=selectedUOMReference;
            
            
            
            NSString *trimmedString = [selectedUom stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            
            

            
            
            
            NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@' ",itemCodeforUOM,trimmedString];
            
            NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
            
            NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
            
            
            
            if (consersionArr.count>0) {
                
                NSInteger lotQty=[[data stringForKey:@"Lot_Qty"] integerValue];
                
                NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
                
                
                NSInteger updatedLotQty= lotQty/conversion;
                
                //[mainProductDict setObject:[NSString stringWithFormat:@"%d",updatedLotQty] forKey:@"Lot_Qty"];
                
                totalGoods = totalGoods + updatedLotQty;
                
                
                
                
                
                
//                
//                NSInteger lotQtyVal=[[data stringForKey:@"Lot_Qty"] integerValue];
//                
//                NSInteger conversionVal=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
//                
//                
//                NSInteger updatedLotQtyVal= lotQtyVal/conversionVal;
//            
//                
//                avblStockVal=avblStockVal+updatedLotQtyVal;
        }
        
        
        
    }
        
        else
        {
            NSInteger lotQtyEA=[[data valueForKey:@"Lot_Qty"] integerValue];
            
            totalGoods=totalGoods+lotQtyEA;
            
            
            
        }
 
        }
    
    
    

    
    
    /**
    
    for (NSDictionary *row in stockView.items) {
        
        
        NSLog(@"total in footer %@", [stockView.items description]);
        
        int qty = [[row objectForKey:@"Lot_Qty"] intValue];
        
        
        
        
        //uom support for footer
        
        
        NSString* selectedUOMReference=[[NSUserDefaults standardUserDefaults]valueForKey:@"SelectedUOMRef"];
        
        
        if ([selectedUOMReference isEqualToString:@"CS"]) {
            
            
            
            NSString* itemCodeforUOM=[product stringForKey:@"Item_Code"];
            NSString* selectedUom=selectedUOMReference;
            
            NSString* conversionQry=[NSString stringWithFormat:@"SELECT Conversion from TBL_Item_Uom where Item_Code='%@' and Item_UOM='%@' ",itemCodeforUOM,selectedUom];
            
            NSMutableArray* consersionArr=[[SWDatabaseManager retrieveManager] fetchDataForQuery:conversionQry];
            
            NSLog(@"check the conversion %@", [consersionArr valueForKey:@"Conversion"]);
            
            
            
            if (consersionArr.count>0) {
                
                
                
                
                NSInteger lotQty=[[product stringForKey:@"Lot_Qty"] integerValue];
                
                NSInteger conversion=[[[consersionArr valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
                
                
                
                NSInteger updatedLotQty= lotQty/conversion;
                
                //[mainProductDict setObject:[NSString stringWi
                
        
        
                totalGoods = totalGoods + updatedLotQty;
                
            }
    }
     
     **/
    
    single.stockAvailble = [NSString stringWithFormat:@"%d",totalGoods];
    
    [countLabel setText:[NSString stringWithFormat:@"%@ : %d",NSLocalizedString(@"Available Stock", nil),totalGoods]];
    
//    NSString *priceString =[NSString stringWithFormat:@"%@ : %d \n %@ : %d \n %@ : %d",NSLocalizedString(@"Available Stock", nil), totalGoods,NSLocalizedString(@"Order Quantity", nil), orderQuantity,NSLocalizedString(@"Allocated Quantity", nil),orderedQuantity] ;
    
    
    
    NSString* priceString=[NSString stringWithFormat:@"Available Stock : %d", totalGoods];
    footerView.titleLabel.font=textFont;
    
    
    [footerView.titleLabel setText:[NSString stringWithFormat:@"Available Stock : %d",totalGoods]];
    
    [self updateAllocation];
}


- (void)productStockViewDidFinishedEditingForRowIndex:(int)rowIndex {
    [self updateAllocation];
}



#pragma mark UIKeyboard Notifications
- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    toolbarFrame.origin.y = self.view.bounds.size.height + 44;
    [UIView animateWithDuration:animationDuration animations:^ {
        //  [self.gridView setFrame:self.view.bounds];
        [self.navigationController.toolbar setFrame:toolbarFrame];
        
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyboardInfo = [notification userInfo];
    
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    float keyboardHeight = MIN(keyboardFrameBeginRect.size.width, keyboardFrameBeginRect.size.height);
    CGRect toolbarFrame = self.navigationController.toolbar.frame;
    toolbarFrame.origin.y = self.view.bounds.size.height + 44 - keyboardHeight;
    
    float animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^ {
        CGRect f = self.view.bounds;
        f.size.height = f.size.height - keyboardHeight;
        // [self.ta setFrame:f];
        [self.navigationController.toolbar setFrame:toolbarFrame];
    }];
}
-(void)viewDidUnload
{
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
