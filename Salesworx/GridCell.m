//
//  GridCell.m
//  SWFoundation
//
//  Created by Irfan Bashir on 6/28/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "GridCell.h"
#import "SWFoundation.h"
#import "SWDefaults.h"
@implementation GridCell

@synthesize columnWidths;
@synthesize labels,labelView;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier columnWidths:(NSArray *)widths andInputColumnIndex:(int)columnIndex{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setColumnWidths:widths];
        [self setLabels:[NSMutableArray array]];
        inputColumnIndex = columnIndex;
        
        for (int i = 0; i < self.columnWidths.count; i++) {
            @autoreleasepool{
            if (inputColumnIndex == i) {
                if (textFieldView) {
                    textFieldView=nil;
                }
                textFieldView = [[UITextField alloc] initWithFrame:CGRectZero] ;
                [textFieldView setDelegate:self];
                [textFieldView setFont:kFontWeblySleekSemiBold(16.0f)];
                [textFieldView setKeyboardType:UIKeyboardTypeNumberPad];
                [textFieldView setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
                [self.labels addObject:textFieldView];
                labelView.textAlignment = NSTextAlignmentRight;
                [self.contentView addSubview:textFieldView];
            } else {
                if (labelView) {
                    labelView=nil;
                }
                labelView = [[UILabel alloc] initWithFrame:CGRectZero] ;
                [labelView setBackgroundColor:[UIColor clearColor]];
                [labelView setTag:i];
                [labelView setFont:kFontWeblySleekSemiBold(16.0f)];
                [labelView setTextColor:[UIColor redColor]];
                [labelView setHighlightedTextColor:[UIColor whiteColor]];
                [self.labels addObject:labelView];
                labelView.textAlignment = NSTextAlignmentRight;
                [self.contentView addSubview:labelView];
            }
        }
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (selected)
    {
        if (inputColumnIndex != -1 && inputColumnIndex < self.labels.count) {
            UITextField *textField = (UITextField *)[self.labels objectAtIndex:inputColumnIndex];
            [textField setTextColor:[UIColor whiteColor]];
            if (![textField isFirstResponder]) {
                [textField becomeFirstResponder];
            }
        }
    } else
    {
        if (inputColumnIndex != -1 && inputColumnIndex < self.labels.count) {
            UITextField *textField = (UITextField *)[self.labels objectAtIndex:inputColumnIndex];
            [textField setTextColor:[UIColor blackColor]];
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    float columnWidth = self.bounds.size.width / self.columnWidths.count;
    float x = 0;
    for (int j = 0; j < self.columnWidths.count; j ++) {
        @autoreleasepool{
        if (self.columnWidths) {
            float cw = [[self.columnWidths objectAtIndex:j] floatValue];
            columnWidth = self.bounds.size.width * ( cw / 100);
        }
        
        if (inputColumnIndex == j) {
            UITextField *textField = (UITextField *)[self.labels objectAtIndex:j];
            CGRect r = CGRectMake(x, 0, columnWidth, 44);
            [textField setFrame:CGRectInset(r, 5, 0)];
        } else {
            UILabel *label = (UILabel *)[self.labels objectAtIndex:j];
            CGRect r = CGRectMake(x, 0, columnWidth, 44);
            [label setFrame:CGRectInset(r, 5, 5)];
        }
        
        
        x = x + columnWidth;
    }
    }
}

- (void)setText:(NSString *)text forColumn:(int)column isTextCenterAlign:(BOOL)isCenterAlign {
    if (inputColumnIndex == column) {
        UITextField *textField = (UITextField *)[self.labels objectAtIndex:column];
        
        
        [textField setTextColor:[UIColor colorWithRed:44.0/255.0 green:57.0/255.0 blue:74.0/255.0 alpha:1.0]];

        [textField setText:text];
//        [textField setFont:LightFontOfSize(12.0f)];
        [textField setFont:kFontWeblySleekSemiBold(16)];
        
        if (isCenterAlign){
            textField.textAlignment = NSTextAlignmentRight;
        }else {
            textField.textAlignment =  NSTextAlignmentLeft;
        }
       
        

    } else {
        UILabel *label = (UILabel *)[self.labels objectAtIndex:column];
        
        [label setTextColor:[UIColor colorWithRed:44.0/255.0 green:57.0/255.0 blue:74.0/255.0 alpha:1.0]];

        [label setText:text];
        //test grid change font color on highlight
        //[label setHighlightedTextColor:[UIColor blueColor]];
//        [label setFont:BoldFontOfSize(12.0f)];
        [label setFont:kFontWeblySleekSemiBold(16)];
       
        if (isCenterAlign){
            label.textAlignment = NSTextAlignmentRight;
        }else {
            label.textAlignment =  NSTextAlignmentLeft;
        }
        
        /*
        if (column==0){
            label.backgroundColor =[UIColor greenColor];
        }else if (column==1){
            label.backgroundColor =[UIColor grayColor];
        }
        else if (column==2){
            label.backgroundColor =[UIColor redColor];
        }
        else if (column==3){
            label.backgroundColor =[UIColor yellowColor];
        }
        else if (column==4){
            label.backgroundColor =[UIColor blueColor];
        }
        */

    }
    
    
}


#pragma mark UITableViewCell+Responder override
- (void)resignResponder {
    if (inputColumnIndex != -1) {
        UITextField *textField = (UITextField *)[self.labels objectAtIndex:inputColumnIndex];
        if ([textField isFirstResponder]) {
            [textField resignFirstResponder];
        }
    }
}

#pragma mark UITextField Delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self setSelected:NO animated:YES];
    if ([self.delegate respondsToSelector:@selector(editableCell:didUpdateValue:forKey:)]) {
        [self.delegate editableCell:self didUpdateValue:textField.text forKey:self.key];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setSelected:YES animated:YES];
    if ([self.delegate respondsToSelector:@selector(editableCellDidStartUpdate:)]) {
        [self.delegate editableCellDidStartUpdate:self];
    }
}
@end
