//
//  CustomerSalesViewController.m
//  Salesworx
//
//  Created by msaad on 6/3/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "CustomerSalesViewController.h"

@interface CustomerSalesViewController ()

@end

@implementation CustomerSalesViewController
- (id)initWithCustomer:(NSMutableDictionary *)cust andProduct:(NSMutableDictionary *)prod;
{
    self = [super init];
    if (self)
    {
        [self setTitle:NSLocalizedString(@"Sales History", nil)];
        customer = [NSMutableDictionary dictionaryWithDictionary:cust];
        product = [NSMutableDictionary dictionaryWithDictionary:prod];
        productSalesArray = [NSMutableArray array];
        bucketSize=30;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                                               style:UIBarButtonItemStylePlain
                                                                              target:self
                                                                              action:@selector(closeOrder)] ];
    
    gridView.tableView.cellLayoutMarginsFollowReadableWidth = NO;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getServiceDidGetProductSales:[[SWDatabaseManager retrieveManager] dbGetCustomerProductSalesWithCustomerID:[customer stringForKey:@"Ship_Customer_ID"] andProductId:[product stringForKey:@"ItemID"]]];
}
-(void)updateViewDataWithProductID:(NSMutableDictionary *)productID
{
    [productSalesArray removeAllObjects];
    [productHeaderView removeFromSuperview];
    [gridView removeFromSuperview];
    product = nil;
    product = [NSMutableDictionary dictionaryWithDictionary:productID];

    [self getServiceDidGetProductSales:[[SWDatabaseManager retrieveManager] dbGetCustomerProductSalesWithCustomerID:[customer stringForKey:@"Ship_Customer_ID"] andProductId:[product stringForKey:@"ItemID"]]];
    
}
- (void)closeOrder {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark GridView DataSource
- (int)numberOfRowsInGrid:(GridView *)gridView {
    return productSalesArray.count;
}

- (int)numberOfColumnsInGrid:(GridView *)gridView {
    return 2;
}

- (float)gridView:(GridView *)gridView widthForColumn:(int)columnIndex {
    switch (columnIndex) {
        case 0:
            return 70.0f;
            break;
        case 1:
            return 31.0f;
            break;
        default:
            break;
    }
    return 20.0f;
}

- (NSString *)gridView:(GridView *)gridView titleForColumn:(int)column {
    NSString *title = @"";
    switch (column)
    {
        case 0:
            title = [NSString  stringWithFormat:@"    %@",NSLocalizedString(@"Period", nil)];
            break;
        case 1:
            title = NSLocalizedString(@"Sales", nil);
            break;
        default:
            break;
    }
    return title;
}

- (NSString *)gridView:(GridView *)gridView textforRow:(int)row andColumn:(int)column {
    NSString *text = @"";
    NSDictionary *data = [productSalesArray objectAtIndex:row];
    int days=0;
    switch (column) {
        case 0:
            if(row==0)
            {
                text = @"    Current Month";
            }
            else if(row==[productSalesArray count]-1)
            {
                text = @"    Prior";
            }
            else
            {
                days = bucketSize * [[data stringForKey:@"Index"] intValue];
                text = [NSString stringWithFormat:@"    %d days ago",days];
            }
            break;
        case 1:
            if(row==[productSalesArray count]-1)
            {
                double price=0;
                for(int i = 0; i < productSalesArray.count-1; i++)
                {
                    NSDictionary *row1 = [productSalesArray objectAtIndex:i];
                    price = price + [[row1 stringForKey:@"Quantity"] doubleValue];
                }
                double finalValue = [[data stringForKey:@"Quantity"] doubleValue] - price;
                text = [NSString stringWithFormat:@"%.0f",finalValue];
            }
            else
            {
                text = [data stringForKey:@"Quantity"];
            }
        default:
            break;
    }
    
    return text;
}

#pragma mark GridView Delegate
- (void)getServiceDidGetProductSales:(NSArray *)list
{
    productHeaderView=nil;
    productHeaderView = [[ProductHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    productHeaderView.headingLabel.font=headerFont;
    [productHeaderView.headingLabel setText:@"Sales History"];
    productHeaderView.backgroundColor=[UIColor colorWithRed:(217.0/255.0) green:(223.0/255.0) blue:(231.0/255.0) alpha:1];
    
    gridView=nil;
    gridView=[[GridView alloc] initWithFrame:CGRectMake(0, 45, 320 , 460) ];
    [gridView setDataSource:self];
    [gridView setDelegate:self];
    
    gridView.tableView.scrollEnabled=YES;
    [gridView setShouldAllowDeleting:YES];
    [gridView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    if(list.count !=0)
    {
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: [list objectAtIndex:0]];
        
        NSMutableDictionary *BK0Dict = [NSMutableDictionary dictionary];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Bns"] forKey:@"Bonus"];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Qty"] forKey:@"Quantity"];
        [BK0Dict setValue:[dic stringForKey:@"Bk0_Amt"] forKey:@"Amount"];
        [BK0Dict setValue:@"0"  forKey:@"Index"];
        
        NSMutableDictionary *BK1Dict = [NSMutableDictionary dictionary];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Bns"] forKey:@"Bonus"];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Qty"] forKey:@"Quantity"];
        [BK1Dict setValue:[dic stringForKey:@"Bk1_Amt"] forKey:@"Amount"];
        [BK1Dict setValue:@"1"  forKey:@"Index"];
        
        NSMutableDictionary *BK2Dict = [NSMutableDictionary dictionary];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Bns"] forKey:@"Bonus"];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Qty"] forKey:@"Quantity"];
        [BK2Dict setValue:[dic stringForKey:@"Bk2_Amt"] forKey:@"Amount"];
        [BK2Dict setValue:@"2"  forKey:@"Index"];
        
        NSMutableDictionary *BK3Dict = [NSMutableDictionary dictionary];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Bns"] forKey:@"Bonus"];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Qty"] forKey:@"Quantity"];
        [BK3Dict setValue:[dic stringForKey:@"Bk3_Amt"] forKey:@"Amount"];
        [BK3Dict setValue:@"3"  forKey:@"Index"];
        
        NSMutableDictionary *BK4Dict = [NSMutableDictionary dictionary];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Bns"] forKey:@"Bonus"];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Qty"] forKey:@"Quantity"];
        [BK4Dict setValue:[dic stringForKey:@"Bk4_Amt"] forKey:@"Amount"];
        [BK4Dict setValue:@"4"  forKey:@"Index"];
        
        NSMutableDictionary *BK5Dict = [NSMutableDictionary dictionary];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Bns"] forKey:@"Bonus"];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Qty"] forKey:@"Quantity"];
        [BK5Dict setValue:[dic stringForKey:@"Bk5_Amt"] forKey:@"Amount"];
        [BK5Dict setValue:@"5"  forKey:@"Index"];
        
        NSMutableDictionary *BKBDict = [NSMutableDictionary dictionary];
        [BKBDict setValue:[dic stringForKey:@"BkB_Bns"] forKey:@"Bonus"];
        [BKBDict setValue:[dic stringForKey:@"BkB_Qty"] forKey:@"Quantity"];
        [BKBDict setValue:[dic stringForKey:@"BkB_Amt"] forKey:@"Amount"];
        [BKBDict setValue:@"6"  forKey:@"Index"];
        
        [productSalesArray addObject:BK0Dict];
        [productSalesArray addObject:BK1Dict];
        [productSalesArray addObject:BK2Dict];
        [productSalesArray addObject:BK3Dict];
        [productSalesArray addObject:BK4Dict];
        [productSalesArray addObject:BK5Dict];
        [productSalesArray addObject:BKBDict];
        
        discountLable=nil;
        discountLable = [[UILabel alloc] initWithFrame:CGRectMake(600,12, 400, 32)] ;
        [discountLable setTextAlignment:NSTextAlignmentCenter];
        [discountLable setBackgroundColor:[UIColor clearColor]];
        [discountLable setTextColor:[UIColor darkGrayColor]];
        [discountLable setFont:RegularFontOfSize(18.0)];
        
        AppControl *appControl = [AppControl retrieveSingleton];
        double avgSales = [[dic stringForKey:@"BkB_Amt"] doubleValue] / [appControl.SD_DURATION_MON doubleValue];
        NSString *resultString = [NSString stringWithFormat:@"Average Sales Amount: %@",[NSString currencyStringWithDouble:avgSales]];
        [discountLable setText:resultString];
        [productHeaderView addSubview:discountLable];
    }
    [self.view addSubview:productHeaderView];
    [self.view addSubview:gridView];
    list=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
