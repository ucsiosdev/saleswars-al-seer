//
//  AlSeerOrderAdditionalInfoViewController.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"
#define kImagesFolder @"Signature"
#import "SignatureViewController.h"
#import "AlSeerSignatureViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface AlSeerOrderAdditionalInfoViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIPopoverControllerDelegate,UIPageViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
    NSMutableDictionary* orderAdditionalInfoDict;
    
    IBOutlet UIView *customerDetailView;
    IBOutlet UIView *commentDetailView;
    IBOutlet UIView *signatureDetailView;
    NSInteger *numberOfRowsInaPage;
    NSInteger *StartIndexInCurrentPage;
}
@property (strong, nonatomic) IBOutlet UILabel *customerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerIDLbl;
@property (strong, nonatomic) IBOutlet UILabel *customerAvailableBalanceLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtfldCustomerDocRef;
@property (strong, nonatomic) IBOutlet UITextView *commentsTxtView;
@property (strong, nonatomic) IBOutlet UITextField *nameTxtFld;
@property (strong, nonatomic) IBOutlet UIView *customSignatureView;
- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)confirmButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderItems;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalOrderValue;
@property (nonatomic) NSInteger pageNumber;


@property(strong,nonatomic) AlSeerSignatureViewController* signVc;

@property (strong, nonatomic) IBOutlet UILabel *reqShipDateLbl;

@property(nonatomic,strong)NSString *performaOrderRef;
@property(strong,nonatomic)NSMutableArray * warehouseDataArray;
@property(strong,nonatomic)NSMutableArray * itemsSalesOrder;
@property (strong, nonatomic) IBOutlet UITableView *orderItemTableView;
@property (strong, nonatomic) IBOutlet UIView *orderItemView;
@property (strong, nonatomic) IBOutlet UILabel *orderItemCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalOrderValueLabel;


@property(strong,nonatomic)NSMutableDictionary* salesOrderDict,*customerDict;

@end
