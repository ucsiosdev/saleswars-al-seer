//
//  OrderHistoryPopUplTableViewCell.h
//  SalesWorx
//
//  Created by Unique Computer Systems on 6/25/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

@interface OrderHistoryPopUplTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *deliveryNumberLbl;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *invoiceNumberLbl;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *tripIdNumberLbl;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *statusLbl;

@property (strong, nonatomic) IBOutlet SalesWorxSingleLineLabel *statusBgView;


@end
