//
//  SWVisitManager.h
//  SWPlatform
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SWPlatform.h"


@interface SWVisitManager : SWViewController {
    
    BOOL  visitStarted;

    NSMutableDictionary *customer;
    UIViewController *parentViewController;
    NSString *visitID;
    NSMutableDictionary *routeDictionary;

    //VisitOptionsViewController *visitViewController;

    
}

@property (nonatomic, strong) NSMutableDictionary *customer;
@property (nonatomic, strong) UIViewController *parentViewController;
@property (nonatomic, strong) NSString *visitID;
@property (nonatomic, strong) NSMutableDictionary *routeDictionary;

@property(nonatomic)BOOL isFromRoute;

//@property (nonatomic, strong) VisitOptionsViewController *visitViewController;



//+ (SWVisitManager *)defaultManager;
- (void)startVisitWithCustomer:(NSDictionary *)customer parent:(UIViewController *)parent andChecker:(NSString *)isParent;
- (void)closeVisit;


@end