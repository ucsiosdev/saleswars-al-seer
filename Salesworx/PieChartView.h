//
//  PieClassAntiAlias.h
//  PieChart
//
//  Created by Reetu Raj on 17/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PieChartView : UIView {
    
    NSArray* itemArray;
    NSArray* myColorArray; 
    int radius;
    
}
@property(nonatomic, strong)NSArray* itemArray;
@property(nonatomic, strong)NSArray* myColorArray;
@property(nonatomic,assign) int radius;



@end
