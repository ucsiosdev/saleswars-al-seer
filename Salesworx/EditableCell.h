//
//  EditableCell.h
//  SWFoundation
//
//  Created by Irfan Bashir on 7/1/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditableCell;

@protocol EditableCellDelegate <NSObject>

@optional
- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key;
- (void)editableCellDidStartUpdate:(EditableCell *)cell;
@end

@interface EditableCell : UITableViewCell {
    id < EditableCellDelegate > delegate;
    NSString *key;
}

@property (unsafe_unretained) id < EditableCellDelegate > delegate;
@property (nonatomic, strong) NSString *key;

@end