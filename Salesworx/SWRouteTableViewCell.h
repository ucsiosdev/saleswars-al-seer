//
//  SWRouteTableViewCell.h
//  SalesWars
//
//  Created by Prasann on 31/12/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWRouteTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerCodeLabel;

@end

NS_ASSUME_NONNULL_END
