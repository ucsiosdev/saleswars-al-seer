//
//  BlockedCustomerTableViewCell.h
//  SalesWars
//
//  Created by Prasann on 28/11/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesWorxSingleLineLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BlockedCustomerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *customerCodeLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *contactNumerLabel;
@property (weak, nonatomic) IBOutlet SalesWorxSingleLineLabel *phoneNumberLabel;

@end

NS_ASSUME_NONNULL_END
