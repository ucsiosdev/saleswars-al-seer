//
//  ManageOrderViewController.m
//  SWCustomer
//
//  Created by msaad on 12/26/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ManageOrderViewController.h"
#import "ProductOrderViewController.h"
#import "SalesOrderNewViewController.h"
#import "AlSeerSalesOrderViewController.h"
#import "ManageOrdersTableViewCell.h"
#import "SalesOrderTemplate.h"

@interface ManageOrderViewController ()

@end

@implementation ManageOrderViewController
@synthesize customerDictornary;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Manage Orders"];
    customerNameLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_Name"]];
    customerNumberLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_No"]];
    double availBal = [[customerDictornary stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
    manageOrderTableView.tableFooterView = [UIView new];
    if (@available(iOS 15.0, *)) {
        manageOrderTableView.sectionHeaderTopPadding = 0;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getSalesOrderServiceDiddbGetPerformaOrder:[[SWDatabaseManager retrieveManager] dbGetPerformaOrder:[customerDictornary stringForKey:@"Ship_Customer_ID"] withSiteId:[customerDictornary stringForKey:@"Ship_Site_Use_ID"]]];
}

#pragma mark UITableView Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return totalOrderArray.count;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString* identifier=@"manageOrderTableCell";
    ManageOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell=nil;
    
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ManageOrdersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:235.0/255.0 green:251.0/255.0 blue:249.0/255.0 alpha:1.0]];
    
    cell.orderNumberLabel.text = @"Reference No.";
    cell.OrderCreationdateLabel.text = @"Date";
    cell.OrderstatusLabel.text = @"Status";
    cell.orderamountLabel.text = @"Amount (AED)";
    
    cell.orderNumberLabel.textColor = TableViewHeaderSectionColor;
    cell.OrderCreationdateLabel.textColor = TableViewHeaderSectionColor;
    cell.OrderstatusLabel.textColor = TableViewHeaderSectionColor;
    cell.orderamountLabel.textColor = TableViewHeaderSectionColor;
    
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier=@"manageOrderTableCell";
    ManageOrdersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"ManageOrdersTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data = [totalOrderArray objectAtIndex:indexPath.row];
    
    cell.orderNumberLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Orig_Sys_Document_Ref"]]];
    cell.OrderCreationdateLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Creation_Date"]]];
    cell.orderamountLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:[data stringForKey:@"Order_Amt"]]];
    if([data objectForKey:@"Schedule_Ship_Date"])
    {
        cell.OrderstatusLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:@"Confirmed"]];
        cell.OrderstatusLabel.textColor = [UIColor colorWithRed:71.0/255.0 green:182.0/255.0 blue:129.0/255.0 alpha:1.0];
    }
    else
    {
        cell.OrderstatusLabel.text = [NSString stringWithFormat:@"%@",[SWDefaults getValidStringValue:@"Unconfirmed"]];
        cell.OrderstatusLabel.textColor = [UIColor colorWithRed:225.0/255.0 green:115.0/255.0 blue:110.0/255.0 alpha:1.0];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Singleton *single = [Singleton retrieveSingleton];
    single.orderStartDate = [NSDate date];
    NSString *orderString = @"";
    if([[totalOrderArray objectAtIndex:indexPath.row] objectForKey:@"Schedule_Ship_Date"])
    {
        orderString = @"confirmed";
    }
    else
    {
        orderString = @"notconfirmed";
    }
    
    NSString *savedCategoryID = [[totalOrderArray objectAtIndex:indexPath.row] stringForKey:@"Custom_Attribute_1"];
    [self dbGetProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customerDictornary] andCategoryName:savedCategoryID andOrderDictionary:[totalOrderArray objectAtIndex:indexPath.row] andOrderString:orderString];
}
- (void) dbGetProductServiceDidGetCategories:(NSArray *)categories andCategoryName:(NSString *)savedCategoryID andOrderDictionary:(NSMutableDictionary *)dict andOrderString:(NSString *)orderString
{
    NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:categories];
	for(int idx = 0; idx < categories.count; idx++)
    {
        NSDictionary *row = [categoryArray objectAtIndex:idx];
        
        if ([[row stringForKey:@"Category"] isEqualToString:savedCategoryID]) {
             //NSMutableDictionary *category=[row mutableCopy];
            NSLog(@"setProductCategory 2");
            [SWDefaults setProductCategory:[row mutableCopy]];
            break;
        }
    }
    
    if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
        
        
        AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
        newSalesOrder.parentViewString = @"MO";
        newSalesOrder.preOrderedItems=[NSMutableArray new];
        
        
        //for unconfirmed orders we wont have ORig_Sys_Document ref so fetch the selected UOM for unconfirmed orders from TBL_Proforma orderd
        
        if ([orderString isEqualToString:@"notconfirmed"]) {
            
            
            [SWDefaults clearSignature];
            
            NSString* fetchUomQry=[NSString stringWithFormat:@"select Order_Quantity_UOM from tbl_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
            
            NSMutableArray* orderUom=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
            if (orderUom.count>0) {
                
                NSLog(@"check order uom resp %@", [orderUom description]);
                NSString* orderwithUOMSelected= [[orderUom valueForKey:@"Order_Quantity_UOM"] objectAtIndex:0];
                
                if (orderwithUOMSelected==nil || [orderwithUOMSelected isEqualToString:@""]||[orderwithUOMSelected isEqual:[NSNull null]])
                {
                    
                    //order qty uom is null select the display uom as default
                    
                    NSString* fetchUomQry=[NSString stringWithFormat:@"select Display_UOM from tbl_Proforma_Order_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
                    
                    //we have value in this array ony if uom is selected handle the condition if uom is not selected and order is saved not confirmed
                    NSMutableArray* orderwithoutUOM=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
                    
                    
                    
                    NSString* orderwithUOMSelected= [[orderwithoutUOM valueForKey:@"Display_UOM"] objectAtIndex:0];
                    
                    [customerDictornary setValue:[[orderwithoutUOM valueForKey:@"Display_UOM"] objectAtIndex:0] forKey:@"Order_Quantity_UOM"];
                }
                else if (orderUom.count>0) {
                    [customerDictornary setValue:[[orderUom valueForKey:@"Order_Quantity_UOM"] objectAtIndex:0] forKey:@"Order_Quantity_UOM"];
                }
            }
        }
        
        else
        {
            //adding the selected uom to the dict for Manage orders
            
            NSString* fetchUomQry=[NSString stringWithFormat:@"select Order_Quantity_UOM from TBL_Order_History_Line_Items where Orig_Sys_Document_Ref='%@'",[dict stringForKey:@"Orig_Sys_Document_Ref"]];
            NSMutableArray* orderUom=[[SWDatabaseManager retrieveManager] fetchDataForQuery:fetchUomQry];
            if (orderUom.count>0) {
                [customerDictornary setValue:[orderUom valueForKey:@"Order_Quantity_UOM"]  forKey:@"Order_Quantity_UOM"];
            }
        }
        NSLog(@"check customer with uom data %@", [customerDictornary description]);
        
        newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customerDictornary];
        newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
        newSalesOrder.isOrderConfirmed=orderString;
        [self.navigationController pushViewController:newSalesOrder animated:YES];
    }
    
    
    else
    {
        
        
        SalesOrderNewViewController *newSalesOrder =[[SalesOrderNewViewController alloc] initWithNibName:@"SalesOrderNewViewController" bundle:nil];
        newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customerDictornary];
        newSalesOrder.parentViewString = @"MO";
        newSalesOrder.preOrderedItems=[NSMutableArray new];
        newSalesOrder.preOrdered = [NSMutableDictionary dictionaryWithDictionary:dict];
        newSalesOrder.isOrderConfirmed=orderString;
        [self.navigationController pushViewController:newSalesOrder animated:YES];
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = [totalOrderArray objectAtIndex:indexPath.row];
    if(![data objectForKey:@"Schedule_Ship_Date"])
    {
        UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            @try {
                [self deleteTableRow:indexPath];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
        }];
        editAction.backgroundColor = [UIColor redColor];
        return @[editAction];
    }
    else {
        UITableViewRowAction *sendPDFAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Send Email" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
            @try {
                [self generateReceiptPDFDocument:indexPath];
            }
            @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
        }];
        sendPDFAction.backgroundColor = [UIColor colorWithRed:71.0/255.0 green:182.0/255.0 blue:129.0/255.0 alpha:1.0];
        return @[sendPDFAction];
    }
    return nil;
}
-(void)deleteTableRow:(NSIndexPath *)indexPath
{
    NSDictionary *data = [totalOrderArray objectAtIndex:indexPath.row];
    [[SWDatabaseManager retrieveManager] deleteManageOrderWithRef:[data stringForKey:@"Orig_Sys_Document_Ref"]];
    [totalOrderArray removeObjectAtIndex:indexPath.row];
    [manageOrderTableView reloadData];
}

-(void)generateReceiptPDFDocument:(NSIndexPath *)indexPath {
    NSMutableDictionary *data = [totalOrderArray objectAtIndex:indexPath.row];

    NSArray *itemsSalesOrder = [[SWDatabaseManager retrieveManager] dbGetSalesConfirmOrderItems:[data objectForKey:@"Orig_Sys_Document_Ref"]];
    
    long numberOfPage = (itemsSalesOrder.count % 10 == 0) ? (itemsSalesOrder.count / 10) : (itemsSalesOrder.count / 10)+1;
    if (numberOfPage == 0) {
        numberOfPage = 1;
    }
    NSMutableData *pdfData = [[NSMutableData alloc]init];
    UIGraphicsBeginPDFContextToData(pdfData, CGRectMake(0, 0, 768, 1024), nil);

    for (int i = 0; i < numberOfPage; i++) {
        SalesOrderTemplate *pdfTemplate = [[SalesOrderTemplate alloc]initWithFrame:CGRectMake(0, 0, 768, 1024)];
        pdfTemplate.pageNumber = i +1;
        pdfTemplate.customerDict = customerDictornary;
        pdfTemplate.salesOrderDict = data;
        pdfTemplate.itemsSalesOrder = [[NSMutableArray alloc] initWithArray:itemsSalesOrder];
        pdfTemplate.isFromManageOrderOnSwipe = true;
        [pdfTemplate updatePDF];
        UIGraphicsBeginPDFPage();
        struct CGContext *pdfContext = UIGraphicsGetCurrentContext();
        if(pdfContext) {
            [pdfTemplate.layer renderInContext:pdfContext];
        }
    }
    UIGraphicsEndPDFContext();

    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[NSString stringWithFormat:@"Sales Order Receipt - %@",customerDictornary[@"Customer_Name"]]];
        [mailViewController setMessageBody:@"Hi,\n Please find the attached receipt." isHTML:NO];
        [mailViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"Sares Order Receipt- %@.pdf",customerDictornary[@"Customer_No"]]];
//        [mailViewController setToRecipients:[NSArray arrayWithObjects:@"neha.gupta@ushyaku.com",nil]];
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:true completion:nil];
    return;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)getSalesOrderServiceDiddbGetPerformaOrder:(NSArray *)performaOrder
{
    performaOrderArray = [NSMutableArray arrayWithArray:performaOrder];
    [self getSalesOrderServiceDiddbGetConfirmOrder:[[SWDatabaseManager retrieveManager] dbGetConfirmOrder:[customerDictornary stringForKey:@"Ship_Customer_ID"] withSiteId:[customerDictornary stringForKey:@"Ship_Site_Use_ID"]]];
    performaOrder=nil;
}


- (void)getSalesOrderServiceDiddbGetConfirmOrder:(NSArray *)performaOrder
{
    confirmOrderArray = [NSMutableArray arrayWithArray:performaOrder];
    totalOrderArray = [[performaOrderArray arrayByAddingObjectsFromArray:confirmOrderArray] mutableCopy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Creation_Date" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    totalOrderArray = [[totalOrderArray sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    [manageOrderTableView reloadData];
    performaOrder=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

@end
