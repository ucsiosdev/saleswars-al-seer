//
//  StockInfoSalesHistoryViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 4/29/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "StockInfoSalesHistoryViewController.h"
#import "CustomerSalesViewController.h"
#import "ProductStockViewController.h"
#import "StockInfoTableViewController.h"
#import "AlSeerProductStockViewTableViewCell.h"
#import "AlSeerProductStockViewHeaderTableViewCell.h"
#import "AlSeerSalesOrderProductStockTableViewCell.h"
#import "AlSeerProductStockHeaderTableViewCell.h"



@interface StockInfoSalesHistoryViewController ()

@end

@implementation StockInfoSalesHistoryViewController

@synthesize mainProductDictionary,customerDictionary;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    productDict=[[NSMutableDictionary alloc]init];
    productStockArray=[[NSMutableArray alloc]init];
    
    UIBarButtonItem* closeButton=[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleDone target:self action:@selector(closeView)];
    
    
    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName : bodyFont, NSForegroundColorAttributeName: [UIColor whiteColor]};
    [[UIBarButtonItem appearanceWhenContainedIn:[StockInfoSalesHistoryViewController class], nil] setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem= closeButton;
    
    self.titleLabel.text=[NSString stringWithFormat:@"%@ - %@",[mainProductDictionary stringForKey:@"Item_Code"],[mainProductDictionary stringForKey:@"Description"]];
    NSLog(@"check main product dict %@", [mainProductDictionary description]);
    
    
    stockViewController = [[ProductStockViewController alloc] initWithProduct:mainProductDictionary] ;
    [stockViewController setDelegate:self];
    [stockViewController setOrderQuantity:[[mainProductDictionary stringForKey:@"Qty"] intValue]];
    [stockViewController setTarget:self];
    [stockViewController setAction:@selector(stockAllocated:)];
    
    
    UIView *stockInfoView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 350,512)];

    stockInfoView.backgroundColor=[UIColor redColor];
    stockInfoView.tag=2;
    CGRect oldFrame=stockInfoView.frame;
    stockInfoView = stockViewController.view;
    stockInfoView.frame=oldFrame;
    [self addChildViewController:stockViewController];
    
    stockInfoScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(8, 38, 387,512)];
    NSLog(@"check stock info new size %ld", (long)self.stockInfoSize);
    
    
    float reqHeight=[[[NSUserDefaults standardUserDefaults]valueForKey:@"StockScrollViewHeight"] floatValue];
    stockInfoScrollView.contentSize=CGSizeMake(784, reqHeight);
    [stockInfoScrollView addSubview:stockInfoView];
    stockInfoScrollView.scrollEnabled=YES;
    
    //sales history
    CustomerSalesViewController *salesHistoryVC = [[CustomerSalesViewController alloc] initWithCustomer:customerDictionary andProduct:mainProductDictionary] ;
    UIView *salesHistoryView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 387,712)];
    salesHistoryView.backgroundColor=[UIColor redColor];
    salesHistoryView.tag=1;
    CGRect olfFrame=salesHistoryView.frame;
    salesHistoryView = salesHistoryVC.view;
    salesHistoryView.frame=olfFrame;
    [self addChildViewController:salesHistoryVC];
    
    UIScrollView* salesHistoryScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(480, 38, 387, 712)];
    [salesHistoryScrollView addSubview:salesHistoryView];
    salesHistoryScrollView.scrollEnabled=YES;
    [self.view addSubview:salesHistoryScrollView];
    
    NSLog(@"main product dict in pop over %@", [mainProductDictionary description]);
    
    
    productDict=mainProductDictionary;
    [self fetchStock];
    
    // Do any additional setup after loading the view from its nib.
    
    _stockTblView.cellLayoutMarginsFollowReadableWidth = NO;
    if (@available(iOS 15.0, *)) {
        self.stockTblView.sectionHeaderTopPadding = 0;
    }
}


-(void)closeView

{
    [self.popOver dismissPopoverAnimated:YES];
}

- (void)stockAllocated:(NSMutableDictionary *)p {
    if([self.customDelegate respondsToSelector:@selector(updateMainProductDict:)])
    {
        [self.customDelegate updateMainProductDict:p];
    }
}

-(void)updateStockScrollContent:(CGSize)contentSize
{
    [stockInfoScrollView setContentSize:CGSizeMake(contentSize.width, contentSize.height)];
    NSLog(@"content updated in method");
}

- (void)gridView:(GridView *)gridView didSelectRow:(GridCellView *)cell atIndex:(int)rowIndex
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ProductStockViewControllerDelegate methods

//OLA!
-(void)didTapSaveButton
{
    
    
   
}


#pragma mark Stock Information methods

-(void)fetchStock
{
    //fetching product stock data
    
    NSString* productStockQry=[NSString stringWithFormat:@"SELECT  IFNULL(Lot_Qty,0) AS Lot_Qty ,IFNULL(Lot_No,0)AS Lot_No, IFNULL(Expiry_Date,0) AS Expiry_Date ,IFNULL(Custom_Attribute_1,0)AS QC,  IFNULL(Custom_Attribute_2,0)AS Blocked FROM TBL_Product_Stock WHERE Item_ID='%@' ORDER BY  Expiry_Date ASC",[productDict valueForKey:@"ItemID"]];
    
    NSLog(@"product stock qry is %@", productStockQry);
    productStockArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:productStockQry];
    if (productStockArray.count>0) {
        NSLog(@"product stock array is %@", [productStockArray description]);
        
    }
    
    NSLog(@"check product dict in product details screen %@",[productDict description] );
    
    
    
    //fetch agency
    
    NSString* agencyQry=[NSString stringWithFormat:@"select IFNULL(Agency,0) AS Agency from TBL_Product where Inventory_Item_ID='%@' and Item_Code='%@' ", [productDict valueForKey:@"Inventory_Item_ID"],[productDict valueForKey:@"Item_Code"]];
    NSLog(@"query for fetching agency %@", agencyQry);
    NSMutableArray* agencyArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:agencyQry];
    NSLog(@"agencyArray details %@", [agencyArray description]);
    if (agencyArray.count>0) {
        //self.agencyLbl.text=[NSString stringWithFormat:@"%@",[[agencyArray valueForKey:@"Agency"] objectAtIndex:0]];
    }
    
    
    
    //fetch sum
    NSString* sumofLotsQty=[NSString stringWithFormat:@"SELECT IFNULL( SUM(Lot_Qty),0) AS Total from TBL_Product_Stock WHERE Item_ID='%@' ",[productDict valueForKey:@"ItemID"]];
    NSLog(@"product stock qry is %@", sumofLotsQty);
    NSMutableArray*  sumArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:sumofLotsQty];
    if (sumArray.count>0) {
        NSLog(@"sumArray array is %@", [sumArray description]);
        
        totalStock=[NSString stringWithFormat:@"%@",[[sumArray valueForKey:@"Total"] objectAtIndex:0]];
    }
    
    
    //fetch conversion
    
    NSArray* conversionArray;

    NSString* selectedUOM=[mainProductDictionary valueForKey:@"SelectedUOM"];
    
    if ([mainProductDictionary objectForKey:@"SelectedUOM"]) {
        
         conversionArray=[SWDatabaseManager fetchConversionFactorforUOMitemCode:[mainProductDictionary valueForKey:@"Item_Code"] ItemUOM:[mainProductDictionary objectForKey:@"SelectedUOM"]];
    }
    else
    {
         conversionArray=[SWDatabaseManager fetchConversionFactorforUOMitemCode:[mainProductDictionary valueForKey:@"Item_Code"] ItemUOM:@"CS"];
    }
    
    
   
    
    NSLog(@"test stock %@", [conversionArray description]);
    
    if (conversionArray.count>0) {
        conversionRate=[[[conversionArray valueForKey:@"Conversion"] objectAtIndex:0] integerValue];
        [self.stockTblView reloadData];
    }
}



#pragma  mark UITableView data source methods
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (productStockArray.count>0) {
        return productStockArray.count;
    }
    else
    {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    static NSString* stockViewCell=@"customHeader";
    AlSeerProductStockHeaderTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    
    if (cell==nil) {
        
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"AlSeerProductStockHeaderTableViewCell" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
        cell.contentView.backgroundColor=[UIColor colorWithRed:(246.0/255.0) green:(247.0/255.0) blue:(251.0/255.0) alpha:1];
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* stockViewCell=@"soStockCell";
    AlSeerSalesOrderProductStockTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:stockViewCell];
    
    if (cell==nil) {
        NSArray*  cellArray=[[NSBundle mainBundle]loadNibNamed:@"AlSeerSalesOrderProductStockTableViewCell" owner:self options:nil];
        cell= [cellArray objectAtIndex:0];
    }
    
    cell.lotNumberLbl.text=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_No"] objectAtIndex:indexPath.row]];
    
    
    NSInteger tempQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Lot_Qty"] objectAtIndex:indexPath.row]] integerValue];
    cell.qtyLbl.text=[NSString stringWithFormat:@"%ld", (long)tempQty];
    
    NSInteger convertedQty= tempQty/conversionRate;
    cell.qtyLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedQty];
    
    
    
    NSInteger blockedQty=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Blocked"] objectAtIndex:indexPath.row]] integerValue];

    NSInteger convertedBlockedQty= blockedQty/conversionRate;

    cell.blockedStockLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedBlockedQty];
    
    
    
    NSInteger qc=[[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"QC"] objectAtIndex:indexPath.row]] integerValue];
    NSInteger convertedQcQty=qc/conversionRate;
    
    
    cell.qualityInspectionLbl.text=[NSString stringWithFormat:@"%ld", (long)convertedQcQty];
    
    
    // Convert string to date object
    NSString*  dateStr=[NSString stringWithFormat:@"%@", [[productStockArray valueForKey:@"Expiry_Date"] objectAtIndex:indexPath.row]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    dateStr = [dateFormat stringFromDate:date];
    
    
    NSLog(@"desired date format %@", dateStr);
    cell.expiryLbl.text=dateStr;
    
    
    return cell;
}

+(NSString*)getDateStr:(NSString *)dateStr
{
    if([dateStr isEqualToString:@""] || dateStr.length<8)
        return @"";
    else
    {
        // Convert string to date object
        dateStr=[dateStr substringToIndex:8];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd MMM, YYYY"];
        dateStr = [dateFormat stringFromDate:date];
        return dateStr;
    }
}


@end
