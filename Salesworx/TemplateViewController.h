//
//  TemplateViewController.h
//  Salesworx
//
//  Created by msaad on 5/26/13.
//  Copyright (c) 2013 msaad. All rights reserved.
//

#import "SWPlatform.h"

@interface TemplateViewController : SWViewController <GridViewDataSource, GridViewDelegate>
{
    NSMutableDictionary *customer;
    GridView *gridView;
    NSMutableArray *templateOrderArray;
    NSMutableArray *totalOrderArray;
}
- (id)initWithCustomer:(NSDictionary *)customer;
@end
