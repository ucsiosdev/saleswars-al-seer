//
//  SWDatePickerViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/17/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWDatePickerViewController.h"

@interface SWDatePickerViewController ()

@end

@implementation SWDatePickerViewController

@synthesize picker;
@synthesize target;
@synthesize action;
@synthesize selectedDate;
@synthesize toolbar,isRoute,forReports;

- (id)initWithTarget:(id)t action:(SEL)a {
    self = [super init];
    if (self) {
        [self setTarget:t];
        [self setAction:a];
        [self setTitle:NSLocalizedString(@"Select Date", nil)];
        self.navigationController.navigationBar.barTintColor  = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];
        self.forReports= NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setPicker:[[UIDatePicker alloc] init]];
    if (@available(iOS 13.4, *)) {
        self.picker.preferredDatePickerStyle = UIDatePickerStyleWheels;
    }
    [self.picker setFrame:CGRectMake(0, 0, 300, 200)];
    [self.picker setDatePickerMode:UIDatePickerModeDate];
    [self.picker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    if (self.isRoute)
    {
        [self.picker setMinimumDate:[NSDate date]];
    }
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1.0];

    if (forReports) {
        
        //date fix in reports
        NSLog(@"test");
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setYear:30];
        NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
        
        [self.picker setMaximumDate:maxDate];

    }
    [self.view addSubview:self.picker];
    
    [self setPreferredContentSize:CGSizeMake(300, 200)];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelSelection)];
    cancelButton.tag=1000;
    self.navigationItem.leftBarButtonItem = cancelButton;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dateSelectionDone)];
    cancelButton.tag=1001;
    self.navigationItem.rightBarButtonItem = doneButton;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}

- (void)dateChanged:(id)sender{
    
}

- (void)cancelSelection {
    
    [self dismissViewControllerAnimated:YES completion:nil];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
   // [self.target performSelector:self.action withObject:self];
#pragma clang diagnostic pop

}

- (void)dateSelectionDone {
    [self setSelectedDate:self.picker.date];
    NSLog(@"slected date %@", self.picker.date);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:self];
#pragma clang diagnostic pop

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
