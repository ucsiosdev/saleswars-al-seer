//
//  SeverEditLocation.h
//  SWPlatform
//
//  Created by msaad on 3/13/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeverEditLocation : UITableViewController
{
//       id target;
    SEL selectionChanged;
    NSMutableArray *serverArray;
}

@property (nonatomic, unsafe_unretained) id target;
@property (nonatomic, assign) SEL selectionChanged;
@property (nonatomic, strong) NSMutableArray *serverArray;

@end