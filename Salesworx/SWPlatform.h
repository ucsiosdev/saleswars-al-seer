//
//  SWPlatform.h
//  SWPlatform
//
//  Created by Irfan Bashir on 5/7/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#ifndef SWPlatform_SWPlatform_h
#define SWPlatform_SWPlatform_h

// Services


// View Controllers
#import "SWViewController.h"
#import "SWTableViewController.h"
#import "CustomersListViewController.h"
#import "ProductListViewController.h"
#import "OrderInvoiceViewController.h"
#import "OrderItemsViewController.h"
// Views
#import "SWDatePickerViewController.h"
#import "SWBarButtonItem.h"
#import "SWSection.h"
#import "DetailBaseView.h"
#import "PlainSectionHeader.h"
#import "GroupSectionHeaderView.h"
#import "GroupSectionFooterView.h"
#import "CustomerHeaderView.h"
#import "ProductHeaderView.h"
#import "ProductBonusView.h"
#import "ProductStockView.h"
#import "ProductTargetView.h"

// Others
#import "SWPlatformDatabaseConstants.h"
#import "SWDatabaseManager.h"
#import "SWDefaults.h"
#import "SWVisitManager.h"
#import "ZBarSDK.h"
#import "PieChartView.h"
#import "Singleton.h"
#import "SWSplitViewController.h"
#import "ZUUIRevealController.h"
#import "PCPieChart.h"
#import "CashCutomerViewController.h"
#import "GenCustListViewController.h"
#import "SurveyViewController.h"
#import "SurveyQuestionViewController.h"
#import "SynViewController.h"
#import "CJSONSerializer.h"
#import "CJSONDeserializer.h"
#import "SynchroniseViewController.h"
#import "PCLineChartView.h"
#import "SurveyDetails.h"
#import "CommunicationViewControllerNew.h"
#import "SurveyFormViewController.h"
#import "DataSyncManager.h"
#import "SeverEditLocation.h"
#import "SettingViewController.h"
#import "Flurry.h"
#import "AppControl.h"
#import "HttpClient.h"

#endif
