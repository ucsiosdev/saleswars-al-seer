//
//  AlSeerSalesOrderViewController.h
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 2/12/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ProductBonusViewController.h"
#import "ProductStockViewController.h"
#import "CustomerSalesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "OutletPopoverTableViewController.h"
#import "AlSeerSalesOrderCalculationViewController.h"
#import "StockInfoSalesHistoryViewController.h"
#import "SalesWorxDropShadowView.h"


#define NUMERIC @"1234567890"

@interface AlSeerSalesOrderViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate,StringPopOverDelegate,UITableViewDataSource,UITableViewDelegate,ProductStockViewDelegate,ProductStockViewControllerDelegate,stockInfoUpdatedDelegate>
{
    IBOutlet UILabel *lblCustomerName;
    IBOutlet UILabel *lblCustomerNo;
    IBOutlet UILabel *lblAvailableBalance;
    
    IBOutlet UILabel *lblProductName;
    IBOutlet UILabel *lblProductCode;
    IBOutlet UILabel *lblProductBrand;
    IBOutlet UILabel *lblWholesalePrice;
    IBOutlet UILabel *statusLbl;
    IBOutlet UITextField *txtfldWholesalePrice;
    IBOutlet UILabel *lblRetailPrice;
    IBOutlet UILabel *lblUOMCode;
    IBOutlet UILabel *lblAvlStock;
    IBOutlet UILabel *lblExpDate;
    IBOutlet UILabel *lblPriceLabel;
    IBOutlet UILabel *lblCustomerLabel;
    IBOutlet UILabel *lblSelectProduct;
    
    IBOutlet UILabel *lblTitleProductCode;
    IBOutlet UILabel *lblTitleWholesalePrice;
    IBOutlet UILabel *lblTitleRetailPrice;
    IBOutlet UILabel *lblTitleUOMCode;
    IBOutlet UILabel *lblTitleAvlStock;
    IBOutlet UILabel *lblTitleExpDate;
    IBOutlet UILabel *txtTitleProductQty;
    IBOutlet UILabel *txtTitleDefBonus;
    IBOutlet UILabel *txtTitleRecBonus;
    IBOutlet UILabel *txtTitleDiscount;
    IBOutlet UILabel *txtTitleFOCQty;
    IBOutlet UILabel *txtTitleFOCItem;
    IBOutlet UILabel *txtTitleNotes;
    
    IBOutlet UITextField *txtProductQty;
    IBOutlet UITextField *txtDefBonus;
    IBOutlet UITextField *txtRecBonus;
    IBOutlet UITextField *txtDiscount;
    IBOutlet UITextField *txtFOCQty;
    
    IBOutlet UITextView *txtNotes;
    
    IBOutlet UIView *productDropDown;
    IBOutlet UIView *salesHistoryView;
    IBOutlet UIView *bonusView;
    IBOutlet UIView *stockView;
    IBOutlet UIView *bottomOrderView;
    
    IBOutlet UIView *dragParentView;
    IBOutlet UIView *dragChildView;
    
    IBOutlet UIView *productDetailView;
    IBOutlet UIView *agencyTargetInfoView;
    IBOutlet SalesWorxDropShadowView *orderItemContainerView;



    
    IBOutlet UIButton *salesHistoryBtn;
    IBOutlet UIButton *bonusBtn;
    IBOutlet UIButton *stockBtn;
    IBOutlet UIButton *focProductBtn;
    IBOutlet UIButton *dragButton;
    IBOutlet UIButton *addButton;
    IBOutlet UIButton *clearButton;

    
    IBOutlet UITableView *productTableView;
    IBOutlet UITableView *focProductTableView;
    IBOutlet UISearchBar *candySearchBar;
    BOOL bSearchIsOn;
    
    
    
    BOOL isPproductDropDownToggled;
    BOOL isSalesHistoryToggled;
    BOOL isBonusToggled;
    BOOL isStockToggled;
    BOOL isErrorSelectProduct;
    
    ProductBonusViewController *productBonusViewController;
    ProductStockViewController *productStockViewController;
    CustomerSalesViewController *customerSalesVC;
    AlSeerSalesOrderCalculationViewController *oldSalesOrderVC;

    IBOutlet SalesWorxDropShadowView *productListView;
    IBOutlet UIView *placeHolderView;
    IBOutlet UIView *customerDetailView;
    
    NSMutableDictionary *productDictionary;
    NSMutableDictionary *mainProductDict;
    
    NSMutableArray *productArray;
    NSMutableArray *filteredCandyArray;
    NSMutableArray *finalProductArray;
    NSMutableSet* _collapsedSections;
    
    int bonus;
    BOOL isBonus;
    BOOL scrollViewDelegateFreezed;
    BOOL isPriceCel;
    BOOL isComment;
    BOOL isSaveOrder;
    BOOL isPopover;
    BOOL isBonusAllow;
    BOOL isDiscountAllow;
    BOOL isFOCAllow;
    BOOL isFOCItemAllow;
    BOOL isDualBonusAllow;
    BOOL isAddedRows;
    BOOL isBonusRowDeleted;
    BOOL checkDiscount;
    BOOL checkFOC;
    BOOL checkBonus;
    BOOL isDidSelectStock;
    BOOL isDidSelectBonus;
    BOOL isDidSelectHistory;
    BOOL isPinchDone;
    
    NSString *isBonusAppControl;
    NSString *isFOCAppControl;
    NSString *isDiscountAppControl;
    NSString *isFOCItemAppControl;
    NSString *isDualBonusAppControl;
    NSString *bonusEdit;
    NSString *isLineItemNotes;
    NSString *showHistory;
    BOOL isShowHistory;
    
    NSArray *bonusInfo;
    NSMutableDictionary *bonusProduct;
    NSMutableDictionary *fBonusProduct;
    
    int temp;
    NSString * tempStr;
    ///////////////////////////////////////////
    //Sales Order Objects

    
    
    OutletPopoverTableViewController* outletPopOver;
    UIPopoverController *valuePopOverController;

    NSString* selectedUOMReference;
    
    
    NSMutableDictionary *productReference;
    
    
    NSUInteger  selectedIndexPath;
    
    NSMutableArray* salesStatsArray;
    
    
    BOOL noPricesAvailableStatus;
    
}

@property (strong, nonatomic) IBOutlet UIView *clearShadowView;
@property (strong, nonatomic) IBOutlet UIView *addButtonShadowView;

@property (nonatomic,strong)NSMutableDictionary *customerDict;
@property (nonatomic ,strong)NSString *parentViewString;
@property (nonatomic ,strong)NSMutableDictionary *preOrdered;
@property (nonatomic ,strong)NSMutableArray *preOrderedItems;
@property (nonatomic ,strong)NSString *isOrderConfirmed;
@property(strong,nonatomic)NSMutableArray * wareHouseDataSalesOrderArray;
-(IBAction)buttonAction:(id)sender;
-(void)ResetData;
@property (strong, nonatomic) IBOutlet UIButton *uomButton;
@property (strong, nonatomic) IBOutlet UITableView *salesOrderTableView;

@property(strong,nonatomic)NSMutableArray* salesOrderArray;
- (void)stockAllocated:(NSMutableDictionary *)p;

@property (strong, nonatomic) IBOutlet UIView *viewUOMCode;

@property (strong, nonatomic) IBOutlet UILabel *targetLbl;
@property (strong, nonatomic) IBOutlet UILabel *salesLbl;
@property (strong, nonatomic) IBOutlet UILabel *balanceToGoLbl;
@property (strong, nonatomic) IBOutlet UILabel *achievementPercentLbl;

@property (strong, nonatomic) IBOutlet UILabel *dailyRequirementLbl;

- (IBAction)infoButtonTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *infoButton;

@property(strong,nonatomic) UIPopoverController* popoverController;

@property (strong, nonatomic) IBOutlet UILabel *uomConversionFactorLbl;
@property (strong, nonatomic) IBOutlet UILabel *stockIndicatorLbl;


@end
