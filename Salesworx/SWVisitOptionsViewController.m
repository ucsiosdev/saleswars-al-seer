//
//  SWVisitOptionsViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/10/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWVisitOptionsViewController.h"
#import "SWCollectionViewController.h"
#import "SWOrderHistoryViewController.h"
#import "SWDistributionCheckViewController.h"
#import "ProductSaleCategoryViewController.h"
#import "ManageOrderViewController.h"
#import "SWPlatform.h"
#import "SWDuesView.h"
#import "ReturnProductCategoryViewController.h"
#import "AnimationUtility.h"
#import "AlSeerSalesOrderViewController.h"
#import "AlseerReturnViewController.h"
#import "PointofPurchaseViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "OnsiteOptionsViewController.h"
#import "SalesWorxFeedbackViewController.h"
#import "SalesWorxVisitOptionsCollectionViewCell.h"
#import "SWAppDelegate.h"

@interface SWVisitOptionsViewController ()
{
    CLLocationManager *locationManager;
}
@end

@implementation SWVisitOptionsViewController
@synthesize visitID,synVC,customerDictornary,statusLabel;


- (void)viewDidLoad {
    [super viewDidLoad];
    //background
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(notificationReceived:) name:@"ordersSentSuccessfully" object:nil];
    
    UIBarButtonItem * imageBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"CaptureImage"] style:UIBarButtonItemStylePlain target:self action:@selector(openImageCapture)];
    self.navigationItem.rightBarButtonItem=imageBtn;
    
    self.navigationItem.titleView=[SWDefaults createNavigationBarTitleView:@"Visit Options"];
    customerNameLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_Name"]];
    customerNumberLabel.text = [SWDefaults getValidStringValue:[customerDictornary valueForKey:@"Customer_No"]];
    double availBal = [[customerDictornary stringForKey:@"Avail_Bal"] doubleValue];
    availableBalanceLabel.text = [[NSString stringWithFormat:@"%f",availBal] currencyString];
    
    //check if there is any visit id of previus visits
    
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    
    if (currentVisitID) {
        
        NSLog(@"previous visit id exists %@", currentVisitID);
        NSLog(@"visit id after clearing %@", [SWDefaults currentVisitID]);
        
    }
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Exit", nil) style:UIBarButtonItemStyleDone target:self action:@selector(closeVisit:)] ];
    OnsiteOptionsViewController* onsiteOptions=[[OnsiteOptionsViewController alloc]init];
    onsiteOptions.delegate=self;
    [self presentPopupViewController:onsiteOptions animationType:MJPopupViewAnimationSlideTopBottom dismissed:nil];
    
    [visitOptionCollectionView registerClass:[SalesWorxVisitOptionsCollectionViewCell class] forCellWithReuseIdentifier:@"visitCell"];
    statusLabel.layer.cornerRadius = 10.0;
    statusLabel.layer.masksToBounds = YES;
    statusLabel.hidden = YES;
    
    if ([[SWDatabaseManager isCustomerBlocked:[customerDictornary valueForKey:@"Customer_ID"]] isEqualToString:@"N"]) {
        statusLabel.hidden = NO;
        statusLabel.text = @"Blocked";
    }
}


- (id)initWithCustomer:(NSMutableDictionary *)c
{
    self = [self init];
    
    if (self) { 
    }
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getServiceDidGetOrderAmount:[[SWDatabaseManager retrieveManager] dbGetOrderAmountForAvl_Balance:[self.customerDictornary stringForKey:@"Customer_ID"]]];
    visitOptionCollectionViewArray = [[NSMutableArray alloc]init];
    
    [SWDefaults clearSignature];
    [Flurry logEvent:@"Main Visit Option View"];
    
    [self getDistributionServiceDidCheckVisitID: [[SWDatabaseManager retrieveManager] checkCurrentVisitInDC:[SWDefaults currentVisitID]]];
    
    AppControl *appControl = [AppControl retrieveSingleton];
    isOrderHistoryEnable = appControl.ENABLE_ORDER_HISTORY;
    isCollectionEnable = appControl.ENABLE_COLLECTION;
    isDistributionEnable = appControl.ENABLE_DISTRIB_CHECK;
    
    NSMutableDictionary *pointsOfPurchaseDict=[[NSMutableDictionary alloc]init];
    [pointsOfPurchaseDict setValue:@"POP" forKey:@"Title"];
    [pointsOfPurchaseDict setValue:@"POP" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:pointsOfPurchaseDict];
    
    NSMutableDictionary *distributionCheckDict=[[NSMutableDictionary alloc]init];
    [distributionCheckDict setValue:@"Distribution Check" forKey:@"Title"];
    [distributionCheckDict setValue:@"Distribution_Check" forKey:@"ThumbNailImage"];
    
    if([isDistributionEnable isEqualToString:@"Y"])
    {
        [visitOptionCollectionViewArray addObject:distributionCheckDict];
        isDistribution=YES;
    }
    
    NSMutableDictionary *salesOrdreDict=[[NSMutableDictionary alloc]init];
    [salesOrdreDict setValue:@"Sales Order" forKey:@"Title"];
    [salesOrdreDict setValue:@"Sales_Order" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:salesOrdreDict];
    
    NSMutableDictionary *manageOrderDict=[[NSMutableDictionary alloc]init];
    [manageOrderDict setValue:@"Manage Order" forKey:@"Title"];
    [manageOrderDict setValue:@"Manage_Order" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:manageOrderDict];
    
    NSMutableDictionary *orderHistoryDict=[[NSMutableDictionary alloc]init];
    [orderHistoryDict setValue:@"Order History" forKey:@"Title"];
    [orderHistoryDict setValue:@"Order_History" forKey:@"ThumbNailImage"];
    
    
    if([isOrderHistoryEnable isEqualToString:@"Y"])
    {
        [visitOptionCollectionViewArray addObject:orderHistoryDict];
    }
    NSMutableDictionary *returnDict=[[NSMutableDictionary alloc]init];
    [returnDict setValue:@"Return" forKey:@"Title"];
    [returnDict setValue:@"Return" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:returnDict];
    
    NSMutableDictionary *surveyDict=[[NSMutableDictionary alloc]init];
    [surveyDict setValue:@"Survey" forKey:@"Title"];
    [surveyDict setValue:@"Survey" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:surveyDict];
    
    NSMutableDictionary *collectionDict=[[NSMutableDictionary alloc]init];
    [collectionDict setValue:@"Collection" forKey:@"Title"];
    [collectionDict setValue:@"Collection" forKey:@"ThumbNailImage"];
    if([isCollectionEnable isEqualToString:@"Y"])
    {
        [visitOptionCollectionViewArray addObject:collectionDict];
        isCollection=YES;
    }
    NSMutableDictionary *sendOrderDict=[[NSMutableDictionary alloc]init];
    [sendOrderDict setValue:@"Send Order" forKey:@"Title"];
    [sendOrderDict setValue:@"Send_Order" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:sendOrderDict];
    
    
    NSMutableDictionary *feedbackDict=[[NSMutableDictionary alloc]init];
    [feedbackDict setValue:@"Feedback" forKey:@"Title"];
    [feedbackDict setValue:@"Feedback" forKey:@"ThumbNailImage"];
    [visitOptionCollectionViewArray addObject:feedbackDict];
}

- (void)getServiceDidGetOrderAmount:(NSArray *)orderAmmount{
    
    [SWDefaults setCustomer:self.customerDictornary];
    self.customerDictornary = [SWDefaults validateAvailableBalance:orderAmmount];
    
    discountRate = [NSString stringWithFormat:@"%@ : ",NSLocalizedString(@"Available Balance", nil)];
    discountRate= [discountRate stringByAppendingString:[[SWDefaults availableBalance] currencyString]];
    discountLable.text = discountRate ;
    orderAmmount=nil;
}

#pragma mark UICOllectionview methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  visitOptionCollectionViewArray.count;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(visitOptionCollectionViewArray.count>8){
        return CGSizeMake(220, 180);
    }
    return CGSizeMake(200, 180);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if(visitOptionCollectionViewArray.count>8){
        return 10.0;
    }
    return 20.0;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *cellIdentifier = @"visitCell";
    
    SalesWorxVisitOptionsCollectionViewCell *cell = (SalesWorxVisitOptionsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.contentView.layer.cornerRadius = 8.0;
    cell.contentView.layer.masksToBounds = YES;
    
    NSMutableArray *data = [visitOptionCollectionViewArray objectAtIndex:indexPath.row];
    cell.optionTitleLbl.text = [data valueForKey:@"Title"];
    cell.optionImageView.image = [UIImage imageNamed:[data valueForKey:@"ThumbNailImage"]];
    return cell;

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* selectedCellTitle=[[visitOptionCollectionViewArray objectAtIndex:indexPath.row ] valueForKey:@"Title"];
    if ([selectedCellTitle isEqualToString:@"POP"]) {
        NSString*  customerID=[customerDictornary valueForKey:@"Customer_ID"];
        NSString*  siteUseID=[customerDictornary valueForKey:@"Site_Use_ID"];
        
        NSString* popQry=[NSString stringWithFormat:@"select IFNULL(POP_ID,0) AS POP_ID, IFNULL(POP_MONTH,0) AS POP_MONTH ,IFNULL(POP_YEAR,0) AS POP_YEAR from TBL_POP where Customer_ID='%@' and Site_Use_ID='%@'", customerID,siteUseID];
        
        NSMutableArray*  popArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:popQry];
        
        if (popArray.count>0) {
            NSString* currentVisitID=[SWDefaults currentVisitID];
            if ([NSString isEmpty:currentVisitID]==YES) {
                [self startVisit];
            }
            NSLog(@"visit id being sent to pop is %@", currentVisitID);
            PointofPurchaseViewController* popVC=[[PointofPurchaseViewController alloc]init];
            popVC.currentVisitID=currentVisitID;
            [self.navigationController pushViewController:popVC animated:YES];
        }
        else
        {
            [SWDefaults showAlertAfterHidingKeyBoard:@"POP data unavailable" andMessage:@"Please try later" withController:self];
        }
    }else if ([selectedCellTitle isEqualToString:@"Collection"]){
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
            NSLog(@"current visit id in collection is %@", [SWDefaults currentVisitID]);
        }
        NSLog(@"visit id in  defaults at collection %@", currentVisitID);
        
        
        SWCollectionViewController *collectionViewController = [[SWCollectionViewController alloc] initWithCustomer:self.customerDictornary] ;
        [self.navigationController pushViewController:collectionViewController animated:YES];
    } else if ([selectedCellTitle isEqualToString:@"Order History"]){
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
            NSLog(@"current visit id in order history is %@", [SWDefaults currentVisitID]);
        }
        
        NSLog(@"visit id in  defaults at order history %@", currentVisitID);
        
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"hideOrderHistoryHeader"];
        
        SWOrderHistoryViewController *orderHistoryViewController = [[SWOrderHistoryViewController alloc]init];
        orderHistoryViewController.customerDictornary = self.customerDictornary;
        [self.navigationController pushViewController:orderHistoryViewController animated:YES];
        orderHistoryViewController=nil;
    }else if ([selectedCellTitle isEqualToString:@"Distribution Check"]){
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
        }
        SWDistributionCheckViewController* distributionCheckVC=[[SWDistributionCheckViewController alloc]init];
        distributionCheckVC.customerDictornary = self.customerDictornary;
        [self.navigationController pushViewController:distributionCheckVC animated:YES];
    } else if ([selectedCellTitle isEqualToString:@"Manage Order"]){
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
        }
        
        Singleton *single = [Singleton retrieveSingleton];
        single.visitParentView = @"MO";
        ManageOrderViewController *viewController = [[ManageOrderViewController alloc]init] ;
        viewController.customerDictornary = self.customerDictornary;
        [self.navigationController pushViewController:viewController animated:YES];
        viewController=nil;
    } else if ([selectedCellTitle isEqualToString:@"Return"]){
        
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
            NSLog(@"current visit id in returns is %@", [SWDefaults currentVisitID]);
        }
        NSLog(@"visit id in  defaults at returns %@", currentVisitID);
        
        
        Singleton *single = [Singleton retrieveSingleton];
        single.visitParentView = @"RO";
        
        
        NSArray* categoriesArray=  [[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:self.customerDictornary];
        if (categoriesArray.count==1) {
            
            single.orderStartDate = [NSDate date];
            [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
            AlseerReturnViewController *returnView = [[AlseerReturnViewController alloc] initWithNibName:@"AlseerReturnViewController" bundle:nil] ;
            returnView.customerDict = customerDictornary;
            
            [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
            
            [self.navigationController pushViewController:returnView animated:YES];
        }
        
        else
        {
            ReturnProductCategoryViewController *productSaleCategoryViewController = [[ReturnProductCategoryViewController alloc] initWithCustomer:self.customerDictornary] ;
            [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
            productSaleCategoryViewController=nil;
        }
    }
    else if([selectedCellTitle isEqualToString:@"Sales Order"]){
        NSString* customerID=[NSString stringWithFormat:@"%@", [self.customerDictornary valueForKey:@"Customer_ID"]];
        if ([[SWDatabaseManager isCustomerBlocked:customerID]isEqualToString:@"N"]) {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Customer Blocked" andMessage:@"Sales Order cannot be taken for a blocked customer" withController:self];
        }
        else
        {
            if ([SWDefaults fetchOnsiteVisitStatus]==YES) {
                
                Singleton *single = [Singleton retrieveSingleton];
                BOOL dcStatusForOnsiteVisit=[SWDefaults fetchDCOptionalStatusforOnsiteVisit];
                
                if ( single.isDistributionChecked==NO || dcStatusForOnsiteVisit ==YES) {
                    [self startSalesOrderwithOptions];
                }
                else {
                    [SWDefaults showAlertAfterHidingKeyBoard:@"This is an Onsite Visit" andMessage:@"Please complete distribution check before starting a sales order" withController:self];
                }
            }
            else
            {
                [self startSalesOrderwithOptions];
            }
        }
    } else if ([selectedCellTitle isEqualToString:@"Survey"]){
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (!currentVisitID) {
            [self startVisit];
            NSLog(@"current visit id in sales order is %@", [SWDefaults currentVisitID]);
        }
        
        
        
        NSLog(@"visit id in  defaults  at sales order %@", currentVisitID);
        
        SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
        objSurveyVC.surveyParentLocation=@"Visit";
        [self.navigationController pushViewController:objSurveyVC animated:YES];
        objSurveyVC=nil;
    }else if ([selectedCellTitle isEqualToString:@"Feedback"]){
        NSMutableArray *arrItemsForFeedback = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FeedBack_Request where Customer_ID = '%@' and Status = 'N' and Expiry_Date > datetime('now', 'localtime')",[customerDictornary valueForKey:@"Customer_ID"]]];
        
        if (arrItemsForFeedback.count > 0) {
            NSString* currentVisitID=[SWDefaults currentVisitID];
            if (currentVisitID.length == 0) {
                [self startVisit];
                currentVisitID = [SWDefaults currentVisitID];
            }
            
            SalesWorxFeedbackViewController *feedbackViewController = [[SalesWorxFeedbackViewController alloc] initWithCustomer:self.customerDictornary andVisitID:currentVisitID] ;
            [self.navigationController pushViewController:feedbackViewController animated:YES];
        } else {
            [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"No feedback pending" withController:self];
        }
    }else{
        
    }

}
-(void)selectedVisitOption:(NSString*)visitOption;
{
    
    
    //if dc is optional is N all the buttons except dc button will be disabled untill user completed dc, new logic is even if dc optinal is N and visit is telephonic we are enabling all the buttons and allowing user to start sales order

    NSLog(@"delegate called with selected visit %@", visitOption);
    
    NSString *isDCFlag = [[SWDefaults userProfile] objectForKey:@"Is_DC_Optional"];

    if ([visitOption isEqualToString:@"Telephonic"] &&  [isDCFlag isEqualToString:@"N"]) {
        
        salesOrderButton.enabled=YES;
        manageOrdersButton.enabled=YES;
        returnsButton.enabled=YES;
        orderHistoryButton.enabled=YES;
        surveyButton.enabled=YES;
        
        
        
        //the following flag is for, if dc optional is N and visit is telephonic we are enabling buttons based on this delegate how ever if user starts a visit and comes back to visit options view will appear is getting called so added a bool class object dcNotOptionalVisitTelephonic which will be check while enabling the buttons when status is checjed from view will appear
        dcNotOptionalVisitTelephonic=YES;



        
        
    }
    
}


-(void)pointofPurchaseTapped

{
    
    
    //check if pop has data
    
    
  NSString*  customerID=[customerDictornary valueForKey:@"Customer_ID"];
   NSString*  siteUseID=[customerDictornary valueForKey:@"Site_Use_ID"];
    
    NSString* popQry=[NSString stringWithFormat:@"select IFNULL(POP_ID,0) AS POP_ID, IFNULL(POP_MONTH,0) AS POP_MONTH ,IFNULL(POP_YEAR,0) AS POP_YEAR from TBL_POP where Customer_ID='%@' and Site_Use_ID='%@'", customerID,siteUseID];
    
    
    NSLog(@"check pop query %@", [popQry description]);
    
    
  NSMutableArray*  popArray=[[SWDatabaseManager retrieveManager]fetchDataForQuery:popQry];
    
    if (popArray.count>0) {
        
        NSLog(@"current visit id in pop %@", [SWDefaults currentVisitID]);

        NSString* currentVisitID=[SWDefaults currentVisitID];
        if ([NSString isEmpty:currentVisitID]==YES) {
            [self startVisit];
        }
        
        NSLog(@"visit id being sent to pop is %@", currentVisitID);
        PointofPurchaseViewController* popVC=[[PointofPurchaseViewController alloc]init];
        popVC.currentVisitID=currentVisitID;
        [self.navigationController pushViewController:popVC animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"POP data unavailable" andMessage:@"Please try later" withController:self];
    }
}

-(void)startVisit
{
    NSLog(@"start visit method called with visit id %@", [SWDefaults currentVisitID]);
    
    
    
    visitID=[NSString createGuid];
    [SWDefaults setCurrentVisitID:visitID];
    
    
    NSMutableString *documentsDirectory=[[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] mutableCopy];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:nil];

    
    NSFileManager * fm =[NSFileManager defaultManager];
    NSError *error = nil;
    for (NSString *file in [fm contentsOfDirectoryAtPath:documentsDirectory error:&error]) {
        
        NSLog(@"trying to remove file at path %@",[[NSString stringWithFormat:@"%@", documentsDirectory] stringByAppendingPathComponent:file]);
        BOOL success = [fm removeItemAtPath:[[NSString stringWithFormat:@"%@", documentsDirectory] stringByAppendingPathComponent:file] error:&error];
        if (!success || error) {
            // it failed.
            NSLog(@"failed to remove image with error %@", error);
        }
        else{
            NSLog(@"removing items from temp");
        }
        
    }
    //routeSer.delegate=self;
    [[SWDatabaseManager retrieveManager] saveVistWithCustomerInfo:self.customerDictornary andVisitID:visitID];
    
    SWAppDelegate *appDelegate = [[SWAppDelegate alloc]init];
    [appDelegate saveLocationData:YES];
}





- (void)viewDidAppear:(BOOL)animated {
    
    
    
    //
    
    [super viewDidAppear:animated];
    [scrollView setContentSize:CGSizeMake(scrollView.bounds.size.width, scrollView.bounds.size.height + 1)];


}

-(void)openImageCapture
{
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in collection is %@", [SWDefaults currentVisitID]);
    }
    SalesWorxCameraRollViewController * cameraRoll=[[SalesWorxCameraRollViewController alloc]init];
    cameraRoll.customerDictornary=self.customerDictornary;
    [self.navigationController pushViewController:cameraRoll animated:YES];
}



- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [scrollView setContentSize:CGSizeMake(scrollView.bounds.size.width, scrollView.bounds.size.height + 1)];
}

- (void)setupToolbar {
    
}


#pragma mark UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    
    if ( alertView.tag==99999 ) {
        
    }
    else
    {
    
    
        switch (buttonIndex) {
            case 0:
                
                if (alertView.tag==1000) {
                    [self performSelector:@selector(closeVisitWithEndDate:) withObject:nil afterDelay:0.0];

                }
                else
                {
                [self performSelector:@selector(closeVisitWithEndDate:) withObject:nil afterDelay:0.0];
                }
                break;
            case 1:
                
                if (alertView.tag==1000) {
                    
                }
                else
                {
                
                [self performSelector:@selector(closeVisitWithOutEndDate:) withObject:nil afterDelay:0.0];
                }
                break;
            default:
                break;
        }
    }
}
- (void)closeVisit:(id)sender {
    
    //count a visit as complete if either DC/POP/Sales Order/Return?Collection are done, then only count the visit as complete, else delete the visit entry from table.
    
    NSString* currentVisitID=[SWDefaults currentVisitID];

    BOOL isTransactionDone=[self isTransactionDone:currentVisitID];
    
    
    if (!currentVisitID) {
        NSLog(@"current visit id in close visit %@", currentVisitID);
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (isTransactionDone==YES)
    {
        UIAlertView* closeVisit=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", nil) message:NSLocalizedString(@"Do you want to close this visit?", nil) delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
        closeVisit.tag=1000;
        [closeVisit show];
        
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Visit cannot be closed" andMessage:NSLocalizedString(@"There are no transactions done in this visit.", nil) withController:self];
    }
}

-(BOOL)isTransactionDone:(NSString*)transactionVisitID
{
    
    //check if VISIT_TRANSACTION_MANDATORY is enable if not then retrun yes by default
    BOOL status=NO;

    NSError * contentError;
    
    NSArray * visitImagesArray=[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[[NSMutableString stringWithString:[SWDefaults applicationDocumentsDirectory]] stringByAppendingPathComponent:@"Visit Images"] stringByAppendingPathComponent:@"Temp"] error:&contentError];
    
    if (visitImagesArray.count>0) {
        [[SWDatabaseManager retrieveManager] saveCapturedImagestoVisitAdditionalInfo];
    }

    
    AppControl* appControl=[AppControl retrieveSingleton];
    NSString* visitTransactionFlag=appControl.VISIT_TRANSACTION_MANDATORY;
    
    if ([visitTransactionFlag isEqualToString:@"Y"]) {
    NSString* salesOrderCountQry=[NSString stringWithFormat:@"select * from TBL_Order where Visit_ID='%@'",transactionVisitID];
    NSMutableArray *salesOrderCountArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesOrderCountQry];
    if (salesOrderCountArray.count>0) {
        NSLog(@"sales order transaction available");
        status=YES;
    }
    
    
    NSString* salesReturnCountQry=[NSString stringWithFormat:@"select * from TBL_RMA where Visit_ID='%@' ",transactionVisitID];
    NSMutableArray *salesReturnCountArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:salesReturnCountQry];
    if (salesReturnCountArray.count>0) {
        NSLog(@"returns transaction available");
        status=YES;
    }
    
    NSString* popRemarksCountQuery=[NSString stringWithFormat:@"select * from TBL_POP_Remarks where Custom_attribute_1='%@'",transactionVisitID];
    NSMutableArray * popRemarksCountArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:popRemarksCountQuery];
    if (popRemarksCountArray.count>0) {
        NSLog(@"POP transaction available");
        status=YES;
    }
    
    NSString * dcItemsCountQuery=[NSString stringWithFormat:@"select * from TBL_Distribution_Check where Visit_ID='%@'",transactionVisitID];
    NSMutableArray *dcCountArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:dcItemsCountQuery];
    if (dcCountArray.count>0) {
        NSLog(@"DC transaction available");
        status=YES;
    }
    
    
       

    
    NSString * imagesQuery=[NSString stringWithFormat:@"select Attrib_Value from TBL_Visit_Addl_Info where Attrib_Name ='Visit_IMG'  and Visit_ID='%@'",transactionVisitID];
    NSMutableArray *imagesCountArray=[[SWDatabaseManager retrieveManager] fetchDataForQuery:imagesQuery];
    if (imagesCountArray.count>0) {
        NSLog(@"images transaction available");
        status=YES;
    }
    
    }
    else{
        //VISIT_TRANSACTION_MANDATORY is N so disabling the check
        status=YES;
    }
    
    
    
    return status;
}

- (void)closeVisitWithOutEndDate:(id)sender
{
    
    Singleton *single = [Singleton retrieveSingleton];
    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
    [visitManager closeVisit];

    if ([single.isCashCustomer isEqualToString:@"Y"]) {
        single.popToCash = @"Y";
    }
    else
    {
        single.popToCash = @"N";

    }

     [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
     [self.navigationController  popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];

}
- (void)closeVisitWithEndDate:(id)sender
{
    //routeSer.delegate=self;
    //[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]];
 
    //clear visit id from defaults
    
    
    if ([SWDefaults currentVisitID]) {
        [self getRouteServiceDidGetRoute:[[SWDatabaseManager retrieveManager] dbGetCollectionFromDate:[NSDate date]]];
        NSLog(@"clearing current visit id %@",[SWDefaults currentVisitID]);
        
        [SWDefaults clearCurrentVisitID];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)getRouteServiceDidGetRoute:(NSArray *)r{
    if(![r count]==0)
    {
        for (int i=0; i<[r count]; i++)
        {

            if([[self.customerDictornary stringForKey:@"Ship_Customer_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Customer_ID"]] && [[self.customerDictornary stringForKey:@"Ship_Site_Use_ID"] isEqualToString:[[r objectAtIndex:i] stringForKey:@"Ship_Site_Use_ID"]])
            {
                //routeSer.delegate=self;
                [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[[r objectAtIndex:i] stringForKey:@"Planned_Visit_ID"]];
                
                NSString* tempVisitID=[SWDefaults currentVisitID];
                if (tempVisitID) {
                    [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
                }
            }
            
        }
    }
    if ([self.customerDictornary objectForKey:@"Planned_Visit_ID"])
    {
        [[SWDatabaseManager retrieveManager] saveVisitStatusWithVisiteID:[self.customerDictornary stringForKey:@"Planned_Visit_ID"]];
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    else
    {
        NSString* tempVisitID=[SWDefaults currentVisitID];
        if (tempVisitID) {
            [[SWDatabaseManager retrieveManager] saveVisitEndDateWithVisiteID:[SWDefaults currentVisitID]];
        }
    }
    
    
    
    
    NSString* tempVisitID=[SWDefaults currentVisitID];

    if (tempVisitID) {
        
        
        
        
    NSMutableArray *locationDict = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"select Latitude , Longitude from TBL_FSR_Actual_Visits where Actual_Visit_ID='%@'",[SWDefaults currentVisitID]]];
        
        
        NSLog(@"check the location dict %@", [locationDict description]);
      
        
        if (locationDict.count>0) {
            
        
        
    if ([[[locationDict objectAtIndex:0]stringForKey:@"Latitude"]isEqualToString:@"0"] || [[[locationDict objectAtIndex:0]stringForKey:@"Longitude"]isEqualToString:@"0"])
    {
       // #define kSQLUpdateEndDate @"UPDATE TBL_FSR_Actual_Visits SET Visit_End_Date='{0}' WHERE Actual_Visit_ID='{1}'; "
        NSString *latitudePoint,*longtitudePoint;
        latitudePoint=@"";
        longtitudePoint = @"";
        
        for (int i= 0; i <=3; i++) {
            //NSLog(@"Location captureing ...ellloooo!!!!");
            
            locationManager = [[CLLocationManager alloc] init];
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            
            [locationManager startUpdatingLocation];
            CLLocation *location = [locationManager location];
            CLLocationCoordinate2D coordinate = [location coordinate];
            latitudePoint = [NSString stringWithFormat:@"%f", coordinate.latitude];
            longtitudePoint = [NSString stringWithFormat:@"%f", coordinate.longitude];
            [locationManager stopUpdatingLocation];
            if ([latitudePoint isEqualToString:@"0"] || [longtitudePoint isEqualToString:@"0"]) {
            }
            else
            {
                break;
            }
        }
        
        
//        NSLog(@"latitude %@",latitudePoint);
//        NSLog(@"longitude %@",longtitudePoint);
        [[SWDatabaseManager retrieveManager] executeNonQuery:[NSString stringWithFormat:@"UPDATE TBL_FSR_Actual_Visits SET Latitude = '%@',Longitude='%@' Where Actual_Visit_ID='%@'",latitudePoint,longtitudePoint,[SWDefaults currentVisitID]]];
    }
    
        }
    
    
    
    SWVisitManager *visitManager = [[SWVisitManager alloc] init];
    [visitManager closeVisit];
    
   Singleton *single = [Singleton retrieveSingleton];
    if ([single.isCashCustomer isEqualToString:@"Y"]) {
        single.popToCash = @"Y";
    }
    else
    {
        single.popToCash = @"N";
        
    }
    r=nil;
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
    }
    
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"went to visit options no activity done");
    }
}


- (void)startCollection:(id)sender {
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in collection is %@", [SWDefaults currentVisitID]);
    }
    NSLog(@"visit id in  defaults at collection %@", currentVisitID);

    
    SWCollectionViewController *collectionViewController = [[SWCollectionViewController alloc] initWithCustomer:self.customerDictornary] ;
    [self.navigationController pushViewController:collectionViewController animated:YES];
    collectionViewController=nil;
}

- (void)startOrderHistory:(id)sender {
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in order history is %@", [SWDefaults currentVisitID]);
    }
    
    NSLog(@"visit id in  defaults at order history %@", currentVisitID);

    
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"hideOrderHistoryHeader"];
    
    SWOrderHistoryViewController *orderHistoryViewController = [[SWOrderHistoryViewController alloc]init];
    orderHistoryViewController.customerDictornary = self.customerDictornary;
    [self.navigationController pushViewController:orderHistoryViewController animated:YES];

    orderHistoryViewController=nil;
}

- (void)startDistributionCheck:(id)sender {
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in distribution check is %@", [SWDefaults currentVisitID]);
    }
    
    NSLog(@"visit id in  defaults at distribution check  %@", currentVisitID);
}
- (void)startManageOrder:(id)sender
{
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in manage order is %@", [SWDefaults currentVisitID]);
    }
    
    NSLog(@"visit id in  defaults at manage orders %@", currentVisitID);

   Singleton *single = [Singleton retrieveSingleton];
    single.visitParentView = @"MO";
    ManageOrderViewController *viewController = [[ManageOrderViewController alloc]init] ;
    viewController.customerDictornary = self.customerDictornary;
    [self.navigationController pushViewController:viewController animated:YES];
    viewController=nil;
}
- (void)startReturn:(id)sender
{
    
    
    
 
    
    
    
    
    
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in returns is %@", [SWDefaults currentVisitID]);
    }
    NSLog(@"visit id in  defaults at returns %@", currentVisitID);

    
   Singleton *single = [Singleton retrieveSingleton];
    single.visitParentView = @"RO";
    
    
    NSArray* categoriesArray=  [[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:self.customerDictornary];

    
    
    
    if (categoriesArray.count==1) {
    
         single.orderStartDate = [NSDate date];
         [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
         AlseerReturnViewController *returnView = [[AlseerReturnViewController alloc] initWithNibName:@"AlseerReturnViewController" bundle:nil] ;
         returnView.customerDict = customerDictornary;
        
        [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];

         [self.navigationController pushViewController:returnView animated:YES];
         
        
        
    }
    
    else
    {
    
    
    ReturnProductCategoryViewController *productSaleCategoryViewController = [[ReturnProductCategoryViewController alloc] initWithCustomer:self.customerDictornary] ;
    [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
    productSaleCategoryViewController=nil;
    }
}




-(void)startSalesOrderwithOptions

{
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in sales order is %@", [SWDefaults currentVisitID]);
    }
    NSArray* categoriesArray=  [[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:self.customerDictornary];
    Singleton *single = [Singleton retrieveSingleton];
    single.visitParentView = @"SO";
    
    if ([[SWDefaults Over_Due] intValue] > 0 || [[SWDefaults availableBalance] intValue] <= 0)
    {
        if ([SWDefaults Over_Due].length>0||[SWDefaults availableBalance].length>0) {
            single.isDueScreen=NO;
            if (categoriesArray.count==1) {
                
                
                AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
                [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
                
                newSalesOrder.wareHouseDataSalesOrderArray=[categoriesArray mutableCopy];
                newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customerDictornary];
                newSalesOrder.parentViewString = @"SO";
                newSalesOrder.preOrderedItems=[NSMutableArray new];
                newSalesOrder.preOrdered = [NSMutableDictionary new];
                newSalesOrder.isOrderConfirmed=@"";
                
                single.orderStartDate = [NSDate date];
                
                [self.navigationController pushViewController:newSalesOrder animated:YES];
                newSalesOrder = nil;
            }
            else
            {
                ProductSaleCategoryViewController *productSaleCategoryViewController = [[ProductSaleCategoryViewController alloc] initWithCustomer:self.customerDictornary] ;
                [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
                productSaleCategoryViewController=nil;
            }
        }
        else
        {
            single.isDueScreen=YES;
            SWDuesView *duesView = [[SWDuesView alloc] initWithCustomer:self.customerDictornary] ;
            duesView.categoriesArrayFromVisitOptions=categoriesArray;
            [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
            [self.navigationController pushViewController:duesView animated:YES];
            duesView=nil;
        }
    }
    
    else
    {
        single.isDueScreen=NO;
        
        
        NSLog(@"check categories array in visit options %@", [categoriesArray description]);
        
        if (categoriesArray.count==1) {
            
            AlSeerSalesOrderViewController *newSalesOrder =[[AlSeerSalesOrderViewController alloc] initWithNibName:@"AlSeerSalesOrderViewController" bundle:nil];
            [SWDefaults setProductCategory:[categoriesArray objectAtIndex:0]];
            newSalesOrder.wareHouseDataSalesOrderArray=[categoriesArray mutableCopy];
            newSalesOrder.customerDict = [NSMutableDictionary dictionaryWithDictionary:customerDictornary];
            newSalesOrder.parentViewString = @"SO";
            newSalesOrder.preOrderedItems=[NSMutableArray new];
            newSalesOrder.preOrdered = [NSMutableDictionary new];
            newSalesOrder.isOrderConfirmed=@"";
            single.orderStartDate = [NSDate date];
            [self.navigationController pushViewController:newSalesOrder animated:YES];
            newSalesOrder = nil;
        }
        else
        {
            
            ProductSaleCategoryViewController *productSaleCategoryViewController = [[ProductSaleCategoryViewController alloc] initWithCustomer:self.customerDictornary] ;
            [self.navigationController pushViewController:productSaleCategoryViewController animated:YES];
            productSaleCategoryViewController=nil;
        }
    }
    
}

-(void)startSurvey:(id)sender
{
    
    NSString* currentVisitID=[SWDefaults currentVisitID];
    if (!currentVisitID) {
        [self startVisit];
        NSLog(@"current visit id in sales order is %@", [SWDefaults currentVisitID]);
    }
    
    
    
    NSLog(@"visit id in  defaults  at sales order %@", currentVisitID);
    
    SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
    objSurveyVC.surveyParentLocation=@"Visit";
    [self.navigationController pushViewController:objSurveyVC animated:YES];
    objSurveyVC=nil;
}
- (void)getDistributionServiceDidCheckVisitID:(NSArray *)dc_ID;
{
    NSString *isDCFlag = [[SWDefaults userProfile] objectForKey:@"Is_DC_Optional"];
    
    NSLog(@"distribution check flag is %@", isDCFlag);
    

    if([isDCFlag isEqualToString:@"N"])
    {
        if([dc_ID count]!=0)
        {
            // Distribution Check is done now disable it and enable rest buttons if is_DC_Optional is Y
            distributionCheckButton.enabled=NO;
            salesOrderButton.enabled=YES;
            returnsButton.enabled=YES;
            collectionsButton.enabled=YES;
            surveyButton.enabled=YES;
            manageOrdersButton.enabled=YES;
            orderHistoryButton.enabled=YES;
            feedbackButton.enabled = YES;
        }
        else
        {
            // Distribution Check is not done now disable rest buttons if is_DC_Optional is Y
            
            
            //update is is dc optional is no then these buttons are disabled, now adding small correction, enable all these buttons even if dd optional is no and visit is telephonic,, this is not getting called when popover is dimissed so go for delegate
            
            
            if ([isDCFlag isEqualToString:@"N"] && [SWDefaults fetchOnsiteVisitStatus]==NO ) {
                
                salesOrderButton.enabled=YES;
                
            }
            
            if (dcNotOptionalVisitTelephonic==YES) {
                
                NSLog(@"bool from delegate called");
                distributionCheckButton.enabled=YES;
                salesOrderButton.enabled=YES;
                returnsButton.enabled=YES;
                collectionsButton.enabled=YES;
                surveyButton.enabled=YES;
                manageOrdersButton.enabled=YES;
                orderHistoryButton.enabled=YES;
                feedbackButton.enabled = YES;
            }
            else
            {
                distributionCheckButton.enabled=YES;
                salesOrderButton.enabled=NO;
                returnsButton.enabled=NO;
                collectionsButton.enabled=NO;
                surveyButton.enabled=NO;
                manageOrdersButton.enabled=NO;
                orderHistoryButton.enabled=NO;
                feedbackButton.enabled = NO;
            }
        }
    }
    else
    {
        if([dc_ID count]!=0)
        {
            distributionCheckButton.enabled=NO;
        }
        else
        {
            distributionCheckButton.enabled=YES;

        }
        
    }
    Singleton *single = [Singleton retrieveSingleton];
    single.isDistributionChecked = distributionCheckButton.enabled;
    dc_ID=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    // Dispose of any resources that can be recreated.
}

#pragma mark Send Order status notification methods


- (void)checksendOrderStatus:(NSNotification *) notification
{
    NSLog(@"order status in visit option is %@", [notification userInfo]);
    
    NSDictionary *notificationStatusDict = notification.userInfo;
    
    NSDictionary * notificationContent= [notificationStatusDict objectForKey:@"sendOrderStatus"];

    NSLog(@"send order dict is %@", [notificationContent description]);
    
    
    
}

-(void)notificationReceived:(NSNotification *)notification {
    NSLog(@"order status in visit option is %@", [notification object]);

    NSLog(@" NOTIFICATION RECEIVED");
    
    if ([[notification object]isEqualToString:@"Successful"]) {

        if (!orderSentSuccessAlert) {
            
            orderSentSuccessAlert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"orders sent successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            orderSentSuccessAlert.tag=99999;
            [orderSentSuccessAlert show];
        }
    }
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    orderSentSuccessAlert=nil;
}

- (void)feedbackTapped:(id)sender {
    
    NSMutableArray *arrItemsForFeedback = [[SWDatabaseManager retrieveManager]fetchDataForQuery:[NSString stringWithFormat:@"SELECT * FROM TBL_FeedBack_Request where Customer_ID = '%@' and Status = 'N' and Expiry_Date > datetime('now', 'localtime')",[customerDictornary valueForKey:@"Customer_ID"]]];
    
    if (arrItemsForFeedback.count > 0) {
        NSString* currentVisitID=[SWDefaults currentVisitID];
        if (currentVisitID.length == 0) {
            [self startVisit];
            currentVisitID = [SWDefaults currentVisitID];
        }
        
        SalesWorxFeedbackViewController *feedbackViewController = [[SalesWorxFeedbackViewController alloc] initWithCustomer:self.customerDictornary andVisitID:currentVisitID] ;
        [self.navigationController pushViewController:feedbackViewController animated:YES];
    } else {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Message" andMessage:@"No feedback pending" withController:self];
    }
}

@end
