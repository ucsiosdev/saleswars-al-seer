//
//  GroupSectionHeaderView.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/5/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "GroupSectionHeaderView.h"
#import "SWFoundation.h"

@implementation GroupSectionHeaderView

@synthesize titleLabel;

- (id)initWithWidth:(CGFloat)width text:(NSString *)text {
    self = [self initWithFrame:CGRectMake(0, 0, width, 28) text:text];
    
    if (self) {
    
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame text:(NSString *)text {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        
        self.titleLabel=nil;
        [self setTitleLabel:[[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.bounds.size.width - 20, self.bounds.size.height)]];
        [self.titleLabel setText:text];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setTextColor:UIColorFromRGB(0x4A5866)];
        [self.titleLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.titleLabel setShadowColor:[UIColor whiteColor]];
        [self.titleLabel setFont:BoldSemiFontOfSize (18.0f)];
        [self.titleLabel setNumberOfLines:0];
        [self.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        
        [self addSubview:self.titleLabel];
    }
    
    return self;
}


- (void)layoutSubviews {
    [super layoutSubviews];
        if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0)
        {
    //[self setBackgroundColor:UIColorFromRGB(0xE0E5Ec)];
            [self.titleLabel setFrame:CGRectMake(10, 0, self.bounds.size.width - 20, self.bounds.size.height)];

        }
    else
    {
        [self.titleLabel setFrame:CGRectMake(55, 0, self.bounds.size.width - 110, self.bounds.size.height)];

    }

}

@end
