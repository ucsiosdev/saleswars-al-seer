//
//  StockInfoTableViewController.m
//  Salesworx
//
//  Created by Syed Ismail Ahamed on 5/1/14.
//  Copyright (c) 2014 msaad. All rights reserved.
//

#import "StockInfoTableViewController.h"
#import "StockInfoTableViewCell.h"
#import "StockInfoTableHeader.h"
#import "SWAppDelegate.h"
#import "SWDefaults.h"
#import "SWDatabaseManager.h"
#import "SWFoundation.h"
#import "StockInfoTableFooterCell.h"
#import "NSDictionary+Additions.h"
#import "StockInfoSalesHistoryViewController.h"


@interface StockInfoTableViewController ()

@end

@implementation StockInfoTableViewController
@synthesize TBL_Product_Dictionary,productCodeText,productCodeString,productTextHeader,productName,lblProductCode,stockArray,productQtyTxtFieldText,saveButton,stockTblView,headerLabel,pVC,savedStockInfoArray,save;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    save.hidden=YES;
    
    //[stockTblView reloadData];
    
    [self  parseDatabase];
    //[saveButton setHidden:YES];

    
    headerLabel.backgroundColor=UIColorFromRGB(0x4790D2);
    
    
    // checking user defaults
    
   // cell=[[StockInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"identifier"];
    
    
    
    
    savedTextFieldArray=[[NSMutableArray alloc]init];
    
    NSLog(@"product code string is %@", productCodeString);
    // Do any additional setup after loading the view from its nib.
    
    if (@available(iOS 15.0, *)) {
        stockTblView.sectionHeaderTopPadding = 0;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
   

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark data base methods

-(void) parseDatabase
{
    NSLog(@"check product string before passing it to db %@", productCodeString);
    
   
  
    
    //getting item id from TBL_Product  
    
    
   
    NSString * sqlString=[NSString stringWithFormat:@" Select Inventory_Item_ID from TBL_Product where Item_Code = '%@' " , productCodeString];
    
   
    
    NSArray * tbl_Product_Array=[[SWDatabaseManager retrieveManager]fetchDataForQuery:sqlString];
   
    
    NSString * result = [[tbl_Product_Array valueForKey:@"Inventory_Item_ID"] componentsJoinedByString:@""];
    
 
    NSLog(@"result %@", result);

    
    
    //converting response array to string
//    
//    NSDictionary * sampleDic= [self indexKeyedDictionaryFromArray:tbl_Product_Array];
//    
//
//    
//   NSString * samplestr1= [sampleDic stringForKey:@"Inventory_Item_ID"];
//  
//    
//    NSString * sampleStr=[tbl_Product_Array  valueForKey:@"Inventory_Item_ID"];
  
    
    
  //  NSArray * response=[[SWDatabaseManager retrieveManager]dbGetStockInfo:sampleStr];
    
   // NSLog(@"Response is %@", [response description]);
    
    
   // NSLog(@"stock array contents %@", [stockArray valueForKey:@"Inventory_Item_ID"]);
    
    
  //passing item id to TBL_Product_Stock to get stock info
    
     stockArray= [[SWDatabaseManager retrieveManager]dbGetStockInfo:result];
    
    NSLog(@"final result %@", [stockArray description]);
    
                          //  NSArray * stockArray=[[SWDatabaseManager retrieveManager ]dbGetStockInfo:tbl_Product_Array stringForKey:@"ItemID"]
    
    
    NSLog(@"lot _no %@", [stockArray valueForKey:@"Lot_No"]);
    
//    NSMutableString * test=[[NSMutableString alloc]init];
//    
//    SWAppDelegate * appDelegate=[[UIApplication sharedApplication]delegate];
//    
//    appDelegate.itemCodeString=test;
    
    
    [productTextHeader setTextColor:[UIColor darkGrayColor]];
    [productTextHeader setFont:BoldSemiFontOfSize(16.0)];
    
    [lblProductCode setFont:LightFontOfSize(14.0)];
    
    
    lblProductCode.text=productCodeString;
    
    
    productTextHeader.text=productName;
    
    NSLog(@"product header fetched from sales order is %@",self.productCodeString);
   

    
    
    
    
    //tbl product dictionary
    
    //    NSMutableDictionary *p = [NSMutableDictionary dictionaryWithDictionary:product];
    //    [p setValue:stockAllocation forKey:@"StockAllocation"];

    
    
   
    
}


- (NSMutableDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
    id objectInstance;
    NSUInteger indexKey = 0;
    
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    for (objectInstance in array)
        [mutableDictionary setObject:objectInstance forKey:[NSNumber numberWithUnsignedInt:indexKey++]];
    
    return mutableDictionary ;
}

#pragma mark table view delegate methods


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    StockInfoTableVieCellw *cell;
//    if (cell==nil) {
//        cell=[[StockInfoTableViewCell alloc]init];
//        
//        
//        cell=[tableView dequeueReusableCellWithIdentifier:@"identifier"];
//
//    }
    
    
    
//   static NSString *CellIdentifier = @"identifier";
//    
//    StockInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[StockInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
//        
////        
////        
//       
//        
////        }
////        
////        
////        
//    }

    
    
    

    
    
    
    
    
    
   // StockInfoTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"identifier"];
    
    // Configure the cell...
    
   
//    NSLog(@"cell content %@", [stockArray valueForKey:@"Lot_No"] );
//    
//    
//     cell.lotNumberLbl.text=[[stockArray valueForKey:@"Lot_No"]objectAtIndex:indexPath.row];
//    
//    
//    
//    return cell;
    
  
    
    
    
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"StockInfoTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
        
        
           }
    
    // tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    NSUserDefaults * defaults=[NSUserDefaults standardUserDefaults];
    
    //[userDefaults setObject:savedTextFieldArray forKey:@"textFieldData"];
    
    NSMutableArray *defaultsDataArray=[[defaults arrayForKey:@"textFieldData"]mutableCopy];
    
    NSLog(@"defaults data array %@", [defaultsDataArray description]);
    
    
    
    
//    NSString * allocatedLabelfooterTxt=[[NSUserDefaults standardUserDefaults]valueForKey:totalString];
//    
//    NSLog(@"total string fetched from user defauls is %@", allocatedLabelfooterTxt);
    
    
   // for (int i=0; i< [defaultsDataArray count]-1; i++) {
    

      cell.allocatedTextField.text=[defaultsDataArray objectAtIndex:indexPath.row];
        
        
        
   // }
     //cell.allocatedTextField.text=@"";
    
   //[defaultsDataArray removeAllObjects];
    
//    NSString *domainName = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"textFieldData"];
    
    NSLog(@"checking defaults after deleting %@", [[NSUserDefaults standardUserDefaults] valueForKey:@"textFieldData"]);
    
    
    
    
    
//    [cell.lotNumberLbl setFont:LightFontOfSize(14.0f)];
//    [cell.expiryLbl setFont:LightFontOfSize(14.0f)];
//    [cell.wareHouseLbl setFont:LightFontOfSize(14.0f)];
//    [cell.quantityLbl setFont:LightFontOfSize(14.0f)];
//    
//    [cell.lotNumberLbl setBackgroundColor:[UIColor clearColor]];
//      [cell.expiryLbl setBackgroundColor:[UIColor clearColor]];
//      [cell.wareHouseLbl setBackgroundColor:[UIColor clearColor]];
//      [cell.quantityLbl setBackgroundColor:[UIColor clearColor]];
//    
//    
//    [cell.lotNumberLbl setShadowColor:[UIColor blackColor]];
//    [cell.expiryLbl setShadowColor:[UIColor blackColor]];
//    [cell.wareHouseLbl setShadowColor:[UIColor blackColor]];
//    [cell.quantityLbl setShadowColor:[UIColor blackColor]];
//    
//    [cell.lotNumberLbl setShadowOffset:CGSizeMake(0, -1)];
//    [cell.expiryLbl setShadowOffset:CGSizeMake(0, -1)];
//    [cell.wareHouseLbl setShadowOffset:CGSizeMake(0, -1)];
//    [cell.quantityLbl setShadowOffset:CGSizeMake(0, -1)];
    
    
    
    
    //Allocated textfield text
    
    
   // NSString * allocatedTextFieldStr=[[NSString alloc]init];
    
    cell.allocatedTextField.keyboardType=UIKeyboardTypeNumberPad;
    
   // cell.allocatedTextField.text=allocatedTextFieldStr;
    
    cell.allocatedTextField.tag=indexPath.row;
    
    
    cell.allocatedTextField.delegate=self;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if (cell.allocatedTextField.tag==0) {
        NSLog(@"tage here is 0");
        
        
    }
    if (cell.allocatedTextField.tag==1) {
        NSLog(@"tage here is 1");
        
        
    }
    if (cell.allocatedTextField.tag==2) {
        NSLog(@"tage here is 2");
        
        
    }
    
    //NSLog(@"Allocated text field string is %d", [cell.allocatedTextField.text integerValue]);
    
    
    
    
    
    cell.lotNumberLbl.text=[[stockArray valueForKey:@"Lot_No"]objectAtIndex:indexPath.row];
    
   
   
  numberStr=[[stockArray objectAtIndex:indexPath.row] valueForKey:@"Lot_Qty"];
    
    
   // [NSNumber numberWithInt:[numberStr intValue]];
    
    
   // cell.lotNumberLbl.text=numberStr;
    
    
    
    strtoNumber = [NSNumber numberWithInteger: [numberStr integerValue]];
    
    
    cell.quantityLbl.text=[strtoNumber stringValue];
    
    
    
    //NSString * str= [data dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle];
   
    
   

    
    NSDictionary *data = [stockArray objectAtIndex:indexPath.row];
  
    NSString * FormatedDate = [data dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle];
        
        
        
        
    
    
    
//    NSDictionary* dict = [NSDictionary dictionaryWithObjects:stockArray
//                                                                          forKeys:[stockArray valueForKey:@"Expiry_Date"]];
//    
//    
//    NSString * dateString=[dict dateStringForKey:sampleDic withDateFormat:NSDateFormatterMediumStyle];
    
    
    
    
    
//    NSDictionary * dateDic= [self indexKeyedDictionaryFromArray:stockArray];
//    
//    NSLog(@"date dic is %@", [dateDic valueForKey:@"Expiry_Date"]);
//    
//    
//    
//    NSDictionary* dict = [NSDictionary dictionaryWithObjects:stockArray
//                                                     forKeys:[stockArray valueForKey:@"Expiry_Date"]];
//    
//    NSLog(@"magic disc %@", [dict objectForKey:@"Expiry_Date"]);
    
    
    
    
    //NSString * dateString= [dateDic dateStringForKey:@"Expiry_Date" withDateFormat:NSDateFormatterMediumStyle];

    
    //NSLog(@"date formatted here %@", dateString);
    
    //working line
    //cell.expiryLbl.text=[[stockArray objectAtIndex:indexPath.row] valueForKey:@"Expiry_Date"];
    
    cell.expiryLbl.text=FormatedDate;
    
    
    
    
    cell.wareHouseLbl.text=[[stockArray objectAtIndex:indexPath.row] valueForKey:@"Org_ID"];
    
    
    [cell.lotNumberLbl setFont:LightFontOfSize(14.0)];
      [cell.quantityLbl setFont:LightFontOfSize(14.0)];
      [cell.expiryLbl setFont:LightFontOfSize(14.0)];
      [cell.wareHouseLbl setFont:LightFontOfSize(14.0)];
    
    
    
    
    
    
// cell.lotNumberLbl.text=@"text here";
    
    return cell;
    
    
    
    
    
    
    
    
}





-(void)SetFont
{
   // [lotNumberLbl setFont:LightFontOfSize(14.0f)];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    StockInfoTableHeader *header=[[[NSBundle mainBundle]loadNibNamed:@"StockInfoTableHeader" owner:self options:nil]firstObject];

   // header.backgroundColor=[UIColor lightGrayColor];
    
    header.backgroundColor=UIColorFromRGB(0xE0E5EC);
    
    
    
    [header.allocatedLbl setTextColor:[UIColor darkGrayColor]];
    [header.allocatedLbl setFont:BoldFontOfSize(14.0)];
    
    [header.quantityLbl setTextColor:[UIColor darkGrayColor]];
    [header.quantityLbl setFont:BoldFontOfSize(14.0)];
    
    [header.expiryLbl setTextColor:[UIColor darkGrayColor]];
    [header.expiryLbl setFont:BoldFontOfSize(14.0)];
    
    [header.wareHouseLbl setTextColor:[UIColor darkGrayColor]];
    [header.wareHouseLbl setFont:BoldFontOfSize(14.0)];
    
    [header.lotNumberLbl setTextColor:[UIColor darkGrayColor]];
    [header.lotNumberLbl setFont:BoldFontOfSize(14.0)];
    
    
    
    
  
    return header;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    
 footerView=[[[NSBundle mainBundle]loadNibNamed:@"StockInfoTableFooterCell" owner:self options:nil]firstObject];
    
    
    //StockInfoTableFooterCell * footerView=[[StockInfoTableFooterCell alloc]init];
    
   // footerView.backgroundColor=[UIColor cyanColor];
    
    
   // NSString * totalAvailableStock=[[NSString alloc]init];
    
    
    
    
    
    //Available stock label
    
    
    
  // NSLog(@"stock array lot qty %@", );
    
    NSMutableArray * sampleStockArray=[[NSMutableArray alloc]initWithArray:[stockArray valueForKey:@"Lot_Qty"]];
    
    
    NSLog(@"Saved array here is %@", [avblStockArray description]);
    
    int totalQty=0;
    
    for(int i=0;i<[sampleStockArray count];i++)
    {
        //int productQty=[tempString1 intValue];
        
        int indexCount =[[sampleStockArray objectAtIndex:i]intValue];
        
        
        totalQty=totalQty+indexCount;

    
    }
    
    NSLog(@"total available stock is %d", totalQty);
    
    
    NSString *totalQtyString = [NSString stringWithFormat:@"%d", totalQty];
    
    footerView.avblStockLbl.text=totalQtyString;

    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if ([stockArray count]==0) {
        NSLog(@"No Stock Available");
    
    }
    
    else
    {
//        NSString *one = [[stockArray valueForKey:@"Lot_Qty"]objectAtIndex:0];
//        NSString *two = [[stockArray valueForKey:@"Lot_Qty"]objectAtIndex:1];
//        NSString *three = [[stockArray valueForKey:@"Lot_Qty"]objectAtIndex:2];
//        
//        
//        int total=[one intValue]+[two intValue]+[three intValue];
//        
//        NSLog(@"total here is %d", total);
//        footerView.avblStockLbl.text=[NSString stringWithFormat:@"%d", total];
    }
   
    
    int total=0;
    
    
    
    for (int i=0; i< [stockArray count]; i++) {
        
       //     NSString * str1=[[stockArray valueForKey:@"Lot_Qty"]stringValue];
        
      // int  stockValue=[str1 intValue];
        
        
       
        NSString * combinedStuff = [stockArray componentsJoinedByString:@","];
        
        NSLog( @"combined %@", [combinedStuff description]);
        
        //total=total+stockValue;
        
        NSLog(@"toal finally %d", total);
    }
    
    
    
    
//    int totalGoods=0;
//    
//    Singleton *single = [Singleton retrieveSingleton];
//    for (NSDictionary *row in stockView.items)
//    {
//        int qty = [[row objectForKey:@"Lot_Qty"] intValue];
//        totalGoods = totalGoods + qty;
//    }
//    
//    
//    single.stockAvailble = [NSString stringWithFormat:@"%d",totalGoods];
//    totaleStock = [single.stockAvailble intValue];

    
    
    
    
    
    
    
    
    
    
    NSLog(@"total stock value 1 %d",total);
    
    
    //NSLog( @"total test %@", [[stockArray valueForKey:@"Lot_Qty"]objectAtIndex:0]);
    
    
   
//    int sampleval=[strtoNumber intValue];
//    
//    NSLog(@"sample val is %d", sampleval);
//    
//    
//    int sum=0;
//    
//    for (int i=0; i<[stockArray count]; i++) {
//        sum=sum+sampleval;
//        
//    }
//    
   // NSLog(@"test sum here is %d", sum);
    
//    int totalGoods=0;
//    
//    
//        int qty = [numberStr intValue];
//        
//        totalGoods = totalGoods + qty;
//    
//    NSLog(@"Total goods %d", totalGoods);
//    
    
    
    
    
   // footerView=[[StockInfoTableFooterCell alloc]init];
    
    
    
    footerView.orderQtyLbl.text=productQtyTxtFieldText;
    
    
    //footerView.allocatedQtyLbl.text= cell.allocatedTextField.text;
    
    
  NSLog(@"Available stock in footer %@", footerView.allocatedQtyLbl.text);
    
    
    footerView.backgroundColor=[UIColor whiteColor];

    //[footerView.orderQtyLbl setBackgroundColor:[UIColor whiteColor]];

    [footerView.orderQtyLbl setTextColor:[UIColor darkGrayColor]];
   [footerView.orderQtyLbl setFont:BoldFontOfSize(14.0)];
//    [footerView.orderQtyLbl setShadowColor:[UIColor blackColor]];
//    
   [footerView.allocatedQtyLbl setTextColor:[UIColor darkGrayColor]];
   [footerView.allocatedQtyLbl setFont:BoldFontOfSize(14.0)];
//    [footerView.allocatedQtyLbl setShadowColor:[UIColor blackColor]];
//    
 [footerView.avblStockLbl setTextColor:[UIColor darkGrayColor]];
    [footerView.avblStockLbl setFont:BoldFontOfSize(14.0)];
//    [footerView.avblStockLbl setShadowColor:[UIColor blackColor]];
    
    
    [footerView.avblStockStatic setTextColor:[UIColor darkGrayColor]];
    [footerView.avblStockStatic setFont:BoldFontOfSize(14.0)];
    
    [footerView.orderQtyStatic setTextColor:[UIColor darkGrayColor]];
    [footerView.orderQtyStatic setFont:BoldFontOfSize(14.0)];
    
    [footerView.allocatedQtyStatic setTextColor:[UIColor darkGrayColor]];
    [footerView.allocatedQtyStatic setFont:BoldFontOfSize(14.0)];
    
    
    
    //Allocated text
    
    
    
//    bool isAllocatedTextSaved= [[NSUserDefaults standardUserDefaults]boolForKey:@"saved"];
//    
//    if ((isAllocatedTextSaved=YES)) {
//        NSString *str=[[NSUserDefaults standardUserDefaults]valueForKey:@"AllocatedQuantityLabel"];
//        NSLog(@"value is %@", str);
//        
//        footerView.allocatedQtyLbl.text=str;
//        
//    }
    
    
    NSString *savedQty= [[NSUserDefaults standardUserDefaults]valueForKey:@"AllocatedQuantityLabel"];
    
    footerView.allocatedQtyLbl.text=savedQty;
    
    
    
    //set the content size here
    
    StockInfoSalesHistoryViewController * salesHistorVC=[[StockInfoSalesHistoryViewController alloc]init];
    
    [salesHistorVC updateStockScrollContent:stockTblView.contentSize];
    
    
    return footerView;
    
}




- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 140.0;
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//   
//    return tableView;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [stockArray count];
}




#pragma mark custom cell text field delegate method

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    /* for backspace */
    if([string length]==0){
        return YES;
    }
    
    /*  limit to only numeric characters  */
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    for (int i = 0; i < [string length]; i++) {
        unichar c = [string characterAtIndex:i];
        if ([myCharSet characterIsMember:c]) {
            return YES;
        }
    }
    
    [SWDefaults showAlertAfterHidingKeyBoard:@"Special Character Entered" andMessage:@"Please enter numeric value" withController:self];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"textFieldData"];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString* output = nil;
    if([textField.text hasPrefix:@"0"])
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"" andMessage:@"Allocated quantity cannot be Zero" withController:self];
        
        output = [textField.text substringFromIndex:1];
        NSLog(@"out put %@", output);
        textField.text=@"";
    }
    
    NSString *tempString = [[NSString alloc]initWithString:textField.text];
    [savedTextFieldArray addObject:tempString];
    NSLog(@"Saved array after editing ends %@", [savedTextFieldArray description]);
    total=0;
    
    for(int i=0;i<[savedTextFieldArray count];i++)
    {
        productQty=[productQtyTxtFieldText intValue];
        allocatedTotal =[[savedTextFieldArray objectAtIndex:i]intValue];
        total=total+allocatedTotal;
        if (bachButtonPressed==NO) {
            if (total>productQty) {
                [SWDefaults showAlertAfterHidingKeyBoard:@"Value Mismatch" andMessage:@"Allocated Quantity exceeds Order Quantity" withController:self];
                footerView.allocatedQtyLbl.text=@"";
                textField.text=@"";
                save.hidden=YES;
            }
            else
            {
                totalString = [NSString stringWithFormat:@"%d", total];
                footerView.allocatedQtyLbl.text=totalString;
                [[NSUserDefaults standardUserDefaults]setValue:totalString forKey:@"AllocatedQuantityLabel"];
            }
        }
        allocatedQuantityLabel=YES;
    }
    
    if (total==productQty) {
        save.hidden=NO;
    }
    
    NSLog( @"test total %d", total);
    NSLog(@"saved text filed array try %@", [savedTextFieldArray description]);
}

#pragma mark table view data source methods

- (IBAction)Save:(id)sender {
    
    if ([cell.allocatedTextField.text isEqualToString:@""]) {
        cell.allocatedTextField.text= @"";
    }
    else
    {
        for (int i=0; i< [savedTextFieldArray count]; i++) {
            cell.allocatedTextField.text=[savedTextFieldArray objectAtIndex:i];
        }
        NSLog(@"text field text is %@", cell.allocatedTextField.text);
    }
    
    [savedTextFieldArray addObject:@"0"];
    
    
    NSLog(@"saved array desc %@", [savedTextFieldArray description]);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:savedTextFieldArray forKey:@"textFieldData"];
    
    NSLog(@"defaults before saving %@", [[NSUserDefaults standardUserDefaults]valueForKey:@"textFieldData"]);
    
    if (cell.allocatedTextField.tag==0) {
        NSLog(@"tage here is 0");
    }
    if (cell.allocatedTextField.tag==1) {
        NSLog(@"tage here is 0");
    }
    if (cell.allocatedTextField.tag==2) {
        NSLog(@"tage here is 0");
    }
    NSLog(@"TextField.tag:%ld and Data %d",(long)cell.allocatedTextField.tag, [cell.allocatedTextField.text intValue]);
    
    savedStockInfoArray=[(NSArray*)stockArray mutableCopy];

    
    NSMutableDictionary * convertedDic=[self indexKeyedDictionaryFromArray:savedStockInfoArray];
    [convertedDic setObject:savedTextFieldArray forKey:@"Allocation"];
    
    NSLog(@"converted dic after setting array %@",[convertedDic description] );
    NSLog(@"Saved stock array here is %@", [savedStockInfoArray description]);
    NSLog(@"saved text field array after adding text field data is  %@", [savedStockInfoArray description]);

    saveButtonPressed=YES;
    bachButtonPressed=YES;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)Back:(id)sender {
    bachButtonPressed=YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
