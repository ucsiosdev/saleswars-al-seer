//
//  TamplateNameViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/11/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//
//TamplateNameViewController
#import "TamplateNameViewController.h"

@interface TamplateNameViewController ()

@end

@implementation TamplateNameViewController

@synthesize target;
@synthesize action,templateName;

- (id)initWithTemplateName {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
        [self setTitle:NSLocalizedString(@"Template Name", nil)];
        self.tableView.backgroundView = nil;
        self.tableView.backgroundColor = [UIColor whiteColor];
        [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Save", nil) style:UIBarButtonItemStyleDone target:self action:@selector(done:)] ];
        
        [self setPreferredContentSize:self.tableView.contentSize];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setPreferredContentSize:CGSizeMake(350, self.tableView.contentSize.height)];
}


- (void)done:(id)sender
{
    [self.view endEditing:YES];
    [((SWTextFieldCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]) resignResponder];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:templateName];
#pragma clang diagnostic pop
    
}

- (void)cancel:(id)sender {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:nil];
#pragma clang diagnostic pop
}

- (void)scroll:(EditableCell *)cell {
    [self.tableView scrollRectToVisible:cell.frame animated:YES];
}
#pragma mark UITableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @autoreleasepool
    {
        //UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        SWTextFieldCell *cell = [[SWTextFieldCell alloc] initWithReuseIdentifier:@"Text"] ;
        [((SWTextFieldCell *)cell).label setText:NSLocalizedString(@"Template Name", nil)];
        [((SWTextFieldCell *)cell).textField setKeyboardType:UIKeyboardTypeAlphabet];
        [((SWTextFieldCell *)cell).textField setPlaceholder:NSLocalizedString(@"Name", nil)];
        [((SWTextFieldCell *)cell) setDelegate:self];
        [((SWTextFieldCell *)cell).textField setText:templateName];
        return cell;
    }
}

#pragma mark UITable View delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   }

#pragma mark EditCell Delegate
- (void)editableCellDidStartUpdate:(EditableCell *)cell {
    [self performSelector:@selector(scroll:) withObject:cell afterDelay:0.4f];
}

- (void)editableCell:(EditableCell *)cell didUpdateValue:(id)value forKey:(NSString *)key {
    templateName = value;
}
@end
