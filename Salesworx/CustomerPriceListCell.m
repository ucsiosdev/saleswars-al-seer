//
//  CustomerPriceListCell.m
//  SWCustomer
//
//  Created by Irfan on 10/9/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "CustomerPriceListCell.h"
#import "SWFoundation.h"

@implementation CustomerPriceListCell


@synthesize refLabelP;
@synthesize dateLabelP;
@synthesize amountLabelP;
@synthesize statusLabelP;
@synthesize priceLabelP;
@synthesize invoiceP;


- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [self setRefLabelP:nil];
        [self setDateLabelP:nil ];
        [self setAmountLabelP:nil];
        [self setStatusLabelP:nil ];
        [self setPriceLabelP:nil];
        
        [self setRefLabelP:[[UILabel alloc] initWithFrame:CGRectZero] ];
        [self setDateLabelP:[[UILabel alloc] initWithFrame:CGRectZero] ];
        [self setAmountLabelP:[[UILabel alloc] initWithFrame:CGRectZero] ];
        [self setStatusLabelP:[[UILabel alloc] initWithFrame:CGRectZero] ];
        [self setPriceLabelP:[[UILabel alloc] initWithFrame:CGRectZero] ];

        
        [self.refLabelP setBackgroundColor:[UIColor clearColor]];
        [self.dateLabelP setBackgroundColor:[UIColor clearColor]];
        [self.amountLabelP setBackgroundColor:[UIColor clearColor]];
        [self.statusLabelP setBackgroundColor:[UIColor clearColor]];
        [self.priceLabelP setBackgroundColor:[UIColor clearColor]];
        
        [self.contentView addSubview:self.refLabelP];
        [self.contentView addSubview:self.dateLabelP];
        [self.contentView addSubview:self.amountLabelP];
        [self.contentView addSubview:self.statusLabelP];
        [self.contentView addSubview:self.priceLabelP];
        
        [self.refLabelP setFont:BoldFontOfSize(14.0)];
        [self.dateLabelP setFont:LightFontOfSize(14.0f)];
        [self.amountLabelP setFont:LightFontOfSize(14.0f)];
        [self.statusLabelP setFont:LightFontOfSize(14.0f)];
        [self.priceLabelP setFont:LightFontOfSize(14.0f)];
        
        self.dateLabelP.numberOfLines =2;
        
        
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect bounds = [self.contentView bounds];
    
    [self.refLabelP setFrame:CGRectMake(10,  (bounds.size.height / 2) - 23, 70, 45)];
    [self.dateLabelP setFrame:CGRectMake(90,  (bounds.size.height / 2) - 23, bounds.size.width/3, 45)];
    [self.priceLabelP setFrame:CGRectMake(bounds.size.width - 300, (bounds.size.height / 2) - 23, 108, 45)];
    [self.amountLabelP setFrame:CGRectMake(bounds.size.width - 220, (bounds.size.height / 2) - 23, 108, 45)];
    [self.statusLabelP setFrame:CGRectMake(bounds.size.width - 100, (bounds.size.height / 2) - 23, 108, 45)];
  
}
- (void)bindInvoicePrice:(NSDictionary *)i 
{
    [self setInvoiceP:i];
    [self.refLabelP setText:[NSString stringWithFormat:@"%@", [self.invoiceP objectForKey:@"Item_Code"]]];
    [self.dateLabelP setText:[NSString stringWithFormat:@"%@", [self.invoiceP objectForKey:@"Description"]]];

    NSString *sellingString = [NSString stringWithFormat:@"%@",[self.invoiceP currencyStringForKey:@"Unit_Selling_Price"]];
    [self.amountLabelP setText:sellingString];
    
    NSString *listString = [NSString stringWithFormat:@"%@",[self.invoiceP currencyStringForKey:@"Unit_List_Price"]];
    
    [self.statusLabelP setText:listString];
    [self.priceLabelP setText:[NSString stringWithFormat:@"%@", [self.invoiceP objectForKey:@"Item_UOM"]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */



@end
