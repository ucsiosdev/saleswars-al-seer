//
//  ReturnProductCategoryViewController.m
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "ReturnProductCategoryViewController.h"
#import "ReturnNewViewController.h"
#import "AlseerReturnViewController.h"


@interface ReturnProductCategoryViewController ()

@end

@implementation ReturnProductCategoryViewController
//@synthesize serProduct;
@synthesize target;
@synthesize action;



- (id)initWithCustomer:(NSMutableDictionary *)c {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
        customer=c;
       // [self setTitle:NSLocalizedString(@"Product Category & Warehouse", nil)];
        
         [self setTitle:NSLocalizedString(@"Return", nil)];

        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)] ];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [Flurry logEvent:@"Product Category and Warehouse  View"];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
    
    if (@available(iOS 15.0, *)) {
        self.tableView.sectionHeaderTopPadding = 0;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getProductServiceDidGetCategories:[[SWDatabaseManager retrieveManager] dbGetCategoriesForCustomer:customer]];
    
    NSLog(@"did appear array %@", [items description]);
    
    
    if (items.count==1) {
        Singleton *single = [Singleton retrieveSingleton];
        single.orderStartDate = [NSDate date];
        [SWDefaults setProductCategory:[items objectAtIndex:0]];
        AlseerReturnViewController *returnView = [[AlseerReturnViewController alloc] initWithNibName:@"AlseerReturnViewController" bundle:nil] ;
        returnView.customerDict = customer;
        [self.navigationController pushViewController:returnView animated:NO];
    }
}

- (void)cancel:(id)sender {
    
    [self.view.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [self.navigationController  popViewControllerAnimated:YES];
    [self.navigationController setToolbarHidden:YES animated:NO];

}

#pragma mark UITableView Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)section
{
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [cell.textLabel setText:[[items objectAtIndex:indexPath.row] stringForKey:@"Item_No"]];
    [cell.detailTextLabel setText:[[items objectAtIndex:indexPath.row] stringForKey:@"Description"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.font = BoldSemiFontOfSize(14);
    cell.detailTextLabel.font = BoldSemiFontOfSize(12);
    cell.textLabel.textColor = UIColorFromRGB(0x4A5866);
    cell.detailTextLabel.textColor = UIColorFromRGB(0x4687281);
    
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section
{
    GroupSectionHeaderView *sectionHeader = [[GroupSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, tv.bounds.size.width, 48) text:NSLocalizedString(@"", nil)] ;
    return sectionHeader;
}

- (CGFloat)tableView:(UITableView *)tv heightForHeaderInSection:(NSInteger)section {
    return 48.0f;
}

#pragma mark UITableView Delegate
- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[SWDefaults checkMultiUOM]isEqualToString:@"Y"]) {
        /*yet to implement*/
        Singleton *single = [Singleton retrieveSingleton];
        single.orderStartDate = [NSDate date];
        [SWDefaults setProductCategory:[items objectAtIndex:indexPath.row]];
        AlseerReturnViewController *returnView = [[AlseerReturnViewController alloc] initWithNibName:@"AlseerReturnViewController" bundle:nil] ;
        returnView.customerDict = customer;
        [self.navigationController pushViewController:returnView animated:YES];
    }
    else
    {
        Singleton *single = [Singleton retrieveSingleton];
        single.orderStartDate = [NSDate date];
        [SWDefaults setProductCategory:[items objectAtIndex:indexPath.row]];
        ReturnNewViewController *returnView = [[ReturnNewViewController alloc] initWithNibName:@"ReturnNewViewController" bundle:nil] ;
        returnView.customerDict = customer;
        [self.navigationController pushViewController:returnView animated:YES];
    }
}

#pragma mark Product Service Delegate
- (void)getProductServiceDidGetCategories:(NSArray *)categories {
    items=[NSArray arrayWithArray:categories];
    [self.tableView reloadData];

    categories=nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
