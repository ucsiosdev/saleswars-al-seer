//
//  NSDictionary+Additions.h
//  SWFoundation
//
//  Created by Irfan Bashir on 6/27/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)
- (NSString *)currencyStringForKey:(id)key;
- (NSString *)stringForKey:(id)key;
- (NSString *)dateStringForKey:(id)key withDateFormat:(NSDateFormatterStyle)format;
@end


@interface UIImage (Additions)
+(UIImage *)imageNamed:(NSString *)name cache:(BOOL)cache;
@end