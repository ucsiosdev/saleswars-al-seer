//
//  SalesWorxFeedbackViewController.h
//  SalesWars
//
//  Created by Neha Gupta on 1/30/19.
//  Copyright © 2019 msaad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MedRepProductCodeLabel.h"
#import "SalesWorxTableView.h"
#import "MedRepTextField.h"
#import "SalesWorxCustomClass.h"
#import "MedRepElementTitleLabel.h"
#import "MedRepElementDescriptionLabel.h"

@interface SalesWorxFeedbackViewController : UIViewController <UIPopoverControllerDelegate>
{
    VisitOption_Feedback *selectedFeedback;
    
    IBOutlet MedRepElementDescriptionLabel *lblName;
    IBOutlet MedRepElementTitleLabel *lblNumber;
    
    IBOutlet UITableView *itemTableView;
    
    IBOutlet UIView *viewParentAvilableBal;
    NSDictionary *customer;
    BOOL anyUpdated;
    IBOutlet MedRepElementDescriptionLabel *lblAvaialbleBal;
    
    NSMutableArray *itemsArray;
    NSMutableArray *statusArray;
    NSMutableArray *indexPathArray;
    IBOutlet UITableViewCell *tabelHeaderView;
    
    MedRepTextField *selectedTextField;
    NSString *VisitID;
    NSString *latitudePoint, *longitudePoint;
    NSString *imgEnableTickMark;
    NSString *imgDisableTickMark;
    
    IBOutlet UILabel *lblDividerTop;
    IBOutlet UILabel *lblDividerBottom;
}
- (id)initWithCustomer:(NSDictionary *)customer andVisitID:(NSString *)currentVisitID;
@property (strong, nonatomic) IBOutlet UIView *viewParentCustomer;
@property (strong, nonatomic) IBOutlet UIView *viewParentFeedback;

@end
