//
//  ProductOrderViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/12/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"
#import "ProductBonusViewController.h"
#import "ProductStockViewController.h"
#import "ProductListViewController.h"
#import "CustomerSalesViewController.h"


@interface ProductOrderViewController : SWViewController < EditableCellDelegate, ProductStockViewDelegate , UITableViewDelegate , UITableViewDataSource , UIPopoverControllerDelegate , UITextFieldDelegate> {
    NSMutableDictionary *product;
    NSArray *bonusInfo;
    GroupSectionFooterView *priceFooterView;
    UITableView *tableView;
    ProductStockView *stockView;
    NSMutableArray *stockItems;
    UIPopoverController *popoverController;
    UIView *bgTransparentImageView;
    UIView *duesView;
    UIButton *continueVisitButton;
    NSString *bonusString;
    NSMutableDictionary *bonusProduct;
    NSMutableDictionary *fBonusProduct;
    //
    UIViewController *parentViewController;
    NSString *isBonusAppControl;
    NSString *isFOCAppControl;
    NSString *isDiscountAppControl;
    NSString *isFOCItemAppControl;
    NSString *isDualBonusAppControl;
    NSString *bonusEdit;
    NSString *isLineItemNotes;
    NSString *showHistory;
    BOOL isShowHistory;

    ProductHeaderView *productHeaderView;
    ProductListViewController *productListViewController;
    UINavigationController *navigationControllerR;
    ProductBonusViewController *productBonusViewController;
    UINavigationController *navigationControllerS;
    ProductStockViewController *productStockViewController;
    UINavigationController *navigationControllerQ;
    CustomerSalesViewController *customerSalesVC;

    
    //id target;
    SEL action;
    int totalGoods;
    int totaleStock;
    int sectionRow;
    int bonus;
    BOOL isBonus;
    BOOL scrollViewDelegateFreezed;
    BOOL isPriceCel;
    BOOL isComment;
    BOOL isSaveOrder;
    BOOL isPopover;
    BOOL isBonusAllow;
    BOOL isDiscountAllow;
    BOOL isFOCAllow;
    BOOL isFOCItemAllow;
    BOOL isDualBonusAllow;
    BOOL isAddedRows;
    BOOL isBonusRowDeleted;
    BOOL checkDiscount;
    BOOL checkFOC;
    BOOL checkBonus;
    
    
    UIView *myBackgroundView;
}
@property (unsafe_unretained) id target;
@property ( assign) SEL action;
@property (nonatomic, strong)NSMutableDictionary *product;


- (id)initWithProduct:(NSDictionary *)product;
- (void)cancel:(id)sender;
@end
