//
//  SWBarButtonItem.m
//  SWPlatform
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWBarButtonItem.h"
#import "SWFoundation.h"

@implementation UIBarButtonItem (SalesWorx)

+ (UIBarButtonItem *)buttonWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action {
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:style target:target action:action];
    [barButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          BoldSemiFontOfSize(14.0f), UITextAttributeFont,
                                          nil] forState:UIControlStateNormal];
    
    return barButtonItem;
}

+ (UIBarButtonItem *)labelButtonWithTitle:(NSString *)title {

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 180, 25)];
    [label setText:title];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:BoldSemiFontOfSize(14.0f)];

    
    return [[UIBarButtonItem alloc] initWithCustomView:label];
}

+ (UIBarButtonItem *)labelButtonWithLabel:(UILabel *)label {
    return [[UIBarButtonItem alloc] initWithCustomView:label];
}

@end