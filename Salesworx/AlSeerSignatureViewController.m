//
//  AlSeerSignatureViewController.m
//  SalesWorx
//
//  Created by Unique Computer Systems on 5/18/15.
//  Copyright (c) 2015 msaad. All rights reserved.
//

#import "AlSeerSignatureViewController.h"
#import "SignatureViewController.h"
#import "OrderAdditionalInfoViewController.h"
#import "MJPopupBackgroundView.h"
#import "UIViewController+MJPopupViewController.h"
@interface AlSeerSignatureViewController ()

@end

@implementation AlSeerSignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFontColor];
   
    float xTemp = 0;
    float yTemp = 48.5;
    float widthTemp = 470;
    float heightTemp = 271.5;
    
    
    //pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(0, 50, 470, 309)];
    pjrSignView=[[PJRSignatureView alloc]initWithFrame:CGRectMake(xTemp, yTemp, widthTemp, heightTemp)];
    pjrSignView.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    
    //[self testShadow:pjrSignView.layer];
    
    [self.view addSubview:pjrSignView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //table bottom round corner
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:pjrSignView.bounds byRoundingCorners:( UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = pjrSignView.bounds;
    maskLayer.path  = maskPath.CGPath;
    pjrSignView.layer.mask = maskLayer;
}

-(void)setFontColor{
    
    lblSignatureStatic.font = kFontWeblySleekSemiLight(16);
    lblSignatureStatic.textColor = UIColorFromRGB(0x2C394A);
}

/*
-(void)testShadow:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
    
}
*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButtonTapped:(id)sender {
    
    
    //    self.glkView=nil;
    //
    //    [self.glkView erase];
    //
    //    [self.view removeFromSuperview];
    
    
    // [self.navigationController popViewControllerAnimated:YES];
    
    //    OrderAdditionalInfoViewController * orderInfoVC=[[OrderAdditionalInfoViewController alloc]init];
    //
    //    [orderInfoVC closeButtonTapped];
    
    // [(id)self.view.superview closeButtonTapped];
    
    //    NSLog(@"check navigation stack %@", [self.navigationController.viewControllers description]);
    //
    //[self dismissViewControllerAnimated:YES completion:nil];
    //
    
    //[self.delegate dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //
    //    self.delegate=nil;
    
    
    
}




- (NSString*) dbGetImagesDocumentPath
{
    
    //     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //     NSString *path = [paths objectAtIndex:0];
    
    //iOS 8 support
    NSString *path;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        path=[SWDefaults applicationDocumentsDirectory];
    }
    
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        path = [paths objectAtIndex:0];
    }
    
    
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:kImagesFolder]];
    if (![[NSFileManager defaultManager]fileExistsAtPath:path]) {
        [[NSFileManager defaultManager]createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"/"]];
    return path;
}

- (void)signatureSaveImage:(UIImage *)image withName:(NSString*)imageName
{
    
    // saving the signature to documents directory
    
    NSString *path = [self dbGetImagesDocumentPath];
    path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",imageName]];
    [UIImagePNGRepresentation(image) writeToFile:path atomically:NO];
}


- (IBAction)saveButtonTapped:(id)sender {
    
    NSLog(@"save button tapped in signature");
    
    UIImage* signImage=[pjrSignView getSignatureImage];
    NSData *imageData = UIImagePNGRepresentation(signImage);
    [SWDefaults setSignature:imageData];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSObject * object = [prefs objectForKey:@"signature"];
    if(object != nil){
        [SWDefaults showAlertAfterHidingKeyBoard:@"Success" andMessage:@"Signature saved successfully" withController:self];
    }
}

- (IBAction)eraseButtonTapped:(id)sender {
    
    if ([pjrSignView.previousSignature superview] || [pjrSignView.previousSignature superview]){
        [pjrSignView.lblSignature removeFromSuperview];
        
        [SWDefaults clearSignature];
        [pjrSignView.previousSignature removeFromSuperview];
        
    }
    
    signImageView.image=[UIImage imageNamed:@""];
    
    [signImageView removeFromSuperview];
    
    
    [SWDefaults clearSignature];
    
    
    [pjrSignView clearSignature];
}
@end
