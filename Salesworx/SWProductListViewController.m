//
//  SWProductListViewController.m
//  SWProducts
//
//  Created by Irfan Bashir on 7/4/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWProductListViewController.h"
#import "SWPlatform.h"
#import "AlSeerProductDetailViewController.h"
#import "AlSeerProductDetailViewController_2.h"

@interface SWProductListViewController (){
    BOOL isProductDetailsVCDidPush;
}

@end

@implementation SWProductListViewController


-(void)viewWillAppear:(BOOL)animated{
    isProductDetailsVCDidPush = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if ( isProductDetailsVCDidPush ){
        NSLog(@"\n\n *** Hint: did select calling multiple times, So returning from here...  ***\n\n");
        return;
    }
    
    
    
    NSLog(@"\n\n *** SWProductListViewController > did select called  ***\n\n");
    
    SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
    
    if (revealController.currentFrontViewPosition !=0) {
        [revealController revealToggle:self];
    }

    
    NSDictionary * row ;
    if (bSearchIsOn)
    {
        row = [filteredCandyArray objectAtIndex:indexPath.row];
    }
    else
    {
        NSString * countryName = [finalProductArray objectAtIndex:indexPath.section];
        NSArray * objectsForCountry = [productDictionary objectForKey:countryName];
        row= [objectsForCountry objectAtIndex:indexPath.row];
    }
    NSArray *produstDetail = [[SWDatabaseManager retrieveManager] dbGetProductDetail:[row stringForKey:@"ItemID"] organizationId:[row stringForKey:@"OrgID"]];
    
    
    if (produstDetail.count>0) {
        
   
    NSDictionary * product =[NSMutableDictionary dictionaryWithDictionary:[produstDetail objectAtIndex:0]];
    
    
    NSLog(@"\n\n *** product detail before pushing %@  ***\n\n", [product description]);

    
    if ([product stringForKey:@"Inventory_Item_ID"])
        
    {
        NSString *itemID = [product stringForKey:@"Inventory_Item_ID"];
        [product setValue:itemID forKey:@"ItemID"];
    }
  
    if ([product stringForKey:@"Organization_ID"]) {
        NSString *itemID = [product stringForKey:@"Organization_ID"];
        [product setValue:itemID forKey:@"OrgID"];
    }
    
     productDetailViewController = [[ProductDetailViewController alloc] initWithProduct:product] ;
    
    AlSeerProductDetailViewController_2 * productDetailVc=[[AlSeerProductDetailViewController_2 alloc]init];
    productDetailVc.productDict=product;
    isProductDetailsVCDidPush = YES;
    [self.navigationController pushViewController:productDetailVc animated:YES];
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:@"Product details unavailable" andMessage:@"Please try again later" withController:self];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}
@end
