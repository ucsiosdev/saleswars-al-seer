//
//  ProductBonusViewController.h
//  SWCustomer
//
//  Created by Irfan Bashir on 7/15/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWPlatform.h"

@interface ProductBonusViewController : SWViewController {
    ProductBonusView *bonusView;
    NSMutableDictionary *product;
}



- (id)initWithProduct:(NSMutableDictionary *)product;
- (void)updateDateWithProduct:(NSMutableDictionary *)item;

@property(strong,nonatomic)    UIPopoverController *popover;


@end