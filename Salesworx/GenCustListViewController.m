//
//  GenCustListViewController.m
//  SWPlatform
//
//  Created by msaad on 1/9/13.
//  Copyright (c) 2013 UCS Solutions. All rights reserved.
//

//
//  SWGenCustListViewController.m
//  SWPlatform
//
//  Created by Irfan Bashir on 5/13/12.
//  Copyright (c) 2012 UCS Solutions. All rights reserved.
//

#import "SWFoundation.h"
#import "GenCustListViewController.h"
#import "SWSection.h"
#import "LocationFilterViewController.h"
#import "SWCustomerListCell.h"
#import "SWBarButtonItem.h"
#import "PlainSectionHeader.h"
#import "SWDefaults.h"

#define kNavigationFilterLocationButton 1
#define kNavigationFilterCategoryButton 2

@interface GenCustListViewController ()
- (void)locationFilter:(id)sender;
- (void)locationFilterSelected;
- (void)setupToolbar;
- (void)executeSearch:(NSString *)term;
@end

@implementation GenCustListViewController

@synthesize target;
@synthesize action;


- (id)init {
    self = [super init];
    
    if (self) {
       // [self setTitle:NSLocalizedString(@"Customers", nil) ];
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = @"Customers";
        titleLabel.font = headerTitleFont;
        titleLabel.textColor = [UIColor whiteColor];
        [titleLabel sizeToFit];
        self.navigationItem.titleView = titleLabel;
        
        searchData=[NSMutableArray array];
  
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
//    [[self.navigationController navigationBar] setTintColor:UIColorFromRGB(0x2A3949)];
//    [[self.navigationController toolbar] setTintColor:UIColorFromRGB(0x2A3949)];
    [SWDefaults clearCustomer];
    //[self.navigationController setToolbarHidden:NO animated:YES];
    [self setupToolbar];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    loadingView=nil;
    loadingView=[[SWLoadingView alloc] initWithFrame:self.view.bounds] ;
    [loadingView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    loadingView.backgroundColor = [UIColor whiteColor];
    
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(8, 8, self.view.frame.size.width-16 , self.view.frame.size.height-138)];
    
    // table view
    tableView = nil;
    tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,52, subView.bounds.size.width, subView.bounds.size.height-57) style:UITableViewStylePlain] ;
    NSLog(@"tableview frame is %@", NSStringFromCGRect(tableView.frame));
    
    
    [tableView setDataSource:self];
    [tableView setDelegate:self];
    [tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [tableView setSectionIndexColor:UIColorFromRGB(0x169C5C)];
    if (@available(iOS 15.0, *)) {
        tableView.sectionHeaderTopPadding = 0;
    }
    
    // search display controller
    searchBar = nil;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, subView.frame.size.width, 52)] ;
    searchBar.showsCancelButton=YES;
    searchBar.delegate = self;
   
    
    subView.layer.cornerRadius = 8.0;
    subView.layer.masksToBounds = true;
    subView.backgroundColor = [UIColor whiteColor];
   
    subView.layer.shadowColor = [UIColor blackColor].CGColor;
    subView.layer.shadowOffset = CGSizeMake(2, 2);
    subView.layer.shadowOpacity = 0.1;
    subView.layer.shadowRadius = 1.0;
    
    [subView addSubview:searchBar];
    [subView addSubview:tableView];
    [subView addSubview:loadingView];
    /*
    [self.view addSubview:searchBar];
    
    [self.view addSubview:tableView];
    [self.view addSubview:loadingView];
     */
    
    [self addShadowOnCustomLayer:subView.layer];
    [self.view addSubview:subView];
    
    // popovers
    LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
    [locationFilterController setTarget:self];
    [locationFilterController setAction:@selector(locationFilterSelected)];
    [locationFilterController setPreferredContentSize:CGSizeMake(300, self.view.bounds.size.height / 2)];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:locationFilterController] ;
    
    locationFilterPopOver=nil;
    locationFilterPopOver=[[UIPopoverController alloc] initWithContentViewController:navigationController];
    locationFilterPopOver.delegate=self;
    // Count label
    infoLabel=nil;
    infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 25)] ;
    [infoLabel setText:@""];
    [infoLabel setTextAlignment:NSTextAlignmentCenter];
    [infoLabel setBackgroundColor:[UIColor clearColor]];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:BoldSemiFontOfSize(14.0f)];
    

    [self performSelector:@selector(navigatetodeals) withObject:nil afterDelay:0.0];
    flexibleSpace=nil;
    totalLabelButton=nil;
    displayActionBarButton=nil;
    displayMapBarButton=nil;
    
    totalLabelButton = [[UIBarButtonItem alloc] init];

    totalLabelButton = [UIBarButtonItem labelButtonWithLabel:infoLabel];
    
    [totalLabelButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                              kFontWeblySleekSemiBold(16), NSFontAttributeName,
                                              [UIColor redColor], NSForegroundColorAttributeName,[UIColor whiteColor],NSBackgroundColorAttributeName,
                                              nil]forState:UIControlStateNormal];

    
    flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;

    displayActionBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filterActiveOff"] style:UIBarButtonItemStylePlain target:self action:@selector(locationFilter:)] ;
    
    displayMapBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BarCode_Scan"] style:UIBarButtonItemStylePlain target:self action:@selector(displayMap:)] ;
   
    tableView.cellLayoutMarginsFollowReadableWidth = NO;
}


-(void)addShadowOnCustomLayer:(CALayer*)customerLayer{
    
    // drop shadow
    customerLayer.shadowColor = [UIColor blackColor].CGColor;
    customerLayer.shadowOffset = CGSizeMake(3, 3);
    customerLayer.shadowOpacity = 0.1;
    customerLayer.shadowRadius = 1.0;
    customerLayer.masksToBounds = NO;
    
    //round corner
    [customerLayer setCornerRadius:8.0f];
    
}


-(void)navigatetodeals
{
    
    //customerSer.delegate = self;
    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
- (void)setupToolbar {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]>= 7.0){
        self.navigationController.toolbar.barTintColor =UIColorFromRGB(0x169C5C);
        self.navigationController.toolbar.translucent = NO;
        
       
        
        
    }
    NSString *text = [NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"Total Customers", nil), totalRecords];
    if ([[SWDefaults locationFilterForCustomerList] length] > 0) {
        text = [NSString stringWithFormat:@"%@ (filtered by: %@)", text, [SWDefaults locationFilterForCustomerList]];
    }
    
    [infoLabel setText:text];
    
    if (!self.navigationController.toolbarHidden) {
        return;
    }
    
    [totalLabelButton setTitle:[infoLabel text]];


    
    
    [self setToolbarItems:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, totalLabelButton, flexibleSpace, nil]];
    if (@available(iOS 15, *)){
        self.navigationController.toolbar.backgroundColor = [UIColor colorWithRed:(22.0/255.0) green:(156.0/255.0) blue:(92.0/255.0) alpha:1];
    }
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    
    self.navigationItem.rightBarButtonItems =[NSArray arrayWithObjects:displayMapBarButton, displayActionBarButton, nil];
    
    //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:buttonsView]  animated:YES];
}
- (void)displayMap:(id)sender {
    
    Singleton *single = [Singleton retrieveSingleton];
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    single.isBarCode = YES;
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    //reader.showsZBarControls = Y;
    [reader.scanner setSymbology: ZBAR_UPCA config: ZBAR_CFG_ENABLE to: 0];
    
    UIView *customOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)] ;
    customOverlay.backgroundColor = [UIColor clearColor];
    customOverlay.opaque = NO;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init] ;
    toolbar.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    
    
    UIBarButtonItem * logoutButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonBackPressed:)];
    NSMutableArray * arr = [NSMutableArray arrayWithObjects:logoutButton, nil];
    
    [toolbar setItems:arr animated:YES];
    [customOverlay addSubview:toolbar];
    reader.cameraOverlayView =customOverlay ;
    reader.wantsFullScreenLayout = NO;

    
    reader.readerView.zoom = 1.0;
    
    reader.showsZBarControls=NO;
    
    
    
    [self presentViewController: reader animated: YES completion:nil ];
    //[reader release];
    
    
}
- (void)barButtonBackPressed:(id)sender{
    [reader dismissViewControllerAnimated: YES completion:nil];
    
}
- (void)imagePickerController: (UIImagePickerController*) readers didFinishPickingMediaWithInfo: (NSDictionary*) info{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    ZBarSymbol *symbol = nil;
    Singleton *single = [Singleton retrieveSingleton];
    for(symbol in results){
        single.valueBarCode=symbol.data;
        // NSString *upcString = symbol.data;
        LocationFilterViewController *locationFilterController = [[LocationFilterViewController alloc] init] ;
        [locationFilterController setTarget:self];
        [locationFilterController setAction:@selector(filterChangedBarCode)];
        [locationFilterController selectionDone:self];
        [reader dismissViewControllerAnimated: YES completion:nil];
        single.isBarCode = NO;
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [tableView selectRowAtIndexPath:nil animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    if (self.target && [self.target respondsToSelector:self.action]) {
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)] ];
        [self setTitle:NSLocalizedString(@"Select Customer", nil)];
    }
    
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: searchBar.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.0}].CGPath;
    
    searchBar.layer.mask = maskLayer;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight)
        return YES;
    
    return NO;}


- (void)locationFilter:(id)sender {
    
    
    [locationFilterPopOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}
- (void)filterChangedBarCode {
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:NO animated:YES];
   // [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];

}
- (void)locationFilterSelected {
    [locationFilterPopOver dismissPopoverAnimated:YES];
    
    [[SWDatabaseManager retrieveManager] cancel];
    
    [loadingView setHidden:NO];
    [self.navigationController setToolbarHidden:NO animated:YES];
   // [self.navigationItem setRightBarButtonItem:nil animated:YES];
    //customerSer.delegate = self;

    //[SWDatabaseManager retrieveManager] dbGetCollection];
    [self getListFunction:[[SWDatabaseManager retrieveManager] dbGetCollection]];
    
    //Handle filter icon
    if([[NSUserDefaults standardUserDefaults] boolForKey:kUD_IS_MENU_SURVEY_LIST_FILTER_ON]) {
        
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveON"]];
        
    } else {
        [displayActionBarButton setImage:[UIImage imageNamed:@"filterActiveOff"]];
    }
    
    if (searchBar.text.length==0){
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)customerList.count]];
    }else {
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }

}

#pragma mark -SEARCH BAR DELEGATE METHODS
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self executeSearch:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    searchBar.text = @"";
    [self executeSearch:@""];
    [self.view endEditing:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self executeSearch:searchBar.text];
    [self.view endEditing:YES];
}

- (void)executeSearch:(NSString *)term {
    [searchData removeAllObjects];
    NSDictionary *element=[NSDictionary dictionary];
    for(element in customerList)
    {
        NSString *customerName = [element objectForKey:@"Customer_Name"];
        NSRange r = [customerName rangeOfString:term options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)];
        if (r.length > 0)
        {
            [searchData addObject:element];
        }
	}
    sectionSearch=nil;
    sectionSearch = [[NSMutableDictionary alloc] init];
    
    BOOL found;
    
    // Loop through the books and create our keys
    for (NSDictionary *book in searchData)
    {
        NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
        
        found = NO;
        
        for (NSString *str in [sectionSearch allKeys])
        {
            if ([str isEqualToString:c])
            {
                found = YES;
            }
        }
        
        if (!found)
        {
            [sectionSearch setValue:[[NSMutableArray alloc] init] forKey:c];
        }
    }
    
    // Loop again and sort the books into their respective keys
    for (NSDictionary *book in searchData)
    {
        [[sectionSearch objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
    }
    
    // Sort each section array
    for (NSString *key in [sectionSearch allKeys])
    {
        [[sectionSearch objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
    }
    
    if (searchBar.text.length==0){
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)customerList.count]];
    }else {
        [infoLabel setText:[NSString stringWithFormat:@"Products found: %lu", (unsigned long)searchData.count]];
    }
    
     [tableView reloadData];
}
- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark Customer List service delegate

-(void)getListFunction:(NSArray *)temp
{
    [loadingView setHidden:YES];
    customerList = [NSMutableArray arrayWithArray:temp ] ;
       
    if(customerList.count > 0)
    {
        if([[searchBar text] length] > 0){
            [self executeSearch:[searchBar text]];
            [tableView reloadData];
        }
        else
        {
            sectionList=nil;
            sectionList = [[NSMutableDictionary alloc] init];
            
            BOOL found;
            
            // Loop through the books and create our keys
            for (NSDictionary *book in customerList)
            {
                NSString *c = [[book objectForKey:@"Customer_Name"] substringToIndex:1];
                
                found = NO;
                
                for (NSString *str in [sectionList allKeys])
                {
                    if ([str isEqualToString:c])
                    {
                        found = YES;
                    }
                }
                
                if (!found)
                {
                    [sectionList setValue:[[NSMutableArray alloc] init] forKey:c];
                }
            }
            
            // Loop again and sort the books into their respective keys
            for (NSDictionary *book in customerList)
            {
                [[sectionList objectForKey:[[book objectForKey:@"Customer_Name"] substringToIndex:1]] addObject:book];
            }
            
            // Sort each section array
            for (NSString *key in [sectionList allKeys])
            {
                [[sectionList objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"Customer_Name" ascending:YES]]];
            }
            
        }
    }
    else
    {
        [SWDefaults showAlertAfterHidingKeyBoard:NSLocalizedString(@"Error", nil) andMessage:@"No customer found." withController:self];
    }
    totalRecords=customerList.count;
    [tableView reloadData];
    [self setupToolbar];
    temp=nil;
}

#pragma mark -TABLE VIEW DELEGATE METHODS
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tv
{
    if([[searchBar text] length] > 0)
    {
        return [[sectionSearch allKeys] count];
    }
    return [[sectionList allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tv numberOfRowsInSection:(NSInteger)s
{
    if([[searchBar text] length] > 0)
    {
        return [(NSArray*)[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
    }
    // return [customerList count];
    return [(NSArray*)[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:s]] count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *Identifier = @"CUSTOMERLISTCELL";
    SWCustomerListCell *cell = [tv dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil)
    {
        cell = [[SWCustomerListCell alloc] initWithReuseIdentifier:Identifier] ;
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    if([[searchBar text] length] > 0)
    {
        [cell applyAttributes:[[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    else
    {
        [cell applyAttributes:[[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row]];
    }
    
     // [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
      cell.contentView.backgroundColor = [UIColor whiteColor];
     UIView *bgColorView = [[UIView alloc] init];
     bgColorView.backgroundColor = [UIColor colorWithRed:255.0/255 green:239.0/255  blue:198.0/255  alpha:1.0];
     [cell setSelectedBackgroundView:bgColorView];
      return cell;
}


- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section{
    
        NSString *title  = @"";
        if([[searchBar text] length] > 0){
        title = [[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        }else {
           title = [[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
        }
    
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, tv.bounds.size.width, 25)]  ;
        UIButton* result = [UIButton buttonWithType:UIButtonTypeCustom];
        result.frame=CGRectMake(0,0, headerView.bounds.size.width, 40) ;
        result.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        result.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
        [result setTitle:title forState:UIControlStateNormal];
        [result.layer setBorderColor:[UIColor whiteColor].CGColor];
        [result.layer setBorderWidth:1.0f];
        result.titleLabel.font=kFontWeblySleekSemiBold(14);
        [result setTitleColor:UIColorFromRGB(0x169C5C) forState:UIControlStateNormal];
        headerView.backgroundColor= UIColorFromRGB(0xE1E8ED);
        [headerView addSubview:result];
        return headerView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;//28.0f;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath {
    return 50.0f;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tv
{
    if([[searchBar text] length] > 0)
    {
        return [[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
    }
    return [[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[searchBar text] length] > 0){
        
        NSDictionary *row = [[sectionSearch valueForKey:[[[sectionSearch allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        if ([self.target respondsToSelector:self.action])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop

        }
        else
        {
            [SWDefaults setCustomer:[row mutableCopy]];
            
            SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
            
            if (revealController.currentFrontViewPosition !=0) {
                [revealController revealToggle:self];
            }
            SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
            objSurveyVC.customer = [row mutableCopy];
            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
        
    } else
    {
        
        NSDictionary *row = [[sectionList valueForKey:[[[sectionList allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        if ([self.target respondsToSelector:self.action])
        {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            [self.target performSelector:self.action withObject:row];
#pragma clang diagnostic pop

        }
        else
        {
            [SWDefaults setCustomer:[row mutableCopy]];
            SWSplitViewController *revealController = [self.parentViewController.parentViewController isKindOfClass:[SWSplitViewController class]] ? (SWSplitViewController *)self.parentViewController.parentViewController : nil;
            if (revealController.currentFrontViewPosition !=0) {
                [revealController revealToggle:self];
            }
            SurveyFormViewController *objSurveyVC =[[SurveyFormViewController alloc] initWithNibName:@"SurveyFormViewController" bundle:nil] ;
            [self.navigationController pushViewController:objSurveyVC animated:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
   
    cell.textLabel.font=kFontWeblySleekSemiBold(14);
    cell.backgroundColor=[UIColor whiteColor];//UIColorFromRGB(0xF6F7FB);
    cell.textLabel.textColor=UIColorFromRGB(0x2C394A);
    cell.detailTextLabel.font=kFontWeblySleekSemiBold(14);
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self isViewLoaded] && self.view.window == nil)
        
        self.view = nil;
    
    // Dispose of any resources that can be recreated.
}

@end
